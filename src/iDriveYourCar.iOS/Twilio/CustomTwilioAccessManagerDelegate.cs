﻿using System;
using Foundation;
using Twilio.Common;

namespace iDriveYourCar.iOS.Twilio
{
    public class CustomTwilioAccessManagerDelegate : TwilioAccessManagerDelegate
    {
        public CustomTwilioAccessManagerDelegate()
        {
        }
        public override void AccessManager(TwilioAccessManager accessManager, NSError error)
        {
            Console.WriteLine("access manager error");
        }

        public override void AccessManagerTokenExpired(TwilioAccessManager accessManager)
        {
            Console.WriteLine("token expired");
        }
    }
}
