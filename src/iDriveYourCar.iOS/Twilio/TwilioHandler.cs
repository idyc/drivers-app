﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Foundation;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Twilio;
using Twilio.Common;
using Twilio.IPMessaging;

namespace iDriveYourCar.iOS.Twilio
{
    public class TwilioHandler : ITwilioHandler
    {
        private TwilioIPMessagingClient client;
        private TwilioAccessManager accessManager;
        private CustomTwilioIPMessagingClientDelegate clientDelegate;


        public TwilioHandler()
        {
        }

        public event EventHandler<TwilioMessageReceivedEventArgs> MessageReceived;

        public Task<IList<ChatMessage>> GetMessagesAsync(int startIndex, int pageSize, string channel)
        {
            var tcs = new TaskCompletionSource<IList<ChatMessage>>();

            var targetChannel = client.ChannelsList.ChannelWithUniqueName(channel);

            if(startIndex == 0)
            {
                targetChannel.Messages.GetLastMessagesWithCount(
                    (nuint)pageSize,
                    (c, messages) =>
                {
                    if(c.IsSuccessful)
                    {
                        var res = messages.Select(m => new ChatMessage { Body = m.Body, Author = m.Author, DateTime = m.DateUpdatedAsDate.ToDateTime() }).ToList();
                        tcs.SetResult(res);
                    }
                    else
                    {
                        tcs.SetException(new Exception(c.Error.Description));
                    }
                });
            }
            else
            {
                targetChannel.Messages.GetMessagesAfter(
                    (nuint)startIndex,
                    (nuint)pageSize,
                    (c, messages) =>
                {
                    if(c.IsSuccessful)
                    {
                        var res = messages.Select(m => new ChatMessage { Body = m.Body, Author = m.Author, DateTime = m.DateUpdatedAsDate.ToDateTime() }).ToList();
                        tcs.SetResult(res);
                    }
                    else
                    {
                        tcs.SetException(new Exception(c.Error.Description));
                    }
                });
            }

            return tcs.Task;
        }

        public Task Initialize(string token)
        {
            if(this.client == null)
            {
                this.accessManager = TwilioAccessManager.AccessManagerWithToken(token, new CustomTwilioAccessManagerDelegate());
                this.clientDelegate = new CustomTwilioIPMessagingClientDelegate(args => this.MessageReceived?.Invoke(this, args));
                this.client = TwilioIPMessagingClient.IpMessagingClientWithAccessManager(
                    accessManager,
                    new TwilioIPMessagingClientProperties { SynchronizationStrategy = ClientSynchronizationStrategy.All },
                    this.clientDelegate);
            }
            return Task.FromResult(0);
        }

        public Task<bool> TryDeleteChannelAsync(string channel)
        {
            var tcs = new TaskCompletionSource<bool>();

            client.ChannelsList.ChannelWithUniqueName(channel).DestroyWithCompletion(
                c =>
                {
                    tcs.SetResult(c.IsSuccessful);
                });

            return tcs.Task;
        }

        public Task<bool> TryJoinOrCreateChannelAsync(string channel)
        {
            var tcs = new TaskCompletionSource<bool>();

            var targetChannel = client.ChannelsList.ChannelWithUniqueName(channel);
            if(targetChannel != null)
            {
                targetChannel.JoinWithCompletion(
                    c =>
                    {
                        if(c.IsSuccessful)
                            Console.WriteLine("successfully joined general channel!");
                        tcs.SetResult(c.IsSuccessful);
                    });
            }
            else
            {
                var options = new NSDictionary($"{channel} Friendly Name", channel, "TWMChannelOptionType", 0);

                client.ChannelsList.CreateChannelWithOptions(options, (creationResult, ch) =>
                {
                    if(creationResult.IsSuccessful)
                    {
                        targetChannel = ch;

                        targetChannel.JoinWithCompletion(
                            c =>
                            {
                                tcs.SetResult(true);
                                //targetChannel.SetUniqueName("general", res => { });
                            });
                    }
                    else
                        tcs.SetResult(false);
                });
            }

            return tcs.Task;
        }

        public Task<bool> TrySendMessageAsync(string channel, string text)
        {
            var tcs = new TaskCompletionSource<bool>();

            var targetChannel = client.ChannelsList.ChannelWithUniqueName(channel);
            if(targetChannel == null)
                return Task.FromResult(false);

            var msg = targetChannel.Messages.CreateMessageWithBody(text);
            targetChannel.Messages.SendMessage(
                msg,
                r =>
                {
                    tcs.SetResult(r.IsSuccessful);
                });

            return tcs.Task;
        }
    }
}