﻿using System;
using Twilio.IPMessaging;
using iDriveYourCar.Core.Twilio;

namespace iDriveYourCar.iOS.Twilio
{
    public class CustomTwilioIPMessagingClientDelegate : TwilioIPMessagingClientDelegate
    {
        private Action<TwilioMessageReceivedEventArgs> callback;

        public CustomTwilioIPMessagingClientDelegate(Action<TwilioMessageReceivedEventArgs> callback)
        {
            this.callback = callback;
        }

        public override void MessageAdded(TwilioIPMessagingClient client, Channel channel, Message message)
        {
            callback?.Invoke(new TwilioMessageReceivedEventArgs { Channel = channel.UniqueName, Message = message.Body });
        }
    }
}
