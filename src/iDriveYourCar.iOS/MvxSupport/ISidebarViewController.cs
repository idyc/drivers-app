﻿using UIKit;
using SidebarNavigation;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.iOS.MvxSupport
{
    public interface ISidebarViewController
    {
        SidebarController SidebarController { get; }

        void Initialize(UINavigationController navigationController);

        void ChangeContentView(UINavigationController navigationController);

        void SetupSideMenu(ISideMenuViewController menu, MenuLocations menuLocation, bool menuHasShadowing);

        void ToggleMenu();

        bool CloseViewModel(IMvxViewModel viewModel);
    }
}
