﻿using System;
using SidebarNavigation;
namespace iDriveYourCar.iOS.MvxSupport
{
	public class PanelPresentationAttribute : Attribute
	{
		public readonly PresentationMode Mode;

		public readonly bool RequiresForeground;

		public readonly bool WrapInNavigationController;

		public readonly string MenuIconName;

		public readonly MenuLocations MenuLocation;

		public readonly bool MenuHasShadowing;

		public PanelPresentationAttribute(PresentationMode mode, bool requiresForeground)
		{
			this.Mode = mode;
			this.RequiresForeground = requiresForeground;
		}

		public PanelPresentationAttribute(PresentationMode mode, bool requiresForeground, string menuIconName)
			: this(mode, requiresForeground)
		{
			this.MenuIconName = menuIconName;
		}

		public PanelPresentationAttribute(PresentationMode mode, bool requiresForeground, MenuLocations menuLocation, bool menuHasShadowing)
			: this(mode, requiresForeground)
		{
			this.MenuLocation = menuLocation;
			this.MenuHasShadowing = menuHasShadowing;
		}

		public PanelPresentationAttribute(PresentationMode mode, bool requiresForeground, bool wrapInNavigationController)
			: this(mode, requiresForeground)
		{
			this.WrapInNavigationController = wrapInNavigationController;
		}
	}

	public enum PresentationMode
	{
		Root,
		CenterRoot,
		CenterChild,
		SideMenu,
		Modal
	}
}
