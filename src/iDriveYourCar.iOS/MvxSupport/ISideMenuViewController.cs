﻿using System;
using UIKit;

namespace iDriveYourCar.iOS.MvxSupport
{
    public interface ISideMenuViewController
    {
        int MenuWidth { get; }

        void StateChanged();
    }
}
