﻿using System;
using DGenix.Mobile.Fwk.iOS.Views;
using DGenix.Mobile.Fwk.Core.ViewModels;
using UIKit;
using SidebarNavigation;
using MvvmCross.Core.ViewModels;
using DGenix.Mobile.Fwk.iOS.Extensions;

namespace iDriveYourCar.iOS.MvxSupport
{
    public abstract class SidebarViewController<TViewModel> : BaseController<TViewModel>, ISidebarViewController
        where TViewModel : FwkBaseViewModel
    {
        public SidebarController SidebarController { get; private set; }

        public bool HasLeftMenu => SidebarController != null && !(SidebarController.MenuAreaController is MvxInitialEmptySideMenu);

        public bool StatusBarHidden { get; set; }

        public bool ToggleStatusBarHiddenOnOpen { get; set; } = false;

        public void Initialize(UINavigationController navigationController)
        {
            var initialEmptySideMenu = new MvxInitialEmptySideMenu();

            this.SidebarController = new SidebarController(this, navigationController, initialEmptySideMenu);
            this.SidebarController.DisablePanGesture = true;
            this.SidebarController.StateChangeHandler += this.StateChangeHandler;
        }

        public virtual void ToggleMenu()
        {
            this.SidebarController.ToggleMenu();
        }

        public void SetupSideMenu(ISideMenuViewController menu, MenuLocations menuLocation, bool menuHasShadowing)
        {
            this.SidebarController.MenuLocation = menuLocation;
            this.SidebarController.HasShadowing = menuHasShadowing;
            this.SidebarController.MenuWidth = menu.MenuWidth;
            this.SidebarController.HasDarkOverlay = true;
            this.SidebarController.DisablePanGesture = false;

            this.SidebarController.ChangeMenuView(menu.GetViewController());
        }

        public void ChangeContentView(UINavigationController navigationController)
        {
            // TODO: Add following code (or similar) to XamarinSidebar
            //var snapshot = UIScreen.MainScreen.SnapshotView(false);
            //newRoot.View.AddSubview(snapshot);
            //UIView.AnimateNotify(
            //    .4f,
            //    0f,
            //    UIViewAnimationOptions.TransitionCrossDissolve,
            //    () => snapshot.Alpha = 0,
            //    handler => snapshot.RemoveFromSuperview());

            this.SidebarController.ChangeContentView(navigationController);
        }

        private void StateChangeHandler(object sender, bool e)
        {
            (this.SidebarController.MenuAreaController as ISideMenuViewController)?.StateChanged();
        }

        public bool CloseViewModel(IMvxViewModel viewModel)
        {
            // close ViewModel shown in NavigationController

            var navController = this.SidebarController.ContentAreaController as UINavigationController;
            if(navController.TopViewController.GetIMvxIosView().ViewModel == viewModel)
            {
                navController.PopViewController(true);
                return true;
            }
            return false;
        }
    }
}
