﻿using MvvmCross.Platform.Exceptions;
using UIKit;

namespace iDriveYourCar.iOS.MvxSupport
{
	public static class ViewControllerExtensions
	{
		public static ISideMenuViewController GetIMvxSidebarMenu(this UIViewController viewController)
		{
			var sidebarView = viewController as ISideMenuViewController;
			if(sidebarView == null)
			{
				throw new MvxException("Could not get ISideMenuViewController from ViewController!");
			}
			return sidebarView;
		}

		public static UIViewController GetViewController(this ISideMenuViewController sidebarView)
		{
			var viewController = sidebarView as UIViewController;
			if(viewController == null)
			{
				throw new MvxException("Could not get UIViewController from ISideMenuViewController!");
			}
			return viewController;
		}
	}
}

