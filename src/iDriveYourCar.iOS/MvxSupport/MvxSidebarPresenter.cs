﻿using System.Linq;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.MvxHints;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Presenters;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;
using MvvmCross.Platform.Exceptions;
using SidebarNavigation;
using UIKit;

namespace iDriveYourCar.iOS.MvxSupport
{
    public class MvxSidebarPresenter : BaseViewPresenter
    {
        protected UINavigationController CurrentNavigationController;
        protected ISidebarViewController SidebarContainerViewController;

        public MvxSidebarPresenter() : base()
        {
        }

        public MvxSidebarPresenter(UIApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
            this.AppDelegate = applicationDelegate;
            this.Window = window;
        }

        public virtual void ToggleSidebarMenu()
        {
            this.SidebarContainerViewController?.ToggleMenu();
        }

        public override void ChangePresentation(MvxPresentationHint hint)
        {
            if(hint.GetType() == typeof(ClearBackStackHint))
            {
                this.Show(new MvxViewModelRequest(typeof(MyTripsViewModel), null, null, MvxRequestedBy.Unknown));
                return;
            }
            base.ChangePresentation(hint);
        }

        protected override void Show(IMvxIosView view, MvxViewModelRequest request)
        {
            var viewController = this.GetViewController(view);

            var attributes = this.GetPresentationAttributes<PanelPresentationAttribute>(viewController);

            switch(attributes.Mode)
            {
                case PresentationMode.Root:
                    this.ShowRootViewController(viewController, attributes.WrapInNavigationController);
                    break;
                case PresentationMode.CenterRoot:
                    this.ShowCenterRootViewController(viewController, attributes.MenuIconName);
                    break;
                case PresentationMode.CenterChild:
                    this.ShowChildViewController(viewController, attributes.RequiresForeground);
                    break;
                case PresentationMode.SideMenu:
                    this.ShowLeftViewController(viewController, attributes.RequiresForeground, attributes.MenuLocation, attributes.MenuHasShadowing);
                    break;
                case PresentationMode.Modal:
                    this.ShowModalViewController(viewController, attributes.WrapInNavigationController);
                    break;
            }
        }

        protected virtual void ShowRootViewController(UIViewController viewController, bool wrapInNavigationController)
        {
            UIViewController newRoot;

            if(viewController is ISidebarViewController)
            {
                this.CurrentNavigationController = new IDYCNavigationController();

                this.SidebarContainerViewController = viewController as ISidebarViewController;
                this.SidebarContainerViewController.Initialize(this.CurrentNavigationController);

                newRoot = this.SidebarContainerViewController as UIViewController;
            }
            else if(wrapInNavigationController)
            {
                this.SidebarContainerViewController = null;
                this.CurrentNavigationController = new IDYCNavigationController(viewController);

                newRoot = this.CurrentNavigationController;
            }
            else
            {
                this.SidebarContainerViewController = null;
                this.CurrentNavigationController = null;

                newRoot = viewController;
            }

            // set RootViewController
            foreach(var v in this.Window.Subviews)
                v.RemoveFromSuperview();

            this.Window.AddSubview(newRoot.View);
            this.Window.RootViewController = newRoot;
        }

        protected virtual void ShowCenterRootViewController(UIViewController viewController, string menuIconName)
        {
            this.CurrentNavigationController = new IDYCNavigationController(viewController);
            this.SidebarContainerViewController.ChangeContentView(this.CurrentNavigationController);

            if(!string.IsNullOrEmpty(menuIconName))
                viewController.NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(UIImage.FromBundle(menuIconName), UIBarButtonItemStyle.Plain, (sender, e) => { this.SidebarContainerViewController.ToggleMenu(); }), false);
        }

        protected virtual void ShowChildViewController(UIViewController viewController, bool requiresForeground)
        {
            //TODO: Manage requiresForeground!

            if(this.CurrentNavigationController == null)
                throw new MvxException("Trying to show a child view whithout a UINavigationController!");

            this.CurrentNavigationController.DismissViewController(false, null);

            this.CurrentNavigationController.PushViewController(viewController, true);
        }

        protected virtual void ShowLeftViewController(UIViewController viewController, bool requiresForeground, MenuLocations menuLocation, bool menuHasShadowing)
        {
            //TODO: Manage requiresForeground!
            this.SidebarContainerViewController.SetupSideMenu(viewController.GetIMvxSidebarMenu(), menuLocation, menuHasShadowing);
        }

        protected virtual void ShowModalViewController(UIViewController viewController, bool wrapInNavigationController)
        {
            this.PresentModalViewController(wrapInNavigationController ? new UINavigationController(viewController) : viewController, true);
        }

        protected override void Close(IMvxViewModel toClose)
        {
            // check if toClose is a modal ViewController
            if(this.Window.RootViewController.PresentedViewController != null)
            {
                UIViewController viewController;
                UINavigationController navigationController = this.Window.RootViewController.PresentedViewController as UINavigationController;
                viewController = navigationController != null
                                    ? navigationController.TopViewController
                                    : this.Window.RootViewController.PresentedViewController;

                var mvxView = viewController.GetIMvxIosView();

                if(mvxView.ViewModel == toClose)
                {
                    this.Window.RootViewController.DismissViewController(true, null);
                    return;
                }
            }

            // check if ViewModel is shown inside TabBarViewController
            if(this.SidebarContainerViewController != null)
            {
                this.SidebarContainerViewController.CloseViewModel(toClose);
                return;
            }

            // close ViewModel shown in NavigationController
            if(this.CurrentNavigationController.TopViewController.GetIMvxIosView().ViewModel == toClose)
                this.CurrentNavigationController.PopViewController(true);
            else
                this.ClosePreviousController(toClose);
        }

        private void ClosePreviousController(IMvxViewModel toClose)
        {
            foreach(var viewController in this.CurrentNavigationController.ViewControllers)
            {
                var mvxView = viewController.GetIMvxIosView();
                if(mvxView.ViewModel == toClose)
                {
                    var newViewControllers = this.CurrentNavigationController.ViewControllers.Where(v => v != viewController).ToArray();
                    this.CurrentNavigationController.ViewControllers = newViewControllers;
                    break;
                }
            }
        }

        public override UIViewController GetTopViewController(UIViewController windowRoot)
        {
            if(windowRoot as ISidebarViewController == null)
                return base.GetTopViewController(windowRoot);

            var sidebarContentRoot = (windowRoot as ISidebarViewController).SidebarController.ContentAreaController;

            return base.GetTopViewController(sidebarContentRoot);
        }
    }
}
