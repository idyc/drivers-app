using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.iOS.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}