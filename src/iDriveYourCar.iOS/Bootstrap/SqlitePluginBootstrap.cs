using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.iOS.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.Sqlite.PluginLoader, MvvmCross.Plugins.Sqlite.iOS.Plugin>
	{}
}
