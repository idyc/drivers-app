using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.iOS.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}