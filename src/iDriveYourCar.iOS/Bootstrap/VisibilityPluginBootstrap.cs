using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.iOS.Bootstrap
{
    public class VisibilityPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.Visibility.PluginLoader, MvvmCross.Plugins.Visibility.iOS.Plugin>
    {
    }
}