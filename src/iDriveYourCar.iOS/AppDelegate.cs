﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iOS;
using DGenix.Mobile.Fwk.iOS.Handlers;
using DGenix.Mobile.Fwk.iOS.Presenters;
using Foundation;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Models.Push;
using iDriveYourCar.Core.PushNotifications;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Plugins.Color.iOS;
using MvvmCross.Plugins.Json;
using UIKit;
using UserNotifications;
using Xamarin;

namespace iDriveYourCar.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : BaseAppDelegate
    {
        private Lazy<AppConfiguration> appConfiguration = new Lazy<AppConfiguration>(() => new AppConfiguration());
        private Lazy<IIDYCSettings> settings = new Lazy<IIDYCSettings>(() => new IDYCSettings());

        public override UIWindow Window { get; set; }

        public override BaseViewPresenter Presenter { get; set; }

        public override string AppStoreAppId => "";

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            base.FinishedLaunching(application, launchOptions);

            //OutputStringToConsole($"Starting app options: {application.ToString()}");
            //OutputStringToConsole($"Starting app options: {launchOptions == null}");

            application.StatusBarStyle = UIStatusBarStyle.LightContent;

            // create a new window instance based on the screen size
            this.Window = new UIWindow(UIScreen.MainScreen.Bounds);

            this.Presenter = new MvxSidebarPresenter(this, this.Window);
            var setup = new Setup(this, this.Presenter);
            setup.Initialize();

            this.RegisterForPushNotifications();

            var hint = this.GetHintFromLaunchOptions(launchOptions);

            Mvx.Resolve<IMvxAppStart>().Start(hint);

            // make the window visible
            this.Window.MakeKeyAndVisible();

            return true;
        }

        #region Application callbacks
        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
        #endregion

        protected override void SetGlobalAppearance()
        {
            var attributes = new UITextAttributes();
            attributes.TextColor = UIColor.White;
            attributes.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeHuge);
            attributes.TextShadowColor = UIColor.Clear;
            UINavigationBar.Appearance.SetBackgroundImage(new UIImage(), UIBarPosition.Any, UIBarMetrics.Default);
            UINavigationBar.Appearance.ShadowImage = new UIImage();

            UINavigationBar.Appearance.SetTitleTextAttributes(attributes);
            UINavigationBar.Appearance.Translucent = false;
            UINavigationBar.Appearance.BarTintColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();
            UINavigationBar.Appearance.TintColor = UIColor.White;
            UINavigationBar.Appearance.BackgroundColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();
            UINavigationBar.Appearance.BackIndicatorImage = new UIImage();

            UITextField.Appearance.TintColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();
            UITextView.Appearance.TintColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();
        }

        #region Push Notifications
        /// <summary>
        /// Gets the hint to handle the start of the app from launch options.
        /// </summary>
        /// <returns>The hint to handle the start.</returns>
        /// <param name="launchOptions">Options associated to the launch of the app.</param>
        private object GetHintFromLaunchOptions(NSDictionary launchOptions)
        {
            if(launchOptions == null)
                return new StartupHint();

            // Handle Notification
            var optionsRemoteNotificationKey = launchOptions[UIApplication.LaunchOptionsRemoteNotificationKey] as NSDictionary;

            // received notification while app was closed
            return this.ProcessNotification(optionsRemoteNotificationKey, fromFinishedLaunching: true);
        }

        private void RegisterForPushNotifications()
        {
            if(UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // iOS 10 mechanism
                UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound, (approved, err) =>
                {
                    UIApplication.SharedApplication.InvokeOnMainThread(() =>
                    {
                        if(approved)
                            UIApplication.SharedApplication.RegisterForRemoteNotifications();

                    });
                });
            }
            else
            {
                // iOS < 10 mechanism
                var permission = UIUserNotificationSettings.GetSettingsForTypes(
                                   UIUserNotificationType.Alert
                                   | UIUserNotificationType.Badge
                                   | UIUserNotificationType.Sound,
                                   new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(permission);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            // Modify device token for compatibility Azure
            var token = deviceToken.Description.Trim('<', '>').Replace(" ", "");

            // store DeviceToken in Settings
            this.settings.Value.PushNotificationsToken = token;
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            // received notification in foreground!
            var hint = this.ProcessNotification(userInfo, fromFinishedLaunching: false);

            // do something in foreground!
            new PushNotificationNavigator(new AlertHandler(new AppConfiguration()), new MvxJsonConverter())
                .Navigate(hint);
        }

        public override async void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            try
            {
                //OutputStringToConsole("Received silent push!");
                if(userInfo == null || !userInfo.ContainsKey(new NSString(PushConstants.DATA)))
                    return;

                var data = userInfo.ObjectForKey(new NSString(PushConstants.DATA)) as NSDictionary;
                var staffId = long.Parse(data[new NSString(PushConstants.STAFF_MEMBER_ID)].ToString());

                //OutputStringToConsole($"Running service. Operator: {staffId}");

                await this.RunService(staffId);
            }
            catch(Exception e)
            {
                if(Insights.IsInitialized)
                    Insights.Report(e, "Geoposition service", "Exception in RunService");
            }

            //OutputStringToConsole("Service ended successfuly");

            completionHandler(UIBackgroundFetchResult.NewData);
        }

        //const string FoundationLibrary = "/System/Library/Frameworks/Foundation.framework/Foundation";

        //[System.Runtime.InteropServices.DllImport(FoundationLibrary)]
        //extern static void NSLog(IntPtr format, IntPtr s);

        //[System.Runtime.InteropServices.DllImport(FoundationLibrary, EntryPoint = "NSLog")]
        //extern static void NSLog_ARM64(IntPtr format, IntPtr p2, IntPtr p3, IntPtr p4, IntPtr p5, IntPtr p6, IntPtr p7, IntPtr p8, IntPtr s);

        //static readonly bool Is64Bit = IntPtr.Size == 8;
        //static readonly bool IsDevice = ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.DEVICE;

        //static readonly Foundation.NSString nsFormat = new Foundation.NSString(@"%@");

        //static void OutputStringToConsole(string text)
        //{
        //    using(var nsText = new Foundation.NSString(text))
        //    {
        //        if(IsDevice && Is64Bit)
        //        {
        //            NSLog_ARM64(nsFormat.Handle, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, nsText.Handle);
        //        }
        //        else
        //        {
        //            NSLog(nsFormat.Handle, nsText.Handle);
        //        }
        //    }
        //}

        private async Task RunService(long operatorId)
        {
            var locationHelper = Mvx.Resolve<IDriverLocationHelper>();

            await locationHelper.SendDriverLocationToServerAsync(operatorId);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            // Something went wrong while registering!
        }

        private StartupHint ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            this.settings.Value.PushNotificationsCounter++;

            var hint = new StartupHint();

            if(options == null || !options.ContainsKey(new NSString(PushConstants.APS)))
                return hint;

            // grab common attributes
            var aps = new NSDictionary();
            if(options.ContainsKey(new NSString(PushConstants.APS)))
                aps = options.ObjectForKey(new NSString(PushConstants.APS)) as NSDictionary;

            if(aps.ContainsKey(new NSString(PushConstants.ALERT)))
            {
                //string title, body, badge, type;

                var alert = aps[new NSString(PushConstants.ALERT)] as NSDictionary;

                if(alert.ContainsKey(new NSString(PushConstants.TITLE)))
                    hint.Title = alert[new NSString(PushConstants.TITLE)].ToString();

                if(alert.ContainsKey(new NSString(PushConstants.BODY)))
                    hint.Body = alert[new NSString(PushConstants.BODY)].ToString();

                //if(aps.ContainsKey(new NSString(PushConstants.BADGE)))
                //    badge = alert[new NSString(PushConstants.BADGE)].ToString();

                //if(aps.ContainsKey(new NSString(PushConstants.TYPE)))
                //    type = alert[new NSString(PushConstants.TYPE)].ToString();
            }

            // grab trip attributes
            var data = new NSDictionary();
            if(options.ContainsKey(new NSString(PushConstants.DATA)))
                data = options.ObjectForKey(new NSString(PushConstants.DATA)) as NSDictionary;

            if(data.ContainsKey(new NSString(PushConstants.CODE)))
            {
                PushTypes pushType = PushTypes.None;
                if(data.ContainsKey(new NSString(PushConstants.CODE)))
                    pushType = EnumExtensions.GetEnumValueByCode<PushTypes>(data[new NSString(PushConstants.CODE)].ToString());

                switch(pushType)
                {
                    case PushTypes.DriverSignedUp:
                    case PushTypes.ClientSignedUp:
                        hint.InitialScreen = InitialScreen.Referrals;
                        break;

                    case PushTypes.AccountActivated:
                    case PushTypes.InPersonTrainingSchedule:
                    case PushTypes.InPersonTrainingStartsSoon:
                        hint.InitialScreen = InitialScreen.MyTrips;
                        break;

                    case PushTypes.NewTripAvailable:
                    case PushTypes.NewTripAvailableDriverRequested:
                    case PushTypes.NewTripAccepted:
                        hint.InitialScreen = InitialScreen.AcceptTrip;
                        hint.Hint = this.GetTripDataFromNotification(data, PushConstants.TRIP);
                        break;

                    case PushTypes.TripHasBeenModified:
                    case PushTypes.TripHasBeenModifiedScheduleConflict:
                        hint.InitialScreen = InitialScreen.UpcomingTripModificationsDetails;
                        hint.Hint = this.GetTripChangesPushFromNotification(data);
                        break;

                    case PushTypes.TripHasBeenCanceled:
                    case PushTypes.TripHasBeenCanceledFee:
                        hint.InitialScreen = InitialScreen.UpcomingTripModificationsDetails;
                        hint.Hint = this.GetTripChangesPushFromNotification(data);
                        ((TripChangesPush)hint.Hint).IsCanceled = true;
                        break;

                    case PushTypes.TripComingUp:
                        hint.InitialScreen = InitialScreen.UpcomingTripDetails;
                        hint.Hint = this.GetTripIdPushFromNotification(data);
                        break;
                    case PushTypes.TimeToStartTrip:
                        hint.InitialScreen = InitialScreen.CurrentTripDetails;
                        hint.Hint = this.GetTripIdPushFromNotification(data);
                        break;

                    case PushTypes.UrgentMessageFromClient:
                    case PushTypes.ClientHasRequestedPickup:
                        hint.InitialScreen = InitialScreen.MyTrips;
                        break;

                    case PushTypes.CSRMessageReceived:
                    case PushTypes.DispatchTierUpgrade:
                    case PushTypes.ScoreHasFallenBellow:
                    case PushTypes.PayoutHasIncreased:
                        hint.InitialScreen = InitialScreen.MyTrips;
                        break;
                }
            }

            return hint;
        }

        private TripPush GetTripDataFromNotification(NSDictionary data, string key)
        {
            if(string.IsNullOrEmpty(key))
                return null;

            var targetTrip = new TripPush();

            var trip = data[new NSString(key)] as NSDictionary;

            if(trip.ContainsKey(new NSString(PushConstants.DROPOFF_ADDRESS)))
                targetTrip.DropoffAddress = trip[new NSString(PushConstants.DROPOFF_ADDRESS)].ToString();

            if(trip.ContainsKey(new NSString(PushConstants.ESTIMATED_TIME)))
                targetTrip.EstimatedTime = float.Parse(trip[new NSString(PushConstants.ESTIMATED_TIME)].ToString());

            if(trip.ContainsKey(new NSString(PushConstants.ID)))
                targetTrip.Id = long.Parse(trip[new NSString(PushConstants.ID)].ToString());

            if(trip.ContainsKey(new NSString(PushConstants.PICKUP_ADDRESS)))
                targetTrip.PickupAddress = trip[new NSString(PushConstants.PICKUP_ADDRESS)].ToString();

            if(trip.ContainsKey(new NSString(PushConstants.PICKUP_DATE)))
                targetTrip.PickupDate = trip[new NSString(PushConstants.PICKUP_DATE)].ToString();

            if(trip.ContainsKey(new NSString(PushConstants.PICKUP_TIME)))
                targetTrip.PickupTime = trip[new NSString(PushConstants.PICKUP_TIME)].ToString();

            if(trip.ContainsKey(new NSString(PushConstants.TIME_FROM_HOME)))
                targetTrip.TimeFromHome = float.Parse(trip[new NSString(PushConstants.TIME_FROM_HOME)].ToString());

            if(trip.ContainsKey(new NSString(PushConstants.TYPE)))
                targetTrip.Type = EnumExtensions.GetEnumValueByCode<TripType>(trip[new NSString(PushConstants.TYPE)].ToString());

            if(trip.ContainsKey(new NSString(PushConstants.RETURN)))
            {
                var returnTrip = trip[new NSString(PushConstants.RETURN)] as NSDictionary;
                if(returnTrip != null)
                    targetTrip.Return = this.GetTripDataFromNotification(returnTrip, PushConstants.RETURN);
            }

            return targetTrip;
        }

        private TripIdPush GetTripIdPushFromNotification(NSDictionary data)
        {
            var targetTripId = new TripIdPush();

            if(data.ContainsKey(new NSString(PushConstants.TRIP_ID)))
                targetTripId.TripId = long.Parse(data[new NSString(PushConstants.TRIP_ID)].ToString());

            return targetTripId;
        }

        private TripChangesPush GetTripChangesPushFromNotification(NSDictionary data)
        {
            var targetTripId = new TripChangesPush();

            if(data.ContainsKey(new NSString(PushConstants.TRIP_ID)))
                targetTripId.TripId = long.Parse(data[new NSString(PushConstants.TRIP_ID)].ToString());

            //if(data.ContainsKey(new NSString(PushConstants.FIELDS_CHANGED)))
            //    targetTripId.Changes = long.Parse(data[new NSString(PushConstants.FIELDS_CHANGED)].ToString());

            return targetTripId;
        }

        #endregion
    }
}

