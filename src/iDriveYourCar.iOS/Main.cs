﻿using UIKit;
using Xamarin;

namespace iDriveYourCar.iOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {
#if DEBUG
			Insights.Initialize(Insights.DebugModeKey);
#else
            Insights.Initialize("3703be3df45ed3076d9b64cf3591d0f050d09ac1");
#endif
            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            UIApplication.Main(args, null, typeof(AppDelegate).Name);
        }
    }
}