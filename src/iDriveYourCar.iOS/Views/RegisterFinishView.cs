﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.Root, true, true)]
    public class RegisterFinishView : IDYCController<RegisterFinishViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer, viewheaderBackground;

        private UIImageView imgFinishedRegistration;

        private IDYCLabel lblCongratsName, lblYouAreFinished, lblARepresentativeWillContactYouSoon;

        private PrimaryBackgroundButton btnComplete;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Finished;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = "#EEEEEE".ToUIColor();
            this.viewContainer = new UIView();

            this.viewheaderBackground = new UIView { BackgroundColor = "#E1E1E0".ToUIColor() };

            this.imgFinishedRegistration = new UIImageView(UIImage.FromBundle(Assets.CheckWhiteBig));

            this.lblCongratsName = new IDYCLabel(Metrics.FontSizeHuge)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblYouAreFinished = new IDYCLabel(Metrics.FontSizeBig)
            {
                Text = IDriveYourCarStrings.YouAreFinished,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblARepresentativeWillContactYouSoon = new IDYCLabel(Metrics.FontSizeBig)
            {
                Text = IDriveYourCarStrings.ARepresentativeWillContactYouSoon,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.btnComplete = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnComplete.SetTitle(IDriveYourCarStrings.Complete.ToUpper(), UIControlState.Normal);

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(this.viewheaderBackground,
                                                 this.lblCongratsName,
                                                 this.lblYouAreFinished,
                                                 this.lblARepresentativeWillContactYouSoon,
                                                 this.btnComplete);
            this.viewheaderBackground.AggregateSubviews(this.imgFinishedRegistration);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.viewheaderBackground.AtTopOf(this.viewContainer),
                this.viewheaderBackground.WithSameWidth(this.viewContainer),
                this.viewheaderBackground.Height().EqualTo(150f),

                this.lblCongratsName.Below(this.viewheaderBackground, CONTROLS_DISTANCE),
                this.lblCongratsName.WithSameCenterX(this.viewContainer),

                this.lblYouAreFinished.Below(this.lblCongratsName, CONTROLS_DISTANCE / 2),
                this.lblYouAreFinished.WithSameCenterX(this.viewContainer),

                this.lblARepresentativeWillContactYouSoon.Below(this.lblYouAreFinished, CONTROLS_DISTANCE / 4),
                this.lblARepresentativeWillContactYouSoon.WithSameCenterX(this.viewContainer),

                this.btnComplete.Below(this.lblARepresentativeWillContactYouSoon, CONTROLS_DISTANCE * 2),
                this.btnComplete.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnComplete.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnComplete.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );

            this.viewheaderBackground.AddConstraints(
                this.imgFinishedRegistration.WithSameCenterX(this.viewheaderBackground),
                this.imgFinishedRegistration.WithSameCenterY(this.viewheaderBackground)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterFinishView, RegisterFinishViewModel>();
            set.Bind(this.lblCongratsName).To(vm => vm.CongratsName);
            set.Bind(this.btnComplete).To(vm => vm.CompleteRegistrationCommand);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.CompleteRegistrationTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}

