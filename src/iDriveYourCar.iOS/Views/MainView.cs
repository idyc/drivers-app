﻿using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.Root, true)]
    public class MainView : SidebarViewController<MainViewModel>
    {
        public MainView()
            : base()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
            this.AddLoadingOverlayBinding(this, nameof(this.ViewModel.LoadDriverDataFromServerSpinnerTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        bool firstTime = true;

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if(firstTime)
            {
                this.ViewModel.ShowInitialViewModelsCommand.Execute(null);

                firstTime = false;
            }
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
