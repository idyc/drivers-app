﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using UIKit;
using CoreGraphics;
using MvvmCross.Plugins.Color.iOS;

namespace iDriveYourCar.iOS.Views
{
    public class CompletedTripsView : IDYCController<CompletedTripsViewModel>
    {
        private const float VIEW_HEADER_PADDING = 16f;
        private const float CONTROLS_DISTANCE_VERTICAL = 12f;
        private const float TOTALS_TOP_PADDING = 4f;
        private const float HEADER_SIDES_PADDING = 44f;

        private UIScrollView scrollView;
        private UIView viewContainer;

        private UIView viewHeader, viewCompletedTrips, viewWeeklyRanking;
        private IDYCMultilineLabel lblWeeklyEarnings, lblCompletedTrips, lblWeeklyRanking;
        private IDYCLabel lblWeeklyEarningsValue, lblCompletedTripsValue, lblWeeklyRankingValue;

        private UIView viewLineHeader;

        private SimpleBindableStackView<CompletedTripDayItemView, CompletedTripsDayItemViewModel> daysStack;

        private IDYCLabel btnToggleWeek;

        public CompletedTripsView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView
            {
                DelaysContentTouches = false,
                BackgroundColor = UIColor.White
            };
            this.viewContainer = new UIView();

            this.viewHeader = new UIView();
            this.viewCompletedTrips = new UIView();
            this.viewWeeklyRanking = new UIView();
            this.lblWeeklyEarnings = new IDYCMultilineLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.WeeklyEarnings,
                TextAlignment = UITextAlignment.Center
            };
            this.lblWeeklyEarningsValue = new IDYCLabel(42f)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center
            };

            this.lblCompletedTrips = new IDYCMultilineLabel(Metrics.FontSizeSmall)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.CompletedTrips
            };
            this.lblCompletedTripsValue = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center
            };
            this.lblWeeklyRanking = new IDYCMultilineLabel(Metrics.FontSizeSmall)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.WeeklyRating,
                TextAlignment = UITextAlignment.Right
            };
            this.lblWeeklyRankingValue = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center
            };
            this.viewLineHeader = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.daysStack = new SimpleBindableStackView<CompletedTripDayItemView, CompletedTripsDayItemViewModel>(UILayoutConstraintAxis.Vertical, 0f);
            this.btnToggleWeek = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor(),
                UserInteractionEnabled = true
            };
            this.btnToggleWeek.AddGestureRecognizer(new UITapGestureRecognizer(
            () =>
            {
                this.ViewModel.GetDataCommand.Execute(null);
                this.scrollView.SetContentOffset(new CGPoint(0, -this.scrollView.ContentInset.Top), true);
            }));

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(this.viewHeader, this.daysStack, this.btnToggleWeek);
            this.viewHeader.AggregateSubviews(
                this.lblWeeklyEarnings,
                this.lblWeeklyEarningsValue,
                this.viewCompletedTrips,
                this.viewWeeklyRanking,
                this.viewLineHeader);
            this.viewCompletedTrips.AggregateSubviews(this.lblCompletedTrips, this.lblCompletedTripsValue);
            this.viewWeeklyRanking.AggregateSubviews(this.lblWeeklyRanking, this.lblWeeklyRankingValue);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.viewHeader.AtLeftOf(this.viewContainer),
                this.viewHeader.AtTopOf(this.viewContainer),
                this.viewHeader.AtRightOf(this.viewContainer),

                this.daysStack.Below(this.viewHeader),
                this.daysStack.AtLeftOf(this.viewContainer),
                this.daysStack.AtRightOf(this.viewContainer),

                this.btnToggleWeek.Below(this.daysStack, CONTROLS_DISTANCE_VERTICAL),
                this.btnToggleWeek.WithSameCenterX(this.viewContainer),
                this.btnToggleWeek.AtBottomOf(this.viewContainer)
            );

            this.viewHeader.AddConstraints(
                this.lblWeeklyEarnings.AtTopOf(this.viewHeader, VIEW_HEADER_PADDING),
                this.lblWeeklyEarnings.AtLeftOf(this.viewHeader, VIEW_HEADER_PADDING),
                this.lblWeeklyEarnings.AtRightOf(this.viewHeader, VIEW_HEADER_PADDING),

                this.lblWeeklyEarningsValue.Below(this.lblWeeklyEarnings),
                this.lblWeeklyEarningsValue.AtLeftOf(this.viewHeader, VIEW_HEADER_PADDING),
                this.lblWeeklyEarningsValue.AtRightOf(this.viewHeader, VIEW_HEADER_PADDING),

                this.viewCompletedTrips.Below(this.lblWeeklyEarningsValue, CONTROLS_DISTANCE_VERTICAL),
                this.viewCompletedTrips.AtLeftOf(this.viewHeader),
                this.viewCompletedTrips.WithRelativeWidth(this.viewHeader, .5f),
                this.viewHeader.Bottom().GreaterThanOrEqualTo(VIEW_HEADER_PADDING).BottomOf(this.viewCompletedTrips),

                this.viewWeeklyRanking.Below(this.lblWeeklyEarningsValue, CONTROLS_DISTANCE_VERTICAL),
                this.viewWeeklyRanking.AtRightOf(this.viewHeader),
                this.viewWeeklyRanking.WithRelativeWidth(this.viewHeader, .5f),
                this.viewHeader.Bottom().GreaterThanOrEqualTo(VIEW_HEADER_PADDING).BottomOf(this.viewWeeklyRanking),

                this.viewLineHeader.AtLeftOf(this.viewHeader),
                this.viewLineHeader.AtBottomOf(this.viewHeader),
                this.viewLineHeader.AtRightOf(this.viewHeader),
                this.viewLineHeader.Height().EqualTo(Metrics.LineThickness)
            );

            this.viewCompletedTrips.AddConstraints(
                this.lblCompletedTrips.AtLeftOf(this.viewCompletedTrips, HEADER_SIDES_PADDING),
                this.lblCompletedTrips.AtTopOf(this.viewCompletedTrips, TOTALS_TOP_PADDING),
                this.lblCompletedTrips.Right().LessThanOrEqualTo(TOTALS_TOP_PADDING).RightOf(this.viewCompletedTrips),

                this.lblCompletedTripsValue.Below(this.lblCompletedTrips),
                this.lblCompletedTripsValue.WithSameCenterX(this.lblCompletedTrips),
                this.lblCompletedTripsValue.AtBottomOf(this.viewCompletedTrips)
            );

            this.viewWeeklyRanking.AddConstraints(
                this.lblWeeklyRanking.Left().GreaterThanOrEqualTo(TOTALS_TOP_PADDING).LeftOf(this.viewWeeklyRanking),
                this.lblWeeklyRanking.AtTopOf(this.viewWeeklyRanking, TOTALS_TOP_PADDING),
                this.lblWeeklyRanking.AtRightOf(this.viewWeeklyRanking, HEADER_SIDES_PADDING),

                this.lblWeeklyRankingValue.Below(this.lblWeeklyRanking),
                this.lblWeeklyRankingValue.WithSameCenterX(this.lblWeeklyRanking),
                this.lblWeeklyRankingValue.AtBottomOf(this.viewWeeklyRanking)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CompletedTripsView, CompletedTripsViewModel>();
            set.Bind(this.lblWeeklyEarningsValue).To(vm => vm.WeeklyEarnings).WithConversion(ValueConverters.DecimalToMoneyConverter);
            set.Bind(this.lblCompletedTripsValue).To(vm => vm.CompletedTripsCount);
            set.Bind(this.lblWeeklyRankingValue).To(vm => vm.WeeklyRating);
            set.Bind(this.btnToggleWeek).To(vm => vm.IsLastWeekActive).WithConversion(ValueConverters.BoolToThisWeekLastWeekConverter);
            set.Bind(this.daysStack).For(v => v.ItemsSource).To(vm => vm.CompletedTrips);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
