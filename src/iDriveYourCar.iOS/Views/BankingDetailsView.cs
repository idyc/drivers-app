﻿using System.Linq;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.Root, true, true)]
    public class BankingDetailsView : IDYCController<BankingDetailsViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;
        private const float HEADER_PADDING = 8f;

        private UIScrollView scrollView;

        private UIView viewContainer, headerContainer;

        private IDYCLabel lblCompleteBankingDetailsToActivateYourAccount;
        private BorderedTextField txtNameOnAccount, txtSSNTIN, txtRoutingNumber, txtAccountNumber;
        private BorderedIconTextField txtTaxClassification;
        private PrimaryBackgroundButton btnSave;

        public override bool HandlesKeyboardNotifications() => true;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Banking;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = IDYCColors.BackgroundColor;

            this.viewContainer = new UIView();
            this.View.AddGestureRecognizer(new UITapGestureRecognizer(() => this.View.EndEditing(true)) { CancelsTouchesInView = false });

            this.headerContainer = new UIView
            {
                BackgroundColor = "#E8412E".ToUIColor(),
                ClipsToBounds = true
            };

            this.lblCompleteBankingDetailsToActivateYourAccount = new IDYCLabel(Metrics.FontSizeSmall)
            {
                Text = IDriveYourCarStrings.CompleteBankingDetailsToActivateYourAccount,
                TextColor = UIColor.White
            };

            this.txtNameOnAccount = new BorderedTextField(IDriveYourCarStrings.NameOnAccount);

            this.txtTaxClassification = new BorderedIconTextField(IDriveYourCarStrings.TaxClassification, Assets.ArrowDown) { UserInteractionEnabled = true };
            this.txtTaxClassification.TextField.Enabled = false;

            this.txtSSNTIN = new BorderedTextField(IDriveYourCarStrings.SSNTIN);
            this.txtSSNTIN.TextField.KeyboardType = UIKeyboardType.NumberPad;

            this.txtRoutingNumber = new BorderedTextField(IDriveYourCarStrings.RoutingNumber);
            this.txtRoutingNumber.TextField.KeyboardType = UIKeyboardType.NumberPad;

            this.txtAccountNumber = new BorderedTextField(IDriveYourCarStrings.AccountNumber);
            this.txtAccountNumber.TextField.KeyboardType = UIKeyboardType.NumberPad;

            this.btnSave = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnSave.SetTitle(IDriveYourCarStrings.Save.ToUpper(), UIControlState.Normal);

            this.View.AggregateSubviews(this.scrollView);

            this.scrollView.AggregateSubviews(this.viewContainer);

            this.viewContainer.AggregateSubviews(
                this.headerContainer,
                this.txtNameOnAccount,
                this.txtTaxClassification,
                this.txtSSNTIN,
                this.txtRoutingNumber,
                this.txtAccountNumber,
                this.btnSave
            );

            this.headerContainer.AggregateSubviews(
                this.lblCompleteBankingDetailsToActivateYourAccount
            );

            this.txtTaxClassification.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    if(!this.ViewModel.TaxAdapter.Items.Any())
                        return;

                    var taxNames = this.ViewModel.TaxAdapter.Items.Select(p => p.Name).ToList();
                    var modalPicker = new ModalPickerViewController(
                        ModalPickerType.Custom,
                        string.Empty,
                        this,
                        Strings.Ok,
                        Strings.Cancel,
                        UIColor.White,
                        IDYCColors.DarkGrayTextColor,
                        UIColor.White);
                    modalPicker.PickerView.Model = new CustomPickerModel(taxNames);

                    var selectedTaxId = this.ViewModel.TaxAdapter.Items.First(c => c.Name == this.ViewModel.TaxAdapter.SelectedItem.Name);

                    modalPicker.PickerView.Select(taxNames.IndexOf(selectedTaxId.Name), 0, true);

                    modalPicker.OnModalPickerDismissed += (s, ea) =>
                    {
                        var index = modalPicker.PickerView.SelectedRowInComponent(0);
                        this.ViewModel.TaxAdapter.SelectedItem = this.ViewModel.TaxAdapter.Items.First(c => c.Name == taxNames[(int)index]);
                    };

                    this.PresentViewController(modalPicker, true, null);
                }));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.headerContainer.AtTopOf(this.viewContainer),
                this.headerContainer.AtLeftOf(this.viewContainer),
                this.headerContainer.AtRightOf(this.viewContainer),

                this.txtNameOnAccount.Below(this.headerContainer, CONTROLS_DISTANCE),
                this.txtNameOnAccount.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.txtNameOnAccount.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.txtTaxClassification.Below(this.txtNameOnAccount, CONTROLS_DISTANCE),
                this.txtTaxClassification.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.txtTaxClassification.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.txtSSNTIN.Below(this.txtTaxClassification, CONTROLS_DISTANCE),
                this.txtSSNTIN.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.txtSSNTIN.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.txtRoutingNumber.Below(this.txtSSNTIN, CONTROLS_DISTANCE),
                this.txtRoutingNumber.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.txtRoutingNumber.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.txtAccountNumber.Below(this.txtRoutingNumber, CONTROLS_DISTANCE),
                this.txtAccountNumber.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.txtAccountNumber.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnSave.Below(this.txtAccountNumber, CONTROLS_DISTANCE),
                this.btnSave.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnSave.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnSave.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );

            this.headerContainer.AddConstraints(
                this.lblCompleteBankingDetailsToActivateYourAccount.AtTopOf(this.headerContainer, HEADER_PADDING),
                this.lblCompleteBankingDetailsToActivateYourAccount.AtLeftOf(this.headerContainer, CONTROLS_DISTANCE),
                this.lblCompleteBankingDetailsToActivateYourAccount.AtRightOf(this.headerContainer, CONTROLS_DISTANCE),
                this.lblCompleteBankingDetailsToActivateYourAccount.AtBottomOf(this.headerContainer, HEADER_PADDING)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<BankingDetailsView, BankingDetailsViewModel>();

            set.Bind(this.txtNameOnAccount.TextField).To(vm => vm.NameBankAccount);
            set.Bind(this.txtTaxClassification.TextField).To(vm => vm.TaxAdapter.SelectedItem.Name);
            set.Bind(this.txtSSNTIN.TextField).To(vm => vm.SSNTIN);
            set.Bind(this.txtRoutingNumber.TextField).To(vm => vm.RoutingNumber);
            set.Bind(this.txtAccountNumber.TextField).To(vm => vm.AccountNumber);
            set.Bind(this.btnSave).To(vm => vm.SaveCommand);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SaveTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.ScrollToCenterOnKeyboardShown = this.scrollView;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
