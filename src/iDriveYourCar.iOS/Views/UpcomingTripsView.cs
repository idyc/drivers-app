﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.Views.CustomViews;
using iDriveYourCar.iOS.Views.Sources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;
using WenchaoD.FSCalendar;

namespace iDriveYourCar.iOS.Views
{
    public class UpcomingTripsView : IDYCController<UpcomingTripsViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UITableView tblUpcomingTrips;
        private UIView viewHeader, viewHeaderContainer;
        private UpcomingTripsTableSource source;

        private UIImageView imgPlaceholder;
        private IDYCLabel lblYouhaveNoUpcomingTrips;
        private PrimaryTextButton btnFindOpenTrips;

        private UpcomingTripsHeaderView tableHeader, calendarHeader;

        private UIView viewCalendar;
        private FSCalendar calendar;

        public UpcomingTripsView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.View.BackgroundColor = IDYCColors.BackgroundColor;

            this.ViewModel.PropertyChanged += this.UpcomingTripsViewModel_PropertyChanged;

            this.tblUpcomingTrips = new AutomaticDimensionTableView(72f)
            {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                BackgroundColor = UIColor.Clear
            };
            this.source = new UpcomingTripsTableSource(this.tblUpcomingTrips);
            this.source.DeselectAutomatically = true;
            this.tblUpcomingTrips.Source = this.source;

            this.imgPlaceholder = new UIImageView(UIImage.FromBundle(Assets.Plane));
            this.lblYouhaveNoUpcomingTrips = new IDYCLabel(Metrics.FontSizeNormalPlus)
            {
                Text = IDriveYourCarStrings.YouHaveNoUpcomingTrips,
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.btnFindOpenTrips = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormalPlus);
            this.btnFindOpenTrips.SetTitle(IDriveYourCarStrings.FindOpenTrips, UIControlState.Normal);

            this.viewCalendar = new UIView { BackgroundColor = UIColor.White };

            this.calendar = new FSCalendar();
            this.calendar.ScrollDirection = FSCalendarScrollDirection.Vertical;
            this.calendar.HeaderHeight = 0f;
            this.calendar.DataSource = new CalendarDataSource(date => this.ViewModel.HasUpcomingTripsForDate(date));
            this.calendar.Delegate = new CalendarDelegate(this.ViewModel);

            this.viewHeader = new UIView();
            this.viewHeaderContainer = new UIView();
            this.tblUpcomingTrips.TableHeaderView = this.viewHeader;

            this.tableHeader = new UpcomingTripsHeaderView(Assets.List, Assets.Calendar);
            this.calendarHeader = new UpcomingTripsHeaderView(Assets.List, Assets.Calendar);

            this.View.AggregateSubviews(this.tblUpcomingTrips, this.imgPlaceholder, this.lblYouhaveNoUpcomingTrips, this.btnFindOpenTrips, this.viewCalendar);
            this.viewHeader.AggregateSubviews(this.viewHeaderContainer);
            this.viewHeaderContainer.AggregateSubviews(this.tableHeader);
            this.viewCalendar.AggregateSubviews(this.calendarHeader, this.calendar);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.viewCalendar.AtLeftOf(this.View),
                this.viewCalendar.AtTopOf(this.View),
                this.viewCalendar.AtRightOf(this.View),
                this.viewCalendar.AtBottomOf(this.View),

                this.tblUpcomingTrips.AtLeftOf(this.View),
                this.tblUpcomingTrips.AtTopOf(this.View),
                this.tblUpcomingTrips.AtRightOf(this.View),
                this.tblUpcomingTrips.AtBottomOf(this.View),

                this.imgPlaceholder.WithSameCenterY(this.View),
                this.imgPlaceholder.WithSameCenterX(this.View),

                this.lblYouhaveNoUpcomingTrips.WithSameCenterX(this.View),
                this.lblYouhaveNoUpcomingTrips.Below(this.imgPlaceholder, Metrics.WINDOW_MARGIN * 3),

                this.btnFindOpenTrips.WithSameCenterX(this.View),
                this.btnFindOpenTrips.Below(this.lblYouhaveNoUpcomingTrips, Metrics.WINDOW_MARGIN)
            );

            this.viewHeader.AddConstraints(
                this.viewHeaderContainer.AtLeftOf(this.viewHeader, Metrics.WINDOW_MARGIN),
                this.viewHeaderContainer.AtTopOf(this.viewHeader, Metrics.WINDOW_MARGIN * 2f),
                this.viewHeaderContainer.AtRightOf(this.viewHeader, Metrics.WINDOW_MARGIN),
                this.viewHeaderContainer.AtBottomOf(this.viewHeader, Metrics.WINDOW_MARGIN)
            );

            this.viewHeaderContainer.AddConstraints(
                this.tableHeader.AtLeftOf(this.viewHeaderContainer),
                this.tableHeader.AtTopOf(this.viewHeaderContainer),
                this.tableHeader.AtRightOf(this.viewHeaderContainer),
                this.tableHeader.AtBottomOf(this.viewHeaderContainer)
            );

            this.viewCalendar.AddConstraints(
                this.calendarHeader.AtLeftOf(this.viewCalendar, Metrics.WINDOW_MARGIN),
                this.calendarHeader.AtTopOf(this.viewCalendar, Metrics.WINDOW_MARGIN * 2f),
                this.calendarHeader.AtRightOf(this.viewCalendar, Metrics.WINDOW_MARGIN),

                this.calendar.Below(this.calendarHeader, CONTROLS_DISTANCE),
                this.calendar.AtLeftOf(this.viewCalendar, Metrics.WINDOW_MARGIN),
                this.calendar.AtRightOf(this.viewCalendar, Metrics.WINDOW_MARGIN),
                this.calendar.AtBottomOf(this.viewCalendar)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<UpcomingTripsView, UpcomingTripsViewModel>();
            set.Bind(this.source).For(v => v.ItemsSource).To(vm => vm.UpcomingTrips);
            set.Bind(this.source).For(v => v.SelectionChangedCommand).To(vm => vm.DisplayUpcomingTripDetailCommand);
            set.Bind(this.tableHeader.LblDate).To(vm => vm.CurrentDate).WithConversion(FwkValueConverters.DateTimeToStringConverter, "DateWithDayAndMonthDescription");
            set.Bind(this.tableHeader.ViewLeft.Tap()).For(g => g.Command).To(vm => vm.ShowListCommand);
            set.Bind(this.tableHeader.ViewRight.Tap()).For(g => g.Command).To(vm => vm.ShowCalendarCommand);
            set.Bind(this.calendarHeader.ViewLeft.Tap()).For(g => g.Command).To(vm => vm.ShowListCommand);
            set.Bind(this.calendarHeader.ViewRight.Tap()).For(g => g.Command).To(vm => vm.ShowCalendarCommand);
            set.Bind(this.calendarHeader.LblDate).To(vm => vm.CurrentMonth).WithConversion(FwkValueConverters.DateTimeToStringConverter, "MonthYear");
            set.Bind(this.tableHeader.ViewLeft).For(v => v.BackgroundColor).To(vm => vm.IsListVisible).WithConversion(ValueConverters.BoolToBackgroundColorConverter);
            set.Bind(this.tableHeader.ViewRight).For(v => v.BackgroundColor).To(vm => vm.IsListVisible).WithConversion(ValueConverters.BoolToBackgroundColorConverter, true);
            set.Bind(this.calendarHeader.ViewLeft).For(v => v.BackgroundColor).To(vm => vm.IsListVisible).WithConversion(ValueConverters.BoolToBackgroundColorConverter);
            set.Bind(this.calendarHeader.ViewRight).For(v => v.BackgroundColor).To(vm => vm.IsListVisible).WithConversion(ValueConverters.BoolToBackgroundColorConverter, true);
            set.Bind(this.tblUpcomingTrips).For(v => v.Hidden).To(vm => vm.IsListVisible).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter);
            set.Bind(this.imgPlaceholder).For(v => v.Hidden).To(vm => vm.HasUpcomingTrips);
            set.Bind(this.lblYouhaveNoUpcomingTrips).For(v => v.Hidden).To(vm => vm.HasUpcomingTrips);
            set.Bind(this.btnFindOpenTrips).For(v => v.Hidden).To(vm => vm.HasUpcomingTrips);
            set.Bind(this.btnFindOpenTrips).To(vm => vm.ShowFindOpenTripsCommand);
            set.Bind(this.viewCalendar).For(v => v.Hidden).To(vm => vm.IsListVisible);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            if (this.ViewModel.Driver != null)
                this.calendar.ReloadData();

            this.calendar.SelectDate(this.ViewModel.CurrentDate.ToNsDate());
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                this.ViewModel.PropertyChanged -= this.UpcomingTripsViewModel_PropertyChanged;

            base.Dispose(disposing);
        }

        void UpcomingTripsViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(this.ViewModel.Driver):
                    if (this.ViewModel.Driver == null)
                        break;

                    //if(this.calendar.DataSource == null)
                    //    this.calendar.DataSource = new CalendarDataSource(date => this.ViewModel.HasUpcomingTripsForDate(date));
                    //else
                    this.calendar.ReloadData();

                    break;

                case nameof(this.ViewModel.CurrentDate):
                    if (this.calendar.SelectedDate.ToDateTime() != this.ViewModel.CurrentDate)
                        this.calendar.SelectDate(this.ViewModel.CurrentDate.ToNsDate());
                    break;
            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            this.tblUpcomingTrips.SetupDynamicHeightTableViewHeader();
        }
    }

    public class CalendarDelegate : FSCalendarDelegate
    {
        private UpcomingTripsViewModel viewModel;

        public CalendarDelegate(UpcomingTripsViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public override void CalendarCurrentMonthDidChange(FSCalendar calendar)
        {
            if (calendar.CurrentPage != null)
                this.viewModel.CurrentMonth = calendar.CurrentPage.ToDateTime();
        }

        public override void DidSelectDate(FSCalendar calendar, Foundation.NSDate date)
        {
            this.viewModel.CurrentDate = calendar.SelectedDate.ToDateTime();
            this.viewModel.ShowListCommand.Execute(null);
        }
    }

    public class CalendarDataSource : FSCalendarDataSource
    {
        private Func<DateTime, bool> hasUpcomingTripsForDate;

        public CalendarDataSource(Func<DateTime, bool> hasUpcomingTripsForDate)
        {
            this.hasUpcomingTripsForDate = hasUpcomingTripsForDate;
        }

        public override bool HasEventForDate(FSCalendar calendar, Foundation.NSDate date)
        {
            if (date == null)
                return false;

            return this.hasUpcomingTripsForDate.Invoke(date.ToDateTime());
        }

        public override Foundation.NSDate MinimumDateForCalendar(FSCalendar calendar)
        {
            return DateTime.Today.Date.ToNsDate();
        }
    }
}
