﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Helpers;
using FFImageLoading;
using FFImageLoading.Transformations;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using MvvmCross.Platform;
using MvvmCross.Plugins.Color.iOS;
using MvvmCross.Plugins.Messenger;
using SidebarNavigation;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.SideMenu, true, MenuLocations.Left, false)]
    public class MenuView : IDYCController<MenuViewModel>, ISideMenuViewController
    {
        private const float MARGIN = 16f;
        private const float IMG_USER_SIZE = 55f;
        private const float CONTROLS_DISTANCE = 16f;
        private const float VIEW_HEADER_HEIGHT = 104f;

        private const float MESSAGES_BADGE_SIZE = 22f;

        private Lazy<IMvxMessenger> messenger = new Lazy<IMvxMessenger>(() => Mvx.Resolve<IMvxMessenger>());

        private MvxSubscriptionToken driverInformationUpdatedToken;

        // header views
        private UIView viewHeader;
        private UIImageView imgUser;
        private IDYCLabel lblName, lblRating;
        private UIImageView imgRating;

        // menu rows
        private UIScrollView rowsScrollView;
        private IDYCLabel lblNewMessagesCounter;
        private MenuRowView rowMyTrips, rowMessages, rowFindOpenTrips, rowAvailability, rowMyReviews, rowReferrals, rowProfile;
        private UIView rowsContainer;

        private MenuRowView previousCheckedItem;

        public MenuView()
        {
        }

        private int MaxMenuWidth = 280;
        private int MinSpaceRightOfTheMenu = 55;

        public int MenuWidth
        {
            get
            {
                var width = int.Parse(UIScreen.MainScreen.Bounds.Width.ToString()) - MinSpaceRightOfTheMenu;
                return width > this.MaxMenuWidth ? this.MaxMenuWidth : width;
            }
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.View.BackgroundColor = UIColor.White;

            #region header view
            this.viewHeader = new UIView
            {
                BackgroundColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor()
            };
            this.imgUser = new UIImageView(UIImage.FromBundle("ic_profile_picture.jpg")) { UserInteractionEnabled = false };
            this.lblName = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = UIColor.White
            };
            this.imgRating = new UIImageView(UIImage.FromBundle(Assets.Star))
            {
                UserInteractionEnabled = false
            };
            this.lblRating = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = UIColor.White,
                UserInteractionEnabled = false
            };
            #endregion

            #region menu rows

            this.rowsScrollView = new UIScrollView
            {
                Bounces = false,
                ShowsHorizontalScrollIndicator = false
            };
            this.rowsContainer = new UIView();

            this.rowMyTrips = new MenuRowView(Assets.MyTrips, IDriveYourCarStrings.MyTrips);
            this.rowMyTrips.SetChecked(true);
            this.previousCheckedItem = this.rowMyTrips;

            this.lblNewMessagesCounter = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = "#E8412E".ToUIColor(),
                ClipsToBounds = true
            };

            this.rowMessages = new MenuRowView(Assets.Messages, IDriveYourCarStrings.Messages);
            this.rowFindOpenTrips = new MenuRowView(Assets.FindOpenTrips, IDriveYourCarStrings.FindOpenTrips);
            this.rowAvailability = new MenuRowView(Assets.Availability, IDriveYourCarStrings.Availability);
            this.rowMyReviews = new MenuRowView(Assets.Reviews, IDriveYourCarStrings.MyReviews);
            this.rowReferrals = new MenuRowView(Assets.Invites, IDriveYourCarStrings.Invites);
            this.rowProfile = new MenuRowView(Assets.Profile, IDriveYourCarStrings.Profile);

            #endregion

            this.View.AggregateSubviews(this.viewHeader, this.rowsScrollView);
            this.viewHeader.AggregateSubviews(this.imgUser, this.lblName, this.imgRating, this.lblRating);
            this.rowsScrollView.AggregateSubviews(this.rowsContainer);
            this.rowsContainer.AggregateSubviews(
                this.lblNewMessagesCounter,
                this.rowMyTrips,
                this.rowMessages,
                this.rowFindOpenTrips,
                this.rowAvailability,
                this.rowMyReviews,
                this.rowReferrals,
                this.rowProfile);

            #region Gesture recognizers
            this.rowMyTrips.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowMyTrips);
                    this.ViewModel.ShowMyTripsCommand.Execute(null);
                })
            );
            this.rowMessages.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowMessages);
                    this.ViewModel.ShowMessagesCommand.Execute(null);
                })
            );
            this.rowFindOpenTrips.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowFindOpenTrips);
                    this.ViewModel.ShowFindOpenTripsCommand.Execute(null);
                })
            );
            this.rowAvailability.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowAvailability);
                    this.ViewModel.ShowAvailabilityCommand.Execute(null);
                })
            );
            this.rowMyReviews.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowMyReviews);
                    this.ViewModel.ShowReviewsCommand.Execute(null);
                })
            );
            this.rowReferrals.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowReferrals);
                    this.ViewModel.ShowInvitesCommand.Execute(null);
                })
            );
            this.rowProfile.AddGestureRecognizer(
                new UITapGestureRecognizer(
                () =>
                {
                    this.ApplySideMenuChanges(this.rowProfile);
                    this.ViewModel.ShowProfileCommand.Execute(null);
                })
            );
            #endregion

            this.StateChanged();

            this.driverInformationUpdatedToken = this.messenger.Value.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message =>
                {
                    this.lblName.Text = new DriverToNameSurnameConverter().Convert(this.ViewModel.Driver, null, null, null) as string;
                    this.lblRating.Text = this.ViewModel.Driver.Rating.HasValue ? this.ViewModel.Driver.Rating.ToString() : IDriveYourCarStrings.Dash;

                    ImageService.Instance.LoadUrl(this.ViewModel.UserImagePath)
                                .LoadingPlaceholder("ic_profile_picture.jpg", FFImageLoading.Work.ImageSource.CompiledResource)
                                .ErrorPlaceholder("ic_profile_picture.jpg", FFImageLoading.Work.ImageSource.CompiledResource)
                                .Transform(new CircleTransformation())
                                .TransformPlaceholders(true)
                                .DownSampleInDip(80, 80)
                                .Into(this.imgUser);

                    this.StateChanged();
                }
            );
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.viewHeader.AtLeftOf(this.View),
                this.viewHeader.AtTopOf(this.View),
                this.viewHeader.AtRightOf(this.View),
                this.viewHeader.Height().EqualTo(VIEW_HEADER_HEIGHT),

                this.rowsScrollView.Below(this.viewHeader),
                this.rowsScrollView.AtLeftOf(this.View),
                this.rowsScrollView.AtRightOf(this.View),
                this.rowsScrollView.AtBottomOf(this.View)
            );

            this.imgRating.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Horizontal);
            this.lblRating.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);

            this.viewHeader.AddConstraints(
                this.imgUser.AtLeftOf(this.viewHeader, MARGIN + 4),
                this.imgUser.WithSameCenterY(this.viewHeader).Plus(5f),
                this.imgUser.Height().EqualTo(IMG_USER_SIZE),
                this.imgUser.Width().EqualTo().HeightOf(this.imgUser),

                this.lblName.ToRightOf(this.imgUser, CONTROLS_DISTANCE),
                this.lblName.WithSameTop(this.imgUser),
                this.lblName.AtRightOf(this.viewHeader, MARGIN),

                this.imgRating.WithSameBottom(this.imgUser),
                this.imgRating.WithSameLeft(this.lblName),

                this.lblRating.ToRightOf(this.imgRating, CONTROLS_DISTANCE / 2),
                this.lblRating.WithSameCenterY(this.imgRating),
                this.lblRating.AtRightOf(this.viewHeader, MARGIN)
            );

            this.rowsScrollView.AddConstraints(
                this.rowsContainer.AtLeftOf(this.rowsScrollView),
                this.rowsContainer.AtTopOf(this.rowsScrollView),
                this.rowsContainer.AtRightOf(this.rowsScrollView),
                this.rowsContainer.AtBottomOf(this.rowsScrollView)
            );

            this.View.AddConstraints(
                this.rowsContainer.WithSameWidth(this.View)
            );

            this.rowsContainer.AddConstraints(
                this.rowMyTrips.AtLeftOf(this.rowsContainer),
                this.rowMyTrips.AtTopOf(this.rowsContainer, MARGIN / 2),
                this.rowMyTrips.AtRightOf(this.rowsContainer),

                this.rowMessages.Below(this.rowMyTrips),
                this.rowMessages.AtLeftOf(this.rowsContainer),
                this.rowMessages.AtRightOf(this.rowsContainer),

                this.lblNewMessagesCounter.AtRightOf(this.rowsContainer, MARGIN),
                this.lblNewMessagesCounter.WithSameCenterY(this.rowMessages),
                this.lblNewMessagesCounter.Width().EqualTo(MESSAGES_BADGE_SIZE),
                this.lblNewMessagesCounter.Height().EqualTo(MESSAGES_BADGE_SIZE),

                this.rowFindOpenTrips.Below(this.rowMessages),
                this.rowFindOpenTrips.AtLeftOf(this.rowsContainer),
                this.rowFindOpenTrips.AtRightOf(this.rowsContainer),

                this.rowAvailability.Below(this.rowFindOpenTrips),
                this.rowAvailability.AtLeftOf(this.rowsContainer),
                this.rowAvailability.AtRightOf(this.rowsContainer),

                this.rowMyReviews.Below(this.rowAvailability),
                this.rowMyReviews.AtLeftOf(this.rowsContainer),
                this.rowMyReviews.AtRightOf(this.rowsContainer),

                this.rowReferrals.Below(this.rowMyReviews),
                this.rowReferrals.AtLeftOf(this.rowsContainer),
                this.rowReferrals.AtRightOf(this.rowsContainer),

                this.rowProfile.Below(this.rowReferrals),
                this.rowProfile.AtLeftOf(this.rowsContainer),
                this.rowProfile.AtRightOf(this.rowsContainer),
                this.rowProfile.AtBottomOf(this.rowsContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<MenuView, MenuViewModel>();
            //set.Bind(this.imgRating.Tap()).For(g => g.Command).To(vm => vm.ShowReviewsCommand);
            //set.Bind(this.lblRating.Tap()).For(g => g.Command).To(vm => vm.ShowReviewsCommand);

            set.Bind(this.rowMyTrips).For(v => v.OptionActive).To(vm => vm.DriverActive);
            set.Bind(this.rowMyTrips).For(v => v.UserInteractionEnabled).To(vm => vm.DriverActive);

            set.Bind(this.rowMessages).For(v => v.OptionActive).To(vm => vm.DriverActive);
            set.Bind(this.rowMessages).For(v => v.UserInteractionEnabled).To(vm => vm.DriverActive);

            set.Bind(this.rowFindOpenTrips).For(v => v.OptionActive).To(vm => vm.DriverActive);
            set.Bind(this.rowFindOpenTrips).For(v => v.UserInteractionEnabled).To(vm => vm.DriverActive);

            set.Bind(this.rowAvailability).For(v => v.OptionActive).To(vm => vm.DriverActive);
            set.Bind(this.rowAvailability).For(v => v.UserInteractionEnabled).To(vm => vm.DriverActive);

            set.Bind(this.rowMyReviews).For(v => v.OptionActive).To(vm => vm.DriverActive);
            set.Bind(this.rowMyReviews).For(v => v.UserInteractionEnabled).To(vm => vm.DriverActive);

            set.Bind(this.rowReferrals).For(v => v.OptionActive).To(vm => vm.DriverActive);
            set.Bind(this.rowReferrals).For(v => v.UserInteractionEnabled).To(vm => vm.DriverActive);
            set.Apply();
        }

        private void ToggleSideMenu()
        {
            var presenter = (UIApplication.SharedApplication.Delegate as AppDelegate).Presenter as MvxSidebarPresenter;
            presenter.ToggleSidebarMenu();
        }

        private void ApplySideMenuChanges(MenuRowView newItem)
        {
            this.ToggleSideMenu();

            this.previousCheckedItem.SetChecked(false);
            newItem.SetChecked(true);
            this.previousCheckedItem = newItem;
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            this.lblNewMessagesCounter.Layer.CornerRadius = this.lblNewMessagesCounter.Frame.Width / 2;
        }

        public void StateChanged()
        {
            this.lblNewMessagesCounter.Hidden = this.ViewModel.NewMessagesCount == 0;
            this.lblNewMessagesCounter.Text = this.ViewModel.NewMessagesCount.ToString();
        }
    }
}