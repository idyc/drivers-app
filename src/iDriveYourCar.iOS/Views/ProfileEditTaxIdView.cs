﻿using System.Linq;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class ProfileEditTaxIdView : IDYCController<ProfileEditTaxIdViewModel>
    {
        private const float TEXT_FIELD_HEIGHT = 30f;
        private const float TOP_MARGIN = 50f;

        private BorderedTextField txtField;

        private UIBarButtonItem btnSave;

        public ProfileEditTaxIdView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = this.ViewModel.DriverEditableFieldConfiguration.Title;

            this.btnSave = new UIBarButtonItem(UIImage.FromBundle(Assets.DoneWhite), UIBarButtonItemStyle.Plain, null);
            this.NavigationItem.SetRightBarButtonItem(this.btnSave, false);

            this.txtField = new BorderedTextField(this.ViewModel.DriverEditableFieldConfiguration.Hint);
            this.txtField.TextField.Enabled = false;

            this.View.AggregateSubviews(this.txtField);

            this.txtField.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    if(!this.ViewModel.TaxAdapter.Items.Any())
                        return;

                    var taxNames = this.ViewModel.TaxAdapter.Items.Select(p => p.Name).ToList();
                    var modalPicker = new ModalPickerViewController(
                        ModalPickerType.Custom,
                        string.Empty,
                        this,
                        Strings.Ok,
                        Strings.Cancel,
                        UIColor.White,
                        IDYCColors.DarkGrayTextColor,
                        UIColor.White);
                    modalPicker.PickerView.Model = new CustomPickerModel(taxNames);

                    var selectedTaxId = this.ViewModel.TaxAdapter.Items.First(c => c.Name == this.ViewModel.TaxAdapter.SelectedItem.Name);

                    modalPicker.PickerView.Select(taxNames.IndexOf(selectedTaxId.Name), 0, true);

                    modalPicker.OnModalPickerDismissed += (s, ea) =>
                    {
                        var index = modalPicker.PickerView.SelectedRowInComponent(0);
                        this.ViewModel.TaxAdapter.SelectedItem = this.ViewModel.TaxAdapter.Items.First(c => c.Name == taxNames[(int)index]);
                    };

                    this.PresentViewController(modalPicker, true, null);
                }));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.txtField.AtTopOf(this.View, TOP_MARGIN),
                this.txtField.AtLeftOf(this.View, Metrics.WINDOW_MARGIN),
                this.txtField.AtRightOf(this.View, Metrics.WINDOW_MARGIN),
                this.txtField.Height().EqualTo(TEXT_FIELD_HEIGHT)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ProfileEditTaxIdView, ProfileEditTaxIdViewModel>();
            set.Bind(this.txtField.TextField).To(vm => vm.TaxAdapter.SelectedItem.Name);
            set.Bind(this.btnSave).For(MvxBindings.Clicked).To(vm => vm.SaveEditFieldCommand);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SaveEditFieldTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
