﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using Foundation;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.MvxBindings;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class CurrentTripDetailView : IDYCTabsCurrentTripController<CurrentTripDetailViewModel>
    {
        private TripSwipeableButton btnWorkflow;

        private NSObject notificationObserver;

        protected override float TabsControllerMarginBottom => Metrics.SwipeButtonHeight;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.CurrentTrip;

            this.topTabBar.SetItems(new NSObject[]
            {
                new NSString(IDriveYourCarStrings.Details.ToUpper()),
                new NSString(IDriveYourCarStrings.Notes.ToUpper())
            });

            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.CurrentTripDetailInfo) as CurrentTripDetailInfoView);
            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.CurrentTripDetailNotes) as CurrentTripDetailNotesView);

            this.pageViewController.SetViewControllers(new UIViewController[] { this.tabsViewControllers[0] }, UIPageViewControllerNavigationDirection.Forward, true, null);
            this.currentSelectedTab = 0;

            this.btnWorkflow = new TripSwipeableButton
            {
                BackgroundColor = IDYCColors.SwipedButtonColor
            };
            this.btnWorkflow.ImgSwipe.Image = UIImage.FromBundle(Assets.ArrowRightWhite);
            this.btnWorkflow.ImgSwiped.Image = UIImage.FromBundle(Assets.CheckWhite);
            this.btnWorkflow.OnPanned += this.BtnWorkflow_OnPanned;

            this.View.AggregateSubviews(this.btnWorkflow);

            this.View.BringSubviewToFront(this.topTabBar);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.btnWorkflow.AtBottomOf(this.View),
                this.btnWorkflow.AtLeftOf(this.View),
                this.btnWorkflow.AtRightOf(this.View),
                this.btnWorkflow.Height().EqualTo(Metrics.SwipeButtonHeight)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CurrentTripDetailView, CurrentTripDetailViewModel>();
            set.Bind(this.View).For(MvxBindings.NavigateManually).To(vm => vm.DisplayNavigationTaskCompletion.IsFaulted).WithFallback(false);
            set.Bind(this.btnWorkflow.FrontView).For(v => v.BackgroundColor).To(vm => vm.FollowingState).WithConversion(ValueConverters.ActiveTripStateToBackgroundColorConverter);
            set.Bind(this.btnWorkflow.LblSwipe).To(vm => vm.FollowingActionName);
            set.Bind(this.btnWorkflow.FrontView).For(v => v.UserInteractionEnabled).To(vm => vm.PerformWorkflowTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.PerformWorkflowTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.DisplayNavigationTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.notificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, this.UpdateState);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.ViewModel.UpdateFollowingStateCommand.Execute(null);
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();

            if(this.notificationObserver != null)
                NSNotificationCenter.DefaultCenter.RemoveObserver(this.notificationObserver);
        }

        private void UpdateState(NSNotification a_notification)
        {
            // DO STUFF HERE
            this.ViewModel.UpdateFollowingStateCommand.Execute(null);
            this.btnWorkflow.Close();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.PerformWorkflowTaskCompletion) && this.ViewModel.PerformWorkflowTaskCompletion != null)
            {
                this.ViewModel.PerformWorkflowTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
            }

            if(e.PropertyName == nameof(this.ViewModel.FollowingActionName))
            {
                this.btnWorkflow.Close();
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(this.ViewModel.PerformWorkflowTaskCompletion.IsFaulted)
              && this.ViewModel.PerformWorkflowTaskCompletion.IsFaulted)
            {
                this.btnWorkflow.Close();
            }
        }

        private void BtnWorkflow_OnPanned(object sender, System.EventArgs e)
        {
            this.ViewModel.PerformWorkflowCommand.Execute(null);
        }
    }
}
