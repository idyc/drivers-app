﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using Foundation;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using UIKit;
using System.Linq;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class UpcomingTripModifiedDetailView : IDYCTabsMenuController<UpcomingTripModifiedDetailViewModel>
    {
        private PrimaryBackgroundButton btnAcceptChanges, btnCallDispatch, btnCallDispatchWide, btnConfirmCancellation;
        private UIImageView imgNotesDot;

        protected override float TabsControllerMarginBottom => Metrics.SwipeButtonHeight;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = this.ViewModel.Title;

            this.topTabBar.SetItems(new NSObject[]
            {
                new NSString(IDriveYourCarStrings.Details.ToUpper()),
                new NSString(IDriveYourCarStrings.Notes.ToUpper())
            });

            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.UpcomingTripDetailDetails) as UpcomingTripDetailInfoView);
            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.UpcomingTripDetailNotes) as UpcomingTripDetailNotesView);

            this.pageViewController.SetViewControllers(new UIViewController[] { this.tabsViewControllers[0] }, UIPageViewControllerNavigationDirection.Forward, true, null);
            this.currentSelectedTab = 0;

            this.btnAcceptChanges = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnAcceptChanges.SetTitle(IDriveYourCarStrings.AcceptChanges.ToUpper(), UIControlState.Normal);

            this.btnCallDispatch = new PrimaryBackgroundButton(this.appConfiguration.Value)
            {
                BackgroundColor = IDYCColors.LightGrayTextColor
            };
            this.btnCallDispatch.SetTitle(IDriveYourCarStrings.CallDispatch.ToUpper(), UIControlState.Normal);
            this.btnCallDispatch.TitleLabel.TextColor = IDYCColors.DarkGrayTextColor;

            this.btnCallDispatchWide = new PrimaryBackgroundButton(this.appConfiguration.Value)
            {
                BackgroundColor = IDYCColors.LightGrayTextColor
            };
            this.btnCallDispatchWide.SetTitle(IDriveYourCarStrings.CallDispatch.ToUpper(), UIControlState.Normal);
            this.btnCallDispatchWide.TitleLabel.TextColor = IDYCColors.DarkGrayTextColor;

            this.btnConfirmCancellation = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnConfirmCancellation.SetTitle(IDriveYourCarStrings.ConfirmCancellation.ToUpper(), UIControlState.Normal);

            this.View.AggregateSubviews(this.btnAcceptChanges, this.btnCallDispatch, this.btnCallDispatchWide, this.btnConfirmCancellation);

            this.View.BringSubviewToFront(this.topTabBar);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.btnCallDispatch.AtBottomOf(this.View),
                this.btnCallDispatch.AtLeftOf(this.View),
                this.btnCallDispatch.WithRelativeWidth(this.View, 0.5f),
                this.btnCallDispatch.Height().EqualTo(Metrics.SwipeButtonHeight),

                this.btnAcceptChanges.WithSameCenterY(this.btnCallDispatch),
                this.btnAcceptChanges.AtRightOf(this.View),
                this.btnAcceptChanges.ToRightOf(this.btnCallDispatch),
                this.btnAcceptChanges.Height().EqualTo(Metrics.SwipeButtonHeight),

                this.btnCallDispatchWide.AtBottomOf(this.View),
                this.btnCallDispatchWide.AtLeftOf(this.View),
                this.btnCallDispatchWide.AtRightOf(this.View),
                this.btnCallDispatchWide.Height().EqualTo(Metrics.SwipeButtonHeight),

                this.btnConfirmCancellation.AtBottomOf(this.View),
                this.btnConfirmCancellation.AtLeftOf(this.View),
                this.btnConfirmCancellation.AtRightOf(this.View),
                this.btnConfirmCancellation.Height().EqualTo(Metrics.SwipeButtonHeight)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<UpcomingTripModifiedDetailView, UpcomingTripModifiedDetailViewModel>();
            set.Bind(this.btnCallDispatch).To(vm => vm.CallDispatchCommand);
            set.Bind(this.btnAcceptChanges).To(vm => vm.AcceptChangesCommand);
            set.Bind(this.btnConfirmCancellation).To(vm => vm.ConfirmCancellationCommand);

            set.Bind(this.btnCallDispatch).For(MvxBindings.Visibility).To(vm => vm.UpcomingTripDetailDetails.Trip.IsCancelled).WithConversion(FwkValueConverters.InvertedVisibilityConverter);
            set.Bind(this.btnCallDispatchWide).For(MvxBindings.Visibility).To(vm => vm.UpcomingTripDetailDetails.Trip).WithConversion(ValueConverters.TripHasModificationsAndHasTripConflictToVisibilityConverter);
            set.Bind(this.btnAcceptChanges).For(MvxBindings.Visibility).To(vm => vm.UpcomingTripDetailDetails.Trip.IsCancelled).WithConversion(FwkValueConverters.InvertedVisibilityConverter);
            set.Bind(this.btnConfirmCancellation).For(MvxBindings.Visibility).To(vm => vm.UpcomingTripDetailDetails.Trip.IsCancelled).WithConversion(FwkValueConverters.VisibilityConverter);

            set.Apply();

            //this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if(this.ViewModel.NotesHaveChanges)
            {
                var label = this.topTabBar.FindSubviewsOfType(typeof(UILabel)).Where(v => ((UILabel)v).Text == IDriveYourCarStrings.Notes.ToUpper()).First();
                this.imgNotesDot = new UIImageView(UIImage.FromBundle(Assets.OrangeDot));
                this.View.AggregateSubviews(this.imgNotesDot);
                this.View.AddConstraints(
                    this.imgNotesDot.WithSameCenterY(label),
                    this.imgNotesDot.ToLeftOf(label, 8f)
                );

            }
        }

        public override void ViewDidAppear(bool animated)
        {
            this.NavigationController.HideBottomShadow();

            base.ViewDidAppear(animated);
        }
    }
}

