﻿using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using Foundation;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.iOS.Views;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class UpcomingTripDetailView : IDYCTabsMenuController<UpcomingTripDetailViewModel>
    {
        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.MyTrips;

            this.topTabBar.SetItems(new NSObject[]
            {
                new NSString(IDriveYourCarStrings.Details.ToUpper()),
                new NSString(IDriveYourCarStrings.Notes.ToUpper())
            });

            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.UpcomingTripDetailDetails) as UpcomingTripDetailInfoView);
            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.UpcomingTripDetailNotes) as UpcomingTripDetailNotesView);


            this.pageViewController.SetViewControllers(new UIViewController[] { this.tabsViewControllers[0] }, UIPageViewControllerNavigationDirection.Forward, true, null);
            this.currentSelectedTab = 0;

            this.View.BringSubviewToFront(this.topTabBar);
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            //this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
        }

        public override void ViewDidAppear(bool animated)
        {
            this.NavigationController.HideBottomShadow();

            base.ViewDidAppear(animated);
        }
    }
}
