﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class DriverUnavailabilityExtendedCreationView : IDYCController<DriverUnavailabilityExtendedCreationViewModel>
    {
        private const float TIME_WIDTH_MULTIPLIER = 0.43f;
        private const float TEXT_FIELD_HEIGHT = 40f;
        private const float TOP_MARGIN = 30f;

        private UIBarButtonItem btnSave;

        private UIScrollView scrollView;
        private UIView viewContainer;

        private BorderedTextField txtDateStart, txtDateEnd;
        private IDYCLabel lblTo;

        public DriverUnavailabilityExtendedCreationView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.SetVacation;

            this.btnSave = new UIBarButtonItem(UIImage.FromBundle(Assets.DoneWhite), UIBarButtonItemStyle.Plain, null);
            this.NavigationItem.SetRightBarButtonItem(this.btnSave, false);

            this.scrollView = new UIScrollView
            {
                DelaysContentTouches = false,
                BackgroundColor = UIColor.White
            };
            this.viewContainer = new UIView();

            this.txtDateStart = new BorderedTextField { UserInteractionEnabled = true };
            this.txtDateStart.BackgroundColor = UIColor.White;
            this.txtDateStart.TextField.Enabled = false;

            this.txtDateEnd = new BorderedTextField { UserInteractionEnabled = true };
            this.txtDateEnd.BackgroundColor = UIColor.White;
            this.txtDateEnd.TextField.Enabled = false;

            this.lblTo = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.To
            };

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.txtDateStart,
                this.txtDateEnd,
                this.lblTo);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 4)
            );

            this.viewContainer.AddConstraints(
                this.txtDateStart.AtTopOf(this.viewContainer, TOP_MARGIN),
                this.txtDateStart.AtLeftOf(this.viewContainer),
                this.txtDateStart.WithRelativeWidth(this.viewContainer, TIME_WIDTH_MULTIPLIER),
                this.txtDateStart.Height().EqualTo(TEXT_FIELD_HEIGHT),

                this.lblTo.WithSameCenterX(this.viewContainer),
                this.lblTo.WithSameCenterY(this.txtDateStart),

                this.txtDateEnd.WithSameTop(this.txtDateStart),
                this.txtDateEnd.AtRightOf(this.viewContainer),
                this.txtDateEnd.WithRelativeWidth(this.viewContainer, TIME_WIDTH_MULTIPLIER),
                this.txtDateEnd.Height().EqualTo(TEXT_FIELD_HEIGHT),
                this.txtDateEnd.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DriverUnavailabilityExtendedCreationView, DriverUnavailabilityExtendedCreationViewModel>();
            set.Bind(this.txtDateStart.Tap()).For(g => g.Command).To(vm => vm.SetDateStartCommand);
            set.Bind(this.txtDateEnd.Tap()).For(g => g.Command).To(vm => vm.SetDateEndCommand);
            set.Bind(this.txtDateStart.TextField).To(vm => vm.DateStart).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");
            set.Bind(this.txtDateEnd.TextField).To(vm => vm.DateEnd).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");
            set.Bind(this.btnSave).For(MvxBindings.Clicked).To(vm => vm.SaveUnavailabilityCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
