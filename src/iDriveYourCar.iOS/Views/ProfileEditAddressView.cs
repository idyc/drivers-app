﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class ProfileEditAddressView : IDYCController<ProfileEditAddressViewModel>
    {
        private const float TEXT_FIELD_HEIGHT = 30f;
        private const float TOP_MARGIN = 50f;
        private const float ACCESSORY_SIZE = 30f;

        private BorderedTextField txtField;
        private OAStackView.OAStackView accessoriesStack;
        private UIButton btnClear;
        private UIActivityIndicatorView activityIndicator;
        private UITableView tblResults;
        private SimpleTableSource<DescriptionTableViewCell, Prediction> source;
        private UIBarButtonItem btnSave;

        public ProfileEditAddressView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = this.ViewModel.DriverEditableFieldConfiguration.Title;

            this.btnSave = new UIBarButtonItem(UIImage.FromBundle(Assets.DoneWhite), UIBarButtonItemStyle.Plain, null);
            this.NavigationItem.SetRightBarButtonItem(this.btnSave, false);

            this.txtField = new BorderedTextField(this.ViewModel.DriverEditableFieldConfiguration.Hint);
            this.accessoriesStack = new OAStackView.OAStackView
            {
                Axis = UILayoutConstraintAxis.Horizontal,
                Distribution = OAStackView.OAStackViewDistribution.Fill
            };
            this.btnClear = new UIButton();
            this.btnClear.SetImage(UIImage.FromBundle(Assets.Close), UIControlState.Normal);
            this.activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);

            this.tblResults = new AutomaticDimensionTableView(44f)
            {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                Bounces = false
            };
            this.source = new SimpleTableSource<DescriptionTableViewCell, Prediction>(this.tblResults);
            this.source.DeselectAutomatically = true;
            this.tblResults.Source = this.source;

            this.View.AggregateSubviews(this.accessoriesStack, this.tblResults);

            this.accessoriesStack.AddArrangedSubview(this.txtField);
            this.accessoriesStack.AddArrangedSubview(this.activityIndicator);
            this.accessoriesStack.AddArrangedSubview(this.btnClear);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.accessoriesStack.AtLeftOf(this.View, Metrics.WINDOW_MARGIN),
                this.accessoriesStack.AtRightOf(this.View, Metrics.WINDOW_MARGIN),
                this.accessoriesStack.AtTopOf(this.View, TOP_MARGIN),
                this.accessoriesStack.Height().EqualTo(36f),

                this.btnClear.Height().EqualTo(ACCESSORY_SIZE),
                this.btnClear.Width().EqualTo(ACCESSORY_SIZE),

                this.activityIndicator.Height().EqualTo(ACCESSORY_SIZE),
                this.activityIndicator.Width().EqualTo(ACCESSORY_SIZE),

                this.tblResults.Below(this.accessoriesStack),
                this.tblResults.AtLeftOf(this.View, Metrics.WINDOW_MARGIN),
                this.tblResults.AtRightOf(this.View, Metrics.WINDOW_MARGIN),
                this.tblResults.AtBottomOf(this.View, Metrics.WINDOW_MARGIN)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ProfileEditAddressView, ProfileEditAddressViewModel>();
            set.Bind(this.txtField.TextField).To(vm => vm.InputText);
            set.Bind(this.activityIndicator).For(v => v.Hidden).To(vm => vm.SearchPredictionsTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);
            set.Bind(this.activityIndicator).For(MvxBindings.ActivityIndicator).To(vm => vm.SearchPredictionsTaskCompletion.IsNotCompleted).WithFallback(false);
            set.Bind(this.btnClear).For(MvxBindings.Visibility).To(vm => vm.SelectedPrediction).WithConversion(FwkValueConverters.VisibilityConverter);
            set.Bind(this.btnClear).To(vm => vm.DismissPredictionCommand);
            set.Bind(this.btnSave).For(MvxBindings.Clicked).To(vm => vm.SaveEditAddressCommand);
            set.Bind(this.source).For(v => v.ItemsSource).To(vm => vm.Predictions);
            set.Bind(this.source).For(v => v.SelectionChangedCommand).To(vm => vm.SelectPredictionCommand);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SaveEditAddressTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
