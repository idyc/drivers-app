﻿using System;
using iDriveYourCar.iOS.MvxSupport;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.Views.Sources;
using UIKit;
using MvvmCross.Binding.BindingContext;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iOS.Controls;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.Cells;

namespace iDriveYourCar.iOS.Views
{
	[PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
	public class RewardsView : IDYCMenuController<RewardsViewModel>
	{
		private UITableView tblRewards;
		private SimpleTableSource<RewardsTableViewCell, RewardItemViewModel> source;

		public RewardsView()
		{
		}

		public override void CreateViews()
		{
			base.CreateViews();

			this.Title = IDriveYourCarStrings.Rewards;

			this.tblRewards = new AutomaticDimensionTableView(120f)
			{
				SeparatorStyle = UITableViewCellSeparatorStyle.None,
				AllowsSelection = false,
				Bounces = false
			};

			this.source = new SimpleTableSource<RewardsTableViewCell, RewardItemViewModel>(this.tblRewards);
			this.tblRewards.Source = this.source;

			this.View.AddSubview(this.tblRewards);
			this.View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
		}

		public override void CreateConstraints()
		{
			base.CreateConstraints();

			this.View.AddConstraints(
				this.tblRewards.AtLeftOf(this.View),
				this.tblRewards.AtTopOf(this.View),
				this.tblRewards.AtRightOf(this.View),
				this.tblRewards.AtBottomOf(this.View)
			);
		}

		public override void CreateMvxBindings()
		{
			base.CreateMvxBindings();

			var set = this.CreateBindingSet<RewardsView, RewardsViewModel>();
			set.Bind(this.source).For(v => v.ItemsSource).To(vm => vm.Rewards);
			set.Apply();
		}

		public override void MvxSubscribe()
		{
			base.MvxSubscribe();
		}

		public override void MvxUnsubscribe()
		{
			base.MvxUnsubscribe();
		}
	}
}
