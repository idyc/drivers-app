﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    public class DriverUnavailabilityExtendedView : IDYCController<DriverUnavailabilityExtendedViewModel>
    {
        private const float CONTROLS_DISTANCE_SMALL = 4f;
        private const float CONTROLS_DISTANCE = 16f;
        private const float CONTROLS_DISTANCE_BIG = 24f;

        private UIScrollView scrollView;

        private UIView viewContainer;

        private IDYCLabel lblSetVacation;
        private IDYCLabel lblScheduledTimeOff;
        private IDYCMultilineLabel lblSetVacationExplanation;
        private PrimaryTextButton btnSetTimeOff;
        private UIView viewLine;
        private SimpleBindableStackView<DriverUnavailabilityExtendedItemView, DriverUnavailabilityExtendedItemViewModel> driverUnavailabilityExtendedStackView;

        public DriverUnavailabilityExtendedView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);

            this.lblSetVacation = new IDYCLabel(Metrics.FontSizeGigant)
            {
                Text = IDriveYourCarStrings.SetVacation,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblSetVacationExplanation = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.NotificationsWillBeTurnedOffAndYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSet,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.btnSetTimeOff = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnSetTimeOff.SetTitle(IDriveYourCarStrings.SetTimeOff, UIControlState.Normal);

            this.lblScheduledTimeOff = new IDYCLabel(Metrics.FontSizeNormal, Metrics.RobotoMedium)
            {
                Text = IDriveYourCarStrings.ScheduledTimeOff,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.driverUnavailabilityExtendedStackView = new SimpleBindableStackView<DriverUnavailabilityExtendedItemView, DriverUnavailabilityExtendedItemViewModel>(UILayoutConstraintAxis.Vertical, 0f);

            this.viewContainer.AggregateSubviews(
                this.lblSetVacation,
                this.lblSetVacationExplanation,
                this.btnSetTimeOff,
                this.lblScheduledTimeOff,
                this.viewLine,
                this.driverUnavailabilityExtendedStackView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.lblSetVacation.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.lblSetVacation.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),

                this.lblSetVacationExplanation.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.lblSetVacationExplanation.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblSetVacationExplanation.Below(this.lblSetVacation, CONTROLS_DISTANCE),

                this.btnSetTimeOff.Below(this.lblSetVacationExplanation, CONTROLS_DISTANCE),
                this.btnSetTimeOff.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),

                this.viewLine.Below(this.btnSetTimeOff, CONTROLS_DISTANCE_BIG),
                this.viewLine.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.viewLine.AtRightOf(this.viewContainer),
                this.viewLine.Height().EqualTo(Metrics.LineThickness),

                this.lblScheduledTimeOff.Below(this.viewLine, CONTROLS_DISTANCE_BIG),
                this.lblScheduledTimeOff.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),

                this.driverUnavailabilityExtendedStackView.Below(this.lblScheduledTimeOff, CONTROLS_DISTANCE_SMALL),
                this.driverUnavailabilityExtendedStackView.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.driverUnavailabilityExtendedStackView.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.driverUnavailabilityExtendedStackView.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DriverUnavailabilityExtendedView, DriverUnavailabilityExtendedViewModel>();
            set.Bind(this.btnSetTimeOff).To(vm => vm.SetTimeOffCommand);
            set.Bind(this.driverUnavailabilityExtendedStackView).For(v => v.ItemsSource).To(vm => vm.ExtendedUnavailabilities);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SetTimeOffTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();

        }
    }
}
