﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Plugins.Color.iOS;
using UIKit;
using iDriveYourCar.Core.Converters;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class CurrentTripSubmitView : IDYCCurrentTripController<CurrentTripSubmitViewModel>
    {
        private UIScrollView scrollView;
        private UIView viewContainer;

        private IDYCLabel lblNotesForDrivers;
        private IDYCMultilineLabel lblNotesExplanation;
        private BorderedTextView txtNotes;

        private TripSwipeableButton btnSwipe;

        public override bool HandlesKeyboardNotifications() => true;

        public CurrentTripSubmitView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.TripComplete;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.lblNotesForDrivers = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                TextColor = UIColor.Black,
                Text = IDriveYourCarStrings.NotesForDrivers
            };
            this.lblNotesExplanation = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                Text = IDriveYourCarStrings.AnythingThatWillHelpUsBetterServeTheClient
            };
            this.txtNotes = new BorderedTextView(IDriveYourCarStrings.AddNote)
            {
                TextViewHeight = 200f
            };
            this.txtNotes.TextView.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeSmall);
            this.txtNotes.Placeholder.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeSmall);
            this.btnSwipe = new TripSwipeableButton
            {
                BackgroundColor = IDYCColors.SwipedButtonColor
            };
            this.btnSwipe.FrontView.BackgroundColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();
            this.btnSwipe.ImgSwipe.Image = UIImage.FromBundle(Assets.ArrowRightWhite);
            this.btnSwipe.ImgSwiped.Image = UIImage.FromBundle(Assets.CheckWhite);
            this.btnSwipe.OnPanned += this.BtnWorkflow_OnPanned;

            this.View.AggregateSubviews(this.scrollView, this.btnSwipe);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(this.lblNotesForDrivers, this.lblNotesExplanation, this.txtNotes);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.btnSwipe.AtBottomOf(this.View),
                this.btnSwipe.AtLeftOf(this.View),
                this.btnSwipe.AtRightOf(this.View),
                this.btnSwipe.Height().EqualTo(Metrics.SwipeButtonHeight),

                this.scrollView.Above(this.btnSwipe),
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.lblNotesForDrivers.AtLeftOf(this.viewContainer, Metrics.DEFAULT_DISTANCE),
                this.lblNotesForDrivers.AtTopOf(this.viewContainer, Metrics.DEFAULT_DISTANCE * 2),
                this.lblNotesForDrivers.AtRightOf(this.viewContainer, Metrics.DEFAULT_DISTANCE),

                this.lblNotesExplanation.Below(this.lblNotesForDrivers, Metrics.DEFAULT_DISTANCE / 2),
                this.lblNotesExplanation.WithSameLeft(this.lblNotesForDrivers),
                this.lblNotesExplanation.WithSameRight(this.lblNotesForDrivers),

                this.txtNotes.Below(this.lblNotesExplanation, Metrics.DEFAULT_DISTANCE / 2),
                this.txtNotes.WithSameLeft(this.lblNotesForDrivers),
                this.txtNotes.WithSameRight(this.lblNotesForDrivers),
                this.txtNotes.AtBottomOf(this.viewContainer, Metrics.DEFAULT_DISTANCE)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CurrentTripSubmitView, CurrentTripSubmitViewModel>();
            set.Bind(this.txtNotes.TextView).To(vm => vm.NotesForDrivers);
            set.Bind(this.txtNotes.Placeholder).For(MvxBindings.Visibility).To(vm => vm.NotesForDrivers).WithConversion(FwkValueConverters.InvertedVisibilityConverter);

            set.Bind(this.btnSwipe.FrontView).For(v => v.BackgroundColor).To(vm => vm.NotesForDrivers).WithConversion(ValueConverters.SubmitTripNotesToBackgroundColorConverter);
            set.Bind(this.btnSwipe.FrontView).For(v => v.UserInteractionEnabled).To(vm => vm.NotesForDrivers).WithConversion(FwkValueConverters.StringEmptyToBoolConverter, true);
            set.Bind(this.btnSwipe.LblSwipe).To(vm => vm.FollowingActionName);
            set.Bind(this.btnSwipe).For(v => v.UserInteractionEnabled).To(vm => vm.SubmitTripTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SubmitTripTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.ScrollToCenterOnKeyboardShown = this.scrollView;
            this.ViewToCenterOnKeyboardShown = this.txtNotes;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.SubmitTripTaskCompletion) && this.ViewModel.SubmitTripTaskCompletion != null)
            {
                this.ViewModel.SubmitTripTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
            }

            if(e.PropertyName == nameof(this.ViewModel.FollowingActionName))
            {
                this.btnSwipe.Close();
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(this.ViewModel.SubmitTripTaskCompletion.IsFaulted)
              && this.ViewModel.SubmitTripTaskCompletion.IsFaulted)
            {
                this.btnSwipe.Close();
            }
        }

        private void BtnWorkflow_OnPanned(object sender, System.EventArgs e)
        {
            this.ViewModel.SubmitTripCommand.Execute(null);
        }
    }
}
