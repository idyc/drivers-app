﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;
using MvvmCross.Plugins.Color.iOS;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class FindOpenTripsDetailView : IDYCController<FindOpenTripsDetailViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;
        private UIView viewContainer;
        private TripDetailsHeaderView headerView;
        private UniqueStopView pickupView, dropoffView;
        private TripDetailsStopsView<TripDetailStopView, TripStopItemViewModel> stopsView;

        private TripActionOverlayView actionOverlayView;

        private UIImageView imgEstimatedTime;
        private IDYCLabel lblEstimatedTime;

        private TripDetailsHeaderView tripReturnHeaderView;
        private UniqueStopView tripReturnPickupView, tripReturnDropoffView;
        private TripDetailsStopsView<TripDetailStopView, TripStopItemViewModel> tripReturnStopsView;

        private UIImageView imgTripReturnEstimatedTime;
        private IDYCLabel lblTripReturnEstimatedTime;

        private TripSwipeableButton btnAccept;

        private FluentLayout tripReturnHeaderHeightConstraint, tripReturnPickupHeightConstraint, tripReturnStopsHeightConstraint,
            tripReturnDropoffHeightConstraint, lblTripReturnEstimatedTimeHeightConstraint, imgTripReturnEstimatedTimeHeightConstraint;

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.btnAccept = new TripSwipeableButton
            {
                BackgroundColor = IDYCColors.SwipedButtonColor
            };
            this.btnAccept.FrontView.BackgroundColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();
            this.btnAccept.ImgSwipe.Image = UIImage.FromBundle(Assets.ArrowRightWhite);
            this.btnAccept.ImgSwiped.Image = UIImage.FromBundle(Assets.CheckWhite);
            this.btnAccept.OnPanned += this.BtnAccept_OnPanned;

            this.Title = IDriveYourCarStrings.AcceptTrip;

            this.headerView = new TripDetailsHeaderView();

            this.pickupView = new UniqueStopView(Assets.Plane) { ClipsToBounds = true };
            this.pickupView.Title.Text = IDriveYourCarStrings.PickUp;
            this.stopsView = new TripDetailsStopsView<TripDetailStopView, TripStopItemViewModel>();
            this.dropoffView = new UniqueStopView(Assets.Plane) { ClipsToBounds = true };
            this.dropoffView.Title.Text = IDriveYourCarStrings.DropOff;

            this.imgEstimatedTime = new UIImageView(UIImage.FromBundle(Assets.Timer));
            this.lblEstimatedTime = new IDYCLabel(Metrics.FontSizeSmall) { TextColor = IDYCColors.LightGrayTextColor };

            //Trip Return
            this.tripReturnHeaderView = new TripDetailsHeaderView() { ClipsToBounds = true };

            this.tripReturnPickupView = new UniqueStopView(Assets.Plane) { ClipsToBounds = true };
            this.tripReturnPickupView.Title.Text = IDriveYourCarStrings.PickUp;
            this.tripReturnStopsView = new TripDetailsStopsView<TripDetailStopView, TripStopItemViewModel>();
            this.tripReturnDropoffView = new UniqueStopView(Assets.Plane) { ClipsToBounds = true };
            this.tripReturnDropoffView.Title.Text = IDriveYourCarStrings.DropOff;

            this.imgTripReturnEstimatedTime = new UIImageView(UIImage.FromBundle(Assets.Timer));
            this.lblTripReturnEstimatedTime = new IDYCLabel(Metrics.FontSizeSmall) { TextColor = IDYCColors.LightGrayTextColor };
            //End Trip Return

            this.actionOverlayView = new TripActionOverlayView(Assets.CheckWhiteBig);
            this.actionOverlayView.ActionTitle.Text = IDriveYourCarStrings.Success;
            this.actionOverlayView.BtnBottom.SetTitle(IDriveYourCarStrings.FindMoreTrips.ToUpper(), UIControlState.Normal);
            this.actionOverlayView.Hidden = true;
            this.actionOverlayView.Alpha = 0f;
            this.actionOverlayView.Icon.Hidden = true;
            this.actionOverlayView.Icon.Alpha = 0f;

            this.View.AggregateSubviews(this.scrollView, this.btnAccept, this.actionOverlayView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.headerView,
                this.pickupView,
                this.stopsView,
                this.dropoffView,
                this.imgEstimatedTime,
                this.lblEstimatedTime,
                this.tripReturnPickupView,
                this.tripReturnStopsView,
                this.tripReturnDropoffView,
                this.imgTripReturnEstimatedTime,
                this.lblTripReturnEstimatedTime,
                this.tripReturnHeaderView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.tripReturnHeaderHeightConstraint = this.tripReturnHeaderView.Height().EqualTo(0f).SetActive(true).WithIdentifier("tripReturnHeaderHeightConstraint");
            this.tripReturnPickupHeightConstraint = this.tripReturnPickupView.Height().EqualTo(0f).SetActive(true).WithIdentifier("tripReturnPickupHeightConstraint");
            this.tripReturnStopsHeightConstraint = this.tripReturnStopsView.Height().EqualTo(0f).SetActive(true).WithIdentifier("tripReturnStopsHeightConstraint");
            this.tripReturnDropoffHeightConstraint = this.tripReturnDropoffView.Height().EqualTo(0f).SetActive(true).WithIdentifier("tripReturnDropoffHeightConstraint");
            this.lblTripReturnEstimatedTimeHeightConstraint = this.lblTripReturnEstimatedTime.Height().EqualTo(0f).SetActive(true).WithIdentifier("lblTripReturnEstimatedTimeHeightConstraint");
            this.imgTripReturnEstimatedTimeHeightConstraint = this.imgTripReturnEstimatedTime.Height().EqualTo(0f).SetActive(true).WithIdentifier("imgTripReturnEstimatedTimeHeightConstraint");

            this.View.AddConstraints(
                this.btnAccept.AtBottomOf(this.View),
                this.btnAccept.AtLeftOf(this.View),
                this.btnAccept.AtRightOf(this.View),
                this.btnAccept.Height().EqualTo(Metrics.SwipeButtonHeight),

                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.Above(this.btnAccept),

                this.actionOverlayView.AtLeftOf(this.View),
                this.actionOverlayView.AtTopOf(this.View),
                this.actionOverlayView.AtRightOf(this.View),
                this.actionOverlayView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.imgEstimatedTime.SetContentHuggingPriority(501, UILayoutConstraintAxis.Horizontal);
            this.lblEstimatedTime.SetContentHuggingPriority(499, UILayoutConstraintAxis.Horizontal);

            this.imgTripReturnEstimatedTime.SetContentHuggingPriority(501, UILayoutConstraintAxis.Horizontal);

            this.viewContainer.AddConstraints(
                this.headerView.AtLeftOf(this.viewContainer),
                this.headerView.AtTopOf(this.viewContainer),
                this.headerView.AtRightOf(this.viewContainer),

                this.pickupView.Below(this.headerView),
                this.pickupView.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.pickupView.AtRightOf(this.viewContainer),

                this.stopsView.Below(this.pickupView),
                this.stopsView.WithSameLeft(this.pickupView),
                this.stopsView.WithSameRight(this.pickupView),

                this.dropoffView.Below(this.stopsView),
                this.dropoffView.WithSameLeft(this.pickupView),
                this.dropoffView.WithSameRight(this.pickupView),

                this.imgEstimatedTime.Below(this.dropoffView, CONTROLS_DISTANCE),
                this.imgEstimatedTime.WithSameLeft(this.stopsView),

                this.lblEstimatedTime.WithSameCenterY(this.imgEstimatedTime),
                this.lblEstimatedTime.ToRightOf(this.imgEstimatedTime, CONTROLS_DISTANCE),
                this.lblEstimatedTime.AtRightOf(this.viewContainer),

                this.tripReturnHeaderView.Below(this.lblEstimatedTime, CONTROLS_DISTANCE / 2),
                this.tripReturnHeaderView.AtLeftOf(this.viewContainer),
                this.tripReturnHeaderView.AtRightOf(this.viewContainer),
                this.tripReturnHeaderHeightConstraint,

                this.tripReturnPickupView.Below(this.tripReturnHeaderView),
                this.tripReturnPickupView.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.tripReturnPickupView.AtRightOf(this.viewContainer),
                this.tripReturnPickupHeightConstraint,

                this.tripReturnStopsView.Below(this.tripReturnPickupView),
                this.tripReturnStopsView.WithSameLeft(this.tripReturnPickupView),
                this.tripReturnStopsView.WithSameRight(this.tripReturnPickupView),
                this.tripReturnStopsHeightConstraint,

                this.tripReturnDropoffView.Below(this.tripReturnStopsView),
                this.tripReturnDropoffView.WithSameLeft(this.tripReturnPickupView),
                this.tripReturnDropoffView.WithSameRight(this.tripReturnPickupView),
                this.tripReturnDropoffHeightConstraint,

                this.imgTripReturnEstimatedTime.Below(this.tripReturnDropoffView, CONTROLS_DISTANCE),
                this.imgTripReturnEstimatedTime.WithSameLeft(this.tripReturnStopsView),
                this.imgTripReturnEstimatedTimeHeightConstraint,

                this.lblTripReturnEstimatedTime.WithSameCenterY(this.imgTripReturnEstimatedTime),
                this.lblTripReturnEstimatedTime.ToRightOf(this.imgEstimatedTime, CONTROLS_DISTANCE),
                this.lblTripReturnEstimatedTime.AtRightOf(this.viewContainer),
                this.lblTripReturnEstimatedTimeHeightConstraint,
                this.lblTripReturnEstimatedTime.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<FindOpenTripsDetailView, FindOpenTripsDetailViewModel>();
            set.Bind(this.headerView.Title).To(vm => vm.Trip.TripType).WithConversion(ValueConverters.TripTypeEnumToTripTypeStringConverter);
            set.Bind(this.headerView.TripDate).To(vm => vm.PickupDate);
            set.Bind(this.headerView.TripTime).To(vm => vm.PickupTime);
            set.Bind(this.headerView.ReturnTrip).For(MvxBindings.Visibility).To(vm => vm.Trip.TripReturn).WithConversion(FwkValueConverters.VisibilityConverter);

            set.Bind(this.headerView.Footer).To(vm => vm.MilesFromHome);

            set.Bind(this.tripReturnHeaderHeightConstraint).For(v => v.Active).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToVisibilityConverter);
            set.Bind(this.tripReturnHeaderView.Title).To(vm => vm.Trip.TripReturn.TripType).WithConversion(ValueConverters.TripTypeEnumToTripTypeStringConverter);
            set.Bind(this.tripReturnHeaderView.TripDate).To(vm => vm.TripReturnPickupDate);
            set.Bind(this.tripReturnHeaderView.TripTime).To(vm => vm.TripReturnPickupTime);
            set.Bind(this.tripReturnHeaderView.ReturnTrip).For(MvxBindings.Visibility).To(vm => vm.Trip.TripReturn.TripReturn).WithConversion(FwkValueConverters.VisibilityConverter);

            set.Bind(this.tripReturnHeaderView.Footer).To(vm => vm.TripReturnMilesFromHome);

            //pickup
            set.Bind(this.pickupView.Subtitle).To(vm => vm.Pickup);
            set.Bind(this.pickupView.Airline).To(vm => vm.Trip.FromAirline.Name);
            set.Bind(this.pickupView.Tap()).For(g => g.Command).To(vm => vm.DisplayPickupDirectionCommand);
            set.Bind(this.pickupView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip).WithConversion(ValueConverters.TripPickupToStopOrAirportIconConverter);

            set.Bind(this.tripReturnPickupHeightConstraint).For(v => v.Active).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToVisibilityConverter);
            set.Bind(this.tripReturnPickupView.Subtitle).To(vm => vm.TripReturnPickup);
            set.Bind(this.tripReturnPickupView.Airline).To(vm => vm.Trip.TripReturn.FromAirline.Name);
            set.Bind(this.tripReturnPickupView.Tap()).For(g => g.Command).To(vm => vm.DisplayPickupDirectionCommand);
            set.Bind(this.tripReturnPickupView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripPickupToStopOrAirportIconConverter);

            //stops
            set.Bind(this.stopsView.StopsStackView).For(v => v.ItemsSource).To(vm => vm.Stops);
            set.Bind(this.stopsView.StopsStackView).For(v => v.SelectionChangedCommand).To(vm => vm.DisplayStopDirectionCommand);

            set.Bind(this.tripReturnStopsHeightConstraint).For(v => v.Active).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToVisibilityConverter);
            set.Bind(this.tripReturnStopsView.StopsStackView).For(v => v.ItemsSource).To(vm => vm.TripReturnStops);
            set.Bind(this.tripReturnStopsView.StopsStackView).For(v => v.SelectionChangedCommand).To(vm => vm.DisplayStopDirectionCommand);

            //dropoff
            set.Bind(this.dropoffView.Subtitle).To(vm => vm.Dropoff);
            set.Bind(this.dropoffView.Airline).To(vm => vm.Trip.ToAirline.Name);
            set.Bind(this.dropoffView.Tap()).For(g => g.Command).To(vm => vm.DisplayDropoffDirectionCommand);
            set.Bind(this.dropoffView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip).WithConversion(ValueConverters.TripDropoffToStopOrAirportIconConverter);

            set.Bind(this.tripReturnDropoffHeightConstraint).For(v => v.Active).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToVisibilityConverter);
            set.Bind(this.tripReturnDropoffView.Subtitle).To(vm => vm.TripReturnDropoff);
            set.Bind(this.tripReturnDropoffView.Airline).To(vm => vm.Trip.TripReturn.ToAirline.Name);
            set.Bind(this.tripReturnDropoffView.Tap()).For(g => g.Command).To(vm => vm.DisplayDropoffDirectionCommand);
            set.Bind(this.tripReturnDropoffView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripDropoffToStopOrAirportIconConverter);

            set.Bind(this.lblEstimatedTime).To(vm => vm.EstimatedDuration);

            set.Bind(this.lblTripReturnEstimatedTimeHeightConstraint).For(v => v.Active).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToVisibilityConverter);
            set.Bind(this.lblTripReturnEstimatedTime).To(vm => vm.TripReturnEstimatedDuration);

            set.Bind(this.imgTripReturnEstimatedTimeHeightConstraint).For(v => v.Active).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToVisibilityConverter);

            set.Bind(this.btnAccept.LblSwipe).To(vm => vm.AcceptText);
            set.Bind(this.btnAccept).For(v => v.UserInteractionEnabled).To(vm => vm.AcceptTripTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);

            set.Bind(this.actionOverlayView.ActionDetail).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToTripAcceptedStringConverter);
            set.Bind(this.actionOverlayView.BtnTop).For(MvxBindings.Title).To(vm => vm.Trip.TripReturn).WithConversion(ValueConverters.TripReturnToTripDetailsStringConverter);
            set.Bind(this.actionOverlayView.BtnTop).To(vm => vm.ViewTripDetailsCommand);
            set.Bind(this.actionOverlayView.BtnBottom).To(vm => vm.FindMoreTripsCommand);

            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.AcceptTripTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.NavigationController.InteractivePopGestureRecognizer.Enabled = false;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            this.NavigationController.InteractivePopGestureRecognizer.Enabled = true;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.AcceptTripTaskCompletion) && this.ViewModel.AcceptTripTaskCompletion != null)
            {
                this.ViewModel.AcceptTripTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(this.ViewModel.AcceptTripTaskCompletion.IsSuccessfullyCompleted)
            {
                this.View.BringSubviewToFront(this.actionOverlayView);

                this.actionOverlayView.Hidden = false;
                UIView.AnimateNotify(
                .2f,
                () => this.actionOverlayView.Alpha = .90f,
                completion =>
                {
                    this.actionOverlayView.Icon.Hidden = false;
                    this.actionOverlayView.IconToTopConstraint.Constant = this.actionOverlayView.MarginTop;
                    UIView.AnimateNotify(
                    .5f,
                    () =>
                    {
                        this.actionOverlayView.Icon.Alpha = 1f;
                        this.actionOverlayView.LayoutIfNeeded();
                    },
                        completionHandler => { }
                    );
                });
            }

            if(this.ViewModel.AcceptTripTaskCompletion.IsFaulted)
                this.btnAccept.Close();
        }

        private void BtnAccept_OnPanned(object sender, System.EventArgs e)
        {
            this.ViewModel.AcceptTripCommand.Execute(null);
        }
    }
}
