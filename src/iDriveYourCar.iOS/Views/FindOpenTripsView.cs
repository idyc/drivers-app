﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.Sources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class FindOpenTripsView : IDYCMenuController<FindOpenTripsViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;
        private const float SIDES_MARGIN = 16f;

        private UITableView tblOpenTrips;
        private UIView viewHeader, viewHeaderContainer;
        private FindOpenTripsTableSource source;
        private UIView viewFilter;
        private UIImageView imgFilter;
        private IDYCLabel lblDate, lblFilter;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.FindOpenTrips;

            this.View.BackgroundColor = IDYCColors.BackgroundColor;

            this.tblOpenTrips = new AutomaticDimensionTableView(72f)
            {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                BackgroundColor = UIColor.Clear
            };
            this.source = new FindOpenTripsTableSource(this.tblOpenTrips);
            this.tblOpenTrips.Source = this.source;

            this.viewHeader = new UIView();
            this.viewHeaderContainer = new UIView();
            this.tblOpenTrips.TableHeaderView = this.viewHeader;

            this.lblDate = new IDYCLabel(Metrics.FontSizeNormal) { TextColor = IDYCColors.DarkGrayTextColor };

            this.viewFilter = new UIView { UserInteractionEnabled = true };
            this.imgFilter = new UIImageView(UIImage.FromBundle(Assets.Sort));
            this.lblFilter = new IDYCLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.Filter.ToUpper(),
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.View.AggregateSubviews(this.tblOpenTrips);
            this.viewHeader.AggregateSubviews(this.viewHeaderContainer);
            this.viewHeaderContainer.AggregateSubviews(this.lblDate, this.viewFilter);
            this.viewFilter.AggregateSubviews(this.lblFilter, this.imgFilter);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.tblOpenTrips.AtLeftOf(this.View),
                this.tblOpenTrips.AtTopOf(this.View),
                this.tblOpenTrips.AtRightOf(this.View),
                this.tblOpenTrips.AtBottomOf(this.View)
            );

            this.viewHeader.AddConstraints(
                this.viewHeaderContainer.AtLeftOf(this.viewHeader, SIDES_MARGIN),
                this.viewHeaderContainer.AtTopOf(this.viewHeader, Metrics.WINDOW_MARGIN * 2f),
                this.viewHeaderContainer.AtRightOf(this.viewHeader, SIDES_MARGIN),
                this.viewHeaderContainer.AtBottomOf(this.viewHeader, Metrics.WINDOW_MARGIN)
            );

            this.viewHeaderContainer.AddConstraints(
                this.lblDate.AtLeftOf(this.viewHeaderContainer),
                this.lblDate.AtTopOf(this.viewHeaderContainer, Metrics.WINDOW_MARGIN),
                this.lblDate.AtBottomOf(this.viewHeaderContainer),

                this.viewFilter.AtRightOf(this.viewHeaderContainer),
                this.viewFilter.WithSameCenterY(this.lblDate)
            );

            this.viewFilter.AddConstraints(
                this.imgFilter.AtRightOf(this.viewFilter),
                this.imgFilter.AtTopOf(this.viewFilter),
                this.imgFilter.AtBottomOf(this.viewFilter),

                this.lblFilter.ToLeftOf(this.imgFilter, CONTROLS_DISTANCE / 2),
                this.lblFilter.AtLeftOf(this.viewFilter),
                this.lblFilter.WithSameCenterY(this.imgFilter)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<FindOpenTripsView, FindOpenTripsViewModel>();
            set.Bind(this.source).For(v => v.ItemsSource).To(vm => vm.OpenTrips);
            set.Bind(this.source).For(v => v.SelectionChangedCommand).To(vm => vm.DisplayOpenTripDetailCommand);
            set.Bind(this.lblDate).To(vm => vm.SelectedDateRange);
            set.Bind(this.viewFilter.Tap()).For(g => g.Command).To(vm => vm.ToggleFilterOpenTripsCommand);
            set.Bind(this.imgFilter).For(MvxBindings.UIImageName).To(vm => vm.IsFilterApplied).WithConversion(ValueConverters.BoolToFilterDrawableConverter);
            set.Bind(this.lblFilter).To(vm => vm.IsFilterApplied).WithConversion(ValueConverters.BoolToFilterTextConverter);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            this.tblOpenTrips.SetupDynamicHeightTableViewHeader();
        }
    }
}
