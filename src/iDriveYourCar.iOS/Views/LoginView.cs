﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.Root, true, true)]
    public class LoginView : IDYCController<LoginViewModel>
    {
        private const float FORM_MARGIN = 60f;
        private const float MARGIN = Metrics.WINDOW_MARGIN * 2;
        private const float CONTROLS_DISTANCE = 8f;
        private const float APPLY_NOW_PADDING = 16f;

        private UIScrollView scrollViewForm;
        private UIView viewContainer;
        private BorderedIconTextField txtUsername, txtPassword;
        private PrimaryBackgroundButton btnLogin;
        private PrimaryTextButton btnForgotPassword;

        private UIView viewLine, viewApplyAsDriver;
        private IDYCLabel lblNotADriver;
        private PrimaryTextButton btnApplyNow;

        public override bool HandlesKeyboardNotifications() => true;

        public LoginView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.View.BackgroundColor = IDYCColors.BackgroundColor;

            this.scrollViewForm = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.txtUsername = new BorderedIconTextField(IDriveYourCarStrings.Email, Assets.Envelope);
            this.txtPassword = new BorderedIconTextField(IDriveYourCarStrings.Password, Assets.Password);
            this.txtPassword.TextField.SecureTextEntry = true;

            this.btnLogin = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnLogin.SetTitle(IDriveYourCarStrings.SignIn.ToUpper(), UIControlState.Normal);
            this.btnForgotPassword = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeSmall);
            this.btnForgotPassword.SetTitle(IDriveYourCarStrings.ForgotPassword, UIControlState.Normal);

            this.viewLine = new UIView { BackgroundColor = IDYCColors.LightGrayTextColor };
            this.viewApplyAsDriver = new UIView();
            this.lblNotADriver = new IDYCLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.NotADriver,
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.btnApplyNow = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnApplyNow.SetTitle(IDriveYourCarStrings.ApplyNow, UIControlState.Normal);


            this.View.AggregateSubviews(this.scrollViewForm, this.viewLine, this.viewApplyAsDriver);
            this.scrollViewForm.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(this.txtUsername, this.txtPassword, this.btnLogin, this.btnForgotPassword);
            this.viewApplyAsDriver.AggregateSubviews(this.lblNotADriver, this.btnApplyNow);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollViewForm.AtLeftOf(this.View),
                this.scrollViewForm.AtTopOf(this.View),
                this.scrollViewForm.AtRightOf(this.View),

                this.viewLine.Below(this.scrollViewForm),
                this.viewLine.AtLeftOf(this.View),
                this.viewLine.AtRightOf(this.View),
                this.viewLine.Height().EqualTo(Metrics.LineThickness * 2),

                this.viewApplyAsDriver.Below(this.viewLine, APPLY_NOW_PADDING),
                this.viewApplyAsDriver.WithSameCenterX(this.View),
                this.viewApplyAsDriver.AtBottomOf(this.View, APPLY_NOW_PADDING)
            );

            this.scrollViewForm.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollViewForm),
                this.viewContainer.AtTopOf(this.scrollViewForm),
                this.viewContainer.AtRightOf(this.scrollViewForm),
                this.viewContainer.AtBottomOf(this.scrollViewForm)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.txtUsername.AtTopOf(this.viewContainer, FORM_MARGIN),
                this.txtUsername.AtLeftOf(this.viewContainer, MARGIN),
                this.txtUsername.AtRightOf(this.viewContainer, MARGIN),

                this.txtPassword.Below(this.txtUsername, Metrics.DEFAULT_DISTANCE),
                this.txtPassword.AtLeftOf(this.viewContainer, MARGIN),
                this.txtPassword.AtRightOf(this.viewContainer, MARGIN),

                this.btnLogin.Below(this.txtPassword, Metrics.DEFAULT_DISTANCE),
                this.btnLogin.AtLeftOf(this.viewContainer, MARGIN),
                this.btnLogin.AtRightOf(this.viewContainer, MARGIN),
                this.btnLogin.AtBottomOf(this.viewContainer, FORM_MARGIN),

                this.btnForgotPassword.Below(this.btnLogin, CONTROLS_DISTANCE),
                this.btnForgotPassword.WithSameRight(this.btnLogin)
            );

            this.txtUsername.AddConstraints(
                this.txtUsername.RightIcon.Width().EqualTo(15f),
                this.txtUsername.RightIcon.Height().EqualTo(15f)
            );

            this.txtPassword.AddConstraints(
                this.txtPassword.RightIcon.Width().EqualTo(15f),
                this.txtPassword.RightIcon.Height().EqualTo(15f)
            );

            this.viewApplyAsDriver.AddConstraints(
                this.lblNotADriver.AtLeftOf(this.viewApplyAsDriver),
                this.lblNotADriver.AtTopOf(this.viewApplyAsDriver),
                this.lblNotADriver.AtBottomOf(this.viewApplyAsDriver),

                this.btnApplyNow.ToRightOf(this.lblNotADriver, CONTROLS_DISTANCE / 2),
                this.btnApplyNow.AtRightOf(this.viewApplyAsDriver),
                this.btnApplyNow.WithSameCenterY(this.lblNotADriver)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<LoginView, LoginViewModel>();
            set.Bind(this.txtUsername.TextField).To(vm => vm.Username);
            set.Bind(this.txtPassword.TextField).To(vm => vm.Password);
            set.Bind(this.btnLogin).To(vm => vm.LoginCommand);
            set.Bind(this.btnForgotPassword).To(vm => vm.ShowForgotPasswordCommand);
            set.Bind(this.btnApplyNow).To(vm => vm.ShowSignUpCommand);
            set.Apply();

            this.AddLoadingOverlayBinding(this, nameof(this.ViewModel.LoginTaskCompletion));
            this.AddNoInternetToastBinding(this.View, nameof(this.ViewModel.LoginTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.ScrollToCenterOnKeyboardShown = this.scrollViewForm;
            this.ViewToCenterOnKeyboardShown = this.txtPassword;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
