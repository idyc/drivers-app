﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Helpers;
using FFImageLoading.Transformations;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.Rules.ProfileGeneral;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class ProfileView : IDYCMenuController<ProfileViewModel>
    {
        private const float AVATAR_SIZE = 45f;
        private const float CONTROLS_DISTANCE = 16f;
        private const float AVATAR_DISTANCES = 20f;

        private UIScrollView scrollView;
        private UIView viewContainer;
        private UIView viewLineGeneral;
        private UIImageView imgAvatar;
        private IDYCMultilineLabel lblNameSurname;

        private InformationHeaderView generalHeader, bankingHeader, documentsHeader;
        private InformationRow viewMobilePhone, viewEmail, viewHomePhone, viewAddress,
                                viewNameOnAccount, viewTaxClassification, viewSSN, viewRoutingNumber, viewAccountNumber;

        private SimpleBindableStackView<ProfileDocumentItemView, DocumentItemViewModel> documentsStack;
        private DarkGrayTextButton btnLogout;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Profile;

            this.scrollView = new UIScrollView
            {
                DelaysContentTouches = false,
                BackgroundColor = UIColor.White
            };
            this.viewContainer = new UIView();
            this.viewLineGeneral = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.imgAvatar = new UIImageView(UIImage.FromBundle("ic_profile_picture.jpg")) { UserInteractionEnabled = true };
            this.lblNameSurname = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.generalHeader = new InformationHeaderView(IDriveYourCarStrings.General);
            this.viewMobilePhone = new InformationRow(IDriveYourCarStrings.MobilePhone);
            this.viewEmail = new InformationRow(IDriveYourCarStrings.Email);
            this.viewEmail.SetEnabled(false);
            this.viewHomePhone = new InformationRow(IDriveYourCarStrings.HomePhone);
            this.viewAddress = new InformationRow(IDriveYourCarStrings.Address);

            this.bankingHeader = new InformationHeaderView(IDriveYourCarStrings.Banking);
            this.viewNameOnAccount = new InformationRow(IDriveYourCarStrings.NameOnAccount);
            this.viewTaxClassification = new InformationRow(IDriveYourCarStrings.TaxClassification);
            this.viewSSN = new InformationRow(IDriveYourCarStrings.SSNTIN);
            this.viewRoutingNumber = new InformationRow(IDriveYourCarStrings.RoutingNumber);
            this.viewAccountNumber = new InformationRow(IDriveYourCarStrings.AccountNumber);

            this.documentsHeader = new InformationHeaderView(IDriveYourCarStrings.Documents);
            this.documentsStack = new SimpleBindableStackView<ProfileDocumentItemView, DocumentItemViewModel>(UILayoutConstraintAxis.Vertical);

            this.btnLogout = new DarkGrayTextButton(Metrics.FontSizeNormal);
            this.btnLogout.SetTitle(IDriveYourCarStrings.Logout.ToUpper(), UIControlState.Normal);

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.generalHeader,
                this.viewLineGeneral,
                this.imgAvatar,
                this.lblNameSurname,
                this.viewMobilePhone,
                this.viewEmail,
                this.viewHomePhone,
                this.viewAddress,
                this.bankingHeader,
                this.viewNameOnAccount,
                this.viewTaxClassification,
                this.viewSSN,
                this.viewRoutingNumber,
                this.viewAccountNumber,
                this.documentsHeader,
                this.documentsStack,
                this.btnLogout);

            #region gesture recognizers
            this.viewMobilePhone.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.CellPhone)));

            this.viewHomePhone.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.HomePhone)));

            this.viewAddress.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverAddressCommand.Execute(null)));

            this.viewNameOnAccount.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.NameBankAccount)));

            this.viewTaxClassification.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.TaxId)));

            this.viewSSN.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.SSNTIN)));

            this.viewRoutingNumber.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.RoutingNumber)));

            this.viewAccountNumber.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.ViewModel.UpdateDriverInformationCommand.Execute(DriverEditableField.AccountNumber)));
            #endregion
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 2)
            );

            this.viewContainer.AddConstraints(
                this.generalHeader.AtLeftOf(this.viewContainer),
                this.generalHeader.AtTopOf(this.viewContainer),
                this.generalHeader.AtRightOf(this.viewContainer),

                this.viewLineGeneral.Below(this.generalHeader),
                this.viewLineGeneral.AtLeftOf(this.viewContainer),
                this.viewLineGeneral.AtRightOf(this.viewContainer),
                this.viewLineGeneral.Height().EqualTo(Metrics.LineThickness),

                this.imgAvatar.Below(this.viewLineGeneral, AVATAR_DISTANCES),
                this.imgAvatar.AtLeftOf(this.viewContainer),
                this.imgAvatar.Height().EqualTo(AVATAR_SIZE),
                this.imgAvatar.Width().EqualTo(AVATAR_SIZE),

                this.lblNameSurname.ToRightOf(this.imgAvatar, AVATAR_DISTANCES),
                this.lblNameSurname.WithSameCenterY(this.imgAvatar),
                this.lblNameSurname.AtRightOf(this.viewContainer),

                this.viewMobilePhone.Below(this.imgAvatar, AVATAR_DISTANCES),
                this.viewMobilePhone.AtLeftOf(this.viewContainer),
                this.viewMobilePhone.AtRightOf(this.viewContainer),

                this.viewEmail.Below(this.viewMobilePhone),
                this.viewEmail.AtLeftOf(this.viewContainer),
                this.viewEmail.AtRightOf(this.viewContainer),

                this.viewHomePhone.Below(this.viewEmail),
                this.viewHomePhone.AtLeftOf(this.viewContainer),
                this.viewHomePhone.AtRightOf(this.viewContainer),

                this.viewAddress.Below(this.viewHomePhone),
                this.viewAddress.AtLeftOf(this.viewContainer),
                this.viewAddress.AtRightOf(this.viewContainer),

                this.bankingHeader.Below(this.viewAddress),
                this.bankingHeader.AtLeftOf(this.viewContainer),
                this.bankingHeader.AtRightOf(this.viewContainer),

                this.viewNameOnAccount.Below(this.bankingHeader),
                this.viewNameOnAccount.AtLeftOf(this.viewContainer),
                this.viewNameOnAccount.AtRightOf(this.viewContainer),

                this.viewTaxClassification.Below(this.viewNameOnAccount),
                this.viewTaxClassification.AtLeftOf(this.viewContainer),
                this.viewTaxClassification.AtRightOf(this.viewContainer),

                this.viewSSN.Below(this.viewTaxClassification),
                this.viewSSN.AtLeftOf(this.viewContainer),
                this.viewSSN.AtRightOf(this.viewContainer),

                this.viewRoutingNumber.Below(this.viewSSN),
                this.viewRoutingNumber.AtLeftOf(this.viewContainer),
                this.viewRoutingNumber.AtRightOf(this.viewContainer),

                this.viewAccountNumber.Below(this.viewRoutingNumber),
                this.viewAccountNumber.AtLeftOf(this.viewContainer),
                this.viewAccountNumber.AtRightOf(this.viewContainer),

                this.documentsHeader.Below(this.viewAccountNumber),
                this.documentsHeader.AtLeftOf(this.viewContainer),
                this.documentsHeader.AtRightOf(this.viewContainer),

                this.documentsStack.Below(this.documentsHeader),
                this.documentsStack.AtLeftOf(this.viewContainer),
                this.documentsStack.AtRightOf(this.viewContainer),

                this.btnLogout.Below(this.documentsStack, CONTROLS_DISTANCE),
                this.btnLogout.WithSameCenterX(this.viewContainer).Minus(Metrics.WINDOW_MARGIN),
                this.btnLogout.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ProfileView, ProfileViewModel>();
            set.Bind(this.imgAvatar.Tap()).For(g => g.Command).To(vm => vm.ShowUploadAvatarCommand);

            set.Bind(this.lblNameSurname).To(vm => vm.Driver).WithConversion(ValueConverters.DriverToNameSurnameConverter);
            set.Bind(this.viewMobilePhone.LblInformation).To(vm => vm.Driver.CellPhone).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);
            set.Bind(this.viewEmail.LblInformation).To(vm => vm.Driver.User.Email).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);
            set.Bind(this.viewHomePhone.LblInformation).To(vm => vm.Driver.HomePhone).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);

            set.Bind(this.viewAddress.LblInformation).To(vm => vm.Driver.User.PrimaryAddress).WithConversion(ValueConverters.ProfileAddressFieldToNoneIfEmptyConverter);

            set.Bind(this.viewNameOnAccount.LblInformation).To(vm => vm.Driver.NameBankAccount).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);
            set.Bind(this.viewTaxClassification.LblInformation).To(vm => vm.Driver.TaxId).WithConversion(ValueConverters.TaxClassificationToNoneIfNullConverter);
            set.Bind(this.viewSSN.LblInformation).To(vm => vm.Driver.SSNTIN).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);
            set.Bind(this.viewRoutingNumber.LblInformation).To(vm => vm.Driver.RoutingNumber).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);
            set.Bind(this.viewAccountNumber.LblInformation).To(vm => vm.Driver.AccountNumber).WithConversion(ValueConverters.ProfileEditableFieldToNoneIfEmptyConverter);

            set.Bind(this.documentsStack).For(v => v.ItemsSource).To(vm => vm.Documents);

            set.Bind(this.btnLogout).To(vm => vm.LogoutCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            FFImageHelper.Instance.LoadImageFromUrl(
                this.imgAvatar,
                this.ViewModel.Driver.User.Avatar,
                "ic_profile_picture.jpg",
                transformation: new CircleTransformation(),
                sampleWidth: 80,
                sampleHeight: 80);
        }
    }
}
