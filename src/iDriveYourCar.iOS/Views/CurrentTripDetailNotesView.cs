﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    public class CurrentTripDetailNotesView : IDYCController<CurrentTripDetailNotesViewModel>
    {
        private UIScrollView scrollView;
        private UIView viewContainer;

        private TripDetailsHeaderView headerView;
        private TripNotesView carKeyView, meetupView, cAccountView, cNotesView;
        private UIView carKeyLine, meetupLine, cAccountLine;

        private FluentLayout carKeyCollapsedConstraint,
                            carKeyLineCollapsedConstraint,
                            meetupCollapsedConstraint,
                            meetupLineCollapsedConstraint,
                            cAccountCollapsedConstraint,
                            cAccountLineCollapsedConstraint,
                            cNotesCollapsedConstraint;

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.carKeyLine = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.meetupLine = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.cAccountLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.headerView = new TripDetailsHeaderView();
            this.carKeyView = new TripNotesView(Assets.Key)
            {
                ClipsToBounds = true
            };
            this.carKeyView.Title.Text = IDriveYourCarStrings.CarDescriptionAndKeyLocation;
            this.meetupView = new TripNotesView(Assets.Person)
            {
                ClipsToBounds = true
            };
            this.meetupView.Title.Text = IDriveYourCarStrings.MeetupInstructions;
            this.cAccountView = new TripNotesView(Assets.Notes)
            {
                ClipsToBounds = true
            };
            this.cAccountView.Title.Text = IDriveYourCarStrings.TripNotes;
            this.cNotesView = new TripNotesView(Assets.Person)
            {
                ClipsToBounds = true
            };
            this.cNotesView.Title.Text = IDriveYourCarStrings.ClientNotes;

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.headerView,
                this.carKeyView,
                this.carKeyLine,
                this.meetupView,
                this.meetupLine,
                this.cAccountView,
                this.cAccountLine,
                this.cNotesView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.carKeyCollapsedConstraint = this.carKeyView.Height().EqualTo(0).SetActive(false);
            this.carKeyLineCollapsedConstraint = this.carKeyLine.Height().EqualTo(0).SetActive(false);
            this.meetupCollapsedConstraint = this.meetupView.Height().EqualTo(0).SetActive(false);
            this.meetupLineCollapsedConstraint = this.meetupLine.Height().EqualTo(0).SetActive(false);
            this.cAccountCollapsedConstraint = this.cAccountView.Height().EqualTo(0).SetActive(false);
            this.cAccountLineCollapsedConstraint = this.cAccountLine.Height().EqualTo(0).SetActive(false);
            this.cNotesCollapsedConstraint = this.cNotesView.Height().EqualTo(0).SetActive(false);

            this.viewContainer.AddConstraints(
                this.headerView.AtLeftOf(this.viewContainer),
                this.headerView.AtTopOf(this.viewContainer),
                this.headerView.AtRightOf(this.viewContainer),

                this.carKeyView.Below(this.headerView),
                this.carKeyView.AtLeftOf(this.viewContainer),
                this.carKeyView.AtRightOf(this.viewContainer),
                this.carKeyCollapsedConstraint,

                this.carKeyLine.Below(this.carKeyView),
                this.carKeyLine.WithSameLeft(this.carKeyView.Title),
                this.carKeyLine.AtRightOf(this.viewContainer),
                this.carKeyLine.Height().EqualTo(Metrics.LineThickness),
                this.carKeyLineCollapsedConstraint,

                this.meetupView.Below(this.carKeyLine),
                this.meetupView.AtLeftOf(this.viewContainer),
                this.meetupView.AtRightOf(this.viewContainer),
                this.meetupCollapsedConstraint,

                this.meetupLine.Below(this.meetupView),
                this.meetupLine.WithSameLeft(this.carKeyLine),
                this.meetupLine.WithSameRight(this.carKeyLine),
                this.meetupLine.Height().EqualTo(Metrics.LineThickness),
                this.meetupLineCollapsedConstraint,

                this.cAccountView.Below(this.meetupLine),
                this.cAccountView.AtLeftOf(this.viewContainer),
                this.cAccountView.AtRightOf(this.viewContainer),
                this.cAccountCollapsedConstraint,

                this.cAccountLine.Below(this.cAccountView),
                this.cAccountLine.WithSameLeft(this.carKeyLine),
                this.cAccountLine.WithSameRight(this.carKeyLine),
                this.cAccountLine.Height().EqualTo(Metrics.LineThickness),
                this.cAccountLineCollapsedConstraint,

                this.cNotesView.Below(this.cAccountLine),
                this.cNotesView.AtLeftOf(this.viewContainer),
                this.cNotesView.AtRightOf(this.viewContainer),
                this.cNotesView.AtBottomOf(this.viewContainer),
                this.cNotesCollapsedConstraint
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CurrentTripDetailNotesView, CurrentTripDetailNotesViewModel>();
            set.Bind(this.headerView.Title).To(vm => vm.Trip).WithConversion(ValueConverters.TripToClientNameSurnameConverter);
            set.Bind(this.headerView.ReturnTrip).For(MvxBindings.Visibility).To(vm => vm.Trip.TripReturn).WithConversion(FwkValueConverters.VisibilityConverter);
            set.Bind(this.headerView.TripDate).To(vm => vm.PickupDate);
            set.Bind(this.headerView.TripTime).To(vm => vm.PickupTime);
            set.Bind(this.headerView.Footer).To(vm => vm.Trip.TripType).WithConversion(ValueConverters.TripTypeEnumToTripTypeStringConverter);
            set.Bind(this.carKeyView.Text).To(vm => vm.Trip.KeyLocationVehicleDescription);
            set.Bind(this.meetupView.Text).To(vm => vm.Trip.MeetupInstructions);
            set.Bind(this.cAccountView.Text).To(vm => vm.Trip.Client.AccountNotes);
            set.Bind(this.cNotesView.Text).To(vm => vm.Trip.Client.NotesForDriver);

            set.Bind(this.carKeyCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.KeyLocationVehicleDescription).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Bind(this.carKeyLineCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.KeyLocationVehicleDescription).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Bind(this.meetupCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.MeetupInstructions).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Bind(this.meetupLineCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.MeetupInstructions).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Bind(this.cAccountCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.Client.AccountNotes).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Bind(this.cAccountLineCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.Client.AccountNotes).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Bind(this.cNotesCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.Client.NotesForDriver).WithConversion(FwkValueConverters.StringEmptyToBoolConverter);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
