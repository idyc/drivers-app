﻿using Airbnb.Lottie;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true)]
    public class TripAcceptanceView : IDYCController<TripAcceptanceViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;
        private const float VIEW_HEADER_FOOTER_HEIGHT = 70f;
        private const float ANIMATION_SIZE = 300f;
        private const float INTERNAL_VIEW_PADDING = ANIMATION_SIZE / 4;

        private IDYCLabel lblReject, lblRoundTripDropoff, lblRoundTripPickup, lblRoundTripDropoffDateTime, lblRoundTripPickupDateTime,
            lblRoundTripDropoffAddress, lblRoundTripPickupAddress, lblPickupAbbreviation, lblPickupAddress, lblDropoffAbbreviation, lblDropoffAddress, lblPickupDate, lblPickupTime;

        private UIView viewContainer, dummyView, viewReject, tripAcceptanceRoundTripDropoffView, tripAcceptanceRoundTripPickupView, tripAcceptanceHeaderView, tripAcceptanceFooterView;

        private UIScrollView scrollView;

        private LOTAnimationView animationView;

        private TripActionOverlayView actionOverlayView;

        private TripAcceptInternalView internalView;

        private UIImageView imgReject;

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView
            {
                DelaysContentTouches = false,
                Bounces = false
            };

            this.viewContainer = new UIView();
            this.dummyView = new UIView();
            this.tripAcceptanceRoundTripDropoffView = new UIView();
            this.tripAcceptanceRoundTripPickupView = new UIView();
            this.tripAcceptanceHeaderView = new UIView();
            this.tripAcceptanceFooterView = new UIView();

            this.lblPickupAbbreviation = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                Text = IDriveYourCarStrings.PickupAbbreviation,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblDropoffAbbreviation = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                Text = IDriveYourCarStrings.DropoffAbbreviation,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblRoundTripDropoff = new IDYCLabel(Metrics.FontSizeGigant)
            {
                Text = IDriveYourCarStrings.DropOff,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblRoundTripPickup = new IDYCLabel(Metrics.FontSizeGigant)
            {
                Text = IDriveYourCarStrings.PickUp,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblRoundTripDropoffDateTime = new IDYCLabel(Metrics.FontSizeHuge)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblRoundTripPickupDateTime = new IDYCLabel(Metrics.FontSizeHuge)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblRoundTripDropoffAddress = new IDYCLabel(Metrics.FontSizeHuge)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblRoundTripPickupAddress = new IDYCLabel(Metrics.FontSizeHuge)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblPickupAddress = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblDropoffAddress = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblPickupDate = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblPickupTime = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.animationView = LOTAnimationView.AnimationNamed("accept_trip");

            this.actionOverlayView = new TripActionOverlayView(Assets.CheckWhiteBig);
            this.actionOverlayView.ActionTitle.Text = IDriveYourCarStrings.Success;
            this.actionOverlayView.BtnTop.SetTitle(IDriveYourCarStrings.ViewTripDetails.ToUpper(), UIControlState.Normal);
            this.actionOverlayView.BtnBottom.SetTitle(IDriveYourCarStrings.FindMoreTrips.ToUpper(), UIControlState.Normal);
            this.actionOverlayView.Hidden = true;
            this.actionOverlayView.Alpha = 0f;
            this.actionOverlayView.Icon.Hidden = true;
            this.actionOverlayView.Icon.Alpha = 0f;

            this.viewReject = new UIView
            {
                UserInteractionEnabled = true
            };

            this.imgReject = new UIImageView(UIImage.FromBundle(Assets.Close));
            this.lblReject = new IDYCLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.Reject.ToUpper(),
                TextColor = IDYCColors.IconGrayColor
            };

            this.internalView = new TripAcceptInternalView();

            this.View.AggregateSubviews(this.scrollView, this.actionOverlayView);

            this.scrollView.AggregateSubviews(this.viewContainer);

            this.viewContainer.AggregateSubviews(
                this.viewReject,
                this.animationView,
                this.internalView,
                this.tripAcceptanceRoundTripDropoffView,
                this.tripAcceptanceRoundTripPickupView,
                this.tripAcceptanceHeaderView,
                this.tripAcceptanceFooterView,
                this.dummyView);

            this.viewReject.AggregateSubviews(this.imgReject, this.lblReject);

            this.tripAcceptanceRoundTripDropoffView.AggregateSubviews(
                this.lblRoundTripDropoff,
                this.lblRoundTripDropoffDateTime,
                this.lblRoundTripDropoffAddress);

            this.tripAcceptanceRoundTripPickupView.AggregateSubviews(
                this.lblRoundTripPickup,
                this.lblRoundTripPickupDateTime,
                this.lblRoundTripPickupAddress);

            this.tripAcceptanceHeaderView.AggregateSubviews(
                this.lblPickupDate,
                this.lblPickupTime
            );

            this.tripAcceptanceFooterView.AggregateSubviews(
                this.lblPickupAbbreviation,
                this.lblDropoffAbbreviation,
                this.lblPickupAddress,
                this.lblDropoffAddress);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.actionOverlayView.AtLeftOf(this.View),
                this.actionOverlayView.AtTopOf(this.View),
                this.actionOverlayView.AtRightOf(this.View),
                this.actionOverlayView.AtBottomOf(this.View),

                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtTopOf(this.scrollView, CONTROLS_DISTANCE),
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.viewReject.AtTopOf(this.viewContainer),
                this.viewReject.AtLeftOf(this.viewContainer, CONTROLS_DISTANCE / 2),

                this.tripAcceptanceRoundTripDropoffView.Below(this.viewReject, CONTROLS_DISTANCE),
                this.tripAcceptanceRoundTripDropoffView.WithSameCenterX(this.viewContainer),
                this.tripAcceptanceRoundTripDropoffView.Above(this.animationView, CONTROLS_DISTANCE / 2),
                this.tripAcceptanceRoundTripDropoffView.Height().EqualTo(VIEW_HEADER_FOOTER_HEIGHT),

                this.tripAcceptanceHeaderView.Below(this.viewReject, CONTROLS_DISTANCE),
                this.tripAcceptanceHeaderView.WithSameCenterX(this.viewContainer),
                this.tripAcceptanceHeaderView.Above(this.animationView, CONTROLS_DISTANCE / 2),
                this.tripAcceptanceHeaderView.Height().EqualTo(VIEW_HEADER_FOOTER_HEIGHT),

                this.animationView.WithSameCenterX(this.viewContainer),
                this.animationView.WithSameCenterY(this.viewContainer),
                this.animationView.Width().EqualTo(ANIMATION_SIZE),
                this.animationView.Height().EqualTo().WidthOf(this.animationView),

                this.internalView.WithSameLeft(this.animationView).Plus(INTERNAL_VIEW_PADDING),
                this.internalView.WithSameTop(this.animationView).Plus(INTERNAL_VIEW_PADDING),
                this.internalView.WithSameRight(this.animationView).Minus(INTERNAL_VIEW_PADDING),
                this.internalView.WithSameBottom(this.animationView).Minus(INTERNAL_VIEW_PADDING),

                this.tripAcceptanceRoundTripPickupView.Below(this.animationView, CONTROLS_DISTANCE / 2),
                this.tripAcceptanceRoundTripPickupView.WithSameCenterX(this.viewContainer),
                this.tripAcceptanceRoundTripPickupView.Height().EqualTo(VIEW_HEADER_FOOTER_HEIGHT),

                this.tripAcceptanceFooterView.Below(this.animationView, CONTROLS_DISTANCE / 2),
                this.tripAcceptanceFooterView.WithSameCenterX(this.viewContainer),
                this.tripAcceptanceFooterView.Height().EqualTo(VIEW_HEADER_FOOTER_HEIGHT),

                this.dummyView.Below(this.tripAcceptanceRoundTripPickupView),
                this.dummyView.AtBottomOf(this.viewContainer)
            );

            this.viewReject.AddConstraints(
                this.lblReject.AtRightOf(this.viewReject),
                this.lblReject.AtTopOf(this.viewReject),
                this.lblReject.AtBottomOf(this.viewReject),

                this.imgReject.ToLeftOf(this.lblReject, Metrics.DEFAULT_DISTANCE / 2),
                this.imgReject.WithSameTop(this.lblReject),
                this.imgReject.WithSameBottom(this.lblReject),
                this.imgReject.AtLeftOf(this.viewReject)
            );

            this.tripAcceptanceRoundTripDropoffView.AddConstraints(
                this.lblRoundTripDropoff.AtTopOf(this.tripAcceptanceRoundTripDropoffView),
                this.lblRoundTripDropoff.WithSameCenterX(this.tripAcceptanceRoundTripDropoffView),
                this.lblRoundTripDropoff.Above(this.lblRoundTripDropoffDateTime, CONTROLS_DISTANCE / 2),

                this.lblRoundTripDropoffDateTime.WithSameCenterX(this.tripAcceptanceRoundTripDropoffView),
                this.lblRoundTripDropoffDateTime.WithSameCenterY(this.tripAcceptanceRoundTripDropoffView),
                this.lblRoundTripDropoffDateTime.Above(this.lblRoundTripDropoffAddress, CONTROLS_DISTANCE / 2),

                this.lblRoundTripDropoffAddress.WithSameCenterX(this.tripAcceptanceRoundTripDropoffView),
                this.lblRoundTripDropoffAddress.AtBottomOf(this.tripAcceptanceRoundTripDropoffView)
            );

            this.tripAcceptanceRoundTripPickupView.AddConstraints(
                this.lblRoundTripPickup.AtTopOf(this.tripAcceptanceRoundTripPickupView),
                this.lblRoundTripPickup.WithSameCenterX(this.tripAcceptanceRoundTripPickupView),

                this.lblRoundTripPickupDateTime.Below(this.lblRoundTripPickup, CONTROLS_DISTANCE / 2),
                this.lblRoundTripPickupDateTime.WithSameCenterX(this.tripAcceptanceRoundTripPickupView),
                this.lblRoundTripPickupDateTime.WithSameCenterY(this.tripAcceptanceRoundTripPickupView),

                this.lblRoundTripPickupAddress.Below(this.lblRoundTripPickupDateTime, CONTROLS_DISTANCE / 2),
                this.lblRoundTripPickupAddress.WithSameCenterX(this.tripAcceptanceRoundTripPickupView),
                this.lblRoundTripPickupAddress.AtBottomOf(this.tripAcceptanceRoundTripPickupView)
            );

            this.tripAcceptanceHeaderView.AddConstraints(
                this.lblPickupDate.AtTopOf(this.tripAcceptanceHeaderView),
                this.lblPickupDate.WithSameCenterX(this.tripAcceptanceHeaderView),

                this.lblPickupTime.Below(this.lblPickupDate, CONTROLS_DISTANCE / 2),
                this.lblPickupTime.WithSameCenterX(this.tripAcceptanceHeaderView),
                this.lblPickupTime.AtBottomOf(this.tripAcceptanceHeaderView)
            );

            this.tripAcceptanceFooterView.AddConstraints(
                this.lblPickupAddress.AtTopOf(this.tripAcceptanceFooterView),
                this.lblPickupAddress.WithSameCenterX(this.tripAcceptanceFooterView),

                this.lblPickupAbbreviation.WithSameCenterY(this.lblPickupAddress),
                this.lblPickupAbbreviation.ToLeftOf(this.lblPickupAddress, CONTROLS_DISTANCE / 2),

                this.lblDropoffAbbreviation.WithSameCenterY(this.lblDropoffAddress),
                this.lblDropoffAbbreviation.ToLeftOf(this.lblDropoffAddress, CONTROLS_DISTANCE / 2),

                this.lblDropoffAddress.Below(this.lblPickupAddress, CONTROLS_DISTANCE / 2),
                this.lblDropoffAddress.WithSameCenterX(this.tripAcceptanceFooterView),
                this.lblDropoffAddress.AtBottomOf(this.tripAcceptanceFooterView)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<TripAcceptanceView, TripAcceptanceViewModel>();

            set.Bind(this.internalView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushTypeToTripPushIconConverter);
            set.Bind(this.internalView.TopLabel).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushToRoundTripOrEstimatedTimeStringConverter);
            set.Bind(this.internalView.BottomLabel).To(vm => vm.Trip.TimeFromHome).WithConversion(ValueConverters.TripPushToTimeFromHomeStringConverter);

            set.Bind(this.lblRoundTripDropoffDateTime).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushRoundTripPickupToDateAtTimeAMPMStringConverter);
            set.Bind(this.lblRoundTripPickupDateTime).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushRoundTripDropoffToDateAtTimeAMPMStringConverter);
            set.Bind(this.lblRoundTripDropoffAddress).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushRoundTripDropoffToPickupDropoffStringConverter);
            set.Bind(this.lblRoundTripPickupAddress).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushRoundTripPickupToPickupDropoffStringConverter);

            set.Bind(this.lblPickupDate).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushPickupToDateStringConverter);
            set.Bind(this.lblPickupTime).To(vm => vm.Trip).WithConversion(ValueConverters.TripPushPickupToTimeAMPMStringConverter);

            set.Bind(this.lblPickupAddress).To(vm => vm.Trip.PickupAddress);
            set.Bind(this.lblDropoffAddress).To(vm => vm.Trip.DropoffAddress);

            set.Bind(this.tripAcceptanceRoundTripDropoffView).For(v => v.Hidden).To(vm => vm.Trip.Return).WithConversion(ValueConverters.TripPushHasReturnTripToInvertedBoolConverter);
            set.Bind(this.tripAcceptanceRoundTripPickupView).For(v => v.Hidden).To(vm => vm.Trip.Return).WithConversion(ValueConverters.TripPushHasReturnTripToInvertedBoolConverter);

            set.Bind(this.tripAcceptanceHeaderView).For(v => v.Hidden).To(vm => vm.Trip.Return).WithConversion(ValueConverters.TripPushHasReturnTripToBoolConverter);
            set.Bind(this.tripAcceptanceFooterView).For(v => v.Hidden).To(vm => vm.Trip.Return).WithConversion(ValueConverters.TripPushHasReturnTripToBoolConverter);

            set.Bind(this.viewReject.Tap()).For(g => g.Command).To(vm => vm.RejectTripCommand);

            set.Bind(this.animationView.Tap()).For(g => g.Command).To(vm => vm.AcceptTripCommand);
            set.Bind(this.internalView.Tap()).For(g => g.Command).To(vm => vm.AcceptTripCommand);

            set.Bind(this.actionOverlayView.Icon).For(MvxBindings.UIImageName).To(vm => vm.ResultAssetName);
            set.Bind(this.actionOverlayView.ActionTitle).To(vm => vm.ResultTitle);
            set.Bind(this.actionOverlayView.ActionDetail).To(vm => vm.ResultDetail);
            set.Bind(this.actionOverlayView.BtnTop).For(MvxBindings.Title).To(vm => vm.ResultTopButton);
            set.Bind(this.actionOverlayView.BtnBottom).For(MvxBindings.Title).To(vm => vm.ResultBottomButton);
            set.Bind(this.actionOverlayView.BtnBottom).For(MvxBindings.Visibility).To(vm => vm.ResultBottomButton).WithConversion(FwkValueConverters.StringNotEmptyToVisibilityConverter);

            set.Bind(this.actionOverlayView.BtnTop).To(vm => vm.TopButtonCommand);
            set.Bind(this.actionOverlayView.BtnBottom).To(vm => vm.BottomButtonCommand);

            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.AcceptTripTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.RejectTripTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.NavigationController.SetNavigationBarHidden(true, false);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            this.animationView.PlayWithCompletion(
                (animationFinished) =>
                {
                    if(!animationFinished)
                        return;

                    this.ViewModel.TimeExpiredCommand.Execute(null);
                    this.DisplayOverlayView();
                });
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.AcceptTripTaskCompletion) && this.ViewModel.AcceptTripTaskCompletion != null)
            {
                this.ViewModel.AcceptTripTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
                this.animationView.Pause();

                this.AcceptTripTaskCompletion_PropertyChanged(this, null);
            }

            if(e.PropertyName == nameof(this.ViewModel.RejectTripTaskCompletion) && this.ViewModel.RejectTripTaskCompletion != null)
            {
                this.ViewModel.RejectTripTaskCompletion.PropertyChanged += this.RejectTripTaskCompletion_PropertyChanged;
                this.animationView.Pause();

                this.RejectTripTaskCompletion_PropertyChanged(this, null);
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(this.ViewModel.AcceptTripTaskCompletion.IsSuccessfullyCompleted)
                this.DisplayOverlayView();

            if(this.ViewModel.AcceptTripTaskCompletion.IsFaulted)
                this.animationView.Play();
        }

        private void RejectTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(this.ViewModel.RejectTripTaskCompletion.IsSuccessfullyCompleted)
                this.DisplayOverlayView();

            if(this.ViewModel.RejectTripTaskCompletion.IsFaulted)
                this.animationView.Play();
        }

        private void DisplayOverlayView()
        {
            this.View.BringSubviewToFront(this.actionOverlayView);

            this.actionOverlayView.Hidden = false;
            UIView.AnimateNotify(
                .2f,
                () => this.actionOverlayView.Alpha = .90f,
                completion =>
                {
                    this.actionOverlayView.Icon.Hidden = false;
                    this.actionOverlayView.IconToTopConstraint.Constant = this.actionOverlayView.MarginTop;
                    UIView.AnimateNotify(
                    .5f,
                    () =>
                    {
                        this.actionOverlayView.Icon.Alpha = 1f;
                        this.actionOverlayView.LayoutIfNeeded();
                    },
                        completionHandler => { }
                    );
                });
        }
    }
}
