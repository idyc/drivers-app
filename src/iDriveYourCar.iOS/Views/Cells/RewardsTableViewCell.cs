﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class RewardsTableViewCell : IDYCTableViewCell
    {
        private const float MARGIN = Metrics.WINDOW_MARGIN * 2;
        private const float CARD_PADDING = 24f;
        private const float CONTROLS_DISTANCE = 8f;

        private CardView cardView;
        private UIImageView imgLogo;
        private IDYCMultilineLabel lblDescription;
        private IDYCLabel lblYourDiscount;
        private UserCodeView viewDiscount;

        public RewardsTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.Clear;

            this.cardView = new CardView();
            this.imgLogo = new UIImageView(UIImage.FromBundle("ic_menu"));
            this.lblDescription = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.LightGrayTextColor,
                TextAlignment = UITextAlignment.Center
            };
            this.lblYourDiscount = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor(),
                Text = IDriveYourCarStrings.YourDiscountCode,
                TextAlignment = UITextAlignment.Center
            };
            this.viewDiscount = new UserCodeView();


            this.ContentView.AddSubview(this.cardView);
            this.cardView.AddSubviews(this.imgLogo, this.lblDescription, this.lblYourDiscount, this.viewDiscount);
            this.ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.cardView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                this.cardView.AtLeftOf(this.ContentView, MARGIN),
                this.cardView.AtTopOf(this.ContentView, MARGIN),
                this.cardView.AtRightOf(this.ContentView, MARGIN),
                this.cardView.AtBottomOf(this.ContentView, MARGIN)
            );

            this.cardView.AddConstraints(
                this.imgLogo.AtTopOf(this.cardView, CARD_PADDING),
                this.imgLogo.WithSameCenterX(this.cardView),

                this.lblDescription.Below(this.imgLogo, CONTROLS_DISTANCE * 2),
                this.lblDescription.AtLeftOf(this.cardView, CARD_PADDING),
                this.lblDescription.AtRightOf(this.cardView, CARD_PADDING),

                this.lblYourDiscount.Below(this.lblDescription, CONTROLS_DISTANCE * 2),
                this.lblYourDiscount.AtLeftOf(this.cardView, CARD_PADDING),
                this.lblYourDiscount.AtRightOf(this.cardView, CARD_PADDING),

                this.viewDiscount.Below(this.lblYourDiscount, CONTROLS_DISTANCE),
                this.viewDiscount.WithSameCenterX(this.cardView),
                this.viewDiscount.AtBottomOf(this.cardView, CARD_PADDING)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RewardsTableViewCell, RewardItemViewModel>();
            set.Bind(this.lblDescription).To(vm => vm.Reward.Detail);
            set.Bind(this.viewDiscount.LblCode).To(vm => vm.Reward.Code);
            set.Apply();
        }

        public override void ConfigureReusedCell(Type itemType, object item)
        {
            base.ConfigureReusedCell(itemType, item);
        }
    }
}
