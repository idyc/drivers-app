﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class DateTripTableViewCell : BaseTableViewCell
    {
        private const float PADDING = 16f;
        private IDYCLabel lblDate;

        public DateTripTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.Clear;

            this.lblDate = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.ContentView.AggregateSubviews(this.lblDate);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                this.lblDate.AtLeftOf(this.ContentView, PADDING),
                this.lblDate.AtTopOf(this.ContentView, PADDING),
                this.lblDate.AtRightOf(this.ContentView, PADDING),
                this.lblDate.AtBottomOf(this.ContentView, PADDING)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DateTripTableViewCell, IDateTripItemViewModel>();
            set.Bind(this.lblDate).To(vm => vm.Date).WithConversion(FwkValueConverters.DateTimeToStringConverter, "DateWithDayAndMonthDescription");
            set.Apply();
        }
    }
}
