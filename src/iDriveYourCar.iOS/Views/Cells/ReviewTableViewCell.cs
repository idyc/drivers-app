﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using MvvmCross.Binding.BindingContext;
using iDriveYourCar.Core.ViewModels.Items;
using DGenix.Mobile.Fwk.iOS.Controls.TagsView;
using FFImageLoading;
using DGenix.Mobile.Fwk.iOS.Controls.RatingView;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.Core.Converters;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class ReviewTableViewCell : IDYCTableViewCell
    {
        private const float CONTROLS_MARGIN = 16f;
        private const float CARD_PADDING = 8f;
        private const float RATING_WIDTH = 90f;

        //private CardView cardView;

        private IDYCLabel lblDate;

        private RatingView viewRating;

        private UIView viewLine;

        private IDYCMultilineLabel lblComment;

        private BindableTagsView tagsView;

        public ReviewTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.Clear;

            //this.cardView = new CardView();
            this.lblDate = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.LightGrayTextColor
            };
            this.viewRating = new RatingView(new RatingConfig(UIImage.FromBundle(Assets.StarEmpty), UIImage.FromBundle(Assets.StarFilled), UIImage.FromBundle(Assets.StarFilled)))
            {
                UserInteractionEnabled = false
            };
            this.viewLine = new UIView
            {
                BackgroundColor = IDYCColors.LineColor
            };
            this.lblComment = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.tagsView = new BindableTagsView(false);
            this.tagsView.TagBackgroundColor = "#EA4D3B".ToUIColor();
            this.tagsView.TagTextColor = UIColor.White;
            this.tagsView.CornerRadius = 5f;
            this.tagsView.TextFont = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeNormal);
            this.tagsView.PaddingY = 4f;
            this.tagsView.PaddingX = 8f;

            //this.ContentView.AddSubview(this.cardView);
            //this.cardView.AddSubviews(this.lblDate, this.viewRating, this.viewLine, this.lblComment, this.tagsView);

            this.ContentView.AggregateSubviews(this.lblDate, this.viewRating, this.viewLine, this.lblComment, this.tagsView);

            //this.ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            //this.cardView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                //this.cardView.AtLeftOf(this.ContentView, Metrics.WINDOW_MARGIN),
                //this.cardView.AtTopOf(this.ContentView, Metrics.WINDOW_MARGIN),
                //this.cardView.AtRightOf(this.ContentView, Metrics.WINDOW_MARGIN),
                //this.cardView.AtBottomOf(this.ContentView, Metrics.WINDOW_MARGIN)

                this.viewLine.AtTopOf(this.ContentView, CONTROLS_MARGIN),
                this.viewLine.AtLeftOf(this.ContentView),
                this.viewLine.AtRightOf(this.ContentView),
                this.viewLine.Height().EqualTo(Metrics.LineThickness),

                this.lblDate.AtRightOf(this.ContentView, CARD_PADDING),
                this.lblDate.Below(this.viewLine, CONTROLS_MARGIN),

                this.viewRating.AtLeftOf(this.ContentView, CARD_PADDING),
                this.viewRating.WithSameHeight(this.lblDate).Plus(4f),
                this.viewRating.WithSameCenterY(this.lblDate),
                this.viewRating.Width().EqualTo(RATING_WIDTH),

                this.lblComment.Below(this.viewRating, CARD_PADDING),
                this.lblComment.AtLeftOf(this.ContentView, CARD_PADDING),
                this.lblComment.AtRightOf(this.ContentView, CARD_PADDING),

                this.tagsView.Below(this.lblComment, CARD_PADDING),
                this.tagsView.AtLeftOf(this.ContentView, CARD_PADDING),
                this.tagsView.AtRightOf(this.ContentView, CARD_PADDING),
                this.tagsView.AtBottomOf(this.ContentView, CARD_PADDING)
            );

            this.lblDate.SetContentHuggingPriority(502f, UILayoutConstraintAxis.Vertical);
            this.tagsView.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Vertical);
            this.lblComment.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Vertical);

            //this.cardView.AddConstraints(
            //	this.lblDate.AtRightOf(this.cardView, CARD_PADDING),
            //	this.lblDate.AtTopOf(this.cardView, CARD_PADDING),

            //	this.viewRating.AtLeftOf(this.cardView, CARD_PADDING),
            //	this.viewRating.WithSameHeight(this.lblDate).Plus(4f),
            //	this.viewRating.WithSameCenterY(this.lblDate),
            //	this.viewRating.Width().EqualTo(RATING_WIDTH),

            //	this.viewLine.Below(this.lblDate, CARD_PADDING),
            //	this.viewLine.AtLeftOf(this.cardView),
            //	this.viewLine.AtRightOf(this.cardView),
            //	this.viewLine.Height().EqualTo(Metrics.LineThickness),

            //	this.lblComment.Below(this.viewLine, CARD_PADDING),
            //	this.lblComment.AtLeftOf(this.cardView, CARD_PADDING),
            //	this.lblComment.AtRightOf(this.cardView, CARD_PADDING),

            //	this.tagsView.Below(this.lblComment, CARD_PADDING),
            //	this.tagsView.AtLeftOf(this.cardView, CARD_PADDING),
            //	this.tagsView.AtRightOf(this.cardView, CARD_PADDING),
            //	this.tagsView.AtBottomOf(this.cardView, CARD_PADDING)
            //);
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ReviewTableViewCell, ReviewItemViewModel>();
            set.Bind(this.lblDate).To(vm => vm.Review.Date).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");
            set.Bind(this.lblComment).To(vm => vm.Review.Comment);
            set.Bind(this.tagsView).For(v => v.ItemsSource).To(vm => vm.Tags);
            set.Bind(this.viewRating).For(v => v.AverageRating).To(vm => vm.Review.Rating);
            set.Apply();
        }
    }
}
