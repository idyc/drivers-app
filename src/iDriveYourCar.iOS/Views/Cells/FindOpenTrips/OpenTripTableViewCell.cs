﻿using System;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class OpenTripTableViewCell : BaseTripTableViewCell
    {
        public OpenTripTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();
        }
    }
}
