﻿using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using MvvmCross.Plugins.Color.iOS;
using UIKit;
using MvvmCross.Binding.BindingContext;
using iDriveYourCar.Core.ViewModels.Items.Message;
using Cirrious.FluentLayouts.Touch;
using System;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class MessageReceivedTableViewCell : BaseTableViewCell
    {
        private UIImageView imgUser;

        private UIView viewBackground;

        private IDYCMultilineLabel lblMessage;

        public MessageReceivedTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.viewBackground = new UIView
            {
                BackgroundColor = this.appConfiguration.Value.DarkPrimaryColor.ToNativeColor()
            };
            this.lblMessage = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = UIColor.White
            };

            this.ContentView.AggregateSubviews(this.viewBackground);
            this.viewBackground.AggregateSubviews(this.lblMessage);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                this.viewBackground.AtLeftOf(this.ContentView),
                this.viewBackground.AtTopOf(this.ContentView),
                this.viewBackground.AtRightOf(this.ContentView),
                this.viewBackground.AtBottomOf(this.ContentView)
            );

            this.viewBackground.AddConstraints(
                this.lblMessage.AtLeftOf(this.viewBackground),
                this.lblMessage.AtTopOf(this.viewBackground),
                this.lblMessage.AtRightOf(this.viewBackground),
                this.lblMessage.AtBottomOf(this.viewBackground)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<MessageReceivedTableViewCell, MessageReceivedItemViewModel>();
            set.Bind(this.lblMessage).To(vm => vm.ChatMessage.Body);
            set.Apply();
        }
    }
}
