﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class BaseTripTableViewCell : BaseTableViewCell
    {
        private const float ROW_HEIGHT = 72f;
        private const float PADDING = 12f;
        private const float CONTROLS_DISTANCE = 4f;

        protected CardView cardView;
        protected TZStackView.StackView accessoriesStack;
        protected OAStackView.OAStackView bottomStack;
        protected IDYCLabel lblTripType, lblPickupTime, lblPickup, lblArrow, lblDropoff;
        protected ReturnTripBadge rtBadge;
        protected UIImageView imgAlert;

        public BaseTripTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.SelectionStyle = UITableViewCellSelectionStyle.None;

            this.BackgroundColor = UIColor.Clear;

            this.cardView = new CardView();

            this.lblTripType = new IDYCLabel(Metrics.FontSizeNormalMinus, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };
            this.lblTripType.SetContentHuggingPriority(499, UILayoutConstraintAxis.Horizontal);
            this.lblPickupTime = new IDYCLabel(Metrics.FontSizeNormalMinus, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.bottomStack = new OAStackView.OAStackView
            {
                Axis = UILayoutConstraintAxis.Horizontal,
                Spacing = 4f
            };
            this.lblPickup = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.lblArrow = new IDYCLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.Arrow,
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.lblDropoff = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.accessoriesStack = new TZStackView.StackView
            {
                Axis = UILayoutConstraintAxis.Horizontal,
                Spacing = 1f
            };
            this.rtBadge = new ReturnTripBadge();
            this.imgAlert = new UIImageView(UIImage.FromBundle(Assets.WarningTrip));

            this.ContentView.AggregateSubviews(this.cardView);
            this.cardView.AggregateSubviews(this.lblTripType, this.accessoriesStack, this.lblPickupTime, this.bottomStack);

            this.accessoriesStack.AddArrangedSubview(this.rtBadge);
            this.accessoriesStack.AddArrangedSubview(this.imgAlert);

            this.bottomStack.AddArrangedSubview(this.lblPickup);
            this.bottomStack.AddArrangedSubview(this.lblArrow);
            this.bottomStack.AddArrangedSubview(this.lblDropoff);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                this.ContentView.Height().EqualTo(ROW_HEIGHT),

                this.cardView.AtLeftOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.cardView.AtTopOf(this.ContentView, Metrics.WINDOW_MARGIN / 2),
                this.cardView.AtRightOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.cardView.AtBottomOf(this.ContentView, Metrics.WINDOW_MARGIN / 2)
            );

            this.cardView.AddConstraints(
                this.lblTripType.AtLeftOf(this.cardView, PADDING),
                this.lblTripType.AtTopOf(this.cardView, PADDING),

                this.accessoriesStack.ToRightOf(this.lblTripType, CONTROLS_DISTANCE),
                this.accessoriesStack.WithSameTop(this.lblTripType),
                this.accessoriesStack.WithSameBottom(this.lblTripType),

                this.lblPickupTime.AtRightOf(this.cardView, PADDING),
                this.lblPickupTime.AtTopOf(this.cardView, PADDING),

                this.rtBadge.WithSameCenterY(this.lblTripType),
                this.imgAlert.WithSameCenterY(this.lblTripType),
                this.imgAlert.Width().EqualTo(15f),
                this.imgAlert.Height().EqualTo(15f),

                this.bottomStack.WithSameLeft(this.lblTripType),
                this.bottomStack.Right().LessThanOrEqualTo().RightOf(this.lblPickupTime),
                this.bottomStack.AtBottomOf(this.cardView, PADDING)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<BaseTripTableViewCell, BaseTripItemViewModel>();
            set.Bind(this.lblTripType).To(vm => vm.Trip.TripType).WithConversion(ValueConverters.TripTypeEnumToTripTypeStringConverter);
            set.Bind(this.rtBadge).For(v => v.Hidden).To(vm => vm.Trip.TripReturn).WithConversion(FwkValueConverters.NullToBoolConverter);
            set.Bind(this.lblPickupTime).To(vm => vm.PickupTime);
            set.Bind(this.lblPickup).To(vm => vm.Pickup);
            set.Bind(this.lblDropoff).To(vm => vm.Dropoff);
            set.Bind(this.imgAlert).For(v => v.Hidden).To(vm => vm.TripHasModifications).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter);
            set.Apply();
        }
    }
}
