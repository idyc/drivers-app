﻿//using System;
//using Cirrious.FluentLayouts.Touch;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
//using iDriveYourCar.Core.ViewModels.Items;
//using MvvmCross.Binding.BindingContext;
//using UIKit;

//namespace iDriveYourCar.iOS.Views.Cells
//{
//	public class DriverUnavailabilityWeeklyTimeTableViewCell : IDYCTableViewCell
//	{
//		private const float CONTROLS_SEPARATION = 8f;

//		private IDYCLabel lblTimeRange;
//		private UIButton btnDelete;

//		public DriverUnavailabilityWeeklyTimeTableViewCell(IntPtr handle) : base(handle)
//		{
//		}

//		protected override void CreateViews()
//		{
//			base.CreateViews();

//			this.lblTimeRange = new IDYCLabel(Metrics.FontSizeSmall, Metrics.RobotoMedium)
//			{
//				TextColor = IDYCColors.LightGrayTextColor
//			};

//			this.btnDelete = new UIButton();
//			this.btnDelete.SetImage(UIImage.FromBundle(Assets.Cloud), UIControlState.Normal);

//			this.AddSubviews(this.lblTimeRange, this.btnDelete);
//		}

//		protected override void CreateConstraints()
//		{
//			base.CreateConstraints();

//			this.AddConstraints(
//				this.lblTimeRange.AtLeftOf(this),
//				this.lblTimeRange.AtRightOf(this),
//				this.lblTimeRange.AtBottomOf(this),
//				this.lblTimeRange.AtTopOf(this),

//				this.btnDelete.ToRightOf(this.lblTimeRange, CONTROLS_SEPARATION),
//				this.btnDelete.AtRightOf(this),
//				this.btnDelete.AtBottomOf(this),
//				this.btnDelete.AtTopOf(this)
//			);
//		}

//		protected override void CreateMvxBindings()
//		{
//			base.CreateMvxBindings();

//			var set = this.CreateBindingSet<DriverUnavailabilityWeeklyTimeTableViewCell, DriverUnavailabilityWeeklyTimeItemViewModel>();
//			//set.Bind(this.lblTimeRange).To(vm => vm.TimeRange);
//			set.Bind(this.btnDelete).To(vm => vm.RemoveUnavailabilityTimeCommand);
//			set.Apply();
//		}
//	}
//}
