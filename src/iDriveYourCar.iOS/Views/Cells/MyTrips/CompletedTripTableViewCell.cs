﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using iDriveYourCar.Core.ViewModels.Items;
using DGenix.Mobile.Fwk.Core.Converters;
using UIKit;
using CoreAnimation;

namespace iDriveYourCar.iOS.Views.Cells
{
	public class CompletedTripTableViewCell : IDYCTableViewCell
	{
		private const float CARD_VIEW_PADDING = 12f;
		private const float CONTROLS_DISTANCE = 8f;

		private float bottomDistance;
		private UIView viewLine;
		private CardView cardView;
		private OAStackView.OAStackView stackView;
		private IDYCMultilineLabel lblDate, lblTime, lblPrice;

		private FluentLayout cardViewToBottomConstraint;

		public CompletedTripTableViewCell(IntPtr handle) : base(handle)
		{
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.BackgroundColor = UIColor.Clear;

			this.cardView = new SidesOnlyCardView();
			this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };
			this.stackView = new OAStackView.OAStackView()
			{
				Spacing = CONTROLS_DISTANCE,
				Distribution = OAStackView.OAStackViewDistribution.FillEqually
			};
			this.lblDate = new IDYCMultilineLabel(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.DarkGrayTextColor,
				TextAlignment = UITextAlignment.Left
			};
			this.lblTime = new IDYCMultilineLabel(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.DarkGrayTextColor,
				TextAlignment = UITextAlignment.Center
			};
			this.lblPrice = new IDYCMultilineLabel(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.DarkGrayTextColor,
				TextAlignment = UITextAlignment.Right
			};

			this.ContentView.AddSubview(this.cardView);
			this.cardView.AddSubviews(this.viewLine, this.stackView);
			this.stackView.AddArrangedSubview(this.lblDate);
			this.stackView.AddArrangedSubview(this.lblTime);
			this.stackView.AddArrangedSubview(this.lblPrice);
			this.ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
			this.cardView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
			this.stackView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.cardViewToBottomConstraint = this.cardView.AtBottomOf(this.ContentView, this.bottomDistance).WithIdentifier("cardViewToBottom");
			this.ContentView.AddConstraints(
				this.cardView.AtLeftOf(this.ContentView, Metrics.WINDOW_MARGIN),
				this.cardView.AtTopOf(this.ContentView),
				this.cardView.AtRightOf(this.ContentView, Metrics.WINDOW_MARGIN),
				this.cardViewToBottomConstraint
			);

			this.cardView.AddConstraints(
				this.viewLine.AtLeftOf(this.cardView),
				this.viewLine.AtTopOf(this.cardView),
				this.viewLine.AtRightOf(this.cardView),
				this.viewLine.Height().EqualTo(Metrics.LineThickness / 2),

				this.stackView.Below(this.viewLine),
				this.stackView.AtLeftOf(this.cardView, CARD_VIEW_PADDING),
				this.stackView.AtRightOf(this.cardView, CARD_VIEW_PADDING),
				this.stackView.AtBottomOf(this.cardView)
			);
		}

		protected override void CreateMvxBindings()
		{
			base.CreateMvxBindings();

			var set = this.CreateBindingSet<CompletedTripTableViewCell, CompletedTripsDayItemViewModel>();
			//set.Bind(this.lblDate).To(vm => vm.CompletedTrip.DropoffDateReal);
			//set.Bind(this.lblTime).To(vm => vm.CompletedTrip.DropoffTimeReal);
			//set.Bind(this.lblPrice).To(vm => vm.CompletedTrip.Price).WithConversion(FwkValueConverters.NumberToPriceConverter);
			set.Apply();
		}

		public void AdjustBottom(bool isLastCell)
		{
			this.bottomDistance = isLastCell ? -8f : 0f;
			if(this.cardViewToBottomConstraint != null)
				this.cardViewToBottomConstraint.Constant = this.bottomDistance;


			if(isLastCell)
			{
				this.SetNeedsLayout();
				this.LayoutIfNeeded();

				var maskPath = UIBezierPath.FromRoundedRect(this.cardView.Bounds, UIRectCorner.BottomLeft | UIRectCorner.BottomRight, new CoreGraphics.CGSize(3f, 3f));
				var maskLayer = new CAShapeLayer();
				maskLayer.Frame = this.cardView.Bounds;
				maskLayer.Path = maskPath.CGPath;
				this.cardView.Layer.Mask = maskLayer;
			}
			else
			{
				if(this.cardView.Layer != null)
					this.cardView.Layer.Mask = null;
			}
		}
	}
}
