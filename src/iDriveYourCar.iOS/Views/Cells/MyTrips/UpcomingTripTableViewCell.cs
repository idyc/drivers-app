﻿using System;

namespace iDriveYourCar.iOS.Views.Cells
{
	public class UpcomingTripTableViewCell : BaseTripTableViewCell
	{
		public UpcomingTripTableViewCell(IntPtr handle) : base(handle)
		{
		}

		protected override void CreateViews()
		{
			base.CreateViews();
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

		}

		protected override void CreateMvxBindings()
		{
			base.CreateMvxBindings();
		}
	}
}
