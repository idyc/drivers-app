﻿using System;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using iDriveYourCar.Core.ViewModels.Items;
using FFImageLoading;
using FFImageLoading.Transformations;
using iDriveYourCar.Core.Converters;

namespace iDriveYourCar.iOS.Views.Cells
{
    public class NotificationTableViewCell : BaseTableViewCell
    {
        private const float IMG_SIZE = 60f;
        private const float IMG_LABEL_TOP_PLUS_ALIGNMENT = 8f;

        private UIImageView imgUser;
        private IDYCMultilineLabel lblTitle;
        private IDYCLabel lblSubtitle, lblDate;

        public NotificationTableViewCell(IntPtr handle)
            : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.imgUser = new UIImageView();

            ImageService.Instance.LoadUrl("asddas")
                                .LoadingPlaceholder("ic_profile_picture.jpg", FFImageLoading.Work.ImageSource.CompiledResource)
                                .ErrorPlaceholder("ic_profile_picture.jpg", FFImageLoading.Work.ImageSource.CompiledResource)
                                .Transform(new CircleTransformation())
                                .TransformPlaceholders(true)
                                .DownSampleInDip(80, 80)
                                .Into(this.imgUser);

            this.lblTitle = new IDYCMultilineLabel(Metrics.FontSizeBig, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                LineBreakMode = UILineBreakMode.TailTruncation,
                Lines = 2
            };
            this.lblSubtitle = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                LineBreakMode = UILineBreakMode.TailTruncation
            };
            this.lblDate = new IDYCLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.LightGrayTextColor
            };

            this.ContentView.AggregateSubviews(
                this.imgUser,
                this.lblTitle,
                this.lblSubtitle,
                this.lblDate);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.lblDate.SetContentCompressionResistancePriority(751, UILayoutConstraintAxis.Horizontal);

            this.ContentView.AddConstraints(
                this.imgUser.AtLeftOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.imgUser.AtTopOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.imgUser.Width().EqualTo(IMG_SIZE),
                this.imgUser.Height().EqualTo().WidthOf(this.imgUser),

                this.lblTitle.ToRightOf(this.imgUser, Metrics.DEFAULT_DISTANCE),
                this.lblTitle.WithSameTop(this.imgUser).Plus(IMG_LABEL_TOP_PLUS_ALIGNMENT),
                this.lblTitle.Trailing().LessThanOrEqualTo().LeadingOf(this.lblDate).Minus(Metrics.DEFAULT_DISTANCE),

                this.lblDate.AtRightOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.lblDate.WithSameTop(this.imgUser).Plus(IMG_LABEL_TOP_PLUS_ALIGNMENT),

                this.lblSubtitle.Below(this.lblTitle),
                this.lblSubtitle.WithSameLeft(this.lblTitle),
                this.lblSubtitle.AtRightOf(this.ContentView, Metrics.WINDOW_MARGIN)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<NotificationTableViewCell, NotificationItemViewModel>();
            set.Bind(this.lblTitle).To(vm => vm.Notification.Title);
            set.Bind(this.lblSubtitle).To(vm => vm.Notification.Body);
            set.Bind(this.lblDate).To(vm => vm.Notification.Date).WithConversion(ValueConverters.DateToDateTimeOrTimeConverter);
            set.Apply();
        }
    }
}
