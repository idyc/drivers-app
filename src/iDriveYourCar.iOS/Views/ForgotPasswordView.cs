﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class ForgotPasswordView : IDYCController<ForgotPasswordViewModel>
    {
        private const float MARGIN = Metrics.WINDOW_MARGIN * 2;
        private const float CONTROLS_DISTANCE = 12f;

        private IDYCMultilineLabel lblEnterEmail;
        private BorderedIconTextField txtEmail;
        private PrimaryBackgroundButton btnSend;

        public ForgotPasswordView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.ForgotPassword;

            this.lblEnterEmail = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Justified,
                Text = IDriveYourCarStrings.PleaseEnterTheEmailAddressAssociatedWithYourIDriveYourCarAccount
            };
            this.txtEmail = new BorderedIconTextField(IDriveYourCarStrings.Email, Assets.Envelope);
            this.btnSend = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnSend.SetTitle(IDriveYourCarStrings.Submit.ToUpper(), UIControlState.Normal);

            this.View.AddSubviews(this.lblEnterEmail, this.txtEmail, this.btnSend);
            this.View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.lblEnterEmail.AtLeftOf(this.View, MARGIN),
                this.lblEnterEmail.AtTopOf(this.View, MARGIN * 2),
                this.lblEnterEmail.AtRightOf(this.View, MARGIN),

                this.txtEmail.Below(this.lblEnterEmail, CONTROLS_DISTANCE),
                this.txtEmail.AtLeftOf(this.View, MARGIN),
                this.txtEmail.AtRightOf(this.View, MARGIN),

                this.btnSend.Below(this.txtEmail, CONTROLS_DISTANCE),
                this.btnSend.AtLeftOf(this.View, MARGIN),
                this.btnSend.AtRightOf(this.View, MARGIN)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ForgotPasswordView, ForgotPasswordViewModel>();
            set.Bind(this.txtEmail.TextField).To(vm => vm.Email);
            set.Bind(this.btnSend).To(vm => vm.RecoverPasswordCommand);
            set.Apply();

            this.AddLoadingOverlayBinding(this, nameof(this.ViewModel.ResetPasswordTaskCompletion));
            this.AddNoInternetToastBinding(this.View, nameof(this.ViewModel.ResetPasswordTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
