﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;

namespace iDriveYourCar.iOS.Views
{
    public class DriverUnavailabilityWeeklyView : IDYCController<DriverUnavailabilityWeeklyViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer;

        private IDYCLabel lblSetTimesYouCannotDrive;
        private IDYCMultilineLabel lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable;
        private PrimaryTextButton btnAddUnavailability;
        private UIView viewLine;
        private SimpleBindableStackView<DriverUnavailabilityWeeklyItemView, DriverUnavailabilityWeeklyDayItemViewModel> driverUnavailabilityWeeklyStackView;

        public DriverUnavailabilityWeeklyView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);

            this.lblSetTimesYouCannotDrive = new IDYCLabel(Metrics.FontSizeGigant)
            {
                Text = IDriveYourCarStrings.SetTimesYouCannotDrive,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.YouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.btnAddUnavailability = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnAddUnavailability.SetTitle(IDriveYourCarStrings.AddUnavailability, UIControlState.Normal);

            this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.driverUnavailabilityWeeklyStackView = new SimpleBindableStackView<DriverUnavailabilityWeeklyItemView, DriverUnavailabilityWeeklyDayItemViewModel>(UILayoutConstraintAxis.Vertical, 0f);

            this.viewContainer.AggregateSubviews(
                this.lblSetTimesYouCannotDrive,
                this.lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable,
                this.btnAddUnavailability,
                this.viewLine,
                this.driverUnavailabilityWeeklyStackView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.lblSetTimesYouCannotDrive.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.lblSetTimesYouCannotDrive.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),
                this.lblSetTimesYouCannotDrive.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable.Below(this.lblSetTimesYouCannotDrive, CONTROLS_DISTANCE),
                this.lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnAddUnavailability.Below(this.lblYouWillNotReceiveAnyLeadsForTripsThatOccurDuringThePeriodYouSetAsUnavailable, CONTROLS_DISTANCE),
                this.btnAddUnavailability.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),

                this.viewLine.Below(this.btnAddUnavailability, CONTROLS_DISTANCE),
                this.viewLine.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.viewLine.AtRightOf(this.viewContainer),
                this.viewLine.Height().EqualTo(Metrics.LineThickness),

                this.driverUnavailabilityWeeklyStackView.Below(this.viewLine, CONTROLS_DISTANCE),
                this.driverUnavailabilityWeeklyStackView.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.driverUnavailabilityWeeklyStackView.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.driverUnavailabilityWeeklyStackView.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DriverUnavailabilityWeeklyView, DriverUnavailabilityWeeklyViewModel>();
            set.Bind(this.btnAddUnavailability).To(vm => vm.AddUnavailabilityCommand);
            set.Bind(this.driverUnavailabilityWeeklyStackView).For(v => v.ItemsSource).To(vm => vm.WeeklyUnavailabilities);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.UpdateUnavailabilityTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
