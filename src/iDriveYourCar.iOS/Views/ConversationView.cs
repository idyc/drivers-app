﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using Foundation;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.Sources;
using UIKit;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Controls.TagsView;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class ConversationView : IDYCController<ConversationViewModel>
    {
        private AutomaticDimensionTableView tblMessages;
        private MessagesTableSource source;

        private UITextField txtSend;
        private UIButton btnSend;

        private TagView tagvasd;

        public ConversationView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            // Perform any additional setup after loading the view, typically from a nib.
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, KeyboardWillShow);
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyboardDidShow);
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyboardWillHide);

            this.View.AddGestureRecognizer(new UITapGestureRecognizer(() => this.View.EndEditing(true)));

            this.tblMessages = new AutomaticDimensionTableView(70f);
            this.source = new MessagesTableSource(this.tblMessages);
            this.tblMessages.Source = source;


            this.txtSend = new UITextField();
            this.txtSend.BorderStyle = UITextBorderStyle.RoundedRect;

            this.btnSend = new UIButton();
            this.btnSend.SetTitle("Send", UIControlState.Normal);
            this.btnSend.SetTitleColor(UIColor.Blue, UIControlState.Normal);


            this.tagvasd = new TagView();
            this.tagvasd.BackgroundColor = UIColor.Red;
            this.tagvasd.Label.Text = "Holaaaaaa";

            this.View.AggregateSubviews(this.txtSend, this.tagvasd, this.btnSend, this.tblMessages);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.txtSend.AtLeftOf(this.View),
                this.txtSend.AtTopOf(this.View),
                this.txtSend.AtRightOf(this.View),

                this.tagvasd.Below(this.txtSend),
                this.tagvasd.AtLeftOf(this.View),

                this.btnSend.Below(this.tagvasd),
                this.btnSend.AtLeftOf(this.View),

                this.tblMessages.Below(this.btnSend),
                this.tblMessages.AtLeftOf(this.View),
                this.tblMessages.AtRightOf(this.View),
                this.tblMessages.AtBottomOf(this.View)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ConversationView, ConversationViewModel>();
            set.Bind(this.source).To(vm => vm.Messages);
            set.Bind(this.txtSend).To(vm => vm.NewText);
            set.Bind(this.btnSend).To(vm => vm.SendNewMessageCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.NavigationItem.Title = $"Logged in as {this.ViewModel.ChatIdentity}";
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        #region Keyboard Management
        private void KeyboardWillShow(NSNotification notification)
        {
            var keyboardHeight = ((NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameBeginUserInfoKey)).RectangleFValue.Height;
            UIView.Animate(0.1, () =>
                {
                    //this.messageTextFieldBottomConstraint.Constant = keyboardHeight + 8;
                    //this.sendButtonBottomConstraint.Constant = keyboardHeight + 8;
                    this.View.LayoutIfNeeded();
                });
        }

        private void KeyboardDidShow(NSNotification notification)
        {
            this.ScrollToBottomMessage();
        }

        private void KeyboardWillHide(NSNotification notification)
        {
            UIView.Animate(0.1, () =>
                {
                    //this.messageTextFieldBottomConstraint.Constant = 8;
                    //this.sendButtonBottomConstraint.Constant = 8;
                });
        }
        #endregion

        public void ScrollToBottomMessage()
        {
            if(this.ViewModel.Messages.Count == 0)
            {
                return;
            }

            var bottomIndexPath = NSIndexPath.FromRowSection(this.tblMessages.NumberOfRowsInSection(0) - 1, 0);
            this.tblMessages.ScrollToRow(bottomIndexPath, UITableViewScrollPosition.Bottom, true);
        }
    }
}
