﻿using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using Foundation;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.iOS.Views;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class MyTripsView : IDYCTabsMenuController<MyTripsViewModel>
    {
        private bool firstTime = true;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.MyTrips;

            this.View.BackgroundColor = IDYCColors.BackgroundColor;

            this.topTabBar.SetItems(new NSObject[]
            {
                new NSString(IDriveYourCarStrings.Upcoming.ToUpper()),
                new NSString(IDriveYourCarStrings.Completed.ToUpper())
            });

            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.UpcomingTrips) as UpcomingTripsView);
            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.CompletedTrips) as CompletedTripsView);

            this.pageViewController.SetViewControllers(new UIViewController[] { this.tabsViewControllers[0] }, UIPageViewControllerNavigationDirection.Forward, true, null);
            this.currentSelectedTab = 0;

            this.View.BringSubviewToFront(this.topTabBar);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if(this.firstTime)
                this.firstTime = false;
            else
                this.ViewModel.ForceRefreshCommand.Execute(null);
        }
    }
}
