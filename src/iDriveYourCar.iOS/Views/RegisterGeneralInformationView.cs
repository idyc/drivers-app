﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class RegisterGeneralInformationView : IDYCController<RegisterGeneralInformationViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;
        private const float ACCESSORY_SIZE = 30f;
        private const float TABLE_HEIGHT = 160f;

        private UIScrollView scrollView;

        private UIView viewContainer;

        private OAStackView.OAStackView accessoriesStack;
        private UIActivityIndicatorView activityIndicator;
        private UIButton btnClear;
        private UITableView tblResults;
        private SimpleTableSource<DescriptionTableViewCell, Prediction> source;

        private IDYCLabel lblCompleteYourProfileToContinue;
        private BorderedTextField txtFirstName, txtLastName, txtEmail, txtMobilePhone, txtDateOfBirth, txtAddress, txtOptionalInviteCode;
        private PrimaryBackgroundButton btnContinue;

        public override bool HandlesKeyboardNotifications() => true;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.GeneralInformation;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = IDYCColors.BackgroundColor;

            this.viewContainer = new UIView();
            this.View.AddGestureRecognizer(new UITapGestureRecognizer(() => this.View.EndEditing(true)) { CancelsTouchesInView = false });


            this.btnClear = new UIButton();
            this.btnClear.SetImage(UIImage.FromBundle(Assets.Close), UIControlState.Normal);
            this.activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);

            this.accessoriesStack = new OAStackView.OAStackView
            {
                Axis = UILayoutConstraintAxis.Horizontal,
                Distribution = OAStackView.OAStackViewDistribution.Fill
            };

            this.tblResults = new AutomaticDimensionTableView(44f)
            {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                Bounces = false
            };
            this.source = new SimpleTableSource<DescriptionTableViewCell, Prediction>(this.tblResults);
            this.source.DeselectAutomatically = true;
            this.tblResults.Source = this.source;

            this.lblCompleteYourProfileToContinue = new IDYCLabel(Metrics.FontSizeSmall)
            {
                Text = IDriveYourCarStrings.CompleteYourProfileToContinue,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.txtFirstName = new BorderedTextField(IDriveYourCarStrings.FirstName);
            this.txtLastName = new BorderedTextField(IDriveYourCarStrings.LastName);
            this.txtEmail = new BorderedTextField(IDriveYourCarStrings.Email);
            this.txtEmail.TextField.KeyboardType = UIKeyboardType.EmailAddress;
            this.txtMobilePhone = new BorderedTextField(IDriveYourCarStrings.MobilePhone);
            this.txtMobilePhone.TextField.KeyboardType = UIKeyboardType.PhonePad;

            this.txtDateOfBirth = new BorderedTextField(IDriveYourCarStrings.DateOfBirth) { UserInteractionEnabled = true };
            this.txtDateOfBirth.TextField.Enabled = false;

            this.txtAddress = new BorderedTextField(IDriveYourCarStrings.Address) { ClipsToBounds = true };

            this.txtOptionalInviteCode = new BorderedTextField(IDriveYourCarStrings.OptionalInviteCode);

            this.btnContinue = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnContinue.SetTitle(IDriveYourCarStrings.Continue.ToUpper(), UIControlState.Normal);

            this.View.AggregateSubviews(this.scrollView);

            this.accessoriesStack.AddArrangedSubview(this.txtAddress);
            this.accessoriesStack.AddArrangedSubview(this.activityIndicator);
            this.accessoriesStack.AddArrangedSubview(this.btnClear);

            this.scrollView.AggregateSubviews(this.viewContainer);

            this.viewContainer.AggregateSubviews(
                this.lblCompleteYourProfileToContinue,
                this.txtFirstName,
                this.txtLastName,
                this.txtEmail,
                this.txtMobilePhone,
                this.txtDateOfBirth,
                this.accessoriesStack,
                this.tblResults,
                this.txtOptionalInviteCode,
                this.btnContinue);

            this.viewContainer.BringSubviewToFront(this.tblResults);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.txtAddress.SetContentHuggingPriority(1000, UILayoutConstraintAxis.Horizontal);

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 2)
            );

            this.viewContainer.AddConstraints(
                this.lblCompleteYourProfileToContinue.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),
                this.lblCompleteYourProfileToContinue.AtLeftOf(this.viewContainer),
                this.lblCompleteYourProfileToContinue.AtRightOf(this.viewContainer),

                this.txtFirstName.Below(this.lblCompleteYourProfileToContinue, CONTROLS_DISTANCE),
                this.txtFirstName.AtLeftOf(this.viewContainer),

                this.txtLastName.AtRightOf(this.viewContainer),
                this.txtLastName.Below(this.lblCompleteYourProfileToContinue, CONTROLS_DISTANCE),
                this.txtLastName.ToRightOf(this.txtFirstName, CONTROLS_DISTANCE),
                this.txtLastName.WithSameWidth(this.txtFirstName),

                this.txtEmail.Below(this.txtFirstName, CONTROLS_DISTANCE),
                this.txtEmail.AtLeftOf(this.viewContainer),
                this.txtEmail.AtRightOf(this.viewContainer),

                this.txtMobilePhone.Below(this.txtEmail, CONTROLS_DISTANCE),
                this.txtMobilePhone.AtLeftOf(this.viewContainer),
                this.txtMobilePhone.AtRightOf(this.viewContainer),

                this.txtDateOfBirth.Below(this.txtMobilePhone, CONTROLS_DISTANCE),
                this.txtDateOfBirth.AtLeftOf(this.viewContainer),
                this.txtDateOfBirth.AtRightOf(this.viewContainer),

                this.accessoriesStack.Below(this.txtDateOfBirth, CONTROLS_DISTANCE),
                this.accessoriesStack.AtLeftOf(this.viewContainer),
                this.accessoriesStack.AtRightOf(this.viewContainer),
                this.accessoriesStack.Height().EqualTo(36f),

                this.btnClear.Height().EqualTo(ACCESSORY_SIZE),
                this.btnClear.Width().EqualTo(ACCESSORY_SIZE),

                this.activityIndicator.Height().EqualTo(ACCESSORY_SIZE),
                this.activityIndicator.Width().EqualTo(ACCESSORY_SIZE),

                this.tblResults.Below(this.accessoriesStack),
                this.tblResults.AtLeftOf(this.viewContainer),
                this.tblResults.AtRightOf(this.viewContainer),
                this.tblResults.Height().EqualTo(TABLE_HEIGHT),

                this.txtOptionalInviteCode.Below(this.accessoriesStack, CONTROLS_DISTANCE),
                this.txtOptionalInviteCode.AtLeftOf(this.viewContainer),
                this.txtOptionalInviteCode.AtRightOf(this.viewContainer),

                this.btnContinue.Below(this.txtOptionalInviteCode, CONTROLS_DISTANCE),
                this.btnContinue.AtLeftOf(this.viewContainer),
                this.btnContinue.AtRightOf(this.viewContainer),
                this.btnContinue.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterGeneralInformationView, RegisterGeneralInformationViewModel>();
            set.Bind(this.txtFirstName.TextField).To(vm => vm.RegisterDriver.FirstName);
            set.Bind(this.txtLastName.TextField).To(vm => vm.RegisterDriver.LastName);
            set.Bind(this.txtEmail.TextField).To(vm => vm.RegisterDriver.User.Email);

            set.Bind(this.txtMobilePhone.TextField).To(vm => vm.RegisterDriver.CellPhone);

            set.Bind(this.txtDateOfBirth.Tap()).For(g => g.Command).To(vm => vm.SetDateOfBirthCommand);
            set.Bind(this.txtDateOfBirth.TextField).To(vm => vm.RegisterDriver.Birthday).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");

            set.Bind(this.txtAddress.TextField).To(vm => vm.InputText);

            set.Bind(this.activityIndicator).For(v => v.Hidden).To(vm => vm.SearchPredictionsTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);
            set.Bind(this.activityIndicator).For(MvxBindings.ActivityIndicator).To(vm => vm.SearchPredictionsTaskCompletion.IsNotCompleted).WithFallback(false);
            set.Bind(this.btnClear).For(MvxBindings.Visibility).To(vm => vm.SelectedPrediction).WithConversion(FwkValueConverters.VisibilityConverter);
            set.Bind(this.btnClear).To(vm => vm.DismissPredictionCommand);

            set.Bind(this.source).For(v => v.ItemsSource).To(vm => vm.Predictions);
            set.Bind(this.source).For(v => v.SelectionChangedCommand).To(vm => vm.SelectPredictionCommand);

            set.Bind(this.tblResults).For(MvxBindings.Visibility).To(vm => vm.Predictions.Count).WithConversion(FwkValueConverters.VisibilityConverter);

            set.Bind(this.txtOptionalInviteCode.TextField).To(vm => vm.RegisterDriver.OptionalInviteCode);
            set.Bind(this.btnContinue).To(vm => vm.ContinueCommand);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SearchPredictionsTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.ContinueTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.ScrollToCenterOnKeyboardShown = this.scrollView;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
