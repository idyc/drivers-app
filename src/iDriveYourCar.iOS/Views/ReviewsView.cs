using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Controls.RatingView;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.Cells;
using iDriveYourCar.iOS.Views.CustomViews;
using iDriveYourCar.iOS.Views.Sources;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class ReviewsView : IDYCMenuController<ReviewsViewModel>
    {
        private const float CONTROLS_DISTANCE = 12f;
        private const float TEXT_SEPARATION = 4f;
        private const float RATING_WIDTH = 90f;
        private const float RATING_HEIGHT = 25f;
        private const float RATING_BAR_HEIGHT = 15f;

        private UITableView tblReviews;
        private UIView viewHeader, viewHeaderContainer;
        private ReviewsTableSource source;
        private IDYCLabel lblYourAverageRating, lblRating, lblOutOfFiveStars;
        private RatingView viewRating;
        private ReviewsProgressBarView rpbFiveStar, rpbFourStar, rpbThreeStar, rpbTwoStar, rpbOneStar;
        private OAStackView.OAStackView progressBarStackView;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.CustomerReviews;

            this.lblYourAverageRating = new IDYCLabel(Metrics.FontSizeBig) { Text = IDriveYourCarStrings.YourAverageRating, TextColor = IDYCColors.DarkGrayTextColor };

            this.viewRating = new RatingView(new RatingConfig(UIImage.FromBundle(Assets.StarEmpty), UIImage.FromBundle(Assets.StarFilled), UIImage.FromBundle(Assets.StarFilled)))
            {
                UserInteractionEnabled = false
            };

            this.lblRating = new IDYCLabel(Metrics.FontSizeSmall) { TextColor = IDYCColors.LightGrayTextColor };

            this.lblOutOfFiveStars = new IDYCLabel(Metrics.FontSizeSmall) { Text = IDriveYourCarStrings.OutOfFiveStars, TextColor = IDYCColors.LightGrayTextColor };

            this.rpbFiveStar = new ReviewsProgressBarView();
            this.rpbFiveStar.LabelStar.Text = IDriveYourCarStrings.FiveStar;

            this.rpbFourStar = new ReviewsProgressBarView();
            this.rpbFourStar.LabelStar.Text = IDriveYourCarStrings.FourStar;

            this.rpbThreeStar = new ReviewsProgressBarView();
            this.rpbThreeStar.LabelStar.Text = IDriveYourCarStrings.ThreeStar;

            this.rpbTwoStar = new ReviewsProgressBarView();
            this.rpbTwoStar.LabelStar.Text = IDriveYourCarStrings.TwoStar;

            this.rpbOneStar = new ReviewsProgressBarView();
            this.rpbOneStar.LabelStar.Text = IDriveYourCarStrings.OneStar;

            this.progressBarStackView = new OAStackView.OAStackView()
            {
                Axis = UILayoutConstraintAxis.Vertical,
                Spacing = CONTROLS_DISTANCE / 2
            };

            this.progressBarStackView.AggregateSubviews(this.rpbFiveStar, this.rpbFourStar, this.rpbThreeStar, this.rpbTwoStar, this.rpbOneStar);

            this.viewHeader = new UIView { ClipsToBounds = true };
            this.viewHeaderContainer = new UIView { ClipsToBounds = true };

            this.tblReviews = new AutomaticDimensionTableView(140f)
            {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                AllowsSelection = false,
                BackgroundColor = UIColor.Clear
            };

            this.tblReviews.TableHeaderView = this.viewHeader;

            this.source = new ReviewsTableSource(this.ViewModel, this.tblReviews);
            this.tblReviews.Source = this.source;

            this.View.AggregateSubviews(this.tblReviews);

            this.viewHeader.AggregateSubviews(this.viewHeaderContainer);

            this.viewHeaderContainer.AggregateSubviews(this.lblYourAverageRating, this.viewRating, this.lblRating, this.lblOutOfFiveStars, this.progressBarStackView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.tblReviews.AtLeftOf(this.View),
                this.tblReviews.AtTopOf(this.View),
                this.tblReviews.AtRightOf(this.View),
                this.tblReviews.AtBottomOf(this.View)
            );

            this.viewHeader.AddConstraints(
                this.viewHeaderContainer.AtLeftOf(this.viewHeader, Metrics.WINDOW_MARGIN),
                this.viewHeaderContainer.AtTopOf(this.viewHeader, Metrics.WINDOW_MARGIN * 1.5f),
                this.viewHeaderContainer.AtRightOf(this.viewHeader, Metrics.WINDOW_MARGIN),
                this.viewHeaderContainer.AtBottomOf(this.viewHeader)
            );

            this.viewHeaderContainer.AddConstraints(
                this.lblYourAverageRating.AtLeftOf(this.viewHeaderContainer),
                this.lblYourAverageRating.AtTopOf(this.viewHeaderContainer),

                this.viewRating.AtLeftOf(this.viewHeaderContainer),
                this.viewRating.Below(this.lblYourAverageRating, CONTROLS_DISTANCE),
                this.viewRating.Width().EqualTo(RATING_WIDTH),
                this.viewRating.Height().EqualTo(RATING_HEIGHT),

                this.lblRating.AtLeftOf(this.viewHeaderContainer, TEXT_SEPARATION),
                this.lblRating.Below(this.viewRating),

                this.lblOutOfFiveStars.ToRightOf(this.lblRating, TEXT_SEPARATION),
                this.lblOutOfFiveStars.WithSameCenterY(this.lblRating),

                this.progressBarStackView.AtLeftOf(this.viewHeaderContainer),
                this.progressBarStackView.Below(this.lblOutOfFiveStars, CONTROLS_DISTANCE * 2f),
                this.progressBarStackView.AtRightOf(this.viewHeaderContainer),
                this.progressBarStackView.AtBottomOf(this.viewHeaderContainer, CONTROLS_DISTANCE * 2f)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ReviewsView, ReviewsViewModel>();

            set.Bind(this.source).To(vm => vm.Items);

            set.Bind(this.viewRating).For(v => v.AverageRating).To(vm => vm.AverageRating);
            set.Bind(this.lblRating).To(vm => vm.AverageRating).WithConversion(ValueConverters.AverageRatingDecimalToAverageRatingStringConverter);

            set.Bind(this.rpbFiveStar.LabelStarQuantity).To(vm => vm.Driver.RateFiveStars);
            set.Bind(this.rpbFourStar.LabelStarQuantity).To(vm => vm.Driver.RateFourStars);
            set.Bind(this.rpbThreeStar.LabelStarQuantity).To(vm => vm.Driver.RateThreeStars);
            set.Bind(this.rpbTwoStar.LabelStarQuantity).To(vm => vm.Driver.RateTwoStars);
            set.Bind(this.rpbOneStar.LabelStarQuantity).To(vm => vm.Driver.RateOneStar);

            set.Bind(this.rpbFiveStar.ProgressStar).For(v => v.Progress).To(vm => vm.Driver.RateFiveStars).WithConversion(ValueConverters.RateIntToFloatPercentConverter, this.ViewModel.ReviewsCount);
            set.Bind(this.rpbFourStar.ProgressStar).For(v => v.Progress).To(vm => vm.Driver.RateFourStars).WithConversion(ValueConverters.RateIntToFloatPercentConverter, this.ViewModel.ReviewsCount);
            set.Bind(this.rpbThreeStar.ProgressStar).For(v => v.Progress).To(vm => vm.Driver.RateThreeStars).WithConversion(ValueConverters.RateIntToFloatPercentConverter, this.ViewModel.ReviewsCount);
            set.Bind(this.rpbTwoStar.ProgressStar).For(v => v.Progress).To(vm => vm.Driver.RateTwoStars).WithConversion(ValueConverters.RateIntToFloatPercentConverter, this.ViewModel.ReviewsCount);
            set.Bind(this.rpbOneStar.ProgressStar).For(v => v.Progress).To(vm => vm.Driver.RateOneStar).WithConversion(ValueConverters.RateIntToFloatPercentConverter, this.ViewModel.ReviewsCount);

            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            this.tblReviews.SetupDynamicHeightTableViewHeader();
        }
    }
}
