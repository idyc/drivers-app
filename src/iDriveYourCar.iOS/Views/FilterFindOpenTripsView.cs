﻿using System.Linq;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class FilterFindOpenTripsView : IDYCController<FilterFindOpenTripsViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;
        private const float DATE_RELATIVE_WIDTH = .42f;
        private const float TABLE_HEIGHT = 160f;

        private UIView viewContainer;

        private UIView viewDates;
        private BorderedIconTextField txtStart, txtEnd, txtFilterType;

        private BorderedTextField txtAddress;
        private UITableView tblResults;
        private SimpleTableSource<DescriptionTableViewCell, Prediction> source;

        private PrimaryBackgroundButton btnApply;

        private FluentLayout txtAddressHeightConstraint;

        public FilterFindOpenTripsView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.FilterTrips;


            this.viewContainer = new UIView();
            this.View.AddGestureRecognizer(new UITapGestureRecognizer(() => this.View.EndEditing(true)) { CancelsTouchesInView = false });

            this.viewDates = new UIView();
            this.txtStart = new BorderedIconTextField(IDriveYourCarStrings.StartDate, Assets.Calendar) { UserInteractionEnabled = true };
            this.txtStart.TextField.Enabled = false;

            this.txtEnd = new BorderedIconTextField(IDriveYourCarStrings.EndDate, Assets.Calendar) { UserInteractionEnabled = true };
            this.txtEnd.TextField.Enabled = false;

            this.txtFilterType = new BorderedIconTextField(IDriveYourCarStrings.DrivingTimeFromHome, Assets.ArrowDown) { UserInteractionEnabled = true };
            this.txtFilterType.TextField.Enabled = false;

            this.txtAddress = new BorderedTextField(IDriveYourCarStrings.StreetAddress)
            {
                ClipsToBounds = true
            };

            this.tblResults = new AutomaticDimensionTableView(44f)
            {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                Bounces = false
            };
            this.source = new SimpleTableSource<DescriptionTableViewCell, Prediction>(this.tblResults);
            this.source.DeselectAutomatically = true;
            this.tblResults.Source = this.source;

            this.btnApply = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnApply.SetTitle(IDriveYourCarStrings.Apply.ToUpper(), UIControlState.Normal);

            this.View.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(this.txtFilterType, this.txtAddress, this.tblResults, this.viewDates, this.btnApply);
            this.viewDates.AggregateSubviews(this.txtStart, this.txtEnd);

            this.viewContainer.BringSubviewToFront(this.tblResults);

            this.txtFilterType.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    if(!this.ViewModel.FilterTypes.Any())
                        return;

                    var filterTypes = this.ViewModel.FilterTypes.Select(p => p.Description).ToList();
                    var modalPicker = new ModalPickerViewController(
                        ModalPickerType.Custom,
                        string.Empty,
                        this,
                        Strings.Ok,
                        Strings.Cancel,
                        UIColor.White,
                        IDYCColors.DarkGrayTextColor,
                        UIColor.White);
                    modalPicker.PickerView.Model = new CustomPickerModel(filterTypes);

                    var selectedType = this.ViewModel.FilterTypes.First(c => c.Type == this.ViewModel.SelectedFilterType.Type);

                    modalPicker.PickerView.Select(filterTypes.IndexOf(selectedType.Description), 0, true);

                    modalPicker.OnModalPickerDismissed += (s, ea) =>
                    {
                        var index = modalPicker.PickerView.SelectedRowInComponent(0);
                        this.ViewModel.SelectedFilterType = this.ViewModel.FilterTypes.First(c => c.Description == filterTypes[(int)index]);
                    };

                    this.PresentViewController(modalPicker, true, null);
                }));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.txtAddressHeightConstraint = this.txtAddress.Height().EqualTo(0f).SetActive(true).WithIdentifier("txtAddressHeightConstraint");

            this.View.AddConstraints(
                this.viewContainer.AtLeftOf(this.View, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtTopOf(this.View, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtRightOf(this.View, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtBottomOf(this.View, Metrics.WINDOW_MARGIN * 2)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 4)
            );

            this.viewContainer.AddConstraints(
                this.txtFilterType.AtLeftOf(this.viewContainer),
                this.txtFilterType.AtTopOf(this.viewContainer),
                this.txtFilterType.AtRightOf(this.viewContainer),

                this.txtAddress.Below(this.txtFilterType, CONTROLS_DISTANCE / 2),
                this.txtAddress.WithSameLeft(this.txtFilterType),
                this.txtAddress.WithSameRight(this.txtFilterType),
                this.txtAddressHeightConstraint,

                this.tblResults.Below(this.txtAddress),
                this.tblResults.WithSameLeft(this.txtAddress),
                this.tblResults.WithSameRight(this.txtAddress),
                this.tblResults.Height().EqualTo(TABLE_HEIGHT),
                //this.tblResults.AtBottomOf(this.viewContainer),

                this.viewDates.Below(this.txtAddress, CONTROLS_DISTANCE / 2),
                this.viewDates.WithSameLeft(this.txtFilterType),
                this.viewDates.WithSameRight(this.txtFilterType),

                this.btnApply.Below(this.viewDates, CONTROLS_DISTANCE),
                this.btnApply.WithSameLeft(this.txtFilterType),
                this.btnApply.WithSameRight(this.txtFilterType)
            );

            this.viewDates.AddConstraints(
                this.txtStart.AtLeftOf(this.viewDates),
                this.txtStart.AtTopOf(this.viewDates),
                this.txtStart.AtBottomOf(this.viewDates),
                this.txtStart.WithRelativeWidth(this.viewDates, DATE_RELATIVE_WIDTH),

                this.txtEnd.AtRightOf(this.viewDates),
                this.txtEnd.WithSameTop(this.txtStart),
                this.txtEnd.WithSameBottom(this.txtStart),
                this.txtEnd.WithRelativeWidth(this.viewDates, DATE_RELATIVE_WIDTH)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<FilterFindOpenTripsView, FilterFindOpenTripsViewModel>();
            set.Bind(this.txtFilterType.TextField).To(vm => vm.SelectedFilterType.Description);

            set.Bind(this.txtAddressHeightConstraint).For(v => v.Active).To(vm => vm.SpecificAddressIsSelected).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter);
            set.Bind(this.txtAddress.TextField).To(vm => vm.InputText);
            set.Bind(this.source).For(v => v.ItemsSource).To(vm => vm.Predictions);
            set.Bind(this.source).For(v => v.SelectionChangedCommand).To(vm => vm.SelectPredictionCommand);

            set.Bind(this.tblResults).For(MvxBindings.Visibility).To(vm => vm.Predictions.Count).WithConversion(FwkValueConverters.VisibilityConverter);
            set.Bind(this.btnApply).For(MvxBindings.Visibility).To(vm => vm.Predictions.Count).WithConversion(FwkValueConverters.InvertedVisibilityConverter);
            set.Bind(this.viewDates).For(MvxBindings.Visibility).To(vm => vm.Predictions.Count).WithConversion(FwkValueConverters.InvertedVisibilityConverter);

            set.Bind(this.txtStart.TextField).For(v => v.Placeholder).To(vm => vm.Criteria.StartDate).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");
            set.Bind(this.txtStart.Tap()).For(g => g.Command).To(vm => vm.ChangeStartDateCommand);
            set.Bind(this.txtEnd.TextField).For(v => v.Placeholder).To(vm => vm.Criteria.EndDate).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");
            set.Bind(this.txtEnd.Tap()).For(g => g.Command).To(vm => vm.ChangeEndDateCommand);
            set.Bind(this.btnApply).To(vm => vm.SaveFilterTripsCommand);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SearchPredictionsTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SaveFilterTripsTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}