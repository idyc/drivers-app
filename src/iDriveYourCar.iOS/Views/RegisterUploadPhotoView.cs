﻿using System.IO;
using System.Threading.Tasks;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using FFImageLoading;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class RegisterUploadPhotoView : IDYCController<RegisterUploadPhotoViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer, viewheaderBackground;

        private UIImageView imgAvatar;

        private IDYCLabel lblProfessionalPicture;
        private IDYCMultilineLabel lblUploadAFriendlyPhotoInProfessionalDressAttire;
        private BulletLabel lblUploadHeadshotPhotosOnly, lblUseASolidLightColorBackdrop,
                    lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie, lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage;

        private PrimaryBackgroundButton btnUpload, btnContinue;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.UploadPhoto;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = IDYCColors.BackgroundColor;
            this.viewContainer = new UIView();

            this.viewheaderBackground = new UIView { BackgroundColor = "#E1E1E0".ToUIColor() };

            this.imgAvatar = new UIImageView(UIImage.FromBundle(Assets.RegisterPhoto))
            {
                ContentMode = UIViewContentMode.ScaleAspectFit
            };

            this.lblProfessionalPicture = new IDYCLabel(Metrics.FontSizeHuge)
            {
                Text = IDriveYourCarStrings.ProfessionalPicture,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblUploadAFriendlyPhotoInProfessionalDressAttire = new IDYCMultilineLabel(Metrics.FontSizeBig)
            {
                Text = IDriveYourCarStrings.UploadAFriendlyPhotoInProfessionalDressAttire,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.lblUploadHeadshotPhotosOnly = new BulletLabel(IDriveYourCarStrings.UploadHeadshotPhotosOnly, Metrics.FontSizeSmall);
            this.lblUseASolidLightColorBackdrop = new BulletLabel(IDriveYourCarStrings.UseASolidLightColorBackDrop, Metrics.FontSizeSmall);
            this.lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie = new BulletLabel(IDriveYourCarStrings.DressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie, Metrics.FontSizeSmall);
            this.lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage = new BulletLabel(IDriveYourCarStrings.MoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage, Metrics.FontSizeSmall);

            this.btnContinue = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnContinue.SetTitle(IDriveYourCarStrings.Continue.ToUpper(), UIControlState.Normal);

            this.btnUpload = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnUpload.SetTitle(IDriveYourCarStrings.Upload.ToUpper(), UIControlState.Normal);
            this.btnUpload.SetImage(UIImage.FromBundle(Assets.UploadWhite), UIControlState.Normal);
            this.btnUpload.TitleEdgeInsets = new UIEdgeInsets(0, 0, 0, this.btnUpload.ImageView.Frame.Size.Width);
            this.btnUpload.ImageEdgeInsets = new UIEdgeInsets(0, this.btnUpload.Frame.Size.Width - (this.btnUpload.ImageView.Frame.Size.Width + 15), 0, 0);

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.viewheaderBackground,
                this.lblProfessionalPicture,
                this.lblUploadAFriendlyPhotoInProfessionalDressAttire,
                this.lblUploadHeadshotPhotosOnly,
                this.lblUseASolidLightColorBackdrop,
                this.lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie,
                this.lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage,
                this.btnUpload,
                this.btnContinue);
            this.viewheaderBackground.AggregateSubviews(this.imgAvatar);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.viewheaderBackground.AtTopOf(this.viewContainer),
                this.viewheaderBackground.WithSameWidth(this.viewContainer),
                this.viewheaderBackground.Height().EqualTo(150f),

                this.lblProfessionalPicture.Below(this.viewheaderBackground, CONTROLS_DISTANCE),
                this.lblProfessionalPicture.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblProfessionalPicture.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblUploadAFriendlyPhotoInProfessionalDressAttire.Below(this.lblProfessionalPicture, CONTROLS_DISTANCE / 2),
                this.lblUploadAFriendlyPhotoInProfessionalDressAttire.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblUploadAFriendlyPhotoInProfessionalDressAttire.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblUploadHeadshotPhotosOnly.Below(this.lblUploadAFriendlyPhotoInProfessionalDressAttire, CONTROLS_DISTANCE),
                this.lblUploadHeadshotPhotosOnly.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblUploadHeadshotPhotosOnly.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblUseASolidLightColorBackdrop.Below(this.lblUploadHeadshotPhotosOnly, CONTROLS_DISTANCE / 2),
                this.lblUseASolidLightColorBackdrop.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblUseASolidLightColorBackdrop.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie.Below(this.lblUseASolidLightColorBackdrop, CONTROLS_DISTANCE / 2),
                this.lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage.Below(this.lblDressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie, CONTROLS_DISTANCE / 2),
                this.lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnUpload.Below(this.lblMoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage, CONTROLS_DISTANCE * 2),
                this.btnUpload.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnUpload.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnContinue.Below(this.btnUpload, CONTROLS_DISTANCE),
                this.btnContinue.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnContinue.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnContinue.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );

            this.viewheaderBackground.AddConstraints(
                this.imgAvatar.WithSameCenterX(this.viewheaderBackground),
                this.imgAvatar.WithSameCenterY(this.viewheaderBackground),

                this.imgAvatar.Left().GreaterThanOrEqualTo(Metrics.DEFAULT_DISTANCE).LeftOf(this.viewheaderBackground),
                this.imgAvatar.Top().GreaterThanOrEqualTo(Metrics.DEFAULT_DISTANCE).TopOf(this.viewheaderBackground),
                this.imgAvatar.Right().LessThanOrEqualTo(Metrics.DEFAULT_DISTANCE).RightOf(this.viewheaderBackground),
                this.imgAvatar.Bottom().LessThanOrEqualTo(Metrics.DEFAULT_DISTANCE).BottomOf(this.viewheaderBackground)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterUploadPhotoView, RegisterUploadPhotoViewModel>();
            set.Bind(this.btnContinue).To(vm => vm.ContinueToUploadDriverLicenseCommand);
            set.Bind(this.btnUpload).To(vm => vm.UploadPhotoCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.TryLoadStreamImage();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if (e.PropertyName == nameof(this.ViewModel.Avatar))
                this.TryLoadStreamImage();
        }

        private void TryLoadStreamImage()
        {
            if (this.ViewModel.Avatar == null || this.ViewModel.Avatar.File == null)
                return;

            ImageService.Instance.LoadStream(
                        async (System.Threading.CancellationToken arg) =>
                        {
                            Stream toReturn = new MemoryStream();
                            await this.ViewModel.Avatar.File.CopyToAsync(toReturn);
                            toReturn.Position = 0;
                            return toReturn;
                        }).Into(this.imgAvatar);
        }
    }
}
