﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using Foundation;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    public class ReferAClientView : IDYCController<ReferAClientViewModel>
    {
        private const float LEFT_MARGIN = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer;

        private UIView viewLineCurrentBalance, viewLineEarnedToDate;

        private ReferralTopLabelView topView;
        private ReferralSocialView socialView;
        private ReferralEmailView emailView;
        private ReferralManualInviteView manualView;
        private ReferralCurrentBalanceView currentBalanceView;
        private ReferralEarnedView earnedView;

        private EmailContactsSelectionView contactsSelectionView;
        private FluentLayout contactsToBottomConstraint, contactsHeightConstraint;

        public override bool HandlesKeyboardNotifications() => true;

        public ReferAClientView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.topView = new ReferralTopLabelView(IDriveYourCarStrings.EarnMoneyForEveryoneYourInvite);
            this.socialView = new ReferralSocialView();
            this.emailView = new ReferralEmailView();
            this.manualView = new ReferralManualInviteView();
            this.currentBalanceView = new ReferralCurrentBalanceView();
            this.earnedView = new ReferralEarnedView();
            this.viewLineCurrentBalance = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.viewLineEarnedToDate = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.contactsSelectionView = new EmailContactsSelectionView { ClipsToBounds = true };

            this.socialView.UserCode.BtnCopy.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    this.socialView.UserCode.LblCopy.TextColor = IDYCColors.DarkGrayTextColor;
                    this.socialView.UserCode.LblCopy.Text = IDriveYourCarStrings.Copied.ToUpper();
                    this.ViewModel.CopyCodeToClipboardCommand.Execute(null);
                }
            ));

            this.socialView.BtnFacebook.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    var title = FromObject("Share Invitation Code");
                    var text = FromObject($"Invitation Code: {this.ViewModel.Driver.User.ReferralCode} http://idriveyourcar.com");

                    var excludedActivities = new NSString[]
                    {
                        UIActivityType.PostToTwitter,
                        UIActivityType.AddToReadingList,
                        UIActivityType.AirDrop,
                        UIActivityType.AssignToContact,
                        UIActivityType.CopyToPasteboard,
                        UIActivityType.Mail,
                        UIActivityType.Message,
                        UIActivityType.OpenInIBooks,
                        UIActivityType.PostToFlickr,
                        UIActivityType.PostToTencentWeibo,
                        UIActivityType.PostToVimeo,
                        UIActivityType.PostToWeibo,
                        UIActivityType.Print,
                        UIActivityType.SaveToCameraRoll,
                        (NSString)"com.apple.reminders.RemindersEditorExtension",
                        (NSString)"com.apple.mobilenotes.SharingExtension",
                        (NSString)"com.apple.mobileslideshow.StreamShareService",
                        (NSString)"com.google.Gmail.ShareExtension"
                    };

                    var activityController = new UIActivityViewController(new NSObject[] { title, text }, null);
                    activityController.ExcludedActivityTypes = excludedActivities;
                    UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(activityController, true, null);
                }
            ));

            this.socialView.BtnTwitter.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    var title = FromObject("Share Invitation Code");
                    var text = FromObject($"Invitation Code: {this.ViewModel.Driver.User.ReferralCode} http://idriveyourcar.com");

                    var excludedActivities = new NSString[]
                    {
                        UIActivityType.PostToFacebook,
                        UIActivityType.AddToReadingList,
                        UIActivityType.AirDrop,
                        UIActivityType.AssignToContact,
                        UIActivityType.CopyToPasteboard,
                        UIActivityType.Mail,
                        UIActivityType.Message,
                        UIActivityType.OpenInIBooks,
                        UIActivityType.PostToFlickr,
                        UIActivityType.PostToTencentWeibo,
                        UIActivityType.PostToVimeo,
                        UIActivityType.PostToWeibo,
                        UIActivityType.Print,
                        UIActivityType.SaveToCameraRoll,
                        (NSString)"com.apple.reminders.RemindersEditorExtension",
                        (NSString)"com.apple.mobilenotes.SharingExtension",
                        (NSString)"com.apple.mobileslideshow.StreamShareService",
                        (NSString)"com.google.Gmail.ShareExtension"
                    };

                    var activityController = new UIActivityViewController(new NSObject[] { title, text }, null);
                    activityController.ExcludedActivityTypes = excludedActivities;

                    UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(activityController, true, null);
                }
            ));

            this.View.AggregateSubviews(this.scrollView, this.contactsSelectionView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.topView,
                this.socialView,
                this.emailView,
                this.manualView,
                this.viewLineCurrentBalance,
                this.currentBalanceView,
                this.viewLineEarnedToDate,
                this.earnedView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.contactsToBottomConstraint = this.contactsSelectionView.AtBottomOf(this.View).SetActive(false).WithIdentifier("contactsToBottomConstraint");
            this.contactsHeightConstraint = this.contactsSelectionView.Height().EqualTo(0f).SetActive(true);

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View),

                this.contactsSelectionView.AtLeftOf(this.View),
                this.contactsSelectionView.AtTopOf(this.View),
                this.contactsSelectionView.AtRightOf(this.View),
                this.contactsToBottomConstraint,
                this.contactsHeightConstraint
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.topView.AtLeftOf(this.viewContainer, LEFT_MARGIN),
                this.topView.AtTopOf(this.viewContainer),
                this.topView.AtRightOf(this.viewContainer),

                this.socialView.Below(this.topView),
                this.socialView.WithSameLeft(this.topView),
                this.socialView.WithSameRight(this.topView),

                this.emailView.Below(this.socialView),
                this.emailView.WithSameLeft(this.topView),
                this.emailView.WithSameRight(this.topView),

                this.manualView.Below(this.emailView),
                this.manualView.WithSameLeft(this.topView),
                this.manualView.WithSameRight(this.topView),

                this.viewLineCurrentBalance.Below(this.manualView),
                this.viewLineCurrentBalance.AtLeftOf(this.viewContainer),
                this.viewLineCurrentBalance.AtRightOf(this.viewContainer),
                this.viewLineCurrentBalance.Height().EqualTo(Metrics.LineThickness),

                this.currentBalanceView.Below(this.viewLineCurrentBalance),
                this.currentBalanceView.WithSameLeft(this.topView),
                this.currentBalanceView.WithSameRight(this.topView),

                this.viewLineEarnedToDate.Below(this.currentBalanceView),
                this.viewLineEarnedToDate.AtLeftOf(this.viewContainer),
                this.viewLineEarnedToDate.AtRightOf(this.viewContainer),
                this.viewLineEarnedToDate.Height().EqualTo(Metrics.LineThickness),

                this.earnedView.Below(this.viewLineEarnedToDate),
                this.earnedView.WithSameLeft(this.topView),
                this.earnedView.WithSameRight(this.topView),
                this.earnedView.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ReferAClientView, ReferAClientViewModel>();
            set.Bind(this.socialView.UserCode.LblCode).To(vm => vm.Driver.User.ReferralCode);
            set.Bind(this.emailView.BtnLearnMore).To(vm => vm.DisplaySyncEmailExplanationCommand);
            set.Bind(this.currentBalanceView.LblCurrentBalanceValue).To(vm => vm.Driver.BalanceOfReferredClients).WithConversion(FwkValueConverters.NumberToPriceConverter);
            set.Bind(this.currentBalanceView.BtnRedeemBalance).For(w => w.Enabled).To(vm => vm.Driver.BalanceOfReferredClients).WithConversion(ValueConverters.BalanceOfReferredClientsToBoolConverter);
            set.Bind(this.earnedView.LblEarnedToDateValue).To(vm => vm.EarnedToDate).WithConversion(FwkValueConverters.NumberToPriceConverter);
            set.Bind(this.manualView.TxtEmails.TextView).To(vm => vm.ManualEmails);
            set.Bind(this.manualView.TxtEmails.Placeholder).For(MvxBindings.Visibility).To(vm => vm.ManualEmails).WithConversion(FwkValueConverters.InvertedVisibilityConverter);
            set.Bind(this.manualView.LblPotentialRewardValue).To(vm => vm.PotentialEarning).WithConversion(FwkValueConverters.NumberToPriceConverter);
            set.Bind(this.manualView.BtnInviteFriends).For(w => w.Enabled).To(vm => vm.ManualEmails).WithConversion(ValueConverters.ManualEmailsToBoolConverter);
            set.Bind(this.manualView.LblInviteExplanation).To(vm => vm.EarnMoneyForEachFriendYouInviteWithNoLimitOnHowManyFriendsYouCanRefer);

            set.Bind(this.contactsSelectionView.Source).For(v => v.ItemsSource).To(vm => vm.ContactsWithEmail);
            set.Bind(this.contactsSelectionView.BtnSelectAll).To(vm => vm.SelectAllContactsWithEmailCommand);

            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.InviteEmailFriendsTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.InviteFriendsManuallyTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.RedeemBalanceTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadDeviceContactsTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.ScrollToCenterOnKeyboardShown = this.scrollView;
            this.ViewToCenterOnKeyboardShown = this.manualView.TxtEmails.TextView;

            this.emailView.BtnInviteFriends.TouchUpInside += this.EmailInviteFriends_ClickHandler;
            this.manualView.BtnInviteFriends.TouchUpInside += this.ManualInviteFriends_ClickHandler;
            this.currentBalanceView.BtnRedeemBalance.TouchUpInside += this.RedeemCurrentBalance_ClickHandler;
            this.contactsSelectionView.BtnSendInvites.TouchUpInside += this.ContactsSelectionSendInvites_TouchUpInside;
            this.contactsSelectionView.BtnClose.TouchUpInside += this.ContactsSelectionBtnClose_TouchUpInside;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();

            this.emailView.BtnInviteFriends.TouchUpInside -= this.EmailInviteFriends_ClickHandler;
            this.manualView.BtnInviteFriends.TouchUpInside -= this.ManualInviteFriends_ClickHandler;
            this.currentBalanceView.BtnRedeemBalance.TouchUpInside -= this.RedeemCurrentBalance_ClickHandler;
            this.contactsSelectionView.BtnSendInvites.TouchUpInside -= this.ContactsSelectionSendInvites_TouchUpInside;
            this.contactsSelectionView.BtnClose.TouchUpInside -= this.ContactsSelectionBtnClose_TouchUpInside;
        }

        private void ManualInviteFriends_ClickHandler(object sender, EventArgs e)
        {
            this.manualView.TxtEmails.TextView.ResignFirstResponder();
            this.ViewModel.InviteFriendsManuallyCommand.Execute(null);
        }

        private void RedeemCurrentBalance_ClickHandler(object sender, EventArgs e)
        {
            this.ViewModel.RedeemBalanceCommand.Execute(null);
        }

        private void EmailInviteFriends_ClickHandler(object sender, System.EventArgs e)
        {
            this.ViewModel.LoadDeviceContactsCommand.Execute(null);

            this.ToggleContactsVisibility();
        }

        private void ToggleContactsVisibility()
        {
            this.contactsToBottomConstraint.Active = !this.contactsToBottomConstraint.Active;
            this.contactsHeightConstraint.Active = !this.contactsHeightConstraint.Active;

            UIView.Animate(
                0.2,
                0.2,
                UIViewAnimationOptions.CurveEaseIn,
                () => this.View.LayoutIfNeeded(),
                () => { });
        }

        private void ContactsSelectionBtnClose_TouchUpInside(object sender, System.EventArgs e)
        {
            this.ViewModel.ClearContactsSelectionCommand.Execute(null);
            this.ToggleContactsVisibility();
        }

        private void ContactsSelectionSendInvites_TouchUpInside(object sender, System.EventArgs e)
        {
            this.ViewModel.InviteEmailFriendsCommand.Execute(null);
            this.ToggleContactsVisibility();
        }
    }
}
