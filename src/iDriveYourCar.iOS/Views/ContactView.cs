﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class ContactView : IDYCMenuController<ContactViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;
        private const float PADDING = 24f;
        private const float CONTACT_WAYS_PROPORTIONAL_WIDTH = .32f;

        private UIScrollView scrollView;
        private UIView viewContainer;

        private CardView cardContact;
        private IDYCLabel lblContactUs, lblContactUsDetail;
        private UIView viewContactWays, viewPhone, viewEmail, viewChat;
        private IDYCMultilineLabel lblPhone, lblEmail, lblChat;
        private UIImageView imgPhone, imgEmail, imgChat;

        private CardView cardForm;
        private UILabel lblSendUsAMessage;
        private BorderedTextField txtSubject;
        private BorderedTextView txtMessage;
        private PrimaryBackgroundButton btnSend;

        public ContactView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView();
            this.viewContainer = new UIView();

            this.cardContact = new CardView();
            this.lblContactUs = new IDYCLabel(Metrics.FontSizeTitanic)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center,
                Text = IDriveYourCarStrings.ContactUs
            };
            this.lblContactUsDetail = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.LightGrayTextColor,
                TextAlignment = UITextAlignment.Center,
                Text = IDriveYourCarStrings.QuestionsWeWouldLoveToHearFromYou
            };
            this.viewContactWays = new UIView();
            this.viewPhone = new UIView() { BackgroundColor = UIColor.Red };
            this.viewEmail = new UIView() { BackgroundColor = UIColor.Blue };
            this.viewChat = new UIView() { BackgroundColor = UIColor.Green };
            this.imgPhone = new UIImageView(UIImage.FromBundle("ic_menu"));
            this.lblPhone = new IDYCMultilineLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center,
                Text = IDriveYourCarStrings.ContactPhone
            };
            this.imgEmail = new UIImageView(UIImage.FromBundle("ic_menu"));
            this.lblEmail = new IDYCMultilineLabel(Metrics.FontSizeBig)
            {
                Font = this.fonts.Value.FontBig,
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center,
                Text = IDriveYourCarStrings.EmailUs
            };
            this.imgChat = new UIImageView(UIImage.FromBundle("ic_menu"));
            this.lblChat = new IDYCMultilineLabel(Metrics.FontSizeBig)
            {
                Font = this.fonts.Value.FontBig,
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center,
                Text = IDriveYourCarStrings.ChatWithUs
            };

            this.cardForm = new CardView();
            this.lblSendUsAMessage = new UILabel
            {
                Font = this.fonts.Value.FontTitanicSemibold,
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Center,
                Text = IDriveYourCarStrings.SendUsAMessage
            };
            this.txtSubject = new BorderedTextField(IDriveYourCarStrings.Subject);
            this.txtMessage = new BorderedTextView(IDriveYourCarStrings.Message);
            this.btnSend = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnSend.SetTitle(IDriveYourCarStrings.Send.ToUpper(), UIControlState.Normal);

            this.View.AddSubview(this.scrollView);
            this.scrollView.AddSubview(this.viewContainer);
            this.viewContainer.AddSubviews(this.cardContact, this.cardForm);
            this.cardContact.AddSubviews(this.lblContactUs, this.lblContactUsDetail, this.viewContactWays);
            this.viewContactWays.AddSubviews(this.viewPhone, this.viewEmail, this.viewChat);
            this.viewPhone.AddSubviews(this.imgPhone, this.lblPhone);
            this.viewEmail.AddSubviews(this.imgEmail, this.lblEmail);
            this.viewChat.AddSubviews(this.imgChat, this.lblChat);
            this.cardForm.AddSubviews(this.lblSendUsAMessage, this.txtSubject, this.txtMessage, this.btnSend);

            this.View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.scrollView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.viewContainer.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.cardContact.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.viewContactWays.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.viewPhone.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.viewEmail.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.viewChat.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.cardForm.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.cardContact.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.cardContact.AtTopOf(this.viewContainer, Metrics.WINDOW_MARGIN * 2),
                this.cardContact.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.cardForm.Below(this.cardContact, CONTROLS_DISTANCE * 2),
                this.cardForm.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.cardForm.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.cardForm.AtBottomOf(this.viewContainer, Metrics.WINDOW_MARGIN)
            );

            #region card contact constraints
            this.cardContact.AddConstraints(
                this.lblContactUs.AtLeftOf(this.cardContact),
                this.lblContactUs.AtTopOf(this.cardContact, PADDING),
                this.lblContactUs.AtRightOf(this.cardContact),

                this.lblContactUsDetail.Below(this.lblContactUs, CONTROLS_DISTANCE),
                this.lblContactUsDetail.AtLeftOf(this.cardContact),
                this.lblContactUsDetail.AtRightOf(this.cardContact),

                this.viewContactWays.Below(this.lblContactUsDetail, CONTROLS_DISTANCE),
                this.viewContactWays.AtLeftOf(this.cardContact, PADDING),
                this.viewContactWays.AtRightOf(this.cardContact, PADDING),
                this.viewContactWays.AtBottomOf(this.cardContact, PADDING)
            );

            this.viewContactWays.AddConstraints(
                this.viewPhone.AtLeftOf(this.viewContactWays),
                this.viewPhone.AtTopOf(this.viewContactWays),
                this.viewPhone.WithRelativeWidth(this.viewContactWays, CONTACT_WAYS_PROPORTIONAL_WIDTH),
                this.viewPhone.AtBottomOf(this.viewContactWays),

                this.viewEmail.WithSameCenterX(this.viewContactWays),
                this.viewEmail.AtTopOf(this.viewContactWays),
                this.viewEmail.WithRelativeWidth(this.viewContactWays, CONTACT_WAYS_PROPORTIONAL_WIDTH),
                this.viewEmail.AtBottomOf(this.viewContactWays),

                this.viewChat.AtTopOf(this.viewContactWays),
                this.viewChat.AtRightOf(this.viewContactWays),
                this.viewChat.WithRelativeWidth(this.viewContactWays, CONTACT_WAYS_PROPORTIONAL_WIDTH),
                this.viewChat.AtBottomOf(this.viewContactWays)
            );

            this.imgPhone.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Vertical);
            this.lblPhone.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Vertical);
            this.viewPhone.AddConstraints(
                this.imgPhone.WithSameCenterX(this.viewPhone),
                this.imgPhone.AtTopOf(this.viewPhone),

                this.lblPhone.Below(this.imgPhone, CONTROLS_DISTANCE),
                this.lblPhone.AtLeftOf(this.viewPhone),
                this.lblPhone.AtRightOf(this.viewPhone),
                this.lblPhone.AtBottomOf(this.viewPhone)
            );

            this.imgEmail.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Vertical);
            this.lblEmail.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Vertical);
            this.viewEmail.AddConstraints(
                this.imgEmail.WithSameCenterX(this.viewEmail),
                this.imgEmail.AtTopOf(this.viewEmail),

                this.lblEmail.Below(this.imgEmail, CONTROLS_DISTANCE),
                this.lblEmail.AtLeftOf(this.viewEmail),
                this.lblEmail.AtRightOf(this.viewEmail),
                this.lblEmail.AtBottomOf(this.viewEmail)
            );

            this.imgChat.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Vertical);
            this.lblChat.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Vertical);
            this.viewChat.AddConstraints(
                this.imgChat.WithSameCenterX(this.viewChat),
                this.imgChat.AtTopOf(this.viewChat),

                this.lblChat.Below(this.imgChat, CONTROLS_DISTANCE),
                this.lblChat.AtLeftOf(this.viewChat),
                this.lblChat.AtRightOf(this.viewChat),
                this.lblChat.AtBottomOf(this.viewChat)
            );
            #endregion

            this.cardForm.AddConstraints(
                this.lblSendUsAMessage.AtLeftOf(this.cardForm),
                this.lblSendUsAMessage.AtTopOf(this.cardForm, PADDING),
                this.lblSendUsAMessage.AtRightOf(this.cardForm),

                this.txtSubject.Below(this.lblSendUsAMessage, CONTROLS_DISTANCE),
                this.txtSubject.AtLeftOf(this.cardForm, PADDING),
                this.txtSubject.AtRightOf(this.cardForm, PADDING),

                this.txtMessage.Below(this.txtSubject, CONTROLS_DISTANCE),
                this.txtMessage.AtLeftOf(this.cardForm, PADDING),
                this.txtMessage.AtRightOf(this.cardForm, PADDING),

                this.btnSend.Below(this.txtMessage, CONTROLS_DISTANCE),
                this.btnSend.AtLeftOf(this.cardForm, PADDING),
                this.btnSend.AtRightOf(this.cardForm, PADDING),
                this.btnSend.AtBottomOf(this.cardForm, PADDING)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
