﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class RegisterCompleteTheQuestionsView : IDYCController<RegisterCompleteTheQuestionsViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer;

        private IDYCMultilineLabel lblHaveYouEverServedAsProfessionalExplanation;
        private IDYCMultilineLabel lblHowDoCreateAnAmazingExperienceForYourClientsExplanation;

        private BorderedTextView txtHaveYouEverServedAsProfessionalExplanation;
        private BorderedTextView txtHowDoCreateAnAmazingExperienceForYourClientsExplanation;

        private PrimaryBackgroundButton btnContinue;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.CompleteTheQuestions;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = "#EEEEEE".ToUIColor();
            this.viewContainer = new UIView();

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);

            this.lblHaveYouEverServedAsProfessionalExplanation = new IDYCMultilineLabel(Metrics.FontSizeSmall)
            {
                Text = IDriveYourCarStrings.HaveYouEverServedAsAProfessionalDriverIfYesPleaseExplain,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblHowDoCreateAnAmazingExperienceForYourClientsExplanation = new IDYCMultilineLabel(Metrics.FontSizeSmall)
            {
                Text = IDriveYourCarStrings.HowDoCreateAnAmazingExperienceForYourClients,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.txtHaveYouEverServedAsProfessionalExplanation = new BorderedTextView();
            this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation = new BorderedTextView();

            this.btnContinue = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnContinue.SetTitle(IDriveYourCarStrings.Continue.ToUpper(), UIControlState.Normal);

            this.viewContainer.AggregateSubviews(
                this.lblHaveYouEverServedAsProfessionalExplanation,
                this.txtHaveYouEverServedAsProfessionalExplanation,
                this.lblHowDoCreateAnAmazingExperienceForYourClientsExplanation,
                this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation,
                this.btnContinue);

            this.View.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.View.EndEditing(true)));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 4)
            );

            this.viewContainer.AddConstraints(
                this.lblHaveYouEverServedAsProfessionalExplanation.AtLeftOf(this.viewContainer),
                this.lblHaveYouEverServedAsProfessionalExplanation.AtRightOf(this.viewContainer),
                this.lblHaveYouEverServedAsProfessionalExplanation.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),

                this.txtHaveYouEverServedAsProfessionalExplanation.AtLeftOf(this.viewContainer),
                this.txtHaveYouEverServedAsProfessionalExplanation.AtRightOf(this.viewContainer),
                this.txtHaveYouEverServedAsProfessionalExplanation.Below(this.lblHaveYouEverServedAsProfessionalExplanation, CONTROLS_DISTANCE),

                this.lblHowDoCreateAnAmazingExperienceForYourClientsExplanation.AtLeftOf(this.viewContainer),
                this.lblHowDoCreateAnAmazingExperienceForYourClientsExplanation.AtRightOf(this.viewContainer),
                this.lblHowDoCreateAnAmazingExperienceForYourClientsExplanation.Below(this.txtHaveYouEverServedAsProfessionalExplanation, CONTROLS_DISTANCE),

                this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation.AtLeftOf(this.viewContainer),
                this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation.AtRightOf(this.viewContainer),
                this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation.Below(this.lblHowDoCreateAnAmazingExperienceForYourClientsExplanation, CONTROLS_DISTANCE),

                this.btnContinue.AtLeftOf(this.viewContainer),
                this.btnContinue.AtRightOf(this.viewContainer),
                this.btnContinue.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE),
                this.btnContinue.Below(this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation, CONTROLS_DISTANCE)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterCompleteTheQuestionsView, RegisterCompleteTheQuestionsViewModel>();
            set.Bind(this.txtHaveYouEverServedAsProfessionalExplanation.TextView.Text).To(vm => vm.RegisterDriver.HaveYouEverServedAsProfessionalExplanation);
            set.Bind(this.txtHowDoCreateAnAmazingExperienceForYourClientsExplanation.TextView.Text).To(vm => vm.RegisterDriver.HowDoCreateAnAmazingExperienceForYourClientsExplanation);
            set.Bind(this.btnContinue).To(vm => vm.ContinueToUploadPhotoCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}

