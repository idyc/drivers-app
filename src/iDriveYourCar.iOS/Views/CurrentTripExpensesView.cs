﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class CurrentTripExpensesView : IDYCCurrentTripController<CurrentTripExpensesViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;
        private UIView viewContainer;

        private SimpleBindableStackView<CurrentTripExpensesItemView, CurrentTripExpenseItemViewModel> currentTripExpensesStackView;

        private UIView viewLine;
        private PrimaryTextButton btnAddExpense;
        private TripSwipeableButton btnWorkflow;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Expenses;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.btnAddExpense = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnAddExpense.SetTitle(IDriveYourCarStrings.AddExpense.ToUpper(), UIControlState.Normal);

            this.btnWorkflow = new TripSwipeableButton
            {
                BackgroundColor = IDYCColors.SwipedButtonColor
            };
            this.btnWorkflow.ImgSwipe.Image = UIImage.FromBundle(Assets.ArrowRightWhite);
            this.btnWorkflow.ImgSwiped.Image = UIImage.FromBundle(Assets.CheckWhite);
            this.btnWorkflow.OnPanned += this.BtnWorkflow_OnPanned;

            this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.currentTripExpensesStackView = new SimpleBindableStackView<CurrentTripExpensesItemView, CurrentTripExpenseItemViewModel>(UILayoutConstraintAxis.Vertical, 0f);

            this.View.AggregateSubviews(this.scrollView, this.btnWorkflow);
            this.scrollView.AggregateSubviews(this.viewContainer);

            this.viewContainer.AggregateSubviews(this.currentTripExpensesStackView, this.btnAddExpense, this.viewLine);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.btnWorkflow.AtBottomOf(this.View),
                this.btnWorkflow.AtLeftOf(this.View),
                this.btnWorkflow.AtRightOf(this.View),
                this.btnWorkflow.Height().EqualTo(Metrics.SwipeButtonHeight),

                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.Above(this.btnWorkflow)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.currentTripExpensesStackView.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),
                this.currentTripExpensesStackView.AtLeftOf(this.viewContainer, CONTROLS_DISTANCE),
                this.currentTripExpensesStackView.AtRightOf(this.viewContainer, CONTROLS_DISTANCE),

                this.viewLine.Below(this.currentTripExpensesStackView, CONTROLS_DISTANCE),
                this.viewLine.AtLeftOf(this.viewContainer),
                this.viewLine.AtRightOf(this.viewContainer),

                this.btnAddExpense.Below(this.currentTripExpensesStackView, CONTROLS_DISTANCE),
                this.btnAddExpense.AtRightOf(this.viewContainer, CONTROLS_DISTANCE),
                this.btnAddExpense.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CurrentTripExpensesView, CurrentTripExpensesViewModel>();
            set.Bind(this.btnAddExpense).To(vm => vm.AddExpenseCommand);
            set.Bind(this.btnWorkflow.FrontView).For(v => v.BackgroundColor).To(vm => vm.FollowingState).WithConversion(ValueConverters.ActiveTripStateToBackgroundColorConverter);
            set.Bind(this.btnWorkflow.LblSwipe).To(vm => vm.FollowingActionName);
            set.Bind(this.currentTripExpensesStackView).For(v => v.ItemsSource).To(vm => vm.Expenses);

            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
        }

        public override void ViewWillAppear(bool animated)
        {
        	base.ViewWillAppear(animated);

        	this.ViewModel.ReloadCommand.Execute(null);
        }

        private void BtnWorkflow_OnPanned(object sender, System.EventArgs e)
        {
            this.ViewModel.ContinueCommand.Execute(null);
        }
    }
}
