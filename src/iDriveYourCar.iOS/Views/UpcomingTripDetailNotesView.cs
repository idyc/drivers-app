﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    public class UpcomingTripDetailNotesView : IDYCController<UpcomingTripDetailNotesViewModel>
    {
        private UIScrollView scrollView;
        private UIView viewContainer;

        private TripDetailsHeaderView headerView;
        private TripNotesView carKeyView, meetupView, publicNotesView, notesForDriverView;
        private UIView carKeyLine, meetupLine, publicNotesLine;

        private FluentLayout carKeyCollapsedConstraint,
                            carKeyLineCollapsedConstraint,
                            meetupCollapsedConstraint,
                            meetupLineCollapsedConstraint,
                            publicNotesCollapsedConstraint,
                            publicNotesLineCollapsedConstraint,
                            notesForDriverCollapsedConstraint;

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.carKeyLine = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.meetupLine = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.publicNotesLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.headerView = new TripDetailsHeaderView();
            this.carKeyView = new TripNotesView
            {
                ClipsToBounds = true
            };
            this.carKeyView.Title.Text = IDriveYourCarStrings.CarDescriptionAndKeyLocation;
            this.meetupView = new TripNotesView
            {
                ClipsToBounds = true
            };
            this.meetupView.Title.Text = IDriveYourCarStrings.MeetupInstructions;
            this.publicNotesView = new TripNotesView
            {
                ClipsToBounds = true
            };
            this.publicNotesView.Title.Text = IDriveYourCarStrings.TripNotes;
            this.notesForDriverView = new TripNotesView
            {
                ClipsToBounds = true
            };
            this.notesForDriverView.Title.Text = IDriveYourCarStrings.ClientNotes;

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.headerView,
                this.carKeyView,
                this.carKeyLine,
                this.meetupView,
                this.meetupLine,
                this.publicNotesView,
                this.publicNotesLine,
                this.notesForDriverView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.carKeyCollapsedConstraint = this.carKeyView.Height().EqualTo(0).SetActive(false);
            this.carKeyLineCollapsedConstraint = this.carKeyLine.Height().EqualTo(0).SetActive(false);
            this.meetupCollapsedConstraint = this.meetupView.Height().EqualTo(0).SetActive(false);
            this.meetupLineCollapsedConstraint = this.meetupLine.Height().EqualTo(0).SetActive(false);
            this.publicNotesCollapsedConstraint = this.publicNotesView.Height().EqualTo(0).SetActive(false);
            this.publicNotesLineCollapsedConstraint = this.publicNotesLine.Height().EqualTo(0).SetActive(false);
            this.notesForDriverCollapsedConstraint = this.notesForDriverView.Height().EqualTo(0).SetActive(false);

            this.viewContainer.AddConstraints(
                this.headerView.AtLeftOf(this.viewContainer),
                this.headerView.AtTopOf(this.viewContainer),
                this.headerView.AtRightOf(this.viewContainer),

                this.carKeyView.Below(this.headerView),
                this.carKeyView.AtLeftOf(this.viewContainer),
                this.carKeyView.AtRightOf(this.viewContainer),
                this.carKeyCollapsedConstraint,

                this.carKeyLine.Below(this.carKeyView),
                this.carKeyLine.WithSameLeft(this.carKeyView.Title),
                this.carKeyLine.AtRightOf(this.viewContainer),
                this.carKeyLine.Height().EqualTo(Metrics.LineThickness),
                this.carKeyLineCollapsedConstraint,

                this.meetupView.Below(this.carKeyLine),
                this.meetupView.AtLeftOf(this.viewContainer),
                this.meetupView.AtRightOf(this.viewContainer),
                this.meetupCollapsedConstraint,

                this.meetupLine.Below(this.meetupView),
                this.meetupLine.WithSameLeft(this.carKeyLine),
                this.meetupLine.WithSameRight(this.carKeyLine),
                this.meetupLine.Height().EqualTo(Metrics.LineThickness),
                this.meetupLineCollapsedConstraint,

                this.publicNotesView.Below(this.meetupLine),
                this.publicNotesView.AtLeftOf(this.viewContainer),
                this.publicNotesView.AtRightOf(this.viewContainer),
                this.publicNotesCollapsedConstraint,

                this.publicNotesLine.Below(this.publicNotesView),
                this.publicNotesLine.WithSameLeft(this.carKeyLine),
                this.publicNotesLine.WithSameRight(this.carKeyLine),
                this.publicNotesLine.Height().EqualTo(Metrics.LineThickness),
                this.publicNotesLineCollapsedConstraint,

                this.notesForDriverView.Below(this.publicNotesLine),
                this.notesForDriverView.AtLeftOf(this.viewContainer),
                this.notesForDriverView.AtRightOf(this.viewContainer),
                this.notesForDriverView.AtBottomOf(this.viewContainer),
                this.notesForDriverCollapsedConstraint
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<UpcomingTripDetailNotesView, UpcomingTripDetailNotesViewModel>();
            set.Bind(this.headerView.Title).To(vm => vm.Trip).WithConversion(ValueConverters.TripToClientNameSurnameConverter);
            set.Bind(this.headerView.ReturnTrip).For(MvxBindings.Visibility).To(vm => vm.Trip.TripReturn).WithConversion(FwkValueConverters.VisibilityConverter);
            set.Bind(this.headerView.ReturnTrip.Tap()).For(g => g.Command).To(vm => vm.ShowReturnTripDetailsCommand);

            set.Bind(this.headerView.TripDate).To(vm => vm.PickupDate);
            set.Bind(this.headerView.TripDate).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.PickupDate));

            set.Bind(this.headerView.TripTime).To(vm => vm.PickupTime);
            set.Bind(this.headerView.TripTime).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.PickupTime));

            set.Bind(this.headerView.Footer).To(vm => vm.Trip.TripType).WithConversion(ValueConverters.TripTypeEnumToTripTypeStringConverter);

            set.Bind(this.carKeyView.Text).To(vm => vm.Trip.KeyLocationVehicleDescription);
            set.Bind(this.carKeyView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToIconConverter, nameof(TripFieldChanged.KeyLocationVehicleDescription));
            set.Bind(this.carKeyView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.KeyLocationVehicleDescription));
            set.Bind(this.carKeyView.Text).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.KeyLocationVehicleDescription));

            set.Bind(this.meetupView.Text).To(vm => vm.Trip.MeetupInstructions);
            set.Bind(this.meetupView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToIconConverter, nameof(TripFieldChanged.MeetupInstructions));
            set.Bind(this.meetupView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.MeetupInstructions));
            set.Bind(this.meetupView.Text).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.MeetupInstructions));

            set.Bind(this.publicNotesView.Text).To(vm => vm.Trip.PublicNotes).WithConversion(ValueConverters.TripPublicNotesToFormattedStringConverter);
            set.Bind(this.publicNotesView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToIconConverter, nameof(TripFieldChanged.PublicNotes));
            set.Bind(this.publicNotesView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.PublicNotes));
            set.Bind(this.publicNotesView.Text).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.PublicNotes));

            set.Bind(this.notesForDriverView.Text).To(vm => vm.Trip.Client.NotesForDriver);
            set.Bind(this.notesForDriverView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToIconConverter, nameof(TripFieldChanged.NotesForDriver));
            set.Bind(this.notesForDriverView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.NotesForDriver));
            set.Bind(this.notesForDriverView.Text).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.NotesForDriver));

            set.Bind(this.carKeyCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip).WithConversion(ValueConverters.TripTypeToCarDescriptionAndKeyLocationVisibilityConverter);
            set.Bind(this.carKeyLineCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip).WithConversion(ValueConverters.TripTypeToCarDescriptionAndKeyLocationVisibilityConverter);
            set.Bind(this.meetupCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip).WithConversion(ValueConverters.TripTypeToMeetupInstructionsVisibilityConverter);
            set.Bind(this.meetupLineCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip).WithConversion(ValueConverters.TripTypeToMeetupInstructionsVisibilityConverter);
            set.Bind(this.publicNotesCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.PublicNotes).WithConversion(FwkValueConverters.CollectionToVisibilityConverter);
            set.Bind(this.publicNotesLineCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.PublicNotes).WithConversion(FwkValueConverters.CollectionToVisibilityConverter);
            set.Bind(this.notesForDriverCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.Client.NotesForDriver).WithConversion(FwkValueConverters.StringNotEmptyToVisibilityConverter);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
