﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using Foundation;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.iOS.Views;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class DriverUnavailabilityView : IDYCTabsMenuController<DriverUnavailabilityViewModel>
    {
        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Availability;

            this.topTabBar.SetItems(new NSObject[]
            {
                new NSString(IDriveYourCarStrings.Recurring.ToUpper()),
                new NSString(IDriveYourCarStrings.Vacation.ToUpper())
            });

            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.WeeklyUnavailabilities) as DriverUnavailabilityWeeklyView);
            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.ExtendedUnavailabilities) as DriverUnavailabilityExtendedView);

            this.pageViewController.SetViewControllers(new UIViewController[] { this.tabsViewControllers[0] }, UIPageViewControllerNavigationDirection.Forward, true, null);
            this.currentSelectedTab = 0;

            this.View.BringSubviewToFront(this.topTabBar);
        }
    }
}
