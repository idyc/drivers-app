﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Extensions;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class UpcomingTripsHeaderView : IDYCView
    {
        private const float CONTROLS_DISTANCE = 8f;
        private const float BTN_PADDING = 4f;

        private UIView viewDate;

        public IDYCLabel LblDate { get; set; }

        public CardView CardView { get; set; }

        public UIView ViewLeft { get; set; }

        public UIView ViewRight { get; set; }

        public UIImageView ImgLeft { get; set; }

        public UIImageView ImgRight { get; set; }

        public UpcomingTripsHeaderView(string imageLeftName, string imageRightName)
        {
            if(!string.IsNullOrEmpty(imageLeftName))
                this.ImgLeft.Image = UIImage.FromBundle(imageLeftName);

            if(!string.IsNullOrEmpty(imageRightName))
                this.ImgRight.Image = UIImage.FromBundle(imageRightName);
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.viewDate = new UIView();

            this.LblDate = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.CardView = new CardView();
            this.ViewLeft = new UIView { UserInteractionEnabled = true };
            this.ViewRight = new UIView { UserInteractionEnabled = true };
            this.ImgLeft = new UIImageView();
            this.ImgRight = new UIImageView();

            this.AggregateSubviews(this.viewDate, this.CardView);
            this.viewDate.AggregateSubviews(this.LblDate);
            this.CardView.AggregateSubviews(this.ViewLeft, this.ViewRight);
            this.ViewLeft.AggregateSubviews(this.ImgLeft);
            this.ViewRight.AggregateSubviews(this.ImgRight);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.viewDate.AtLeftOf(this, CONTROLS_DISTANCE),
                this.viewDate.AtTopOf(this),
                this.viewDate.AtBottomOf(this),

                this.CardView.AtRightOf(this),
                this.CardView.AtTopOf(this),
                this.CardView.AtBottomOf(this)
            );

            this.viewDate.AddConstraints(
                this.LblDate.AtLeftOf(this.viewDate),
                this.LblDate.AtRightOf(this.viewDate),
                this.LblDate.AtTopOf(this.viewDate),
                this.LblDate.AtBottomOf(this.viewDate)
            );

            this.CardView.AddConstraints(
                this.ViewLeft.AtLeftOf(this.CardView),
                this.ViewLeft.AtTopOf(this.CardView),
                this.ViewLeft.AtBottomOf(this.CardView),

                this.ViewRight.ToRightOf(this.ViewLeft),
                this.ViewRight.AtRightOf(this.CardView),
                this.ViewRight.AtTopOf(this.CardView),
                this.ViewRight.AtBottomOf(this.CardView)
            );

            this.ViewLeft.AddConstraints(
                this.ImgLeft.AtLeftOf(this.ViewLeft, BTN_PADDING),
                this.ImgLeft.AtTopOf(this.ViewLeft, BTN_PADDING),
                this.ImgLeft.AtRightOf(this.ViewLeft, BTN_PADDING),
                this.ImgLeft.AtBottomOf(this.ViewLeft, BTN_PADDING)
            );

            this.ViewRight.AddConstraints(
                this.ImgRight.AtLeftOf(this.ViewRight, BTN_PADDING),
                this.ImgRight.AtTopOf(this.ViewRight, BTN_PADDING),
                this.ImgRight.AtRightOf(this.ViewRight, BTN_PADDING),
                this.ImgRight.AtBottomOf(this.ViewRight, BTN_PADDING)
            );
        }
    }
}
