﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class CompletedTripItemView : IDYCBindableView
    {
        private const float HEIGHT = 28f;
        private const float LABEL_WIDTH_MULTIPLIER = .4f;
        private const float CONTROLS_DISTANCE = 4f;

        private IDYCLabel lblTime, lblEarning;

        public CompletedTripItemView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.lblTime = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };
            this.lblEarning = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                TextAlignment = UIKit.UITextAlignment.Right
            };

            this.AggregateSubviews(this.lblTime, this.lblEarning);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Height().EqualTo(HEIGHT),

                this.lblEarning.AtRightOf(this, 28f),
                this.lblEarning.WithSameCenterY(this),

                this.lblTime.AtLeftOf(this, 44f),
                this.lblTime.ToLeftOf(this.lblEarning, CONTROLS_DISTANCE),
                this.lblTime.WithSameCenterY(this)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CompletedTripItemView, CompletedTripItemViewModel>();
            set.Bind(this.lblTime).To(vm => vm.PickupTime);
            set.Bind(this.lblEarning).To(vm => vm.CompletedTrip.Price).WithConversion(ValueConverters.DecimalToMoneyConverter);
            set.Apply();
        }
    }
}
