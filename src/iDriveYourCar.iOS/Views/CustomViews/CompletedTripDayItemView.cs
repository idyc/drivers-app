﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class CompletedTripDayItemView : IDYCBindableView
    {
        private OAStackView.OAStackView expandableStack;

        private CollapsedCompletedTripsDayView viewCollapsed;
        private UIView viewExpanded;

        private UIView viewLine;

        private SimpleBindableStackView<CompletedTripItemView, CompletedTripItemViewModel> tripsStack;

        public CompletedTripDayItemView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.expandableStack = new OAStackView.OAStackView
            {
                Axis = UILayoutConstraintAxis.Vertical,
                Distribution = OAStackView.OAStackViewDistribution.Fill
            };
            this.viewCollapsed = new CollapsedCompletedTripsDayView();
            this.viewExpanded = new UIView { Hidden = true };
            this.tripsStack = new SimpleBindableStackView<CompletedTripItemView, CompletedTripItemViewModel>(UILayoutConstraintAxis.Vertical, 0f);
            this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(this.expandableStack, this.viewLine);

            this.expandableStack.AddArrangedSubview(this.viewCollapsed);
            this.expandableStack.AddArrangedSubview(this.viewExpanded);

            this.viewExpanded.AggregateSubviews(this.tripsStack);

            this.viewCollapsed.BtnAccessory.TouchUpInside += (sender, e) =>
            {
                this.viewExpanded.Hidden = !this.viewExpanded.Hidden;
                if(this.viewExpanded.Hidden)
                    this.viewCollapsed.BtnAccessory.SetImage(UIImage.FromBundle("arrow_right"), UIControlState.Normal);
                else
                    this.viewCollapsed.BtnAccessory.SetImage(UIImage.FromBundle("arrow_down"), UIControlState.Normal);
            };
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.expandableStack.AtLeftOf(this),
                this.expandableStack.AtTopOf(this),
                this.expandableStack.AtRightOf(this),

                this.viewLine.Below(this.expandableStack),
                this.viewLine.AtLeftOf(this),
                this.viewLine.AtRightOf(this),
                this.viewLine.AtBottomOf(this),
                this.viewLine.Height().EqualTo(Metrics.LineThickness)
            );

            this.viewExpanded.AddConstraints(
                this.tripsStack.AtLeftOf(this.viewExpanded),
                this.tripsStack.AtTopOf(this.viewExpanded),
                this.tripsStack.AtRightOf(this.viewExpanded),
                this.tripsStack.AtBottomOf(this.viewExpanded)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CompletedTripDayItemView, CompletedTripsDayItemViewModel>();
            set.Bind(this.viewCollapsed.LblDay).To(vm => vm.Date).WithConversion(FwkValueConverters.DateTimeToStringConverter, "DayMonthDescriptionAndDayNumber");
            set.Bind(this.viewCollapsed.LblEarning).To(vm => vm.DayEarnings).WithConversion(ValueConverters.DecimalToMoneyConverter);
            set.Bind(this.tripsStack).For(v => v.ItemsSource).To(vm => vm.CompletedTripsTimes);
            set.Apply();
        }
    }
}
