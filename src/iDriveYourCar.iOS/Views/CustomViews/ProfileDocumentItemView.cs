﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class ProfileDocumentItemView : IDYCBindableView
    {
        private const float PADDING = 16f;
        private const float IMG_PREVIEW_SIZE = 30f;
        private const float CONTROLS_DISTANCE = 16f;

        private UIView viewLine;
        private UIImageView imgPreview;
        private IDYCLabel lblDocumentType, lblLastUpdated, lblLastUpdatedValue;
        private UIButton btnUpload;

        public ProfileDocumentItemView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.Clear;

            this.UserInteractionEnabled = true;
            this.ClipsToBounds = true;

            this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.imgPreview = new UIImageView(UIImage.FromBundle("ic_profile_document"));
            this.lblDocumentType = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.lblLastUpdated = new IDYCLabel(Metrics.FontSizeSmall)
            {
                Text = IDriveYourCarStrings.LastUpdated,
                TextColor = IDYCColors.MidGrayTextColor
            };
            this.lblLastUpdatedValue = new IDYCLabel(Metrics.FontSizeSmall)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                TextAlignment = UITextAlignment.Left
            };
            this.btnUpload = new UIButton();
            this.btnUpload.SetImage(UIImage.FromBundle(Assets.Upload), UIControlState.Normal);

            this.AggregateSubviews(
                this.viewLine,
                this.imgPreview,
                this.lblDocumentType,
                this.lblLastUpdated,
                this.lblLastUpdatedValue,
                this.btnUpload);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.btnUpload.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Horizontal);
            this.lblDocumentType.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);
            this.lblLastUpdated.SetContentHuggingPriority(500f, UILayoutConstraintAxis.Horizontal);
            this.lblLastUpdatedValue.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);

            this.AddConstraints(
                this.viewLine.AtLeftOf(this),
                this.viewLine.AtTopOf(this),
                this.viewLine.AtRightOf(this),
                this.viewLine.Height().EqualTo(Metrics.LineThickness),

                this.imgPreview.AtLeftOf(this),
                this.imgPreview.Below(this.viewLine, PADDING),
                this.imgPreview.Width().EqualTo(IMG_PREVIEW_SIZE),
                this.imgPreview.Height().EqualTo(IMG_PREVIEW_SIZE),
                this.imgPreview.AtBottomOf(this, PADDING),

                this.btnUpload.WithSameCenterY(this.imgPreview),
                this.btnUpload.AtRightOf(this, PADDING),

                this.lblDocumentType.ToRightOf(this.imgPreview, CONTROLS_DISTANCE),
                this.lblDocumentType.WithSameTop(this.imgPreview),
                this.lblDocumentType.ToLeftOf(this.btnUpload, CONTROLS_DISTANCE),

                this.lblLastUpdated.Below(this.lblDocumentType),
                this.lblLastUpdated.WithSameLeft(this.lblDocumentType),

                this.lblLastUpdatedValue.ToRightOf(this.lblLastUpdated, 4f),
                this.lblLastUpdatedValue.WithSameCenterY(this.lblLastUpdated),
                this.lblLastUpdatedValue.ToLeftOf(this.btnUpload, CONTROLS_DISTANCE / 2)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ProfileDocumentItemView, DocumentItemViewModel>();
            set.Bind(this.lblDocumentType).To(vm => vm.DocumentTypeString);
            set.Bind(this.lblLastUpdatedValue).To(vm => vm.Document.UpdatedAt).WithConversion(FwkValueConverters.DateTimeToStringConverter, "ShortDate");
            set.Bind(this.btnUpload).To(vm => vm.UploadDocumentCommand);
            set.Bind(this.Tap()).For(g => g.Command).To(vm => vm.DisplayDocumentCommand);
            set.Bind(this).For(MvxBindings.NetworkActivityIndicator).To(vm => vm.DisplayDocumentTaskCompletion.IsNotCompleted).WithFallback(false);
            set.Apply();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.imgPreview.Layer.CornerRadius = this.imgPreview.Frame.Width / 2;
        }
    }
}
