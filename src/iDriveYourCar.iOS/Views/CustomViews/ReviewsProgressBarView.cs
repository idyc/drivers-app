﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class ReviewsProgressBarView : IDYCView
    {
        private const float CONTROLS_DISTANCE = 12f;
        private const float TEXT_SEPARATION = 4f;
        private const float RATING_BAR_HEIGHT = 15f;

        public IDYCLabel LabelStar { get; set; }

        public IDYCLabel LabelStarQuantity { get; set; }

        public UIProgressView ProgressStar { get; set; }

        protected override void CreateViews()
        {
            base.CreateViews();

            var primaryColor = appConfiguration.Value.PrimaryColor.ToNativeColor();

            this.LabelStar = new IDYCLabel(Metrics.FontSizeTiny) { TextColor = primaryColor };
            this.ProgressStar = new UIProgressView();
            this.LabelStarQuantity = new IDYCLabel(Metrics.FontSizeTiny) { TextColor = primaryColor };

            this.AggregateSubviews(this.LabelStar, this.ProgressStar, this.LabelStarQuantity);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LabelStar.AtLeftOf(this),
                this.LabelStar.AtTopOf(this),
                this.LabelStar.AtBottomOf(this, 5f),

                this.ProgressStar.ToLeftOf(this.LabelStarQuantity, TEXT_SEPARATION),
                this.ProgressStar.ToRightOf(this.LabelStar, TEXT_SEPARATION),
                this.ProgressStar.WithSameCenterY(this.LabelStar),
                this.ProgressStar.Height().EqualTo(RATING_BAR_HEIGHT),

                this.LabelStarQuantity.AtRightOf(this),
                this.LabelStarQuantity.WithSameCenterY(this.LabelStar)
            );
        }
    }
}
