﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class TripDetailStopView : IDYCBindableView
    {
        private const float TEXT_SEPARATION = 4f;

        public IDYCLabel lblStopType, lblStopCity;
        private UIImageView imgIcon;
        private UIView viewLine;

        protected override void CreateViews()
        {
            base.CreateViews();

            this.lblStopType = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.Stop
            };
            this.lblStopCity = new IDYCLabel(Metrics.FontSizeNormalMinus) { TextColor = IDYCColors.MidGrayTextColor };
            this.imgIcon = new UIImageView(UIImage.FromBundle(Assets.Location));

            this.viewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(this.lblStopType, this.lblStopCity, this.imgIcon, this.viewLine);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.imgIcon.AtLeftOf(this, Metrics.WINDOW_MARGIN * 2),
                this.imgIcon.WithSameTop(this.lblStopType),
                this.imgIcon.Width().EqualTo(18f),
                this.imgIcon.Height().EqualTo().WidthOf(this.imgIcon),

                this.lblStopType.AtTopOf(this, Metrics.DEFAULT_DISTANCE),
                this.lblStopType.ToRightOf(this.imgIcon, Metrics.DEFAULT_DISTANCE / 2),
                this.lblStopType.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.lblStopCity.Below(this.lblStopType, TEXT_SEPARATION),
                this.lblStopCity.WithSameLeft(this.lblStopType),
                this.lblStopCity.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.viewLine.Below(this.lblStopCity, Metrics.DEFAULT_DISTANCE),
                this.viewLine.WithSameLeft(this.lblStopType),
                this.viewLine.AtRightOf(this),
                this.viewLine.Height().EqualTo(Metrics.LineThickness),
                this.viewLine.AtBottomOf(this)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<TripDetailStopView, TripStopItemViewModel>();
            set.Bind(this.lblStopCity).To(vm => vm.TripStop.Address.NameCityAndStateFormatted);
            set.Apply();
        }
    }
}