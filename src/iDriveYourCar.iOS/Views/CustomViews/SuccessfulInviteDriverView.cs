﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class SuccessfulInviteDriverView : IDYCBindableView
    {
        private const float HEIGHT = 88f;
        private const float MARGIN = 16f;
        private const float CONTROLS_DISTANCE = 8f;

        private UIView viewContainer;
        private IDYCMultilineLabel lblContactName, lblInvitationDetails;

        private GreenBackgroundButton btnRedeem;

        public SuccessfulInviteDriverView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.viewContainer = new UIView();
            this.lblContactName = new IDYCMultilineLabel(Metrics.FontSizeNormal, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.lblInvitationDetails = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.LightGrayTextColor
            };
            this.btnRedeem = new GreenBackgroundButton();
            this.btnRedeem.SetTitle(IDriveYourCarStrings.Redeem.ToUpper(), UIControlState.Normal);
            this.btnRedeem.SetTitle(IDriveYourCarStrings.Redeemed.ToUpper(), UIControlState.Disabled);
            this.btnRedeem.ContentEdgeInsets = new UIEdgeInsets(6f, 6f, 6f, 6f);

            this.AggregateSubviews(this.viewContainer, this.btnRedeem);
            this.viewContainer.AggregateSubviews(this.lblContactName, this.lblInvitationDetails);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Height().EqualTo(HEIGHT),

                this.viewContainer.AtLeftOf(this, MARGIN),
                this.viewContainer.WithSameCenterY(this),
                this.viewContainer.ToLeftOf(this.btnRedeem, CONTROLS_DISTANCE),

                this.btnRedeem.AtRightOf(this, MARGIN),
                this.btnRedeem.WithSameCenterY(this),
                this.btnRedeem.WithRelativeWidth(this, .32f)
            );

            this.viewContainer.AddConstraints(
                this.lblContactName.AtLeftOf(this.viewContainer),
                this.lblContactName.AtTopOf(this.viewContainer),
                this.lblContactName.AtRightOf(this.viewContainer),

                this.lblInvitationDetails.Below(this.lblContactName, CONTROLS_DISTANCE),
                this.lblInvitationDetails.AtLeftOf(this.viewContainer),
                this.lblInvitationDetails.AtRightOf(this.viewContainer),
                this.lblInvitationDetails.AtBottomOf(this.viewContainer)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<SuccessfulInviteDriverView, DriverInvitationSuccessfulItemViewModel>();
            set.Bind(this).For(v => v.BackgroundColor).To(vm => vm.GrayBackground).WithConversion(ValueConverters.GrayBackgroundColorConverter);
            set.Bind(this.lblContactName).To(vm => vm.DriverInvitation.Contact);
            set.Bind(this.lblInvitationDetails).To(vm => vm.ApplicantStatusDescription);
            set.Bind(this.btnRedeem).To(vm => vm.RedeemCommand);
            set.Bind(this.btnRedeem).For(v => v.Enabled).To(vm => vm.DriverInvitation.RedeemRequested).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter);
            set.Apply();
        }
    }
}
