﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Controls.TagsView;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class CurrentTripExpensesItemView : IDYCBindableView
    {
        private const float CONTROLS_DISTANCE = 16f;

        private TagView filename;
        public UIButton BtnDelete { get; set; }
        public IDYCLabel LblDescription { get; set; }
        public IDYCLabel LblAmount { get; set; }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.filename = new TagView();
            this.filename.Hidden = true;
            this.filename.BackgroundColor = "#E0E0E0".ToUIColor();
            this.filename.Label.TextColor = UIColor.DarkTextColor;
            this.filename.Layer.CornerRadius = 10f;
            this.filename.Label.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeTiny);
            this.filename.PaddingY = 6f;
            this.filename.PaddingX = 12f;

            this.LblDescription = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.LblAmount = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.BtnDelete = new UIButton();
            this.BtnDelete.SetImage(UIImage.FromBundle(Assets.Delete), UIControlState.Normal);

            this.AggregateSubviews(this.LblDescription, this.LblAmount, this.filename, this.BtnDelete);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblDescription.AtTopOf(this, CONTROLS_DISTANCE),
                this.LblDescription.AtLeftOf(this),
                this.LblDescription.AtRightOf(this),

                this.BtnDelete.WithSameCenterY(this.LblDescription),
                this.BtnDelete.AtRightOf(this),

                this.LblAmount.WithSameCenterY(this.LblDescription),
                this.LblAmount.ToLeftOf(this.BtnDelete, CONTROLS_DISTANCE),

                this.filename.Below(this.LblDescription, CONTROLS_DISTANCE / 2),
                this.filename.AtLeftOf(this),
                this.filename.AtBottomOf(this)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CurrentTripExpensesItemView, CurrentTripExpenseItemViewModel>();
            set.Bind(this.LblDescription).To(vm => vm.Expense.Description);
            set.Bind(this.LblAmount).To(vm => vm.Expense.Amount).WithConversion(ValueConverters.DecimalToMoneyConverter);
            set.Bind(this.BtnDelete).To(vm => vm.RemoveExpenseCommand);
            set.Bind(this.filename).For(v => v.Hidden).To(vm => vm.Expense.Filename).WithConversion(FwkValueConverters.StringEmptyToBoolConverter).Mode(MvvmCross.Binding.MvxBindingMode.OneWay);
            set.Bind(this.filename.Label).To(vm => vm.Expense.Filename);
            set.Apply();
        }
    }
}

