﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class PendingInviteDriverView : IDYCBindableView
    {
        private const float HEIGHT = 88f;
        private const float MARGIN = 16f;
        private const float CONTROLS_DISTANCE = 8f;

        private IDYCMultilineLabel lblContactName, lblInvitationDetails;

        private WhiteBorderedButton btnRemind;

        public PendingInviteDriverView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.lblContactName = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.lblInvitationDetails = new IDYCMultilineLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };
            this.btnRemind = new WhiteBorderedButton();
            this.btnRemind.SetTitle(IDriveYourCarStrings.Remind.ToUpper(), UIControlState.Normal);
            this.btnRemind.ContentEdgeInsets = new UIEdgeInsets(6f, 6f, 6f, 6f);

            this.AggregateSubviews(this.lblContactName, this.lblInvitationDetails, this.btnRemind);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Height().EqualTo(HEIGHT),

                this.lblContactName.AtTopOf(this, MARGIN),
                this.lblContactName.ToLeftOf(this.btnRemind, CONTROLS_DISTANCE),
                this.lblContactName.AtLeftOf(this, MARGIN),

                this.lblInvitationDetails.Below(this.lblContactName, CONTROLS_DISTANCE),
                this.lblInvitationDetails.WithSameLeft(this.lblContactName),
                this.lblInvitationDetails.WithSameRight(this.lblContactName),
                this.lblInvitationDetails.Bottom().LessThanOrEqualTo(MARGIN).BottomOf(this),

                this.btnRemind.AtRightOf(this, MARGIN),
                this.btnRemind.WithSameCenterY(this),
                this.btnRemind.WithRelativeWidth(this, .25f)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<PendingInviteDriverView, DriverInvitationPendingItemViewModel>();
            set.Bind(this).For(v => v.BackgroundColor).To(vm => vm.GrayBackground).WithConversion(ValueConverters.GrayBackgroundColorConverter);
            set.Bind(this.lblContactName).To(vm => vm.DriverInvitation).WithConversion(ValueConverters.DriverInvitationContactToDriverInvitationEmailConverter);
            set.Bind(this.lblInvitationDetails).To(vm => vm.ApplicantStatusDescription);
            set.Bind(this.btnRemind).For(v => v.Hidden).To(vm => vm.CanRemind).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter);
            set.Bind(this.btnRemind).To(vm => vm.RemindCommand);
            set.Apply();
        }
    }
}
