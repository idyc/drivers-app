﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Extensions;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class DriverUnavailabilityExtendedItemView : IDYCBindableView
    {
        private const float MARGIN = 4f;
        private const float CONTROLS_DISTANCE = 8f;

        private IDYCLabel lblDateRange;
        private UIButton btnDelete;

        public DriverUnavailabilityExtendedItemView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.lblDateRange = new IDYCLabel(Metrics.FontSizeNormal, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.btnDelete = new UIButton();
            this.btnDelete.SetImage(UIImage.FromBundle(Assets.Delete), UIControlState.Normal);

            this.AggregateSubviews(this.lblDateRange, this.btnDelete);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.btnDelete.AtRightOf(this, MARGIN * 2),
                this.btnDelete.WithSameCenterY(this),

                this.lblDateRange.ToLeftOf(this.btnDelete, CONTROLS_DISTANCE),
                this.lblDateRange.AtLeftOf(this),
                this.lblDateRange.AtTopOf(this, MARGIN),
                this.lblDateRange.AtBottomOf(this, MARGIN)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DriverUnavailabilityExtendedItemView, DriverUnavailabilityExtendedItemViewModel>();
            set.Bind(this.lblDateRange).To(vm => vm.DateRange);
            set.Bind(this.btnDelete).To(vm => vm.RemoveUnavailabilityExtendedCommand);
            set.Apply();
        }
    }
}
