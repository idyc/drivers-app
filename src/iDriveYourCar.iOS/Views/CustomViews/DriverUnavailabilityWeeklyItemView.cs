﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Controls.TagsView;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views.CustomViews
{
    public class DriverUnavailabilityWeeklyItemView : IDYCBindableView
    {
        private const float MARGIN = 16f;

        private IDYCLabel lblEveryXDay;
        private BindableTagsView<DriverUnavailabilityWeeklyTimeItemViewModel> timesView;
        private DriverUnavailabilityToTimeRangeConverter converter;

        public DriverUnavailabilityWeeklyItemView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.lblEveryXDay = new IDYCLabel(Metrics.FontSizeNormal, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.converter = new DriverUnavailabilityToTimeRangeConverter();

            this.timesView = new BindableTagsView<DriverUnavailabilityWeeklyTimeItemViewModel>(item => (string)this.converter.Convert(item.DriverUnavailability, null, null, null));
            this.timesView.TagBackgroundColor = "#F29C9A".ToUIColor();
            this.timesView.RemoveIconName = Assets.CancelSmall;
            this.timesView.CornerRadius = 12f;
            this.timesView.TextFont = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeNormalMinus);
            this.timesView.PaddingY = 4f;
            this.timesView.ControlsDistance = 2f;
            this.timesView.PaddingX = 8f;

            this.AggregateSubviews(this.lblEveryXDay, this.timesView);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(

                this.lblEveryXDay.AtLeftOf(this),
                this.lblEveryXDay.AtTopOf(this, MARGIN),

                this.timesView.Below(this.lblEveryXDay, Metrics.DEFAULT_DISTANCE / 2),
                this.timesView.AtLeftOf(this),
                this.timesView.AtRightOf(this),
                this.timesView.AtBottomOf(this)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DriverUnavailabilityWeeklyItemView, DriverUnavailabilityWeeklyDayItemViewModel>();
            set.Bind(this.timesView).For(g => g.RemoveButtonCommand).To(vm => vm.RemoveUnavailabilityTimeCommand);
            set.Bind(this.lblEveryXDay).To(vm => vm.DayOfWeek).WithConversion(ValueConverters.DayNumberToEveryDayNameConverter);
            set.Bind(this.timesView).For(v => v.ItemsSource).To(vm => vm.DriverUnavailabilityTimes);
            set.Apply();
        }
    }
}
