﻿//using Cirrious.FluentLayouts.Touch;
//using DGenix.Mobile.Fwk.Core.Converters;
//using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
//using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
//using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
//using iDriveYourCar.Core.Converters;
//using iDriveYourCar.Core.ViewModels;
//using iDriveYourCar.iOS.MvxSupport;
//using MvvmCross.Binding.BindingContext;
//using UIKit;

//namespace iDriveYourCar.iOS.Views
//{
//	[PanelPresentation(PresentationMode.CenterChild, true)]
//	public class UploadDocumentView : IDYCController<UploadDocumentViewModel>
//	{
//		private const float MARGIN = Metrics.WINDOW_MARGIN * 2;
//		private const float CONTROLS_DISTANCE = 16f;
//		private const float IMG_THUMBNAIL_SIZE = 60f;

//		private IDYCLabel lblDocumentType;
//		private IDYCMultilineLabel lblUploadDocumentDetails, lblLastUpdated;
//		private UIImageView imgDocumentThumbnail;
//		private PrimaryBackgroundButton btnCamera, btnGallery;

//		public UploadDocumentView()
//		{
//		}

//		public override void CreateViews()
//		{
//			base.CreateViews();

//			this.View.BackgroundColor = UIColor.White;
//			this.Title = IDriveYourCarStrings.Upload;

//			this.lblDocumentType = new IDYCLabel(Metrics.FontSizeGigant)
//			{
//				TextColor = IDYCColors.DarkGrayTextColor,
//				TextAlignment = UITextAlignment.Center
//			};
//			this.lblUploadDocumentDetails = new IDYCMultilineLabel(Metrics.FontSizeNormal)
//			{
//				TextColor = IDYCColors.LightGrayTextColor,
//				TextAlignment = UITextAlignment.Center
//			};
//			this.imgDocumentThumbnail = new UIImageView(UIImage.FromBundle("ic_profile_picture.jpg"));
//			this.lblLastUpdated = new IDYCMultilineLabel(Metrics.FontSizeSmall)
//			{
//				TextColor = IDYCColors.LightGrayTextColor,
//				TextAlignment = UITextAlignment.Center
//			};
//			this.btnCamera = new PrimaryBackgroundButton(this.appConfiguration.Value);
//			this.btnCamera.SetTitle(IDriveYourCarStrings.Camera.ToUpper(), UIControlState.Normal);
//			this.btnGallery = new PrimaryBackgroundButton(this.appConfiguration.Value);
//			this.btnGallery.SetTitle(IDriveYourCarStrings.PhotoLibrary.ToUpper(), UIControlState.Normal);

//			this.View.AddSubviews(
//				this.lblDocumentType,
//				this.lblUploadDocumentDetails,
//				this.imgDocumentThumbnail,
//				this.lblLastUpdated,
//				this.btnCamera,
//				this.btnGallery
//			);
//			this.View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
//		}

//		public override void CreateConstraints()
//		{
//			base.CreateConstraints();

//			this.View.AddConstraints(
//				this.lblDocumentType.AtTopOf(this.View, MARGIN * 1.5f),
//				this.lblDocumentType.AtLeftOf(this.View, MARGIN),
//				this.lblDocumentType.AtRightOf(this.View, MARGIN),

//				this.lblUploadDocumentDetails.Below(this.lblDocumentType, CONTROLS_DISTANCE),
//				this.lblUploadDocumentDetails.AtLeftOf(this.View, MARGIN),
//				this.lblUploadDocumentDetails.AtRightOf(this.View, MARGIN),

//				this.imgDocumentThumbnail.Below(this.lblUploadDocumentDetails, CONTROLS_DISTANCE),
//				this.imgDocumentThumbnail.WithSameCenterX(this.View),
//				this.imgDocumentThumbnail.Width().EqualTo(IMG_THUMBNAIL_SIZE),
//				this.imgDocumentThumbnail.Height().EqualTo(IMG_THUMBNAIL_SIZE),

//				this.lblLastUpdated.Below(this.imgDocumentThumbnail, CONTROLS_DISTANCE / 2),
//				this.lblLastUpdated.AtLeftOf(this.View, MARGIN),
//				this.lblLastUpdated.AtRightOf(this.View, MARGIN),

//				this.btnCamera.Below(this.lblLastUpdated, CONTROLS_DISTANCE * 2),
//				this.btnCamera.AtLeftOf(this.View, MARGIN),
//				this.btnCamera.AtRightOf(this.View, MARGIN),

//				this.btnGallery.Below(this.btnCamera, CONTROLS_DISTANCE),
//				this.btnGallery.AtLeftOf(this.View, MARGIN),
//				this.btnGallery.AtRightOf(this.View, MARGIN)
//			);
//		}

//		public override void CreateMvxBindings()
//		{
//			base.CreateMvxBindings();

//			var set = this.CreateBindingSet<UploadDocumentView, UploadDocumentViewModel>();
//			//set.Bind(this).For(Mvxbin).To(vm => vm.UploadDocumentTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);
//			set.Bind(this.lblDocumentType).To(vm => vm.DocumentTypeString);
//			set.Bind(this.lblUploadDocumentDetails).To(vm => vm.ReuploadDocumentByTakingAClearPictureOrChoosingFromYourPhotoLibrary);
//			set.Bind(this.lblLastUpdated).To(vm => vm.Document.UpdatedAt).WithConversion(ValueConverters.DateTimeToFormattedStringUploadDocumentConverter, "ShortDate");
//			set.Bind(this.btnCamera).To(vm => vm.UploadDocumentFromCameraCommand);
//			set.Bind(this.btnGallery).To(vm => vm.UploadDocumentFromPhotoLibraryCommand);
//			set.Apply();

//			this.AddNetworkActivityIndicatorBinding(this, nameof(this.ViewModel.UploadDocumentTaskCompletion));
//		}

//		public override void MvxSubscribe()
//		{
//			base.MvxSubscribe();
//		}

//		public override void MvxUnsubscribe()
//		{
//			base.MvxUnsubscribe();
//		}
//	}
//}
