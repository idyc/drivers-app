﻿using DGenix.Mobile.Fwk.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using Foundation;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.Cells;
using UIKit;

namespace iDriveYourCar.iOS.Views.Sources
{
    public class FindOpenTripsTableSource : BaseSimpleTableSource
    {
        public FindOpenTripsTableSource(UITableView tableView) : base(tableView)
        {
            this.DeselectAutomatically = true;
        }

        protected override BaseTableViewCell GetReusableCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return this.DequeueAndConfigureReusableCell(indexPath, item);
        }

        protected override void RegisterCellsForReuse()
        {
            this.RegisterCell<OpenTripTableViewCell, OpenTripItemViewModel>();
            this.RegisterCell<DateTripTableViewCell, DateOpenTripItemViewModel>();
        }
    }
}
