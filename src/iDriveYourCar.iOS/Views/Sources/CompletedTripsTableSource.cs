﻿//using System;
//using DGenix.Mobile.Fwk.iOS.Views.Cells;
//using DGenix.Mobile.Fwk.iOS.Views.Sources;
//using Foundation;
//using UIKit;
//using iDriveYourCar.Core.ViewModels.Items;
//using iDriveYourCar.iOS.Views.Cells;
//using System.Collections.ObjectModel;

//namespace iDriveYourCar.iOS.Views.Sources
//{
//	public class CompletedTripsTableSource : BaseSimpleTableSource
//	{
//		private ObservableCollection<CompletedTripsDayItemViewModel> Source => this.ItemsSource as ObservableCollection<CompletedTripsDayItemViewModel>;

//		public CompletedTripsTableSource(UITableView tableView) : base(tableView)
//		{
//		}

//		protected override BaseTableViewCell GetReusableCellFor(UITableView tableView, NSIndexPath indexPath, object item)
//		{
//			var cell = this.DequeueAndConfigureReusableCell(indexPath, item) as CompletedTripTableViewCell;

//			cell.AdjustBottom(this.Source.IndexOf(item as CompletedTripsDayItemViewModel) == this.Source.Count - 1);

//			return cell;
//		}

//		protected override void RegisterCellsForReuse()
//		{
//			this.RegisterCell(typeof(CompletedTripTableViewCell), typeof(CompletedTripsDayItemViewModel));
//		}
//	}
//}
