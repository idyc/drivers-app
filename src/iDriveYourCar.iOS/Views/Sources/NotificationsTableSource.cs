﻿using System.Collections.Generic;
using System.Windows.Input;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using Foundation;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.Cells;
using MvvmCross.Binding.ExtensionMethods;
using UIKit;

namespace iDriveYourCar.iOS.Views.Sources
{
    public class NotificationsTableSource : BaseSimpleTableSource
    {
        private readonly NotificationsViewModel viewModel;

        public NotificationsTableSource(NotificationsViewModel viewModel, UITableView tableView)
            : base(tableView)
        {
            this.viewModel = viewModel;
        }

        public ICommand RemoveRowCommand { get; set; }

        protected override BaseTableViewCell GetReusableCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            var cell = this.DequeueAndConfigureReusableCell(indexPath, item);

            if(indexPath.Item == (this.ItemsSource.Count() - 5)
               && (this.viewModel.FetchItemsTaskCompletion == null || !this.viewModel.FetchItemsTaskCompletion.IsNotCompleted))
            {
                this.viewModel.FetchItemsCommand.Execute(null);
            }

            return cell;
        }

        protected override void RegisterCellsForReuse()
        {
            this.RegisterCell<NotificationTableViewCell, NotificationItemViewModel>();
        }

        public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
        {
            return true;
        }

        public override UITableViewCellEditingStyle EditingStyleForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if(this.RemoveRowCommand.CanExecute(indexPath.Row))
            {
                return UITableViewCellEditingStyle.Delete;
            }

            return UITableViewCellEditingStyle.None;
        }

        public override UITableViewRowAction[] EditActionsForRow(UITableView tableView, NSIndexPath indexPath)
        {
            var rowActions = new List<UITableViewRowAction>();

            if(this.RemoveRowCommand.CanExecute(indexPath.Row))
            {
                rowActions.Add(UITableViewRowAction.Create(
                    UITableViewRowActionStyle.Destructive,
                    IDriveYourCarStrings.Delete,
                    (action, path) =>
                    {
                        this.RemoveRowCommand.Execute(this.viewModel.Items[indexPath.Row]);
                    }
                ));
            }

            return rowActions.ToArray();
        }
    }
}
