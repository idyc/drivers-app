﻿using DGenix.Mobile.Fwk.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using Foundation;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.Cells;
using UIKit;

namespace iDriveYourCar.iOS.Views.Sources
{
	public class UpcomingTripsTableSource : BaseSimpleTableSource
	{
		public UpcomingTripsTableSource(UITableView tableView) : base(tableView)
		{
		}

		protected override BaseTableViewCell GetReusableCellFor(UITableView tableView, NSIndexPath indexPath, object item)
		{
			return this.DequeueAndConfigureReusableCell(indexPath, item);
		}

		protected override void RegisterCellsForReuse()
		{
			this.RegisterCell(typeof(UpcomingTripTableViewCell), typeof(UpcomingTripItemViewModel));
			this.RegisterCell(typeof(DateTripTableViewCell), typeof(DateUpcomingTripItemViewModel));
		}
	}
}
