﻿using DGenix.Mobile.Fwk.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using Foundation;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.Cells;
using MvvmCross.Binding.ExtensionMethods;
using UIKit;

namespace iDriveYourCar.iOS.Views.Sources
{
    public class ReviewsTableSource : BaseSimpleTableSource
    {
        private readonly ReviewsViewModel viewModel;

        public ReviewsTableSource(ReviewsViewModel viewModel, UITableView tableView)
                    : base(tableView)
        {
            this.viewModel = viewModel;
        }

        protected override BaseTableViewCell GetReusableCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            var cell = this.DequeueAndConfigureReusableCell(indexPath, item);

            if(indexPath.Item == (this.ItemsSource.Count() - 5)
               && (this.viewModel.FetchItemsTaskCompletion == null || !this.viewModel.FetchItemsTaskCompletion.IsNotCompleted))
            {
                this.viewModel.FetchItemsCommand.Execute(null);
            }

            return cell;
        }

        protected override void RegisterCellsForReuse()
        {
            this.RegisterCell<ReviewTableViewCell, ReviewItemViewModel>();
        }
    }
}
