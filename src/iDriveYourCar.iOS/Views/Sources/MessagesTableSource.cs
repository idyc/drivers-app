﻿using DGenix.Mobile.Fwk.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using Foundation;
using iDriveYourCar.Core.ViewModels.Items.Message;
using iDriveYourCar.iOS.Views.Cells;
using UIKit;

namespace iDriveYourCar.iOS.Views.Sources
{
    public class MessagesTableSource : BaseSimpleTableSource
    {
        public MessagesTableSource(UITableView tableView) : base(tableView)
        {
        }

        protected override BaseTableViewCell GetReusableCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return this.DequeueAndConfigureReusableCell(indexPath, item);
        }

        protected override void RegisterCellsForReuse()
        {
            this.RegisterCell<MessageReceivedTableViewCell, MessageReceivedItemViewModel>();
        }
    }
}
