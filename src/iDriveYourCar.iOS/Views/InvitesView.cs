﻿using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using Foundation;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.iOS.Views;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class InvitesView : IDYCTabsMenuController<InvitesViewModel>
    {
        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Invites;

            this.topTabBar.SetItems(new NSObject[]
            {
                new NSString(IDriveYourCarStrings.ReferAClient.ToUpper()),
                new NSString(IDriveYourCarStrings.ReferADriver.ToUpper())
            });

            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.ReferAClient) as ReferAClientView);
            this.tabsViewControllers.Add(this.CreateViewControllerFor(this.ViewModel.ReferADriver) as ReferADriverView);


            this.pageViewController.SetViewControllers(new UIViewController[] { this.tabsViewControllers[0] }, UIPageViewControllerNavigationDirection.Forward, true, null);
            this.currentSelectedTab = 0;

            this.View.BringSubviewToFront(this.topTabBar);
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
        }
    }
}
