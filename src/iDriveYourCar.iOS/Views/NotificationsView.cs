﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using iDriveYourCar.iOS.Views.Sources;
using MvvmCross.Binding.BindingContext;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterRoot, true, "ic_menu")]
    public class NotificationsView : IDYCMenuController<NotificationsViewModel>
    {
        private UITableView tblView;
        private NotificationsTableSource source;

        public NotificationsView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.Messages;

            this.tblView = new UITableView
            {
                RowHeight = 75f,
                SeparatorStyle = UITableViewCellSeparatorStyle.None
            };
            this.source = new NotificationsTableSource(this.ViewModel, this.tblView);
            this.tblView.Source = this.source;

            this.View.AggregateSubviews(this.tblView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.tblView.AtLeftOf(this.View),
                this.tblView.AtTopOf(this.View),
                this.tblView.AtRightOf(this.View),
                this.tblView.AtBottomOf(this.View)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<NotificationsView, NotificationsViewModel>();
            set.Bind(this.source).To(vm => vm.Items);
            set.Bind(this.source).For(s => s.SelectionChangedCommand).To(vm => vm.PerformNavigationCommand);
            set.Bind(this.source).For(s => s.RemoveRowCommand).To(vm => vm.RemoveNotificationCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
