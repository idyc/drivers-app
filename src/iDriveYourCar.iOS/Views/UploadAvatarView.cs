﻿using System;
using iDriveYourCar.iOS.MvxSupport;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using iDriveYourCar.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;

namespace iDriveYourCar.iOS.Views
{
	[PanelPresentation(PresentationMode.CenterChild, true)]
	public class UploadAvatarView : IDYCController<UploadAvatarViewModel>
	{
		private const float MARGIN = Metrics.WINDOW_MARGIN * 2;
		private const float CONTROLS_DISTANCE = 16f;

		private IDYCLabel lblProfessionalPicture;

		private IDYCMultilineLabel lblProfessionalPictureDetail;

		private BulletLabel lblDetail1, lblDetail2, lblDetail3, lblDetail4;

		private PrimaryBackgroundButton btnUpload;

		public UploadAvatarView()
		{
		}

		public override void CreateViews()
		{
			base.CreateViews();

			this.Title = IDriveYourCarStrings.UploadPhoto;

			this.lblProfessionalPicture = new IDYCLabel(Metrics.FontSizeTitanic)
			{
				TextColor = IDYCColors.DarkGrayTextColor,
				Text = IDriveYourCarStrings.ProfessionalPicture
			};
			this.lblProfessionalPictureDetail = new IDYCMultilineLabel(Metrics.FontSizeBig)
			{
				TextColor = IDYCColors.LightGrayTextColor,
				Text = IDriveYourCarStrings.UploadAFriendlyPhotoInProfessionalDressAttire
			};
			this.lblDetail1 = new BulletLabel(IDriveYourCarStrings.UploadHeadshotPhotosOnly, Metrics.FontSizeSmall);
			this.lblDetail2 = new BulletLabel(IDriveYourCarStrings.UseASolidLightColorBackDrop, Metrics.FontSizeSmall);
			this.lblDetail3 = new BulletLabel(IDriveYourCarStrings.DressInACleanPressedSolidDarkSuitAWhiteDressShirtAndTie, Metrics.FontSizeSmall);
			this.lblDetail4 = new BulletLabel(IDriveYourCarStrings.MoreImportantUseAFriendlyPhotoWithAPositiveProfessionalImage, Metrics.FontSizeSmall);
			this.btnUpload = new PrimaryBackgroundButton(this.appConfiguration.Value);
			this.btnUpload.SetTitle(IDriveYourCarStrings.Upload.ToUpper(), UIKit.UIControlState.Normal);

			this.View.AddSubviews(
				this.lblProfessionalPicture,
				this.lblProfessionalPictureDetail,
				this.lblDetail1,
				this.lblDetail2,
				this.lblDetail3,
				this.lblDetail4,
				this.btnUpload);
			this.View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
		}

		public override void CreateConstraints()
		{
			base.CreateConstraints();

			this.View.AddConstraints(
				this.lblProfessionalPicture.AtLeftOf(this.View, MARGIN),
				this.lblProfessionalPicture.AtTopOf(this.View, MARGIN * 2),
				this.lblProfessionalPicture.AtRightOf(this.View, MARGIN),

				this.lblProfessionalPictureDetail.Below(this.lblProfessionalPicture, CONTROLS_DISTANCE),
				this.lblProfessionalPictureDetail.AtLeftOf(this.View, MARGIN),
				this.lblProfessionalPictureDetail.AtRightOf(this.View, MARGIN),

				this.lblDetail1.Below(this.lblProfessionalPictureDetail, CONTROLS_DISTANCE),
				this.lblDetail1.AtLeftOf(this.View, MARGIN),
				this.lblDetail1.AtRightOf(this.View, MARGIN),

				this.lblDetail2.Below(this.lblDetail1),
				this.lblDetail2.AtLeftOf(this.View, MARGIN),
				this.lblDetail2.AtRightOf(this.View, MARGIN),

				this.lblDetail3.Below(this.lblDetail2),
				this.lblDetail3.AtLeftOf(this.View, MARGIN),
				this.lblDetail3.AtRightOf(this.View, MARGIN),

				this.lblDetail4.Below(this.lblDetail3),
				this.lblDetail4.AtLeftOf(this.View, MARGIN),
				this.lblDetail4.AtRightOf(this.View, MARGIN),

				this.btnUpload.Below(this.lblDetail4, CONTROLS_DISTANCE),
				this.btnUpload.AtLeftOf(this.View, MARGIN),
				this.btnUpload.AtRightOf(this.View, MARGIN)
			);
		}

		public override void CreateMvxBindings()
		{
			base.CreateMvxBindings();

			var set = this.CreateBindingSet<UploadAvatarView, UploadAvatarViewModel>();
			set.Bind(this.btnUpload).To(vm => vm.UploadPictureCommand);
			set.Apply();

			this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.UploadFileTask));
		}

		public override void MvxSubscribe()
		{
			base.MvxSubscribe();
		}

		public override void MvxUnsubscribe()
		{
			base.MvxUnsubscribe();
		}
	}
}
