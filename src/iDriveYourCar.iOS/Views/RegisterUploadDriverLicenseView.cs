﻿using System.IO;
using System.Threading.Tasks;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using FFImageLoading;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class RegisterUploadDriverLicenseView : IDYCController<RegisterUploadDriverLicenseViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer, viewheaderBackground;

        private UIImageView imgDriverLicense;

        private IDYCLabel lblDriversLicense;
        private IDYCMultilineLabel lblTakeAPictureOfYourLegalDriversLicense;

        private PrimaryBackgroundButton btnUpload, btnContinue;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.DriversLicense;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = IDYCColors.BackgroundColor;
            this.viewContainer = new UIView();

            this.viewheaderBackground = new UIView { BackgroundColor = "#E1E1E0".ToUIColor() };

            this.imgDriverLicense = new UIImageView(UIImage.FromBundle(Assets.RegisterDriverLicense))
            {
                ContentMode = UIViewContentMode.ScaleAspectFit
            };

            this.lblDriversLicense = new IDYCLabel(Metrics.FontSizeHuge)
            {
                Text = IDriveYourCarStrings.DriversLicense,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblTakeAPictureOfYourLegalDriversLicense = new IDYCMultilineLabel(Metrics.FontSizeBig)
            {
                Text = IDriveYourCarStrings.TakeAPictureOfYourLegalDriversLicense,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.btnContinue = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnContinue.SetTitle(IDriveYourCarStrings.Continue.ToUpper(), UIControlState.Normal);

            this.btnUpload = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnUpload.SetTitle(IDriveYourCarStrings.TakePhoto.ToUpper(), UIControlState.Normal);
            this.btnUpload.SetImage(UIImage.FromBundle(Assets.Camera), UIControlState.Normal);
            this.btnUpload.TitleEdgeInsets = new UIEdgeInsets(0, 0, 0, this.btnUpload.ImageView.Frame.Size.Width);
            this.btnUpload.ImageEdgeInsets = new UIEdgeInsets(0, this.btnUpload.Frame.Size.Width - (this.btnUpload.ImageView.Frame.Size.Width + 15), 0, 0);

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.viewheaderBackground,
                this.lblDriversLicense,
                this.lblTakeAPictureOfYourLegalDriversLicense,
                this.btnContinue,
                this.btnUpload);
            this.viewheaderBackground.AggregateSubviews(this.imgDriverLicense);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.viewheaderBackground.AtTopOf(this.viewContainer),
                this.viewheaderBackground.WithSameWidth(this.viewContainer),
                this.viewheaderBackground.Height().EqualTo(150f),

                this.lblDriversLicense.Below(this.viewheaderBackground, CONTROLS_DISTANCE),
                this.lblDriversLicense.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblDriversLicense.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblTakeAPictureOfYourLegalDriversLicense.Below(this.lblDriversLicense, CONTROLS_DISTANCE / 2),
                this.lblTakeAPictureOfYourLegalDriversLicense.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblTakeAPictureOfYourLegalDriversLicense.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnUpload.Below(this.lblTakeAPictureOfYourLegalDriversLicense, CONTROLS_DISTANCE * 2),
                this.btnUpload.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnUpload.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnContinue.Below(this.btnUpload, CONTROLS_DISTANCE),
                this.btnContinue.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnContinue.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnContinue.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );

            this.viewheaderBackground.AddConstraints(
                this.imgDriverLicense.WithSameCenterX(this.viewheaderBackground),
                this.imgDriverLicense.WithSameCenterY(this.viewheaderBackground),

                this.imgDriverLicense.Left().GreaterThanOrEqualTo(Metrics.DEFAULT_DISTANCE).LeftOf(this.viewheaderBackground),
                this.imgDriverLicense.Top().GreaterThanOrEqualTo(Metrics.DEFAULT_DISTANCE).TopOf(this.viewheaderBackground),
                this.imgDriverLicense.Right().LessThanOrEqualTo(Metrics.DEFAULT_DISTANCE).RightOf(this.viewheaderBackground),
                this.imgDriverLicense.Bottom().LessThanOrEqualTo(Metrics.DEFAULT_DISTANCE).BottomOf(this.viewheaderBackground)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterUploadDriverLicenseView, RegisterUploadDriverLicenseViewModel>();
            set.Bind(this.btnContinue).To(vm => vm.ContinueToUploadVehicleInsuranceCommand);
            set.Bind(this.btnUpload).To(vm => vm.UploadDriverLicenseCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.TryLoadStreamImage();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if (e.PropertyName == nameof(this.ViewModel.DriverLicense))
                this.TryLoadStreamImage();
        }

        private void TryLoadStreamImage()
        {
            if (this.ViewModel.DriverLicense == null || this.ViewModel.DriverLicense.File == null)
                return;

            ImageService.Instance.LoadStream(
                        async (System.Threading.CancellationToken arg) =>
                        {
                            Stream toReturn = new MemoryStream();
                            await this.ViewModel.DriverLicense.File.CopyToAsync(toReturn);
                            toReturn.Position = 0;
                            return toReturn;
                        }).Into(this.imgDriverLicense);
        }
    }
}

