﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls.TagsView;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class CurrentTripAddExpenseView : IDYCCurrentTripController<CurrentTripAddExpenseViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;
        private UIView viewContainer;

        private TagRemoveView filename;
        private BorderedTextField txtExpenseDescription, txtExpenseAmount;
        private PrimaryTextButton btnAttachReceipt;
        private PrimaryBackgroundButton btnSubmit;

        private FluentLayout filenameCollapsedConstraint;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.AddExpense;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.txtExpenseDescription = new BorderedTextField(IDriveYourCarStrings.EnterExpenseDescription);

            this.txtExpenseAmount = new BorderedTextField(IDriveYourCarStrings.Amount);
            this.txtExpenseAmount.TextField.KeyboardType = UIKeyboardType.DecimalPad;

            this.btnAttachReceipt = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnAttachReceipt.SetTitle(IDriveYourCarStrings.AttachReceipt.ToUpper(), UIControlState.Normal);

            this.btnSubmit = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.btnSubmit.SetTitle(IDriveYourCarStrings.Submit.ToUpper(), UIControlState.Normal);

            this.filename = new TagRemoveView() { ClipsToBounds = true };
            this.filename.BackgroundColor = "#E0E0E0".ToUIColor();
            this.filename.Label.LineBreakMode = UILineBreakMode.TailTruncation;
            this.filename.Label.TextColor = UIColor.DarkTextColor;
            this.filename.Layer.CornerRadius = 10f;
            this.filename.Label.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeTiny);
            this.filename.PaddingY = 4f;
            this.filename.PaddingX = 8f;
            this.filename.RemoveButton.LineWidth = 0f;
            this.filename.RemoveButton.SetImage(UIImage.FromBundle(Assets.CancelGraySmall), UIControlState.Normal);

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);

            this.viewContainer.AggregateSubviews(
                this.txtExpenseDescription,
                this.txtExpenseAmount,
                this.filename,
                this.btnAttachReceipt,
                this.btnSubmit);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.filename.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Horizontal);
            this.btnAttachReceipt.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);

            this.filenameCollapsedConstraint = this.filename.Height().EqualTo(0).SetActive(false);

            this.viewContainer.AddConstraints(
                this.txtExpenseDescription.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),
                this.txtExpenseDescription.AtLeftOf(this.viewContainer, CONTROLS_DISTANCE),
                this.txtExpenseDescription.AtRightOf(this.viewContainer, CONTROLS_DISTANCE),

                this.txtExpenseAmount.Below(this.txtExpenseDescription, CONTROLS_DISTANCE),
                this.txtExpenseAmount.AtLeftOf(this.viewContainer, CONTROLS_DISTANCE),
                this.txtExpenseAmount.AtRightOf(this.viewContainer, CONTROLS_DISTANCE),

                this.btnAttachReceipt.Below(this.txtExpenseAmount, CONTROLS_DISTANCE),
                this.btnAttachReceipt.AtRightOf(this.viewContainer, CONTROLS_DISTANCE),

                this.filename.AtLeftOf(this.viewContainer, CONTROLS_DISTANCE),
                this.filename.Right().LessThanOrEqualTo(CONTROLS_DISTANCE * 2).LeftOf(this.btnAttachReceipt),
                this.filename.WithSameCenterY(this.btnAttachReceipt),
                this.filenameCollapsedConstraint,

                this.btnSubmit.Below(this.btnAttachReceipt, CONTROLS_DISTANCE),
                this.btnSubmit.AtLeftOf(this.viewContainer, CONTROLS_DISTANCE),
                this.btnSubmit.AtRightOf(this.viewContainer, CONTROLS_DISTANCE),
                this.btnSubmit.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<CurrentTripAddExpenseView, CurrentTripAddExpenseViewModel>();
            set.Bind(this.txtExpenseDescription.TextField).To(vm => vm.Description);
            set.Bind(this.txtExpenseAmount.TextField).To(vm => vm.Amount);
            set.Bind(this.btnAttachReceipt).To(vm => vm.AttachReceiptCommand);
            set.Bind(this.filenameCollapsedConstraint).For(v => v.Active).To(vm => vm.Filename).WithConversion(FwkValueConverters.StringEmptyToBoolConverter).Mode(MvvmCross.Binding.MvxBindingMode.OneWay);
            set.Bind(this.filename.Label).To(vm => vm.Filename);
            set.Bind(this.filename).To(vm => vm.RemoveAttachedFileCommand);
            set.Bind(this.btnSubmit).To(vm => vm.SubmitExpenseCommand);
            set.Bind(this.btnSubmit).For(v => v.Enabled).To(vm => vm.SubmitExpenseTaskCompletion.IsNotCompleted).WithConversion(FwkValueConverters.BoolToOppositeBoolConverter).WithFallback(true);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.AttachReceiptTaskCompletion));
            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SubmitExpenseTaskCompletion));
        }
    }
}
