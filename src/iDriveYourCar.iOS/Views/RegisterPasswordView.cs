﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using iDriveYourCar.Core.ViewModels.Register;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class RegisterPasswordView : IDYCController<RegisterPasswordViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer;

        private IDYCLabel lblCreateAPasswordToContinue;
        private BorderedTextField btfPassword, btfConfirmPassword;

        private PrimaryBackgroundButton btnContinue;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.SetPassword;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = IDYCColors.BackgroundColor;
            this.viewContainer = new UIView();

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);

            this.btfPassword = new BorderedTextField(IDriveYourCarStrings.Password);
            this.btfPassword.TextField.SecureTextEntry = true;

            this.btfConfirmPassword = new BorderedTextField(IDriveYourCarStrings.ConfirmPassword);
            this.btfConfirmPassword.TextField.SecureTextEntry = true;

            this.lblCreateAPasswordToContinue = new IDYCLabel(Metrics.FontSizeSmall)
            {
                Text = IDriveYourCarStrings.CreateAPasswordToContinue,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.btnContinue = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnContinue.SetTitle(IDriveYourCarStrings.Continue.ToUpper(), UIControlState.Normal);

            this.viewContainer.AggregateSubviews(
                this.lblCreateAPasswordToContinue,
                this.btfPassword,
                this.btfConfirmPassword,
                this.btnContinue);

            this.View.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.View.EndEditing(true)));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 4)
            );

            this.viewContainer.AddConstraints(
                this.lblCreateAPasswordToContinue.AtTopOf(this.viewContainer, CONTROLS_DISTANCE),
                this.lblCreateAPasswordToContinue.AtLeftOf(this.viewContainer),
                this.lblCreateAPasswordToContinue.AtRightOf(this.viewContainer),

                this.btfPassword.Below(this.lblCreateAPasswordToContinue, CONTROLS_DISTANCE),
                this.btfPassword.AtLeftOf(this.viewContainer),
                this.btfPassword.AtRightOf(this.viewContainer),

                this.btfConfirmPassword.Below(this.btfPassword, CONTROLS_DISTANCE),
                this.btfConfirmPassword.AtLeftOf(this.viewContainer),
                this.btfConfirmPassword.AtRightOf(this.viewContainer),

                this.btnContinue.Below(this.btfConfirmPassword, CONTROLS_DISTANCE),
                this.btnContinue.AtLeftOf(this.viewContainer),
                this.btnContinue.AtRightOf(this.viewContainer),
                this.btnContinue.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterPasswordView, RegisterPasswordViewModel>();
            set.Bind(this.btfPassword.TextField).To(vm => vm.RegisterDriver.Password);
            set.Bind(this.btfConfirmPassword.TextField).To(vm => vm.ConfirmPassword);
            set.Bind(this.btnContinue).To(vm => vm.ContinueCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}

