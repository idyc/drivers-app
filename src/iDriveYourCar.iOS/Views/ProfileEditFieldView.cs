﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Converters;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class ProfileEditFieldView : IDYCController<ProfileEditFieldViewModel>
    {
        private const float TEXT_FIELD_HEIGHT = 30f;
        private const float TOP_MARGIN = 50f;

        private BorderedTextField txtField;

        private UIBarButtonItem btnSave;

        public ProfileEditFieldView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = this.ViewModel.DriverEditableFieldConfiguration.Title;

            this.btnSave = new UIBarButtonItem(UIImage.FromBundle(Assets.DoneWhite), UIBarButtonItemStyle.Plain, null);
            this.NavigationItem.SetRightBarButtonItem(this.btnSave, false);

            this.txtField = new BorderedTextField(this.ViewModel.DriverEditableFieldConfiguration.Hint);

            this.View.AggregateSubviews(this.txtField);

            this.View.AddGestureRecognizer(new UITapGestureRecognizer(
                () => this.View.EndEditing(true)));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.txtField.AtTopOf(this.View, TOP_MARGIN),
                this.txtField.AtLeftOf(this.View, Metrics.WINDOW_MARGIN),
                this.txtField.AtRightOf(this.View, Metrics.WINDOW_MARGIN),
                this.txtField.Height().EqualTo(TEXT_FIELD_HEIGHT)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ProfileEditFieldView, ProfileEditFieldViewModel>();
            set.Bind(this.txtField.TextField).To(vm => vm.InputText);
            set.Bind(this.txtField.TextField).For(v => v.KeyboardType).To(vm => vm.DriverEditableFieldConfiguration.InputType).WithConversion(iOSValueConverters.CoreInputTypeToiOSInputTypeConverter);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.SaveEditFieldTaskCompletion));
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.btnSave.Clicked += this.Save;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();

            this.btnSave.Clicked -= this.Save;
        }

        private void Save(object sender, EventArgs args)
        {
            this.View.EndEditing(true);
            this.ViewModel.SaveEditFieldCommand.Execute(null);
        }
    }
}
