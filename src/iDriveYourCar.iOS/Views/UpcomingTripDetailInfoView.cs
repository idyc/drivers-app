﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings.Extensions;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.iOS.Views.CustomViews;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    public class UpcomingTripDetailInfoView : IDYCController<UpcomingTripDetailInfoViewModel>
    {
        private UIScrollView scrollView;
        private UIView viewContainer;

        private TripDetailsHeaderView headerView;
        private TripVehicleLocationView vehicleLocationView;
        private UniqueStopView pickupView, dropoffView;
        private TripDetailsStopsView<TripModificationsDetailStopView, TripStopItemViewModel> stopsView;
        private TripOfferView offerView;

        private FluentLayout vehicleLocationCollapsedConstraint, offerCollapsedConstraint;

        public override void CreateViews()
        {
            base.CreateViews();

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.headerView = new TripDetailsHeaderView();
            this.vehicleLocationView = new TripVehicleLocationView { ClipsToBounds = true };

            this.pickupView = new UniqueStopView { ClipsToBounds = true, UserInteractionEnabled = true };
            this.pickupView.Title.Text = IDriveYourCarStrings.PickUp;
            this.stopsView = new TripDetailsStopsView<TripModificationsDetailStopView, TripStopItemViewModel>();
            this.dropoffView = new UniqueStopView { ClipsToBounds = true, UserInteractionEnabled = true };
            this.dropoffView.Title.Text = IDriveYourCarStrings.DropOff;

            this.offerView = new TripOfferView { ClipsToBounds = true };

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.headerView,
                this.vehicleLocationView,
                this.pickupView,
                this.stopsView,
                this.dropoffView,
                this.offerView);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.vehicleLocationCollapsedConstraint = this.vehicleLocationView.Height().EqualTo(0).SetActive(false);
            this.offerCollapsedConstraint = this.offerView.Height().EqualTo(0).SetActive(false);

            this.viewContainer.AddConstraints(
                this.headerView.AtLeftOf(this.viewContainer),
                this.headerView.AtTopOf(this.viewContainer),
                this.headerView.AtRightOf(this.viewContainer),

                this.vehicleLocationView.Below(this.headerView),
                this.vehicleLocationView.AtLeftOf(this.viewContainer),
                this.vehicleLocationView.WithSameRight(this.headerView),
                this.vehicleLocationCollapsedConstraint,

                this.pickupView.Below(this.vehicleLocationView),
                this.pickupView.AtLeftOf(this.viewContainer),
                this.pickupView.AtRightOf(this.viewContainer),

                this.stopsView.Below(this.pickupView),
                this.stopsView.WithSameLeft(this.pickupView),
                this.stopsView.WithSameRight(this.pickupView),

                this.dropoffView.Below(this.stopsView),
                this.dropoffView.WithSameLeft(this.pickupView),
                this.dropoffView.WithSameRight(this.pickupView),

                this.offerView.Below(this.dropoffView),
                this.offerView.AtLeftOf(this.viewContainer),
                this.offerView.AtRightOf(this.viewContainer),
                this.offerView.AtBottomOf(this.viewContainer),
                this.offerCollapsedConstraint
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<UpcomingTripDetailInfoView, UpcomingTripDetailInfoViewModel>();
            set.Bind(this.headerView.Title).To(vm => vm.Trip).WithConversion(ValueConverters.TripToClientNameSurnameConverter);
            set.Bind(this.headerView.ReturnTrip).For(MvxBindings.Visibility).To(vm => vm.Trip.TripReturn).WithConversion(FwkValueConverters.VisibilityConverter);
            set.Bind(this.headerView.ReturnTrip.Tap()).For(g => g.Command).To(vm => vm.ShowReturnTripDetailsCommand);

            set.Bind(this.headerView.TripDate).To(vm => vm.PickupDate);
            set.Bind(this.headerView.TripDate).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.PickupDate));

            set.Bind(this.headerView.TripTime).To(vm => vm.PickupTime);
            set.Bind(this.headerView.TripTime).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.PickupTime));

            set.Bind(this.headerView.Footer).To(vm => vm.Trip.TripType).WithConversion(ValueConverters.TripTypeEnumToTripTypeStringConverter);

            set.Bind(this.vehicleLocationView.Text).To(vm => vm.Trip.VehicleAddress.NameCityAndStateFormatted);

            set.Bind(this.vehicleLocationView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToIconConverter, nameof(TripFieldChanged.VehicleAddressId));
            set.Bind(this.vehicleLocationView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.VehicleAddressId));
            set.Bind(this.vehicleLocationView.Text).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.VehicleAddressId));

            //pickup
            set.Bind(this.pickupView.Subtitle).To(vm => vm.Pickup);
            set.Bind(this.pickupView.Airline).To(vm => vm.Trip.FromAirline.Name);
            set.Bind(this.pickupView.Tap()).For(g => g.Command).To(vm => vm.DisplayPickupDirectionCommand);
            set.Bind(this.pickupView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip).WithConversion(ValueConverters.TripModificationsPickupToStopOrAirportIconConverter);
            set.Bind(this.pickupView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.PickupAddressId));
            set.Bind(this.pickupView.Subtitle).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.PickupAddressId));

            //stops
            set.Bind(this.stopsView.StopsStackView).For(v => v.ItemsSource).To(vm => vm.Stops);
            set.Bind(this.stopsView.StopsStackView).For(v => v.SelectionChangedCommand).To(vm => vm.DisplayStopDirectionCommand);

            //dropoff
            set.Bind(this.dropoffView.Subtitle).To(vm => vm.Dropoff);
            set.Bind(this.dropoffView.Airline).To(vm => vm.Trip.ToAirline.Name);
            set.Bind(this.dropoffView.Tap()).For(g => g.Command).To(vm => vm.DisplayDropoffDirectionCommand);
            set.Bind(this.dropoffView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip).WithConversion(ValueConverters.TripModificationsDropoffToStopOrAirportIconConverter);
            set.Bind(this.dropoffView.Title).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToTitleTextColorConverter, nameof(TripFieldChanged.DropoffAddressId));
            set.Bind(this.dropoffView.Subtitle).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.DropoffAddressId));

            //offer
            set.Bind(this.offerView.Text).To(vm => vm.Trip.Offer);
            set.Bind(this.offerView.Icon).For(MvxBindings.UIImageName).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToIconConverter, nameof(TripFieldChanged.Offer));
            set.Bind(this.offerView.Text).For(v => v.TextColor).To(vm => vm.Trip.FieldsChanged).WithConversion(ValueConverters.FieldChangedToSectionTextColorConverter, nameof(TripFieldChanged.Offer));

            set.Bind(this.vehicleLocationCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip).WithConversion(ValueConverters.TripTypeToVehicleLocationVisibilityConverter);
            set.Bind(this.offerCollapsedConstraint).For(v => v.Active).To(vm => vm.Trip.Offer).WithConversion(FwkValueConverters.StringNotEmptyToVisibilityConverter);
            set.Apply();

            this.AddNetworkActivityIndicatorBinding(this.View, nameof(this.ViewModel.LoadTask));
        }
    }
}
