﻿using System.Linq;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.MvxBindings;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views.Gestures;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class DriverUnavailabilityWeeklyCreationView : IDYCController<DriverUnavailabilityWeeklyCreationViewModel>
    {
        private const float TIME_WIDTH_MULTIPLIER = 0.43f;
        private const float TEXT_FIELD_HEIGHT = 40f;
        private const float TOP_MARGIN = 30f;

        private UIBarButtonItem btnSave;

        private UIScrollView scrollView;
        private UIView viewContainer;

        private BorderedIconTextField txtDaySelector;
        private BorderedTextField txtTimeStart, txtTimeStop;
        private IDYCLabel lblTo;

        public DriverUnavailabilityWeeklyCreationView()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.AddUnavailability;

            this.btnSave = new UIBarButtonItem(UIImage.FromBundle(Assets.DoneWhite), UIBarButtonItemStyle.Plain, null);
            this.NavigationItem.SetRightBarButtonItem(this.btnSave, false);

            this.scrollView = new UIScrollView
            {
                DelaysContentTouches = false,
                BackgroundColor = UIColor.White
            };
            this.viewContainer = new UIView();

            this.txtDaySelector = new BorderedIconTextField(IDriveYourCarStrings.Monday, Assets.ArrowDown) { UserInteractionEnabled = true };
            this.txtDaySelector.TextField.Enabled = false;

            this.txtTimeStart = new BorderedTextField { UserInteractionEnabled = true };
            this.txtTimeStart.TextField.Enabled = false;

            this.txtTimeStop = new BorderedTextField { UserInteractionEnabled = true };
            this.txtTimeStop.TextField.Enabled = false;

            this.lblTo = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.To
            };

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.txtDaySelector,
                this.txtTimeStart,
                this.txtTimeStop,
                this.lblTo);

            this.txtDaySelector.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    if(!this.ViewModel.Days.Any())
                        return;

                    var days = this.ViewModel.Days.Select(p => p.Name).ToList();
                    var modalPicker = new ModalPickerViewController(
                        ModalPickerType.Custom,
                        string.Empty,
                        this,
                        Strings.Ok,
                        Strings.Cancel,
                        UIColor.White,
                        IDYCColors.DarkGrayTextColor,
                        UIColor.White);
                    modalPicker.PickerView.Model = new CustomPickerModel(days);

                    var selectedDay = this.ViewModel.Days.First(c => c.Number == this.ViewModel.SelectedDay.Number);

                    modalPicker.PickerView.Select(days.IndexOf(selectedDay.Name), 0, true);

                    modalPicker.OnModalPickerDismissed += (s, ea) =>
                    {
                        var index = modalPicker.PickerView.SelectedRowInComponent(0);
                        this.ViewModel.SelectedDay = this.ViewModel.Days.First(c => c.Name == days[(int)index]);
                    };

                    this.PresentViewController(modalPicker, true, null);
                }));
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtTopOf(this.scrollView, Metrics.WINDOW_MARGIN),
                this.viewContainer.AtRightOf(this.scrollView, Metrics.WINDOW_MARGIN * 2),
                this.viewContainer.AtBottomOf(this.scrollView, Metrics.WINDOW_MARGIN)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View).Minus(Metrics.WINDOW_MARGIN * 4)
            );

            this.viewContainer.AddConstraints(
                this.txtDaySelector.AtLeftOf(this.viewContainer),
                this.txtDaySelector.AtTopOf(this.viewContainer, TOP_MARGIN),
                this.txtDaySelector.AtRightOf(this.viewContainer),
                this.txtDaySelector.Height().EqualTo(TEXT_FIELD_HEIGHT),

                this.txtTimeStart.Below(this.txtDaySelector, Metrics.DEFAULT_DISTANCE),
                this.txtTimeStart.AtLeftOf(this.viewContainer),
                this.txtTimeStart.WithRelativeWidth(this.viewContainer, TIME_WIDTH_MULTIPLIER),
                this.txtTimeStart.Height().EqualTo(TEXT_FIELD_HEIGHT),

                this.lblTo.WithSameCenterX(this.viewContainer),
                this.lblTo.WithSameCenterY(this.txtTimeStart),

                this.txtTimeStop.Below(this.txtDaySelector, Metrics.DEFAULT_DISTANCE),
                this.txtTimeStop.AtRightOf(this.viewContainer),
                this.txtTimeStop.WithRelativeWidth(this.viewContainer, TIME_WIDTH_MULTIPLIER),
                this.txtTimeStop.Height().EqualTo(TEXT_FIELD_HEIGHT),
                this.txtTimeStop.AtBottomOf(this.viewContainer)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<DriverUnavailabilityWeeklyCreationView, DriverUnavailabilityWeeklyCreationViewModel>();
            set.Bind(this.txtDaySelector.TextField).To(vm => vm.SelectedDay.Name);
            set.Bind(this.txtTimeStart.Tap()).For(g => g.Command).To(vm => vm.SetStartTimeCommand);
            set.Bind(this.txtTimeStop.Tap()).For(g => g.Command).To(vm => vm.SetStopTimeCommand);
            set.Bind(this.txtTimeStart.TextField).To(vm => vm.TimeStartString);
            set.Bind(this.txtTimeStop.TextField).To(vm => vm.TimeStopString);
            set.Bind(this.btnSave).For(MvxBindings.Clicked).To(vm => vm.SaveUnavailabilityTimeCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }
    }
}
