﻿using System.IO;
using System.Threading.Tasks;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using DGenix.Mobile.Fwk.iOS.Extensions;
using FFImageLoading;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.iOS.MvxSupport;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace iDriveYourCar.iOS.Views
{
    [PanelPresentation(PresentationMode.CenterChild, true)]
    public class RegisterUploadVehicleInsuranceView : IDYCController<RegisterUploadVehicleInsuranceViewModel>
    {
        private const float CONTROLS_DISTANCE = 16f;

        private UIScrollView scrollView;

        private UIView viewContainer, viewheaderBackground;

        private UIImageView imgVehicleInsurance;

        private IDYCLabel lblPersonalVehicleInsurance;
        private IDYCMultilineLabel lblTakeAPictureOfYourPersonalVehicleInsurance;

        private PrimaryBackgroundButton btnUpload, btnContinue;

        public override void CreateViews()
        {
            base.CreateViews();

            this.Title = IDriveYourCarStrings.VehicleInsurance;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.View.BackgroundColor = IDYCColors.BackgroundColor;
            this.viewContainer = new UIView();

            this.viewheaderBackground = new UIView { BackgroundColor = "#E1E1E0".ToUIColor() };

            this.imgVehicleInsurance = new UIImageView(UIImage.FromBundle(Assets.RegisterVehicleInsurance))
            {
                ContentMode = UIViewContentMode.ScaleAspectFit
            };

            this.lblPersonalVehicleInsurance = new IDYCLabel(Metrics.FontSizeHuge)
            {
                Text = IDriveYourCarStrings.PersonalVehicleInsurance,
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.lblTakeAPictureOfYourPersonalVehicleInsurance = new IDYCMultilineLabel(Metrics.FontSizeBig)
            {
                Text = IDriveYourCarStrings.TakeAPictureOfYourPersonalVehicleInsurance,
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.btnContinue = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnContinue.SetTitle(IDriveYourCarStrings.Continue.ToUpper(), UIControlState.Normal);

            this.btnUpload = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeNormal);
            this.btnUpload.SetTitle(IDriveYourCarStrings.TakePhoto.ToUpper(), UIControlState.Normal);
            this.btnUpload.SetImage(UIImage.FromBundle(Assets.Camera), UIControlState.Normal);
            this.btnUpload.TitleEdgeInsets = new UIEdgeInsets(0, 0, 0, this.btnUpload.ImageView.Frame.Size.Width);
            this.btnUpload.ImageEdgeInsets = new UIEdgeInsets(0, this.btnUpload.Frame.Size.Width - (this.btnUpload.ImageView.Frame.Size.Width + 15), 0, 0);

            this.View.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(
                this.viewheaderBackground,
                this.lblPersonalVehicleInsurance,
                this.lblTakeAPictureOfYourPersonalVehicleInsurance,
                this.btnContinue,
                this.btnUpload);
            this.viewheaderBackground.AggregateSubviews(this.imgVehicleInsurance);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.scrollView.AtLeftOf(this.View),
                this.scrollView.AtTopOf(this.View),
                this.scrollView.AtRightOf(this.View),
                this.scrollView.AtBottomOf(this.View)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.View.AddConstraints(
                this.viewContainer.WithSameWidth(this.View)
            );

            this.viewContainer.AddConstraints(
                this.viewheaderBackground.AtTopOf(this.viewContainer),
                this.viewheaderBackground.WithSameWidth(this.viewContainer),
                this.viewheaderBackground.Height().EqualTo(150f),

                this.lblPersonalVehicleInsurance.Below(this.viewheaderBackground, CONTROLS_DISTANCE),
                this.lblPersonalVehicleInsurance.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblPersonalVehicleInsurance.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.lblTakeAPictureOfYourPersonalVehicleInsurance.Below(this.lblPersonalVehicleInsurance, CONTROLS_DISTANCE / 2),
                this.lblTakeAPictureOfYourPersonalVehicleInsurance.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.lblTakeAPictureOfYourPersonalVehicleInsurance.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnUpload.Below(this.lblTakeAPictureOfYourPersonalVehicleInsurance, CONTROLS_DISTANCE * 2),
                this.btnUpload.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnUpload.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),

                this.btnContinue.Below(this.btnUpload, CONTROLS_DISTANCE),
                this.btnContinue.AtLeftOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnContinue.AtRightOf(this.viewContainer, Metrics.WINDOW_MARGIN),
                this.btnContinue.AtBottomOf(this.viewContainer, CONTROLS_DISTANCE)
            );

            this.viewheaderBackground.AddConstraints(
                this.imgVehicleInsurance.WithSameCenterX(this.viewheaderBackground),
                this.imgVehicleInsurance.WithSameCenterY(this.viewheaderBackground),

                this.imgVehicleInsurance.Left().GreaterThanOrEqualTo(Metrics.DEFAULT_DISTANCE).LeftOf(this.viewheaderBackground),
                this.imgVehicleInsurance.Top().GreaterThanOrEqualTo(Metrics.DEFAULT_DISTANCE).TopOf(this.viewheaderBackground),
                this.imgVehicleInsurance.Right().LessThanOrEqualTo(Metrics.DEFAULT_DISTANCE).RightOf(this.viewheaderBackground),
                this.imgVehicleInsurance.Bottom().LessThanOrEqualTo(Metrics.DEFAULT_DISTANCE).BottomOf(this.viewheaderBackground)
            );
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<RegisterUploadVehicleInsuranceView, RegisterUploadVehicleInsuranceViewModel>();
            set.Bind(this.btnContinue).To(vm => vm.ContinueToFinishScreenCommand);
            set.Bind(this.btnUpload).To(vm => vm.UploadVehicleInsuranceCommand);
            set.Apply();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.TryLoadStreamImage();
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if (e.PropertyName == nameof(this.ViewModel.VehicleInsurance))
                this.TryLoadStreamImage();
        }

        private void TryLoadStreamImage()
        {
            if (this.ViewModel.VehicleInsurance == null || this.ViewModel.VehicleInsurance.File == null)
                return;

            ImageService.Instance.LoadStream(
                        async (System.Threading.CancellationToken arg) =>
                        {
                            Stream toReturn = new MemoryStream();
                            await this.ViewModel.VehicleInsurance.File.CopyToAsync(toReturn);
                            toReturn.Position = 0;
                            return toReturn;
                        }).Into(this.imgVehicleInsurance);
        }
    }
}

