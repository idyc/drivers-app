﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS;
using DGenix.Mobile.Fwk.iOS;
using iDriveYourCar.Core.PushNotifications;
using iDriveYourCar.Core.Twilio;
using iDriveYourCar.iOS.PushNotifications;
using iDriveYourCar.iOS.Twilio;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;

namespace iDriveYourCar.iOS
{
    public class Setup : IDYCSetup
    {
        public Setup(MvxApplicationDelegate delg, MvxBaseIosViewPresenter presenter) : base(delg, presenter)
        {

        }

        protected override IMvxApplication CreateApp()
        {
            return new iDriveYourCar.Core.App();
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();

            Mvx.RegisterType<IPushNotificationRegisterer, PushNotificationRegisterer>();

            Mvx.LazyConstructAndRegisterSingleton<ITwilioHandler, TwilioHandler>();
        }

        protected override IEnumerable<Assembly> ValueConverterAssemblies
        {
            get
            {
                var toReturn = base.ValueConverterAssemblies.ToList();
                toReturn.Add(typeof(DateTimeToStringConverter).Assembly);
                return toReturn;
            }
        }

        protected override IEnumerable<Assembly> GetViewAssemblies()
        {
            var list = base.GetViewAssemblies().ToList();
            list.Add(typeof(BaseSetup).Assembly);
            return list;
        }
    }
}
