using System.Linq;
using Android.Content;
using DGenix.Mobile.Fwk.Droid;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid;
using iDriveYourCar.Core;
using iDriveYourCar.Core.PushNotifications;
using iDriveYourCar.Core.Twilio;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.PushNotifications;
using iDriveYourCar.Droid.Twilio;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;

namespace iDriveYourCar.Droid
{
    public class Setup : IDYCSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
            this.RootViewModels = (new[] { typeof(MainViewModel), typeof(LoginViewModel), typeof(RegisterFinishViewModel), typeof(BankingDetailsViewModel) }).ToList();
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();

            Mvx.RegisterType<IPushNotificationRegisterer, PushNotificationsRegisterer>();

            Mvx.LazyConstructAndRegisterSingleton<ITwilioHandler, TwilioHandler>();
        }

        protected override IMvxTrace CreateDebugTrace() => new DebugTrace();
    }
}