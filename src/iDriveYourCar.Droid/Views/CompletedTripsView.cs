﻿using Android.Runtime;
using Android.Support.V4.Widget;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Views
{
	[MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
	[Register("idriveyourcar.droid.views.CompletedTripsView")]
	public class CompletedTripsView : IDYCChildFragment<CompletedTripsViewModel>
	{
		public override int FragmentId => Resource.Layout.CompletedTripsView;

		public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
		{
			var view = base.OnCreateView(inflater, container, savedInstanceState);

			//var completedTripsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.completed_trips_recycler_view);
			//if (completedTripsRecyclerView != null)
			//{
			//	completedTripsRecyclerView.NestedScrollingEnabled = false;
			//}

			return view;
		}
	}
}