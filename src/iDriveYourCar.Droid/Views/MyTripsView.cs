﻿using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;
using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.MyTripsView")]
    public class MyTripsView : IDYCRootFragment<MyTripsViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.MyTripsView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private ViewPager viewPager;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.MyTrips;

            this.viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPagerTrips);
            if(viewPager != null)
            {
                var fragments = new List<MvxCachingFragmentStatePagerAdapter.FragmentInfo>();

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Upcoming,
                        typeof(UpcomingTripsView).FullName,
                        typeof(UpcomingTripsView),
                        this.ViewModel.UpcomingTrips)
                );
                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Completed,
                        typeof(CompletedTripsView).FullName,
                        typeof(CompletedTripsView),
                        this.ViewModel.CompletedTrips)
                );

                this.viewPager.OffscreenPageLimit = fragments.Count;

                this.viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(this.Activity, this.ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            if(tabLayout != null)
            {
                tabLayout.SetupWithViewPager(this.viewPager);
            }

            return view;
        }

        public override void OnResume()
        {
            if(!this.IsResumedFirstTime)
                this.ViewModel.ForceRefreshCommand.Execute(null);

            base.OnResume();
        }

        public void ResetFragment()
        {
            this.viewPager.PostDelayed(() => this.viewPager.SetCurrentItem(0, false), 100);
        }
    }
}
