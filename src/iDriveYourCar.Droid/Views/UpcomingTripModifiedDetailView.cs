﻿using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;
using DGenix.Mobile.Fwk.Droid.Support;
using Android.Widget;

namespace iDriveYourCar.Droid.Views
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.UpcomingTripModifiedDetailView")]
    public class UpcomingTripModifiedDetailView : IDYCRootFragment<UpcomingTripModifiedDetailViewModel>
    {
        public override int FragmentId => Resource.Layout.UpcomingTripModifiedDetailView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private ViewPager viewPager;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.TripModifications;

            this.viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPagerTripDetails);
            if(viewPager != null)
            {
                var fragments = new List<MvxCachingFragmentStatePagerAdapter.FragmentInfo>();

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Details,
                        typeof(UpcomingTripDetailInfoView).FullName,
                        typeof(UpcomingTripDetailInfoView),
                        this.ViewModel.UpcomingTripDetailDetails)
                );

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Notes,
                        typeof(UpcomingTripDetailNotesView).FullName,
                        typeof(UpcomingTripDetailNotesView),
                        this.ViewModel.UpcomingTripDetailNotes)
                );

                this.viewPager.OffscreenPageLimit = fragments.Count;

                this.viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(this.Activity, this.ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            if(tabLayout != null)
            {
                tabLayout.SetupWithViewPager(this.viewPager);
            }

            //If Notes fields have changes then set custom view and show icon next to NOTES title
            if(this.ViewModel.NotesHaveChanges)
            {
                var layout = new LinearLayout(this.Activity);
                layout.SetGravity(GravityFlags.Center);
                layout.SetMinimumWidth(ViewGroup.LayoutParams.MatchParent);
                layout.SetMinimumHeight(ViewGroup.LayoutParams.MatchParent);

                var imageView = new ImageView(this.Activity);
                imageView.SetMinimumWidth(ViewGroup.LayoutParams.WrapContent);
                imageView.SetMinimumHeight(ViewGroup.LayoutParams.WrapContent);
                imageView.SetImageResource(Resource.Drawable.bullet_notes);

                var textView = new TextView(this.Activity);
                var parameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                parameters.SetMargins(10, 0, 0, 0);
                textView.LayoutParameters = parameters;
                textView.Text = IDriveYourCarStrings.Notes.ToUpper();
                textView.TextAlignment = TextAlignment.Center;
                textView.TextSize = 16f;
                textView.SetTextColor(Android.Graphics.Color.White);

                layout.AddView(imageView);
                layout.AddView(textView);

                tabLayout.GetTabAt(1).SetCustomView(layout);
            }

            return view;
        }
    }
}