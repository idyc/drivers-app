﻿using Android.Runtime;
using Android.Views.Animations;
using Android.Widget;
using AndroidSwipeLayout;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.FindOpenTripsDetailView")]
    public class FindOpenTripsDetailView : IDYCFragment<FindOpenTripsDetailViewModel>
    {
        public override int FragmentId => Resource.Layout.FindOpenTripsDetailView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private SwipeLayout swipe;
        private RelativeLayout tripAcceptedOverlay;
        private ImageView imgSuccess;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.OpenTrip;

            this.tripAcceptedOverlay = view.FindViewById<RelativeLayout>(Resource.Id.rl_trip_overlay);

            this.imgSuccess = view.FindViewById<ImageView>(Resource.Id.img_success);

            this.swipe = view.FindViewById<SwipeLayout>(Resource.Id.my_swipe_button);
            this.swipe.AddDrag(SwipeLayout.DragEdge.Left, view.FindViewById<LinearLayout>(Resource.Id.back_swipe_button));
            this.swipe.RightSwipeEnabled = false;

            return view;
        }

        private void Swipe_Opened(object sender, SwipeLayout.OpenedEventArgs e)
        {
            this.ViewModel.AcceptTripCommand.Execute(null);
        }

        public override void OnResume()
        {
            base.OnResume();

            this.swipe.Opened += this.Swipe_Opened;
        }

        public override void OnPause()
        {
            base.OnPause();

            this.tripAcceptedOverlay.Visibility = Android.Views.ViewStates.Gone;
            this.swipe.Opened -= this.Swipe_Opened;
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.AcceptTripTaskCompletion) && this.ViewModel.AcceptTripTaskCompletion != null)
            {
                this.ViewModel.AcceptTripTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(this.ViewModel.AcceptTripTaskCompletion.IsSuccessfullyCompleted)
              && this.ViewModel.AcceptTripTaskCompletion.IsSuccessfullyCompleted)
            {
                this.ShowTripOverlay();
            }
            if(e.PropertyName == nameof(this.ViewModel.AcceptTripTaskCompletion.IsFaulted)
              && this.ViewModel.AcceptTripTaskCompletion.IsFaulted)
            {
                this.swipe.Close(true);
            }
        }

        private void ShowTripOverlay()
        {
            this.tripAcceptedOverlay.Visibility = Android.Views.ViewStates.Visible;

            var animationSet = new AnimationSet(false);
            int duration = 500;

            var fadeInAnimation = new AlphaAnimation(0, 1);
            fadeInAnimation.SetInterpolator(this.Context, Android.Resource.Animation.AccelerateInterpolator);
            fadeInAnimation.Duration = duration;

            var translateAnimation = new TranslateAnimation(Dimension.Absolute, 0, Dimension.Absolute, 0, Dimension.Absolute, 200, Dimension.Absolute, 0);
            translateAnimation.Duration = duration;
            translateAnimation.FillAfter = true;

            animationSet.AddAnimation(fadeInAnimation);
            animationSet.AddAnimation(translateAnimation);

            this.imgSuccess.StartAnimation(animationSet);
        }
    }
}