using System;
using Android.Graphics;
using Android.Runtime;
using Android.Support.Graphics.Drawable;
using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;
using ClinicApp.Droid.Support;
using DGenix.Mobile.Fwk.Droid.Controls;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.Adapters;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.NotificationView")]
    public class NotificationsView : IDYCFragment<NotificationsViewModel>
    {
        public override int FragmentId => Resource.Layout.NotificationsView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private MvxRecyclerView notificationsRecyclerView;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.Messages;

            notificationsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.notifications_recycler_view);
            if(notificationsRecyclerView != null)
            {
                notificationsRecyclerView.HasFixedSize = true;
                var layoutManager = new LinearLayoutManager(this.Activity);
                notificationsRecyclerView.SetLayoutManager(layoutManager);

                var onScrollListener = new XamarinRecyclerViewOnScrollListener(layoutManager);
                onScrollListener.LoadMoreEvent += (object sender, EventArgs e) =>
                {
                    if(this.ViewModel.FetchItemsTaskCompletion == null || !this.ViewModel.FetchItemsTaskCompletion.IsNotCompleted)
                        this.ViewModel.FetchItemsCommand.Execute(null);
                };
                notificationsRecyclerView.AddOnScrollListener(onScrollListener);
                notificationsRecyclerView.Adapter = new NotificationsAdapter(Activity, (IMvxAndroidBindingContext)this.BindingContext);
            }

            this.InitSwipe();

            return view;
        }

        private void InitSwipe()
        {
            var itemtouchSimpleCallbackForSwipe = new ItemTouchSimpleCallbackForSwipe(0, ItemTouchHelper.Left, this.OnSwipe);
            itemtouchSimpleCallbackForSwipe.SwipeLeftColor = Color.ParseColor("#EEEEEE");

            var deleteSvg = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_delete, this.Activity.Theme);
            itemtouchSimpleCallbackForSwipe.DeleteIcon = deleteSvg.GetBitmapDrawable(this.Resources).Bitmap;

            var itemTouchHelper = new ItemTouchHelper(itemtouchSimpleCallbackForSwipe);
            itemTouchHelper.AttachToRecyclerView(this.notificationsRecyclerView);
        }

        private bool OnSwipe(int direction, int position)
        {
            if(direction != ItemTouchHelper.Left)
                return false;

            this.notificationsRecyclerView.GetAdapter().NotifyItemChanged(position);
            this.ViewModel.RemoveNotificationCommand.Execute(this.ViewModel.Items[position]);
            return true;
        }
    }
}
