﻿using System;
using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.DriverUnavailabilityView")]
    public class DriverUnavailabilityView : IDYCRootFragment<DriverUnavailabilityViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.DriverUnavailabilityView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        protected override bool IsRoot => true;

        private ViewPager viewPager;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.Availability;

            this.viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPagerUnavailability);
            if(viewPager != null)
            {
                var fragments = new List<MvxCachingFragmentStatePagerAdapter.FragmentInfo>();

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Recurring,
                        typeof(DriverUnavailabilityWeeklyView).FullName,
                        typeof(DriverUnavailabilityWeeklyView),
                        this.ViewModel.WeeklyUnavailabilities)
                );
                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Vacation,
                        typeof(DriverUnavailabilityExtendedView).FullName,
                        typeof(DriverUnavailabilityExtendedView),
                        this.ViewModel.ExtendedUnavailabilities)
                );

                this.viewPager.OffscreenPageLimit = fragments.Count;

                this.viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(this.Activity, this.ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            if(tabLayout != null)
            {
                tabLayout.SetupWithViewPager(this.viewPager);
            }

            return view;
        }

        public void ResetFragment()
        {

        }
    }
}
