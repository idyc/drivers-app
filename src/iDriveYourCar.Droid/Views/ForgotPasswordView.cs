﻿using Android.App;
using Android.OS;
using Android.Support.Graphics.Drawable;
using Android.Views;
using Android.Widget;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.Droid.Views.MvxBindings.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;

namespace iDriveYourCar.Droid.Views
{
	[Activity(
		NoHistory = false,
		ClearTaskOnLaunch = true,
		Theme = "@style/AppTheme.Login",
		Name = "idriveyourcar.droid.views.ForgotPasswordView"
	)]
	public class ForgotPasswordView : IDYCCompatActivity<ForgotPasswordViewModel>
	{
		protected override int LayoutId => Resource.Layout.ForgotPasswordView;

		private EditText txtEmail;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			if(toolbar != null)
			{
				this.SetSupportActionBar(toolbar);
				this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
				this.SupportActionBar.Title = IDriveYourCarStrings.ResetPassword;
			}

			this.AddLoadingOverlayBinding(this, nameof(this.ViewModel.ResetPasswordTaskCompletion));
			this.AddNoInternetToastBinding(this, nameof(this.ViewModel.ResetPasswordTaskCompletion));

			this.txtEmail = this.FindViewById<EditText>(Resource.Id.txt_forgot_password);
			VectorDrawableCompat envelopeDrawable = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.envelope, this.Theme);
			this.txtEmail.SetCompoundDrawablesWithIntrinsicBounds(null, null, envelopeDrawable, null);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch(item.ItemId)
			{
				case Android.Resource.Id.Home:
					this.HideSoftKeyboard();
					this.Finish();
					return true;
			}

			return base.OnOptionsItemSelected(item);
		}
	}
}