﻿using System;
using Android.Runtime;
using Android.Support.V7.Widget;
using DGenix.Mobile.Fwk.Droid.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.Adapters;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.ReviewsView")]
    public class ReviewsView : IDYCRootFragment<ReviewsViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.ReviewsView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.CustomerReviews;

            var reviewsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.reviews_recycler_view);
            if(reviewsRecyclerView != null)
            {
                reviewsRecyclerView.NestedScrollingEnabled = false;

                //reviewsRecyclerView.HasFixedSize = true;
                var layoutManager = new LinearLayoutManager(this.Activity);
                reviewsRecyclerView.SetLayoutManager(layoutManager);

                var onScrollListener = new XamarinRecyclerViewOnScrollListener(layoutManager);
                onScrollListener.LoadMoreEvent += (object sender, EventArgs e) =>
                {
                    if(this.ViewModel.FetchItemsTaskCompletion == null || !this.ViewModel.FetchItemsTaskCompletion.IsNotCompleted)
                        this.ViewModel.FetchItemsCommand.Execute(null);
                };
                reviewsRecyclerView.AddOnScrollListener(onScrollListener);
                reviewsRecyclerView.Adapter = new ReviewsAdapter(Activity, (IMvxAndroidBindingContext)this.BindingContext);
            }
            return view;
        }

        public void ResetFragment()
        {

        }
    }
}
