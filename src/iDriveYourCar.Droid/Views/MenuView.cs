﻿using System;
using System.Threading.Tasks;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.Graphics.Drawable;
using Android.Views;
using Android.Widget;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Views;
using iDriveYourCar.Core.Converters;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.navigation_frame)]
    [Register("idriveyourcar.droid.views.MenuView")]
    public class MenuView : IDYCChildFragment<MenuViewModel>, NavigationView.IOnNavigationItemSelectedListener
    {
        private Lazy<IMvxMessenger> messenger = new Lazy<IMvxMessenger>(() => Mvx.Resolve<IMvxMessenger>());

        private MvxSubscriptionToken driverInformationUpdatedToken;

        private NavigationView navigationView;
        private ImageViewAsync imgNavigationViewHeader;
        private LinearLayout llRating;
        private TextView txtNameSurname, txtRating;
        private IMenuItem previousMenuItem;

        public override int FragmentId => Resource.Layout.MenuView;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            if(this.BaseActivity is IDrawerLayoutBaseCachingFragmentCompatActivity)
                ((IDrawerLayoutBaseCachingFragmentCompatActivity)this.BaseActivity).DrawerLayout.DrawerOpened += this.DrawerLayout_DrawerOpened;

            this.navigationView = view.FindViewById<NavigationView>(Resource.Id.navigation_view);
            this.navigationView.SetNavigationItemSelectedListener(this);

            var navigationHeader = this.navigationView.GetHeaderView(0);
            this.imgNavigationViewHeader = navigationHeader.FindViewById<ImageViewAsync>(Resource.Id.img_user_header);
            this.txtNameSurname = navigationHeader.FindViewById<TextView>(Resource.Id.text_view_fullname);
            this.llRating = navigationHeader.FindViewById<LinearLayout>(Resource.Id.llRating);
            this.txtRating = navigationHeader.FindViewById<TextView>(Resource.Id.text_reputation);

            var myTrips = this.navigationView.Menu.FindItem(Resource.Id.nav_my_trips);
            myTrips.SetTitle(IDriveYourCarStrings.MyTrips);
            myTrips.SetCheckable(false);
            myTrips.SetChecked(true);
            this.previousMenuItem = myTrips;
            var vdMyTrips = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_mytrips, this.Activity.Theme);
            myTrips.SetIcon(vdMyTrips);

            var messages = this.navigationView.Menu.FindItem(Resource.Id.nav_messages);
            messages.SetTitle(IDriveYourCarStrings.Messages);
            messages.SetCheckable(false);
            var vdMessages = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_messages, this.Activity.Theme);
            messages.SetIcon(vdMessages);
            this.SetNavItemCount(Resource.Id.nav_messages, this.ViewModel.NewMessagesCount);

            var findOpenTrips = this.navigationView.Menu.FindItem(Resource.Id.nav_find_open_trips);
            findOpenTrips.SetTitle(IDriveYourCarStrings.FindOpenTrips);
            findOpenTrips.SetCheckable(false);
            var vdFindOpenTrips = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_findopentrips, this.Activity.Theme);
            findOpenTrips.SetIcon(vdFindOpenTrips);

            var availability = this.navigationView.Menu.FindItem(Resource.Id.nav_availability);
            availability.SetTitle(IDriveYourCarStrings.Availability);
            availability.SetCheckable(false);
            var vdAvailability = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_availability, this.Activity.Theme);
            availability.SetIcon(vdAvailability);

            var myReviews = this.navigationView.Menu.FindItem(Resource.Id.nav_my_reviews);
            myReviews.SetTitle(IDriveYourCarStrings.MyReviews);
            myReviews.SetCheckable(false);
            var vdMyReviews = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_myreviews, this.Activity.Theme);
            myReviews.SetIcon(vdMyReviews);

            var invites = this.navigationView.Menu.FindItem(Resource.Id.nav_invites);
            invites.SetTitle(IDriveYourCarStrings.Invites);
            invites.SetCheckable(false);
            var vdReferrals = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_invites, this.Activity.Theme);
            invites.SetIcon(vdReferrals);

            var profile = this.navigationView.Menu.FindItem(Resource.Id.nav_profile);
            profile.SetTitle(IDriveYourCarStrings.Profile);
            profile.SetCheckable(false);
            var vdProfile = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.ic_menu_profile, this.Activity.Theme);
            profile.SetIcon(vdProfile);

            this.driverInformationUpdatedToken = this.messenger.Value.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message => this.LoadHeaderInformation());

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            if(this.ViewModel.Driver != null)
                this.LoadHeaderInformation();
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            if(previousMenuItem != null)
                previousMenuItem.SetChecked(false);

            item.SetCheckable(true);
            item.SetChecked(true);

            previousMenuItem = item;

            DGenix.Mobile.Fwk.Core.TaskExtensions.RunInSandbox(this.Navigate(item.ItemId));

            return true;
        }

        private void SetNavItemCount(int itemId, int count)
        {
            var actionView = this.navigationView.Menu.FindItem(itemId).ActionView;
            var item = actionView.FindViewById<TextView>(Resource.Id.txt_messages_counter);
            actionView.Visibility = count > 0 ? ViewStates.Visible : ViewStates.Gone;
            item.Text = count.ToString();
        }

        private async Task Navigate(int itemId)
        {
            ((MainView)Activity).DrawerLayout.CloseDrawers();

            // add a small delay to prevent any UI issues
            await Task.Delay(TimeSpan.FromMilliseconds(200));

            switch(itemId)
            {
                case Resource.Id.nav_my_trips:
                    this.ViewModel.ShowMyTripsCommand.Execute(null);
                    break;
                case Resource.Id.nav_messages:
                    this.ViewModel.ShowMessagesCommand.Execute(null);
                    break;
                case Resource.Id.nav_find_open_trips:
                    this.ViewModel.ShowFindOpenTripsCommand.Execute(null);
                    break;
                case Resource.Id.nav_availability:
                    this.ViewModel.ShowAvailabilityCommand.Execute(null);
                    break;
                case Resource.Id.nav_my_reviews:
                    this.ViewModel.ShowReviewsCommand.Execute(null);
                    break;
                case Resource.Id.nav_invites:
                    this.ViewModel.ShowInvitesCommand.Execute(null);
                    break;
                case Resource.Id.nav_profile:
                    this.ViewModel.ShowProfileCommand.Execute(null);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private void LoadHeaderInformation()
        {
            this.txtNameSurname.Text = new DriverToNameSurnameConverter().Convert(this.ViewModel.Driver, null, null, null) as string;
            this.txtRating.Text = this.ViewModel.Driver.Rating.HasValue ? this.ViewModel.Driver.Rating.ToString() : IDriveYourCarStrings.Dash;
            ImageService.Instance.LoadUrl(this.ViewModel.UserImagePath)
                .LoadingPlaceholder("@drawable/ic_profile_picture", FFImageLoading.Work.ImageSource.CompiledResource)
                .ErrorPlaceholder("@drawable/ic_profile_picture", FFImageLoading.Work.ImageSource.CompiledResource)
                .DownSampleInDip(80, 80)
                .TransformPlaceholders(true)
                .Transform(new CircleTransformation())
                .Into(imgNavigationViewHeader);

            this.SetNavItemCount(Resource.Id.nav_messages, this.ViewModel.NewMessagesCount);
        }

        public void SetCheckedItem(int menuItemResourceId)
        {
            if(this.navigationView == null)
                return;

            if(this.previousMenuItem != null)
                this.previousMenuItem.SetChecked(false);

            IMenuItem item = this.navigationView.Menu.FindItem(menuItemResourceId);
            item.SetCheckable(true);
            item.SetChecked(true);

            this.previousMenuItem = item;
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                this.driverInformationUpdatedToken?.Dispose();
                this.driverInformationUpdatedToken = null;

                if(this.BaseActivity is IDrawerLayoutBaseCachingFragmentCompatActivity)
                    ((IDrawerLayoutBaseCachingFragmentCompatActivity)this.BaseActivity).DrawerLayout.DrawerOpened -= this.DrawerLayout_DrawerOpened;
            }
            base.Dispose(disposing);
        }

        private void DrawerLayout_DrawerOpened(object sender, Android.Support.V4.Widget.DrawerLayout.DrawerOpenedEventArgs e)
        {
            this.SetNavItemCount(Resource.Id.nav_messages, this.ViewModel.NewMessagesCount);
        }
    }
}
