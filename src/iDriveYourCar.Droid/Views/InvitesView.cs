﻿using System;
using System.Collections.Generic;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.InvitesView")]
    public class InvitesView : IDYCRootFragment<InvitesViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.InvitesView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private ViewPager viewPager;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.Invites;

            this.viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPagerInvites);
            if(viewPager != null)
            {
                var fragments = new List<MvxCachingFragmentStatePagerAdapter.FragmentInfo>();

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.ReferAClient,
                        typeof(ReferAClientView).FullName,
                        typeof(ReferAClientView),
                        this.ViewModel.ReferAClient)
                );
                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.ReferADriver,
                        typeof(ReferADriverView).FullName,
                        typeof(ReferADriverView),
                        this.ViewModel.ReferADriver)
                );

                this.viewPager.OffscreenPageLimit = fragments.Count;

                this.viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(this.Activity, this.ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            if(tabLayout != null)
            {
                tabLayout.SetupWithViewPager(this.viewPager);
            }

            return view;
        }

        public void ResetFragment()
        {

        }
    }
}
