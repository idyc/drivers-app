﻿using Android.Runtime;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.Adapters;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;
using DGenix.Mobile.Fwk.Droid.Support;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using System;

namespace iDriveYourCar.Droid
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.FindOpenTripsView")]
    public class FindOpenTripsView : IDYCRootFragment<FindOpenTripsViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.FindOpenTripsView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.FindOpenTrips;

            var openTripsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.open_trips_recycler_view);
            if(openTripsRecyclerView != null)
            {
                openTripsRecyclerView.ItemTemplateSelector = new OpenTripsTemplateSelector();
                openTripsRecyclerView.NestedScrollingEnabled = false;
            }

            return view;
        }

        public void ResetFragment()
        {

        }
    }
}