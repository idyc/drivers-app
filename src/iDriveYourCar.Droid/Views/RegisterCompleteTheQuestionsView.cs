﻿using Android.App;
using Android.OS;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;

namespace iDriveYourCar.Droid.Views
{
	[Activity(
		NoHistory = false,
		ClearTaskOnLaunch = true,
		Theme = "@style/AppTheme.Login",
		Name = "idriveyourcar.droid.views.RegisterCompleteTheQuestionsView"
	)]
	public class RegisterCompleteTheQuestionsView : IDYCCompatActivity<RegisterCompleteTheQuestionsViewModel>
	{
		protected override int LayoutId => Resource.Layout.RegisterCompleteTheQuestionsView;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			if(toolbar != null)
			{
				this.SetSupportActionBar(toolbar);
				this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
				this.SupportActionBar.Title = IDriveYourCarStrings.CompleteTheQuestions;
			}
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch(item.ItemId)
			{
				case Android.Resource.Id.Home:
					this.HideSoftKeyboard();
					this.Finish();
					return true;
			}

			return base.OnOptionsItemSelected(item);
		}
	}
}