﻿using Android.Runtime;
using Android.Widget;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.UpcomingTripDetailInfoView")]
    public class UpcomingTripDetailInfoView : IDYCChildFragment<UpcomingTripDetailInfoViewModel>
    {
        public override int FragmentId => Resource.Layout.UpcomingTripDetailInfoView;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var stopsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.stops_recycler_view);
            if(stopsRecyclerView != null)
            {
                stopsRecyclerView.NestedScrollingEnabled = false;
            }

            return view;
        }
    }
}
