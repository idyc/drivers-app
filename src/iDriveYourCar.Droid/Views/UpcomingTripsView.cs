﻿using System;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using DGenix.Mobile.Fwk.Droid.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.Views.Adapters;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;
using Square.TimesSquare;
using DGenix.Mobile.Fwk.Droid.Support;

namespace iDriveYourCar.Droid.Views
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.UpcomingTripsView")]
    public class UpcomingTripsView : IDYCChildFragment<UpcomingTripsViewModel>, AbsListView.IOnScrollListener
    {
        public override int FragmentId => Resource.Layout.UpcomingTripsView;

        private CalendarPickerView calendar;

        // since 2 years are displayed, current month is number 12 (it starts counting from 0)
        private int currentDisplayedMonth = 12;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var upcomingTripsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.upcoming_trips_recycler_view);
            if(upcomingTripsRecyclerView != null)
            {
                upcomingTripsRecyclerView.ItemTemplateSelector = new UpcomingTripsTemplateSelector();
                upcomingTripsRecyclerView.NestedScrollingEnabled = false;
            }

            this.calendar = view.FindViewById<CalendarPickerView>(Resource.Id.calendar_view);

            this.calendar.SetOnScrollListener(this);

            var nextYear = DateTime.Now.AddYears(1);
            var lastYear = DateTime.Now.AddYears(-1);

            var today = DateTime.Now;

            this.calendar.DividerHeight = 0;
            this.calendar.Decorators = new[] { new SampleDecorator(this.BaseActivity, this.ViewModel) };
            this.calendar.SetCustomDayView(new DayAdapter());
            this.calendar
                .Init(today, nextYear)
                .SetShortWeekdays(new string[] { "    ", "S    ", "M    ", "T    ", "W    ", "T    ", "F    ", "S   " })
                .InMode(CalendarPickerView.SelectionMode.Single)
                .WithSelectedDate(today);

            this.calendar.DateSelected += (sender, e) =>
            {
                this.ViewModel.CurrentDate = e.Date;
                this.ViewModel.ShowListCommand.Execute(null);
            };

            return view;
        }

        public void OnScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
        {
            if(totalItemCount > 0 && firstVisibleItem != this.currentDisplayedMonth)
            {
                var numberToAdd = firstVisibleItem - this.currentDisplayedMonth;
                this.ViewModel.CurrentMonth = this.ViewModel.CurrentMonth.AddMonths(numberToAdd);
                this.currentDisplayedMonth = firstVisibleItem;
            }
        }

        public void OnScrollStateChanged(AbsListView view, [GeneratedEnum] ScrollState scrollState)
        {

        }
    }

    public class SampleDecorator : CalendarCellDecorator
    {
        private readonly BaseCachingFragmentCompatActivity activity;
        private readonly UpcomingTripsViewModel upcomingTripsViewModel;

        public SampleDecorator(BaseCachingFragmentCompatActivity activity, UpcomingTripsViewModel upcomingTripsViewModel)
        {
            this.activity = activity;
            this.upcomingTripsViewModel = upcomingTripsViewModel;
        }

        public override void Decorate(CalendarCellView cellView, DateTime date)
        {
            if(!cellView.CurrentMonth)
                cellView.DayOfMonthTextView.SetTextColor(new Android.Graphics.Color(ContextCompat.GetColor(this.activity, Resource.Color.light_gray)));
            else if(cellView.Selected || cellView.Today)
                cellView.DayOfMonthTextView.SetTextColor(Android.Graphics.Color.White);
            else
                cellView.DayOfMonthTextView.SetTextColor(new Android.Graphics.Color(ContextCompat.GetColor(this.activity, Resource.Color.dark_gray)));

            var icon = cellView.FindViewById<ImageView>(Resource.Id.day_event);
            if(icon != null)
                icon.Visibility = this.upcomingTripsViewModel.HasUpcomingTripsForDate(date) ? ViewStates.Visible : ViewStates.Invisible;

            cellView.DayOfMonthTextView.Text = date.Day.ToString();
        }
    }

    public class DayAdapter : DefaultDayViewAdapter
    {
        public override void MakeCellView(CalendarCellView parent)
        {
            var layout = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.day_view_custom, null);
            parent.AddView(layout);
            parent.DayOfMonthTextView = layout.FindViewById<TextView>(Resource.Id.day_view);
        }
    }
}