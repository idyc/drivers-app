﻿using System.IO;
using Android.App;
using Android.OS;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using FFImageLoading;
using FFImageLoading.Views;
using iDriveYourCar.Core.ViewModels;
using Android.Support.V4.Content;

namespace iDriveYourCar.Droid.Views
{
    [Activity(
        NoHistory = false,
        Theme = "@style/AppTheme.Login",
        Name = "idriveyourcar.droid.views.RegisterUploadPhotoView"
    )]
    public class RegisterUploadPhotoView : IDYCCompatActivity<RegisterUploadPhotoViewModel>
    {
        private ImageViewAsync imgAvatar;

        protected override int LayoutId => Resource.Layout.RegisterUploadPhotoView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                this.SetSupportActionBar(toolbar);
                this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                this.SupportActionBar.Title = IDriveYourCarStrings.UploadPhoto;
            }

            this.imgAvatar = this.FindViewById<ImageViewAsync>(Resource.Id.img_avatar);
        }

        protected override void OnResume()
        {
            base.OnResume();

            this.TryLoadStreamImage();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.HideSoftKeyboard();
                    this.Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if (e.PropertyName == nameof(this.ViewModel.Avatar))
                this.TryLoadStreamImage();
        }

        private void TryLoadStreamImage()
        {
            if (this.ViewModel.Avatar == null || this.ViewModel.Avatar.File == null)
            {
                this.imgAvatar.SetImageDrawable(ContextCompat.GetDrawable(this, Resource.Drawable.ic_register_photo));
                return;
            }

            ImageService.Instance.LoadStream(
                        async (System.Threading.CancellationToken arg) =>
                        {
                            Stream toReturn = new MemoryStream();
                            await this.ViewModel.Avatar.File.CopyToAsync(toReturn);
                            toReturn.Position = 0;
                            return toReturn;
                        }).Into(this.imgAvatar);
        }
    }
}