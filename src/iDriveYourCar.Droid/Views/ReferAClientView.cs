﻿using Android.Runtime;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using Android.Widget;
using Android.Views.Animations;
using Android.Content;
using Android.Views;
using Android.Support.V7.Widget;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.ReferAClientView")]
    public class ReferAClientView : IDYCChildFragment<ReferAClientViewModel>
    {
        public override int FragmentId => Resource.Layout.ReferAClientView;

        private ScrollView mainContent;
        private LinearLayout llContacts;
        private Button btnEmail, btnSendInvites;
        private ImageView imgClearContactsAndClose;
        private CardView cardFacebookShare, cardTwitterShare;

        private bool isMainContentEnabled;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.mainContent = view.FindViewById<ScrollView>(Resource.Id.main_content);
            this.llContacts = view.FindViewById<LinearLayout>(Resource.Id.contacts_selection);
            this.btnEmail = view.FindViewById<Button>(Resource.Id.btn_email);
            this.btnSendInvites = view.FindViewById<Button>(Resource.Id.btn_send_invites);
            this.imgClearContactsAndClose = view.FindViewById<ImageView>(Resource.Id.img_clear_contacts_and_close);
            this.cardFacebookShare = view.FindViewById<CardView>(Resource.Id.card_facebook_share);
            this.cardTwitterShare = view.FindViewById<CardView>(Resource.Id.card_twitter_share);

            this.btnEmail.Click += (sender, e) =>
            {
                this.isMainContentEnabled = false;
                this.ConfigureClickableContentView(this.mainContent);
                this.llContacts.Visibility = Android.Views.ViewStates.Visible;
                this.llContacts.StartAnimation(AnimationUtils.LoadAnimation(this.Context, Resource.Animation.slide_down));
            };

            this.btnSendInvites.Click += (sender, e) =>
            {
                this.ViewModel.InviteEmailFriendsCommand.Execute(null);

                this.llContacts.StartAnimation(AnimationUtils.LoadAnimation(this.Context, Resource.Animation.slide_up));
                this.llContacts.Visibility = Android.Views.ViewStates.Gone;
            };

            this.llContacts.AnimationEnd += (sender, e) =>
            {
                this.llContacts.Visibility = llContacts.Visibility == Android.Views.ViewStates.Gone ? Android.Views.ViewStates.Visible : Android.Views.ViewStates.Gone;
            };

            this.imgClearContactsAndClose.Click += (sender, e) =>
            {
                this.isMainContentEnabled = true;
                this.ConfigureClickableContentView(this.mainContent);
                this.ViewModel.ClearContactsSelectionCommand.Execute(null);

                this.llContacts.StartAnimation(AnimationUtils.LoadAnimation(this.Context, Resource.Animation.slide_up));
                this.llContacts.Visibility = Android.Views.ViewStates.Gone;
            };

            this.cardFacebookShare.Click += (sender, e) =>
            {
                var packageName = "com.facebook.katana";
                var title = "Share on Facebook";
                var subject = "Invitation Code";
                var description = $"Invitation Code: {this.ViewModel.Driver.User.ReferralCode} http://idriveyourcar.com";
                var type = "text/plain";

                var intent = this.Activity.PackageManager.GetLaunchIntentForPackage(packageName);
                if (intent != null)
                {
                    Intent shareIntent = new Intent();
                    shareIntent.SetAction(Intent.ActionSend);
                    shareIntent.SetType(type);
                    shareIntent.SetPackage(packageName);

                    shareIntent.PutExtra(Intent.ExtraTitle, title);
                    shareIntent.PutExtra(Intent.ExtraSubject, subject);
                    shareIntent.PutExtra(Intent.ExtraText, description);

                    if (shareIntent.ResolveActivity(this.Activity.PackageManager) != null)
                    {
                        this.StartActivity(shareIntent);
                    }

                }
            };

            this.cardTwitterShare.Click += (sender, e) =>
            {
                var packageName = "com.twitter.android";
                var title = "Share on Twitter";
                var subject = "Invitation Code";
                var description = $"Invitation Code: {this.ViewModel.Driver.User.ReferralCode} http://idriveyourcar.com";
                var type = "text/plain";

                var intent = this.Activity.PackageManager.GetLaunchIntentForPackage(packageName);
                if (intent != null)
                {
                    Intent shareIntent = new Intent();
                    shareIntent.SetAction(Intent.ActionSend);
                    shareIntent.SetType(type);
                    shareIntent.SetPackage(packageName);

                    shareIntent.PutExtra(Intent.ExtraTitle, title);
                    shareIntent.PutExtra(Intent.ExtraSubject, subject);
                    shareIntent.PutExtra(Intent.ExtraText, description);
                    if (shareIntent.ResolveActivity(this.Activity.PackageManager) != null)
                    {
                        this.StartActivity(shareIntent);
                    }
                }
            };

            return view;
        }

        private void ConfigureClickableContentView(View v)
        {
            if (v is ScrollView)
            {
                var scroll = (ScrollView)v;
                if (this.isMainContentEnabled)
                    scroll.Touch -= this.Scroll_Touch;
                else
                {
                    scroll.Touch -= this.Scroll_Touch;
                    scroll.Touch += this.Scroll_Touch;
                }
                var child = scroll.GetChildAt(0);
                this.ConfigureClickableContentView(child);
                return;
            }

            v.Enabled = this.isMainContentEnabled;

            if (v is ViewGroup)
            {
                var viewGroup = (ViewGroup)v;
                for (int i = 0; i < viewGroup.ChildCount; i++)
                {
                    var child = viewGroup.GetChildAt(i);
                    this.ConfigureClickableContentView(child);
                }
            }
        }

        private void Scroll_Touch(object sender, View.TouchEventArgs e)
        {
            if (!this.isMainContentEnabled) e.Handled = true;
        }
    }
}