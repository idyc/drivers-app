using System;
using Android.Runtime;
using DGenix.Mobile.Fwk.Droid.Helpers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using FFImageLoading.Transformations;
using FFImageLoading.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.Adapters;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.ProfileView")]
    public class ProfileView : IDYCRootFragment<ProfileViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.ProfileView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private MvxRecyclerView recyclerView;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.Profile;

            var imgUserProfile = view.FindViewById<ImageViewAsync>(Resource.Id.img_user_profile);
            FFImageHelper.Instance.LoadImageFromUrl(
                imgUserProfile,
                this.ViewModel.Driver.User.Avatar,
                "@drawable/ic_profile_picture",
                transformation: new CircleTransformation(),
                sampleWidth: 80,
                sampleHeight: 80);

            this.recyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.documents_recycler_view);
            if(this.recyclerView != null)
            {
                this.recyclerView.HasFixedSize = true;
                this.recyclerView.Adapter = new ProfileDocumentsAdapter((IMvxAndroidBindingContext)this.BindingContext);
            }

            return view;
        }

        public void ResetFragment()
        {

        }
    }
}
