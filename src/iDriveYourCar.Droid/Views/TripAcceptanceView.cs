using System;
using System.Threading.Tasks;
using Android.Animation;
using Android.Content.Res;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Com.Airbnb.Lottie;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.TripAcceptanceView")]
    public class TripAcceptanceView : IDYCFragment<TripAcceptanceViewModel>, Animator.IAnimatorListener, ValueAnimator.IAnimatorUpdateListener
    {
        private LottieAnimationView animationView;
        private RelativeLayout tripAcceptedOverlay, rejectTrip, acceptTrip;
        private ImageView overlayIcon;
        private LinearLayout mainContent;
        private bool onPauseCalled;

        public override int FragmentId => Resource.Layout.TripAcceptanceView;

        protected override int? ToolbarLayoutId => null;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.mainContent = view.FindViewById<LinearLayout>(Resource.Id.main_container);

            this.acceptTrip = view.FindViewById<RelativeLayout>(Resource.Id.rl_animation_internal_view);
            this.rejectTrip = view.FindViewById<RelativeLayout>(Resource.Id.rl_reject_trip);

            this.animationView = view.FindViewById<LottieAnimationView>(Resource.Id.animation_view);
            this.tripAcceptedOverlay = view.FindViewById<RelativeLayout>(Resource.Id.rl_trip_overlay);
            this.overlayIcon = view.FindViewById<ImageView>(Resource.Id.img_success);

            this.animationView.AddAnimatorListener(this);
            this.animationView.AddAnimatorUpdateListener(this);

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            this.onPauseCalled = false;
            this.animationView.ResumeAnimation();
        }

        public override void OnPause()
        {
            base.OnPause();

            this.onPauseCalled = true;
            if(this.animationView.IsAnimating)
                this.animationView.PauseAnimation();
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            this.mainContent.PostDelayed(() => this.SetTripOverlayHeightOnConfigurationChange(), 200);
        }

        #region Animator.IAnimatorListener
        public void OnAnimationCancel(Animator animation)
        {
        }

        public void OnAnimationEnd(Animator animation)
        {
            if(this.onPauseCalled)
                return;

            var anim = animation as ValueAnimator;
            if(anim != null && anim.AnimatedFraction < 1)
                return;

            this.ViewModel.TimeExpiredCommand.Execute(null);
            this.ShowTripOverlay();
        }

        public void OnAnimationRepeat(Animator animation)
        {

        }

        public void OnAnimationStart(Animator animation)
        {

        }
        #endregion

        #region ValueAnimator.IAnimatorUpdateListener
        public void OnAnimationUpdate(ValueAnimator animation)
        {
            //this.seekBar.Progress = (int)(animation.AnimatedFraction * 100);
        }
        #endregion

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.AcceptTripTaskCompletion) && this.ViewModel.AcceptTripTaskCompletion != null)
            {
                this.ViewModel.AcceptTripTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
                this.animationView.PauseAnimation();

                this.AcceptTripTaskCompletion_PropertyChanged(this, null);
            }

            if(e.PropertyName == nameof(this.ViewModel.RejectTripTaskCompletion) && this.ViewModel.RejectTripTaskCompletion != null)
            {
                this.ViewModel.RejectTripTaskCompletion.PropertyChanged += this.RejectTripTaskCompletion_PropertyChanged;
                this.animationView.PauseAnimation();

                this.RejectTripTaskCompletion_PropertyChanged(this, null);
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(this.ViewModel.AcceptTripTaskCompletion.IsSuccessfullyCompleted)
                this.ShowTripOverlay();

            if(this.ViewModel.AcceptTripTaskCompletion.IsFaulted)
                this.animationView.PauseAnimation();
        }

        private void RejectTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(this.ViewModel.RejectTripTaskCompletion.IsSuccessfullyCompleted)
                this.ShowTripOverlay();

            if(this.ViewModel.RejectTripTaskCompletion.IsFaulted)
                this.animationView.PauseAnimation();
        }

        private void ShowTripOverlay()
        {
            this.SetTripOverlayHeight();

            this.tripAcceptedOverlay.Visibility = Android.Views.ViewStates.Visible;

            this.acceptTrip.Enabled = false;
            this.rejectTrip.Enabled = false;
            this.animationView.Enabled = false;

            var animationSet = new AnimationSet(false);
            int duration = 500;

            var fadeInAnimation = new AlphaAnimation(0, 1);
            fadeInAnimation.SetInterpolator(this.Context, Android.Resource.Animation.AccelerateInterpolator);
            fadeInAnimation.Duration = duration;

            var translateAnimation = new TranslateAnimation(Dimension.Absolute, 0, Dimension.Absolute, 0, Dimension.Absolute, 200, Dimension.Absolute, 0);
            translateAnimation.Duration = duration;
            translateAnimation.FillAfter = true;

            animationSet.AddAnimation(fadeInAnimation);
            animationSet.AddAnimation(translateAnimation);

            this.overlayIcon.StartAnimation(animationSet);
        }

        private void SetTripOverlayHeight()
        {
            if(Resources.Configuration.Orientation != Android.Content.Res.Orientation.Portrait)
            {
                this.tripAcceptedOverlay.SetMinimumHeight(this.mainContent.Height * 2);
                return;
            }

            this.tripAcceptedOverlay.SetMinimumHeight(this.mainContent.Height);
            this.tripAcceptedOverlay.PostInvalidate();
        }

        private void SetTripOverlayHeightOnConfigurationChange()
        {
            if(Resources.Configuration.Orientation != Android.Content.Res.Orientation.Portrait)
                return;

            this.tripAcceptedOverlay.SetMinimumHeight(this.mainContent.Height);
            this.tripAcceptedOverlay.PostInvalidate();
        }
    }
}
