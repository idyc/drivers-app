﻿using Android.App;
using Android.OS;
using Android.Support.Graphics.Drawable;
using Android.Widget;
using DGenix.Mobile.Fwk.Droid.Views.MvxBindings.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;

namespace iDriveYourCar.Droid.Views
{
	[Activity(
		NoHistory = false,
		ClearTaskOnLaunch = true,
		Theme = "@style/AppTheme.Login",
		Name = "idriveyourcar.droid.views.LoginView"
	)]
	public class LoginView : IDYCCompatActivity<LoginViewModel>
	{
		private EditText txtUsername, txtPassword;

		protected override int LayoutId => Resource.Layout.LoginView;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			this.AddLoadingOverlayBinding(this, nameof(this.ViewModel.LoginTaskCompletion));
			this.AddNoInternetToastBinding(this, nameof(this.ViewModel.LoginTaskCompletion));
			//var set = this.CreateBindingSet<LoginView, LoginViewModel>();
			//set.Bind(this).For(MvxBindings.LoadingOverlay).To(vm => vm.LoginTask.IsNotCompleted).WithFallback(false);
			//set.Apply();

			this.txtUsername = this.FindViewById<EditText>(Resource.Id.txt_username);
			VectorDrawableCompat envelopeDrawable = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.envelope, this.Theme);
			this.txtUsername.SetCompoundDrawablesWithIntrinsicBounds(null, null, envelopeDrawable, null);

			this.txtPassword = this.FindViewById<EditText>(Resource.Id.txt_password);
			VectorDrawableCompat padlockDrawable = VectorDrawableCompat.Create(this.Resources, Resource.Drawable.padlock, this.Theme);
			this.txtPassword.SetCompoundDrawablesWithIntrinsicBounds(null, null, padlockDrawable, null);
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			this.txtPassword.EditorAction -= this.TxtPassword_EditorAction;
		}

		private void TxtPassword_EditorAction(object sender, TextView.EditorActionEventArgs e)
		{
			if(e.ActionId == Android.Views.InputMethods.ImeAction.Done
			   || (e.Event != null && e.Event.KeyCode == Android.Views.Keycode.Enter))
			{
				this.ViewModel.LoginCommand.Execute(null);
			}
		}
	}
}
