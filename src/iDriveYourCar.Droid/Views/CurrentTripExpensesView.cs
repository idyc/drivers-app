﻿using Android.Runtime;
using Android.Widget;
using AndroidSwipeLayout;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.CurrentTripExpensesView")]
    public class CurrentTripExpensesView : IDYCCurrentTripFragment<CurrentTripExpensesViewModel>
    {
        public override int FragmentId => Resource.Layout.CurrentTripExpensesView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private SwipeLayout swipe;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.Expenses;

            var expensesRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.expenses_recycler_view);
            if(expensesRecyclerView != null)
            {
                expensesRecyclerView.NestedScrollingEnabled = false;
            }

            this.swipe = view.FindViewById<SwipeLayout>(Resource.Id.my_swipe_button);
            this.swipe.AddDrag(SwipeLayout.DragEdge.Left, view.FindViewById<LinearLayout>(Resource.Id.back_swipe_button));
            this.swipe.RightSwipeEnabled = false;
            this.swipe.Opened += Swipe_Opened;

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            this.swipe.Close(true);

            this.ViewModel.ReloadCommand.Execute(null);

            //this.ViewModel.UpdateFollowingStateCommand.Execute(null);
        }

        private void Swipe_Opened(object sender, SwipeLayout.OpenedEventArgs e)
        {
            this.ViewModel.ContinueCommand.Execute(null);
        }
    }
}