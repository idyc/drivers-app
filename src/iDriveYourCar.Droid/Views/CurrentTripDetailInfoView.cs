﻿using Android.Runtime;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.CurrentTripDetailInfoView")]
    public class CurrentTripDetailInfoView : IDYCChildFragment<CurrentTripDetailInfoViewModel>
    {
        public override int FragmentId => Resource.Layout.CurrentTripDetailInfoView;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var stopsRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.stops_recycler_view);
            if(stopsRecyclerView != null)
            {
                stopsRecyclerView.NestedScrollingEnabled = false;
            }

            return view;
        }
    }
}