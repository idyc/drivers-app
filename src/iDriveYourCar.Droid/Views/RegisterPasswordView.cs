﻿using Android.App;
using Android.OS;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels.Register;

namespace iDriveYourCar.Droid.Views
{
    [Activity(
        NoHistory = false,
        Theme = "@style/AppTheme.Login",
        Name = "idriveyourcar.droid.views.RegisterPasswordView"
    )]
    public class RegisterPasswordView : IDYCCompatActivity<RegisterPasswordViewModel>
    {
        protected override int LayoutId => Resource.Layout.RegisterPasswordView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if(toolbar != null)
            {
                this.SetSupportActionBar(toolbar);
                this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                this.SupportActionBar.Title = IDriveYourCarStrings.SetPassword;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch(item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.HideSoftKeyboard();
                    this.Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}