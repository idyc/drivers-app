﻿using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;

namespace iDriveYourCar.Droid.Views
{
    [Activity(
        NoHistory = false,
        Theme = "@style/AppTheme.Login",
        Name = "idriveyourcar.droid.views.RegisterGeneralInformationView"
    )]
    public class RegisterGeneralInformationView : IDYCCompatActivity<RegisterGeneralInformationViewModel>
    {
        protected override int LayoutId => Resource.Layout.RegisterGeneralInformationView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if(toolbar != null)
            {
                this.SetSupportActionBar(toolbar);
                this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                this.SupportActionBar.Title = IDriveYourCarStrings.GeneralInformation;
            }

            var scrollView = this.FindViewById<ScrollView>(Resource.Id.register_general_info_scrollview);

            var txtAddress = this.FindViewById<EditText>(Resource.Id.txt_address);
            txtAddress.FocusChange += async (sender, e) =>
            {
                if(e.HasFocus)
                {
                    await Task.Delay(200);
                    scrollView.ScrollTo(0, scrollView.Bottom);
                }
            };
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch(item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.HideSoftKeyboard();
                    this.Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}