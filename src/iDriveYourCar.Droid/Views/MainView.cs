﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Droid.Support;
using Xamarin;
using DGenix.Mobile.Fwk.Droid.Views.MvxBindings.Extensions;

namespace iDriveYourCar.Droid.Views
{
    [Activity(
        ClearTaskOnLaunch = true,
        Theme = "@style/AppTheme",
        Name = "idriveyourcar.droid.views.MainView",
          ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.Navigation | ConfigChanges.ScreenSize | ConfigChanges.Locale)]
    public class MainView : IDYCCachingFragmentCompatActivity<MainViewModel>
    {
        private MenuView menuView;
        private MenuInflater _menuInflater;

        public override int LayoutId => Resource.Layout.MainView;

        public override int MainFragmentLayoutId => Resource.Id.content_frame;

        protected override int DrawerLayoutId => Resource.Id.drawer_layout;

        private PendingIntent stickyIntent;

        public override MenuInflater MenuInflater
        {
            get
            {
                this._menuInflater = this._menuInflater ?? new I18NMenuInflater(this);
                return this._menuInflater;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //this.stickyIntent = PendingIntent.GetActivity(this, 0, new Intent(this, typeof(CustomFirebaseMessagingService)), PendingIntentFlags.OneShot);
            //AndroidEnvironment.UnhandledExceptionRaiser += (sender, e) =>
            //{
            //    var mgr = (AlarmManager)GetSystemService(AlarmService);
            //    mgr.Set(AlarmType.Rtc, JavaSystem.CurrentTimeMillis() + 2000, this.stickyIntent);
            //    JavaSystem.Exit(2);
            //};

            this.AddLoadingOverlayBinding(this, nameof(this.ViewModel.LoadDriverDataFromServerSpinnerTaskCompletion));

            if(bundle == null)
                this.ViewModel.ShowInitialViewModelsCommand.Execute(null);
        }

        public override void OnBackPressed()
        {
            if(this.SupportFragmentManager.BackStackEntryCount > 1)
                base.OnBackPressed();
            else
                this.MoveTaskToBack(true);
        }

        protected override void OnResume()
        {
            base.OnResume();

            this.SupportFragmentManager.BackStackChanged += this.BackStack_ChangeHandler;
        }

        protected override void OnPause()
        {
            base.OnPause();

            this.SupportFragmentManager.BackStackChanged -= this.BackStack_ChangeHandler;
        }

        private void BackStack_ChangeHandler(object sender, EventArgs args)
        {
            if(this.menuView == null)
                this.menuView = this.SupportFragmentManager.FindFragmentById(Resource.Id.navigation_frame) as MenuView;

            var currentFragment = this.SupportFragmentManager.FindFragmentById(Resource.Id.content_frame) as BaseFragment;

            var currentFragmentTypeName = "Unknown!";

            switch(currentFragment?.FragmentId)
            {
                case Resource.Layout.MyTripsView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_my_trips);
                    currentFragmentTypeName = typeof(MyTripsView).Name;
                    break;
                case Resource.Layout.NotificationsView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_messages);
                    currentFragmentTypeName = typeof(NotificationsView).Name;
                    break;
                case Resource.Layout.FindOpenTripsView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_find_open_trips);
                    currentFragmentTypeName = typeof(FindOpenTripsView).Name;
                    break;
                case Resource.Layout.DriverUnavailabilityView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_availability);
                    currentFragmentTypeName = typeof(DriverUnavailabilityView).Name;
                    break;
                case Resource.Layout.ReviewsView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_my_reviews);
                    currentFragmentTypeName = typeof(ReviewsView).Name;
                    break;
                case Resource.Layout.InvitesView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_invites);
                    currentFragmentTypeName = typeof(InvitesView).Name;
                    break;
                case Resource.Layout.ProfileView:
                    this.menuView.SetCheckedItem(Resource.Id.nav_profile);
                    currentFragmentTypeName = typeof(ProfileView).Name;
                    break;
            }

            if(Insights.IsInitialized)
                Insights.Track("Droid: BackStackChanged", "Current Fragment", currentFragmentTypeName);
        }
    }
}
