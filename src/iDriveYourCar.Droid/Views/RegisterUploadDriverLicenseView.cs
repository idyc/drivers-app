﻿using Android.App;
using Android.OS;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using FFImageLoading;
using iDriveYourCar.Core.ViewModels;
using FFImageLoading.Views;
using System.IO;
using Android.Support.V4.Content;

namespace iDriveYourCar.Droid.Views
{
    [Activity(
        NoHistory = false,
        Theme = "@style/AppTheme.Login",
        Name = "idriveyourcar.droid.views.RegisterUploadDriverLicenseView"
    )]
    public class RegisterUploadDriverLicenseView : IDYCCompatActivity<RegisterUploadDriverLicenseViewModel>
    {
        protected override int LayoutId => Resource.Layout.RegisterUploadDriverLicenseView;

        private ImageViewAsync imgDriverLicense;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                this.SetSupportActionBar(toolbar);
                this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                this.SupportActionBar.Title = IDriveYourCarStrings.DriversLicense;
            }

            this.imgDriverLicense = this.FindViewById<ImageViewAsync>(Resource.Id.img_driverLicense);
        }

        protected override void OnResume()
        {
            base.OnResume();

            this.TryLoadStreamImage();
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.HideSoftKeyboard();
                    this.Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if (e.PropertyName == nameof(this.ViewModel.DriverLicense))
                this.TryLoadStreamImage();
        }

        private void TryLoadStreamImage()
        {
            if (this.ViewModel.DriverLicense == null || this.ViewModel.DriverLicense.File == null)
            {
                this.imgDriverLicense.SetImageDrawable(ContextCompat.GetDrawable(this, Resource.Drawable.ic_register_driver_license));
                return;
            }

            ImageService.Instance.LoadStream(
                        async (System.Threading.CancellationToken arg) =>
                        {
                            Stream toReturn = new MemoryStream();
                            await this.ViewModel.DriverLicense.File.CopyToAsync(toReturn);
                            toReturn.Position = 0;
                            return toReturn;
                        }).Into(this.imgDriverLicense);
        }
    }
}