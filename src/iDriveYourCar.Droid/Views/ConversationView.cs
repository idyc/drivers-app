﻿using System;
using Android.Runtime;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Support;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.ConversationView")]
    public class ConversationView : IDYCFragment<ConversationViewModel>
    {
        public override int FragmentId => Resource.Layout.ConversationView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            //this.BaseActivity.SupportActionBar.Title = this.ViewModel.DriverEditableFieldConfiguration.Title;

            return view;
        }
    }
}
