﻿using System;
using Android.Runtime;
using Android.Widget;
using AndroidSwipeLayout;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.CurrentTripSubmitView")]
    public class CurrentTripExpensesView : IDYCCurrentTripFragment<CurrentTripSubmitViewModel>
    {
        public override int FragmentId => Resource.Layout.CurrentTripSubmitView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private SwipeLayout swipe;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.TripComplete;

            this.swipe = view.FindViewById<SwipeLayout>(Resource.Id.my_swipe_button);
            this.swipe.AddDrag(SwipeLayout.DragEdge.Left, view.FindViewById<LinearLayout>(Resource.Id.back_swipe_button));
            this.swipe.RightSwipeEnabled = false;

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            this.swipe.Opened += this.Swipe_Opened;
            this.swipe.Close(true);
            //this.ViewModel.UpdateFollowingStateCommand.Execute(null);
        }

        public override void OnPause()
        {
            base.OnPause();

            this.swipe.Opened -= this.Swipe_Opened;
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.SubmitTripTaskCompletion) && this.ViewModel.SubmitTripTaskCompletion != null)
            {
                this.ViewModel.SubmitTripTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
            }

            if(e.PropertyName == nameof(this.ViewModel.FollowingActionName))
            {
                this.swipe.Close(true);
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(this.ViewModel.SubmitTripTaskCompletion.IsFaulted)
              && this.ViewModel.SubmitTripTaskCompletion.IsFaulted)
            {
                this.swipe.Close(true);
            }
        }

        private void Swipe_Opened(object sender, SwipeLayout.OpenedEventArgs e)
        {
            this.ViewModel.SubmitTripCommand.Execute(null);
        }
    }
}
