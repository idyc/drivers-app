using System;
using Android.Runtime;
using DGenix.Mobile.Fwk.Droid.Views.MvxBindings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.ContactView")]
    public class ContactView : IDYCRootFragment<ContactViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.ContactView;

        protected override bool IsRoot => true;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.ContactUs;

            var set = this.CreateBindingSet<ContactView, ContactViewModel>();

            set.Bind(this.Activity).For(MvxBindings.LoadingOverlay).To(vm => vm.SendMessageTaskCompletion.IsNotCompleted).WithFallback(false);

            set.Apply();

            return view;
        }

        public void ResetFragment()
        {

        }
    }
}