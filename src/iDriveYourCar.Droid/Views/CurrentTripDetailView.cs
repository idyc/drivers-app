﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using AndroidSwipeLayout;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;

namespace iDriveYourCar.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.CurrentTripDetailView")]
    public class CurrentTripDetailView : IDYCCurrentTripFragment<CurrentTripDetailViewModel>
    {
        private const int NOTIFICATION_ID = 3998;

        public override int FragmentId => Resource.Layout.CurrentTripDetailView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        protected override bool IsRoot => true;

        private SwipeLayout swipe;
        private ViewPager viewPager;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.CurrentTrip;

            this.viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPagerTripDetails);
            if(viewPager != null)
            {
                var fragments = new List<MvxCachingFragmentStatePagerAdapter.FragmentInfo>();

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Details,
                        typeof(CurrentTripDetailInfoView).FullName,
                        typeof(CurrentTripDetailInfoView),
                        this.ViewModel.CurrentTripDetailInfo)
                );
                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Notes,
                        typeof(CurrentTripDetailNotesView).FullName,
                        typeof(CurrentTripDetailNotesView),
                        this.ViewModel.CurrentTripDetailNotes)
                );

                this.viewPager.OffscreenPageLimit = fragments.Count;

                this.viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(this.Activity, this.ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            if(tabLayout != null)
            {
                tabLayout.SetupWithViewPager(this.viewPager);
            }

            this.swipe = view.FindViewById<SwipeLayout>(Resource.Id.my_swipe_button);
            this.swipe.AddDrag(SwipeLayout.DragEdge.Left, view.FindViewById<LinearLayout>(Resource.Id.back_swipe_button));
            this.swipe.RightSwipeEnabled = false;
            this.swipe.Opened += this.Swipe_Opened;

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            this.BaseActivity.CloseOptionsMenu();

            this.swipe.Close(true);

            this.swipe.Opened += this.Swipe_Opened;
            this.ViewModel.UpdateFollowingStateCommand.Execute(null);

            this.CancelLocalNotification();
        }

        public override void OnPause()
        {
            base.OnPause();

            this.swipe.Opened -= this.Swipe_Opened;

            if(this.ViewModel.Trip.ActiveTripState != Core.Models.ActiveTripState.EndTrip)
                this.DisplayLocalNotification();
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if(e.PropertyName == nameof(this.ViewModel.PerformWorkflowTaskCompletion) && this.ViewModel.PerformWorkflowTaskCompletion != null)
            {
                this.ViewModel.PerformWorkflowTaskCompletion.PropertyChanged += this.AcceptTripTaskCompletion_PropertyChanged;
            }

            if(e.PropertyName == nameof(this.ViewModel.FollowingActionName))
            {
                this.swipe.Close(true);
            }
        }

        private void AcceptTripTaskCompletion_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(this.ViewModel.PerformWorkflowTaskCompletion.IsFaulted)
              && this.ViewModel.PerformWorkflowTaskCompletion.IsFaulted)
            {
                this.swipe.Close(true);
            }
        }

        private void Swipe_Opened(object sender, SwipeLayout.OpenedEventArgs e)
        {
            this.ViewModel.PerformWorkflowCommand.Execute(null);
        }

        private void DisplayLocalNotification()
        {
            // When the user clicks the notification, SecondActivity will start up.
            var resultIntent = new Intent(this.Context, typeof(SplashScreen));

            // Pass some values 
            //var bundle = new Bundle();
            //bundle.PutString("tripId", this.ViewModel.Trip.Id.ToString());
            //resultIntent.PutExtras(bundle);

            var pendingIntent = PendingIntent.GetActivity(this.Context, NOTIFICATION_ID, resultIntent, PendingIntentFlags.UpdateCurrent);


            // Build the notification:
            var builder = new NotificationCompat.Builder(this.Context)
                .SetAutoCancel(true)
                .SetContentIntent(pendingIntent)
                .SetContentTitle(IDriveYourCarStrings.ReturnToTrip)
                .SetOngoing(true)
                .SetSmallIcon(Resource.Drawable.ic_notification_car)
                .SetContentText(IDriveYourCarStrings.TapToReturnToTrip);

            // Finally, publish the notification:
            var notificationManager = (NotificationManager)this.Context.GetSystemService(Context.NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, builder.Build());
        }

        private void CancelLocalNotification()
        {
            try
            {
                var notificationManager = (NotificationManager)this.Context.GetSystemService(Context.NotificationService);
                notificationManager.Cancel(NOTIFICATION_ID);
            }
            catch(System.Exception ex)
            {

            }
        }
    }
}