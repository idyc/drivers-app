﻿using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Support.V4;
using DGenix.Mobile.Fwk.Droid.Support;

namespace iDriveYourCar.Droid.Views
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.UpcomingTripDetailView")]
    public class UpcomingTripDetailView : IDYCRootFragment<UpcomingTripDetailViewModel>
    {
        public override int FragmentId => Resource.Layout.UpcomingTripDetailView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        private ViewPager viewPager;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.UpcomingTrip;

            this.viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPagerTripDetails);
            if(viewPager != null)
            {
                var fragments = new List<MvxCachingFragmentStatePagerAdapter.FragmentInfo>();

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Details,
                        typeof(UpcomingTripDetailInfoView).FullName,
                        typeof(UpcomingTripDetailInfoView),
                        this.ViewModel.UpcomingTripDetailDetails)
                );

                fragments.Add(
                    new MvxCachingFragmentStatePagerAdapter.FragmentInfo(
                        IDriveYourCarStrings.Notes,
                        typeof(UpcomingTripDetailNotesView).FullName,
                        typeof(UpcomingTripDetailNotesView),
                        this.ViewModel.UpcomingTripDetailNotes)
                );

                this.viewPager.OffscreenPageLimit = fragments.Count;

                this.viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(this.Activity, this.ChildFragmentManager, fragments);
            }

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            if(tabLayout != null)
            {
                tabLayout.SetupWithViewPager(this.viewPager);
            }

            return view;
        }
    }
}