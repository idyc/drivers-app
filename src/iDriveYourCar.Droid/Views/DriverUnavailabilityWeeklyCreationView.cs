﻿using Android.Runtime;
using Android.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
	[MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
	[Register("idriveyourcar.droid.views.DriverUnavailabilityWeeklyCreationView")]
	public class DriverUnavailabilityWeeklyCreationView : IDYCFragment<DriverUnavailabilityWeeklyCreationViewModel>, IMenuItemOnMenuItemClickListener
	{
		public override int FragmentId => Resource.Layout.DriverUnavailabilityWeeklyCreationView;

		protected override int? ToolbarLayoutId => Resource.Id.toolbar;

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState)
		{
			this.HasOptionsMenu = true;

			var view = base.OnCreateView(inflater, container, savedInstanceState);

			this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.AddUnavailability;

			return view;
		}

		public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
		{
			inflater.Inflate(Resource.Menu.menu_profile_edit_field, menu);

			var mnuSaveEditText = menu.FindItem(Resource.Id.menu_save);
			mnuSaveEditText.SetOnMenuItemClickListener(this);

			base.OnCreateOptionsMenu(menu, inflater);
		}

		public virtual bool OnMenuItemClick(IMenuItem item)
		{
			if(item.ItemId == Resource.Id.menu_save)
			{
				this.ViewModel.SaveUnavailabilityTimeCommand.Execute(null);
				return true;
			}

			return false;
		}
	}
}
