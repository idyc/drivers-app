﻿using System;
using Android.Runtime;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Support;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;

namespace iDriveYourCar.Droid.Views
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.ProfileEditTaxIdView")]
    public class ProfileEditTaxIdView : IDYCFragment<ProfileEditTaxIdViewModel>, IMenuItemOnMenuItemClickListener
    {
        public override int FragmentId => Resource.Layout.ProfileEditTaxIdView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;

            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = this.ViewModel.DriverEditableFieldConfiguration.Title;

            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.menu_profile_edit_field, menu);

            var mnuSaveEditText = menu.FindItem(Resource.Id.menu_save);
            mnuSaveEditText.SetOnMenuItemClickListener(this);

            base.OnCreateOptionsMenu(menu, inflater);
        }

        public virtual bool OnMenuItemClick(IMenuItem item)
        {
            if(item.ItemId == Resource.Id.menu_save)
            {
                this.ViewModel.SaveEditFieldCommand.Execute(null);
                return true;
            }

            return false;
        }
    }
}
