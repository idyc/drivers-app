﻿using Android.App;
using Android.OS;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using FFImageLoading.Views;
using FFImageLoading;
using System.IO;
using Android.Support.V4.Content;

namespace iDriveYourCar.Droid.Views
{
    [Activity(
        NoHistory = false,
        Theme = "@style/AppTheme.Login",
        Name = "idriveyourcar.droid.views.RegisterUploadVehicleInsuranceView"
    )]
    public class RegisterUploadVehicleInsuranceView : IDYCCompatActivity<RegisterUploadVehicleInsuranceViewModel>
    {
        protected override int LayoutId => Resource.Layout.RegisterUploadVehicleInsuranceView;

        private ImageViewAsync imgVehicleInsurance;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var toolbar = this.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                this.SetSupportActionBar(toolbar);
                this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                this.SupportActionBar.Title = IDriveYourCarStrings.VehicleInsurance;
            }

            this.imgVehicleInsurance = this.FindViewById<ImageViewAsync>(Resource.Id.img_vehicleInsurance);
        }

        protected override void OnResume()
        {
            base.OnResume();

            this.TryLoadStreamImage();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.HideSoftKeyboard();
                    this.Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.ViewModel_PropertyChanged(sender, e);

            if (e.PropertyName == nameof(this.ViewModel.VehicleInsurance))
                this.TryLoadStreamImage();
        }

        private void TryLoadStreamImage()
        {
            if (this.ViewModel.VehicleInsurance == null || this.ViewModel.VehicleInsurance.File == null)
            {
                this.imgVehicleInsurance.SetImageDrawable(ContextCompat.GetDrawable(this, Resource.Drawable.ic_register_vehicle_insurance));
                return;
            }

            ImageService.Instance.LoadStream(
                        async (System.Threading.CancellationToken arg) =>
                        {
                            Stream toReturn = new MemoryStream();
                            await this.ViewModel.VehicleInsurance.File.CopyToAsync(toReturn);
                            toReturn.Position = 0;
                            return toReturn;
                        }).Into(this.imgVehicleInsurance);
        }
    }
}