﻿using System;
using System.Collections.Generic;
using MvvmCross.Droid.Support.V7.RecyclerView.ItemTemplates;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Droid.Views.Adapters
{
	public class UpcomingTripsTemplateSelector : IMvxTemplateSelector
	{
		private readonly Dictionary<Type, int> openTripsTypeDictionary = new Dictionary<Type, int>
		{
			[typeof(UpcomingTripItemViewModel)] = Resource.Layout.item_upcoming_trip,
			[typeof(DateUpcomingTripItemViewModel)] = Resource.Layout.item_date,
		};

		public int GetItemLayoutId(int fromViewType)
		{
			return fromViewType;
		}

		public int GetItemViewType(object forItemObject)
		{
			return this.openTripsTypeDictionary[forItemObject.GetType()];
		}
	}
}
