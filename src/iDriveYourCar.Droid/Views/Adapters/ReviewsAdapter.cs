﻿using System.Collections.ObjectModel;
using Android.App;
using Android.Support.V7.Widget;
using Android.Views;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Adapters
{
    public class ReviewsAdapter : MvxRecyclerAdapter
    {
        public ObservableCollection<ReviewItemViewModel> Source => this.ItemsSource as ObservableCollection<ReviewItemViewModel>;

        public ReviewsAdapter(Activity activity, IMvxAndroidBindingContext context)
                    : base(context)
        {
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, this.BindingContext.LayoutInflaterHolder);

            var viewHolder = new ReviewViewHolder(this.InflateViewForHolder(parent, viewType, itemBindingContext), itemBindingContext);

            if(viewHolder == null)
                return null;

            viewHolder.Click = this.ItemClick;
            viewHolder.LongClick = this.ItemLongClick;

            return viewHolder;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);

            if(holder.GetType().Equals(typeof(ReviewViewHolder)))
            {
                var reviewViewHolder = holder as ReviewViewHolder;
                var reviewItem = this.Source[position] as ReviewItemViewModel;
            }
        }
    }
}