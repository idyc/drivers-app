﻿using System.Collections.ObjectModel;
using Android.Support.V7.Widget;
using Android.Views;
using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Views;
using FFImageLoading.Work;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Adapters
{
	public class ProfileDocumentsAdapter : MvxRecyclerAdapter
	{
		public ObservableCollection<DocumentItemViewModel> Source => this.ItemsSource as ObservableCollection<DocumentItemViewModel>;

		public ProfileDocumentsAdapter(IMvxAndroidBindingContext context) : base(context)
		{
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			var itemBindingContext = new MvxAndroidBindingContext(parent.Context, this.BindingContext.LayoutInflaterHolder);
			return new ProfileDocumentViewHolder(InflateViewForHolder(parent, viewType, itemBindingContext), itemBindingContext)
			{
				Click = ItemClick,
				LongClick = ItemLongClick
			};
		}

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			base.OnBindViewHolder(holder, position);

			var profileDocumentViewHolder = holder as ProfileDocumentViewHolder;

			profileDocumentViewHolder.ImgDocumentLoadingWork?.Cancel();

			profileDocumentViewHolder.ImgDocumentLoadingWork = ImageService.Instance.LoadUrl(this.Source[position].DocumentImagePath)
				.LoadingPlaceholder("@drawable/ic_profile_document", ImageSource.CompiledResource)
				.ErrorPlaceholder("@drawable/ic_profile_document", ImageSource.CompiledResource)
				.DownSampleInDip(100, 100)
				.TransformPlaceholders(true)
				.Transform(new CircleTransformation())
				.Into(profileDocumentViewHolder.ImgDocument);
		}
	}

	public class ProfileDocumentViewHolder : MvxRecyclerViewHolder
	{
		public ImageViewAsync ImgDocument { get; set; }

		public IScheduledWork ImgDocumentLoadingWork { get; set; }

		public ProfileDocumentViewHolder(View itemView, IMvxAndroidBindingContext context)
			: base(itemView, context)
		{
			this.ImgDocument = itemView.FindViewById<ImageViewAsync>(Resource.Id.img_document);
		}
	}
}

