﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Droid.Support.V7.RecyclerView.ItemTemplates;

namespace iDriveYourCar.Droid.Adapters
{
	public class OpenTripsTemplateSelector : IMvxTemplateSelector
	{
		private readonly Dictionary<Type, int> openTripsTypeDictionary = new Dictionary<Type, int>
		{
			[typeof(OpenTripItemViewModel)] = Resource.Layout.item_open_trip,
			[typeof(DateOpenTripItemViewModel)] = Resource.Layout.item_date,
		};

		public int GetItemLayoutId(int fromViewType)
		{
			return fromViewType;
		}

		public int GetItemViewType(object forItemObject)
		{
			return this.openTripsTypeDictionary[forItemObject.GetType()];
		}
	}
}
