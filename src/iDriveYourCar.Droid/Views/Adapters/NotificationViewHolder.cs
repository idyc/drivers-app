﻿using Android.Views;
using FFImageLoading.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Adapters
{
    public class NotificationViewHolder : MvxRecyclerViewHolder
    {
        public ImageViewAsync UserImage { get; set; }

        public NotificationViewHolder(View itemView, IMvxAndroidBindingContext context)
        : base(itemView, context)
        {
            this.UserImage = itemView.FindViewById<ImageViewAsync>(Resource.Id.img_user);
        }
    }
}