﻿using System.Collections.ObjectModel;
using Android.App;
using Android.Support.V7.Widget;
using Android.Views;
using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace iDriveYourCar.Droid.Adapters
{
    public class NotificationsAdapter : MvxRecyclerAdapter
    {
        public ObservableCollection<NotificationItemViewModel> Source => this.ItemsSource as ObservableCollection<NotificationItemViewModel>;

        public NotificationsAdapter(Activity activity, IMvxAndroidBindingContext context)
            : base(context)
        {
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, this.BindingContext.LayoutInflaterHolder);

            var viewHolder = new NotificationViewHolder(this.InflateViewForHolder(parent, viewType, itemBindingContext), itemBindingContext);

            if(viewHolder == null)
                return null;

            viewHolder.Click = this.ItemClick;
            viewHolder.LongClick = this.ItemLongClick;

            return viewHolder;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);

            if(holder.GetType().Equals(typeof(NotificationViewHolder)))
            {
                var notificationViewHolder = holder as NotificationViewHolder;
                var notificationItem = this.Source[position] as NotificationItemViewModel;

                ImageService.Instance.LoadUrl(notificationItem.Notification.Image)
                    .LoadingPlaceholder("@drawable/ic_profile_picture", ImageSource.CompiledResource)
                    .ErrorPlaceholder("@drawable/ic_profile_picture", ImageSource.CompiledResource)
                    .DownSampleInDip(40, 40)
                    .TransformPlaceholders(true)
                    .Transform(new CircleTransformation())
                    .Into(notificationViewHolder.UserImage);
            }
        }
    }
}