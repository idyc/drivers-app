﻿using Android.Runtime;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using DGenix.Mobile.Fwk.Droid.Support;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using System;

namespace iDriveYourCar.Droid.Views
{
    [MvxReplaceFragmentAndViewModel]
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("idriveyourcar.droid.views.FilterFindOpenTripsView")]
    public class FilterFindOpenTripsView : IDYCRootFragment<FilterFindOpenTripsViewModel>, IIDYCRootFragment
    {
        public override int FragmentId => Resource.Layout.FilterFindOpenTripsView;

        protected override int? ToolbarLayoutId => Resource.Id.toolbar;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            this.BaseActivity.SupportActionBar.Title = IDriveYourCarStrings.FilterTrips;

            return view;
        }

        public void ResetFragment()
        {

        }
    }
}