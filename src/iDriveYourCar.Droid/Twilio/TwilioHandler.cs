﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Twilio;
using Twilio.IPMessaging;
using Plugin.CurrentActivity;
using Twilio.Common;

namespace iDriveYourCar.Droid.Twilio
{
    public class TwilioHandler : ITwilioHandler, AccessManager.IListener
    {
        IPMessagingClient client;

        public TwilioHandler()
        {
        }

        public IntPtr Handle => IntPtr.Zero;

        public event EventHandler<TwilioMessageReceivedEventArgs> MessageReceived;

        public void Dispose()
        {
        }

        public Task<IList<ChatMessage>> GetMessagesAsync(int startIndex, int pageSize, string channel)
        {
            var tcs = new TaskCompletionSource<IList<ChatMessage>>();

            //var targetChannel = client.Channels.ChannelWithUniqueName(channel);

            //if(startIndex == 0)
            //{
            //    targetChannel.Messages.GetLastMessagesWithCount(
            //        (nuint)pageSize,
            //        (c, messages) =>
            //    {
            //        if(c.IsSuccessful)
            //        {
            //            var res = messages.Select(m => new ChatMessage { Body = m.Body, Author = m.Author, DateTime = m.DateUpdatedAsDate.ToDateTime() }).ToList();
            //            tcs.SetResult(res);
            //        }
            //        else
            //        {
            //            tcs.SetException(new Exception(c.Error.Description));
            //        }
            //    });
            //}
            //else
            //{
            //    targetChannel.Messages.GetMessagesAfter(
            //        (nuint)startIndex,
            //        (nuint)pageSize,
            //        (c, messages) =>
            //    {
            //        if(c.IsSuccessful)
            //        {
            //            var res = messages.Select(m => new ChatMessage { Body = m.Body, Author = m.Author, DateTime = m.DateUpdatedAsDate.ToDateTime() }).ToList();
            //            tcs.SetResult(res);
            //        }
            //        else
            //        {
            //            tcs.SetException(new Exception(c.Error.Description));
            //        }
            //    });
            //}

            return tcs.Task;
        }

        public Task Initialize(string token)
        {
            if(this.client == null)
            {
                var currentActivity = CrossCurrentActivity.Current.Activity;

                var accessManager = new AccessManager(currentActivity, token, this);

                var properties = new IPMessagingClient.Properties.Builder().SetSynchronizationStrategy(IPMessagingClient.SynchronizationStrategy.All).CreateProperties();

                try
                {

                    this.client = IPMessagingClient.Create(currentActivity, accessManager, properties, new CustomTwilioClientListener() as Java.Lang.Object);
                }
                catch(Exception ex)
                {

                }
            }
            return Task.FromResult(0);
        }

        public void OnError(AccessManager p0, string p1)
        {
        }

        public void OnTokenExpired(AccessManager p0)
        {
        }

        public void OnTokenUpdated(AccessManager p0)
        {
        }

        public Task<bool> TryDeleteChannelAsync(string channel)
        {
            var tcs = new TaskCompletionSource<bool>();

            this.client.Channels.GetChannelByUniqueName(channel).Destroy(new CustomConstantsStatusListener(tcs));

            return tcs.Task;
        }

        public Task<bool> TryJoinOrCreateChannelAsync(string channel)
        {
            var tcs = new TaskCompletionSource<bool>();

            var targetChannel = client.Channels.GetChannelByUniqueName(channel);
            if(targetChannel != null)
            {
                targetChannel.Join(new CustomConstantsStatusListener(tcs));
            }
            else
            {
                //var options = new NSDictionary($"{channel} Friendly Name", channel, "TWMChannelOptionType", 0);

                //client.Channels.CreateChannel().CreateChannelWithOptions(options, (creationResult, ch) =>
                //{
                //    if(creationResult.IsSuccessful)
                //    {
                //        targetChannel = ch;

                //        targetChannel.JoinWithCompletion(
                //            c =>
                //            {
                //                tcs.SetResult(true);
                //                //targetChannel.SetUniqueName("general", res => { });
                //            });
                //    }
                //    else
                tcs.SetResult(false);
                //});
            }

            return tcs.Task;
        }

        public Task<bool> TrySendMessageAsync(string channel, string text)
        {
            var tcs = new TaskCompletionSource<bool>();

            var targetChannel = this.client.Channels.GetChannelByUniqueName(channel);
            if(targetChannel == null)
                return Task.FromResult(false);

            var msg = targetChannel.Messages.CreateMessage(text);
            targetChannel.Messages.SendMessage(
                msg,
                new CustomConstantsStatusListener(tcs));

            return tcs.Task;
        }
    }
}
