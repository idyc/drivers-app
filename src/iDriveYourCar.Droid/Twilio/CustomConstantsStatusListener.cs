﻿using System;
using Twilio.IPMessaging;
using System.Threading.Tasks;
namespace iDriveYourCar.Droid.Twilio
{
    public class CustomConstantsStatusListener : ConstantsStatusListener
    {
        private TaskCompletionSource<bool> tcs;

        public CustomConstantsStatusListener(TaskCompletionSource<bool> tcs)
        {
            this.tcs = tcs;
        }

        public override void OnError(ErrorInfo errorInfo)
        {
            tcs.SetResult(false);
        }

        public override void OnSuccess()
        {
            tcs.SetResult(true);
        }
    }
}
