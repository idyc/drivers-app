﻿using System;
using Android.Content;
using Java.Interop;
using Twilio.Common;
namespace iDriveYourCar.Droid.Twilio
{
    public class CustomTwilioAccessManagerListener : AccessManagerListener
    {
        public CustomTwilioAccessManagerListener(Context context, string token)
        : base(context, token)
        {
        }

        //public IntPtr Handle
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        //public void Dispose()
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnError(AccessManager p0, string p1)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnTokenExpired(AccessManager p0)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnTokenUpdated(AccessManager p0)
        //{
        //    //throw new NotImplementedException();
        //}

        [Export("onError")]
        public new void OnError(AccessManager accessManager, string msg)
        {

        }

        [Export("onTokenExpired")]
        public new void OnTokenExpired(AccessManager accessManager)
        {

        }

        [Export("onTokenUpdated")]
        public new void OnTokenUpdated(AccessManager accessManager)
        {

        }
    }
}
