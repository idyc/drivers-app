using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.Droid.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}