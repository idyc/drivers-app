using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.Droid.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}