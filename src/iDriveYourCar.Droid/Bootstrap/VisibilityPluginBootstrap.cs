using MvvmCross.Platform.Plugins;

namespace iDriveYourCar.Droid.Bootstrap
{
    public class VisibilityPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Visibility.PluginLoader>
    {
    }
}