﻿using System;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;

namespace ClinicApp.Droid.Support
{
    public class ItemTouchSimpleCallbackForSwipe : ItemTouchHelper.SimpleCallback
    {
        private Func<int, int, bool> onSwipe;
        private Paint p = new Paint();

        public ItemTouchSimpleCallbackForSwipe(int dragDirs, int swipeDirs, Func<int, int, bool> onSwipe)
            : base(dragDirs, swipeDirs)
        {
            this.onSwipe = onSwipe;
        }

        public ItemTouchSimpleCallbackForSwipe(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        // Properties
        public Bitmap DeleteIcon { get; set; }

        public Color SwipeLeftColor { get; set; }

        // Methods
        public override bool OnMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
        {
            return false;
        }

        public override void OnMoved(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int fromPos, RecyclerView.ViewHolder target, int toPos, int x, int y)
        {
            base.OnMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
        }

        public override void OnSwiped(RecyclerView.ViewHolder viewHolder, int direction)
        {
            if(!this.onSwipe.Invoke(direction, viewHolder.AdapterPosition))
                return;
        }

        public override void OnChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, bool isCurrentlyActive)
        {
            if(actionState == ItemTouchHelper.ActionStateSwipe)
            {
                var itemView = viewHolder.ItemView;
                float height = (float)itemView.Bottom - (float)itemView.Top;
                float width = height / 3;

                if(dX < 0)
                {
                    this.p.Color = this.SwipeLeftColor;
                    RectF background = new RectF((float)itemView.Right + dX, (float)itemView.Top, (float)itemView.Right, (float)itemView.Bottom);
                    c.DrawRect(background, this.p);
                    RectF iconDest = new RectF((float)itemView.Right - 2 * width, (float)itemView.Top + width, (float)itemView.Right - width, (float)itemView.Bottom - width);
                    c.DrawBitmap(this.DeleteIcon, null, iconDest, p);
                }

                if(dX == 0)
                {
                    var cleanPaint = new Paint();
                    cleanPaint.Color = Color.Transparent;
                    c.DrawRect(new RectF((float)itemView.Right, (float)itemView.Top, (float)itemView.Right, (float)itemView.Bottom), cleanPaint);
                }
            }

            base.OnChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    }
}