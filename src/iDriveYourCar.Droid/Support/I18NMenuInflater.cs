﻿using Android.Content;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Support;

namespace iDriveYourCar.Droid.Support
{
    public class I18NMenuInflater : IDYCI18NMenuInflater
    {
        public I18NMenuInflater(Context context)
            : base(context)
        {
        }

        protected override void RegisterMenus()
        {
            base.RegisterMenus();

            this.RegisterMenuWithMenuItems(Resource.Menu.menu_profile_edit_field, Resource.Id.menu_save);
            this.RegisterMenuWithMenuItems(
                Resource.Menu.toolbar_current_trip,
                Resource.Id.menu_call_passenger,
                Resource.Id.menu_message_passenger,
                Resource.Id.menu_add_expense,
                Resource.Id.menu_call_dispatch,
                Resource.Id.menu_message_dispatch);
        }
    }
}