﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Xamarin;

namespace iDriveYourCar.Droid
{
#if DEBUG
    [Application(Debuggable = true)]
#else
    [Application(Debuggable = false)]
#endif
    public class MainApplication : DGenix.Mobile.Fwk.Droid.MainApplication, Application.IActivityLifecycleCallbacks
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer)
            : base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            InitializeInsights(this);
        }

        public static void InitializeInsights(Context context)
        {
            if(!Insights.IsInitialized)
            {
                Log.Error("iDriveYourCar", "Insights Initialized");

#if DEBUG
                Insights.Initialize(Insights.DebugModeKey, context);
#else
                Insights.Initialize("3703be3df45ed3076d9b64cf3591d0f050d09ac1", context);
#endif
            }
        }
    }
}