﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using iDriveYourCar.Core.Helpers;
using MvvmCross.Droid.Services;
using MvvmCross.Platform;
using Xamarin;
using iDriveYourCar.Core.PushNotifications;
using Android.Util;

namespace iDriveYourCar.Droid.PushNotifications
{
    [Service]
    public class GeopositionService : MvxIntentService
    {
        public GeopositionService() : base("anyname")
        {
        }

        protected override void OnHandleIntent(Intent intent)
        {
            base.OnHandleIntent(intent);

            Log.Info("iDriveYourCar", $"GeopositionService started");

            var memberId = intent.GetLongExtra(PushConstants.STAFF_MEMBER_ID, default(long));

            try
            {
                this.RunService(memberId).Wait();
            }
            catch(Exception e)
            {
                if(Insights.IsInitialized)
                    Insights.Report(e, "Geoposition service", "Exception in RunService");
            }

        }

        private async Task RunService(long operatorId)
        {
            var locationHelper = Mvx.Resolve<IDriverLocationHelper>();

            await locationHelper.SendDriverLocationToServerAsync(operatorId);

            Log.Info("iDriveYourCar", $"Service success: Operator: {operatorId}");
        }
    }
}
