﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Support.V4.App;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Droid.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core;
using Firebase.Messaging;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.PushNotifications;
using MvvmCross.Plugins.Json;
using Newtonsoft.Json;
using Plugin.CurrentActivity;
using iDriveYourCar.Core.Models.Push;
using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using Android.Util;

namespace iDriveYourCar.Droid.PushNotifications
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class CustomFirebaseMessagingService : FirebaseMessagingService
    {
        private Lazy<IIDYCSettings> settings = new Lazy<IIDYCSettings>(() => new IDYCSettings());

        const string TAG = "MyFirebaseMsgService";

        public override void OnMessageReceived(RemoteMessage message)
        {
            var hint = new StartupHint();

            if(!message.Data.Keys.Any() || !message.Data.ContainsKey(PushConstants.CODE))
                return;

            var pushType = EnumExtensions.GetEnumValueByCode<PushTypes>(message.Data[PushConstants.CODE]);
            Log.Info("iDriveYourCar", $"Push received {pushType}");

            if(pushType == PushTypes.LocationRequest)
            {
                var staffMemberId = long.Parse(message.Data[PushConstants.STAFF_MEMBER_ID]);
                this.LaunchGeopositionService(staffMemberId);
                return;
            }

            this.settings.Value.PushNotificationsCounter++;

            hint.Title = message.Data[PushConstants.TITLE];
            hint.Body = message.Data[PushConstants.BODY];

            switch(pushType)
            {
                case PushTypes.DriverSignedUp:
                case PushTypes.ClientSignedUp:
                    hint.InitialScreen = InitialScreen.Referrals;
                    break;

                case PushTypes.AccountActivated:
                case PushTypes.InPersonTrainingSchedule:
                case PushTypes.InPersonTrainingStartsSoon:
                    hint.InitialScreen = InitialScreen.MyTrips;
                    break;

                case PushTypes.NewTripAvailable:
                case PushTypes.NewTripAvailableDriverRequested:
                case PushTypes.NewTripAccepted:
                    hint.InitialScreen = InitialScreen.AcceptTrip;
                    hint.Hint = this.GetExtraDataFromNotification<TripPush>(message.Data, PushConstants.TRIP);
                    break;

                case PushTypes.TripHasBeenModified:
                case PushTypes.TripHasBeenModifiedScheduleConflict:
                    hint.InitialScreen = InitialScreen.UpcomingTripModificationsDetails;
                    hint.Hint = new TripChangesPush
                    {
                        TripId = long.Parse(message.Data[PushConstants.TRIP_ID]),
                        FieldChanges = this.GetExtraDataFromNotification<List<TripFieldChanged>>(message.Data, PushConstants.FIELDS_CHANGED)
                    };
                    break;

                case PushTypes.TripHasBeenCanceled:
                case PushTypes.TripHasBeenCanceledFee:
                    hint.InitialScreen = InitialScreen.UpcomingTripModificationsDetails;
                    hint.Hint = new TripChangesPush
                    {
                        IsCanceled = true,
                        TripId = long.Parse(message.Data[PushConstants.TRIP_ID])
                    };

                    break;

                case PushTypes.TripComingUp:
                    hint.InitialScreen = InitialScreen.UpcomingTripDetails;
                    hint.Hint = this.GetExtraDataFromNotification<TripIdPush>(message.Data, PushConstants.TRIP);
                    break;

                case PushTypes.TimeToStartTrip:
                    hint.InitialScreen = InitialScreen.CurrentTripDetails;
                    hint.Hint = this.GetExtraDataFromNotification<TripIdPush>(message.Data, PushConstants.TRIP);
                    break;

                case PushTypes.UrgentMessageFromClient:
                case PushTypes.ClientHasRequestedPickup:
                    hint.InitialScreen = InitialScreen.MyTrips;
                    break;

                case PushTypes.CSRMessageReceived:
                case PushTypes.DispatchTierUpgrade:
                case PushTypes.ScoreHasFallenBellow:
                case PushTypes.PayoutHasIncreased:
                    hint.InitialScreen = InitialScreen.MyTrips;
                    break;
            }

            // do something with notification!
            if(DGenix.Mobile.Fwk.Droid.MainApplication.IsApplicationInForeground)
                this.HandleNotificationInForeground(hint);
            else
                this.HandleNotificationInBackground(hint);
        }

        private void HandleNotificationInForeground(StartupHint hint)
        {
            new PushNotificationNavigator(new AlertHandler(new AppConfiguration()), new MvxJsonConverter())
                .Navigate(hint);
        }

        private void HandleNotificationInBackground(StartupHint hint)
        {
            // The following statements control which activity open when user presses the notification
            var startupIntent = new Intent(this, typeof(SplashScreen));
            startupIntent.PutExtra(PushConstants.PUSH_HINT, JsonConvert.SerializeObject(hint));
            startupIntent.AddFlags(ActivityFlags.NewTask | ActivityFlags.SingleTop | ActivityFlags.ClearTop);

            var pendingIntent = PendingIntent.GetActivity(this, 1, startupIntent, PendingIntentFlags.UpdateCurrent);

            var builder = new NotificationCompat.Builder(this);

            //var style = new NotificationCompat.BigTextStyle();
            //style.BigText("My super awesome and big really big text");

            var notification = builder.SetContentIntent(pendingIntent)
                                      .SetContentTitle(hint.Title)
                                      .SetSmallIcon(Resource.Drawable.ic_notification_car)
                                      .SetLargeIcon(BitmapFactory.DecodeResource(CrossCurrentActivity.Current.Activity.Resources, Resource.Drawable.ic_person))
                                      .SetContentText(hint.Body)
                                      //.SetStyle(style)
                                      .SetAutoCancel(true)
                                      .SetSound(Android.Provider.Settings.System.DefaultNotificationUri)
                                      .Build();

            var notificationManager = GetSystemService(NotificationService) as NotificationManager;
            notificationManager.Notify(100, notification);
        }

        private void LaunchGeopositionService(long staffMemberId)
        {
            var serviceIntent = new Intent(this, typeof(GeopositionService));
            serviceIntent.PutExtra(PushConstants.STAFF_MEMBER_ID, staffMemberId);
            this.StartService(serviceIntent);
        }


        private TPushModel GetExtraDataFromNotification<TPushModel>(IDictionary<string, string> data, string tripKey) where TPushModel : class, new()
        {
            if(string.IsNullOrEmpty(tripKey))
                return null;

            var targetTrip = new TPushModel();

            string tripSerialized;
            if(data.TryGetValue(tripKey, out tripSerialized))
                targetTrip = JsonConvert.DeserializeObject<TPushModel>(tripSerialized);

            return targetTrip;
        }
    }
}
