﻿using System;
using Android.App;
using Firebase.Iid;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;

namespace iDriveYourCar.Droid.PushNotifications
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseIdService : FirebaseInstanceIdService
    {
        private Lazy<IIDYCSettings> settings = new Lazy<IIDYCSettings>(() => new IDYCSettings());

        public override void OnTokenRefresh()
        {
            this.settings.Value.PushNotificationsToken = FirebaseInstanceId.Instance.Token;
        }
    }
}
