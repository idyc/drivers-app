﻿using System;
using DGenix.Mobile.Fwk.Core.Handlers;
using iDriveYourCar.Core.PushNotifications;

namespace iDriveYourCar.Droid.PushNotifications
{
    public class PushNotificationsRegisterer : IPushNotificationRegisterer
    {
        private readonly IAlertHandler alertHandler;

        public PushNotificationsRegisterer(IAlertHandler alertHandler)
        {
            this.alertHandler = alertHandler;
        }

        public void UnregisterForPushNotifications()
        {
            //GcmClient.UnRegister(Application.Context);
        }

        public string GetDeviceId()
        {
            throw new NotImplementedException();
            //return CrossDeviceInfo.Current.Id;
        }
    }
}
