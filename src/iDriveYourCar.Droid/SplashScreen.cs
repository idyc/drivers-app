using Android.App;
using Android.Content.PM;
using DGenix.Mobile.Fwk.Droid;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Plugin.SecureStorage;
using iDriveYourCar.Core.Models;
using Android.Gms.Common;
using Android.Widget;
using Newtonsoft.Json;
using iDriveYourCar.Core.PushNotifications;
using Android.Content;

namespace iDriveYourCar.Droid
{
    [Activity(
        MainLauncher = true,
        Theme = "@style/Theme.Splash",
        Name = "idriveyourcar.droid.SplashScreen",
        NoHistory = true,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : BaseSplashScreen
    {
        private StartupHint hint;

        public SplashScreen()
            : base(iDriveYourCar.Droid.Resource.Layout.SplashScreen)
        {
        }

        protected override void OnCreate(Android.OS.Bundle bundle)
        {
            base.OnCreate(bundle);

            if(!this.IsGooglePlayServicesAvailable())
                return;

            if(this.Intent.Extras == null || this.Intent.Extras.IsEmpty || !this.Intent.Extras.ContainsKey(PushConstants.PUSH_HINT))
                return;

            var hintSerialized = this.Intent.Extras.GetString(PushConstants.PUSH_HINT);
            if(hintSerialized != null)
                this.hint = JsonConvert.DeserializeObject<StartupHint>(hintSerialized);
        }

        private bool _isResumed;

        protected override void OnResume()
        {
            _isResumed = true;
            base.OnResume();
        }

        protected override void OnPause()
        {
            _isResumed = false;
            base.OnPause();
        }

        public override void InitializationComplete()
        {
            if(!_isResumed)
                return;

            SecureStorageImplementation.StoragePassword = "idriveyourcar_Iybitwc01@";

            TriggerFirstNavigate();
        }

        protected override void TriggerFirstNavigate()
        {
            Mvx.Resolve<IMvxAppStart>().Start(this.hint);
        }

        private bool IsGooglePlayServicesAvailable()
        {
            var resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if(resultCode == ConnectionResult.Success)
                return true;

            var text = "This device is not supported!";

            if(GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
            {
                text = GoogleApiAvailability.Instance.GetErrorString(resultCode);
                text = "Google Play Services is not installed";
                Toast.MakeText(this, text, ToastLength.Long).Show();
            }
            else
                this.Finish();

            return false;
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
        }
    }
}
