﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;
namespace iDriveYourCar.Core.Converters
{
    public class TripPushRoundTripDropoffToDateAtTimeAMPMStringConverter : MvxValueConverter<TripPush, string>
    {
        protected override string Convert(TripPush value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var pickupDate = DateTime.Parse(value?.Return?.PickupDate).ToFormattedString(DateStringFormat.ShortDayShortMonthDayNumber);
            var pickupTime = DateTime.Parse(value?.Return?.PickupTime).ToFormattedString(DateStringFormat.TimeAMPM);

            return $"{pickupDate} at {pickupTime}";
        }
    }
}
