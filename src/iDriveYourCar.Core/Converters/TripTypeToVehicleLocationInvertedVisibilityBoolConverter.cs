﻿using System;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;
namespace iDriveYourCar.Core.Converters
{
    public class TripTypeToVehicleLocationInvertedVisibilityBoolConverter : MvxValueConverter<Trip>
    {
        protected override object Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if((value.TripType == TripType.OneWayPickup || value.TripType == TripType.AirportPickup || value.TripType == TripType.DriveMeHome)
               && !string.IsNullOrEmpty(value.KeyLocationVehicleDescription))
            {
                return false;
            }

            return true;
        }
    }
}
