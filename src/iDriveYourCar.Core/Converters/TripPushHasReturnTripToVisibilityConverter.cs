﻿using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
    public class TripPushHasReturnTripToVisibilityConverter : MvxBaseVisibilityValueConverter<TripPush>
    {
        protected override MvxVisibility Convert(TripPush value, object parameter, CultureInfo culture)
        {
            return value != null ? MvxVisibility.Visible : MvxVisibility.Collapsed;
        }
    }
}


