﻿using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;

namespace iDriveYourCar.Core
{
	public class DriverInvitationToRedeemBackgroundColorConverter : MvxColorValueConverter<bool>
	{
		protected override MvxColor Convert(bool value, object parameter, System.Globalization.CultureInfo culture)
		{
			var inverted = (bool)(parameter ?? false);

			if(inverted)
				value = !value;

			return value ? new MvxColor(204, 204, 204) : new MvxColor(58, 185, 80);
		}
	}
}


