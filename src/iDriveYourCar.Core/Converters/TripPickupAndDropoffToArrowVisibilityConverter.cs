﻿using System.Globalization;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
	public class TripPickupAndDropoffToArrowVisibilityConverter : MvxBaseVisibilityValueConverter<BaseTripItemViewModel>
	{
		protected override MvxVisibility Convert(BaseTripItemViewModel value, object parameter, CultureInfo culture)
		{
			if((!string.IsNullOrEmpty(value.Pickup)) && (!string.IsNullOrEmpty(value.Dropoff)))
			{
				return MvxVisibility.Visible;
			}

			return MvxVisibility.Collapsed;
		}
	}
}

