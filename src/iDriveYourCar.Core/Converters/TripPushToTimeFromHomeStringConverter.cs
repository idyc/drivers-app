﻿using System;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TripPushToTimeFromHomeStringConverter : MvxValueConverter<float, string>
    {
        protected override string Convert(float value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return $"{value} mi. from home";
        }
    }
}


