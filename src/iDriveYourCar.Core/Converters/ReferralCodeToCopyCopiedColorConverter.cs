﻿using System;
using System.Globalization;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;

namespace iDriveYourCar.Core.Converters
{
	public class ReferralCodeToCopyCopiedColorConverter : MvxColorValueConverter<bool>
	{
		protected override MvxColor Convert(bool value, object parameter, CultureInfo culture)
		{
			return value ? MvxColors.Black : new MvxColor(60, 128, 246);
		}
	}
}