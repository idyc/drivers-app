﻿using System;
using DGenix.Mobile.Fwk.Core.Helpers;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class BoolToFilterDrawableConverter : MvxValueConverter<bool, string>
    {
        protected override string Convert(bool value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconName = value ? "ic_close" : "ic_sort";

            return CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(iconName);
        }
    }
}

