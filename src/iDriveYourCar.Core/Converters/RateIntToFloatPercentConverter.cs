﻿using System;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class RateIntToFloatPercentConverter : MvxValueConverter<int, float>
    {
        protected override float Convert(int value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var itemsCount = (int)parameter;

            float valueToReturn = 0;
            if(value != 0 && itemsCount != 0)
            {
                valueToReturn = (float)value / itemsCount;
            }

            return valueToReturn;
        }
    }
}

