﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class BoolToThisWeekLastWeekConverter : MvxValueConverter<bool, string>
	{
		protected override string Convert(bool value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value ? IDriveYourCarStrings.ViewThisWeek.ToUpper() : IDriveYourCarStrings.ViewLastWeek.ToUpper();
		}
	}
}