﻿using System;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Helpers;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class FieldChangedToIconConverter : MvxValueConverter<List<TripFieldChanged>, string>
    {
        protected override string Convert(List<TripFieldChanged> value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconName = string.Empty;
            var enumValue = (TripFieldChanged)Enum.Parse(typeof(TripFieldChanged), parameter.ToString());

            switch (enumValue)
            {
                //case TripFieldChanged.VehicleAddress:
                //iconName = value.Contains(TripFieldChanged.VehicleAddress) ? "ic_vehicle_red" : "ic_vehicle";
                //break;
                case TripFieldChanged.VehicleAddressId:
                    iconName = value.Contains(TripFieldChanged.VehicleAddressId) ? "ic_vehicle_red" : "ic_vehicle";
                    break;
                case TripFieldChanged.Stops:
                    iconName = value.Contains(TripFieldChanged.Stops) ? "ic_location_red" : "ic_location";
                    break;
                case TripFieldChanged.Offer:
                    iconName = value.Contains(TripFieldChanged.KeyLocationVehicleDescription) ? "ic_warning_red" : "ic_warning";
                    break;
                case TripFieldChanged.KeyLocationVehicleDescription:
                    iconName = value.Contains(TripFieldChanged.KeyLocationVehicleDescription) ? "ic_key_red" : "ic_key";
                    break;
                case TripFieldChanged.MeetupInstructions:
                    iconName = value.Contains(TripFieldChanged.MeetupInstructions) ? "ic_people_red" : "ic_people";
                    break;
                case TripFieldChanged.PublicNotes:
                    iconName = value.Contains(TripFieldChanged.MeetupInstructions) ? "ic_notes_red" : "ic_notes";
                    break;
                case TripFieldChanged.NotesForDriver:
                    iconName = value.Contains(TripFieldChanged.NotesForDriver) ? "ic_person_red" : "ic_person";
                    break;
            }

            return CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(iconName);
        }
    }
}
