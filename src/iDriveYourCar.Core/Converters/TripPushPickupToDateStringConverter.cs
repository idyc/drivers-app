﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TripPushPickupToDateStringConverter : MvxValueConverter<TripPush, string>
    {
        protected override string Convert(TripPush value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var pickupDate = DateTime.Parse(value.PickupDate).ToFormattedString(DateStringFormat.DayMonthDayNumber);
            return pickupDate;
        }
    }
}

