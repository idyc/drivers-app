﻿using System;
using System.Globalization;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;
namespace iDriveYourCar.Core.Converters
{
    public class GrayBackgroundColorConverter : MvxColorValueConverter<bool>
    {
        protected override MvxColor Convert(bool value, object parameter, CultureInfo culture)
        {
            return value ? new MvxColor(251, 251, 251) : MvxColors.White;
        }
    }
}
