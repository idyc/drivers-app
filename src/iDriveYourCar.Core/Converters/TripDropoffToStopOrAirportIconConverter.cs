﻿using System;
using DGenix.Mobile.Fwk.Core.Helpers;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TripDropoffToStopOrAirportIconConverter : MvxValueConverter<Trip, string>
    {
        protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconName = string.Empty;
            switch(value.TripType)
            {
                case TripType.AirportDropoff:
                    iconName = "ic_plane";
                    break;
                case TripType.AirportPickup:
                case TripType.DriveMeHome:
                case TripType.OneWayDropoff:
                case TripType.OneWayPickup:
                case TripType.WaitAndReturn:
                    iconName = "ic_location";
                    break;
            }

            return CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(iconName);
        }
    }
}
