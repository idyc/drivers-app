﻿using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
    public class TripReturnToInvertedVisibilityConverter : MvxBaseVisibilityValueConverter<Trip>
    {
        protected override MvxVisibility Convert(Trip value, object parameter, CultureInfo culture)
        {
            if(value == null)
            {
                return MvxVisibility.Visible;
            }

            return MvxVisibility.Collapsed;
        }
    }
}



