﻿using System;
using DGenix.Mobile.Fwk.Core.Helpers;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TripModificationsDropoffToStopOrAirportIconConverter : MvxValueConverter<Trip, string>
    {
        protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconName = string.Empty;
            switch (value.TripType)
            {
                case TripType.AirportDropoff:
                    iconName = value.FieldsChanged.Contains(TripFieldChanged.DropoffAddressId) ? "ic_plane_red" : "ic_plane";
                    break;
                case TripType.AirportPickup:
                case TripType.DriveMeHome:
                case TripType.OneWayDropoff:
                case TripType.OneWayPickup:
                case TripType.WaitAndReturn:
                    iconName = value.FieldsChanged.Contains(TripFieldChanged.DropoffAddressId) ? "ic_location_red" : "ic_location";
                    break;
            }

            return CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(iconName);
        }
    }
}