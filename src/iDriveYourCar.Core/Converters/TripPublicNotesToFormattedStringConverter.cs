﻿using System;
using MvvmCross.Platform.Converters;
using System.Collections.Generic;

namespace iDriveYourCar.Core.Converters
{
    public class TripPublicNotesToFormattedStringConverter : MvxValueConverter<List<string>, string>
    {
        protected override string Convert(List<string> value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Join("\n", value);
        }
    }
}
