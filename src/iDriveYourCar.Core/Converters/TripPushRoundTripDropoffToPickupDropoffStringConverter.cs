﻿using System;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;
namespace iDriveYourCar.Core.Converters
{
    public class TripPushRoundTripDropoffToPickupDropoffStringConverter : MvxValueConverter<TripPush, string>
    {
        protected override string Convert(TripPush value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return $"{value.PickupAddress} - {value.DropoffAddress}";
        }
    }
}
