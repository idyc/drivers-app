﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class TripTimesRangeToTripDurationStringConverter : MvxValueConverter<Trip, string>
	{
		protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			string duration = string.Empty;

			var durationTimeSpan = (DateTime.Parse(value.DropoffTime) - DateTime.Parse(value.PickupTime));
			if (durationTimeSpan.Hours == 0)
			{
				if (durationTimeSpan.Minutes == 1)
				{
					duration = $"{durationTimeSpan.Minutes} {IDriveYourCarStrings.Minute}";
				}
				else
				{
					duration = $"{durationTimeSpan.Minutes} {IDriveYourCarStrings.Minutes}";
				}
			}
			else
			{
				if (durationTimeSpan.Hours == 1)
				{
					if (durationTimeSpan.Minutes == 1)
					{
						duration = $"{durationTimeSpan.Hours} {IDriveYourCarStrings.Hour}, {durationTimeSpan.Minutes} {IDriveYourCarStrings.Minute}";
					}
					else
					{
						duration = $"{durationTimeSpan.Hours} {IDriveYourCarStrings.Hour}, {durationTimeSpan.Minutes} {IDriveYourCarStrings.Minutes}";
					}
				}
				else
				{
					if (durationTimeSpan.Minutes == 1)
					{
						duration = $"{durationTimeSpan.Hours} {IDriveYourCarStrings.Hours}, {durationTimeSpan.Minutes} {IDriveYourCarStrings.Minute}";
					}
					else
					{
						duration = $"{durationTimeSpan.Hours} {IDriveYourCarStrings.Hours}, {durationTimeSpan.Minutes} {IDriveYourCarStrings.Minutes}";
					}
				}
			}
			return duration;
		}
	}
}

