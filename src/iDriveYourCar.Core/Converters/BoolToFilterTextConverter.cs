﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class BoolToFilterTextConverter : MvxValueConverter<bool, string>
    {
        protected override string Convert(bool value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value ? IDriveYourCarStrings.ClearFilter.ToUpper() : IDriveYourCarStrings.Filter.ToUpper();
        }
    }
}

