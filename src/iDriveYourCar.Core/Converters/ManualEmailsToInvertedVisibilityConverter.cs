﻿using System.Globalization;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
    public class ManualEmailsToInvertedVisibilityConverter : MvxBaseVisibilityValueConverter<string>
    {
        protected override MvxVisibility Convert(string value, object parameter, CultureInfo culture)
        {
            return string.IsNullOrEmpty(value) ? MvxVisibility.Visible : MvxVisibility.Collapsed;
        }
    }
}


