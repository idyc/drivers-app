﻿using System;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace iDriveYourCar.Core.Converters
{
    public class TripReturnToTripAcceptedStringConverter : MvxValueConverter<Trip, string>
    {
        protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? IDriveYourCarStrings.YourTripsHaveBeenAccepted : IDriveYourCarStrings.YourTripHasBeenAccepted;
        }
    }
}


