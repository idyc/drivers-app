﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class DayNumberToEveryDayNameConverter : MvxValueConverter<long, string>
	{
		protected override string Convert(long value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return $"{IDriveYourCarStrings.Every} {DaysOfWeek.GetByNumber((int)value).Name}";
		}
	}
}
