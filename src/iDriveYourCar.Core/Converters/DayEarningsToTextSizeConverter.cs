﻿using System;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class DayEarningsToTextSizeConverter : MvxValueConverter<decimal, float>
    {
        protected override float Convert(decimal value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value != 0) ? 15 : 25;
        }
    }
}
