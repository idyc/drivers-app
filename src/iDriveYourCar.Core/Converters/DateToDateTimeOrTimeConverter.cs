﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class DateToDateTimeOrTimeConverter : MvxValueConverter<string, string>
    {
        protected override string Convert(string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var valueToReturn = string.Empty;

            var date = DateTime.Parse(value);
            if(date == DateTime.Today)
            {
                valueToReturn = DateTime.Parse(value).ToFormattedString(DateStringFormat.TimeAMPM);
            }
            else
            {
                valueToReturn = DateTime.Parse(value).ToFormattedString(DateStringFormat.ShortDateMonthYearSlashes);
            }

            return valueToReturn;
        }
    }
}
