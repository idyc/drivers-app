﻿using System;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace iDriveYourCar.Core.Converters
{
	public class TripTypeEnumToTripTypeStringConverter : MvxValueConverter<TripType, string>
	{
		protected override string Convert(TripType value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			switch(value)
			{
				case TripType.AirportPickup:
					return IDriveYourCarStrings.AirportPickup;
				case TripType.AirportDropoff:
					return IDriveYourCarStrings.AirportDropoff;
				case TripType.OneWayPickup:
					return IDriveYourCarStrings.OneWayPickup;
				case TripType.OneWayDropoff:
					return IDriveYourCarStrings.OneWayDropoff;
				case TripType.WaitAndReturn:
					return IDriveYourCarStrings.WaitAndReturn;
				case TripType.DriveMeHome:
					return IDriveYourCarStrings.DriveMeHome;
				default:
					return string.Empty;
			}
		}
	}
}
