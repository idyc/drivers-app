﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class DriverUnavailabilityToTimeRangeConverter : MvxValueConverter<DriverUnavailabilityWeek, string>
    {
        protected override string Convert(DriverUnavailabilityWeek value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return $"{value.TimeStart.ToFormattedString(DateStringFormat.TimeAMPM)} - {value.TimeStop.ToFormattedString(DateStringFormat.TimeAMPM)}";
        }
    }
}
