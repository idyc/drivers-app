﻿using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
	public class TripTypeToMeetupInstructionsVisibilityConverter : MvxBaseVisibilityValueConverter<Trip>
	{
		protected override MvxVisibility Convert(Trip value, object parameter, CultureInfo culture)
		{
			if(value.TripType == TripType.DriveMeHome && !string.IsNullOrEmpty(value.MeetupInstructions))
			{
				return MvxVisibility.Visible;
			}

			return MvxVisibility.Collapsed;
		}
	}
}
