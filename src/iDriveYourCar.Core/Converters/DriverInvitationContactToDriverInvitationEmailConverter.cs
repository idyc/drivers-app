﻿using System;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class DriverInvitationContactToDriverInvitationEmailConverter : MvxValueConverter<DriverInvitation, string>
	{
		private const string UNKNOWN_CONTACT_IDENTIFIER = "UNKNOW";

		protected override string Convert(DriverInvitation value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value.Contact == UNKNOWN_CONTACT_IDENTIFIER ? value.Email : value.Contact;
		}
	}
}
