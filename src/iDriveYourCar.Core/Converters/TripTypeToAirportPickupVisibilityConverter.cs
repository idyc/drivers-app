﻿using System;
using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
    public class TripTypeToAirportPickupVisibilityConverter : MvxBaseVisibilityValueConverter<Trip>
    {
        protected override MvxVisibility Convert(Trip value, object parameter, CultureInfo culture)
        {
            return value.TripType == TripType.AirportPickup ? MvxVisibility.Visible : MvxVisibility.Collapsed;
        }
    }
}

