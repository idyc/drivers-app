﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class AverageRatingDecimalToAverageRatingStringConverter : MvxValueConverter<decimal, string>
	{
		protected override string Convert(decimal value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
            return value.ToString("0.00");
		}
	}
}
