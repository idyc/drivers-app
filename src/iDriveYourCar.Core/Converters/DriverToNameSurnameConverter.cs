﻿using System;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.Converters
{
	public class DriverToNameSurnameConverter : MvxValueConverter<Driver, string>
	{
		protected override string Convert(Driver value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value != null ? $"{value.FirstName} {value.LastName}" : string.Empty;
		}
	}
}
