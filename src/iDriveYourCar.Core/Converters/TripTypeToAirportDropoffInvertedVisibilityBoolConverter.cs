﻿using System;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.Converters
{
    public class TripTypeToAirportDropoffInvertedVisibilityBoolConverter : MvxValueConverter<Trip, bool>
    {
        protected override bool Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !(value.TripType == TripType.AirportDropoff);
        }
    }
}
