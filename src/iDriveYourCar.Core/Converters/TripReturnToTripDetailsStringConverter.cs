﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Converters
{
    public class TripReturnToTripDetailsStringConverter : MvxValueConverter<Trip, string>
    {
        protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? IDriveYourCarStrings.ViewRoundTripDetails.ToUpper() : IDriveYourCarStrings.ViewTripDetails.ToUpper();
        }
    }
}






