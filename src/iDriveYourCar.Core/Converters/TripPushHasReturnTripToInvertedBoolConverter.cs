﻿using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TripPushHasReturnTripToInvertedBoolConverter : MvxValueConverter<TripPush, bool>
    {
        protected override bool Convert(TripPush value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return value == null;
        }
    }
}