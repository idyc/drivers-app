﻿using System;
using System.Globalization;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.iDriveYourCar.Core;

namespace iDriveYourCar.Core.Converters
{
    public class SubmitTripNotesToBackgroundColorConverter : MvxColorValueConverter<string>
    {
        private Lazy<IAppConfiguration> appConfiguration = new Lazy<IAppConfiguration>(() => new AppConfiguration());

        protected override MvxColor Convert(string value, object parameter, CultureInfo culture)
        {
            return string.IsNullOrEmpty(value) ? this.appConfiguration.Value.LightPrimaryColor : this.appConfiguration.Value.PrimaryColor;
        }
    }
}
