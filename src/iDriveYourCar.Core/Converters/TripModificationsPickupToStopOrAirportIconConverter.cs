﻿using System;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Helpers;

namespace iDriveYourCar.Core.Converters
{
    public class TripModificationsPickupToStopOrAirportIconConverter : MvxValueConverter<Trip, string>
    {
        protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconName = string.Empty;
            switch (value.TripType)
            {
                case TripType.AirportPickup:
                    iconName = value.FieldsChanged.Contains(TripFieldChanged.PickupAddressId) ? "ic_plane_red" : "ic_plane";
                    break;
                case TripType.AirportDropoff:
                case TripType.DriveMeHome:
                case TripType.OneWayDropoff:
                case TripType.OneWayPickup:
                case TripType.WaitAndReturn:
                    iconName = value.FieldsChanged.Contains(TripFieldChanged.PickupAddressId) ? "ic_location_red" : "ic_location";
                    break;
            }

            return CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(iconName);
        }
    }
}
