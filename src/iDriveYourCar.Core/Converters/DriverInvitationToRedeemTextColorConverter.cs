﻿using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;

namespace iDriveYourCar.Core
{
	public class DriverInvitationToRedeemTextColorConverter : MvxColorValueConverter<bool>
	{
		protected override MvxColor Convert(bool value, object parameter, System.Globalization.CultureInfo culture)
		{
			var inverted = (bool)(parameter ?? false);

			if(inverted)
				value = !value;

			return value ? new MvxColor(128, 128, 128) : new MvxColor(255, 255, 255);
		}
	}
}

