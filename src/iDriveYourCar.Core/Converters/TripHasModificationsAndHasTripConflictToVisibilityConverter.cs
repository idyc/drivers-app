﻿using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
    public class TripHasModificationsAndHasTripConflictToVisibilityConverter : MvxBaseVisibilityValueConverter<Trip>
    {
        protected override MvxVisibility Convert(Trip value, object parameter, CultureInfo culture)
        {
            return (!value.IsCancelled && value.HasTripConflict) ? MvxVisibility.Visible : MvxVisibility.Collapsed;
        }
    }
}



