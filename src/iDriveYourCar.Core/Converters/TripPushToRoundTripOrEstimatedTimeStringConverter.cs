﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;
namespace iDriveYourCar.Core.Converters
{
    public class TripPushToRoundTripOrEstimatedTimeStringConverter : MvxValueConverter<TripPush, string>
    {
        protected override string Convert(TripPush value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value?.Return != null ? IDriveYourCarStrings.RoundTrip : $"Est. {value.EstimatedTime} hours";
        }
    }
}

