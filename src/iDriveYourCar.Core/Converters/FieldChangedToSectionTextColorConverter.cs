﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;

namespace iDriveYourCar.Core
{
    public class FieldChangedToSectionTextColorConverter : MvxColorValueConverter<List<TripFieldChanged>>
    {
        protected override MvxColor Convert(List<TripFieldChanged> value, object parameter, System.Globalization.CultureInfo culture)
        {
            var enumValue = (TripFieldChanged)Enum.Parse(typeof(TripFieldChanged), parameter.ToString());
            return value.Contains(enumValue) ? new MvxColor(219, 50, 54, 160) : new MvxColor(175, 175, 175);
        }
    }
}
