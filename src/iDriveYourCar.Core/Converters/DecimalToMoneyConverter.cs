﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class DecimalToMoneyConverter : MvxValueConverter<decimal, string>
	{
		protected override string Convert(decimal value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return (value != 0) ? string.Format(IDriveYourCarStrings.CurrencyAndQuantity, value) : IDriveYourCarStrings.Dash;
		}
	}
}