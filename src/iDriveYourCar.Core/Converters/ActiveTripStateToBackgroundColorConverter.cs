﻿using System;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.iDriveYourCar.Core;

namespace iDriveYourCar.Core.Converters
{
    public class ActiveTripStateToBackgroundColorConverter : MvxColorValueConverter<ActiveTripState>
    {
        private Lazy<IAppConfiguration> appConfiguration = new Lazy<IAppConfiguration>(() => new AppConfiguration());

        protected override MvxColor Convert(ActiveTripState value, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == ActiveTripState.EndTrip ? MvxColors.Red : this.appConfiguration.Value.PrimaryColor;
        }
    }
}
