﻿using System;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class TripToClientNameSurnameConverter : MvxValueConverter<Trip, string>
	{
		protected override string Convert(Trip value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return $"{value.Client.FirstName} {value.Client.LastName}";
		}
	}
}
