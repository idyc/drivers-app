﻿using System;
using System.Reflection;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Converters.Json
{
    public class StringNumberToBoolConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.GetTypeInfo().IsValueType;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var theString = reader.Value?.ToString();

            if(string.IsNullOrWhiteSpace(theString))
                return false;

            if(objectType.GetTypeInfo().IsGenericType && objectType.GetTypeInfo().GetGenericTypeDefinition() == typeof(Nullable<>))
                objectType = Nullable.GetUnderlyingType(objectType);

            bool toReturn;
            if(!bool.TryParse(theString, out toReturn))
                toReturn = Convert.ToBoolean(Convert.ToInt16(theString));

            return toReturn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value != null ? Convert.ToInt32(value).ToString() : null);
        }
    }
}
