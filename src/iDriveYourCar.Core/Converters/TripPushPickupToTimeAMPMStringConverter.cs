﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TripPushPickupToTimeAMPMStringConverter : MvxValueConverter<TripPush, string>
    {
        protected override string Convert(TripPush value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var pickupTime = DateTime.Parse(value.PickupTime).ToFormattedString(DateStringFormat.TimeAMPM);
            return pickupTime;
        }
    }
}