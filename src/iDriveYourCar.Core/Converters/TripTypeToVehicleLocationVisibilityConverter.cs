﻿using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace iDriveYourCar.Core.Converters
{
    public class TripTypeToVehicleLocationVisibilityConverter : MvxBaseVisibilityValueConverter<Trip>
    {
        protected override MvxVisibility Convert(Trip value, object parameter, CultureInfo culture)
        {
            if((value.TripType == TripType.OneWayPickup || value.TripType == TripType.AirportPickup || value.TripType == TripType.DriveMeHome)
               && value.VehicleAddress != null)
            {

                return MvxVisibility.Visible;
            }

            return MvxVisibility.Collapsed;
        }
    }
}
