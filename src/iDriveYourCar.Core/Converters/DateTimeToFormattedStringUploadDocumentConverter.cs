﻿using System;
using DGenix.Mobile.Fwk.Core.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace iDriveYourCar.Core.Converters
{
	public class DateTimeToFormattedStringUploadDocumentConverter : DateTimeToStringConverter
	{
		public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return $"{IDriveYourCarStrings.LastUpdated} {base.Convert(value, targetType, parameter, culture)}";
		}
	}
}
