﻿using System;
using MvvmCross.Platform.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
namespace iDriveYourCar.Core.Converters
{
	public class ProfileEditableFieldToNoneIfEmptyConverter : MvxValueConverter<string, string>
	{
		protected override string Convert(string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return !string.IsNullOrEmpty(value) ? value : IDriveYourCarStrings.None;
		}
	}
}
