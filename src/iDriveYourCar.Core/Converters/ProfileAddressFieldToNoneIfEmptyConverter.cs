﻿using System;
using MvvmCross.Platform.Converters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Converters
{
    public class ProfileAddressFieldToNoneIfEmptyConverter : MvxValueConverter<Address, string>
    {
        protected override string Convert(Address value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? value.Raw : IDriveYourCarStrings.None;
        }
    }
}
