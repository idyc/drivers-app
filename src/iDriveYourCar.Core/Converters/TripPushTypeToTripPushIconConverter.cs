﻿using System;
using MvvmCross.Platform.Converters;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Helpers;

namespace iDriveYourCar.Core.Converters
{
    public class TripPushTypeToTripPushIconConverter : MvxValueConverter<TripPush, string>
    {
        protected override string Convert(TripPush value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconName = string.Empty;

            switch(value.Type)
            {
                case TripType.AirportPickup:
                case TripType.AirportDropoff:
                    iconName = value.Return != null ? "ic_trip_push_plane" : "ic_trip_push_flight";
                    break;
                case TripType.DriveMeHome:
                case TripType.OneWayDropoff:
                case TripType.OneWayPickup:
                case TripType.WaitAndReturn:
                    //iconName = value.Return != null ? "ic_trip_push_car" : "ic_trip_push_car";
                    iconName = "ic_trip_push_car";
                    break;
            }

            return CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(iconName);
        }
    }
}



