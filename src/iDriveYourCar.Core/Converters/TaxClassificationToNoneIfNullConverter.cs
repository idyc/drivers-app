﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
    public class TaxClassificationToNoneIfNullConverter : MvxValueConverter<TaxClassification, string>
    {
        protected override string Convert(TaxClassification value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch(value)
            {
                case TaxClassification.SolePropietor:
                    return IDriveYourCarStrings.SolePropietor;

                case TaxClassification.LLC:
                    return IDriveYourCarStrings.LLC;

                case TaxClassification.Partnership:
                    return IDriveYourCarStrings.Partnership;

                case TaxClassification.Corporation:
                    return IDriveYourCarStrings.Corporation;
            }

            return IDriveYourCarStrings.None;
        }
    }
}