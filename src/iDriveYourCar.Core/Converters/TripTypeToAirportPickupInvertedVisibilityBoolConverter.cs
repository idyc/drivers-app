﻿using System;
using System.Globalization;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform.Converters;
namespace iDriveYourCar.Core.Converters
{
    public class TripTypeToAirportPickupInvertedVisibilityBoolConverter : MvxValueConverter<Trip, bool>
    {
        protected override bool Convert(Trip value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(value.TripType == TripType.AirportPickup);
        }
    }
}
