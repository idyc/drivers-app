﻿using System;
using MvvmCross.Platform.Converters;

namespace iDriveYourCar.Core.Converters
{
	public class BalanceOfReferredClientsToBoolConverter : MvxValueConverter<decimal?, bool>
	{
		protected override bool Convert(decimal? value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if(value == null)
				return false;

			return value.Value != 0;
		}
	}
}

