using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Repositories;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.ServiceBridges;
using iDriveYourCar.Core.ViewModels.Support;
using MvvmCross.Platform;

namespace iDriveYourCar.Core
{
    public class App : IDYCApp
    {
        private IIDYCSettings appSettings;
        private IIDYCSettings idycSettings;

        protected IDriveYourCarSQLiteAdapter sqliteAdapter;
        private IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;

        public override void Initialize()
        {
            base.Initialize();

            this.appSettings = new IDYCSettings();
            Mvx.RegisterSingleton(typeof(IIDYCSettings), this.appSettings); ;

            this.sqliteAdapter = new IDriveYourCarSQLiteAdapter();
            Mvx.RegisterSingleton<ISQLiteAdapter>(this.sqliteAdapter);

            this.idycSettings = new IDYCSettings();
            Mvx.RegisterSingleton(typeof(IDYCSettings), this.idycSettings);

            Mvx.LazyConstructAndRegisterSingleton<IProviderFactory, IDriveYourCarProviderFactory>();

            this.universalObjectWrapperSingletonDriver = new UniversalObjectNotifyWrapperSingleton<Driver>();
            Mvx.RegisterSingleton<IUniversalObjectNotifyWrapperSingleton<Driver>>(this.universalObjectWrapperSingletonDriver);

            this.universalObjectWrapperSingletonRegisterDriver = new UniversalObjectNotifyWrapperSingleton<RegisterDriver>();
            Mvx.RegisterSingleton<IUniversalObjectNotifyWrapperSingleton<RegisterDriver>>(this.universalObjectWrapperSingletonRegisterDriver);

            // menus
            Mvx.RegisterType<IMenuNavigationActionsWrapper, MainMenuNavigationActionsWrapper>();
            Mvx.RegisterType<ICurrentTripMenuNavigationActionsWrapper, CurrentTripNavigationActionsWrapper>();

            // service bridges
            Mvx.RegisterType<IFwkBaseListViewModelServiceBridge<INotificationService, Notification, NotificationCriteria>, NotificationsServiceBridge>();
            Mvx.RegisterType<IFwkBaseListViewModelServiceBridge<IDriverService, Review, ReviewCriteria>, ReviewsServiceBridge>();

            this.RegisterAppStart(new AppStart());
        }

        protected override IList<string> ConventionTags
        {
            get
            {
                var tags = base.ConventionTags;
                tags.Add("ApiProvider");
                tags.Add("LocalProvider");
                return tags;
            }
        }
    }
}