﻿using System;
using MvvmCross.Plugins.Messenger;
using iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.MvxMessages
{
	public class RequestUploadDocumentMessage : MvxMessage
	{
		public Document Document { get; set; }

		public RequestUploadDocumentMessage(object sender, Document document) : base(sender)
		{
			this.Document = document;
		}
	}
}
