﻿using System;
using MvvmCross.Plugins.Messenger;
using iDriveYourCar.Core.Criteria;
namespace iDriveYourCar.Core.MvxMessages
{
	public class FilterOpenTripsUpdatedMessage : MvxMessage
	{
		public OpenTripCriteria Criteria { get; private set; }

		public FilterOpenTripsUpdatedMessage(object sender, OpenTripCriteria updatedCriteria)
		: base(sender)
		{
			this.Criteria = updatedCriteria;
		}
	}
}
