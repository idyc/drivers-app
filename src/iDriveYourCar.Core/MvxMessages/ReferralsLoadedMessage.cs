﻿using System;
using MvvmCross.Plugins.Messenger;
using iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.MvxMessages
{
    public class ReferralsLoadedMessage : MvxMessage
    {
        public ReferralsLoadedMessage(object sender, Referrals referrals) : base(sender)
        {
            this.Referrals = referrals;
        }

        public Referrals Referrals { get; private set; }
    }
}
