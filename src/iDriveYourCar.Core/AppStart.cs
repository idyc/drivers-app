﻿using DGenix.Mobile.Fwk.Core.Repositories;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;

namespace iDriveYourCar.Core
{
    public class AppStart : FwkBaseNavigatingObject, IMvxAppStart
    {
        public void Start(object hint = null)
        {
            var userSettings = new UserSettings();

            if(!string.IsNullOrEmpty(userSettings.Token))
            {
                var idycSettings = new IDYCSettings();

                if(idycSettings.HasBankInfo)
                {
                    string serializedHint = null;
                    if(hint != null)
                        serializedHint = JsonConvert.SerializeObject(hint);

                    this.ShowViewModelWithTrack<MainViewModel>(hint != null ? new { hint = serializedHint } : null);
                }
                else
                {
                    this.ShowViewModelWithTrack<BankingDetailsViewModel>();
                }
            }
            else
            {
                this.ShowViewModelWithTrack<LoginViewModel>();
            }
        }
    }
}
