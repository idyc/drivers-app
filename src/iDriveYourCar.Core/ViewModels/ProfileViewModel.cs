using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Rules.ProfileGeneral;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ProfileViewModel : IDYCWithMenuActionsHostViewModel
    {
        private const int NUMBER_OF_SECTIONS = 1;

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IInputFieldHandler inputFieldHandler;
        private readonly IUserInformationService userInformationService;

        private readonly IMvxJsonConverter jsonConverter;
        private readonly IEnumHelper enumHelper;
        private readonly IMvxMessenger mvxMessenger;
        private readonly IDocumentPreviewHandler documentPreviewHandler;
        private readonly IFileService fileService;
        private readonly IAlertHandler alertHandler;
        private readonly IImageHelper imageHelper;
        private readonly ISnackbarHandler snackbarHandler;

        private MvxSubscriptionToken requestUploadDocumentToken, driverUpdatedMessage;

        public ProfileViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IInputFieldHandler inputFieldHandler,
            IUserInformationService userInformationService,
            IMvxJsonConverter jsonConverter,
            IEnumHelper enumHelper,
            IMvxMessenger mvxMessenger,
            IDocumentPreviewHandler documentPreviewHandler,
            IFileService fileService,
            IAlertHandler alertHandler,
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IImageHelper imageHelper,
            ISnackbarHandler snackbarHandler)
            : base(menuNavigationActionsWrapper)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.inputFieldHandler = inputFieldHandler;
            this.userInformationService = userInformationService;
            this.jsonConverter = jsonConverter;
            this.enumHelper = enumHelper;
            this.mvxMessenger = mvxMessenger;
            this.documentPreviewHandler = documentPreviewHandler;
            this.fileService = fileService;
            this.alertHandler = alertHandler;
            this.imageHelper = imageHelper;
            this.snackbarHandler = snackbarHandler;

            this.Documents = new ObservableCollection<DocumentItemViewModel>();

            this.requestUploadDocumentToken = this.mvxMessenger.SubscribeOnMainThread<RequestUploadDocumentMessage>(
                message => this.UploadDocumentTaskCompletion = NotifyTaskCompletion.Create(() => this.UploadDocumentAsync(message.Document)));

            this.driverUpdatedMessage = this.mvxMessenger.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message => this.RaisePropertyChanged(() => this.Driver));
        }


        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ShowUploadAvatarCommand = new MvxCommand(() => this.ShowViewModelWithTrack<UploadAvatarViewModel>());

            this.UpdateDriverInformationCommand = new MvxCommand<DriverEditableField>(this.UpdateDriverEditableField);

            this.UpdateDriverAddressCommand = new MvxCommand(this.UpdateDriverAddress);

            this.LogoutCommand = new MvxCommand(this.Logout);
        }

        // MVX Life cycle
        public void Init()
        {

        }

        public override void Start()
        {
            base.Start();

            var driverLicense = this.Driver.Documents.Any(d => d.DocumentType == Models.DocumentType.DriverLicense)
                               ? this.Driver.Documents.Where(d => d.DocumentType == Models.DocumentType.DriverLicense).MaxBy(d => d.Version)
                               : new Document { DocumentType = Models.DocumentType.DriverLicense };
            this.Documents.Add(new DocumentItemViewModel(driverLicense, this.enumHelper, this.documentPreviewHandler, this.mvxMessenger, this.fileService));

            var vehicleInsurance = this.Driver.Documents.Any(d => d.DocumentType == Models.DocumentType.VehicleInsurance)
                               ? this.Driver.Documents.Where(d => d.DocumentType == Models.DocumentType.VehicleInsurance).MaxBy(d => d.Version)
                               : new Document { DocumentType = Models.DocumentType.VehicleInsurance };
            this.Documents.Add(new DocumentItemViewModel(vehicleInsurance, this.enumHelper, this.documentPreviewHandler, this.mvxMessenger, this.fileService));
        }

        // MVVM Properties
        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        public INotifyTaskCompletion LogoutTaskCompletion { get; private set; }

        public INotifyTaskCompletion UploadDocumentTaskCompletion { get; private set; }

        public ObservableCollection<DocumentItemViewModel> Documents { get; set; }

        // MVVM Commands
        public ICommand ShowUploadAvatarCommand { get; private set; }

        public ICommand UpdateDriverInformationCommand { get; private set; }

        public ICommand UpdateDriverAddressCommand { get; private set; }

        public ICommand LogoutCommand { get; private set; }

        // public methods

        // private methods
        private void UpdateDriverEditableField(DriverEditableField field)
        {
            if (field == DriverEditableField.TaxId)
                this.ShowViewModelWithTrack<ProfileEditTaxIdViewModel>(new { field });
            else
                this.ShowViewModelWithTrack<ProfileEditFieldViewModel>(new { field });
        }

        private void UpdateDriverAddress()
        {
            this.ShowViewModelWithTrack<ProfileEditAddressViewModel>(new { field = DriverEditableField.Raw });
        }

        private async Task UploadDocumentAsync(Document document)
        {
            var options = new[] { IDriveYourCarStrings.TakePhoto, IDriveYourCarStrings.ChooseFromGallery };
            var selectedSource = await this.inputFieldHandler.ShowOptionsAlertAsync(
                this.enumHelper.GetDocumentTypeDescription(document.DocumentType),
                options);

            // if no option was selected, return
            if (string.IsNullOrEmpty(selectedSource))
                return;

            ImageSource source = selectedSource == IDriveYourCarStrings.TakePhoto ? ImageSource.Camera : ImageSource.Gallery;

            var result = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
                            source,
                            document.DocumentType.ToString(),
                            Guid.NewGuid().ToString(),
                            Plugin.Media.Abstractions.PhotoSize.Full,
                            100);

            // if no image was selected, return
            if (result == null)
                return;

            var fileName = Path.GetFileName(result.Path);
            await this.fileService.UploadDocumentAsync(
                result.GetStream(),
                fileName,
                document.DocumentType,
                success: (uploadedDocument) =>
            {
                var newDocument = new DocumentItemViewModel(uploadedDocument, this.enumHelper, this.documentPreviewHandler, this.mvxMessenger, this.fileService);
                this.Documents.ReplaceIfExists(newDocument, ivm => ivm.Document.DocumentType == uploadedDocument.DocumentType);

                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.DocumentUploadedSuccessfully);
            });

        }

        private void Logout()
        {
            this.alertHandler.ShowMessage(IDriveYourCarStrings.ApplicationName, IDriveYourCarStrings.DoYouReallyWantToLogOut, Strings.Yes, Strings.No,
                positiveCallback: () =>
                {
                    this.LogoutTaskCompletion = NotifyTaskCompletion.Create(this.LogoutAsync);
                });
        }

        private async Task LogoutAsync()
        {
            await this.userInformationService.LogoutAsync(
                success: () => this.ShowViewModelWithTrack<LoginViewModel>());
        }
    }
}