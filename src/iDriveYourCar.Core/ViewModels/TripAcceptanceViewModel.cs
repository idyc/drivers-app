﻿using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using Nito.AsyncEx;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.MvxHints;

namespace iDriveYourCar.Core.ViewModels
{
    public class TripAcceptanceViewModel : IDYCViewModel
    {
        private const string TRIP_SERIALIZED_BUNDLE_KEY = "tripSerialized";
        private const string ACCEPT_ASSET_NAME = "ic_check_white_big";
        private const string REJECT_ASSET_NAME = "ic_cancel_white_big";

        private readonly IMvxJsonConverter jsonConverter;
        private readonly IAlertHandler alertHandler;
        private readonly ITripsService tripService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;

        private string tripPushSerialized;

        public TripAcceptanceViewModel(
            IMvxJsonConverter jsonConverter,
            IAlertHandler alertHandler,
            ITripsService tripService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver)
        {
            this.jsonConverter = jsonConverter;
            this.alertHandler = alertHandler;
            this.tripService = tripService;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.AcceptTripCommand = new MvxCommand(() => this.AcceptTripTaskCompletion = NotifyTaskCompletion.Create(this.AcceptTripAsync),
                                                    () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.AcceptTripTaskCompletion));
            this.RejectTripCommand = new MvxCommand(this.RejectTrip);
            this.TimeExpiredCommand = new MvxCommand(this.HandleTimeExpired);
        }

        // MVX Life cycle
        public void Init(string tripPushSerialized)
        {
            this.tripPushSerialized = tripPushSerialized;
        }

        protected override void SaveStateToBundle(MvvmCross.Core.ViewModels.IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_SERIALIZED_BUNDLE_KEY] = this.tripPushSerialized;
        }

        protected override void ReloadFromBundle(MvvmCross.Core.ViewModels.IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripPushSerialized = state.Data[TRIP_SERIALIZED_BUNDLE_KEY];
        }

        public override void Start()
        {
            base.Start();

            if(!string.IsNullOrEmpty(this.tripPushSerialized))
                this.Trip = this.jsonConverter.DeserializeObject<TripPush>(this.tripPushSerialized);

            //this.Trip = new TripPush
            //{
            //    Id = 1,
            //    EstimatedTime = 15,
            //    PickupDate = "Monday 17 April, 2017",
            //    PickupTime = "2:30 pm",
            //    TimeFromHome = 12,
            //    Type = TripType.AirportPickup,
            //    PickupAddress = "Westport, CT",
            //    DropoffAddress = "JFK",
            //    Return = new TripPush
            //    {
            //        Id = 2,
            //        EstimatedTime = 10,
            //        PickupDate = "Tuesday 18 April, 2017",
            //        PickupTime = "3:45 pm",
            //        TimeFromHome = 17,
            //        Type = TripType.DriveMeHome,
            //        PickupAddress = "JFK",
            //        DropoffAddress = "Westport, CT"
            //    }
            //};

        }

        // MVVM Properties
        public TripPush Trip { get; set; }

        public INotifyTaskCompletion AcceptTripTaskCompletion { get; private set; }

        public INotifyTaskCompletion RejectTripTaskCompletion { get; private set; }

        public string ResultTitle { get; private set; }

        public string ResultDetail { get; private set; }

        public string ResultTopButton { get; private set; }

        public string ResultBottomButton { get; private set; }

        public string ResultAssetName { get; private set; }

        // MVVM Commands
        public ICommand TimeExpiredCommand { get; private set; }

        public ICommand RejectTripCommand { get; private set; }

        public ICommand AcceptTripCommand { get; private set; }

        public ICommand TopButtonCommand { get; private set; }

        public ICommand BottomButtonCommand { get; private set; }

        // public methods

        // private methods
        private async Task AcceptTripAsync()
        {
            await this.tripService.AcceptTripAsync(
                this.Trip.Id,
                success: driver =>
                {
                    this.universalObjectWrapperSingletonDriver.UniversalObject = driver;

                    this.ResultAssetName = CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(ACCEPT_ASSET_NAME);

                    this.ResultTitle = IDriveYourCarStrings.Success;
                    this.ResultDetail = this.Trip.Return != null ? IDriveYourCarStrings.YourTripsHaveBeenAccepted : IDriveYourCarStrings.YourTripHasBeenAccepted;

                    this.ResultTopButton = this.Trip.Return != null ? IDriveYourCarStrings.ViewRoundTripDetails.ToUpper() : IDriveYourCarStrings.ViewTripDetails.ToUpper();
                    this.TopButtonCommand = new MvxCommand(this.ViewTripDetails);

                    this.ResultBottomButton = IDriveYourCarStrings.FindMoreTrips.ToUpper();
                    this.BottomButtonCommand = new MvxCommand(() => this.ShowViewModelWithTrack<FindOpenTripsViewModel>());
                });
        }

        private void RejectTrip()
        {
            this.alertHandler.ShowMessage(
                IDriveYourCarStrings.RejectTrip,
                IDriveYourCarStrings.AreYouSureYouWouldLikeToRejectThisTrip,
                Strings.Yes,
                Strings.No,
                positiveCallback: () => this.RejectTripTaskCompletion = NotifyTaskCompletion.Create(this.RejectTripAsync));
        }

        private async Task RejectTripAsync()
        {
            await this.tripService.RejectTripAsync(
                this.Trip.Id,
                success: () =>
                {
                    this.ResultAssetName = CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(REJECT_ASSET_NAME);

                    this.ResultTitle = IDriveYourCarStrings.Rejected;
                    this.ResultDetail = this.Trip.Return != null ? IDriveYourCarStrings.YouHaveRejectedTheseTrips : IDriveYourCarStrings.YouHaveRejectedThisTrip;

                    this.ResultTopButton = IDriveYourCarStrings.FindMoreTrips.ToUpper();
                    this.TopButtonCommand = new MvxCommand(() => this.ShowViewModelWithTrack<FindOpenTripsViewModel>());

                    this.ResultBottomButton = IDriveYourCarStrings.ViewMyTrips.ToUpper();
                    this.BottomButtonCommand = new MvxCommand(() => this.ChangePresentation(new ClearBackStackHint()));
                });
        }

        private void HandleTimeExpired()
        {
            this.ResultAssetName = CrossDeviceInfoHelper.GetLocalImageUrlByPlatform(REJECT_ASSET_NAME);

            this.ResultTitle = IDriveYourCarStrings.TimeExpired;
            this.ResultDetail = IDriveYourCarStrings.YourTimeToAcceptThisTripHasExpired;

            this.ResultTopButton = IDriveYourCarStrings.FindMoreTrips.ToUpper();
            this.TopButtonCommand = new MvxCommand(() => this.ShowViewModelWithTrack<FindOpenTripsViewModel>());

            this.ResultBottomButton = string.Empty;
        }

        private void ViewTripDetails()
        {
            this.CloseWithTrack(this);

            this.ShowViewModelWithTrack<UpcomingTripDetailViewModel>(new { tripId = this.Trip.Id });
        }
    }
}
