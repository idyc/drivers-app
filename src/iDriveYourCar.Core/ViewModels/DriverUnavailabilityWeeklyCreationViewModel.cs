﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using iDriveYourCar.Core.ViewModels.Items;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using System.Threading.Tasks;
using Nito.AsyncEx;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using System.Linq;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.ViewModels
{
    public class DriverUnavailabilityWeeklyCreationViewModel : IDYCViewModel
    {
        private readonly IMvxMessenger mvxMessenger;
        private readonly IInputFieldHandler inputFieldHandler;
        private readonly ISnackbarHandler snackbarHandler;

        public DriverUnavailabilityWeeklyCreationViewModel(
            IMvxMessenger mvxMessenger,
            IInputFieldHandler inputFieldHandler,
            ISnackbarHandler snackbarHandler)
        {
            this.mvxMessenger = mvxMessenger;
            this.inputFieldHandler = inputFieldHandler;
            this.snackbarHandler = snackbarHandler;

            this.Days = new List<Day>(DaysOfWeek.Days);
            this.SelectedDay = this.Days.First();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SetStartTimeCommand = new MvxCommand(() => this.TimeStartTaskCompletion = NotifyTaskCompletion.Create(this.SetStartTime),
                                                      () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.TimeStartTaskCompletion));

            this.SetStopTimeCommand = new MvxCommand(() => this.TimeStopTaskCompletion = NotifyTaskCompletion.Create(this.SetStopTime),
                                                     () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.TimeStopTaskCompletion));

            this.SaveUnavailabilityTimeCommand = new MvxCommand(this.SaveUnavailabilityTime);
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public List<Day> Days { get; set; }

        public Day SelectedDay { get; set; }

        public DateTime TimeStart { get; set; } = DateTime.Now;

        public DateTime TimeStop { get; set; } = DateTime.Now;

        public string TimeStartString => this.TimeStart.ToFormattedString(DateStringFormat.TimeAMPM);

        public string TimeStopString => this.TimeStop.ToFormattedString(DateStringFormat.TimeAMPM);

        public INotifyTaskCompletion TimeStartTaskCompletion { get; private set; }

        public INotifyTaskCompletion TimeStopTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SetStartTimeCommand { get; private set; }

        public ICommand SetStopTimeCommand { get; private set; }

        public ICommand SaveUnavailabilityTimeCommand { get; private set; }

        // public methods

        // private methods
        private void SaveUnavailabilityTime()
        {
            if(!this.Validate())
                return;

            var driverUnavailability = new DriverUnavailabilityWeek
            {
                Day = this.SelectedDay.Number,
                TimeStart = this.TimeStart,
                TimeStop = this.TimeStop
            };

            var itemViewModel = new DriverUnavailabilityWeeklyTimeItemViewModel(this.mvxMessenger, driverUnavailability);

            this.mvxMessenger.Publish(new GenericCUDMessage<DriverUnavailabilityWeeklyTimeItemViewModel>(this, itemViewModel, CUDOperation.Create));

            this.CloseWithTrack(this);
        }

        private bool Validate()
        {
            if(this.TimeStop < this.TimeStart || this.CompareDatesEqualityByComponents())
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PleaseEnterAValidTimeBlock);
                return false;
            }
            return true;
        }

        private async Task SetStartTime()
        {
            var time = await this.inputFieldHandler.ShowTimeInputFieldAsync(this.TimeStart);

            if(!time.HasValue)
                return;

            this.TimeStart = time.Value;
        }

        private async Task SetStopTime()
        {
            var time = await this.inputFieldHandler.ShowTimeInputFieldAsync(this.TimeStop);

            if(!time.HasValue)
                return;

            this.TimeStop = time.Value;
        }

        private bool CompareDatesEqualityByComponents()
        {
            return this.TimeStop.Year == this.TimeStart.Year
                       && this.TimeStop.Month == this.TimeStart.Month
                       && this.TimeStop.Day == this.TimeStart.Day
                       && this.TimeStop.Hour == this.TimeStart.Hour
                       && this.TimeStop.Minute == this.TimeStart.Minute
                       && this.TimeStop.Second == this.TimeStart.Second;
        }
    }
}
