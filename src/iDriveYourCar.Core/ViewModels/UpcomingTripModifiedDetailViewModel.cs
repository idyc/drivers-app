using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using Plugin.Messaging;
using System.Linq;
using MvvmCross.Platform.Platform;
using iDriveYourCar.Core.Models.Push;

namespace iDriveYourCar.Core.ViewModels
{
    public class UpcomingTripModifiedDetailViewModel : UpcomingTripDetailViewModel
    {
        private const string TRIP_CHANGES_SERIALIZED_BUNDLEKEY = "tripChangesSerialized";

        private readonly ITripsService tripService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IMvxJsonConverter jsonConverter;

        private string tripChangesSerialized;

        private Trip modifiedOrCanceledTrip;

        public UpcomingTripModifiedDetailViewModel(
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            ITripsService tripService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IMvxJsonConverter jsonConverter)
         : base(menuNavigationActionsWrapper)
        {
            this.tripService = tripService;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.jsonConverter = jsonConverter;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.AcceptChangesCommand = new MvxCommand(() => this.AcceptChangesTaskCompletion = NotifyTaskCompletion.Create(this.AcceptChangesAsync),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.AcceptChangesTaskCompletion));

            this.CallDispatchCommand = new MvxCommand(this.CallDispatch);

            this.ConfirmCancellationCommand = new MvxCommand(() => this.ConfirmCancellationTaskCompletion = NotifyTaskCompletion.Create(this.ConfirmCancellationAsync),
                                                             () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.ConfirmCancellationTaskCompletion));
        }

        // MVX Life cycle
        public new void Init(long tripId)
        {
        }

        public void Init(string tripChangesSerialized)
        {
            this.tripChangesSerialized = tripChangesSerialized;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_CHANGES_SERIALIZED_BUNDLEKEY] = this.tripChangesSerialized;
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripChangesSerialized = state.Data[TRIP_CHANGES_SERIALIZED_BUNDLEKEY];
        }

        public override void Start()
        {
            var changes = this.jsonConverter.DeserializeObject<TripChangesPush>(this.tripChangesSerialized);
            this.tripId = changes.TripId;

            if (changes.IsCanceled)
            {
                if (!this.universalObjectWrapperSingletonDriver.UniversalObject.CanceledTrips.Any(t => t.Id == changes.TripId))
                {
                    this.modifiedOrCanceledTrip = this.universalObjectWrapperSingletonDriver.UniversalObject.UpcomingTrips.First(trip => trip.Id == changes.TripId);
                    this.universalObjectWrapperSingletonDriver.UniversalObject.UpcomingTrips.Remove(this.modifiedOrCanceledTrip);

                    this.modifiedOrCanceledTrip.IsCancelled = true;
                    this.universalObjectWrapperSingletonDriver.UniversalObject.CanceledTrips.Add(this.modifiedOrCanceledTrip);
                }
                else
                {
                    this.modifiedOrCanceledTrip = this.universalObjectWrapperSingletonDriver.UniversalObject.CanceledTrips.First((t => t.Id == changes.TripId));
                }
            }
            else
            {
                this.modifiedOrCanceledTrip = this.universalObjectWrapperSingletonDriver.UniversalObject.UpcomingTrips.First(t => t.Id == changes.TripId);
                this.modifiedOrCanceledTrip.FieldsChanged = changes.FieldChanges;
            }

            base.Start();
        }

        // MVVM Properties
        public string Title => this.modifiedOrCanceledTrip.IsCancelled ? IDriveYourCarStrings.TripCancelled : IDriveYourCarStrings.TripModifications;

        public bool NotesHaveChanges => this.modifiedOrCanceledTrip.FieldsChanged != null &&
                                        this.modifiedOrCanceledTrip.FieldsChanged.Contains(TripFieldChanged.KeyLocationVehicleDescription) ||
                                        this.modifiedOrCanceledTrip.FieldsChanged.Contains(TripFieldChanged.MeetupInstructions) ||
                                        this.modifiedOrCanceledTrip.FieldsChanged.Contains(TripFieldChanged.PublicNotes) ||
                                        this.modifiedOrCanceledTrip.FieldsChanged.Contains(TripFieldChanged.NotesForDriver);

        public INotifyTaskCompletion AcceptChangesTaskCompletion { get; private set; }

        public INotifyTaskCompletion ConfirmCancellationTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand AcceptChangesCommand { get; private set; }

        public ICommand CallDispatchCommand { get; private set; }

        public ICommand ConfirmCancellationCommand { get; private set; }

        // public methods

        // private methods
        private async Task AcceptChangesAsync()
        {
            await this.tripService.AcceptTripChangesAsync(
                this.tripId,
                updatedTrip =>
                {
                    this.AcceptChangesTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();
                    this.CloseWithTrack(this);
                });
        }

        private void CallDispatch()
        {
            if (CrossMessaging.Current.PhoneDialer.CanMakePhoneCall)
                CrossMessaging.Current.PhoneDialer.MakePhoneCall("+1 855-238-6708", "Drive My Car");
        }

        private Task ConfirmCancellationAsync()
        {
            this.ConfirmCancellationTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();
            this.CloseWithTrack(this);

            return Task.FromResult(0);
        }
    }
}
