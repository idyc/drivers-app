﻿//using System;
//using System.IO;
//using System.Threading.Tasks;
//using System.Windows.Input;
//using DGenix.Mobile.Fwk.Core.Helpers;
//using DGenix.Mobile.Fwk.Core.Models;
//using DGenix.Mobile.Fwk.Core.MvxMessages;
//using DGenix.Mobile.Fwk.Core.Support.Extensions;
//using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
//using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
//using iDriveYourCar.Core.Helpers;
//using iDriveYourCar.Core.Models;
//using iDriveYourCar.Core.Services;
//using MvvmCross.Core.ViewModels;
//using MvvmCross.Platform.Platform;
//using MvvmCross.Plugins.Messenger;
//using Nito.AsyncEx;

//namespace iDriveYourCar.Core.ViewModels
//{
//	public class UploadDocumentViewModel : IDYCViewModel
//	{
//		private const string DOCUMENT_SERIALIZED_BUNDLE_KEY = "documentSerialized";

//		private readonly IUniversalObjectWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
//		private readonly IMvxJsonConverter jsonConverter;
//		private readonly IImageHelper imageHelper;
//		private readonly IFileService fileService;
//		private readonly IEnumHelper enumHelper;
//		private readonly IMvxMessenger mvxMessenger;

//		private string documentSerialized;

//		public UploadDocumentViewModel(
//			IUniversalObjectWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
//			IMvxJsonConverter jsonConverter,
//			IImageHelper imageHelper,
//			IFileService fileService,
//			IEnumHelper enumHelper,
//			IMvxMessenger mvxMessenger)
//		{
//			this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
//			this.jsonConverter = jsonConverter;
//			this.imageHelper = imageHelper;
//			this.fileService = fileService;
//			this.enumHelper = enumHelper;
//			this.mvxMessenger = mvxMessenger;
//		}

//		protected override void InitializeCommands()
//		{
//			base.InitializeCommands();

//			this.UploadDocumentFromCameraCommand = new MvxCommand(() => this.UploadDocumentTaskCompletion = NotifyTaskCompletion.Create(() => this.UploadDocumentAsync(ImageSource.Camera)),
//																  () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.UploadDocumentTaskCompletion));

//			this.UploadDocumentFromPhotoLibraryCommand = new MvxCommand(() => this.UploadDocumentTaskCompletion = NotifyTaskCompletion.Create(() => this.UploadDocumentAsync(ImageSource.Gallery)),
//																  		() => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.UploadDocumentTaskCompletion));
//		}

//		// MVX Life cycle
//		public void Init(string documentSerialized)
//		{
//			this.documentSerialized = documentSerialized;
//		}

//		protected override void SaveStateToBundle(IMvxBundle bundle)
//		{
//			base.SaveStateToBundle(bundle);

//			bundle.Data[DOCUMENT_SERIALIZED_BUNDLE_KEY] = this.documentSerialized;
//		}

//		protected override void ReloadFromBundle(IMvxBundle state)
//		{
//			base.ReloadFromBundle(state);

//			this.documentSerialized = state.Data[DOCUMENT_SERIALIZED_BUNDLE_KEY];
//		}

//		public override void Start()
//		{
//			base.Start();

//			this.Document = this.jsonConverter.DeserializeObject<Document>(this.documentSerialized);
//		}

//		// MVVM Properties
//		public Driver Driver
//		{
//			get
//			{
//				return this.universalObjectWrapperSingletonDriver.UniversalObject;
//			}
//			set
//			{
//				this.universalObjectWrapperSingletonDriver.UniversalObject = value;
//			}
//		}

//		public Document Document { get; set; }

//		public string ReuploadDocumentByTakingAClearPictureOrChoosingFromYourPhotoLibrary => this.GetReuploadDocumentTypeResourceString();

//		public string DocumentTypeString => this.enumHelper.GetDocumentTypeDescription(this.Document.DocumentType);



//		// MVVM Commands
//		public ICommand UploadDocumentFromCameraCommand { get; private set; }

//		public ICommand UploadDocumentFromPhotoLibraryCommand { get; private set; }

//		// public methods

//		// private methods
//		private string GetReuploadDocumentTypeResourceString()
//		{
//			string reuploadDocumentTypeResourceString;
//			switch (this.Document.DocumentType)
//			{
//				case DocumentType.DriverLicense:
//					reuploadDocumentTypeResourceString = IDriveYourCarStrings.ReuploadYourDriversLicenseByTakingAClearPictureOrChoosingFromYourPhotoLibrary;
//					break;
//				case DocumentType.VehicleInsurance:
//					reuploadDocumentTypeResourceString = IDriveYourCarStrings.ReuploadProofOfYourVehicleInsuranceByTakingAClearPictureOrChoosingFromYourPhotoLibrary;
//					break;
//				default:
//					reuploadDocumentTypeResourceString = string.Empty;
//					break;
//			}
//			return reuploadDocumentTypeResourceString;
//		}

//		private async Task UploadDocumentAsync(ImageSource source)
//		{
//			var result = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
//							source,
//							this.Document.DocumentType.ToString(),
//							Guid.NewGuid().ToString(),
//							Plugin.Media.Abstractions.PhotoSize.Full,
//							100);

//			if (result != null)
//			{
//				var fileName = Path.GetFileName(result.Path);
//				await this.fileService.UploadDocumentAsync(
//					result.GetStream(),
//					fileName,
//					this.Document.DocumentType,
//					success: (document) =>
//				{
//					this.mvxMessenger.Publish(new GenericCUDMessage<Document>(this, document, CUDOperation.Create));
//					this.CloseWithTrack(this);
//				});
//			}
//		}
//	}
//}
