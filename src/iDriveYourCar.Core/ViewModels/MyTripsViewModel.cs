﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels
{
    public class MyTripsViewModel : IDYCWithMenuActionsHostViewModel
    {
        private readonly IMvxMessenger mvxMessenger;

        public MyTripsViewModel(
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IMvxMessenger mvxMessenger)
            : base(menuNavigationActionsWrapper)
        {
            this.mvxMessenger = mvxMessenger;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ForceRefreshCommand = new MvxCommand(this.ForceRefresh);
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();

            this.UpcomingTrips = this.LoadViewModel<UpcomingTripsViewModel>();
            this.CompletedTrips = this.LoadViewModel<CompletedTripsViewModel>();
        }

        // MVVM Properties
        public UpcomingTripsViewModel UpcomingTrips { get; set; }

        public CompletedTripsViewModel CompletedTrips { get; set; }

        // MVVM Commands
        public ICommand ForceRefreshCommand { get; private set; }

        // public methods

        // private methods
        private void ForceRefresh()
        {
            this.UpcomingTrips.ForceRefresh();
            this.CompletedTrips.ForceRefresh();
        }
    }
}
