﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services.Interfaces;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class DriverUnavailabilityWeeklyViewModel : IDYCViewModel
    {
        private readonly IDriverUnavailabilityService driverUnavailabilityService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IMvxMessenger mvxMessenger;
        private readonly ISnackbarHandler snackbarHandler;

        private MvxSubscriptionToken driverLoadedSubscriptionToken, driverUnavailabilityAddedToken;

        public DriverUnavailabilityWeeklyViewModel(
            IDriverUnavailabilityService driverUnavailabilityService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IMvxMessenger mvxMessenger,
            ISnackbarHandler snackbarHandler)
        {
            this.driverUnavailabilityService = driverUnavailabilityService;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.mvxMessenger = mvxMessenger;
            this.snackbarHandler = snackbarHandler;

            this.WeeklyUnavailabilities = new ObservableCollection<DriverUnavailabilityWeeklyDayItemViewModel>();

            // driver loaded
            this.driverLoadedSubscriptionToken = this.mvxMessenger.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message => this.FillWeeklyUnavailabilities());

            // time added or removed
            this.driverUnavailabilityAddedToken = this.mvxMessenger.SubscribeOnMainThread<GenericCUDMessage<DriverUnavailabilityWeeklyTimeItemViewModel>>(
                message =>
                {
                    switch(message.Operation)
                    {
                        case CUDOperation.Create:
                            this.AddDriverUnavailabilityTime(message.Target);
                            break;
                        case CUDOperation.Delete:
                            this.RemoveDriverUnavailabilityTime(message.Target);
                            break;
                    }

                    this.UpdateUnavailabilityTaskCompletion = NotifyTaskCompletion.Create(this.PersistUnavailabilitiesAsync);
                });
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.AddUnavailabilityCommand = new MvxCommand(this.RequestAddUnavailability);
        }

        // MVX Life cycle
        public void Init()
        {

        }

        public override void Start()
        {
            base.Start();

            if(this.Driver != null)
                this.FillWeeklyUnavailabilities();
        }

        public override void Destroy()
        {
            base.Destroy();

            // TODO: Find right place to put this things
            //this.driverLoadedSubscriptionToken?.Dispose();
            //this.driverLoadedSubscriptionToken = null;

            //this.driverUnavailabilityAddedToken?.Dispose();
            //this.driverUnavailabilityAddedToken = null;
        }

        // MVVM Properties
        public ObservableCollection<DriverUnavailabilityWeeklyDayItemViewModel> WeeklyUnavailabilities { get; set; }

        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        public INotifyTaskCompletion UpdateUnavailabilityTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand AddUnavailabilityCommand { get; private set; }

        // public methods


        // private methods
        private void FillWeeklyUnavailabilities()
        {
            this.WeeklyUnavailabilities.Clear();

            foreach(var driverUnavailability in this.Driver.WeeklyUnavailability)
                this.AddDriverUnavailabilityTime(new DriverUnavailabilityWeeklyTimeItemViewModel(this.mvxMessenger, driverUnavailability));
        }

        private void RequestAddUnavailability()
        {
            this.ShowViewModelWithTrack<DriverUnavailabilityWeeklyCreationViewModel>();
        }

        private void RemoveDriverUnavailabilityDay(DriverUnavailabilityWeeklyDayItemViewModel itemViewModel)
        {
            if(this.WeeklyUnavailabilities.Contains(itemViewModel))
                this.WeeklyUnavailabilities.Remove(itemViewModel);
        }

        private void AddDriverUnavailabilityTime(DriverUnavailabilityWeeklyTimeItemViewModel itemViewModel)
        {
            var currentDay = this.WeeklyUnavailabilities.SingleOrDefault(w => w.DayOfWeek == itemViewModel.DriverUnavailability.Day);
            if(currentDay == null)
            {
                currentDay = new DriverUnavailabilityWeeklyDayItemViewModel(this.mvxMessenger)
                {
                    DayOfWeek = itemViewModel.DriverUnavailability.Day
                };
                this.WeeklyUnavailabilities.Add(currentDay);

                var weekOrderSecured = this.WeeklyUnavailabilities.OrderBy(w => w.DayOfWeek).ToList();

                this.WeeklyUnavailabilities.Fill(weekOrderSecured);
            }
            currentDay.DriverUnavailabilityTimes.Add(itemViewModel);

            this.RaisePropertyChanged(() => this.WeeklyUnavailabilities);
        }

        private void RemoveDriverUnavailabilityTime(DriverUnavailabilityWeeklyTimeItemViewModel itemViewModel)
        {
            foreach(var day in this.WeeklyUnavailabilities)
            {
                if(day.DriverUnavailabilityTimes.Contains(itemViewModel))
                {
                    day.DriverUnavailabilityTimes.Remove(itemViewModel);
                    if(!day.DriverUnavailabilityTimes.Any())
                        this.RemoveDriverUnavailabilityDay(day);

                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.TimeBlockRemoved);

                    this.RaisePropertyChanged(() => this.WeeklyUnavailabilities);

                    return;
                }
            }
        }

        private async Task PersistUnavailabilitiesAsync()
        {
            var unavailabilities = new List<DriverUnavailabilityWeek>();
            foreach(var day in this.WeeklyUnavailabilities)
                unavailabilities.AddRange(day.DriverUnavailabilityTimes.Select(times => times.DriverUnavailability));

            await this.driverUnavailabilityService.UpdateDriverWeeklyUnavailabilityAsync(
                unavailabilities,
                this.Driver.MaxDrivingTime,
                success: updatedDriver => this.universalObjectWrapperSingletonDriver.UniversalObject = updatedDriver);
        }
    }
}