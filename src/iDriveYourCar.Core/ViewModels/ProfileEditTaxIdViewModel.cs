﻿using System;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules.ProfileGeneral;
using iDriveYourCar.Core.Services;
using DGenix.Mobile.Fwk.Core.ViewModels.Adapters;

namespace iDriveYourCar.Core.ViewModels
{
    public class ProfileEditTaxIdViewModel : ProfileEditFieldViewModel
    {
        public ProfileEditTaxIdViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IDriverService driverService,
            IDriverEditableFieldConfigurationFactory driverEditableFieldConfigurationFactory,
            ISnackbarHandler snackbarHandler)
            : base(
                universalObjectWrapperSingletonDriver,
                driverService,
                driverEditableFieldConfigurationFactory,
                snackbarHandler)
        {
        }

        public override void Start()
        {
            base.Start();

            this.TaxAdapter = new EnumAdapterViewModel<TaxClassification, TaxClassificationEnumWrapper>(
                this.OnTaxClassificationSelected,
                defaultSelected: new TaxClassificationEnumWrapper { Value = this.universalObjectWrapperSingletonDriver.UniversalObject.TaxId });
            this.TaxAdapter.Start();
        }

        // MVVM Properties
        public EnumAdapterViewModel<TaxClassification, TaxClassificationEnumWrapper> TaxAdapter { get; set; }

        // MVVM Commands

        // public methods

        // private methods
        private void OnTaxClassificationSelected(TaxClassificationEnumWrapper obj)
        {
            this.universalObjectWrapperSingletonDriver.UniversalObject.TaxId = obj.Value.Value;
        }
    }
}
