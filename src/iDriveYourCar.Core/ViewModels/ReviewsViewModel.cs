﻿using System.Linq;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.ViewModels
{
    public class ReviewsViewModel : IDYCListWithMenuActionsViewModel<IDriverService, Review, ReviewItemViewModel, ReviewCriteria>
    {
        private readonly IDriverService driverService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton;

        public ReviewsViewModel(
            IFwkBaseListViewModelServiceBridge<IDriverService, Review, ReviewCriteria> reviewsBridge,
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IDriverService driverService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton)
            : base(reviewsBridge, menuNavigationActionsWrapper)
        {
            this.driverService = driverService;
            this.universalObjectWrapperSingleton = universalObjectWrapperSingleton;
        }

        // MVX Life cycle
        public override void Init()
        {
            base.Init();
        }

        public override void Start()
        {
            base.Start();
        }

        // Properties

        //TODO: Remove hardcoded values
        //public long TotalRating => 58;
        //public long ReviewsCount => 18;
        public long TotalRating => this.Driver.RateFiveStars * 5 + this.Driver.RateFourStars * 4 + this.Driver.RateThreeStars * 3 + this.Driver.RateTwoStars * 2 + this.Driver.RateOneStar;
        public long ReviewsCount => this.Driver.RateFiveStars + this.Driver.RateFourStars + this.Driver.RateThreeStars + this.Driver.RateTwoStars + this.Driver.RateOneStar;

        public decimal AverageRating { get; set; }

        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingleton.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingleton.UniversalObject = value;
            }
        }


        // MVVM Commands

        // public methods

        // private methods
        protected override void AfterLoadingItemsToCollection()
        {
            base.AfterLoadingItemsToCollection();

            this.UpdateAverageRating();
        }

        private void UpdateAverageRating()
        {
            //TODO: Remove hardcoded values
            //this.Driver.RateFiveStars = 3;
            //this.Driver.RateFourStars = 4;
            //this.Driver.RateThreeStars = 8;
            //this.Driver.RateTwoStars = 1;
            //this.Driver.RateOneStar = 2;

            if (this.Driver == null || this.Items == null || !this.Items.Any())
                return;

            if (this.TotalRating == 0 || this.ReviewsCount == 0)
                return;

            this.AverageRating = this.TotalRating / this.ReviewsCount;
        }

        protected override ReviewCriteria CreateCriteria(bool isFetching = false)
        {
            return new ReviewCriteria
            {
                Page = this.nextPage
            };
        }

        protected override ReviewItemViewModel GetItemFromResultModel(Review model) => new ReviewItemViewModel(model);
    }
}
