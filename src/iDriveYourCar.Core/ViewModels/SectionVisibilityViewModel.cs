﻿using System.Windows.Input;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
    public class SectionVisibilityViewModel : MvxNotifyPropertyChanged
    {
        public SectionVisibilityViewModel()
        {
            this.IsVisible = true;
            this.ToggleVisibilityCommand = new MvxCommand(() => ToggleVisibility());
        }

        public bool IsVisible { get; set; }

        public ICommand ToggleVisibilityCommand { get; private set; }

        private void ToggleVisibility()
        {
            this.IsVisible = !this.IsVisible;
        }
    }
}