﻿using System.Threading.Tasks;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Services;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels
{
    public class InvitesViewModel : IDYCWithMenuActionsHostViewModel
    {
        private readonly IReferralsService referralService;
        private readonly IMvxMessenger mvxMessenger;

        public InvitesViewModel(
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IReferralsService referralService,
            IMvxMessenger mvxMessenger)
            : base(menuNavigationActionsWrapper)
        {
            this.referralService = referralService;
            this.mvxMessenger = mvxMessenger;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();

            this.ReferAClient = this.LoadViewModel<ReferAClientViewModel>();
            this.ReferADriver = this.LoadViewModel<ReferADriverViewModel>();

            this.CreateLoadTask(this.GetDriverReferralsAsync);
        }

        // MVVM Properties
        public ReferAClientViewModel ReferAClient { get; set; }

        public ReferADriverViewModel ReferADriver { get; set; }

        // MVVM Commands

        // public methods

        // private methods
        private async Task GetDriverReferralsAsync()
        {
            await this.referralService.GetAllReferralsAsync(
                cacheSuccess: referrals => this.mvxMessenger.Publish(new ReferralsLoadedMessage(this, referrals)),
                success: referrals => this.mvxMessenger.Publish(new ReferralsLoadedMessage(this, referrals)));
        }
    }
}