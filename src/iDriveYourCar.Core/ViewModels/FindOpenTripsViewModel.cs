using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Rules.OpenTrips;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.Messenger;
using DGenix.Mobile.Fwk.Core.MvxMessages;

namespace iDriveYourCar.Core.ViewModels
{
    public class FindOpenTripsViewModel : IDYCWithMenuActionsViewModel
    {
        private readonly ITripsService tripsService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IOpenTripItemViewModelFactory openTripItemViewModelFactory;
        private readonly IGeolocatorHelper geolocatorHelper;
        private readonly IMvxMessenger mvxMessenger;
        private readonly IMvxJsonConverter jsonConverter;

        private OpenTripCriteria filterCriteria;
        private MvxSubscriptionToken filterChangedSubscription, tripAcceptedSubscription;

        public FindOpenTripsViewModel(
            ITripsService tripsService,
            IOpenTripItemViewModelFactory openTripItemViewModelFactory,
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IGeolocatorHelper geolocatorHelper,
            IMvxMessenger mvxMessenger,
            IMvxJsonConverter jsonConverter)
            : base(menuNavigationActionsWrapper)
        {
            this.tripsService = tripsService;
            this.openTripItemViewModelFactory = openTripItemViewModelFactory;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.geolocatorHelper = geolocatorHelper;
            this.mvxMessenger = mvxMessenger;
            this.jsonConverter = jsonConverter;

            this.OpenTrips = new ObservableCollection<IBaseTripItemViewModel>();

            this.filterChangedSubscription = this.mvxMessenger.SubscribeOnMainThread<FilterOpenTripsUpdatedMessage>(
                message => this.HandleCriteriaChanged(message.Criteria));

            this.tripAcceptedSubscription = this.mvxMessenger.SubscribeOnMainThread<GenericCUDMessage<Trip>>(
                message =>
            {
                var toRemove = this.OpenTrips?.Where(t => t is OpenTripItemViewModel)?.Select(t => t as OpenTripItemViewModel)?.FirstOrDefault(viewModel => viewModel.Trip.Id == message.Target.Id);
                if(toRemove != null)
                    this.OpenTrips.Remove(toRemove);
            });
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.DisplayOpenTripDetailCommand = new MvxCommand<IBaseTripItemViewModel>(this.DisplayOpenTripDetail);

            this.ToggleFilterOpenTripsCommand = new MvxCommand(this.ToggleFilterOpenTrips);

            this.LoadTripsDataCommand = new MvxCommand(() => this.CreateLoadTask(this.LoadTripsDataAsync),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.LoadTask));
        }

        // MVX Life cycle
        public void Init()
        {

        }

        public override void Start()
        {
            base.Start();

            this.CreateFilterCriteria();

            this.CreateLoadTask(this.LoadTripsDataAsync);
        }

        public override void Destroy()
        {
            base.Destroy();

            this.filterChangedSubscription?.Dispose();
            this.filterChangedSubscription = null;

            this.tripAcceptedSubscription?.Dispose();
            this.tripAcceptedSubscription = null;
        }

        // MVVM Properties
        public ObservableCollection<IBaseTripItemViewModel> OpenTrips { get; set; }

        public string SelectedDateRange => $"{this.filterCriteria.StartDate.ToFormattedString(DateStringFormat.ShortMonthNumberYear)}";//- {this.filterCriteria.EndDate.ToFormattedString(DateStringFormat.ShortMonthNumberYear)}";

        public bool IsFilterApplied { get; set; }

        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        // MVVM Commands
        public ICommand DisplayOpenTripDetailCommand { get; private set; }

        public ICommand LoadTripsDataCommand { get; private set; }

        public ICommand ToggleFilterOpenTripsCommand { get; private set; }

        // public methods

        // private methods
        private void DisplayOpenTripDetail(IBaseTripItemViewModel tripItemViewModel)
        {
            var openTripItemViewModel = tripItemViewModel as OpenTripItemViewModel;

            if(openTripItemViewModel == null)
                return;

            var tripSerialized = this.jsonConverter.SerializeObject(openTripItemViewModel.Trip);
            this.ShowViewModelWithTrack<FindOpenTripsDetailViewModel>(new { tripSerialized });

            //var tripPush = this.MapFromOpenTrip(openTripItemViewModel.Trip);
            //var tripPushSerialized = this.jsonConverter.SerializeObject(tripPush);
            //this.ShowViewModelWithTrack<TripAcceptanceViewModel>(new { tripPushSerialized });
        }

        //private TripPush MapFromOpenTrip(Trip openTrip)
        //{
        //    if(openTrip == null)
        //        return null;

        //    return new TripPush
        //    {
        //        Id = openTrip.Id,
        //        Type = openTrip.TripType,
        //        PickupAddress = openTrip.PickupAddress != null ? $"{openTrip.PickupAddress.City}, {openTrip.PickupAddress.State}" : string.Empty,
        //        DropoffAddress = openTrip.DropoffAddress != null ? $"{openTrip.DropoffAddress.City}, {openTrip.DropoffAddress.State}" : string.Empty,
        //        EstimatedTime = 0,
        //        TimeFromHome = 0,
        //        PickupDate = openTrip.PickupDate,
        //        PickupTime = openTrip.PickupTime,
        //        Return = this.MapFromOpenTrip(openTrip.TripReturn)
        //    };
        //}

        private void CreateFilterCriteria()
        {
            this.filterCriteria = new OpenTripCriteria
            {
                MaxDrivingTime = 1000000,
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(7)
            };
        }

        private void HandleCriteriaChanged(OpenTripCriteria criteria)
        {
            this.IsFilterApplied = true;

            this.filterCriteria = criteria;

            this.RaisePropertyChanged(() => this.SelectedDateRange);

            this.CleanLoadTask();
            this.CreateLoadTask(this.LoadTripsDataAsync);
        }

        private async Task LoadTripsDataAsync()
        {
            var position = await this.geolocatorHelper.GetCurrentPositionAsync(timeoutMilliseconds: 5000);
            this.filterCriteria.Latitude = position != null ? (decimal)position.Latitude : (decimal?)null;
            this.filterCriteria.Longitude = position != null ? (decimal)position.Longitude : (decimal?)null;

            // TODO: Delete! mock criteria
            this.filterCriteria.Latitude = (decimal)41.074340;
            this.filterCriteria.Longitude = (decimal)-73.523479;

            //Mock Trips
            //this.OpenTrips.Clear();
            //this.MockFindOpenTrips();

            await this.tripsService.GetOpenTripsAsync(
                this.filterCriteria,
                success: this.HandleLoadOpenTrips);
        }

        private void HandleLoadOpenTrips(IEnumerable<Trip> openTrips)
        {
            this.OpenTrips.Clear();

            if(!openTrips.Any())
                return;

            var openTripsGroupByPickupDate = openTrips.OrderBy(openTrip => openTrip.PickupDate.ToDateTimeFromFormat()).ToList();

            string currentPickupDate = null;

            foreach(var openTrip in openTripsGroupByPickupDate)
            {
                if(currentPickupDate != openTrip.PickupDate)
                {
                    var pickupDate = openTrip.PickupDate.ToDateTimeFromFormat();

                    this.OpenTrips.Add(this.openTripItemViewModelFactory.Create(pickupDate));
                    currentPickupDate = openTrip.PickupDate;
                }

                this.OpenTrips.Add(this.openTripItemViewModelFactory.Create(openTrip));
            }
        }

        private void ToggleFilterOpenTrips()
        {
            if(this.IsFilterApplied)
            {
                this.IsFilterApplied = false;

                this.CreateFilterCriteria();

                this.RaisePropertyChanged(() => this.SelectedDateRange);

                this.CleanLoadTask();
                this.CreateLoadTask(this.LoadTripsDataAsync);
            }
            else
            {
                var criteriaSerialized = this.jsonConverter.SerializeObject(this.filterCriteria);

                this.ShowViewModelWithTrack<FilterFindOpenTripsViewModel>(new { criteriaSerialized });
            }
        }

        /*
        private void MockFindOpenTrips()
        {
            this.OpenTrips.Add(new DateOpenTripItemViewModel(new DateTime(2017, 03, 21)));
            var trip1 = new Trip
            {
                Id = 1,
                FieldsChanged = new List<TripFieldChanged>
                {
                    TripFieldChanged.DropoffTimeEstimated,
                    TripFieldChanged.MeetupInstructions,
                    TripFieldChanged.NotesForDriver,
                    TripFieldChanged.VehicleLocation,
                    TripFieldChanged.Offer,
                    TripFieldChanged.DropoffAddress,
                    TripFieldChanged.KeyLocationVehicleDescription,
                    TripFieldChanged.PickupAddress,
                    TripFieldChanged.PickupDate,
                    TripFieldChanged.PublicNotes,
                    TripFieldChanged.Stops,
                    TripFieldChanged.PickupTime
                },
                TripReturn = new Trip
                {
                    Id = 2,
                    Duration = (decimal?)48.222556,
                    Distance = 12,
                    TripType = TripType.AirportDropoff,
                    PickupDate = "Wednesday March 22, 2017",
                    PickupTime = "6:30 pm",
                    KeyLocationVehicleDescription = "New York, NY",
                    PublicNotesBlobbed = "",
                    ToAirport = new Airport
                    {
                        Name = "JFK John F. Kennedy Intl Airport",
                        Lat = (decimal?)40.6413151,
                        Lng = (decimal?)(-73.7803278)
                    },
                    ToAirline = new Airline
                    {
                        Name = "LAN Airlines (AA) 101"
                    },
                    Client = new Client
                    {
                        FirstName = "Albert",
                        LastName = "Jhonsson"
                    },
                    PickupAddress = new Address
                    {
                        Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                        City = "Washington",
                        Country = "WA",
                        State = "WA",
                        Lat = (decimal?)40.660264,
                        Lng = (decimal?)(-73.7788892)
                    },
                    DropoffAddress = new Address
                    {
                        Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                        City = "Washington",
                        Country = "WA",
                        State = "WA",
                        Lat = (decimal?)40.660264,
                        Lng = (decimal?)(-73.7788892)
                    },
                    Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                  }
                },
                Duration = (decimal?)48.222556,
                TripType = TripType.WaitAndReturn,
                PickupDate = "Tuesday March 21, 2017",
                PickupTime = "6:30 pm",
                KeyLocationVehicleDescription = "New York, NY",
                MeetupInstructions = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                PublicNotesBlobbed = "Client has requested the daily New York Times, a large Starbucks coffe with 1 milk and 2 sugars & 3 Sausage Egg McMuffin from McDonalds.",
                Client = new Client
                {
                    FirstName = "Albert",
                    LastName = "Jhonsson",
                    AccountNotes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                    NotesForDriver = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur."
                },
                PickupAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                },
                DropoffAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                },
                Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                }
            };
            this.OpenTrips.Add(new OpenTripItemViewModel(trip1));
            this.OpenTrips.Add(new DateOpenTripItemViewModel(new DateTime(2017, 03, 22)));
            var trip2 = new Trip
            {
                Id = 2,
                Duration = (decimal?)48.222556,
                TripType = TripType.AirportDropoff,
                PickupDate = "Wednesday March 22, 2017",
                PickupTime = "6:30 pm",
                KeyLocationVehicleDescription = "New York, NY",
                PublicNotesBlobbed = "",
                ToAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)40.6413151,
                    Lng = (decimal?)(-73.7803278)
                },
                ToAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101"
                },
                Client = new Client
                {
                    FirstName = "Albert",
                    LastName = "Jhonsson"
                },
                PickupAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                },
                Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      },
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                  }
            };
            this.OpenTrips.Add(new OpenTripItemViewModel(trip2));
            this.OpenTrips.Add(new DateOpenTripItemViewModel(new DateTime(2017, 03, 24)));
            var trip3 = new Trip
            {
                Id = 3,
                Duration = (decimal?)48.222556,
                TripType = TripType.AirportPickup,
                PickupDate = "Friday March 24, 2017",
                PickupTime = "6:30 pm",
                FromAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)40.6413151,
                    Lng = (decimal?)(-73.7803278)
                },
                FromAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101"
                },
                KeyLocationVehicleDescription = "New York, NY",
                PublicNotesBlobbed = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                MeetupInstructions = null,
                Client = new Client
                {
                    FirstName = "Albert",
                    LastName = "Jhonsson"
                },
                Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                  },
                DropoffAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                }
            };
            this.OpenTrips.Add(new OpenTripItemViewModel(trip3));
            var trip4 = new Trip
            {
                Id = 4,
                Duration = (decimal?)48.222556,
                TripType = TripType.AirportPickup,
                PickupDate = "Friday March 24, 2017",
                PickupTime = "6:30 pm",
                FromAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)40.6413151,
                    Lng = (decimal?)(-73.7803278)
                },
                FromAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101"
                },
                KeyLocationVehicleDescription = "New York, NY",
                PublicNotesBlobbed = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                MeetupInstructions = null,
                Client = new Client
                {
                    FirstName = "Albert",
                    LastName = "Jhonsson"
                },
                Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                        Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                  },
                DropoffAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                }
            };
            this.OpenTrips.Add(new OpenTripItemViewModel(trip4));
            var trip5 = new Trip
            {
                Id = 5,
                Duration = (decimal?)48.222556,
                TripType = TripType.AirportPickup,
                PickupDate = "Friday March 24, 2017",
                PickupTime = "6:30 pm",
                FromAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)40.6413151,
                    Lng = (decimal?)(-73.7803278)
                },
                FromAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101"
                },
                KeyLocationVehicleDescription = "New York, NY",
                PublicNotesBlobbed = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                MeetupInstructions = null,
                Client = new Client
                {
                    FirstName = "Albert",
                    LastName = "Jhonsson"
                },
                Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                       Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                  },
                DropoffAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                }
            };
            this.OpenTrips.Add(new OpenTripItemViewModel(trip5));
            var trip6 = new Trip
            {
                Id = 6,
                Duration = (decimal?)48.222556,
                TripType = TripType.AirportPickup,
                PickupDate = "Friday March 24, 2017",
                PickupTime = "6:30 pm",
                FromAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)40.6413151,
                    Lng = (decimal?)(-73.7803278)
                },
                FromAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101"
                },
                KeyLocationVehicleDescription = "New York, NY",
                PublicNotesBlobbed = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                MeetupInstructions = null,
                Client = new Client
                {
                    FirstName = "Albert",
                    LastName = "Jhonsson"
                },
                Stops = new List<TripStop>
                  {
                      new TripStop
                      {
                       Address = new Address
                          {
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            City = "Washington",
                            Country = "WA",
                            State = "WA",
                            Lat = (decimal?)40.660264,
                            Lng = (decimal?)(-73.7788892)
                          }
                      }
                  },
                DropoffAddress = new Address
                {
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    City = "Washington",
                    Country = "WA",
                    State = "WA",
                    Lat = (decimal?)40.660264,
                    Lng = (decimal?)(-73.7788892)
                }
            };
            this.OpenTrips.Add(new OpenTripItemViewModel(trip6));
        }
    */
    }
}
