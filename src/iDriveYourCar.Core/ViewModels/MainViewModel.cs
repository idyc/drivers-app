using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.PushNotifications;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using Nito.AsyncEx;
using DGenix.Mobile.Fwk.Core.Handlers;

namespace iDriveYourCar.Core.ViewModels
{
    public class MainViewModel : IDYCViewModel
    {
        private const string HINT_KEY = "hint";

        private readonly IDriverService driverService;
        private readonly IGeolocatorHelper geolocatorHelper;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IUserInformationService userInformationService;
        private readonly ISecureStorageHelper secureStorageHelper;
        private readonly IIDYCSettings idycSettings;
        private readonly IChatService messageService;
        private readonly IMvxJsonConverter jsonConverter;
        private readonly IAlertHandler alertHandler;

        private string hintSerialized;
        private StartupHint pushHint;

        public MainViewModel(
            IDriverService driverService,
            IGeolocatorHelper geolocatorHelper,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IUserInformationService userInformationService,
            ISecureStorageHelper secureStorageHelper,
            IIDYCSettings idycSettings,
            IChatService messageService,
            IMvxJsonConverter jsonConverter,
            IAlertHandler alertHandler)
        {
            this.driverService = driverService;
            this.geolocatorHelper = geolocatorHelper;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.userInformationService = userInformationService;
            this.secureStorageHelper = secureStorageHelper;
            this.idycSettings = idycSettings;
            this.messageService = messageService;
            this.jsonConverter = jsonConverter;
            this.alertHandler = alertHandler;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ShowInitialViewModelsCommand = new MvxCommand(this.ShowInitialViewModels);
        }

        // MVX Life cycle
        public void Init(string hint)
        {
            this.hintSerialized = hint;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[HINT_KEY] = this.hintSerialized;
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.hintSerialized = state.Data[HINT_KEY];
        }

        public override void Start()
        {
            base.Start();

            this.pushHint = this.hintSerialized != null
                ? this.jsonConverter.DeserializeObject<StartupHint>(this.hintSerialized)
                : new StartupHint();

            this.CreateLoadTask(this.LoadCachedUserDataAsync);

            this.VerifySessionTaskCompletion = NotifyTaskCompletion.Create(this.VerifySessionAsync);
        }

        // MVVM Properties
        public INotifyTaskCompletion LoadUserDataTaskCompletion { get; private set; }

        public INotifyTaskCompletion SendDeviceIdTaskCompletion { get; private set; }

        //public INotifyTaskCompletion SendLocationTaskCompletion { get; private set; }

        public INotifyTaskCompletion VerifySessionTaskCompletion { get; private set; }

        public INotifyTaskCompletion ObtainTwilioChatTokenTaskCompletion { get; private set; }

        // this one does not require a spinner on the UI
        public INotifyTaskCompletion LoadDriverDataFromServerTaskCompletion { get; private set; }

        // this one requires a spinner on the UI
        public INotifyTaskCompletion LoadDriverDataFromServerSpinnerTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand ShowInitialViewModelsCommand { get; private set; }

        // public methods

        // private methods
        private void ShowInitialViewModels()
        {
            this.ShowViewModelWithTrack<MenuViewModel>();

            new PushNotificationNavigator(this.alertHandler, this.jsonConverter)
                .PerformNavigationByHint(this.pushHint);
        }

        private Task LoadCachedUserDataAsync()
        {
            return this.driverService.GetCachedDriverDataAsync(
                startDate: this.GetFirstDayOfPreviousWeek(),
                endDate: this.GetLastDayOfCurrentWeek(),
                success: driver =>
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = driver;
            });
        }

        private Task LoadUserDataAsync()
        {
            return this.driverService.GetDriverDataAsync(
                startDate: this.GetFirstDayOfPreviousWeek(),
                endDate: this.GetLastDayOfCurrentWeek(),
                success: driver => this.universalObjectWrapperSingletonDriver.UniversalObject = driver);
        }

        private async Task SendDeviceIdAsync()
        {
            if(string.IsNullOrEmpty(this.idycSettings.PushNotificationsToken))
                return;

            await this.driverService.UpdateDeviceIdAsync(
                this.idycSettings.PushNotificationsToken,
                success: () => { });
        }

        //private async Task SendDriverLocationAsync()
        //{
        //    var position = await this.geolocatorHelper.GetCurrentPositionAsync().ConfigureAwait(false);

        //    if(position == null)
        //        return;

        //    await this.driverService.UpdateDriverLocationAsync(
        //        position,
        //        success: () => { });
        //}

        private async Task VerifySessionAsync()
        {
            var userValues = this.secureStorageHelper.GetUserValuesOrDefault();

            try
            {
                await this.userInformationService.VerifyUserAsync(
                    userValues.Item1,
                    userValues.Item2,
                    success: user => this.HandleVerifyUserSuccess());
            }
            catch(Exception ex) when(ex is BusinessException || ex is UnauthorizedException)
            {
                await this.userInformationService.LogoutAsync(
                           success: () => this.ShowViewModelWithTrack<LoginViewModel>());
            }
        }

        private void HandleVerifyUserSuccess()
        {
            if(this.universalObjectWrapperSingletonDriver.UniversalObject == null)
                this.LoadDriverDataFromServerSpinnerTaskCompletion = NotifyTaskCompletion.Create(this.LoadUserDataAsync);
            else
                this.LoadDriverDataFromServerTaskCompletion = NotifyTaskCompletion.Create(this.LoadUserDataAsync);

            this.SendDeviceIdTaskCompletion = NotifyTaskCompletion.Create(this.SendDeviceIdAsync);
            //this.SendLocationTaskCompletion = NotifyTaskCompletion.Create(this.SendDriverLocationAsync);
            this.ObtainTwilioChatTokenTaskCompletion = NotifyTaskCompletion.Create(this.ObtainTwilioChatTokenAsync);
        }

        private async Task ObtainTwilioChatTokenAsync()
        {
            await this.messageService.GetTwilioChatToken(
                this.idycSettings.PushNotificationsToken,
                success: token => { });
        }

        #region helper methods
        private DateTime GetFirstDayOfPreviousWeek()
        {
            return DateTime.Today.AddDays(-7).GetFirstDateOfWeek();
        }

        private DateTime GetLastDayOfCurrentWeek()
        {
            return DateTime.Today.GetLastDateOfWeek();
        }
        #endregion
    }
}