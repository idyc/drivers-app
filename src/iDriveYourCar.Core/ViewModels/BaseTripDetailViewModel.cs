﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
    public class BaseTripDetailViewModel : IDYCViewModel
    {
        private readonly IExternalMapHandler externalMapHandler;
        private readonly IAlertHandler alertHandler;
        private readonly IAppStoreHandler appStoreHandler;

        public BaseTripDetailViewModel(
            IExternalMapHandler externalMapHandler,
            IAlertHandler alertHandler,
            IAppStoreHandler appStoreHandler)
        {
            this.externalMapHandler = externalMapHandler;
            this.alertHandler = alertHandler;
            this.appStoreHandler = appStoreHandler;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.DisplayPickupDirectionCommand = new MvxCommand(this.DisplayPickupDirection);
            this.DisplayDropoffDirectionCommand = new MvxCommand(this.DisplayDropoffDirection);

            this.DisplayStopDirectionCommand = new MvxCommand<TripStopItemViewModel>((tripStopItemViewModel) => this.DisplayStopDirection(tripStopItemViewModel));

            this.ShowReturnTripDetailsCommand = new MvxCommand(this.ShowReturnTripDetails);
        }

        // MVX Life cycle
        public void Init()
        {

        }

        public override void Start()
        {
            base.Start();

            this.Stops = new ObservableCollection<TripStopItemViewModel>();

            this.FillStops();
        }

        // MVVM Properties
        public Trip Trip { get; set; }

        public string PickupDate => DateTime.Parse(this.Trip?.PickupDate).ToFormattedString(DateStringFormat.DayMonthDayNumber);

        public string PickupTime => DateTime.Parse(this.Trip?.PickupTime).ToFormattedString(DateStringFormat.TimeAMPM);

        public string Pickup => this.Trip.TripType == TripType.AirportPickup ? this.Trip.FromAirport.Name : this.Trip.PickupAddress.NameCityAndStateFormatted;

        public string Dropoff => this.Trip.TripType == TripType.AirportDropoff ? this.Trip.ToAirport.Name : this.Trip.DropoffAddress.NameCityAndStateFormatted;

        public ObservableCollection<TripStopItemViewModel> Stops { get; set; }

        // MVVM Commands
        public ICommand DisplayPickupDirectionCommand { get; private set; }

        public ICommand DisplayDropoffDirectionCommand { get; private set; }

        public ICommand DisplayStopDirectionCommand { get; private set; }

        public ICommand ShowReturnTripDetailsCommand { get; private set; }

        // public methods

        // private methods
        private void FillStops()
        {
            if(this.Trip?.Stops != null)
            {
                foreach(var stop in this.Trip.Stops)
                {
                    this.Stops.Add(new TripStopItemViewModel(stop, this.Trip));
                }
            }
        }

        private void DisplayPickupDirection()
        {
            Tuple<decimal?, decimal?> pickupAddress = this.Trip.TripType == TripType.AirportPickup
                                                        ? new Tuple<decimal?, decimal?>(this.Trip.FromAirport.Lat, this.Trip.FromAirport.Lng)
                                                        : new Tuple<decimal?, decimal?>(this.Trip.PickupAddress.Lat, this.Trip.PickupAddress.Lng);

            this.DisplayAddress(pickupAddress);
        }

        private void DisplayStopDirection(TripStopItemViewModel tripStopItemViewModel)
        {
            this.DisplayAddress(new Tuple<decimal?, decimal?>(tripStopItemViewModel.TripStop.Address.Lat, tripStopItemViewModel.TripStop.Address.Lng));
        }

        private void DisplayDropoffDirection()
        {
            Tuple<decimal?, decimal?> dropoffAddress = this.Trip.TripType == TripType.AirportDropoff
                                                           ? new Tuple<decimal?, decimal?>(this.Trip.ToAirport.Lat, this.Trip.ToAirport.Lng)
                                                           : new Tuple<decimal?, decimal?>(this.Trip.DropoffAddress.Lat, this.Trip.DropoffAddress.Lng);

            this.DisplayAddress(dropoffAddress);
        }

        private void DisplayAddress(Tuple<decimal?, decimal?> address)
        {
            if(!address.Item1.HasValue || !address.Item2.HasValue)
                return;

            var canOpenGoogleMaps = this.externalMapHandler.TryDisplayDirection(address.Item1.Value, address.Item2.Value);

            if(!canOpenGoogleMaps)
            {
                //It will never be prompted on iOS because if you don't have Google Maps installed, Apple Maps will be launched
                this.alertHandler.ShowMessage(
                    IDriveYourCarStrings.GoogleMapsIsRequired,
                    IDriveYourCarStrings.PleaseInstallGoogleMaps,
                    IDriveYourCarStrings.Download,
                    IDriveYourCarStrings.Cancel,
                    () => this.appStoreHandler.ShowAppStoreGoogleMaps(),
                    () => { }
                );
            }
        }

        private void ShowReturnTripDetails()
        {
            if(this.Trip.TripReturn != null)
                this.ShowViewModelWithTrack<UpcomingTripDetailViewModel>(new { tripId = this.Trip.TripReturn.Id });
        }
    }
}
