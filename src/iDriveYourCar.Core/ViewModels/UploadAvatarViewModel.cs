﻿using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class UploadAvatarViewModel : IDYCViewModel
    {
        private readonly IImageHelper imageHelper;
        private readonly IFileService fileService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;

        public UploadAvatarViewModel(
            IImageHelper imageHelper,
            IFileService fileService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver)
        {
            this.imageHelper = imageHelper;
            this.fileService = fileService;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.UploadPictureCommand = new MvxCommand(() => this.UploadFileTask = NotifyTaskCompletion.Create(this.UploadPictureAsync),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.UploadFileTask));
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public INotifyTaskCompletion UploadFileTask { get; private set; }

        // MVVM Commands
        public ICommand UploadPictureCommand { get; private set; }

        // public methods

        // private methods
        private async Task UploadPictureAsync()
        {
            var file = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
                ImageSource.Gallery,
                "avatarCacheKey",
                "avatar",
                Plugin.Media.Abstractions.PhotoSize.Large,
                70);

            if(file != null)
            {
                await this.fileService.UploadAvatarAsync(
                    file.GetStream(),
                    Path.GetFileName(file.Path),
                    success: driver =>
                    {
                        this.universalObjectWrapperSingletonDriver.UniversalObject = driver;
                        this.CloseWithTrack(this);
                    });
            }
        }
    }
}
