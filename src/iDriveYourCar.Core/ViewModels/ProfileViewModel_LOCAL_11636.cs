﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
	public class ProfileViewModel : IDYCHostViewModel
	{
		private const int NUMBER_OF_SECTIONS = 4;
		private readonly IDriverService driverService;
		private readonly IAlertHandler alertHandler;

		public ProfileViewModel(
			IDriverService driverService,
			IAlertHandler alertHandler)
		{
			this.driverService = driverService;
			this.alertHandler = alertHandler;
		}

		protected override void InitializeCommands()
		{
			base.InitializeCommands();

			this.AddMobileCommand = new MvxCommand(() => this.AddNew(this.MobileNumbers));
			this.AddEmailCommand = new MvxCommand(() => this.AddNew(this.Emails));
			this.AddHomeCommand = new MvxCommand(() => this.AddNew(this.HomeNumbers));
			this.AddAddressCommand = new MvxCommand(() => this.AddNew(this.Addresses));

			this.SaveMobileCommand = new MvxCommand(() => { });
			this.SaveEmailCommand = new MvxCommand(() => { });
			this.SaveHomeCommand = new MvxCommand(() => { });
			this.SaveAddressCommand = new MvxCommand(() => { });

			//this.RemoveMobileCommand = new MvxCommand(() => this.Remove(this.MobileNumbers));
			//this.RemoveEmailCommand = new MvxCommand(() => this.Remove(this.Emails));
			//this.RemoveHomeCommand = new MvxCommand(() => { });
			//this.RemoveAddressCommand = new MvxCommand(() => { });
		}

		// MVX Life cycle
		public void Init()
		{
			this.MobileNumbers = new ObservableCollection<string>
			{
				"(234) 626 - 1015",
				"(234) 626 - 1016",
				"(234) 626 - 1017",
				"(234) 626 - 1018"
			};
			this.Emails = new ObservableCollection<string>
			{
				"chris@idriveyourcar.com",
				"tom@idriveyourcar.com",
				"martin@idriveyourcar.com"
			};
			this.HomeNumbers = new ObservableCollection<string>
			{
				"(523) 745 - 1015",
				"(523) 745 - 1016"
			};
			this.Addresses = new ObservableCollection<string>
			{
				"208 Bayberry Lane Westport, CT 06880",
				"13 Highland Rd, Westport, CT 06880",
				"1615 Cross Highway, Fairfield, CT 06824",
				"5 Silent Grove N, Westport, CT 06880"
			};
		}

		public override void Start()
		{
			base.Start();

			this.ProfileGeneral = this.LoadViewModel<ProfileGeneralViewModel>();
			this.ProfileBanking = this.LoadViewModel<ProfileBankingViewModel>();
			this.ProfileDocuments = this.LoadViewModel<ProfileDocumentsViewModel>();

			this.SectionVisibilityItems = new List<SectionVisibilityViewModel>();

			for(int i = 0; i < NUMBER_OF_SECTIONS; i++)
				this.SectionVisibilityItems.Add(new SectionVisibilityViewModel());
		}

		// MVVM Properties
		public ProfileGeneralViewModel ProfileGeneral { get; set; }

		public ProfileBankingViewModel ProfileBanking { get; set; }

		public ProfileDocumentsViewModel ProfileDocuments { get; set; }

		public ObservableCollection<string> MobileNumbers { get; set; }
		public ObservableCollection<string> Emails { get; set; }
		public ObservableCollection<string> HomeNumbers { get; set; }
		public ObservableCollection<string> Addresses { get; set; }
		public List<SectionVisibilityViewModel> SectionVisibilityItems { get; set; }

		// MVVM Commands
		public ICommand AddMobileCommand { get; private set; }
		public ICommand AddEmailCommand { get; private set; }
		public ICommand AddHomeCommand { get; private set; }
		public ICommand AddAddressCommand { get; private set; }

		public ICommand SaveMobileCommand { get; private set; }
		public ICommand SaveEmailCommand { get; private set; }
		public ICommand SaveHomeCommand { get; private set; }
		public ICommand SaveAddressCommand { get; private set; }

		//public ICommand RemoveMobileCommand { get; private set; }
		//public ICommand RemoveEmailCommand { get; private set; }
		//public ICommand RemoveHomeCommand { get; private set; }
		//public ICommand RemoveAddressCommand { get; private set; }

		// public methods

		// private methods
		private void AddNew(ObservableCollection<string> items)
		{
			this.alertHandler.ShowSimpleInputPopup(IDriveYourCarStrings.AddNew,
				(text) =>
				{
					if(string.IsNullOrEmpty(text))
						return Task.FromResult(false);

					items.Add(text);
					return Task.FromResult(true);
				});
		}

		//private void Remove(ObservableCollection<string> items, string itemToRemove)
		//{
		//    this.alertHandler.ShowMessage(IDriveYourCarStrings.ApplicationName,
		//        IDriveYourCarStrings.DoYouWantToRemoveThisItem,
		//        IDriveYourCarStrings.Ok,
		//        IDriveYourCarStrings.No,
		//        () =>
		//        {
		//            items.Remove(itemToRemove);
		//        });
		//}
	}
}