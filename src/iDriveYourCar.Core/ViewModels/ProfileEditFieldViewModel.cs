﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules.ProfileGeneral;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ProfileEditFieldViewModel : IDYCViewModel
    {
        private const string DRIVER_FIELD_BUNDLE_KEY = "field";

        protected readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        protected readonly IDriverService driverService;
        protected readonly IDriverEditableFieldConfigurationFactory driverEditableFieldConfigurationFactory;
        protected readonly ISnackbarHandler snackbarHandler;

        protected DriverEditableField driverField;

        public ProfileEditFieldViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IDriverService driverService,
            IDriverEditableFieldConfigurationFactory driverEditableFieldConfigurationFactory,
            ISnackbarHandler snackbarHandler)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.driverService = driverService;
            this.driverEditableFieldConfigurationFactory = driverEditableFieldConfigurationFactory;
            this.snackbarHandler = snackbarHandler;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SaveEditFieldCommand = new MvxCommand(() => this.SaveEditFieldTaskCompletion = NotifyTaskCompletion.Create(this.SaveEditField),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SaveEditFieldTaskCompletion));
        }

        // MVX Life cycle
        public void Init(DriverEditableField field)
        {
            this.driverField = field;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[DRIVER_FIELD_BUNDLE_KEY] = this.driverField.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.driverField = (DriverEditableField)Enum.Parse(typeof(DriverEditableField), state.Data[DRIVER_FIELD_BUNDLE_KEY]);
        }

        public override void Start()
        {
            base.Start();

            this.DriverEditableFieldConfiguration = this.driverEditableFieldConfigurationFactory.GetDriverEditableFieldConfiguration(
                this.universalObjectWrapperSingletonDriver.UniversalObject,
                this.driverField);
        }

        // MVVM Properties
        public DriverEditableFieldConfigurationModel DriverEditableFieldConfiguration { get; set; }

        private string inputText;
        public string InputText
        {
            get
            {
                return inputText ?? DriverEditableFieldConfiguration?.DefaultValue;
            }
            set
            {
                inputText = value;
            }
        }

        public INotifyTaskCompletion SaveEditFieldTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SaveEditFieldCommand { get; private set; }

        // public methods

        // private methods
        private Task SaveEditField()
        {
            if(!this.Validate())
                return Task.FromResult(0);

            // update driver's information only if it is a raw text field
            if(this.driverField != DriverEditableField.TaxId)
                this.driverEditableFieldConfigurationFactory.UpdateDriverEditableField(
                    this.universalObjectWrapperSingletonDriver.UniversalObject,
                    this.DriverEditableFieldConfiguration.Field,
                    this.InputText);

            if(this.driverField == DriverEditableField.NameBankAccount
               || this.driverField == DriverEditableField.RoutingNumber
               || this.driverField == DriverEditableField.AccountNumber
               || this.driverField == DriverEditableField.SSNTIN
               || this.driverField == DriverEditableField.TaxId)
            {
                return this.SaveBankingInfoAsync();
            }
            else
            {
                return this.SaveDriverInfoAsync();
            }
        }

        private bool Validate()
        {
            if(this.DriverEditableFieldConfiguration.ValidateEntry == null
              || this.DriverEditableFieldConfiguration.ValidateEntry.Invoke(this.InputText))
                return true;

            this.snackbarHandler.ShowMessage(this.DriverEditableFieldConfiguration.ErrorMessage);
            return false;
        }

        private async Task SaveBankingInfoAsync()
        {
            await this.driverService.UpdateBankingInformationAsync(
                this.universalObjectWrapperSingletonDriver.UniversalObject,
                success: this.HandleInfoSavedSuccess);
        }

        private async Task SaveDriverInfoAsync()
        {
            await this.driverService.UpdateDriverDataAsync(
                this.universalObjectWrapperSingletonDriver.UniversalObject,
                success: this.HandleInfoSavedSuccess);
        }

        private void HandleInfoSavedSuccess(Driver driver)
        {
            this.universalObjectWrapperSingletonDriver.UniversalObject = driver;

            this.SaveEditFieldTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();

            this.CloseWithTrack(this);
        }
    }
}
