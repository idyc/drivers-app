﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Support;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class FilterFindOpenTripsViewModel : IDYCWithMenuActionsViewModel
    {
        private const string CRITERIA_BUNDLE_KEY = "criteria";

        private readonly IMvxJsonConverter jsonConverter;
        private readonly IMvxMessenger mvxMessenger;
        private readonly IInputFieldHandler inputFieldHandler;
        private readonly IAddressService addressService;
        private readonly ISnackbarHandler snackbarHandler;

        private string criteriaSerialized;

        public FilterFindOpenTripsViewModel(
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IMvxMessenger mvxMessenger,
            IMvxJsonConverter jsonConverter,
            IInputFieldHandler inputFieldHandler,
            IAddressService addressService,
            ISnackbarHandler snackbarHandler)
            : base(menuNavigationActionsWrapper)
        {
            this.mvxMessenger = mvxMessenger;
            this.jsonConverter = jsonConverter;
            this.inputFieldHandler = inputFieldHandler;
            this.addressService = addressService;
            this.snackbarHandler = snackbarHandler;

            this.Predictions = new ObservableCollection<Prediction>();

            this.FilterTypes = new ObservableCollection<FindOpenTripsFilterType>();
            this.FilterTypes.Add(new FindOpenTripsFilterType(IDriveYourCarStrings.DrivingTimeFromHome, FindOpenTripsFilterTypes.DrivingTimeFromHome));
            this.FilterTypes.Add(new FindOpenTripsFilterType(IDriveYourCarStrings.SpecificAddress, FindOpenTripsFilterTypes.SpecificAddress));

            this.SelectedFilterType = this.FilterTypes.First();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SaveFilterTripsCommand = new MvxCommand(() => this.SaveFilterTripsTaskCompletion = NotifyTaskCompletion.Create(this.SaveFilterTrips),
                                                         () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SaveFilterTripsTaskCompletion));

            this.SelectPredictionCommand = new MvxCommand<Prediction>(this.SelectPrediction);

            this.ChangeStartDateCommand = new MvxCommand(() => this.ChangeStartDateTaskCompletion = NotifyTaskCompletion.Create(this.ChangeStartDateAsync),
                                                         () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.ChangeStartDateTaskCompletion));

            this.ChangeEndDateCommand = new MvxCommand(() => this.ChangeEndDateTaskCompletion = NotifyTaskCompletion.Create(this.ChangeEndDateAsync),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.ChangeEndDateTaskCompletion));
        }

        // MVX Life cycle
        public void Init(string criteriaSerialized)
        {
            this.criteriaSerialized = criteriaSerialized;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[CRITERIA_BUNDLE_KEY] = this.criteriaSerialized;
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.criteriaSerialized = state.Data[CRITERIA_BUNDLE_KEY];
        }

        public override void Start()
        {
            base.Start();

            this.Criteria = this.jsonConverter.DeserializeObject<OpenTripCriteria>(this.criteriaSerialized);
        }

        // MVVM Properties
        public ObservableCollection<FindOpenTripsFilterType> FilterTypes { get; set; }

        private FindOpenTripsFilterType selectedFilterType;
        public FindOpenTripsFilterType SelectedFilterType
        {
            get
            {
                return this.selectedFilterType;
            }
            set
            {
                this.selectedFilterType = value;

                if(!this.SpecificAddressIsSelected && this.Criteria != null)
                {
                    this.Criteria.Latitude = null;
                    this.Criteria.Longitude = null;
                    this.InputText = string.Empty;
                }
            }
        }

        public bool SpecificAddressIsSelected => this.SelectedFilterType?.Type == FindOpenTripsFilterTypes.SpecificAddress;

        public OpenTripCriteria Criteria { get; set; }

        public INotifyTaskCompletion ChangeStartDateTaskCompletion { get; private set; }

        public INotifyTaskCompletion ChangeEndDateTaskCompletion { get; private set; }

        public ObservableCollection<Prediction> Predictions { get; set; }

        public Prediction SelectedPrediction { get; set; }

        private string inputText;
        public string InputText
        {
            get
            {
                return inputText;
            }
            set
            {
                this.inputText = value ?? string.Empty;

                if(string.IsNullOrEmpty(value) || value.Trim().Length < 4)
                {
                    this.Predictions.Clear();
                    return;
                }

                this.SearchPredictionsTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();
                this.SearchPredictionsTaskCompletion = NotifyTaskCompletion.Create(this.GetPredictionsAsync);
            }
        }

        public INotifyTaskCompletion SearchPredictionsTaskCompletion { get; private set; }

        public INotifyTaskCompletion SaveFilterTripsTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SaveFilterTripsCommand { get; private set; }

        public ICommand ChangeStartDateCommand { get; private set; }

        public ICommand ChangeEndDateCommand { get; private set; }

        public ICommand SelectPredictionCommand { get; private set; }

        // public methods

        // private methods
        private async Task SaveFilterTrips()
        {
            if(this.Criteria.StartDate < DateTime.Today)
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.StartDateCannotBeLessThanCurrentDate);
                return;
            }
            if(this.Criteria.StartDate > this.Criteria.EndDate)
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.StartDateCannotBeGreaterThanEndDate);
                return;
            }

            if(this.SpecificAddressIsSelected)
            {
                var address = await this.GetAddressDetailsAsync();

                this.Criteria.Latitude = address.Lat;
                this.Criteria.Longitude = address.Lng;
            }

            this.mvxMessenger.Publish(new FilterOpenTripsUpdatedMessage(this, this.Criteria));

            this.CloseWithTrack(this);
        }

        private async Task ChangeStartDateAsync()
        {
            var newStart = await this.inputFieldHandler.ShowDateInputFieldAsync(defaultValue: this.Criteria.StartDate);

            if(newStart.HasValue)
                this.Criteria.StartDate = newStart.Value;
        }

        private async Task ChangeEndDateAsync()
        {
            var newEnd = await this.inputFieldHandler.ShowDateInputFieldAsync(defaultValue: this.Criteria.EndDate);

            if(newEnd.HasValue)
                this.Criteria.EndDate = newEnd.Value;
        }

        private async Task GetPredictionsAsync()
        {
            await this.addressService.GetAddressPredictionsAsync(
                this.inputText,
                success: predictions => this.Predictions.Fill(predictions));
        }

        private async Task<Address> GetAddressDetailsAsync()
        {
            Address address = null;
            await this.addressService.GetAddressDetailsAsync(
                this.SelectedPrediction.PlaceId,
                success: details =>
            {
                address = new Address
                {
                    GooglePlaceId = details.PlaceId,
                    Lat = details.Geometry.Location.Lat,
                    Lng = details.Geometry.Location.Lng
                };
            });

            return address;
        }

        private void SelectPrediction(Prediction prediction)
        {
            this.SelectedPrediction = prediction;
            this.inputText = this.SelectedPrediction.Description;

            this.Predictions.Clear();

            this.RaisePropertyChanged(() => this.InputText);
        }
    }
}
