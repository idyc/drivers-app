﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Validations;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ReferADriverViewModel : BaseReferralViewModel
    {
        private readonly IMvxMessenger mvxMessenger;
        private readonly IEnumHelper enumHelper;

        private MvxSubscriptionToken referralsLoadedToken;

        protected override InvitationType invitationType => InvitationType.Driver;

        protected override decimal invitationBonus => 100;

        public override string EarnMoneyForEveryFriendYouInvite => IDriveYourCarStrings.EarnMoneyForEveryFriendYouInviteToDrive;
        public override string EarnMoneyForEachFriendYouInviteWithNoLimitOnHowManyFriendsYouCanRefer => IDriveYourCarStrings.EarnMoneyForEachFriendYouInviteWithNoLimitOnHowManyFriendsYouCanReferReceiveYourReferralBonusWhenYourFriendIsAcceptedIntoTheIDYCNetwork;

        public ReferADriverViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IReferralsService referralService,
            IClipboardHandler clipboardHandler,
            IInputFieldHandler inputFieldHandler,
            ISnackbarHandler snackbarHandler,
            IValidatorHelper validatorHelper,
            IMvxMessenger mvxMessenger,
            IEnumHelper enumHelper)
            : base(
                universalObjectWrapperSingletonDriver,
                referralService,
                clipboardHandler,
                inputFieldHandler,
                snackbarHandler,
                validatorHelper)
        {
            this.mvxMessenger = mvxMessenger;
            this.enumHelper = enumHelper;

            this.PendingInvites = new ObservableCollection<DriverInvitationBaseItemViewModel>();

            this.SuccessfulInvites = new ObservableCollection<DriverInvitationBaseItemViewModel>();

            //this.MockInvites();

            this.referralsLoadedToken = this.mvxMessenger.SubscribeOnMainThread<ReferralsLoadedMessage>(
                message => this.CalculateTotalsAndFillPendingAndSuccessfulInvites(message.Referrals));
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

        }

        // MVX Life cycle
        public override void Init()
        {
        }

        public override void Start()
        {
            base.Start();

            //this.MockInvites();


        }

        public override void Destroy()
        {
            base.Destroy();

            this.referralsLoadedToken?.Dispose();
            this.referralsLoadedToken = null;
        }

        // MVVM Properties
        public string InviteYourFriendsToDriveAndEarnX => IDriveYourCarStrings.InviteYourFriendsToDriveAndEarnX.Replace("\\n", Environment.NewLine);

        public List<DriverInvitation> Invitations { get; set; }

        public INotifyTaskCompletion InviteGmailFriendsTaskCompletion { get; private set; }

        public decimal PendingInvitesEarning { get; set; }

        public ObservableCollection<DriverInvitationBaseItemViewModel> PendingInvites { get; set; }

        public decimal SuccessfulInvitesEarning { get; set; }

        public ObservableCollection<DriverInvitationBaseItemViewModel> SuccessfulInvites { get; set; }

        // MVVM Commands

        // public methods

        // private methods
        private void MockInvites()
        {
            this.Invitations = new List<DriverInvitation>
            {
                new DriverInvitation
                {
                    Email = "carlos@thequizproject.com",
                    Contact = "Carlos Romaris",
                    PotentialEarning = 100,
                    RedeemRequested = true,
                    Invited = new Driver
                    {
                        ApplicantStatus = ApplicantStatus.PendingComplete
                    }
                },
                new DriverInvitation
                {
                    Email = "ana@thequizproject.com",
                    Contact = "Carlos Romaris",
                    PotentialEarning = 100,
                    RedeemRequested = false,
                    Invited = new Driver
                    {
                        ApplicantStatus = ApplicantStatus.Active
                    }
                },
                new DriverInvitation
                {
                    Email = "josemaria@thequizproject.com",
                    Contact = "Carlos Romaris",
                    PotentialEarning = 100,
                    RedeemRequested = false,
                    Invited = new Driver
                    {
                        ApplicantStatus = ApplicantStatus.Interviewing
                    }
                },
            };

            this.PendingInvites.Clear();
            var grayBackground = true;
            foreach(var invitation in this.Invitations)
            {
                this.PendingInvites.Add(new DriverInvitationPendingItemViewModel(invitation, this.enumHelper, this.snackbarHandler, grayBackground));
                this.PendingInvitesEarning += invitation.PotentialEarning;
                this.SuccessfulInvites.Add(new DriverInvitationSuccessfulItemViewModel(invitation, this.enumHelper, this.referralService, this.snackbarHandler, grayBackground));
                this.SuccessfulInvitesEarning += invitation.PotentialEarning;

                grayBackground = !grayBackground;
            }
        }

        private void CalculateTotalsAndFillPendingAndSuccessfulInvites(Referrals referrals)
        {
            this.PendingInvites.Clear();
            this.SuccessfulInvites.Clear();
            this.PendingInvitesEarning = 0;
            this.SuccessfulInvitesEarning = 0;

            if(referrals == null)
                return;

            var grayBackgroundPending = true;
            var grayBackgroundSuccessful = true;

            foreach(var referral in referrals.Invitations)
            {
                if(referral.InvitedId == null || referral.Invited?.ApplicantStatus != ApplicantStatus.Active)
                {
                    this.PendingInvites.Add(new DriverInvitationPendingItemViewModel(referral, this.enumHelper, this.snackbarHandler, grayBackgroundPending));
                    this.PendingInvitesEarning += referral.PotentialEarning;

                    grayBackgroundPending = !grayBackgroundPending;
                }
                else
                {
                    this.SuccessfulInvitesEarning += referral.PotentialEarning;
                    this.SuccessfulInvites.Add(new DriverInvitationSuccessfulItemViewModel(referral, this.enumHelper, this.referralService, this.snackbarHandler, grayBackgroundSuccessful));

                    grayBackgroundSuccessful = !grayBackgroundSuccessful;
                }
            }
        }
    }
}
