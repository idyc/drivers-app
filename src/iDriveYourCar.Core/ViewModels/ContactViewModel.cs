﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.Validations;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ContactViewModel : IDYCWithMenuActionsViewModel
    {
        private readonly IToastHandler toastHandler;
        private readonly IValidatorHelper validatorHelper;

        public ContactViewModel(IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IToastHandler toastHandler,
            IValidatorHelper validatorHelper)
            : base(menuNavigationActionsWrapper)
        {
            this.toastHandler = toastHandler;
            this.validatorHelper = validatorHelper;

            this.Contact = new Contact();

            this.ConfigureValidator();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SendMessageCommand = new MvxCommand(() => this.SendMessageTaskCompletion = NotifyTaskCompletion.Create(this.SendMessage),
                                                     () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SendMessageTaskCompletion));
        }

        // Properties
        public Contact Contact { get; set; }

        public INotifyTaskCompletion SendMessageTaskCompletion { get; private set; }

        public ObservableDictionary<string, string> Errors { get; set; }

        // MVVM Commands
        public ICommand SendMessageCommand { get; private set; }

        // private methods
        private async Task SendMessage()
        {
            if(!this.Validate())
            {
                return;
            }

            await Task.FromResult(0);
            //await this.contactService.SendContactMessageDataAsync(this.Contact,
            //	success: () =>
            //	{
            //		this.toastHandler.ShowToast(IDriveYourCarStrings.MessageSent);
            //		this.Contact.Subject = string.Empty;
            //		this.Contact.Message = string.Empty;
            //	});
        }

        private void ConfigureValidator()
        {
            this.validatorHelper.AddRequiredFieldValidationRule(() => this.Contact.Subject, IDriveYourCarStrings.Subject, "Subject")
                                .AddRequiredFieldValidationRule(() => this.Contact.Message, IDriveYourCarStrings.Message, "Message");
        }

        private bool Validate()
        {
            this.Errors = this.validatorHelper.ValidateAll();

            return !this.Errors.Any();
        }
    }
}
