﻿using System.Collections.Generic;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
    public class UpcomingTripDetailViewModel : IDYCWithMenuActionsHostViewModel
    {
        private const string TRIP_ID_BUNDLE_KEY = "tripId";

        protected long tripId;

        public UpcomingTripDetailViewModel(
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper)
            : base(menuNavigationActionsWrapper)
        {
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();
        }

        public virtual void Init(long tripId)
        {
            this.tripId = tripId;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_ID_BUNDLE_KEY] = this.tripId.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripId = long.Parse(state.Data[TRIP_ID_BUNDLE_KEY]);
        }

        // MVX Life cycle
        public override void Start()
        {
            base.Start();

            var data = new Dictionary<string, string>
            {
                { TRIP_ID_BUNDLE_KEY, this.tripId.ToString()  }
            };

            IMvxBundle bundle = new MvxBundle(data);

            this.UpcomingTripDetailDetails = this.LoadViewModel<UpcomingTripDetailInfoViewModel>(bundle);
            this.UpcomingTripDetailNotes = this.LoadViewModel<UpcomingTripDetailNotesViewModel>(bundle);
        }

        // MVVM Properties
        public UpcomingTripDetailInfoViewModel UpcomingTripDetailDetails { get; set; }

        public UpcomingTripDetailNotesViewModel UpcomingTripDetailNotes { get; set; }

        // MVVM Commands

        // public methods

        // private methods
    }
}

