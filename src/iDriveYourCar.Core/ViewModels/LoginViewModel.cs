﻿using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;

namespace iDriveYourCar.Core.ViewModels
{
    [Preserve(AllMembers = true)]
    public class LoginViewModel : IDYCViewModel
    {
        private readonly IUserInformationService userInformationService;
        private readonly IAlertHandler alertHandler;
        private readonly ISecureStorageHelper secureStorageHelper;
        private readonly IIDYCSettings idycSettings;

        public LoginViewModel(
            IUserInformationService userInformationService,
            IAlertHandler alertHandler,
            ISecureStorageHelper secureStorageHelper,
            IIDYCSettings idycSettings)
        {
            this.userInformationService = userInformationService;
            this.alertHandler = alertHandler;
            this.secureStorageHelper = secureStorageHelper;
            this.idycSettings = idycSettings;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.LoginCommand = new MvxCommand(() => this.LoginTaskCompletion = NotifyTaskCompletion.Create(this.LoginAsync),
                                               () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.LoginTaskCompletion));

            this.ShowForgotPasswordCommand = new MvxCommand(() => this.ShowViewModelWithTrack<ForgotPasswordViewModel>());

            this.ShowSignUpCommand = new MvxCommand(() => this.ShowViewModelWithTrack<RegisterGeneralInformationViewModel>());
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public INotifyTaskCompletion LoginTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand LoginCommand { get; private set; }

        public ICommand ShowForgotPasswordCommand { get; private set; }

        public ICommand ShowSignUpCommand { get; private set; }

        // public methods

        // private methods
        private async Task LoginAsync()
        {
#if DEBUG
            this.Username = "mariano.mino@d-genix.com";
            this.Password = "123456";
#endif

            if(string.IsNullOrWhiteSpace(this.Username) || string.IsNullOrWhiteSpace(this.Password))
            {
                this.alertHandler.ShowMessage(IDriveYourCarStrings.PleaseCompleteAllFields);
                return;
            }

            await this.userInformationService.VerifyUserAsync(
                    this.Username.Trim(),
                    this.Password.Trim(),
                    success: user =>
                    {
                        this.secureStorageHelper.SetUserValues(this.Username.Trim(), this.Password.Trim());
                        this.idycSettings.HasBankInfo = user.HasBankInfo;

                        if(user.HasBankInfo)
                            this.ShowViewModelWithTrack<MainViewModel>();
                        else
                            this.ShowViewModelWithTrack<BankingDetailsViewModel>();
                    });
        }
    }
}
