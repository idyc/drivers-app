﻿using System.ComponentModel;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.PushNotifications;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform.WeakSubscription;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class NotificationsViewModel : IDYCListWithMenuActionsViewModel<INotificationService, Notification, NotificationItemViewModel, NotificationCriteria>
    {
        private readonly IIDYCSettings idycSettings;
        private readonly ISnackbarHandler snackbarHandler;
        private readonly IAlertHandler alertHandler;
        private readonly INotificationService notificationService;
        private readonly IMvxJsonConverter jsonConverter;

        public NotificationsViewModel(
            IFwkBaseListViewModelServiceBridge<INotificationService, Notification, NotificationCriteria> notificationsBridge,
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IIDYCSettings idycSettings,
            ISnackbarHandler snackbarHandler,
            IAlertHandler alertHandler,
            INotificationService notificationService,
            IMvxJsonConverter jsonConverter)
            : base(notificationsBridge, menuNavigationActionsWrapper)
        {
            this.idycSettings = idycSettings;
            this.snackbarHandler = snackbarHandler;
            this.alertHandler = alertHandler;
            this.notificationService = notificationService;
            this.jsonConverter = jsonConverter;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.PerformNavigationCommand = new MvxCommand<NotificationItemViewModel>(n => this.PerformNavigation(n));
            this.RemoveNotificationCommand = new MvxCommand<NotificationItemViewModel>(n => this.RemoveNotification(n));
        }

        // MVX Life cycle
        public override void Init()
        {
            base.Init();

            this.idycSettings.PushNotificationsCounter = 0;
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public INotifyTaskCompletion RemoveNotificationTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand RemoveNotificationCommand { get; private set; }

        public ICommand PerformNavigationCommand { get; private set; }

        // public methods

        // private methods
        protected override NotificationCriteria CreateCriteria(bool isFetching = false)
        {
            return new NotificationCriteria
            {
                Page = this.nextPage
            };
        }

        protected override NotificationItemViewModel GetItemFromResultModel(Notification model) => new NotificationItemViewModel(model);

        private void RemoveNotification(NotificationItemViewModel item)
        {
            if (!item.Notification.CanBeDeleted)
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.ThisNotificationCannotBeDeleted);
                return;
            }

            this.alertHandler.ShowMessage(
                IDriveYourCarStrings.ApplicationName,
                IDriveYourCarStrings.DoYouWantToDeleteThisNotification,
                DGenix.Mobile.Fwk.Core.Resources.Strings.Yes,
                DGenix.Mobile.Fwk.Core.Resources.Strings.No,
                () =>
                {
                    this.Items.Remove(item);

                    this.RemoveNotificationTaskCompletion = NotifyTaskCompletion.Create(this.notificationService.RemoveNotificationAsync(
                        item.Notification.LocalId,
                        () =>
                        {
                            this.snackbarHandler.ShowMessage(IDriveYourCarStrings.TheNotificationHasBeenSuccessfullyDeleted);
                        }));

                    this.RemoveNotificationTaskCompletion.WeakSubscribe(this.RemoveNotificationTaskCompletion_PropertyChanged);
                },
                () =>
                {

                });
        }

        private void RemoveNotificationTaskCompletion_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(this.RemoveNotificationTaskCompletion.IsFaulted) && this.RemoveNotificationTaskCompletion.IsFaulted)
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.TheNotificationCouldNotBeDeletedPleaseTryAgain);

                this.RefreshItemsCommand.Execute(null);
            }
        }

        private void PerformNavigation(NotificationItemViewModel item)
        {
            new NotificationNavigator(this.alertHandler, this.jsonConverter)
                .PerformNavigationByCode(item.Notification.NotificationData);
        }

        #region Mock Notifications
        //protected override void StartLoad()
        //{
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Cannot be deleted", CanBeDeleted = false, Title = "Test1", NotificationData = new NotificationData { NotificationCode = PushTypes.DriverSignedUp } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test7", NotificationData = new NotificationData { NotificationCode = PushTypes.DriverSignedUp } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test4", NotificationData = new NotificationData { NotificationCode = PushTypes.ClientSignedUp } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test2", NotificationData = new NotificationData { NotificationCode = PushTypes.AccountActivated } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test8", NotificationData = new NotificationData { NotificationCode = PushTypes.InPersonTrainingSchedule } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test9", NotificationData = new NotificationData { NotificationCode = PushTypes.InPersonTrainingStartsSoon } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test12", NotificationData = new NotificationData { NotificationCode = PushTypes.NewTripAvailable } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test13", NotificationData = new NotificationData { NotificationCode = PushTypes.NewTripAvailableDriverRequested } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test11", NotificationData = new NotificationData { NotificationCode = PushTypes.NewTripAccepted } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test21", NotificationData = new NotificationData { NotificationCode = PushTypes.TripHasBeenModified, TripId = "37" } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test23", NotificationData = new NotificationData { NotificationCode = PushTypes.TripHasBeenModifiedScheduleConflict, TripId = "37" } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test19", NotificationData = new NotificationData { NotificationCode = PushTypes.TripHasBeenCanceled, TripId = "37" } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test20", NotificationData = new NotificationData { NotificationCode = PushTypes.TripHasBeenCanceledFee, TripId = "37" } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test18", NotificationData = new NotificationData { NotificationCode = PushTypes.TripComingUp, TripId = "37" } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test17", NotificationData = new NotificationData { NotificationCode = PushTypes.TimeToStartTrip, TripId = "37" } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test24", NotificationData = new NotificationData { NotificationCode = PushTypes.UrgentMessageFromClient } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test3", NotificationData = new NotificationData { NotificationCode = PushTypes.ClientHasRequestedPickup } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test5", NotificationData = new NotificationData { NotificationCode = PushTypes.CSRMessageReceived } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test6", NotificationData = new NotificationData { NotificationCode = PushTypes.DispatchTierUpgrade } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test16", NotificationData = new NotificationData { NotificationCode = PushTypes.ScoreHasFallenBellow } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test15", NotificationData = new NotificationData { NotificationCode = PushTypes.PayoutHasIncreased } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test10", NotificationData = new NotificationData { NotificationCode = PushTypes.LocationRequest } }));
        //this.Items.Add(new NotificationItemViewModel(new Notification { Body = "Can be deleted", CanBeDeleted = true, Title = "Test14", NotificationData = new NotificationData { NotificationCode = PushTypes.None } }));
        //}
        #endregion
    }
}
