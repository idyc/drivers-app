using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Models.Push;
using iDriveYourCar.Core.Rules;
using iDriveYourCar.Core.Rules.UpcomingTrips;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels
{
    public class UpcomingTripsViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IMvxMessenger mvxMessenger;
        private readonly IUpcomingTripItemViewModelFactory upcomingTripItemViewModelFactory;
        private readonly ITripWorkflowManager tripWorkflowManager;
        private readonly IAlertHandler alertHandler;
        private readonly IMvxJsonConverter jsonConverter;

        private MvxSubscriptionToken driverLoadedSubscriptionToken;

        public UpcomingTripsViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IMvxMessenger mvxMessenger,
            IUpcomingTripItemViewModelFactory upcomingTripItemViewModelFactory,
            ITripWorkflowManager tripWorkflowManager,
            IAlertHandler alertHandler,
            IMvxJsonConverter jsonConverter)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.mvxMessenger = mvxMessenger;
            this.upcomingTripItemViewModelFactory = upcomingTripItemViewModelFactory;
            this.tripWorkflowManager = tripWorkflowManager;
            this.alertHandler = alertHandler;
            this.jsonConverter = jsonConverter;

            this.UpcomingTrips = new ObservableCollection<IBaseTripItemViewModel>();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.DisplayUpcomingTripDetailCommand = new MvxCommand<IBaseTripItemViewModel>(this.DisplayUpcomingTripDetail);
            this.ShowFindOpenTripsCommand = new MvxCommand(() => this.ShowViewModelWithTrack<FindOpenTripsViewModel>());
            this.ShowListCommand = new MvxCommand(() => this.ToggleListVisibility(true));
            this.ShowCalendarCommand = new MvxCommand(() => this.ToggleListVisibility(false));
        }

        // MVX Life cycle
        public void Init()
        {
            this.driverLoadedSubscriptionToken = this.mvxMessenger.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message =>
            {
                this.RaisePropertyChanged(() => this.Driver);
                this.FillUpcomingTripsByCurrentDate();
            });
        }

        public override void Start()
        {
            base.Start();

            if (this.Driver != null)
                this.FillUpcomingTripsByCurrentDate();
        }

        public override void Destroy()
        {
            base.Destroy();

            this.driverLoadedSubscriptionToken?.Dispose();
            this.driverLoadedSubscriptionToken = null;
        }

        // MVVM Properties
        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        public ObservableCollection<IBaseTripItemViewModel> UpcomingTrips { get; set; }

        private DateTime currentDate = DateTime.Today;
        public DateTime CurrentDate
        {
            get
            {
                return this.currentDate;
            }
            set
            {
                this.currentDate = value;
                this.FillUpcomingTripsByCurrentDate();
            }
        }

        public DateTime CurrentMonth { get; set; } = DateTime.Today;

        public bool IsListVisible { get; set; } = true;

        public bool HasUpcomingTrips { get; set; } = true;

        // MVVM Commands
        public ICommand DisplayUpcomingTripDetailCommand { get; private set; }

        public ICommand ShowFindOpenTripsCommand { get; private set; }

        public ICommand ShowListCommand { get; private set; }

        public ICommand ShowCalendarCommand { get; private set; }

        // public methods
        public bool HasUpcomingTripsForDate(DateTime date)
        {
            if (this.Driver == null)
                return false;

            return this.Driver.UpcomingTrips.Any(trip => trip.PickupDate.ToDateTimeFromFormat().Date == date.Date);
        }

        public void ForceRefresh()
        {
            this.RaisePropertyChanged(() => this.Driver);
            this.FillUpcomingTripsByCurrentDate();
        }

        // private methods
        private void ToggleListVisibility(bool visible)
        {
            this.IsListVisible = visible;
        }

        private void DisplayUpcomingTripDetail(IBaseTripItemViewModel tripItemViewModel)
        {
            var upcomingTripItemViewModel = tripItemViewModel as UpcomingTripItemViewModel;

            if (upcomingTripItemViewModel == null)
                return;

            this.alertHandler.ShowMessage(
                string.Empty,
                IDriveYourCarStrings.PressYesToForceInTrip,
                Strings.Yes,
                Strings.No,
                () => this.ForceInTrip(upcomingTripItemViewModel),
                () => this.ExecuteNormally(upcomingTripItemViewModel));
        }

        private void ForceInTrip(UpcomingTripItemViewModel upcomingTripItemViewModel)
        {
            if (upcomingTripItemViewModel.Trip.ActiveTripState == ActiveTripState.EndTrip)
                this.ShowViewModelWithTrack<CurrentTripSubmitViewModel>(new { tripId = upcomingTripItemViewModel.Trip.Id });
            else
                this.ShowViewModelWithTrack<CurrentTripDetailViewModel>(new { tripId = upcomingTripItemViewModel.Trip.Id });
        }

        private void ExecuteNormally(UpcomingTripItemViewModel upcomingTripItemViewModel)
        {
            if (this.tripWorkflowManager.IsTripActive(upcomingTripItemViewModel.Trip))
            {
                if (upcomingTripItemViewModel.Trip.ActiveTripState == ActiveTripState.EndTrip)
                    this.ShowViewModelWithTrack<CurrentTripSubmitViewModel>(new { tripId = upcomingTripItemViewModel.Trip.Id });
                else
                    this.ShowViewModelWithTrack<CurrentTripDetailViewModel>(new { tripId = upcomingTripItemViewModel.Trip.Id });
            }
            else
            {
                if (upcomingTripItemViewModel.Trip.FieldsChanged != null && upcomingTripItemViewModel.Trip.FieldsChanged.Any())
                {
                    var tripChanges = new TripChangesPush
                    {
                        TripId = upcomingTripItemViewModel.Trip.Id,
                        IsCanceled = upcomingTripItemViewModel.Trip.IsCancelled,
                        FieldChanges = upcomingTripItemViewModel.Trip.FieldsChanged
                    };
                    var tripChangesSerialized = this.jsonConverter.SerializeObject(tripChanges);
                    this.ShowViewModelWithTrack<UpcomingTripModifiedDetailViewModel>(new { tripChangesSerialized });
                }
                else
                    this.ShowViewModelWithTrack<UpcomingTripDetailViewModel>(new { tripId = upcomingTripItemViewModel.Trip.Id });
            }
        }

        private void FillUpcomingTripsByCurrentDate()
        {
            this.UpcomingTrips.Clear();

            #region Get Trips Real Data
            var upcomingTripsOrderedByDate = this.Driver.UpcomingTrips.Where(trip => trip.PickupDate.ToDateTimeFromFormat().Date >= this.CurrentDate.Date).OrderBy(trip => trip.PickupDate.ToDateTimeFromFormat()).ToList();

            string currentPickupDate = null;

            foreach (var upcomingTrips in upcomingTripsOrderedByDate)
            {
                if (currentPickupDate != upcomingTrips.PickupDate)
                {
                    var pickupDate = upcomingTrips.PickupDate.ToDateTimeFromFormat();

                    if (pickupDate.Date != this.currentDate.Date)
                        this.UpcomingTrips.Add(this.upcomingTripItemViewModelFactory.Create(pickupDate));

                    currentPickupDate = upcomingTrips.PickupDate;
                }
                this.UpcomingTrips.Add(this.upcomingTripItemViewModelFactory.Create(upcomingTrips));
            }

            this.HasUpcomingTrips = this.UpcomingTrips.Any();
            #endregion

            #region Get Trips Mocked
            //this.Driver.UpcomingTrips = this.GetTripsMocked();
            //this.HasUpcomingTrips = this.UpcomingTrips.Any();
            #endregion
        }

        #region Get Trips Mocked methods

        private List<Trip> GetTripsMocked()
        {
            var trips = new List<Trip>();
            var trip1 = new Trip
            {
                Id = 15,
                IsCancelled = false,
                TripReturn = new Trip
                {
                    Id = 3,
                    Duration = (decimal?)48.222556,
                    TripType = TripType.DriveMeHome,
                    PickupDate = "Wednesday April 5, 2017",
                    PickupTime = "3:50 pm",
                    KeyLocationVehicleDescription = "New York, NY",
                    PublicNotesBlobbed = "",
                    ToAirport = new Airport
                    {
                        Name = "JFK John F. Kennedy Intl Airport",
                        Lat = (decimal?)-32.9487127,
                        Lng = (decimal?)(-60.7184787)
                    },
                    ToAirline = new Airline
                    {
                        Name = "LAN Airlines (AA) 101"
                    },
                    Client = new Client
                    {
                        FirstName = "Albert",
                        LastName = "Jhonsson"
                    },
                    PickupAddress = new Address
                    {
                        Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                        City = "Washington",
                        Country = "WA",
                        Lat = (decimal?)-32.9555904,
                        Lng = (decimal?)(-60.6517247)
                    },
                    Stops = new List<TripStop>
                    {
                        new TripStop
                        {
                              Address = new Address
                              {
                                Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                                City = "Washington",
                                Country = "WA",
                                Lat = (decimal?)-32.9590143,
                                Lng = (decimal?)(-60.6523266)
                              }
                        }
                    },
                    DropoffAddress = new Address
                    {
                        Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                        City = "Washington",
                        Country = "WA",
                        Lat = (decimal?)-32.9543356,
                        Lng = (decimal?)(-60.6392746)
                    }
                },
                TripType = TripType.DriveMeHome,
                Duration = (decimal?)48.222556,
                PickupDate = "Wednesday April 5, 2017",
                PickupTime = "17:18 pm",
                FieldsChanged = new List<TripFieldChanged>
                {
                    TripFieldChanged.DropoffTimeEstimated,
                    TripFieldChanged.MeetupInstructions,
                    TripFieldChanged.NotesForDriver,
                    TripFieldChanged.VehicleAddressId,
                    TripFieldChanged.Offer,
                    TripFieldChanged.DropoffAddressId,
                    TripFieldChanged.KeyLocationVehicleDescription,
                    TripFieldChanged.PickupAddressId,
                    TripFieldChanged.PickupDate,
                    TripFieldChanged.PublicNotes,
                    TripFieldChanged.Stops,
                    TripFieldChanged.PickupTime
                },
                VehicleAddress = new Address
                {
                    Name = "Bayberry 2155",
                    City = "New York",
                    State = "NY"
                },
                ToAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)-32.9487127,
                    Lng = (decimal?)(-60.7184787),
                    Code = "JFK"
                },
                ToAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101",
                    Code = "LAN"
                },
                Offer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                KeyLocationVehicleDescription = "New York, NY",
                MeetupInstructions = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                PublicNotesBlobbed = "Client has requested the daily New York Times, a large Starbucks coffe with 1 milk and 2 sugars & 3 Sausage Egg McMuffin from McDonalds.",
                PublicNotes = new List<string>
                {
                    "Client has requested the daily New York Times, a large Starbucks ",
                    "coffe with 1 milk and 2 sugars & 3 Sausage"
                },
                Client = new Client
                {
                    FirstName = "John Joe",
                    LastName = "Johanssen Jhonsson Jhonsson Jhonsson",
                    AccountNotes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                    NotesForDriver = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur."
                },
                PickupAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.6523266)
                },
                Stops = new List<TripStop>
                  {
                    new TripStop
                    {
                        Address = new Address
                          {
                            Name = "Bayberry 2125",
                            City = "Washington",
                            State = "WA",
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            Country = "WA",
                            Lat = (decimal?)-32.9590143,
                            Lng = (decimal?)(-60.6523266)
                          }
                    }
                  },
                DropoffAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.6523266)
                }
            };
            var trip2 = new Trip
            {
                Id = 16,
                IsCancelled = false,
                TripReturn = new Trip
                {
                    Id = 2,
                    Duration = (decimal?)48.222556,
                    TripType = TripType.DriveMeHome,
                    PickupDate = "Wednesday April 5, 2017",
                    PickupTime = "3:50 pm",
                    KeyLocationVehicleDescription = "New York, NY",
                    PublicNotesBlobbed = "",
                    ToAirport = new Airport
                    {
                        Name = "JFK John F. Kennedy Intl Airport",
                        Lat = (decimal?)-32.9487127,
                        Lng = (decimal?)(-60.7184787)
                    },
                    ToAirline = new Airline
                    {
                        Name = "LAN Airlines (AA) 101"
                    },
                    Client = new Client
                    {
                        FirstName = "Albert",
                        LastName = "Jhonsson"
                    },
                    PickupAddress = new Address
                    {
                        Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                        City = "Washington",
                        Country = "WA",
                        Lat = (decimal?)-32.9555904,
                        Lng = (decimal?)(-60.6517247)
                    },
                    Stops = new List<TripStop>
                    {
                        new TripStop
                        {
                              Address = new Address
                              {
                                Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                                City = "Washington",
                                Country = "WA",
                                Lat = (decimal?)-32.9590143,
                                Lng = (decimal?)(-60.6523266)
                              }
                        }
                    },
                    DropoffAddress = new Address
                    {
                        Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                        City = "Washington",
                        Country = "WA",
                        Lat = (decimal?)-32.9543356,
                        Lng = (decimal?)(-60.6392746)
                    }
                },
                TripType = TripType.DriveMeHome,
                Duration = (decimal?)48.222556,
                PickupDate = "Wednesday April 5, 2017",
                PickupTime = "17:18 pm",
                FieldsChanged = new List<TripFieldChanged>
                {
                    TripFieldChanged.DropoffTimeEstimated,
                    TripFieldChanged.VehicleAddressId,
                    TripFieldChanged.Offer,
                    TripFieldChanged.DropoffAddressId,
                    TripFieldChanged.PickupAddressId,
                    TripFieldChanged.PickupDate,
                    TripFieldChanged.Stops,
                    TripFieldChanged.PickupTime
                },
                VehicleAddress = new Address
                {
                    Name = "Bayberry 2155",
                    City = "New York",
                    State = "NY"
                },
                ToAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)-32.9487127,
                    Lng = (decimal?)(-60.7184787),
                    Code = "JFK"
                },
                ToAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101",
                    Code = "LAN"
                },
                Offer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                KeyLocationVehicleDescription = "New York, NY",
                MeetupInstructions = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                PublicNotesBlobbed = "Client has requested the daily New York Times, a large Starbucks coffe with 1 milk and 2 sugars & 3 Sausage Egg McMuffin from McDonalds.",
                PublicNotes = new List<string>
                {
                    "Client has requested the  daily New York Times, a large Starbucks ",
                    "coffe with 1 milk and 2 sugars & 3 Sausage"
                },
                Client = new Client
                {
                    FirstName = "Larry Lorry",
                    LastName = "Lorrey",
                    AccountNotes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                    NotesForDriver = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur."
                },
                PickupAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.6523266)
                },
                Stops = new List<TripStop>
                  {
                    new TripStop
                    {
                        Address = new Address
                          {
                            Name = "Bayberry 2125",
                            City = "Washington",
                            State = "WA",
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            Country = "WA",
                            Lat = (decimal?)-32.9590143,
                            Lng = (decimal?)(-60.6523266)
                          }
                    }
                  },
                DropoffAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.6523266)
                }
            };
            var trip3 = new Trip
            {
                Id = 17,
                IsCancelled = false,
                FieldsChanged = new List<TripFieldChanged>(),
                TripType = TripType.AirportDropoff,
                Duration = (decimal?)48.222556,
                PickupDate = "Wednesday April 5, 2017",
                PickupTime = "17:18 pm",
                VehicleAddress = new Address
                {
                    Name = "Bayberry 2155",
                    City = "New York",
                    State = "NY"
                },
                ToAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)-32.9487127,
                    Lng = (decimal?)(-60.7184787),
                    Code = "JFK"
                },
                ToAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101",
                    Code = "LAN"
                },
                Offer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                KeyLocationVehicleDescription = "New York, NY",
                MeetupInstructions = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                PublicNotesBlobbed = "Client has requested the daily New York Times, a large Starbucks coffe with 1 milk and 2 sugars & 3 Sausage Egg McMuffin from McDonalds.",
                PublicNotes = new List<string>
                {
                    "Client has requested the daily New York Times, a large Starbucks ",
                    "coffe with 1 milk and 2 sugars & 3 Sausage"
                },
                Client = new Client
                {
                    FirstName = "Ken Kennedy",
                    LastName = "Khan",
                    AccountNotes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                    NotesForDriver = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur."
                },
                PickupAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.6523266)
                },
                Stops = new List<TripStop>
                  {
                    new TripStop
                    {
                        Address = new Address
                          {
                            Name = "Bayberry 2125",
                            City = "Washington",
                            State = "WA",
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            Country = "WA",
                            Lat = (decimal?)-32.9590143,
                            Lng = (decimal?)(-60.6523266)
                          }
                    }
                  },
                DropoffAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.65232)
                }
            };
            var trip4 = new Trip
            {
                Id = 19,
                IsCancelled = false,
                HasTripConflict = true,
                TripType = TripType.AirportPickup,
                Duration = (decimal?)48.222556,
                PickupDate = "Wednesday April 5, 2017",
                PickupTime = "17:18 pm",
                FieldsChanged = new List<TripFieldChanged>
                {
                    TripFieldChanged.DropoffTimeEstimated,
                    TripFieldChanged.VehicleAddressId,
                    TripFieldChanged.Offer,
                    TripFieldChanged.DropoffAddressId,
                    TripFieldChanged.PickupAddressId,
                    TripFieldChanged.PickupDate,
                    TripFieldChanged.Stops,
                    TripFieldChanged.PickupTime
                },
                VehicleAddress = new Address
                {
                    Name = "Bayberry 2155",
                    City = "New York",
                    State = "NY"
                },
                FromAirport = new Airport
                {
                    Name = "JFK John F. Kennedy Intl Airport",
                    Lat = (decimal?)-32.9487127,
                    Lng = (decimal?)(-60.7184787),
                    Code = "JFK"
                },
                FromAirline = new Airline
                {
                    Name = "LAN Airlines (AA) 101",
                    Code = "LAN"
                },
                Offer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                KeyLocationVehicleDescription = "New York, NY",
                MeetupInstructions = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                PublicNotesBlobbed = "Client has requested the daily New York Times, a large Starbucks coffe with 1 milk and 2 sugars & 3 Sausage Egg McMuffin from McDonalds.",
                PublicNotes = new List<string>
                {
                    "Client has requested the  daily New York Times, a large Starbucks ",
                    "coffe with 1 milk and 2 sugars & 3 Sausage"
                },
                Client = new Client
                {
                    FirstName = "Mark Merhiel",
                    LastName = "Murrad",
                    AccountNotes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur.",
                    NotesForDriver = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor a dolor sit amet consectur."
                },
                PickupAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.6523266)
                },
                Stops = new List<TripStop>
                  {
                    new TripStop
                    {
                        Address = new Address
                          {
                            Name = "Bayberry 2125",
                            City = "Washington",
                            State = "WA",
                            Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                            Country = "WA",
                            Lat = (decimal?)-32.9590143,
                            Lng = (decimal?)(-60.6523266)
                          }
                    }
                  },
                DropoffAddress = new Address
                {
                    Name = "Bayberry 2125",
                    City = "Washington",
                    State = "WA",
                    Raw = "132-27 Farmers Blvd Jamaica, NY 11434",
                    Country = "WA",
                    Lat = (decimal?)-32.9590143,
                    Lng = (decimal?)(-60.65232)
                }
            };

            this.UpcomingTrips.Add(new UpcomingTripItemViewModel(trip1));
            this.UpcomingTrips.Add(new UpcomingTripItemViewModel(trip2));
            this.UpcomingTrips.Add(new UpcomingTripItemViewModel(trip3));
            this.UpcomingTrips.Add(new UpcomingTripItemViewModel(trip4));

            trips.Add(trip1);
            trips.Add(trip2);
            trips.Add(trip3);
            trips.Add(trip4);

            return trips;
        }
        #endregion

    }
}
