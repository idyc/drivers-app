﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;

namespace iDriveYourCar.Core.ViewModels
{
    public class MenuViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IMvxMessenger mvxMessenger;
        private readonly IIDYCSettings idycSettings;

        private MvxSubscriptionToken driverLoadedSubscriptionToken;

        public MenuViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IMvxMessenger mvxMessenger,
            IIDYCSettings idycSettings)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.mvxMessenger = mvxMessenger;
            this.idycSettings = idycSettings;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ShowMyTripsCommand = new MvxCommand(() => this.ShowViewModelWithTrack<MyTripsViewModel>());
            this.ShowMessagesCommand = new MvxCommand(() => this.ShowViewModelWithTrack<NotificationsViewModel>());//(new { channelIdentifier = "general" }));
            this.ShowFindOpenTripsCommand = new MvxCommand(() => this.ShowViewModelWithTrack<FindOpenTripsViewModel>());
            this.ShowAvailabilityCommand = new MvxCommand(() => this.ShowViewModelWithTrack<DriverUnavailabilityViewModel>());
            this.ShowReviewsCommand = new MvxCommand(() => this.ShowViewModelWithTrack<ReviewsViewModel>());
            this.ShowInvitesCommand = new MvxCommand(() => this.ShowViewModelWithTrack<InvitesViewModel>());
            this.ShowProfileCommand = new MvxCommand(() => this.ShowViewModelWithTrack<ProfileViewModel>());
        }

        // MVX Life cycle
        public void Init()
        {
            this.driverLoadedSubscriptionToken = this.mvxMessenger.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message =>
            {
                this.RaisePropertyChanged(() => this.Driver);
                this.RaisePropertyChanged(() => this.DriverActive);
            });
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public string UserImagePath => this.TryToGetImagePath(() => this.Driver.User.Avatar);

        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        public bool DriverActive => this.Driver.ApplicantStatus == ApplicantStatus.Active;

        public int NewMessagesCount => this.idycSettings.PushNotificationsCounter;

        // MVVM Commands
        public ICommand ShowReviewsCommand { get; private set; }

        public ICommand ShowMyTripsCommand { get; private set; }

        public ICommand ShowMessagesCommand { get; private set; }

        public ICommand ShowFindOpenTripsCommand { get; private set; }

        public ICommand ShowAvailabilityCommand { get; private set; }

        public ICommand ShowInvitesCommand { get; private set; }

        public ICommand ShowProfileCommand { get; private set; }

        public ICommand ShowContactUsCommand { get; private set; }

        // public methods

        // private methods
    }
}
