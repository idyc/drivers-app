﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.Validations;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ReferAClientViewModel : BaseReferralViewModel
    {
        private readonly IMvxMessenger mvxMessenger;

        private MvxSubscriptionToken referralsLoadedToken;

        protected override InvitationType invitationType => InvitationType.Client;

        protected override decimal invitationBonus => 35;

        public override string EarnMoneyForEveryFriendYouInvite => IDriveYourCarStrings.EarnMoneyForEveryFriendYouInviteToRide;
        public override string EarnMoneyForEachFriendYouInviteWithNoLimitOnHowManyFriendsYouCanRefer => IDriveYourCarStrings.EarnMoneyForEachFriendYouInviteWithNoLimitOnHowManyFriendsYouCanReferReceiveYourReferralBonusWhenYourFriendHasCompletedTheirFirstTripWithIDYC;

        public ReferAClientViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IReferralsService referralService,
            IClipboardHandler clipboardHandler,
            IInputFieldHandler inputFieldHandler,
            ISnackbarHandler snackbarHandler,
            IValidatorHelper validatorHelper,
            IMvxMessenger mvxMessenger)
            : base(
                universalObjectWrapperSingletonDriver,
                referralService,
                clipboardHandler,
                inputFieldHandler,
                snackbarHandler,
                validatorHelper)
        {
            this.mvxMessenger = mvxMessenger;

            this.referralsLoadedToken = this.mvxMessenger.SubscribeOnMainThread<ReferralsLoadedMessage>(
                message => this.CalculateEarnedToDate(message.Referrals));
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.RedeemBalanceCommand = new MvxCommand(() => this.RedeemBalanceTaskCompletion = NotifyTaskCompletion.Create(this.RedeemBalanceAsync),
                                               () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.RedeemBalanceTaskCompletion));
        }

        // MVX Life cycle
        public override void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        public override void Destroy()
        {
            base.Destroy();

            this.referralsLoadedToken?.Dispose();
            this.referralsLoadedToken = null;
        }

        // MVVM Properties
        public string EarnMoneyForEveryoneYourInvite => IDriveYourCarStrings.EarnMoneyForEveryoneYourInvite.Replace("\\n", Environment.NewLine);

        public INotifyTaskCompletion RedeemBalanceTaskCompletion { get; private set; }

        public decimal EarnedToDate { get; private set; }

        // MVVM Commands
        public ICommand RedeemBalanceCommand { get; private set; }

        // public methods

        // private methods
        private async Task RedeemBalanceAsync()
        {
            await this.referralService.RedeemAsync(
                success: () => this.snackbarHandler.ShowMessage(IDriveYourCarStrings.RedeemBalanceRequestSent));
        }

        private void CalculateEarnedToDate(Referrals referrals)
        {
            if(referrals == null || !referrals.Clients.Any())
                this.EarnedToDate = 0;
            else
                foreach(var clientInvited in referrals.Clients)
                    this.EarnedToDate += clientInvited.ReferralBalance;
        }
    }
}
