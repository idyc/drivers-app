﻿using System.Collections.ObjectModel;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.ViewModels
{
	public class RewardsViewModel : IDYCWithMenuActionsViewModel
	{
		public RewardsViewModel(IMenuNavigationActionsWrapper menuNavigationActionsWrapper)
			: base(menuNavigationActionsWrapper)
		{
			this.Rewards = new ObservableCollection<RewardItemViewModel>();
		}

		protected override void InitializeCommands()
		{
			base.InitializeCommands();
		}

		// MVX Life cycle
		public void Init()
		{
		}

		public override void Start()
		{
			base.Start();

			for(int i = 0; i < 10; i++)
				this.Rewards.Add(
					new RewardItemViewModel(new Reward
					{
						Id = i,
						Title = $"Reward number {i}",
						Detail = "Jiffy Lube helps you get a new set of whatever you need for an unbeatable price. Just copy the discount code and apply it to your next purchase ay any store.",
						Code = $"ABSZ{i}R2Q"
					}));
		}

		// MVVM Properties
		public ObservableCollection<RewardItemViewModel> Rewards { get; set; }

		// MVVM Commands

		// public methods

		// private methods
	}
}
