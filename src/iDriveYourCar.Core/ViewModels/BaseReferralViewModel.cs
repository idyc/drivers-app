﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.Validations;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using Plugin.Contacts;

namespace iDriveYourCar.Core.ViewModels
{
    public abstract class BaseReferralViewModel : IDYCViewModel
    {
        protected readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        protected readonly IReferralsService referralService;
        protected readonly IClipboardHandler clipboardHandler;
        protected readonly IInputFieldHandler inputFieldHandler;
        protected readonly ISnackbarHandler snackbarHandler;
        protected readonly IValidatorHelper validatorHelper;

        protected abstract InvitationType invitationType { get; }

        protected abstract decimal invitationBonus { get; }

        public abstract string EarnMoneyForEveryFriendYouInvite { get; }
        public abstract string EarnMoneyForEachFriendYouInviteWithNoLimitOnHowManyFriendsYouCanRefer { get; }

        public BaseReferralViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IReferralsService referralService,
            IClipboardHandler clipboardHandler,
            IInputFieldHandler inputFieldHandler,
            ISnackbarHandler snackbarHandler,
            IValidatorHelper validatorHelper)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.referralService = referralService;
            this.clipboardHandler = clipboardHandler;
            this.inputFieldHandler = inputFieldHandler;
            this.snackbarHandler = snackbarHandler;
            this.validatorHelper = validatorHelper;

            this.ContactsWithEmail = new ObservableCollection<EmailInvitation>();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.CopyCodeToClipboardCommand = new MvxCommand(this.CopyCodeToClipboard);

            this.DisplaySyncEmailExplanationCommand = new MvxCommand(this.DisplaySyncEmailExplanation);

            this.LoadDeviceContactsCommand = new MvxCommand(() => this.LoadDeviceContactsTaskCompletion = NotifyTaskCompletion.Create(this.LoadContactsWithEmailAsync),
                                                            () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.LoadDeviceContactsTaskCompletion));

            this.SelectAllContactsWithEmailCommand = new MvxCommand(this.SelectAllContactsWithEmail);

            this.ClearContactsSelectionCommand = new MvxCommand(this.ClearContactsSelection);

            this.InviteEmailFriendsCommand = new MvxCommand(() => this.InviteEmailFriendsTaskCompletion = NotifyTaskCompletion.Create(this.InviteEmailFriendsAsync),
                                                () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.InviteEmailFriendsTaskCompletion));

            this.InviteFriendsManuallyCommand = new MvxCommand(() => this.InviteFriendsManuallyTaskCompletion = NotifyTaskCompletion.Create(this.InviteFriendsManuallyAsync),
                                                () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.InviteFriendsManuallyTaskCompletion));
        }

        // MVX Life cycle
        public virtual void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        public bool IsReferralCodeCopied { get; set; } = false;

        private string manualEmails;
        public string ManualEmails
        {
            get { return this.manualEmails; }
            set
            {
                this.manualEmails = value;

                var emails = this.manualEmails.Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

                this.PotentialEarning = this.ValidateEmails(emails) ? emails.Count() * this.invitationBonus : 0;
            }
        }

        public decimal PotentialEarning { get; set; }

        public ObservableCollection<EmailInvitation> ContactsWithEmail { get; private set; }

        public INotifyTaskCompletion InviteEmailFriendsTaskCompletion { get; protected set; }

        public INotifyTaskCompletion InviteFriendsManuallyTaskCompletion { get; protected set; }

        public INotifyTaskCompletion LoadDeviceContactsTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand CopyCodeToClipboardCommand { get; protected set; }

        public ICommand DisplaySyncEmailExplanationCommand { get; protected set; }

        public ICommand InviteEmailFriendsCommand { get; protected set; }

        public ICommand InviteFriendsManuallyCommand { get; protected set; }

        public ICommand LoadDeviceContactsCommand { get; private set; }

        public ICommand SelectAllContactsWithEmailCommand { get; private set; }

        public ICommand ClearContactsSelectionCommand { get; private set; }

        // public methods

        // private methods
        private async Task InviteEmailFriendsAsync()
        {
            await this.referralService.InviteEmailFriendsAsync(
                this.invitationType,
                this.ContactsWithEmail.Where(c => c.Selected),
                success: () => this.snackbarHandler.ShowMessage(IDriveYourCarStrings.InvitationSentSuccessfully));
        }

        private async Task InviteFriendsManuallyAsync()
        {
            var emails = this.ManualEmails.Split(',');

            if (!this.ValidateEmails(emails))
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PleaseInputValidEmailAddresses);
                return;
            }

            await this.referralService.InviteFriendsManuallyAsync(
                this.invitationType,
                emails,
                success: () =>
                {
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.InvitationSentSuccessfully);
                    this.ManualEmails = string.Empty;
                });
        }

        private bool ValidateEmails(string[] emails)
        {
            foreach (var email in emails)
            {
                if (!this.validatorHelper.ValidateEmailRule(email))
                    return false;
            }
            return true;
        }

        private void CopyCodeToClipboard()
        {
            this.clipboardHandler.CopyToClipboard(this.Driver.User.ReferralCode);
            this.IsReferralCodeCopied = true;
        }

        private void DisplaySyncEmailExplanation()
        {
            this.inputFieldHandler.DisplayEmailContactsExplanation();
        }

        private async Task LoadContactsWithEmailAsync()
        {
            if (!await CrossContacts.Current.RequestPermission())
                return;

            CrossContacts.Current.PreferContactAggregation = false;

            if (CrossContacts.Current.Contacts == null)
                return;

            // WORKAROUND: ToList() must be done at beginning: https://github.com/jamesmontemagno/ContactsPlugin/issues/2
            this.ContactsWithEmail.Fill(CrossContacts.Current.Contacts
                                            .ToList()
                                            .Where(c => !string.IsNullOrWhiteSpace(c.LastName) && c.Emails.Count > 0)
                                            .OrderBy(c => c.LastName)
                                            .Select(c => new EmailInvitation($"{c.FirstName} {c.LastName}", c.Emails.First().Address))
                                            .ToList());
        }

        private void SelectAllContactsWithEmail()
        {
            foreach (var c in this.ContactsWithEmail)
                c.SetSelectedCommand.Execute(true);
        }

        private void ClearContactsSelection()
        {
            foreach (var c in this.ContactsWithEmail)
                c.SetSelectedCommand.Execute(false);
        }
    }
}
