﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Twilio;
using iDriveYourCar.Core.ViewModels.Items.Message;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ConversationViewModel : IDYCViewModel
    {
        private const string CHANNEL_IDENTIFIER_BUNDLE_KEY = "channelIdentifier";
        private const int PAGE_SIZE = 50;

        private readonly IIDYCSettings idycSettings;
        private readonly ITwilioHandler twilioHandler;

        private string channelIdentifier;

        public ConversationViewModel(
            IIDYCSettings idycSettings,
            ITwilioHandler twilioHandler)
        {
            this.idycSettings = idycSettings;
            this.twilioHandler = twilioHandler;

            this.Messages = new ObservableCollection<MessageReceivedItemViewModel>();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SendNewMessageCommand = new MvxCommand(() => this.SendNewMessageTaskCompletion = NotifyTaskCompletion.Create(this.SendNewMessage),
                                                        () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SendNewMessageTaskCompletion));
        }

        // MVX Life cycle
        public void Init(string channelIdentifier)
        {
            this.channelIdentifier = channelIdentifier;

            this.ChatIdentity = this.idycSettings.ChatIdentity;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[CHANNEL_IDENTIFIER_BUNDLE_KEY] = this.channelIdentifier;
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.channelIdentifier = state.Data[CHANNEL_IDENTIFIER_BUNDLE_KEY];
        }

        public override void Start()
        {
            base.Start();

            this.CreateLoadTask(this.InitializeTwilio);

            this.twilioHandler.MessageReceived += this.TwilioHandler_MessageReceived;
        }

        // MVVM Properties
        public string ChatIdentity { get; set; }

        public ObservableCollection<MessageReceivedItemViewModel> Messages { get; set; }

        public string NewText { get; set; }

        public INotifyTaskCompletion SendNewMessageTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SendNewMessageCommand { get; private set; }

        // public methods

        // private methods
        private async Task InitializeTwilio()
        {
            await this.twilioHandler.Initialize(this.idycSettings.ChatToken);

            await this.twilioHandler.TryJoinOrCreateChannelAsync(this.channelIdentifier);

            var messages = await this.twilioHandler.GetMessagesAsync(0, PAGE_SIZE, this.channelIdentifier);

            this.Messages.Fill(messages.Select(m => new MessageReceivedItemViewModel(m)));
        }

        private async Task SendNewMessage()
        {
            await this.twilioHandler.TrySendMessageAsync(this.channelIdentifier, this.NewText);

            this.NewText = null;
        }

        private void TwilioHandler_MessageReceived(object sender, Twilio.TwilioMessageReceivedEventArgs e)
        {
            if(e.Channel == this.channelIdentifier)
                this.Messages.Add(new MessageReceivedItemViewModel(new Models.ChatMessage { Author = "asd", Body = e.Message }));
        }
    }
}
