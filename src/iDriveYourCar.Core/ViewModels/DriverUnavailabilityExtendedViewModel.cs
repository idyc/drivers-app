﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;
using iDriveYourCar.Core.Services.Interfaces;
using System.Linq;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Handlers;

namespace iDriveYourCar.Core.ViewModels
{
    public class DriverUnavailabilityExtendedViewModel : IDYCViewModel
    {
        private readonly IDriverUnavailabilityService driverUnavailabilityService;
        private readonly IMvxMessenger mvxMessenger;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly ISnackbarHandler snackbarHandler;

        private MvxSubscriptionToken itemAddedOrRemovedToken, driverLoadedSubscriptionToken;

        public DriverUnavailabilityExtendedViewModel(
            IDriverUnavailabilityService driverUnavailabilityService,
            IMvxMessenger mvxMessenger,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            ISnackbarHandler snackbarHandler)
        {
            this.driverUnavailabilityService = driverUnavailabilityService;
            this.mvxMessenger = mvxMessenger;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.snackbarHandler = snackbarHandler;

            this.ExtendedUnavailabilities = new ObservableCollection<DriverUnavailabilityExtendedItemViewModel>();

            // driver loaded
            this.driverLoadedSubscriptionToken = this.mvxMessenger.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message => this.FillUnavailabilities());

            this.itemAddedOrRemovedToken = this.mvxMessenger.SubscribeOnMainThread<GenericCUDMessage<DriverUnavailabilityExtendedItemViewModel>>(
                message =>
            {
                switch(message.Operation)
                {
                    case CUDOperation.Create:
                        this.AddUnavailability(message.Target);
                        break;
                    case CUDOperation.Delete:
                        this.RemoveUnavailability(message.Target);
                        break;
                }

                this.SetTimeOffTaskCompletion = NotifyTaskCompletion.Create(this.PersistDriverUnavailabilityAsync);
            });
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SetTimeOffCommand = new MvxCommand(this.RequestAddTimeOff);
        }

        // MVX Life cycle
        public void Init()
        {

        }

        public override void Start()
        {
            base.Start();

            if(this.Driver != null)
                this.FillUnavailabilities();
        }

        public override void Destroy()
        {
            base.Destroy();

            // TODO: Find right place to put this things
            //this.driverLoadedSubscriptionToken?.Dispose();
            //this.driverLoadedSubscriptionToken = null;

            //this.itemAddedOrRemovedToken?.Dispose();
            //this.itemAddedOrRemovedToken = null;
        }

        // MVVM Properties
        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        public ObservableCollection<DriverUnavailabilityExtendedItemViewModel> ExtendedUnavailabilities { get; set; }

        public INotifyTaskCompletion SetTimeOffTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SetTimeOffCommand { get; private set; }

        // public methods

        // private methods
        private void FillUnavailabilities()
        {
            this.ExtendedUnavailabilities.Clear();

            foreach(var driverUnavailability in this.Driver.Unavailability)
                this.ExtendedUnavailabilities.Add(new DriverUnavailabilityExtendedItemViewModel(this.mvxMessenger, driverUnavailability));
        }

        private void RequestAddTimeOff()
        {
            this.ShowViewModelWithTrack<DriverUnavailabilityExtendedCreationViewModel>();
        }

        private void RemoveUnavailability(DriverUnavailabilityExtendedItemViewModel itemViewModel)
        {
            if(this.ExtendedUnavailabilities.Contains(itemViewModel))
                this.ExtendedUnavailabilities.Remove(itemViewModel);

            this.snackbarHandler.ShowMessage(IDriveYourCarStrings.DateBlockRemoved);
        }

        private void AddUnavailability(DriverUnavailabilityExtendedItemViewModel itemViewModel)
        {
            this.ExtendedUnavailabilities.Add(itemViewModel);
        }

        private async Task PersistDriverUnavailabilityAsync()
        {
            await this.driverUnavailabilityService.UpdateDriverUnavailabilityAsync(
                this.ExtendedUnavailabilities.Select(i => i.DriverUnavailability),
                success: updatedDriver => this.universalObjectWrapperSingletonDriver.UniversalObject = updatedDriver);
        }
    }
}
