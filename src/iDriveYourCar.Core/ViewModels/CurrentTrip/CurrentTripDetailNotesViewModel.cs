﻿using System;
using System.Linq;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
    public class CurrentTripDetailNotesViewModel : BaseTripDetailViewModel
    {
        private const string TRIP_ID_BUNDLE_KEY = "tripId";

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton;

        private long tripId;

        public CurrentTripDetailNotesViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton,
            IExternalMapHandler externalMapHandler,
            IAlertHandler alertHandler,
            IAppStoreHandler appStoreHandler)
            : base(externalMapHandler, alertHandler, appStoreHandler)
        {
            this.universalObjectWrapperSingleton = universalObjectWrapperSingleton;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();
        }

        // MVX Life cycle
        public void Init(long tripId)
        {
            this.tripId = tripId;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_ID_BUNDLE_KEY] = this.tripId.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripId = long.Parse(state.Data[TRIP_ID_BUNDLE_KEY]);
        }

        public override void Start()
        {
            base.Start();

            this.Trip = this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.First(t => t.Id == this.tripId);
        }

        // MVVM Properties

        // MVVM Commands

        // public methods

        // private methods

    }
}
