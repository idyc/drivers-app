﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;
using PropertyChanged;

namespace iDriveYourCar.Core.ViewModels
{
    public class CurrentTripAddExpenseViewModel : IDYCWithCurrentTripMenuActionsViewModel
    {
        private const string TRIP_ID_BUNDLE_KEY = "tripId";
        private const string CLOSE_ON_CONTINUE_BUNDLE_KEY = "closeOnContinue";

        private long tripId;
        private bool closeOnContinue;

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton;
        private readonly IInputFieldHandler inputFieldHandler;
        private readonly IImageHelper imageHelper;
        private readonly ITripsService tripService;
        private readonly IMvxMessenger mvxMessenger;

        public CurrentTripAddExpenseViewModel(
            ICurrentTripMenuNavigationActionsWrapper currentTripMenuNavigationActionsWrapper,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton,
            IInputFieldHandler inputFieldHandler,
            IImageHelper imageHelper,
            ITripsService tripService,
            IMvxMessenger mvxMessenger)
        : base(currentTripMenuNavigationActionsWrapper)
        {
            this.universalObjectWrapperSingleton = universalObjectWrapperSingleton;
            this.inputFieldHandler = inputFieldHandler;
            this.imageHelper = imageHelper;
            this.tripService = tripService;
            this.mvxMessenger = mvxMessenger;

            this.CurrentTripMenuNavigationActionsWrapper.ToggleMenuAvailability(CurrentTripMenuAction.AddExpense);
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SubmitExpenseCommand = new MvxCommand(() => this.SubmitExpenseTaskCompletion = NotifyTaskCompletion.Create(this.SubmitExpenseAsync),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SubmitExpenseTaskCompletion));

            this.AttachReceiptCommand = new MvxCommand(() => AttachReceiptTaskCompletion = NotifyTaskCompletion.Create(this.AttachReceiptAsync),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(AttachReceiptTaskCompletion));

            this.RemoveAttachedFileCommand = new MvxCommand(this.RemoveAttachedFile);
        }

        public void Init(long tripId, bool closeOnContinue)
        {
            this.tripId = tripId;
            this.closeOnContinue = closeOnContinue;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_ID_BUNDLE_KEY] = this.tripId.ToString();
            bundle.Data[CLOSE_ON_CONTINUE_BUNDLE_KEY] = this.closeOnContinue.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripId = long.Parse(state.Data[TRIP_ID_BUNDLE_KEY]);
            this.closeOnContinue = bool.Parse(state.Data[CLOSE_ON_CONTINUE_BUNDLE_KEY]);
        }

        public override void Start()
        {
            base.Start();

            this.Trip = this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.First(t => t.Id == this.tripId);
        }

        // MVVM Properties
        [DoNotCheckEquality]
        public Trip Trip { get; set; }

        public Stream File { get; set; } = null;

        public string Filename { get; set; } = string.Empty;

        public string Description { get; set; }

        public decimal? Amount { get; set; }

        public INotifyTaskCompletion SubmitExpenseTaskCompletion { get; private set; }

        public INotifyTaskCompletion AttachReceiptTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SubmitExpenseCommand { get; private set; }

        public ICommand AttachReceiptCommand { get; private set; }

        public ICommand RemoveAttachedFileCommand { get; private set; }

        // public methods

        // private methods
        private async Task SubmitExpenseAsync()
        {
            if(string.IsNullOrEmpty(this.Description) || !this.Amount.HasValue)
                return;

            if(this.File != null)
                await this.AddExpenseWithFile();
            else
                await this.AddExpenseWithoutFile();
        }

        private async Task AddExpenseWithFile()
        {
            await this.tripService.AddExpenseAsync(
                    this.tripId,
                    this.File,
                    this.Filename,
                    this.Description,
                    this.Amount.Value,
                    success: trip =>
                    {
                        this.UpdateCurrentTrip(trip);
                    });
        }

        private async Task AddExpenseWithoutFile()
        {
            await this.tripService.AddExpenseAsync(
                            this.tripId,
                            this.Description,
                            this.Amount.Value,
                            success: trip =>
                            {
                                this.UpdateCurrentTrip(trip);
                            });
        }

        private void UpdateCurrentTrip(Trip trip)
        {
            this.Trip = trip;

            if(this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.Contains(this.Trip))
                this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.Remove(this.Trip);

            this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.Add(this.Trip);

            this.File = null;
            this.Filename = string.Empty;
            this.Description = string.Empty;
            this.Amount = null;

            if(this.closeOnContinue)
            {
                this.CloseWithTrack(this);
                return;
            }

            if(this.Trip.ActiveTripState == ActiveTripState.EndTrip)
                this.ContinueToViewModel<CurrentTripExpensesViewModel>();
            else
                this.ContinueToViewModel<CurrentTripDetailViewModel>();
        }

        private async Task AttachReceiptAsync()
        {
            var options = new[] { IDriveYourCarStrings.TakePhoto, IDriveYourCarStrings.ChooseFromGallery };
            var selectedSource = await this.inputFieldHandler.ShowOptionsAlertAsync(IDriveYourCarStrings.AttachReceipt, options);

            // if no option was selected, return
            if(string.IsNullOrEmpty(selectedSource))
                return;

            var source = selectedSource == IDriveYourCarStrings.TakePhoto ? ImageSource.Camera : ImageSource.Gallery;

            var file = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
                            source,
                            "receiptCacheKey",
                            Guid.NewGuid().ToString(),
                            Plugin.Media.Abstractions.PhotoSize.Full,
                            100);

            // if no image was selected, return
            if(file == null)
                return;

            this.File = file.GetStream();
            this.Filename = Path.GetFileName(file.Path);
        }

        private void RemoveAttachedFile()
        {
            this.Filename = string.Empty;
            this.File = null;
            return;
        }

        private void ContinueToViewModel<TViewModel>() where TViewModel : FwkBaseViewModel
        {
            this.CloseWithTrack(this);

            this.ShowViewModelWithTrack<TViewModel>(new { tripId = this.tripId });
        }
    }
}
