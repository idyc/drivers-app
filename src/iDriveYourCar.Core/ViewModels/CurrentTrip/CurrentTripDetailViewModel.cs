using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using PropertyChanged;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Resources;

namespace iDriveYourCar.Core.ViewModels
{
    public class CurrentTripDetailViewModel : IDYCWithCurrentTripMenuActionsHostViewModel
    {
        private const string TRIP_ID_BUNDLE_KEY = "tripId";
        private const string FOLLOWING_STATE_BUNDLE_KEY = "followingState";

        private long tripId;

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton;
        private readonly IExternalMapHandler externalMapHandler;
        private readonly IGeolocatorHelper geolocatorHelper;
        private readonly IAlertHandler alertHandler;
        private readonly IAppStoreHandler appStoreHandler;
        private readonly ITripWorkflowManager tripWorkflowManager;
        private readonly ITripsService tripService;

        public CurrentTripDetailViewModel(
            ICurrentTripMenuNavigationActionsWrapper currentTripMenuNavigationActionsWrapper,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton,
            IExternalMapHandler externalMapHandler,
            IGeolocatorHelper geolocatorHelper,
            IAlertHandler alertHandler,
            IAppStoreHandler appStoreHandler,
            ITripWorkflowManager tripWorkflowManager,
            ITripsService tripService)
        : base(currentTripMenuNavigationActionsWrapper)
        {
            this.universalObjectWrapperSingleton = universalObjectWrapperSingleton;
            this.externalMapHandler = externalMapHandler;
            this.geolocatorHelper = geolocatorHelper;
            this.alertHandler = alertHandler;
            this.appStoreHandler = appStoreHandler;
            this.tripWorkflowManager = tripWorkflowManager;
            this.tripService = tripService;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.PerformWorkflowCommand = new MvxCommand(() => PerformWorkflowTaskCompletion = NotifyTaskCompletion.Create(this.PerformWorkflowActionAsync),
                                                          () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(PerformWorkflowTaskCompletion));

            this.UpdateFollowingStateCommand = new MvxCommand(this.UpdateTripAndSetFollowingTripState);
        }

        public virtual void Init(long tripId)
        {
            this.tripId = tripId;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_ID_BUNDLE_KEY] = this.tripId.ToString();
            bundle.Data[FOLLOWING_STATE_BUNDLE_KEY] = this.FollowingState.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripId = long.Parse(state.Data[TRIP_ID_BUNDLE_KEY]);

            ActiveTripState tripState = ActiveTripState.NotStarted;
            Enum.TryParse<ActiveTripState>(state.Data[FOLLOWING_STATE_BUNDLE_KEY], out tripState);

            this.FollowingState = tripState;
        }

        // MVX Life cycle
        public override void Start()
        {
            base.Start();

            this.UpdateTripAndSetFollowingTripState();

            // start child ViewModels
            var data = new Dictionary<string, string> { [TRIP_ID_BUNDLE_KEY] = this.tripId.ToString() };

            IMvxBundle bundle = new MvxBundle(data);

            this.CurrentTripDetailInfo = this.LoadViewModel<CurrentTripDetailInfoViewModel>(bundle);
            this.CurrentTripDetailNotes = this.LoadViewModel<CurrentTripDetailNotesViewModel>(bundle);
        }

        // MVVM Properties
        public CurrentTripDetailInfoViewModel CurrentTripDetailInfo { get; set; }

        public CurrentTripDetailNotesViewModel CurrentTripDetailNotes { get; set; }

        public INotifyTaskCompletion PerformWorkflowTaskCompletion { get; private set; }

        public INotifyTaskCompletion DisplayNavigationTaskCompletion { get; private set; }

        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingleton.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingleton.UniversalObject = value;
            }
        }

        [DoNotCheckEquality]
        public Trip Trip { get; private set; }

        public ActiveTripState FollowingState { get; private set; }

        public string FollowingActionName { get; set; }

        // MVVM Commands
        public ICommand PerformWorkflowCommand { get; private set; }

        public ICommand UpdateFollowingStateCommand { get; private set; }

        // public methods

        // private methods
        private async Task PerformWorkflowActionAsync()
        {
            await this.tripService.PerformTripWorkflowAsync(
                this.tripId,
                this.FollowingState,
                updatedTrip =>
                {
                    this.Trip = updatedTrip;
                    this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.Replace(this.Trip);
                    this.DisplayNavigationTaskCompletion = NotifyTaskCompletion.Create(this.DisplayNavigationAsync);
                });
        }

        private async Task DisplayNavigationAsync()
        {
            var stops = new List<Tuple<decimal, decimal>>(this.tripWorkflowManager.GetFollowingDirectionsByState(this.Trip, this.FollowingState));

            if(!stops.Any())
            {
                this.UpdateTripAndSetFollowingTripState();
                return;
            }

            var userLocation = await this.geolocatorHelper.GetCurrentPositionAsync(10000);
            // mocked location
            //Geoposition userLocation = new Geoposition
            //{
            //    Latitude = -32.9477281,
            //    Longitude = -60.6548185
            //};

            if(userLocation == null)
            {
                this.alertHandler.ShowMessage(IDriveYourCarStrings.TheCurrentLocationCouldNotBeFetchedPleaseInitializeNavigationManually);
                return;
            }
            stops.Insert(0, new Tuple<decimal, decimal>((decimal)userLocation.Latitude, (decimal)userLocation.Longitude));

            if(!this.externalMapHandler.TryInitiateNavigation(stops))
            {
                // if navigation could not be initialized, then prompt the user to install Google Maps
                this.alertHandler.ShowMessage(
                    IDriveYourCarStrings.GoogleMapsIsRequired,
                    IDriveYourCarStrings.PleaseInstallGoogleMaps,
                    IDriveYourCarStrings.Download,
                    IDriveYourCarStrings.Cancel,
                    () => this.appStoreHandler.ShowAppStoreGoogleMaps(),
                    () => { }
                );
            }
        }

        private void UpdateTripAndSetFollowingTripState()
        {
            this.Trip = this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.First(t => t.Id == this.tripId);

            this.FollowingState = this.tripWorkflowManager.GetFollowingTripState(this.Trip.TripType, this.Trip.ActiveTripState);
            this.FollowingActionName = this[this.FollowingState.ToString()].ToUpper();

            // if the next stage is submit, then close this screen and show SubmitTrip
            if(this.FollowingState == ActiveTripState.SubmitTrip)
            {
                if(!this.Trip.Expenses.Any())
                {
                    this.alertHandler.ShowMessage(
                        string.Empty,
                        IDriveYourCarStrings.WouldYouLikeToAddAnExpenseToThisTrip,
                        Strings.Yes,
                        Strings.No,
                        this.ContinueToViewModel<CurrentTripAddExpenseViewModel>,
                        this.ContinueToViewModel<CurrentTripSubmitViewModel>);
                }
                else
                {
                    this.ContinueToViewModel<CurrentTripExpensesViewModel>();
                }
            }
        }

        private void ContinueToViewModel<TViewModel>() where TViewModel : IDYCViewModel
        {
            this.CloseWithTrack(this);

            this.ShowViewModelWithTrack<TViewModel>(new { tripId = this.tripId });
        }
    }
}
