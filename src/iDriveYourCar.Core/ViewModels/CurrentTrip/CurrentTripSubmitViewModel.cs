﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.MvxHints;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using PropertyChanged;

namespace iDriveYourCar.Core.ViewModels
{
    public class CurrentTripSubmitViewModel : IDYCWithCurrentTripMenuActionsViewModel
    {
        private const string TRIP_ID_BUNDLE_KEY = "tripId";

        private readonly ITripsService tripService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton;

        private long tripId;

        public CurrentTripSubmitViewModel(
            ICurrentTripMenuNavigationActionsWrapper currentTripMenuNavigationActionsWrapper,
            ITripsService tripService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton)
            : base(currentTripMenuNavigationActionsWrapper)
        {
            this.tripService = tripService;
            this.universalObjectWrapperSingleton = universalObjectWrapperSingleton;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SubmitTripCommand = new MvxCommand(() => this.SubmitTripTaskCompletion = NotifyTaskCompletion.Create(this.SubmitTripAsync),
                                                    () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SubmitTripTaskCompletion));
        }

        // MVX Life cycle
        public void Init(long tripId)
        {
            this.tripId = tripId;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_ID_BUNDLE_KEY] = this.tripId.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripId = long.Parse(state.Data[TRIP_ID_BUNDLE_KEY]);
        }

        public override void Start()
        {
            base.Start();

            this.Trip = this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.First(t => t.Id == this.tripId);
        }

        // MVVM Properties
        public string NotesForDrivers { get; set; }

        public string FollowingActionName => IDriveYourCarStrings.SubmitTrip.ToUpper();

        [DoNotCheckEquality]
        public Trip Trip { get; private set; }

        public INotifyTaskCompletion SubmitTripTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SubmitTripCommand { get; private set; }

        // public methods

        // private methods
        private async Task SubmitTripAsync()
        {
            await this.tripService.SubmitTripAsync(
                this.tripId,
                this.NotesForDrivers,
                success: trip =>
            {
                this.Trip = trip;

                if(this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.Contains(this.Trip))
                    this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.Remove(this.Trip);

                this.universalObjectWrapperSingleton.UniversalObject.FinishedTrips.Add(this.Trip);

                this.ChangePresentation(new ClearBackStackHint());
            });
        }
    }
}
