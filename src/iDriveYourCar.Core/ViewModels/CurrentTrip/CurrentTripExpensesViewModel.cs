﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;
using PropertyChanged;
using DGenix.Mobile.Fwk.Core;

namespace iDriveYourCar.Core.ViewModels
{
    public class CurrentTripExpensesViewModel : IDYCWithCurrentTripMenuActionsViewModel
    {
        private const string TRIP_ID_BUNDLE_KEY = "tripId";

        private long tripId;

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton;
        private readonly ITripWorkflowManager tripWorkflowManager;
        private readonly IMvxMessenger mvxMessenger;
        private readonly ISnackbarHandler snackbarHandler;
        private readonly ITripsService tripService;

        private ActiveTripState followingState;

        private MvxSubscriptionToken expenseRemovedToken;

        public CurrentTripExpensesViewModel(
            ICurrentTripMenuNavigationActionsWrapper currentTripMenuNavigationActionsWrapper,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingleton,
            ITripWorkflowManager tripWorkflowManager,
            IMvxMessenger mvxMessenger,
            ISnackbarHandler snackbarHandler,
            ITripsService tripService)
        : base(currentTripMenuNavigationActionsWrapper)
        {
            this.universalObjectWrapperSingleton = universalObjectWrapperSingleton;
            this.tripWorkflowManager = tripWorkflowManager;
            this.mvxMessenger = mvxMessenger;
            this.snackbarHandler = snackbarHandler;
            this.tripService = tripService;

            this.expenseRemovedToken = this.mvxMessenger.SubscribeOnMainThread<GenericCUDMessage<CurrentTripExpenseItemViewModel>>(
                message =>
            {
                this.RemoveExpenseTaskCompletion = NotifyTaskCompletion.Create(this.RemoveExpenseAsync(message.Target));
            });

            this.Expenses = new ObservableCollection<CurrentTripExpenseItemViewModel>();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.AddExpenseCommand = new MvxCommand(this.AddExpense);
            this.ContinueCommand = new MvxCommand(this.ContinueToSubmitTrip);
            this.ReloadCommand = new MvxCommand(this.ReloadExpenses);
        }

        public void Init(long tripId)
        {
            this.tripId = tripId;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_ID_BUNDLE_KEY] = this.tripId.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripId = long.Parse(state.Data[TRIP_ID_BUNDLE_KEY]);
        }

        public override void Start()
        {
            base.Start();

            this.Trip = this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.FirstOrDefault(t => t.Id == this.tripId);

            this.FillExpenses();

            //Mock expenses
            //this.Trip.Expenses = new List<Expense>
            //{
            //    new Expense { Description = "Starbucks Coffee", Amount = (decimal?)10.99, Filename = "screenshot1.png" },
            //    new Expense { Description = "Traffic Toll", Amount = (decimal?)19.99 },
            //    new Expense { Description = "NYT Newspaper", Amount = (decimal?)10.99, Filename = "screenshot3.png" },
            //    new Expense { Description = "Starbucks Muffin", Amount = (decimal?)8.99  },
            //    new Expense { Description = "Starbucks Coffee", Amount = (decimal?)10.99, Filename = "screenshot1.png" },
            //    new Expense { Description = "Traffic Toll", Amount = (decimal?)19.99 },
            //    new Expense { Description = "NYT Newspaper", Amount = (decimal?)10.99, Filename = "screenshot3.png" },
            //    new Expense { Description = "Starbucks Muffin", Amount = (decimal?)8.99  }
            //};

            //var expenses = new ObservableCollection<CurrentTripExpenseItemViewModel>
            //{
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "Starbucks Coffee", Amount = (decimal?)10.99, Filename = "screenshot1.png" }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "Traffic Toll", Amount = (decimal?)19.99 }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "NYT Newspaper", Amount = (decimal?)10.99, Filename = "screenshot3.png" }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "Starbucks Muffin", Amount = (decimal?)8.99  }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "Starbucks Coffee", Amount = (decimal?)10.99, Filename = "screenshot1.png" }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "Traffic Toll", Amount = (decimal?)19.99 }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "NYT Newspaper", Amount = (decimal?)10.99, Filename = "screenshot3.png" }),
            //    new CurrentTripExpenseItemViewModel(this.mvxMessenger, new Expense { Description = "Starbucks Muffin", Amount = (decimal?)8.99  })
            //};
            //this.Expenses.Fill(expenses);
        }

        // MVVM Properties
        [DoNotCheckEquality]
        public Trip Trip { get; set; }

        public ObservableCollection<CurrentTripExpenseItemViewModel> Expenses { get; set; }

        public string FollowingActionName { get; set; } = IDriveYourCarStrings.Continue.ToUpper();

        public INotifyTaskCompletion AddExpenseTaskCompletion { get; private set; }

        public INotifyTaskCompletion RemoveExpenseTaskCompletion { get; private set; }

        public ActiveTripState FollowingState { get; private set; }

        // MVVM Commands
        public ICommand AddExpenseCommand { get; private set; }

        public ICommand ContinueCommand { get; private set; }

        public ICommand ReloadCommand { get; private set; }

        // public methods

        // private methods
        private void ReloadExpenses()
        {
            this.FillExpenses();
        }

        private void FillExpenses()
        {
            this.Expenses.Clear();

            this.Trip = this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.FirstOrDefault(t => t.Id == this.tripId);

            foreach(var expense in this.Trip.Expenses)
                this.Expenses.Add(new CurrentTripExpenseItemViewModel(this.mvxMessenger, expense));
        }

        private void AddExpense()
        {
            this.ShowViewModelWithTrack<CurrentTripAddExpenseViewModel>(new { tripId = this.tripId, closeOnContinue = true });
        }

        private void ContinueToSubmitTrip()
        {
            this.ShowViewModelWithTrack<CurrentTripSubmitViewModel>(new { tripId = this.tripId });
        }

        private async Task RemoveExpenseAsync(CurrentTripExpenseItemViewModel expenseItemViewModel)
        {
            await this.tripService.DeleteExpenseAsync(
                this.tripId,
                expenseItemViewModel.Expense.Id,
                updatedTrip =>
            {
                this.Trip = updatedTrip;
                this.universalObjectWrapperSingleton.UniversalObject.UpcomingTrips.ReplaceIfExists(updatedTrip, oldTrip => oldTrip.Id == updatedTrip.Id);

                if(this.Expenses.Contains(expenseItemViewModel))
                    this.Expenses.Remove(expenseItemViewModel);

                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.ExpenseRemoved);
            });
        }
    }
}
