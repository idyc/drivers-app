﻿using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Support
{
    public class TaxClassificationType
    {
        public TaxClassificationType(string description, TaxClassification type)
        {
            this.Description = description;
            this.Type = type;
        }

        public string Description { get; set; }
        public TaxClassification Type { get; set; }
    }
}
