﻿using System;
namespace iDriveYourCar.Core.ViewModels.Support
{
	public class FindOpenTripsFilterType
	{
		public FindOpenTripsFilterType(string description, FindOpenTripsFilterTypes type)
		{
			this.Description = description;
			this.Type = type;
		}

		public string Description { get; set; }
		public FindOpenTripsFilterTypes Type { get; set; }
	}

	public enum FindOpenTripsFilterTypes
	{
		DrivingTimeFromHome,
		SpecificAddress
	}
}
