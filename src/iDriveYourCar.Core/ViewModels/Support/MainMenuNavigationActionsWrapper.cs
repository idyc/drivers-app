﻿using System;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Plugin.Messaging;

namespace iDriveYourCar.Core.ViewModels.Support
{
    public class MainMenuNavigationActionsWrapper : BaseMenuNavigationActionsWrapper
    {
        // TODO: Delete when all the commands get implemented
        private readonly Lazy<IToastHandler> toastHandler = new Lazy<IToastHandler>(() => Mvx.Resolve<IToastHandler>());

        protected override void InitializeCommands()
        {
            this.CallDispatchCommand = new MvxCommand(this.CallDispatch);
            this.MessageDispatchCommand = new MvxCommand(this.ActionMocked);
        }

        private void ActionMocked()
        {
            this.toastHandler.Value.ShowToast("Not implemented yet");
        }

        private void CallDispatch()
        {
            if(CrossMessaging.Current.PhoneDialer.CanMakePhoneCall)
                CrossMessaging.Current.PhoneDialer.MakePhoneCall("+1 855-238-6708", "Drive My Car");
        }
    }
}