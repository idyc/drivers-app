﻿using System;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules;
using MvvmCross.Core.ViewModels;
using Plugin.Messaging;

namespace iDriveYourCar.Core.ViewModels.Support
{
    public class CurrentTripNavigationActionsWrapper : CurrentTripMenuNavigationActionsWrapper
    {
        private readonly ITripWorkflowManager tripWorkflowManager;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;

        private Dictionary<CurrentTripMenuAction, bool> currentTripMenuActionAvailability;

        public CurrentTripNavigationActionsWrapper(
            ITripWorkflowManager tripWorkflowManager,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver)
        {
            this.tripWorkflowManager = tripWorkflowManager;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;

            this.currentTripMenuActionAvailability = new Dictionary<CurrentTripMenuAction, bool>();
            foreach(CurrentTripMenuAction menuAction in Enum.GetValues(typeof(CurrentTripMenuAction)))
                this.currentTripMenuActionAvailability.Add(menuAction, true);
        }

        protected override void InitializeCommands()
        {
            this.CallDispatchCommand = new MvxCommand(this.CallDispatch, () => this.CanExecuteMenuAction(CurrentTripMenuAction.CallDispatch));
            this.MessageDispatchCommand = new MvxCommand(this.MessageDispatch, () => this.CanExecuteMenuAction(CurrentTripMenuAction.MessageDispatch));
            this.AddExpenseCommand = new MvxCommand(this.AddExpense, () => this.CanExecuteMenuAction(CurrentTripMenuAction.AddExpense));
            this.CallPassengerCommand = new MvxCommand(this.CallPassenger, () => this.CanExecuteMenuAction(CurrentTripMenuAction.CallPassenger));
            this.TextPassengerCommand = new MvxCommand(this.TextPassenger, () => this.CanExecuteMenuAction(CurrentTripMenuAction.TextPassenger));
        }

        private void CallDispatch()
        {
            if(CrossMessaging.Current.PhoneDialer.CanMakePhoneCall)
                CrossMessaging.Current.PhoneDialer.MakePhoneCall("123456789", "Sample number");
        }

        private void MessageDispatch()
        {
            if(CrossMessaging.Current.SmsMessenger.CanSendSms)
                CrossMessaging.Current.SmsMessenger.SendSms("123456789", "Sample number");
        }

        private void AddExpense()
        {
            var activeTrip = this.tripWorkflowManager.GetActiveTrip(universalObjectWrapperSingletonDriver.UniversalObject);
            if(activeTrip != null)
                this.ShowViewModelWithTrack<CurrentTripAddExpenseViewModel>(new { tripId = activeTrip.Id });
        }

        private void CallPassenger()
        {
            if(CrossMessaging.Current.PhoneDialer.CanMakePhoneCall)
                CrossMessaging.Current.PhoneDialer.MakePhoneCall("123456789", "Sample number");
        }

        private void TextPassenger()
        {
            if(CrossMessaging.Current.SmsMessenger.CanSendSms)
                CrossMessaging.Current.SmsMessenger.SendSms("123456789", "Sample number");
        }

        public override void ToggleMenuAvailability(CurrentTripMenuAction currentTripMenuAction)
        {
            this.currentTripMenuActionAvailability[currentTripMenuAction] = !this.currentTripMenuActionAvailability[currentTripMenuAction];
        }

        private bool CanExecuteMenuAction(CurrentTripMenuAction menuAction)
        {
            return this.currentTripMenuActionAvailability[menuAction];
        }
    }
}
