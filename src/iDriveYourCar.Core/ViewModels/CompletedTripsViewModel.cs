﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels
{
    public class CompletedTripsViewModel : IDYCViewModel
    {
        private const int NUMBER_OF_DAYS_IN_A_WEEK = 7;

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IMvxMessenger mvxMessenger;

        private MvxSubscriptionToken driverLoadedSubscriptionToken;

        public CompletedTripsViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IMvxMessenger mvxMessenger)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.mvxMessenger = mvxMessenger;

            this.CompletedTrips = new ObservableCollection<CompletedTripsDayItemViewModel>();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.GetDataCommand = new MvxCommand(this.GetData);
        }

        // MVX Life cycle
        public void Init()
        {
            this.driverLoadedSubscriptionToken = this.mvxMessenger.SubscribeOnMainThread<UniversalObjectUpdatedMessage<Driver>>(
                message => this.GetThisWeekData());
        }

        public override void Start()
        {
            base.Start();

            if(this.Driver != null)
                this.GetThisWeekData();
        }

        // MVVM Properties
        public decimal? WeeklyEarnings { get; set; }

        public int CompletedTripsCount { get; set; }

        public decimal WeeklyRating { get; set; }

        public ObservableCollection<CompletedTripsDayItemViewModel> CompletedTrips { get; set; }

        public string WeekRange { get; set; }

        public bool IsLastWeekActive { get; set; }

        public Driver Driver
        {
            get
            {
                return this.universalObjectWrapperSingletonDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = value;
            }
        }

        // MVVM Commands
        public ICommand GetDataCommand { get; private set; }

        // public methods
        public void ForceRefresh()
        {
            this.GetThisWeekData();
        }

        // private methods
        private void FillCompletedTripsWithWeekDays(DateTime firstDateOfWeek)
        {
            for(int i = 0; i < NUMBER_OF_DAYS_IN_A_WEEK; i++)
            {
                this.CompletedTrips.Add(new CompletedTripsDayItemViewModel
                {
                    Date = firstDateOfWeek.AddDays(i + 1),
                    DayOfWeek = i
                });
            }
        }

        private void GetData()
        {
            if(this.IsLastWeekActive)
            {
                this.GetThisWeekData();
            }
            else
            {
                this.GetLastWeekData();
            }
        }

        private void GetThisWeekData()
        {
            this.IsLastWeekActive = false;

            this.CompletedTrips.Clear();
            this.FillCompletedTripsWithWeekDays(this.GetFirstDayOfCurrentWeek());

            var finishedTripsThisWeek = this.Driver?.FinishedTrips.Where(
                trip => trip.DropoffDateReal.ToDateTimeFromFormat() >= this.GetFirstDayOfCurrentWeek() &&
                trip.DropoffDateReal.ToDateTimeFromFormat() <= this.GetLastDayOfCurrentWeek());

            this.CalculateTotalsAndFillCompletedTrips(finishedTripsThisWeek);
            //this.CalculateTotalsAndFillCompletedTrips(this.GetMockFinishedTrips());
        }

        private void GetLastWeekData()
        {
            this.IsLastWeekActive = true;

            this.CompletedTrips.Clear();
            this.FillCompletedTripsWithWeekDays(this.GetFirstDayOfPreviousWeek());

            var finishedTripsLastWeek = this.Driver?.FinishedTrips.Where(
                trip => trip.DropoffDateReal.ToDateTimeFromFormat() >= this.GetFirstDayOfPreviousWeek() &&
                trip.DropoffDateReal.ToDateTimeFromFormat() <= this.GetLastDayOfPreviousWeek());

            this.CalculateTotalsAndFillCompletedTrips(finishedTripsLastWeek);
            //this.CalculateTotalsAndFillCompletedTrips(this.GetMockFinishedTrips());
        }

        private void CalculateTotalsAndFillCompletedTrips(IEnumerable<Trip> finishedTrips)
        {
            this.WeeklyEarnings = 0;
            this.CompletedTripsCount = 0;
            this.WeeklyRating = 0;

            decimal ratingTotal = 0;

            if(finishedTrips != null && finishedTrips.Any())
            {
                foreach(var finishedTrip in finishedTrips)
                {
                    var finishedTripDate = this.GetDateTime(finishedTrip.PickupDate);

                    var currentDate = this.CompletedTrips.SingleOrDefault(w => w.Date == finishedTripDate);

                    if(currentDate != null)
                    {
                        currentDate.AddCompletedTripTime(finishedTrip);

                        this.WeeklyEarnings += finishedTrip.Price;
                        this.CompletedTripsCount++;
                        ratingTotal += finishedTrip.Rating;
                    }
                }
            }

            if(this.CompletedTripsCount != 0)
                this.WeeklyRating = ratingTotal / this.CompletedTripsCount;
        }

        private DateTime GetFirstDayOfCurrentWeek()
        {
            return DateTime.Today.GetFirstDateOfWeek();
        }

        private DateTime GetLastDayOfCurrentWeek()
        {
            return DateTime.Today.GetLastDateOfWeek();
        }

        private DateTime GetFirstDayOfPreviousWeek()
        {
            return DateTime.Today.AddDays(-7).GetFirstDateOfWeek();
        }

        private DateTime GetLastDayOfPreviousWeek()
        {
            return DateTime.Today.AddDays(-7).GetLastDateOfWeek();
        }

        private DateTime GetDateTime(string dateString)
        {
            return DateTime.Parse(dateString);
        }

        private int GetDayOfWeek(DateTime date)
        {
            return (int)date.DayOfWeek;
        }

        private IEnumerable<Trip> GetMockFinishedTrips()
        {
            return new List<Trip>
            {
                new Trip
                {
                    PickupDate = "Sunday April 16, 2017",
                    PickupTime = "2:30 pm",
                    TotalReal = 110,
                    Price = 110,
                    Rating = 3
                },
                new Trip
                {
                    PickupDate = "Monday April 17, 2017",
                    PickupTime = "4:30 pm",
                    TotalReal = 54,
                    Price = 54,
                    Rating = 4
                },
                new Trip
                {
                    PickupDate = "Saturday April 15, 2017",
                    PickupTime = "6:00 pm",
                    TotalReal = 12,
                    Price = 12,
                    Rating = 3
                },
                new Trip
                {
                    PickupDate = "Saturday April 15, 2017",
                    PickupTime = "6:00 pm",
                    TotalReal = 23,
                    Price = 23,
                    Rating = 5
                },
                new Trip
                {
                    PickupDate = "Saturday April 15, 2017",
                    PickupTime = "6:00 pm",
                    TotalReal = 17,
                    Price = 17,
                    Rating = 2
                },
                new Trip
                {
                    PickupDate = "Friday April 14, 2017",
                    PickupTime = "6:00 pm",
                    TotalReal = 10,
                    Price = 10,
                    Rating = 4
                },
                new Trip
                {
                    PickupDate = "Friday April 14, 2017",
                    PickupTime = "6:00 pm",
                    TotalReal = 25,
                    Price = 25,
                    Rating = 3
                },
                new Trip
                {
                    PickupDate = "Friday April 14, 2017",
                    PickupTime = "7:00 pm",
                    TotalReal = 25,
                    Price = 25,
                    Rating = 3
                },
                new Trip
                {
                    PickupDate = "Thursday April 13, 2017",
                    PickupTime = "6:00 pm",
                    TotalReal = 15,
                    Price = 15,
                    Rating = 5
                }
            };
        }
    }
}
