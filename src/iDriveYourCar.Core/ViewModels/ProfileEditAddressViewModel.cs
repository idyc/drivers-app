﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules.ProfileGeneral;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ProfileEditAddressViewModel : IDYCWithMenuActionsViewModel
    {
        private const string DRIVER_FIELD_BUNDLE_KEY = "field";

        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IDriverService driverService;
        private readonly IAddressService addressService;
        private readonly ISnackbarHandler snackbarHandler;
        private readonly IDriverEditableFieldConfigurationFactory driverEditableFieldConfigurationFactory;

        private DriverEditableField driverField;

        public ProfileEditAddressViewModel(
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IDriverService driverService,
            IAddressService addressService,
            ISnackbarHandler snackbarHandler,
            IDriverEditableFieldConfigurationFactory driverEditableFieldConfigurationFactory)
            : base(menuNavigationActionsWrapper)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.driverService = driverService;
            this.addressService = addressService;
            this.snackbarHandler = snackbarHandler;
            this.driverEditableFieldConfigurationFactory = driverEditableFieldConfigurationFactory;

            this.Predictions = new ObservableCollection<Prediction>();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SelectPredictionCommand = new MvxCommand<Prediction>(this.SelectPrediction);

            this.DismissPredictionCommand = new MvxCommand(this.DismissSelection);

            this.SaveEditAddressCommand = new MvxCommand(() => this.SaveEditAddressTaskCompletion = NotifyTaskCompletion.Create(this.SaveEditAddress),
                                                       () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SaveEditAddressTaskCompletion));
        }

        // MVX Life cycle
        public void Init(DriverEditableField field)
        {
            this.driverField = field;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[DRIVER_FIELD_BUNDLE_KEY] = this.driverField.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.driverField = (DriverEditableField)Enum.Parse(typeof(DriverEditableField), state.Data[DRIVER_FIELD_BUNDLE_KEY]);
        }

        public override void Start()
        {
            base.Start();

            this.DriverEditableFieldConfiguration = this.driverEditableFieldConfigurationFactory.GetDriverEditableFieldConfiguration(
                this.universalObjectWrapperSingletonDriver.UniversalObject, this.driverField);
        }

        // MVVM Properties
        public DriverEditableFieldConfigurationModel DriverEditableFieldConfiguration { get; set; }

        public ObservableCollection<Prediction> Predictions { get; set; }

        public Prediction SelectedPrediction { get; set; }

        private string inputText;
        public string InputText
        {
            get
            {
                return inputText ?? DriverEditableFieldConfiguration?.DefaultValue;
            }
            set
            {
                this.inputText = value ?? string.Empty;

                if(string.IsNullOrEmpty(value) || value.Trim().Length < 4)
                {
                    this.Predictions.Clear();
                    return;
                }

                this.SearchPredictionsTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();
                this.SearchPredictionsTaskCompletion = NotifyTaskCompletion.Create(this.GetPredictionsAsync);
            }
        }

        public INotifyTaskCompletion SearchPredictionsTaskCompletion { get; private set; }

        public INotifyTaskCompletion SaveEditAddressTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand SelectPredictionCommand { get; private set; }

        public ICommand DismissPredictionCommand { get; private set; }

        public ICommand SaveEditAddressCommand { get; private set; }

        // public methods

        // private methods
        private async Task SaveEditAddress()
        {
            if(this.InputText.Equals(this.DriverEditableFieldConfiguration?.DefaultValue))
            {
                this.CloseWithTrack(this);
                return;
            }

            if(this.SelectedPrediction == null)
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PleaseEnterAValidAddress);
                return;
            }

            var address = await this.GetAddressDetailsAsync();

            this.universalObjectWrapperSingletonDriver.UniversalObject.User.PrimaryAddress = address;

            await this.driverService.UpdateDriverAddressAsync(
                address,
                success: updatedAddress =>
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject.User.PrimaryAddress = updatedAddress;

                this.SaveEditAddressTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();

                this.CloseWithTrack(this);
            });
        }

        private async Task GetPredictionsAsync()
        {
            await this.addressService.GetAddressPredictionsAsync(
                this.inputText,
                success: predictions => this.Predictions.Fill(predictions));
        }

        private async Task<Address> GetAddressDetailsAsync()
        {
            Address address = null;
            await this.addressService.GetAddressDetailsAsync(
                this.SelectedPrediction.PlaceId,
                success: details =>
            {
                address = new Address
                {
                    City = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.Locality))?.LongName,
                    State = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.AdministrativeAreaLevel1))?.ShortName,
                    Country = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.Country))?.LongName,
                    GooglePlaceId = details.PlaceId,
                    Lat = details.Geometry.Location.Lat,
                    Lng = details.Geometry.Location.Lng,
                    Raw = details.FormattedAddress,
                    Name = details.Name,
                    Primary = true,
                    PostalCode = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.PostalCode))?.LongName
                };
            });

            return address;
        }

        private void SelectPrediction(Prediction prediction)
        {
            this.SelectedPrediction = prediction;
            this.InputText = this.SelectedPrediction.Description;
        }

        private void DismissSelection()
        {
            this.SelectedPrediction = null;
            this.InputText = string.Empty;
        }
    }
}
