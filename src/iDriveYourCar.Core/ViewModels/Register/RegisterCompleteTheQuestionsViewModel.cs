﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
    public class RegisterCompleteTheQuestionsViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;

        public RegisterCompleteTheQuestionsViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ContinueToUploadPhotoCommand = new MvxCommand(() => this.ShowViewModelWithTrack<RegisterUploadPhotoViewModel>());
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }

        }

        // MVVM Commands
        public ICommand ContinueToUploadPhotoCommand { get; private set; }

        // public methods

        // private methods
    }
}
