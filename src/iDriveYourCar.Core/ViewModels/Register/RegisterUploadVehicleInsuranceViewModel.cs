﻿using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using iDriveYourCar.Core.Services;

namespace iDriveYourCar.Core.ViewModels
{
    public class RegisterUploadVehicleInsuranceViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;
        private readonly IImageHelper imageHelper;
        private readonly IDriverService driverService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;

        public RegisterUploadVehicleInsuranceViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver,
            IImageHelper imageHelper,
            IDriverService driverService,
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
            this.imageHelper = imageHelper;
            this.driverService = driverService;
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.UploadVehicleInsuranceCommand = new MvxCommand(() => this.UploadVehicleInsuranceTaskCompletion = NotifyTaskCompletion.Create(this.UploadVehicleInsuranceAsync),
                                                                () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.UploadVehicleInsuranceTaskCompletion));

            this.ContinueToFinishScreenCommand = new MvxCommand(() => this.RegisterDriverTaskCompletion = NotifyTaskCompletion.Create(this.RegisterDriverAsync),
                                                                () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.RegisterDriverTaskCompletion));
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }
        }

        public StreamFileRequest VehicleInsurance
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.VehicleInsurance;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.VehicleInsurance = value;
            }
        }

        public INotifyTaskCompletion UploadVehicleInsuranceTaskCompletion { get; private set; }

        public INotifyTaskCompletion RegisterDriverTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand UploadVehicleInsuranceCommand { get; private set; }

        public ICommand ContinueToFinishScreenCommand { get; private set; }

        // public methods

        // private methods
        private async Task UploadVehicleInsuranceAsync()
        {
            var file = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
                ImageSource.Camera,
                "vehicleInsuranceCacheKey",
                "vehicleInsurance",
                Plugin.Media.Abstractions.PhotoSize.Large,
                70);

            if(file != null)
                this.VehicleInsurance = new StreamFileRequest(file.GetStream(), Path.GetFileName(file.Path), "vehicleInsurance");
        }

        private async Task RegisterDriverAsync()
        {
            await this.driverService.CreateDriverAsync(
                this.RegisterDriver,
                success: createdDriver =>
                {
                    //this.universalObjectWrapperSingletonDriver.UniversalObject = createdDriver;
                    this.ShowViewModelWithTrack<RegisterFinishViewModel>();
                }
            );
        }
    }
}
