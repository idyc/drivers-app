﻿using System;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels.Register
{
    public class RegisterPasswordViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;
        private readonly ISnackbarHandler snackbarHandler;

        public RegisterPasswordViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver,
            ISnackbarHandler snackbarHandler)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
            this.snackbarHandler = snackbarHandler;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ContinueCommand = new MvxCommand(this.Continue);
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }
        }

        public string ConfirmPassword { get; set; }

        // MVVM Commands
        public ICommand ContinueCommand { get; private set; }

        // public methods

        // private methods
        private void Continue()
        {
            if(string.IsNullOrEmpty(this.RegisterDriver.Password) || string.IsNullOrEmpty(this.ConfirmPassword))
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PleaseCompleteAllFields);
                return;
            }

            if(!this.RegisterDriver.Password.Equals(this.ConfirmPassword))
            {
                this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PasswordsDoNotmatchPleaseCheck);
                return;
            }

            this.ShowViewModelWithTrack<RegisterCompleteTheQuestionsViewModel>();
        }
    }
}
