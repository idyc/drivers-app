﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.Validations;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using iDriveYourCar.Core.ViewModels.Register;

namespace iDriveYourCar.Core.ViewModels
{
    public class RegisterGeneralInformationViewModel : IDYCViewModel
    {
        private const string EMAIL_ERROR_TOKEN = "email";
        private const string EMAIL_REQUIRED_TOKEN = "email_required";
        private const string FIRST_NAME_REQUIRED_TOKEN = "first_name_required";
        private const string LAST_NAME_REQUIRED_TOKEN = "last_name_required";

        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;
        private readonly IInputFieldHandler inputFieldHandler;
        private readonly IAddressService addressService;
        private readonly ISnackbarHandler snackbarHandler;
        private readonly IValidatorHelper validatorHelper;

        public RegisterGeneralInformationViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver,
            IInputFieldHandler inputFieldHandler,
            IAddressService addressService,
            ISnackbarHandler snackbarHandler,
            IValidatorHelper validatorHelper)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
            this.inputFieldHandler = inputFieldHandler;
            this.addressService = addressService;
            this.snackbarHandler = snackbarHandler;
            this.validatorHelper = validatorHelper;

            this.Predictions = new ObservableCollection<Prediction>();

            this.ConfigureValidator();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SetDateOfBirthCommand = new MvxCommand(() => this.SetDateOfBirthTaskCompletion = NotifyTaskCompletion.Create(this.SetDateOfBirthAsync),
                                                        () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SetDateOfBirthTaskCompletion));

            this.SelectPredictionCommand = new MvxCommand<Prediction>(this.SelectPrediction);

            this.DismissPredictionCommand = new MvxCommand(this.DismissSelection);

            this.ContinueCommand = new MvxCommand(() => this.ContinueTaskCompletion = NotifyTaskCompletion.Create(this.ContinueAsync),
                                                () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.ContinueTaskCompletion));
        }

        // MVX Life cycle
        public void Init()
        {
            this.RegisterDriver = new RegisterDriver
            {
                User = new User(),
                Birthday = DateTime.Today
            };
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }
        }

        public INotifyTaskCompletion SetDateOfBirthTaskCompletion { get; private set; }

        public INotifyTaskCompletion ContinueTaskCompletion { get; private set; }

        public INotifyTaskCompletion SearchPredictionsTaskCompletion { get; private set; }

        public ObservableCollection<Prediction> Predictions { get; set; }

        public Prediction SelectedPrediction { get; set; }

        private string inputText;
        public string InputText
        {
            get
            {
                return inputText;
            }
            set
            {
                this.inputText = value ?? string.Empty;

                if(string.IsNullOrEmpty(value) || value.Trim().Length < 4)
                {
                    this.Predictions.Clear();
                    return;
                }

                this.SearchPredictionsTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();
                this.SearchPredictionsTaskCompletion = NotifyTaskCompletion.Create(this.GetPredictionsAsync);
            }
        }

        // MVVM Commands
        public ICommand SetDateOfBirthCommand { get; private set; }

        public ICommand SelectPredictionCommand { get; private set; }

        public ICommand DismissPredictionCommand { get; private set; }

        public ICommand ContinueCommand { get; private set; }

        // public methods

        // private methods
        private async Task SetDateOfBirthAsync()
        {
            var date = await this.inputFieldHandler.ShowDateInputFieldAsync(this.RegisterDriver.Birthday);

            if(!date.HasValue)
                return;

            this.RegisterDriver.Birthday = date.Value;
        }

        private async Task GetPredictionsAsync()
        {
            await this.addressService.GetAddressPredictionsAsync(
                this.inputText,
                success: predictions => this.Predictions.Fill(predictions));
        }

        private async Task<Address> GetAddressDetailsAsync()
        {
            Address address = null;
            await this.addressService.GetAddressDetailsAsync(
                this.SelectedPrediction.PlaceId,
                success: details =>
            {
                address = new Address
                {
                    City = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.Locality))?.LongName,
                    State = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.AdministrativeAreaLevel1))?.ShortName,
                    Country = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.Country))?.LongName,
                    GooglePlaceId = details.PlaceId,
                    Lat = details.Geometry.Location.Lat,
                    Lng = details.Geometry.Location.Lng,
                    Raw = details.FormattedAddress,
                    Name = details.Name,
                    Primary = true,
                    PostalCode = details.AddressComponents.FirstOrDefault(c => c.Types.Contains(DGenix.Mobile.Fwk.GooglePlacesAPI.Models.Type.PostalCode))?.LongName
                };
            });

            return address;
        }

        private void SelectPrediction(Prediction prediction)
        {
            this.SelectedPrediction = prediction;
            this.inputText = this.SelectedPrediction.Description;

            this.Predictions.Clear();

            this.RaisePropertyChanged(() => this.InputText);
        }

        private void DismissSelection()
        {
            this.SelectedPrediction = null;
            this.InputText = string.Empty;
        }

        private void ConfigureValidator()
        {
            this.validatorHelper
                .AddEmailValidationRule(() => this.RegisterDriver.User.Email, EMAIL_ERROR_TOKEN)
                .AddRequiredFieldValidationRule(() => this.RegisterDriver.User.Email, nameof(this.RegisterDriver.User.Email), EMAIL_REQUIRED_TOKEN)
                .AddRequiredFieldValidationRule(() => this.RegisterDriver.FirstName, nameof(this.RegisterDriver.FirstName), FIRST_NAME_REQUIRED_TOKEN)
                .AddRequiredFieldValidationRule(() => this.RegisterDriver.LastName, nameof(this.RegisterDriver.LastName), LAST_NAME_REQUIRED_TOKEN);
        }

        private async Task ContinueAsync()
        {
            var errors = this.validatorHelper.ValidateAll();

            if(errors.Any())
            {
                if(errors.ContainsKey(FIRST_NAME_REQUIRED_TOKEN))
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.FirstName));

                if(errors.ContainsKey(LAST_NAME_REQUIRED_TOKEN))
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.LastName));

                if(errors.ContainsKey(EMAIL_ERROR_TOKEN) || errors.ContainsKey(EMAIL_REQUIRED_TOKEN))
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.TheEmailEnteredIsNotValid);

                return;
            }

            if(this.SelectedPrediction != null)
            {
                var address = await this.GetAddressDetailsAsync();

                if(string.IsNullOrEmpty(address.PostalCode) || string.IsNullOrEmpty(address.GooglePlaceId) || string.IsNullOrEmpty(address.Country))
                {
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PleaseEnterAValidAddress);
                    this.InputText = null;
                    this.SelectedPrediction = null;

                    return;
                }

                this.RegisterDriver.User.PrimaryAddress = address;
            }

            this.RegisterDriver.User.Username = this.RegisterDriver.User.Email;

            this.ShowViewModelWithTrack<RegisterPasswordViewModel>();
        }
    }
}