﻿using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class RegisterFinishViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;
        private readonly IMvxJsonConverter jsonConverter;
        private readonly IUserInformationService userInformationService;
        private readonly ISecureStorageHelper secureStorageHelper;

        public RegisterFinishViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver,
            IMvxJsonConverter jsonConverter,
            IUserInformationService userInformationService,
            ISecureStorageHelper secureStorageHelper)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
            this.jsonConverter = jsonConverter;
            this.userInformationService = userInformationService;
            this.secureStorageHelper = secureStorageHelper;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.CompleteRegistrationCommand = new MvxCommand(() => this.CompleteRegistrationTaskCompletion = NotifyTaskCompletion.Create(this.CompleteRegistrationAsync),
                                                              () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.CompleteRegistrationTaskCompletion));
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }
        }

        public string CongratsName => string.Format(IDriveYourCarStrings.CongratsName, this.RegisterDriver.FirstName);

        public INotifyTaskCompletion CompleteRegistrationTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand CompleteRegistrationCommand { get; private set; }

        // public methods

        // private methods
        private async Task CompleteRegistrationAsync()
        {
            await Task.FromResult(0);

            this.ShowViewModelWithTrack<LoginViewModel>();

            //await this.userInformationService.VerifyUserAsync(
            //        this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.User.Email,
            //        this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.Password,
            //        success: user =>
            //        {
            //            this.secureStorageHelper.SetUserValues(
            //                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.User.Email,
            //                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.Password
            //            );

            //            this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = null;

            //            var hint = new StartupHint { InitialScreen = InitialScreen.Profile };
            //            var hintSerialized = this.jsonConverter.SerializeObject(hint);
            //            this.ShowViewModelWithTrack<MainViewModel>(new { hint = hintSerialized });
            //        });
        }
    }
}
