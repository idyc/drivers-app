﻿using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class RegisterUploadDriverLicenseViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;
        private readonly IImageHelper imageHelper;

        public RegisterUploadDriverLicenseViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver,
            IImageHelper imageHelper)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
            this.imageHelper = imageHelper;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.UploadDriverLicenseCommand = new MvxCommand(() => this.UploadDriverLicenseTaskCompletion = NotifyTaskCompletion.Create(this.UploadDriverLicenseAsync),
                                                             () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.UploadDriverLicenseTaskCompletion));

            this.ContinueToUploadVehicleInsuranceCommand = new MvxCommand(() => this.ShowViewModelWithTrack<RegisterUploadVehicleInsuranceViewModel>());
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }
        }

        public StreamFileRequest DriverLicense
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.DriverLicense;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.DriverLicense = value;
            }
        }

        public INotifyTaskCompletion UploadDriverLicenseTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand UploadDriverLicenseCommand { get; private set; }

        public ICommand ContinueToUploadVehicleInsuranceCommand { get; private set; }

        // public methods

        // private methods
        private async Task UploadDriverLicenseAsync()
        {
            var file = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
                ImageSource.Camera,
                "driverLicenseCacheKey",
                "driverLicense",
                Plugin.Media.Abstractions.PhotoSize.Large,
                70);

            if (file != null)
                this.DriverLicense = new StreamFileRequest(file.GetStream(), Path.GetFileName(file.Path), "driverLicense");
        }
    }
}
