﻿using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class RegisterUploadPhotoViewModel : IDYCViewModel
    {
        private readonly IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver;
        private readonly IImageHelper imageHelper;

        public RegisterUploadPhotoViewModel(
            IUniversalObjectNotifyWrapperSingleton<RegisterDriver> universalObjectWrapperSingletonRegisterDriver,
            IImageHelper imageHelper)
        {
            this.universalObjectWrapperSingletonRegisterDriver = universalObjectWrapperSingletonRegisterDriver;
            this.imageHelper = imageHelper;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.UploadPhotoCommand = new MvxCommand(() => this.UploadPhotoTaskCompletion = NotifyTaskCompletion.Create(this.UploadPhotoAsync),
                                                     () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.UploadPhotoTaskCompletion));

            this.ContinueToUploadDriverLicenseCommand = new MvxCommand(() => this.ShowViewModelWithTrack<RegisterUploadDriverLicenseViewModel>());
        }

        // MVX Life cycle
        public void Init()
        {
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            // TODO: Test! Do we have to save the state of images here??
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public RegisterDriver RegisterDriver
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject = value;
            }
        }

        public StreamFileRequest Avatar
        {
            get
            {
                return this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.Avatar;
            }
            set
            {
                this.universalObjectWrapperSingletonRegisterDriver.UniversalObject.Avatar = value;
            }
        }

        public INotifyTaskCompletion UploadPhotoTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand UploadPhotoCommand { get; private set; }

        public ICommand ContinueToUploadDriverLicenseCommand { get; private set; }

        // public methods

        // private methods
        private async Task UploadPhotoAsync()
        {
            var file = await this.imageHelper.LoadImageFromSourceAndStoreInCacheAsync(
                ImageSource.Gallery,
                "avatarCacheKey",
                "avatar",
                Plugin.Media.Abstractions.PhotoSize.Large,
                70);

            if(file != null)
                this.Avatar = new StreamFileRequest(file.GetStream(), Path.GetFileName(file.Path), "avatar");
        }
    }
}
