﻿using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels
{
    public class ForgotPasswordViewModel : IDYCViewModel
    {
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IAlertHandler alertHandler;

        public ForgotPasswordViewModel(IForgotPasswordService forgotPasswordService, IAlertHandler alertHandler)
        {
            this.forgotPasswordService = forgotPasswordService;
            this.alertHandler = alertHandler;

            this.EmailSent = false;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.RecoverPasswordCommand = new MvxCommand(() => this.ResetPasswordTaskCompletion = NotifyTaskCompletion.Create(this.RecoverPassword),
                                                     () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.ResetPasswordTaskCompletion));

            this.ReturnToLoginCommand = new MvxCommand(() => this.CloseWithTrack(this));
        }

        // MVX Life cycle
        public void Init()
        {
        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public string Email { get; set; }

        public INotifyTaskCompletion ResetPasswordTaskCompletion { get; private set; }

        public bool EmailSent { get; set; }

        // MVVM Commands
        public ICommand RecoverPasswordCommand { get; private set; }

        public ICommand ReturnToLoginCommand { get; private set; }

        // public methods

        // private methods
        private async Task RecoverPassword()
        {
            if(this.EmailSent)
                return;

            if(string.IsNullOrWhiteSpace(this.Email))
            {
                this.alertHandler.ShowMessage(IDriveYourCarStrings.PleaseCompleteAllFields);
                return;
            }

            await this.forgotPasswordService.RecoverPasswordDataAsync(
                this.Email,
                success: () =>
                {
                    this.EmailSent = true;
                });
        }
    }
}
