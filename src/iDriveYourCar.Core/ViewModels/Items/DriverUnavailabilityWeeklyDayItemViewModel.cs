﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class DriverUnavailabilityWeeklyDayItemViewModel : FwkBaseNavigatingObject
    {
        private readonly IMvxMessenger mvxMessenger;

        //private MvxSubscriptionToken timeAddedOrRemovedToken;

        public DriverUnavailabilityWeeklyDayItemViewModel(
            IMvxMessenger mvxMessenger)
        {
            this.mvxMessenger = mvxMessenger;

            this.DriverUnavailabilityTimes = new ObservableCollection<DriverUnavailabilityWeeklyTimeItemViewModel>();

            this.RemoveUnavailabilityTimeCommand = new MvxCommand<DriverUnavailabilityWeeklyTimeItemViewModel>(this.RemoveUnavailabilityTime);

            this.InitializeCommands();

            //this.timeAddedOrRemovedToken = this.mvxMessenger.SubscribeOnMainThread<GenericCUDMessage<DriverUnavailabilityWeeklyTimeItemViewModel>>(
            //    message =>
            //    {
            //        if(message.Target.DriverUnavailability.Day != this.DayOfWeek)
            //            return;

            //        switch(message.Operation)
            //        {
            //            case CUDOperation.Create:
            //                this.DriverUnavailabilityTimes.Add(message.Target);
            //                break;
            //            case CUDOperation.Delete:
            //                this.DriverUnavailabilityTimes.Remove(message.Target);
            //                break;
            //        }

            //        if(!this.DriverUnavailabilityTimes.Any())
            //            this.mvxMessenger.Publish(new GenericCUDMessage<DriverUnavailabilityWeeklyDayItemViewModel>(this, this, CUDOperation.Delete));
            //    });
        }

        protected void InitializeCommands()
        {

        }

        // MVVM Properties
        public ObservableCollection<DriverUnavailabilityWeeklyTimeItemViewModel> DriverUnavailabilityTimes { get; set; }

        public long DayOfWeek { get; set; }

        // MVVM Commands
        public ICommand RemoveUnavailabilityTimeCommand { get; private set; }

        // Private methods
        private void RemoveUnavailabilityTime(DriverUnavailabilityWeeklyTimeItemViewModel itemViewModel)
        {
            this.mvxMessenger.Publish(new GenericCUDMessage<DriverUnavailabilityWeeklyTimeItemViewModel>(this, itemViewModel, CUDOperation.Delete));
        }
    }
}
