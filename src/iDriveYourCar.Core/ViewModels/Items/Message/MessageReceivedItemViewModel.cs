﻿using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items.Message
{
    public class MessageReceivedItemViewModel : BaseChatItemViewModel
    {
        public MessageReceivedItemViewModel(ChatMessage chatMessage) : base(chatMessage)
        {

        }

    }
}
