﻿using System;
using iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.ViewModels.Items
{
    public class MessageSentItemViewModel : BaseChatItemViewModel
    {
        public MessageSentItemViewModel(ChatMessage chatMessage)
            : base(chatMessage)
        {
        }
    }
}
