﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public abstract class BaseChatItemViewModel : IDYCNavigatingObject
    {
        public BaseChatItemViewModel(ChatMessage chatMessage)
        {
            this.ChatMessage = chatMessage;
        }

        // MVVM Properties
        public ChatMessage ChatMessage { get; set; }

        // MVVM Commands

        // public methods

        // private methods  
    }
}
