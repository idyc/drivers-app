using System;
using System.Linq;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class BaseTripItemViewModel : IDYCNavigatingObject
    {
        public BaseTripItemViewModel(Trip trip)
        {
            this.Trip = trip;
        }

        // MVVM Properties
        public Trip Trip { get; set; }

        public bool TripHasModifications => this.Trip.FieldsChanged != null && this.Trip.FieldsChanged.Any();

        public string PickupTime => DateTime.Parse(this.Trip?.PickupTime).ToFormattedString(DateStringFormat.TimeAMPM);

        public string Pickup => this.GetPickup(this.Trip);

        public string Dropoff => this.GetDropoff(this.Trip);

        // MVVM Commands

        // public methods

        // private methods
        private string GetPickup(Trip trip)
        {
            var pickup = string.Empty;

            if (trip.TripType == TripType.AirportPickup)
            {
                pickup = trip.FromAirport?.Code;
            }
            else //if trip.TripType == AirportDropoff, OneWayPickup, OneWayDropoff, WaitAndReturn or DriveMeHome
            {
                pickup = trip.PickupAddress.CityAndStateFormatted;
            }

            return pickup;
        }

        private string GetDropoff(Trip trip)
        {
            var dropoff = string.Empty;

            if (trip.TripType == TripType.AirportDropoff)
            {
                dropoff = trip.ToAirport?.Code;
            }
            else //if trip.TripType == AirportPickup, OneWayPickup, OneWayDropoff, WaitAndReturn or DriveMeHome
            {
                dropoff = trip.DropoffAddress.CityAndStateFormatted;
            }

            return dropoff;
        }
    }
}