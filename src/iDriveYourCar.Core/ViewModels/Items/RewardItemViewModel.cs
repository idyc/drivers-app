﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class RewardItemViewModel : FwkBaseNotifyPropertyChanged
	{
		public RewardItemViewModel(Reward reward)
		{
			this.Reward = reward;
		}

		public Reward Reward { get; set; }
	}
}
