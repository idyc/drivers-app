﻿using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class OpenTripItemViewModel : BaseTripItemViewModel, IBaseTripItemViewModel
    {
        public OpenTripItemViewModel(Trip trip)
            : base(trip)
        {
            this.Trip = trip;

            this.InitializeCommands();
        }

        protected void InitializeCommands()
        {

        }

        // MVVM Properties

        // MVVM Commands


        // public methods

        // private methods

    }
}
