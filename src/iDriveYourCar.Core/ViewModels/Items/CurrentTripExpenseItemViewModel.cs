﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class CurrentTripExpenseItemViewModel : FwkBaseNavigatingObject
    {
        private readonly IMvxMessenger mvxMessenger;

        public CurrentTripExpenseItemViewModel(
            IMvxMessenger mvxMessenger,
            Expense expense)
        {
            this.mvxMessenger = mvxMessenger;
            this.Expense = expense;

            this.InitializeCommands();
        }

        protected void InitializeCommands()
        {
            this.RemoveExpenseCommand = new MvxCommand(this.RemoveExpense);
        }

        // MVVM Properties
        public Expense Expense { get; set; }

        // MVVM Commands
        public ICommand RemoveExpenseCommand { get; private set; }

        // public methods

        // private methods
        private void RemoveExpense()
        {
            this.mvxMessenger.Publish(new GenericCUDMessage<CurrentTripExpenseItemViewModel>(this, this, CUDOperation.Delete));
        }
    }
}
