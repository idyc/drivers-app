﻿using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class DriverInvitationSuccessfulItemViewModel : DriverInvitationBaseItemViewModel
    {
        private readonly IReferralsService referralService;
        private readonly ISnackbarHandler snackbarHandler;

        public DriverInvitationSuccessfulItemViewModel(
            DriverInvitation driverInvitation,
            IEnumHelper enumHelper,
            IReferralsService referralService,
            ISnackbarHandler snackbarHandler,
            bool grayBackground)
            : base(driverInvitation, enumHelper, grayBackground)
        {
            this.referralService = referralService;
            this.snackbarHandler = snackbarHandler;

            this.InitializeCommands();
        }

        protected void InitializeCommands()
        {
            this.RedeemCommand = new MvxCommand(() => this.RedeemTaskCompletion = NotifyTaskCompletion.Create(this.RedeemAsync),
                                               () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.RedeemTaskCompletion));
        }

        // MVVM Properties
        public INotifyTaskCompletion RedeemTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand RedeemCommand { get; set; }

        // public methods

        // private methods
        private async Task RedeemAsync()
        {
            await this.referralService.RedeemAsync(
                this.DriverInvitation.Id,
                success:
                () =>
                {
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.RedeemRequestSentSuccessfully);
                    this.DriverInvitation.RedeemRequested = true;
                });
        }
    }
}
