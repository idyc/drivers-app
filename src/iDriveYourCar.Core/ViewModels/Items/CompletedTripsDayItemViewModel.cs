﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class CompletedTripsDayItemViewModel : FwkBaseNavigatingObject
	{
		private const int NUMBER_OF_SECTIONS = 7;

		public CompletedTripsDayItemViewModel()
		{
			this.CompletedTripsTimes = new ObservableCollection<CompletedTripItemViewModel>();

			this.SectionVisibility = new SectionVisibilityViewModel();

			this.InitializeCommands();
		}

		protected void InitializeCommands()
		{
		}

		// MVVM Properties
		public int DayOfWeek { get; set; }

		public DateTime Date { get; set; }

		public decimal DayEarnings { get; set; }

		public ObservableCollection<CompletedTripItemViewModel> CompletedTripsTimes { get; set; }

		public SectionVisibilityViewModel SectionVisibility { get; set; }

		// MVVM Commands

		// public methods
		public void AddCompletedTripTime(Trip trip)
		{
			this.CompletedTripsTimes.Add(new CompletedTripItemViewModel(trip));
			this.DayEarnings = this.GetDayEarnings();
		}

		// private methods
		private decimal GetDayEarnings()
		{
			decimal dayEarnings = 0;
			foreach(var completedTripTime in this.CompletedTripsTimes)
			{
				dayEarnings += completedTripTime.CompletedTrip.Price;
			}
			return dayEarnings;
		}
	}
}