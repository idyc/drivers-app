﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using Plugin.Messaging;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class DriverInvitationPendingItemViewModel : DriverInvitationBaseItemViewModel
    {
        private readonly ISnackbarHandler snackbarHandler;

        public DriverInvitationPendingItemViewModel(
            DriverInvitation driverInvitation,
            IEnumHelper enumHelper,
            ISnackbarHandler snackbarHandler,
            bool grayBackground)
            : base(driverInvitation, enumHelper, grayBackground)
        {
            this.snackbarHandler = snackbarHandler;

            this.CanRemind = true;

            this.InitializeCommands();
        }

        protected void InitializeCommands()
        {
            this.RemindCommand = new MvxCommand(this.RemindAsync);
        }

        // MVVM Properties
        public bool CanRemind { get; set; }

        // MVVM Commands
        public ICommand RemindCommand { get; private set; }

        // public methods

        // private methods
        private void RemindAsync()
        {
            this.CanRemind = false;

            if(!CrossMessaging.Current.EmailMessenger.CanSendEmail)
                return;

            CrossMessaging.Current.EmailMessenger.SendEmail(
                to: this.DriverInvitation.Email,
                subject: IDriveYourCarStrings.HaveYouFinishedSigningUp,
                message: IDriveYourCarStrings.RemindEmailContent);
        }
    }
}
