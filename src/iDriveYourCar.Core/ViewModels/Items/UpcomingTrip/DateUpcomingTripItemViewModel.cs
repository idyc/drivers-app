﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class DateUpcomingTripItemViewModel : IDYCNavigatingObject, IDateTripItemViewModel
	{
		public DateUpcomingTripItemViewModel(DateTime date)
		{
			this.Date = date;

			this.InitializeCommands();
		}

		public DateTime Date { get; set; }

		protected void InitializeCommands()
		{

		}

		// MVVM Properties

		// MVVM Commands

		// public methods

		// private methods

	}
}
