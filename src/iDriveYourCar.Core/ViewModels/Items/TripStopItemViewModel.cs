﻿using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class TripStopItemViewModel : FwkBaseNotifyPropertyChanged
    {
        public TripStopItemViewModel(TripStop tripStop, Trip trip)
        {
            this.TripStop = tripStop;
            this.Trip = trip;
        }

        public TripStop TripStop { get; set; }
        public Trip Trip { get; set; }
    }
}
