﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class ReviewItemViewModel : FwkBaseNavigatingObject
	{
		public ReviewItemViewModel(Review review)
		{
			this.Review = review;

			var tagsFromReview = new List<string>();
			if(this.Review.PoorCommunication)
				tagsFromReview.Add(IDriveYourCarStrings.PoorCommunication);
			if(this.Review.PoorDriving)
				tagsFromReview.Add(IDriveYourCarStrings.PoorDriving);
			if(this.Review.Unpunctual)
				tagsFromReview.Add(IDriveYourCarStrings.Unpunctual);

			this.Tags = new ObservableCollection<string>(tagsFromReview);

			this.InitializeCommands();
		}

		protected void InitializeCommands()
		{

		}

		// MVVM Properties
		public Review Review { get; set; }

		public ObservableCollection<string> Tags { get; set; }

		// MVVM Commands

		// public methods

		// private methods

	}
}
