﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class DriverUnavailabilityWeeklyTimeItemViewModel : FwkBaseNavigatingObject
	{
		private readonly IMvxMessenger mvxMessenger;

		public DriverUnavailabilityWeeklyTimeItemViewModel(
			IMvxMessenger mvxMessenger,
			DriverUnavailabilityWeek driverUnavailability)
		{
			this.DriverUnavailability = driverUnavailability;
			this.mvxMessenger = mvxMessenger;

			this.InitializeCommands();
		}

		protected void InitializeCommands()
		{
			this.RemoveUnavailabilityTimeCommand = new MvxCommand(this.RemoveUnavailabilityTime);
		}

		// MVVM Properties
		public DriverUnavailabilityWeek DriverUnavailability { get; set; }

		// MVVM Commands
		public ICommand RemoveUnavailabilityTimeCommand { get; private set; }

		// public methods

		// private methods
		private void RemoveUnavailabilityTime()
		{
			this.mvxMessenger.Publish(new GenericCUDMessage<DriverUnavailabilityWeeklyTimeItemViewModel>(this, this, CUDOperation.Delete));
		}
	}
}
