﻿using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class DriverUnavailabilityExtendedItemViewModel : FwkBaseViewModel
    {
        private const string DATE_FORMAT = "ddd MMM d, yyyy";

        private readonly IMvxMessenger mvxMessenger;

        public DriverUnavailabilityExtendedItemViewModel(
            IMvxMessenger mvxMessenger,
            DriverUnavailability driverUnavailability)
        {
            this.mvxMessenger = mvxMessenger;
            this.DriverUnavailability = driverUnavailability;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.RemoveUnavailabilityExtendedCommand = new MvxCommand(this.RemoveUnavailability);
        }

        // MVX Life cycle
        public void Init()
        {

        }

        public override void Start()
        {
            base.Start();
        }

        // MVVM Properties
        public DriverUnavailability DriverUnavailability { get; set; }

        public string DateRange => $"{this.DriverUnavailability?.StartDate.ToString(DATE_FORMAT)} - {this.DriverUnavailability?.EndDate.ToString(DATE_FORMAT)}";

        // MVVM Commands
        public ICommand RemoveUnavailabilityExtendedCommand { get; private set; }

        // public methods

        // private methods
        private void RemoveUnavailability()
        {
            this.mvxMessenger.Publish(new GenericCUDMessage<DriverUnavailabilityExtendedItemViewModel>(this, this, CUDOperation.Delete));
        }
    }
}
