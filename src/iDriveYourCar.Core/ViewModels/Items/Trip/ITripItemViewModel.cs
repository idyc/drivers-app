﻿using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.Items
{
	public interface ITripItemViewModel : IBaseTripItemViewModel
	{
		Trip Trip { get; set; }
	}
}
