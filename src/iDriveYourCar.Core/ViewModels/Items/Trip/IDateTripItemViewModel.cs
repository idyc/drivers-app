﻿using System;
namespace iDriveYourCar.Core.ViewModels.Items
{
	public interface IDateTripItemViewModel : IBaseTripItemViewModel
	{
		DateTime Date { get; set; }
	}
}
