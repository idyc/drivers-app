﻿using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class DriverInvitationBaseItemViewModel : FwkBaseNavigatingObject
    {
        private readonly IEnumHelper enumHelper;

        public DriverInvitationBaseItemViewModel(
            DriverInvitation driverInvitation,
            IEnumHelper enumHelper,
            bool grayBackground)
        {
            this.DriverInvitation = driverInvitation;
            this.enumHelper = enumHelper;

            this.GrayBackground = grayBackground;
            this.ApplicantStatusDescription = $"${this.DriverInvitation.PotentialEarning}";

            if(this.DriverInvitation.Invited != null)
                this.ApplicantStatusDescription += $" | {this.enumHelper.GetApplicantStatusDescription(this.DriverInvitation.Invited.ApplicantStatus)}";
        }

        // MVVM Properties
        public DriverInvitation DriverInvitation { get; set; }

        public bool GrayBackground { get; private set; }

        public string ApplicantStatusDescription { get; private set; }

        // MVVM Commands

        // public methods

        // private methods
    }
}