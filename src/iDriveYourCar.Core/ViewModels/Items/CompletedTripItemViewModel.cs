﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class CompletedTripItemViewModel : FwkBaseNavigatingObject
	{
		public CompletedTripItemViewModel(Trip completedTrip)
		{
			this.CompletedTrip = completedTrip;

			this.InitializeCommands();
		}

		protected void InitializeCommands()
		{
		}

		// MVVM Properties
		public Trip CompletedTrip { get; set; }

		public string PickupTime => DateTime.Parse(this.CompletedTrip?.PickupTime).ToFormattedString(DateStringFormat.TimeAMPM);

		// MVVM Commands

		// public methods

		// private methods
	}
}
