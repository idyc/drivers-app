﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.ViewModels.Items
{
    public class NotificationItemViewModel : IDYCViewModel
    {

        public NotificationItemViewModel(Notification notification)
        {
            this.Notification = notification;

            this.InitializeCommands();
        }

        protected void InitializeCommands()
        {
            base.InitializeCommands();
        }

        // MVVM Properties
        public Notification Notification { get; set; }

        //public string Time => DateTime.Parse(this.Notification?.Date).ToFormattedString(DateStringFormat.TimeAMPM);

        // MVVM Commands


        // public methods

        // private methods

    }
}
