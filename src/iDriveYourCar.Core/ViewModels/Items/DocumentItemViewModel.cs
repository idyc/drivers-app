﻿using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Helpers;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.MvxMessages;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Nito.AsyncEx;

namespace iDriveYourCar.Core.ViewModels.Items
{
	public class DocumentItemViewModel : IDYCNavigatingObject
	{
		private readonly IEnumHelper enumHelper;
		private readonly IDocumentPreviewHandler documentPreviewHandler;
		private readonly IMvxMessenger mvxMessenger;
		private readonly IFileService fileService;

		public DocumentItemViewModel(
			Document document,
			IEnumHelper enumHelper,
			IDocumentPreviewHandler documentPreviewHandler,
			IMvxMessenger mvxMessenger,
			IFileService fileService)
		{
			this.Document = document;
			this.enumHelper = enumHelper;
			this.documentPreviewHandler = documentPreviewHandler;
			this.mvxMessenger = mvxMessenger;
			this.fileService = fileService;

			this.InitializeCommands();
		}

		protected void InitializeCommands()
		{
			this.DisplayDocumentCommand = new MvxCommand(() => this.DisplayDocumentTaskCompletion = NotifyTaskCompletion.Create(this.DisplayDocument),
														 () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.DisplayDocumentTaskCompletion));

			this.UploadDocumentCommand = new MvxCommand(this.RequestUploadDocument);
		}

		// MVVM Properties
		public Document Document { get; set; }

		public string DocumentTypeString => this.enumHelper.GetDocumentTypeDescription(this.Document.DocumentType);

		public string DocumentImagePath => this.TryToGetImagePath(() => this.Document.Path);

		public INotifyTaskCompletion DisplayDocumentTaskCompletion { get; private set; }

		// MVVM Commands
		public ICommand DisplayDocumentCommand { get; private set; }

		public ICommand UploadDocumentCommand { get; private set; }

		// public methods

		// private methods
		private void RequestUploadDocument()
		{
			this.mvxMessenger.Publish(new RequestUploadDocumentMessage(this, this.Document));
		}

		private async Task DisplayDocument()
		{
			await this.fileService.GetDocumentUrl(
				this.Document,
				success: stream => this.documentPreviewHandler.DisplayDocument(this.Document.Filename, this.Document.Extension, stream));
		}
	}
}
