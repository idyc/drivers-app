﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.Validations;
using DGenix.Mobile.Fwk.Core.ViewModels.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using MvvmCross.Core.ViewModels;
using Nito.AsyncEx;
using System;

namespace iDriveYourCar.Core.ViewModels
{
    public class BankingDetailsViewModel : IDYCViewModel
    {
        private const string NAME_ON_ACCOUNT_REQUIRED_TOKEN = "name_bank_account_required";
        private const string SSN_TIN_REQUIRED_TOKEN = "ssn_tin_required";
        private const string ROUTING_NUMBER_REQUIRED_TOKEN = "routing_number_required";
        private const string ACCOUNT_NUMBER_REQUIRED_TOKEN = "account_number_required";
        private const string TAX_ID_REQUIRED_TOKEN = "tax_id_required";

        private const string SSN_TIN_LENGTH_TOKEN = "ssn_tin_length";
        private const string ROUTING_NUMBER_LENGTH_TOKEN = "routing_number_length";
        private const string ACCOUNT_NUMBER_LENGTH_TOKEN = "account_number_length";

        private const string NameBankAccountBundleKey = "nameBankAccount";
        private const string SSNTINBundleKey = "ssntin";
        private const string RoutingNumberBundleKey = "routingNumber";
        private const string AccountNumberBundleKey = "accountNumber";
        private const string TaxIdBundleKey = "taxId";

        private readonly IDriverService driverService;
        private readonly IValidatorHelper validatorHelper;
        private readonly ISnackbarHandler snackbarHandler;
        private readonly IIDYCSettings idycSettings;

        public BankingDetailsViewModel(
            IDriverService driverService,
            IValidatorHelper validatorHelper,
            ISnackbarHandler snackbarHandler,
            IIDYCSettings idycSettings)
        {
            this.driverService = driverService;
            this.validatorHelper = validatorHelper;
            this.snackbarHandler = snackbarHandler;
            this.idycSettings = idycSettings;

            this.ConfigureValidator();
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.SaveCommand = new MvxCommand(() => this.SaveTaskCompletion = NotifyTaskCompletion.Create(this.SaveBankingDetailsAsync),
                                              () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SaveTaskCompletion));
        }

        // MVX Life cycle
        public void Init()
        {
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[NameBankAccountBundleKey] = this.NameBankAccount;
            bundle.Data[SSNTINBundleKey] = this.SSNTIN;
            bundle.Data[RoutingNumberBundleKey] = this.RoutingNumber;
            bundle.Data[AccountNumberBundleKey] = this.AccountNumber;
            bundle.Data[TaxIdBundleKey] = this.TaxId.ToString();
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.NameBankAccount = state.Data[NameBankAccountBundleKey];
            this.SSNTIN = state.Data[SSNTINBundleKey];
            this.RoutingNumber = state.Data[RoutingNumberBundleKey];
            this.AccountNumber = state.Data[AccountNumberBundleKey];
            this.TaxId = (TaxClassification)Enum.Parse(typeof(TaxClassification), state.Data[TaxIdBundleKey]);
        }

        public override void Start()
        {
            base.Start();

            this.StartTaxAdapter();
        }

        // MVVM Properties
        public string NameBankAccount { get; set; }

        public string SSNTIN { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public TaxClassification TaxId { get; set; }

        public EnumAdapterViewModel<TaxClassification, TaxClassificationEnumWrapper> TaxAdapter { get; set; }

        // MVVM Commands
        public ICommand SaveCommand { get; private set; }

        public INotifyTaskCompletion SaveTaskCompletion { get; private set; }

        // public methods

        // private methods
        private async Task SaveBankingDetailsAsync()
        {
            var errors = this.validatorHelper.ValidateAll();

            if(errors.Any())
            {
                if(errors.ContainsKey(NAME_ON_ACCOUNT_REQUIRED_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.NameOnAccount));
                    return;
                }
                if(errors.ContainsKey(SSN_TIN_REQUIRED_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.SSNTIN));
                    return;
                }
                if(errors.ContainsKey(TAX_ID_REQUIRED_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.TaxClassification));
                    return;
                }
                if(errors.ContainsKey(SSN_TIN_LENGTH_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.SSNTINMustHaveNineCharactersLength);
                    return;
                }
                if(errors.ContainsKey(ROUTING_NUMBER_REQUIRED_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.RoutingNumber));
                    return;
                }
                if(errors.ContainsKey(ROUTING_NUMBER_LENGTH_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.RoutingNumberMustHaveNineCharactersLength);
                    return;
                }
                if(errors.ContainsKey(ACCOUNT_NUMBER_REQUIRED_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(string.Format(IDriveYourCarStrings.PleaseEnterYourX, IDriveYourCarStrings.AccountNumber));
                    return;
                }
                if(errors.ContainsKey(ACCOUNT_NUMBER_LENGTH_TOKEN))
                {
                    this.snackbarHandler.ShowMessage(IDriveYourCarStrings.AccountNumberMustHaveBetweenSixAndSixteenCharactersLength);
                    return;
                }
            }

            var driver = new Driver
            {
                NameBankAccount = this.NameBankAccount,
                SSNTIN = this.SSNTIN,
                RoutingNumber = this.RoutingNumber,
                AccountNumber = this.AccountNumber,
                TaxId = this.TaxId
            };

            await this.driverService.UpdateBankingInformationAsync(
               driver,
               success: this.HandleInfoSavedSuccess);
        }

        private void HandleInfoSavedSuccess(Driver driver)
        {
            this.idycSettings.HasBankInfo = true;

            this.SaveTaskCompletion = NotifyTaskCompletionExtensions.CreateCleanedNotifyTaskCompletion();

            this.ShowViewModelWithTrack<MainViewModel>();
        }

        private void StartTaxAdapter()
        {
            this.TaxAdapter = new EnumAdapterViewModel<TaxClassification, TaxClassificationEnumWrapper>(
                this.OnTaxClassificationSelected,
                defaultSelected: this.TaxId != TaxClassification.None ? new TaxClassificationEnumWrapper(this.TaxId) : null
            );
            this.TaxAdapter.Start();
        }

        private void OnTaxClassificationSelected(TaxClassificationEnumWrapper obj)
        {
            this.TaxId = obj.Value.Value;
        }

        private void ConfigureValidator()
        {
            this.validatorHelper
                .AddRequiredFieldValidationRule(() => this.NameBankAccount, nameof(this.NameBankAccount), NAME_ON_ACCOUNT_REQUIRED_TOKEN)
                .AddRequiredFieldValidationRule(() => this.SSNTIN, nameof(this.SSNTIN), SSN_TIN_REQUIRED_TOKEN)
                .AddRequiredFieldValidationRule(() => this.RoutingNumber, nameof(this.RoutingNumber), ROUTING_NUMBER_REQUIRED_TOKEN)
                .AddRequiredFieldValidationRule(() => this.AccountNumber, nameof(this.AccountNumber), ACCOUNT_NUMBER_REQUIRED_TOKEN)
                .AddRequiredFieldCustomValidationRule(() => this.TaxId != TaxClassification.None, nameof(this.TaxId), TAX_ID_REQUIRED_TOKEN)
                .AddTextLengthValidationRule(() => this.SSNTIN, 9, RangeBound.Included, 9, RangeBound.Included, nameof(this.SSNTIN), SSN_TIN_LENGTH_TOKEN)
                .AddTextLengthValidationRule(() => this.RoutingNumber, 9, RangeBound.Included, 9, RangeBound.Included, nameof(this.RoutingNumber), ROUTING_NUMBER_LENGTH_TOKEN)
                .AddTextLengthValidationRule(() => this.AccountNumber, 6, RangeBound.Included, 16, RangeBound.Included, nameof(this.AccountNumber), ACCOUNT_NUMBER_LENGTH_TOKEN);
        }
    }
}

