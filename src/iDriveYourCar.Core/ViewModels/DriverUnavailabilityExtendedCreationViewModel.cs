﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Handlers;
using iDriveYourCar.Core.ViewModels.Items;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.MvxMessages;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using Nito.AsyncEx;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using System.Threading.Tasks;

namespace iDriveYourCar.Core.ViewModels
{
	public class DriverUnavailabilityExtendedCreationViewModel : IDYCViewModel
	{
		private readonly IMvxMessenger mvxMessenger;
		private readonly IInputFieldHandler inputFieldHandler;
		private readonly ISnackbarHandler snackbarHandler;

		public DriverUnavailabilityExtendedCreationViewModel(
			IMvxMessenger mvxMessenger,
			IInputFieldHandler inputFieldHandler,
			ISnackbarHandler snackbarHandler)
		{
			this.mvxMessenger = mvxMessenger;
			this.inputFieldHandler = inputFieldHandler;
			this.snackbarHandler = snackbarHandler;
		}

		protected override void InitializeCommands()
		{
			base.InitializeCommands();

			this.SetDateStartCommand = new MvxCommand(() => this.SetDateStartTaskCompletion = NotifyTaskCompletion.Create(this.SetDateStartAsync),
													  () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SetDateStartTaskCompletion));

			this.SetDateEndCommand = new MvxCommand(() => this.SetDateEndTaskCompletion = NotifyTaskCompletion.Create(this.SetDateEndAsync),
													() => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.SetDateEndTaskCompletion));

			this.SaveUnavailabilityCommand = new MvxCommand(this.SaveUnavailability);
		}

		// MVX Life cycle
		public void Init()
		{
		}

		public override void Start()
		{
			base.Start();
		}

		// MVVM Properties
		public DateTime DateStart { get; set; } = DateTime.Today;

		public DateTime DateEnd { get; set; } = DateTime.Today;

		public INotifyTaskCompletion SetDateStartTaskCompletion { get; private set; }

		public INotifyTaskCompletion SetDateEndTaskCompletion { get; private set; }

		// MVVM Commands
		public ICommand SetDateStartCommand { get; private set; }

		public ICommand SetDateEndCommand { get; private set; }

		public ICommand SaveUnavailabilityCommand { get; private set; }

		// public methods

		// private methods
		private void SaveUnavailability()
		{
			if(!this.Validate())
				return;

			var itemViewModel = new DriverUnavailabilityExtendedItemViewModel(
			  this.mvxMessenger,
			  new DriverUnavailability
			  {
				  StartDate = this.DateStart,
				  EndDate = this.DateEnd
			  });

			this.mvxMessenger.Publish(new GenericCUDMessage<DriverUnavailabilityExtendedItemViewModel>(this, itemViewModel, CUDOperation.Create));

			this.CloseWithTrack(this);
		}

		private bool Validate()
		{
			if(this.DateEnd.Date < this.DateStart.Date || this.CompareDatesEqualityByComponents())
			{
				this.snackbarHandler.ShowMessage(IDriveYourCarStrings.PleaseEnterAValidDateBlock);
				return false;
			}
			return true;
		}

		private bool CompareDatesEqualityByComponents()
		{
			return this.DateEnd.Year == this.DateStart.Year
					   && this.DateEnd.Month == this.DateStart.Month
					   && this.DateEnd.Day == this.DateStart.Day;
		}

		private async Task SetDateStartAsync()
		{
			var date = await this.inputFieldHandler.ShowDateInputFieldAsync(this.DateStart);

			if(!date.HasValue)
				return;

			this.DateStart = date.Value;
		}

		private async Task SetDateEndAsync()
		{
			var date = await this.inputFieldHandler.ShowDateInputFieldAsync(this.DateEnd);

			if(!date.HasValue)
				return;

			this.DateEnd = date.Value;
		}
	}
}
