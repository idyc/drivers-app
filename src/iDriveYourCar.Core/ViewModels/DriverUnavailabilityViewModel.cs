﻿using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace iDriveYourCar.Core.ViewModels
{
	public class DriverUnavailabilityViewModel : IDYCWithMenuActionsHostViewModel
	{
		public DriverUnavailabilityViewModel(
			IMenuNavigationActionsWrapper menuNavigationActionsWrapper)
			: base(menuNavigationActionsWrapper)
		{

		}

		protected override void InitializeCommands()
		{
			base.InitializeCommands();
		}

		// MVX Life cycle
		public void Init()
		{
		}

		public override void Start()
		{
			base.Start();

			this.WeeklyUnavailabilities = this.LoadViewModel<DriverUnavailabilityWeeklyViewModel>();
			this.ExtendedUnavailabilities = this.LoadViewModel<DriverUnavailabilityExtendedViewModel>();
		}

		// MVVM Properties
		public DriverUnavailabilityWeeklyViewModel WeeklyUnavailabilities { get; set; }

		public DriverUnavailabilityExtendedViewModel ExtendedUnavailabilities { get; set; }

		// MVVM Commands

		// public methods

		// private methods
	}
}
