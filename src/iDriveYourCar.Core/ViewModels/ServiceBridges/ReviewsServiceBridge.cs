﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;

using DGenix.Mobile.Fwk.Core.Models;
using System.Threading.Tasks;

namespace iDriveYourCar.Core.ViewModels.ServiceBridges
{
    public class ReviewsServiceBridge : IFwkBaseListViewModelServiceBridge<IDriverService, Review, ReviewCriteria>
    {
        public ReviewsServiceBridge(IDriverService driverService)
        {
            this.Service = driverService;
        }

        public IDriverService Service { get; }

        public async Task FetchItemsAsync(ReviewCriteria criteria, Action<IPagedListResult<Review>> onFetchItems)
        {
            await this.Service.GetDriverReviewsAsync(
                criteria,
                success: reviews => onFetchItems.Invoke(reviews));
        }

        public async Task LoadItemsFirstPageAsync(ReviewCriteria criteria, Action<IPagedListResult<Review>> onLoadItemsFirstPage)
        {
            await this.Service.GetDriverReviewsAsync(
                criteria,
                cacheSuccess: reviews => onLoadItemsFirstPage.Invoke(reviews),
                success: reviews => onLoadItemsFirstPage.Invoke(reviews));
        }
    }
}
