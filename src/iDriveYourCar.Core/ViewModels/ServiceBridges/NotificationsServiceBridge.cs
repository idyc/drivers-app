﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.Criteria;
using DGenix.Mobile.Fwk.Core.Models;
using System.Threading.Tasks;

namespace iDriveYourCar.Core.ViewModels.ServiceBridges
{
    public class NotificationsServiceBridge : IFwkBaseListViewModelServiceBridge<INotificationService, Notification, NotificationCriteria>
    {
        public NotificationsServiceBridge(INotificationService notificationService)
        {
            this.Service = notificationService;
        }

        public INotificationService Service { get; }

        public async Task FetchItemsAsync(NotificationCriteria criteria, Action<IPagedListResult<Notification>> onFetchItems)
        {
            await this.Service.GetNotificationsAsync(
                criteria,
                success: notifications => onFetchItems.Invoke(notifications));
        }

        public async Task LoadItemsFirstPageAsync(NotificationCriteria criteria, Action<IPagedListResult<Notification>> onLoadItemsFirstPage)
        {
            await this.Service.GetNotificationsAsync(
                criteria,
                cacheSuccess: notifications => onLoadItemsFirstPage.Invoke(notifications),
                success: notifications => onLoadItemsFirstPage.Invoke(notifications));
        }
    }
}
