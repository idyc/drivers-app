using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Services;
using iDriveYourCar.Core.ViewModels.Items;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using Nito.AsyncEx;
using MvvmCross.Plugins.Messenger;
using DGenix.Mobile.Fwk.Core.MvxMessages;

namespace iDriveYourCar.Core.ViewModels
{
    public class FindOpenTripsDetailViewModel : BaseTripDetailViewModel
    {
        private const string TRIP_SERIALIZED_BUNDLE_KEY = "tripSerialized";

        private readonly IMvxJsonConverter jsonConverter;
        private readonly ITripsService tripService;
        private readonly IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver;
        private readonly IMvxMessenger mvxMessenger;

        private string tripSerialized;

        public FindOpenTripsDetailViewModel(
            IUniversalObjectNotifyWrapperSingleton<Driver> universalObjectWrapperSingletonDriver,
            IMvxJsonConverter jsonConverter,
            IExternalMapHandler externalMapHandler,
            IAlertHandler alertHandler,
            IAppStoreHandler appStoreHandler,
            ITripsService tripService,
            IMvxMessenger mvxMessenger)
            : base(externalMapHandler, alertHandler, appStoreHandler)
        {
            this.universalObjectWrapperSingletonDriver = universalObjectWrapperSingletonDriver;
            this.jsonConverter = jsonConverter;
            this.tripService = tripService;
            this.mvxMessenger = mvxMessenger;
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();

            this.ViewTripDetailsCommand = new MvxCommand(this.ViewTripDetails);

            this.AcceptTripCommand = new MvxCommand(() => this.AcceptTripTaskCompletion = NotifyTaskCompletion.Create(this.AcceptTripAsync),
                                                    () => NotifyTaskCompletionExtensions.CanExecuteTaskCompletion(this.AcceptTripTaskCompletion));

            this.FindMoreTripsCommand = new MvxCommand(this.FindMoreTrips);
        }

        // MVX Life cycle
        public void Init(string tripSerialized)
        {
            this.tripSerialized = tripSerialized;
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

            bundle.Data[TRIP_SERIALIZED_BUNDLE_KEY] = this.tripSerialized;
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);

            this.tripSerialized = state.Data[TRIP_SERIALIZED_BUNDLE_KEY];
        }

        public override void Start()
        {
            this.Trip = this.jsonConverter.DeserializeObject<Trip>(this.tripSerialized);

            this.TripReturnStops = new ObservableCollection<TripStopItemViewModel>();

            this.FillTripReturnStops();

            //base.Start needs this.Trip
            base.Start();
        }

        // MVVM Properties
        public string AcceptText => Trip.TripReturn != null ? IDriveYourCarStrings.SlideToAcceptTrips.ToUpper() : IDriveYourCarStrings.SlideToAccept.ToUpper();

        public string MilesFromHome => string.Format(IDriveYourCarStrings.XMilesFromHome, (int)this.Trip.Distance);

        public string EstimatedDuration => string.Format(IDriveYourCarStrings.EstXMinutes, (int)this.Trip.Duration.Value);

        public string TripReturnPickupDate => DateTime.Parse(this.Trip.TripReturn?.PickupDate).ToFormattedString(DateStringFormat.DayMonthDayNumber);

        public string TripReturnPickupTime => DateTime.Parse(this.Trip.TripReturn?.PickupTime).ToFormattedString(DateStringFormat.TimeAMPM);

        public string TripReturnMilesFromHome => string.Format(IDriveYourCarStrings.XMilesFromHome, (int)this.Trip.TripReturn?.Distance);

        public string TripReturnEstimatedDuration => string.Format(IDriveYourCarStrings.EstXMinutes, (int)this.Trip.TripReturn?.Duration.Value);

        public string TripReturnPickup => this.Trip.TripReturn?.TripType == TripType.AirportPickup ? this.Trip.TripReturn?.FromAirport.Name : this.Trip.TripReturn?.PickupAddress.CityAndStateFormatted;

        public string TripReturnDropoff => this.Trip.TripReturn?.TripType == TripType.AirportDropoff ? this.Trip.TripReturn?.ToAirport.Name : this.Trip.TripReturn?.DropoffAddress.CityAndStateFormatted;

        public ObservableCollection<TripStopItemViewModel> TripReturnStops { get; set; }

        public INotifyTaskCompletion AcceptTripTaskCompletion { get; private set; }

        // MVVM Commands
        public ICommand ViewTripDetailsCommand { get; private set; }

        public ICommand AcceptTripCommand { get; private set; }

        public ICommand FindMoreTripsCommand { get; private set; }

        // public methods

        // private methods
        private void FillTripReturnStops()
        {
            if(this.Trip?.TripReturn?.Stops != null)
            {
                foreach(var stop in this.Trip.TripReturn.Stops)
                {
                    this.TripReturnStops.Add(new TripStopItemViewModel(stop, this.Trip));
                }
            }
        }

        private async Task AcceptTripAsync()
        {
            await this.tripService.AcceptTripAsync(
                this.Trip.Id,
                success: updatedDriver =>
            {
                this.universalObjectWrapperSingletonDriver.UniversalObject = updatedDriver;
                this.mvxMessenger.Publish(new GenericCUDMessage<Trip>(this, this.Trip, CUDOperation.Delete));
            });
        }

        private void ViewTripDetails()
        {
            this.CloseWithTrack(this);

            this.ShowViewModelWithTrack<UpcomingTripDetailViewModel>(new { tripId = this.Trip.Id });
        }

        private void FindMoreTrips()
        {
            this.CloseWithTrack(this);
        }
    }
}

