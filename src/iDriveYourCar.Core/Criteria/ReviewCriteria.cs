﻿using DGenix.Mobile.Fwk.Core.Criterias;

public class ReviewCriteria : ISimplePagedListCriteriaModel
{
    /// <summary>
    /// Unused
    /// </summary>
    public bool OnlyActive { get; set; }

    /// <summary>
    /// Page to request
    /// </summary>
    public int Page { get; set; }

    /// <summary>
    /// Quantity of items to request
    /// </summary>
    public int Results { get; set; } = 20;
}