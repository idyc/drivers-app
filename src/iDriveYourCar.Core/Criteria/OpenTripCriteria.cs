﻿using System;
using PropertyChanged;
namespace iDriveYourCar.Core.Criteria
{
    [ImplementPropertyChanged]
    public class OpenTripCriteria
    {
        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public long? MaxDrivingTime { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
