using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Models;
using SQLite;
using System.Linq;

namespace iDriveYourCar.Core.Repositories
{
    public class IDriveYourCarSQLiteAdapter : SQLiteAdapter
    {
        public IDriveYourCarSQLiteAdapter()
            : base()
        {
        }

        protected override string DatabaseName => "idriveyourcar.sqlite";

        public override System.Collections.Generic.IEnumerable<System.Type> TableTypes
        {
            get
            {
                return new[] {
                    typeof(Address),
                    typeof(User),
                    typeof(Document),
                    typeof(DriverUnavailabilityWeek),
                    typeof(DriverUnavailability),
                    typeof(Driver),
                    typeof(Client),
                    typeof(DriverInvitation),
                    typeof(Referrals),
                    typeof(Airport),
                    typeof(Airline),
                    typeof(Coupon),
                    typeof(Passenger),
                    typeof(TripStatus),
                    typeof(TripStop),
                    typeof(Expense),
                    typeof(Trip),
                    typeof(Review),
                    typeof(Notification),
                    typeof(NotificationData)
                };
            }
        }

        protected override async Task Initialize(SQLiteAsyncConnection connection)
        {
            await base.Initialize(connection).ConfigureAwait(false);

            await connection.CreateTablesAsync(CreateFlags.None, this.TableTypes.ToArray()).ConfigureAwait(false);
        }

        public override async Task DropTables()
        {
            await base.DropTables().ConfigureAwait(false);

            var tasks = new[]
            {
                this.Connection.DropTableAsync<Address>(),
                this.Connection.DropTableAsync<User>(),
                this.Connection.DropTableAsync<Document>(),
                this.Connection.DropTableAsync<DriverUnavailabilityWeek>(),
                this.Connection.DropTableAsync<DriverUnavailability>(),
                this.Connection.DropTableAsync<Driver>(),
                this.Connection.DropTableAsync<Client>(),
                this.Connection.DropTableAsync<DriverInvitation>(),
                this.Connection.DropTableAsync<Referrals>(),
                this.Connection.DropTableAsync<Airport>(),
                this.Connection.DropTableAsync<Airline>(),
                this.Connection.DropTableAsync<Coupon>(),
                this.Connection.DropTableAsync<Passenger>(),
                this.Connection.DropTableAsync<TripStatus>(),
                this.Connection.DropTableAsync<TripStop>(),
                this.Connection.DropTableAsync<Expense>(),
                this.Connection.DropTableAsync<Trip>(),
                this.Connection.DropTableAsync<Review>(),
                this.Connection.DropTableAsync<Notification>(),
                this.Connection.DropTableAsync<NotificationData>(),
            };

            await Task.WhenAll(tasks).ConfigureAwait(false);

            await this.Initialize(this.Connection).ConfigureAwait(false);
        }
    }
}
