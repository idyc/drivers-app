﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Repositories;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Repositories
{
	public interface IUserInformationRepository : IRepository<User, long>
	{
		Task ClearDatabaseAsync();

		Task<User> GetUserInformationAsync();
	}
}
