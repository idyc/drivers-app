﻿using System;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Repositories;

namespace iDriveYourCar.Core.Repositories
{
    public interface INotificationRepository : ISimpleRepository<Notification>
    {
    }
}
