﻿using System;
using DGenix.Mobile.Fwk.Core.Repositories;
using iDriveYourCar.Core.Models;
using System.Threading.Tasks;
namespace iDriveYourCar.Core.Repositories
{
	public interface IReferralRepository : IRepository<Referrals, long>
	{
		Task<Referrals> GetDriverReferrals();
	}
}
