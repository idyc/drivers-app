﻿using System;
using DGenix.Mobile.Fwk.Core.Repositories;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Repositories
{
	public interface IReviewRepository : IRepository<Review, long>
	{
	}
}
