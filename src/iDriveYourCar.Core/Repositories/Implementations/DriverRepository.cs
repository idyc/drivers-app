using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Models;
using SQLiteNetExtensionsAsync.Extensions;
using DGenix.Mobile.Fwk.Core;

namespace iDriveYourCar.Core.Repositories
{
    public class DriverRepository : SQLiteRepository<Driver, long>, IDriverRepository
    {
        public DriverRepository(ISQLiteAdapter adapter)
        : base(adapter)
        {
        }

        public async Task<Driver> GetCurrentDriverAsync(long driverId)
        {
            var driver = await this.Adapter.Connection.GetWithChildrenAsync<Driver>(driverId, true).ConfigureAwait(false);

            if(driver != null)
            {
                driver.FinishedTrips = driver.UpcomingTrips != null ? driver.UpcomingTrips.Where(t => t.IsFinished).ToList() : null;
                driver.CanceledTrips = driver.UpcomingTrips != null ? driver.UpcomingTrips.Where(t => t.IsCancelled).ToList() : null;
                driver.UpcomingTrips = driver.UpcomingTrips != null ? driver.UpcomingTrips.Where(t => !t.IsFinished && !t.IsCancelled).ToList() : null;
            }

            return driver;
        }

        public override async Task<Driver> CreateOrUpdateAsync(Driver entity)
        {
            entity.CanceledTrips.ForEach(t => t.IsCancelled = true);

            entity.UpcomingTrips.AddRange(entity.FinishedTrips);
            entity.UpcomingTrips.AddRange(entity.CanceledTrips);

            await base.CreateOrUpdateAsync(entity).ConfigureAwait(false);

            foreach(var finished in entity.FinishedTrips)
                entity.UpcomingTrips.Remove(finished);

            foreach(var canceled in entity.CanceledTrips)
                entity.UpcomingTrips.Remove(canceled);

            return entity;
        }
    }
}
