﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Models;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;

namespace iDriveYourCar.Core.Repositories
{
    public class NotificationRepository : SQLiteSimpleRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(ISQLiteAdapter adapter)
            : base(adapter)
        {
        }

        public override async Task<IEnumerable<Notification>> PersistManyAsync(IEnumerable<Notification> entities)
        {
            var currentItems = await this.GetAllAsync().ConfigureAwait(false);

            await this.Adapter.Connection.DeleteAllAsync(currentItems, true).ConfigureAwait(false);

            await this.Adapter.Connection.RunInTransactionAsync(
                (SQLiteConnection obj) =>
            {
                obj.InsertAll(entities);
            }).ConfigureAwait(false);

            return entities;
        }
    }
}
