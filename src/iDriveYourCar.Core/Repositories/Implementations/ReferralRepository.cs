﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Models;
using System.Linq;

namespace iDriveYourCar.Core.Repositories
{
	public class ReferralRepository : SQLiteRepository<Referrals, long>, IReferralRepository
	{
		public ReferralRepository(ISQLiteAdapter adapter)
		: base(adapter)
		{
		}

		public async Task<Referrals> GetDriverReferrals()
		{
			var all = await this.GetAllAsync().ConfigureAwait(false);

			return all.FirstOrDefault();
		}

	}
}
