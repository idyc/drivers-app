﻿using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Repositories
{
	public class TripsRepository : SQLiteRepository<Trip, long>, ITripRepository
	{
		public TripsRepository(ISQLiteAdapter adapter)
		: base(adapter)
		{
		}
	}
}