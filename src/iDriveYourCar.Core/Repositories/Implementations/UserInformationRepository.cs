﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Repository.SQLite;
using iDriveYourCar.Core.Models;
using SQLiteNetExtensionsAsync.Extensions;

namespace iDriveYourCar.Core.Repositories
{
	public class UserInformationRepository : SQLiteRepository<User, long>, IUserInformationRepository
	{
		public UserInformationRepository(ISQLiteAdapter adapter)
		: base(adapter)
		{
		}

		public Task ClearDatabaseAsync()
		{
			return this.Adapter.DropTables();
		}

		public async Task<User> GetUserInformationAsync()
		{
			var result = await this.Adapter.Connection.GetAllWithChildrenAsync<User>(recursive: true).ConfigureAwait(false);

			return result.FirstOrDefault();
		}
	}
}
