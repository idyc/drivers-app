﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Helpers
{
    public class EnumHelper : IEnumHelper
    {
        public string GetDocumentTypeDescription(DocumentType documentType)
        {
            string documentTypeResourceString;
            switch(documentType)
            {
                case DocumentType.DriverLicense:
                    documentTypeResourceString = IDriveYourCarStrings.DriversLicense;
                    break;
                case DocumentType.VehicleInsurance:
                    documentTypeResourceString = IDriveYourCarStrings.VehicleInsurance;
                    break;
                default:
                    documentTypeResourceString = string.Empty;
                    break;
            }
            return documentTypeResourceString;
        }

        public string GetApplicantStatusDescription(ApplicantStatus applicantStatus)
        {
            string applicantStatusResourceString;
            switch(applicantStatus)
            {
                case ApplicantStatus.Active:
                    applicantStatusResourceString = IDriveYourCarStrings.Active;
                    break;
                case ApplicantStatus.PendingStarred:
                    applicantStatusResourceString = IDriveYourCarStrings.PendingStarred;
                    break;
                case ApplicantStatus.PendingIncomplete:
                    applicantStatusResourceString = IDriveYourCarStrings.PendingIncomplete;
                    break;
                case ApplicantStatus.ReviewRequiredIncomplete:
                    applicantStatusResourceString = IDriveYourCarStrings.ReviewRequiredIncomplete;
                    break;
                case ApplicantStatus.PendingComplete:
                    applicantStatusResourceString = IDriveYourCarStrings.PendingComplete;
                    break;
                case ApplicantStatus.Rejected:
                    applicantStatusResourceString = IDriveYourCarStrings.Rejected;
                    break;
                case ApplicantStatus.Interviewing:
                    applicantStatusResourceString = IDriveYourCarStrings.Interviewing;
                    break;
                case ApplicantStatus.Training:
                    applicantStatusResourceString = IDriveYourCarStrings.Training;
                    break;
                case ApplicantStatus.PendingActivation:
                    applicantStatusResourceString = IDriveYourCarStrings.PendingActivation;
                    break;
                default:
                    applicantStatusResourceString = string.Empty;
                    break;
            }
            return applicantStatusResourceString;
        }
    }
}
