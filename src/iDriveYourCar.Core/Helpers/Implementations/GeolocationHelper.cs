﻿using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Models;
using iDriveYourCar.Core.Services;

namespace iDriveYourCar.Core.Helpers
{
    public class GeolocationHelper : IDriverLocationHelper
    {
        private readonly IGeolocatorHelper geolocationHelper;
        private readonly IDriverService driverService;

        public GeolocationHelper(
            IGeolocatorHelper geolocationHelper,
            IDriverService driverService)
        {
            this.geolocationHelper = geolocationHelper;
            this.driverService = driverService;
        }

        public async Task SendDriverLocationToServerAsync(long operatorId)
        {
            var position = await this.geolocationHelper.GetCachedPositionAsync();

            if(position == default(Geoposition))
                position = await this.geolocationHelper.GetCurrentPositionAsync();

            if(position == default(Geoposition))
                return;

            await this.driverService.UpdateDriverLocationAsync(
                position,
                operatorId,
                success: () => { });
        }
    }
}
