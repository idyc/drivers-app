﻿using System;
using System.Threading.Tasks;

namespace iDriveYourCar.Core.Helpers
{
    public interface IDriverLocationHelper
    {
        Task SendDriverLocationToServerAsync(long operatorId);
    }
}
