﻿using System;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Helpers
{
	public interface IEnumHelper
	{
		string GetDocumentTypeDescription(DocumentType documentType);

		string GetApplicantStatusDescription(ApplicantStatus applicantStatus);
	}
}
