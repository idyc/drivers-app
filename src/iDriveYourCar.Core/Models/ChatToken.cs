﻿using System;
namespace iDriveYourCar.Core.Models
{
    public class ChatToken
    {
        public string Identity { get; set; }

        public string Token { get; set; }
    }
}
