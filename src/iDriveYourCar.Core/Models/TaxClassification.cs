﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum TaxClassification : long
    {
        [Code("NONE")]
        None,

        [Code("SOLE PROPRIETOR")]
        SolePropietor,

        [Code("LLC")]
        LLC,

        [Code("PARTNERSHIP")]
        Partnership,

        [Code("CORPORATION")]
        Corporation
    }
}
