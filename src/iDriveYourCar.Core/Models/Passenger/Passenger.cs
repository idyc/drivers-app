﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class Passenger : BaseEntityModel
    {
        [JsonProperty(PropertyName = "passengerable_type")]
        public string PassengerableType { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }



        [ForeignKey(typeof(Trip))]
        public long TripId { get; set; }



        [ForeignKey(typeof(Client))]
        [JsonProperty(PropertyName = "passengerable_id")]
        public long PassengerableId { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "passengerable")]
        public Client Passengerable { get; set; }
    }
}
