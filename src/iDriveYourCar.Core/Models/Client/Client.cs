using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class Client : BaseEntityModel
    {
        public string Prefix { get; set; }

        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "account_number")]
        public string AccountNumber { get; set; }

        [JsonProperty(PropertyName = "vip_number")]
        public string VipNumber { get; set; }

        [JsonProperty(PropertyName = "personal_email")]
        public string PersonalEmail { get; set; }

        [JsonProperty(PropertyName = "bussines_email")]
        public string BusinessEmail { get; set; }

        [JsonProperty(PropertyName = "home_phone")]
        public string HomePhone { get; set; }

        [JsonProperty(PropertyName = "cell_phone")]
        public string CellPhone { get; set; }

        [JsonProperty(PropertyName = "bussines_phone")]
        public string BusinessPhone { get; set; }

        [JsonProperty(PropertyName = "fax_number")]
        public string FaxNumber { get; set; }

        [JsonProperty(PropertyName = "referal_code")]
        public string ReferralCode { get; set; }

        [JsonProperty(PropertyName = "referered_by")]
        public long? ReferredBy { get; set; }

        [JsonProperty(PropertyName = "referal_balance")]
        public decimal ReferralBalance { get; set; }

        [JsonProperty(PropertyName = "notes_for_driver")]
        public string NotesForDriver { get; set; }

        [JsonProperty(PropertyName = "account_notes")]
        public string AccountNotes { get; set; }

        [JsonProperty(PropertyName = "total_trips")]
        public long TotalTrips { get; set; }

        [JsonProperty(PropertyName = "total_cost")]
        public decimal TotalCost { get; set; }

        [JsonProperty(PropertyName = "last_reservation")]
        public DateTime? LastReservation { get; set; }

        public ClientType Type { get; set; }

        [JsonProperty(PropertyName = "booking_contact_first_name")]
        public string BookingContactFirstName { get; set; }

        [JsonProperty(PropertyName = "booking_contact_last_name")]
        public string BookingContactLastName { get; set; }

        [JsonProperty(PropertyName = "booking_contact_email")]
        public string BookingContactEmail { get; set; }

        [JsonProperty(PropertyName = "booking_contact_cell_phone")]
        public string BookingContactCellPhone { get; set; }

        [JsonProperty(PropertyName = "booking_contact_bussines_phone")]
        public string BookingContactBusinessPhone { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }



        [ForeignKey(typeof(User))]
        [JsonProperty(PropertyName = "user_id")]
        public long UserId { get; set; }

        [OneToOne(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
        public User User { get; set; }



        [ForeignKey(typeof(Referrals))]
        public long ReferralsLocalId { get; set; }
    }
}
