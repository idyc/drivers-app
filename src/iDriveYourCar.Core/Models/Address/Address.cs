﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class Address : BaseEntityModel
    {
        [JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool Primary { get; set; }

        [JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool Regular { get; set; }

        [JsonProperty(PropertyName = "postal_code")]
        public string PostalCode { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public string Raw { get; set; }

        public decimal? Lat { get; set; }

        public decimal? Lng { get; set; }

        [JsonProperty(PropertyName = "google_place_id")]
        public string GooglePlaceId { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }



        [ForeignKey(typeof(User))]
        [JsonProperty(PropertyName = "user_id")]
        public long UserId { get; set; }

        [Ignore]
        public string NameCityAndStateFormatted => $"{this.Name}, {this.City}, {this.State}";

        [Ignore]
        public string CityAndStateFormatted => $"{this.City}, {this.State}";
    }
}
