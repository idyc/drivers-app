﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models
{
	public class Airport : BaseEntityModel
	{
		[JsonProperty(PropertyName = "code")]
		public string Code { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "google_place_id")]
		public string GooglePlaceId { get; set; }

		[JsonProperty(PropertyName = "lat")]
		public decimal? Lat { get; set; }

		[JsonProperty(PropertyName = "lng")]
		public decimal? Lng { get; set; }
	}
}
