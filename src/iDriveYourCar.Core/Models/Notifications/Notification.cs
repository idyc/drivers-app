﻿using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class Notification : BaseEntityLocalModel
    {
        public string Image { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        [JsonProperty(PropertyName = "readed")]
        public bool? Read { get; set; }

        [JsonProperty(PropertyName = "date_created")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "channel_sid")]
        public string ChannelSID { get; set; }

        [JsonProperty(PropertyName = "type"), JsonConverter(typeof(CodeEnumJsonConverter))]
        public NotificationType NotificationType { get; set; }

        [JsonProperty(PropertyName = "can_be_deleted")]
        public bool CanBeDeleted { get; set; }

        [ForeignKey(typeof(NotificationData))]
        public long NotificationDataId { get; set; }

        [OneToOne(CascadeOperations = CascadeOperation.All)]
        [JsonProperty(PropertyName = "data")]
        public NotificationData NotificationData { get; set; }
    }
}
