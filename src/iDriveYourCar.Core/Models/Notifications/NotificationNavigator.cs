﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Handlers;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Platform.Platform;
using iDriveYourCar.Core.Models.Push;
using System.Collections.Generic;

namespace iDriveYourCar.Core.PushNotifications
{
    public class NotificationNavigator : FwkBaseNavigatingObject
    {
        private readonly IAlertHandler alertHandler;
        private readonly IMvxJsonConverter jsonConverter;

        public NotificationNavigator(
            IAlertHandler alertHandler,
            IMvxJsonConverter jsonConverter)
        {
            this.alertHandler = alertHandler;
            this.jsonConverter = jsonConverter;
        }

        public void PerformNavigationByCode(NotificationData data)
        {
            switch (data.NotificationCode)
            {
                case PushTypes.DriverSignedUp:
                case PushTypes.ClientSignedUp:
                    this.ShowViewModelWithTrack<InvitesViewModel>();
                    break;

                case PushTypes.AccountActivated:
                case PushTypes.InPersonTrainingSchedule:
                case PushTypes.InPersonTrainingStartsSoon:
                    this.ShowViewModelWithTrack<MyTripsViewModel>();
                    break;

                case PushTypes.NewTripAvailable:
                case PushTypes.NewTripAvailableDriverRequested:
                case PushTypes.NewTripAccepted:
                    this.ShowViewModelWithTrack<FindOpenTripsViewModel>();
                    break;

                case PushTypes.TripHasBeenModified:
                case PushTypes.TripHasBeenModifiedScheduleConflict:
                    this.ShowViewModelWithTrack<UpcomingTripModifiedDetailViewModel>(new { tripChangesSerialized = this.jsonConverter.SerializeObject(GetTripChangesPush(data)) });
                    break;

                case PushTypes.TripHasBeenCanceled:
                case PushTypes.TripHasBeenCanceledFee:
                    this.ShowViewModelWithTrack<UpcomingTripModifiedDetailViewModel>(new { tripChangesSerialized = this.jsonConverter.SerializeObject(GetTripChangesPush(data, true)) });
                    break;

                case PushTypes.TripComingUp:
                    this.ShowViewModelWithTrack<UpcomingTripDetailViewModel>(new { tripId = data.TripId });
                    break;

                case PushTypes.TimeToStartTrip:
                    this.ShowViewModelWithTrack<MyTripsViewModel>();
                    break;

                case PushTypes.UrgentMessageFromClient:
                case PushTypes.ClientHasRequestedPickup:
                case PushTypes.CSRMessageReceived:
                case PushTypes.DispatchTierUpgrade:
                case PushTypes.ScoreHasFallenBellow:
                case PushTypes.PayoutHasIncreased:
                    this.ShowViewModelWithTrack<MyTripsViewModel>();
                    break;
            }
        }

        private TripChangesPush GetTripChangesPush(NotificationData data, bool isCancelled = false)
        {
            return new TripChangesPush
            {
                TripId = long.Parse(data.TripId),
                FieldChanges = data.FieldsChanges ?? new List<TripFieldChanged>(),
                IsCanceled = isCancelled
            };
        }
    }
}
