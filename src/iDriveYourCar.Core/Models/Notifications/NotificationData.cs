﻿using System;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class NotificationData : BaseEntityLocalModel
    {
        [JsonProperty(PropertyName = "code"), JsonConverter(typeof(CodeEnumJsonConverter))]
        public PushTypes NotificationCode { get; set; }

        [JsonProperty(PropertyName = "trip_id")]
        public string TripId { get; set; }

        [TextBlob("FieldsChangedBlobbed")]
        [JsonProperty(PropertyName = "fields_changed"), JsonConverter(typeof(ListCodeEnumJsonConverter<TripFieldChanged>))]
        public List<TripFieldChanged> FieldsChanges { get; set; }

        public string FieldsChangedBlobbed { get; set; }
    }
}
