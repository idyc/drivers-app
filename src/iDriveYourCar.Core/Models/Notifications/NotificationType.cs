﻿using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum NotificationType : long
    {
        [Code("ChatMessage::class")]
        ChatMessage,
        [Code("Notification::class")]
        Notification
    }
}
