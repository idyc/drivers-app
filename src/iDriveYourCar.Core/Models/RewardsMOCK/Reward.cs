﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;

namespace iDriveYourCar.Core.Models
{
	public class Reward : BaseEntityModel
	{
		public string Title { get; set; }

		public string Detail { get; set; }

		public string Code { get; set; }
	}
}
