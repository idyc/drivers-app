﻿using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using PropertyChanged;

namespace iDriveYourCar.Core.Models
{
    [ImplementPropertyChanged]
    public class Contact : BaseEntityModel
    {
        [JsonProperty(PropertyName = "subject")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}
