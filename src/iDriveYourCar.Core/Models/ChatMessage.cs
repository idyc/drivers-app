﻿using System;
namespace iDriveYourCar.Core.Models
{
    public class ChatMessage
    {
        public string Body { get; set; }

        public string Author { get; set; }

        public DateTime DateTime { get; set; }
    }
}
