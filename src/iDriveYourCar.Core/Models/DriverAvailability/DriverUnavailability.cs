﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
	public class DriverUnavailability : BaseEntityModel
	{
		[JsonProperty(PropertyName = "start_date")]
		public DateTime StartDate { get; set; }

		[JsonProperty(PropertyName = "end_date")]
		public DateTime EndDate { get; set; }

		[JsonProperty(PropertyName = "updated_at")]
		public DateTime UpdatedAt { get; set; }

		[JsonProperty(PropertyName = "created_at")]
		public DateTime CreatedAt { get; set; }


		[ForeignKey(typeof(Driver))]
		[JsonProperty(PropertyName = "driver_id")]
		public long DriverId { get; set; }
	}
}
