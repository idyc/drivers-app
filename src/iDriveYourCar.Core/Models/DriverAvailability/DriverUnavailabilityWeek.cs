﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using SQLiteNetExtensions.Attributes;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Converters.Json;

namespace iDriveYourCar.Core.Models
{
    public class DriverUnavailabilityWeek : BaseEntityModel
    {
        [JsonProperty(PropertyName = "day")]
        public long Day { get; set; }

        [JsonProperty(PropertyName = "start")]
        public long Start { get; set; }

        [JsonProperty(PropertyName = "stop")]
        public long Stop { get; set; }

        [JsonProperty(PropertyName = "time_start"), JsonConverter(typeof(TimeToPHPTimeConverter))]
        public DateTime TimeStart { get; set; }

        [JsonProperty(PropertyName = "time_stop"), JsonConverter(typeof(TimeToPHPTimeConverter))]
        public DateTime TimeStop { get; set; }

        [JsonProperty(PropertyName = "all_day")]
        public bool IsUnavailableAllDay { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }


        [ForeignKey(typeof(Driver))]
        [JsonProperty(PropertyName = "driver_id")]
        public long DriverId { get; set; }
    }
}
