﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace iDriveYourCar.Core.Models
{
    public class Review : BaseEntityLocalModel
    {
        [JsonProperty(PropertyName = "rating")]
        public decimal Rating { get; set; }

        [JsonProperty(PropertyName = "poor_driving"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool PoorDriving { get; set; }

        [JsonProperty(PropertyName = "poor_comunication"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool PoorCommunication { get; set; }

        [JsonProperty(PropertyName = "unpunctual"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool Unpunctual { get; set; }

        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime? Date { get; set; }
    }
}
