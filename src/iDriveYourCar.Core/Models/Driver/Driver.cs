using System;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SQLiteNetExtensions.Attributes;
using SQLite;
using PropertyChanged;

namespace iDriveYourCar.Core.Models
{
    [ImplementPropertyChanged]
    public class Driver : BaseEntityModel
    {
        public Driver()
        {
            this.UpcomingTrips = new List<Trip>();
            this.FinishedTrips = new List<Trip>();
            this.Documents = new List<Document>();
            this.Unavailability = new List<DriverUnavailability>();
            this.WeeklyUnavailability = new List<DriverUnavailabilityWeek>();
        }

        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "personal_email")]
        public string PersonalEmail { get; set; }

        [JsonProperty(PropertyName = "bussines_email")]
        public string BusinessEmail { get; set; }

        [JsonProperty(PropertyName = "home_phone")]
        public string HomePhone { get; set; }

        [JsonProperty(PropertyName = "cell_phone")]
        public string CellPhone { get; set; }

        [JsonProperty(PropertyName = "bussines_phone")]
        public string BusinessPhone { get; set; }

        [JsonProperty(PropertyName = "fax_number")]
        public string FaxNumber { get; set; }

        [JsonProperty(PropertyName = "balance_of_referred_clients")]
        public decimal? BalanceOfReferredClients { get; set; }

        public decimal? Rating { get; set; }

        [JsonProperty(PropertyName = "has_valid_driver_license"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool HasValidDriverLicence { get; set; }

        [JsonProperty(PropertyName = "has_clean_motor_vehicle_record"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool HasCleanMotorVehicleRecord { get; set; }

        [JsonProperty(PropertyName = "has_clean_criminal_record"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool HasCleanCriminalRecord { get; set; }

        [JsonProperty(PropertyName = "has_experience_as_profesional_driver"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool HasExperienceAsProfessionalDriver { get; set; }

        //[JsonProperty(PropertyName = "experience_as_profesional_driver")]
        //public object ExperienceAsProfessionalDriver { get; set; }

        //[JsonProperty(PropertyName = "additional_qualifications_or_licenses")]
        //public object AdditionalQualificationsOrLicenses { get; set; }

        //[JsonProperty(PropertyName = "additional_comments_about_skills")]
        //public object AdditionalCommentsAboutSkills { get; set; }

        [JsonProperty(PropertyName = "name_bank_account")]
        public string NameBankAccount { get; set; }

        [JsonProperty(PropertyName = "tax_id"), JsonConverter(typeof(CodeEnumJsonConverter))]
        public TaxClassification TaxId { get; set; }

        [JsonProperty(PropertyName = "ssn_tin")]
        public string SSNTIN { get; set; }

        [JsonProperty(PropertyName = "routing_number")]
        public string RoutingNumber { get; set; }

        [JsonProperty(PropertyName = "account_number")]
        public string AccountNumber { get; set; }

        [JsonProperty(PropertyName = "max_driving_time")]
        public long? MaxDrivingTime { get; set; }

        //public object Company { get; set; }

        [JsonProperty(PropertyName = "fee_for_hour")]
        public decimal? FeePerHour { get; set; }

        [JsonProperty(PropertyName = "applicant_status"), JsonConverter(typeof(CodeEnumJsonConverter))]
        public ApplicantStatus ApplicantStatus { get; set; }

        [JsonConverter(typeof(CodeEnumJsonConverter))]
        public DriverType Type { get; set; }

        #region unused (?) properties
        //[JsonProperty(PropertyName = "recruiter_notes")]
        //public object RecruiterNotes { get; set; }

        //[JsonProperty(PropertyName = "interviewed_at")]
        //public object InterviewedAt { get; set; }

        //[JsonProperty(PropertyName = "trained_at")]
        //public object TrainedAt { get; set; }

        //[JsonProperty(PropertyName = "activated_at")]
        //public object ActivatedAt { get; set; }

        //[JsonProperty(PropertyName = "interviewed_by")]
        //public object InterviewedBy { get; set; }

        //[JsonProperty(PropertyName = "trained_by")]
        //public object TrainedBy { get; set; }

        //[JsonProperty(PropertyName = "interview_scheduled")]
        //public object InterviewScheduled { get; set; }

        //[JsonProperty(PropertyName = "trained_scheduled")]
        //public object TrainedScheduled { get; set; }

        //[JsonProperty(PropertyName = "interview_location")]
        //public object InterviewLocation { get; set; }

        //[JsonProperty(PropertyName = "trained_location")]
        //public object TrainedLocation { get; set; }

        //[JsonProperty(PropertyName = "starred_for_later")]
        //public object StarredForLater { get; set; }

        //[JsonProperty(PropertyName = "interview_score")]
        //public object InterviewScore { get; set; }
        #endregion

        [JsonProperty(PropertyName = "birthdate"), JsonConverter(typeof(ZeroDateTimeToMinDateTimeConverter))]
        public DateTime? Birthday { get; set; }

        [JsonProperty(PropertyName = "rate_one_star")]
        public long RateOneStar { get; set; }

        [JsonProperty(PropertyName = "rate_two_stars")]
        public long RateTwoStars { get; set; }

        [JsonProperty(PropertyName = "rate_three_stars")]
        public long RateThreeStars { get; set; }

        [JsonProperty(PropertyName = "rate_four_stars")]
        public long RateFourStars { get; set; }

        [JsonProperty(PropertyName = "rate_five_stars")]
        public long RateFiveStars { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }



        [OneToOne(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
        public User User { get; set; }

        [ForeignKey(typeof(User))]
        [JsonProperty(PropertyName = "user_id")]
        public long UserId { get; set; }



        [OneToMany(CascadeOperations = CascadeOperation.All)]
        [JsonProperty(PropertyName = "upcoming_trips")]
        public List<Trip> UpcomingTrips { get; set; }



        [Ignore]
        [JsonProperty(PropertyName = "finished_trips")]
        public List<Trip> FinishedTrips { get; set; }



        [Ignore]
        [JsonProperty(PropertyName = "canceled_trips")]
        public List<Trip> CanceledTrips { get; set; }



        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Document> Documents { get; set; }




        [JsonProperty(PropertyName = "unavailability_week")]
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<DriverUnavailabilityWeek> WeeklyUnavailability { get; set; }




        [JsonProperty(PropertyName = "unavailability")]
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<DriverUnavailability> Unavailability { get; set; }

        public override void FillBeforePersist()
        {
            base.FillBeforePersist();

            this.User.PrimaryAddressId = this.User.PrimaryAddress != null ? this.User.PrimaryAddress.Id : (long?)null;
        }
    }
}
