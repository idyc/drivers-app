﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
	public enum DriverType : long
	{
		[Code("TIER 1")]
		Tier1,
		[Code("TIER 2")]
		Tier2,
		[Code("TIER 3")]
		Tier3
	}
}
