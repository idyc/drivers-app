﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum ApplicantStatus : long
    {
        [Code("ACTIVE")]
        Active,
        [Code("PENDING STARRED")]
        PendingStarred,
        [Code("PENDING INCOMPLETE")]
        PendingIncomplete,
        [Code("PENDING COMPLETE")]
        PendingComplete,
        [Code("IN PERSON TRAINING COMPLETE")]
        InPersonTrainingComplete,
        [Code("REJECTED")]
        Rejected,
        [Code("INTERVIEWING")]
        Interviewing,
        [Code("TRAINING")]
        Training,
        [Code("PENDING ACTIVATION")]
        PendingActivation,
        [Code("REVIEW REQUIRED INCOMPLETE")]
        ReviewRequiredIncomplete
    }
}
