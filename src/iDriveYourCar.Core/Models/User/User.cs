﻿using System;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;
using PropertyChanged;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    [ImplementPropertyChanged]
    public class User : BaseEntityModel
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Avatar { get; set; }

        [JsonConverter(typeof(CodeEnumJsonConverter))]
        public UserType UserType { get; set; }

        [JsonProperty(PropertyName = "referal_code")]
        public string ReferralCode { get; set; }

        [JsonProperty(PropertyName = "referered_by")]
        public long? ReferredBy { get; set; }

        [JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool Confirmed { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "primary_address")]
        [OneToOne(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
        public Address PrimaryAddress { get; set; }

        [ForeignKey(typeof(Address))]
        [JsonProperty(PropertyName = "primary_address_id")]
        public long? PrimaryAddressId { get; set; }

        /// <summary>
        /// This property is only retrieved after a successfull login
        /// </summary>
        /// <value><c>true</c> if has bank info; otherwise, <c>false</c>.</value>
        [JsonProperty(PropertyName = "has_bank_info")]
        public bool HasBankInfo { get; set; }

        //[OneToMany]
        //public List<Address> Addresses { get; set; }

        #region unused properties

        //[JsonProperty(PropertyName = "confirmation_code")]
        //public string ConfirmationCode { get; set; }

        //[JsonProperty(PropertyName = "remember_token")]
        //public string RememberToken { get; set; }

        #endregion

    }
}
