﻿using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.Models
{
	public class UserToken : Token
	{
		public User User { get; set; }
	}
}
