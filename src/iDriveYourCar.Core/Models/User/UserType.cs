﻿using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
	public enum UserType : long
	{
		[Code("DRIVER")]
		Driver
	}
}
