﻿using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum DocumentType : long
    {
        [Code("VEHICLE INSURANCE")]
        VehicleInsurance,

        [Code("DRIVER LICENSE")]
        DriverLicense,

        [Code("MOTOR VEHICLE REPORT")]
        MotorVehicleReport,

        [Code("BACKGROUND CHECK")]
        BackgroundCheck
    }
}
