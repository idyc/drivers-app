﻿using System;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
	public class Document : BaseEntityModel
	{
		[ForeignKey(typeof(Driver))]
		[JsonProperty(PropertyName = "driver_id")]
		public long DriverId { get; set; }

		[JsonProperty(PropertyName = "document_type"), JsonConverter(typeof(CodeEnumJsonConverter))]
		public DocumentType DocumentType { get; set; }

		[JsonProperty(PropertyName = "original_filename")]
		public string OriginalFilename { get; set; }

		[JsonProperty(PropertyName = "filename")]
		public string Filename { get; set; }

		public string Extension { get; set; }

		public string Path { get; set; }

		[JsonProperty(PropertyName = "mime_type")]
		public string MimeType { get; set; }

		[JsonProperty(PropertyName = "byte_size")]
		public long ByteSize { get; set; }

		//public object Disk { get; set; }

		public long Version { get; set; }

		[JsonProperty(PropertyName = "updated_at")]
		public DateTime UpdatedAt { get; set; }

		[JsonProperty(PropertyName = "created_at")]
		public DateTime CreatedAt { get; set; }
	}
}
