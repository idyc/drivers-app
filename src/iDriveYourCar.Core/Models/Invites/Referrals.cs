﻿using System;
using System.Collections.Generic;
using SQLiteNetExtensions.Attributes;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;

namespace iDriveYourCar.Core.Models
{
	public class Referrals : BaseEntityLocalModel
	{
		[OneToMany(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
		public List<DriverInvitation> Invitations { get; set; }

		[OneToMany(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
		public List<Client> Clients { get; set; }

		public string DummyNotUsedProperty { get; set; }
	}
}
