﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum InvitationType
    {
        [Code("CLIENT")]
        Client,
        [Code("DRIVER")]
        Driver
    }
}
