﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
	public class DriverInvitation : BaseEntityModel
	{
		[JsonProperty(PropertyName = "inviter_id")]
		public long InviterId { get; set; }

		[JsonProperty(PropertyName = "potential_earning")]
		public decimal PotentialEarning { get; set; }

		public string Contact { get; set; }

		public string Email { get; set; }

		[JsonProperty(PropertyName = "referal_code")]
		public string ReferralCode { get; set; }

		[JsonProperty(PropertyName = "has_reedemed"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool HasRedeemed { get; set; }

		[JsonProperty(PropertyName = "redeem_requested"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool RedeemRequested { get; set; }

		[JsonProperty(PropertyName = "updated_at")]
		public DateTime UpdatedAt { get; set; }

		[JsonProperty(PropertyName = "created_at")]
		public DateTime CreatedAt { get; set; }

		[JsonProperty(PropertyName = "invited")]
		[OneToOne(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
		public Driver Invited { get; set; }

		[JsonProperty(PropertyName = "invited_id")]
		[ForeignKey(typeof(Driver))]
		public long? InvitedId { get; set; }

		[ForeignKey(typeof(Referrals))]
		public long ReferralsLocalId { get; set; }
	}
}
