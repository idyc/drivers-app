﻿using System;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models.MyTrips.Completed
{
    public class CompletedTrip
    {
        [JsonProperty(PropertyName = "date_completed")]
        public DateTime DateCompleted { get; set; }

        [JsonProperty(PropertyName = "earning")]
        public decimal Earning { get; set; }
    }
}
