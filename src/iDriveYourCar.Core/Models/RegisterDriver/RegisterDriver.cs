﻿using DGenix.Mobile.Fwk.Core.Models;

namespace iDriveYourCar.Core.Models
{
    public class RegisterDriver : Driver
    {
        public string OptionalInviteCode { get; set; }

        public string HaveYouEverServedAsProfessionalExplanation { get; set; }

        public string HowDoCreateAnAmazingExperienceForYourClientsExplanation { get; set; }

        public string Password { get; set; }

        public StreamFileRequest Avatar { get; set; }

        public StreamFileRequest DriverLicense { get; set; }

        public StreamFileRequest VehicleInsurance { get; set; }
    }
}
