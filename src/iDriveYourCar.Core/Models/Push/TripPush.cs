﻿using System;
using Newtonsoft.Json;
using DGenix.Mobile.Fwk.Core.Converters.Json;
namespace iDriveYourCar.Core.Models
{
    public class TripPush
    {
        [JsonProperty(PropertyName = "dropoff_address")]
        public string DropoffAddress { get; set; }

        [JsonProperty(PropertyName = "estimated_time")]
        public float EstimatedTime { get; set; }

        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "pickup_address")]
        public string PickupAddress { get; set; }

        [JsonProperty(PropertyName = "pickup_date")]
        public string PickupDate { get; set; }

        [JsonProperty(PropertyName = "pickup_time")]
        public string PickupTime { get; set; }

        [JsonProperty(PropertyName = "time_from_home")]
        public float TimeFromHome { get; set; }

        [JsonConverter(typeof(CodeEnumJsonConverter))]
        [JsonProperty(PropertyName = "type")]
        public TripType Type { get; set; }

        [JsonProperty(PropertyName = "return")]
        public TripPush Return { get; set; }
    }
}