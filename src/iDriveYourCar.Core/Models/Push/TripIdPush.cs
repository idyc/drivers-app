﻿using System;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models.Push
{
    public class TripIdPush
    {
        [JsonProperty(PropertyName = "trip_id")]
        public long TripId { get; set; }
    }
}
