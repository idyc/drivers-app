﻿using System;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models.Push
{
    public class TripChangesPush : TripIdPush
    {
        [JsonProperty(PropertyName = "fields_changed"), JsonConverter(typeof(ListCodeEnumJsonConverter<TripFieldChanged>))]
        public List<TripFieldChanged> FieldChanges { get; set; }

        public bool IsCanceled { get; set; }
    }
}
