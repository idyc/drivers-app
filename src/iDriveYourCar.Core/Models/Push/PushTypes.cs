﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum PushTypes
    {
        None,

        [Code("DRIVER_SIGNED_UP")]
        DriverSignedUp,

        [Code("CLIENT_SIGNED_UP")]
        ClientSignedUp,

        [Code("ACCOUNT_ACTIVATED")]
        AccountActivated,

        // missing Introduction Push - Tips for starting off right

        [Code("INPERSON_TRAINING_SCHEDULE")]
        InPersonTrainingSchedule,

        [Code("INPERSON_TRAINING_STARTS_SOON")]
        InPersonTrainingStartsSoon,

        [Code("NEW_TRIP_AVAILABLE")]
        NewTripAvailable,

        [Code("NEW_TRIP_AVAILABLE_DRIVER_REQUESTED")]
        NewTripAvailableDriverRequested,

        [Code("NEW_TRIP_ACCEPTED")]
        NewTripAccepted,

        [Code("TRIP_HAS_BEEN_MODIFIED")]
        TripHasBeenModified,

        [Code("TRIP_HAS_BEEN_MODIFIED_SCHEDULE_CONFLICT")]
        TripHasBeenModifiedScheduleConflict,

        [Code("TRIP_HAS_BEEN_CANCELED")]
        TripHasBeenCanceled,

        [Code("TRIP_HAS_BEEN_CANCELED_FEE")]
        TripHasBeenCanceledFee,

        [Code("TRIP_COMING_UP")]
        TripComingUp,

        [Code("TIME_TO_START_TRIP")]
        TimeToStartTrip,

        [Code("URGENT_MESSAGE_FROM_CLIENT")]
        UrgentMessageFromClient,

        [Code("CLIENT_HAS_REQUESTED_PICKUP")]
        ClientHasRequestedPickup,

        [Code("CSR_MESSAGE_RECEIVED")]
        CSRMessageReceived,

        [Code("DISPATCH_TIER_UPGRADE")]
        DispatchTierUpgrade,

        [Code("SCORE_HAS_FALLEN_BELLOW")]
        ScoreHasFallenBellow,

        [Code("PAYOUT_HAS_INCREASED")]
        PayoutHasIncreased,

        /// <summary>
        /// Silent push notification used by the operator to get the driver's location
        /// </summary>
        [Code("LOCATION_REQUEST")]
        LocationRequest
    }
}
