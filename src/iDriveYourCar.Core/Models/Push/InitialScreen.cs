﻿using System;
namespace iDriveYourCar.Core.Models
{
    public enum InitialScreen
    {
        MyTrips,
        Referrals,
        UpcomingTripDetails,
        CurrentTripDetails,
        AcceptTrip,
        Profile,
        UpcomingTripModificationsDetails
    }
}