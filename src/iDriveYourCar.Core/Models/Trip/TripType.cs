﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
	public enum TripType : long
	{
		[Code("TOAIRPORT")]
		AirportDropoff,
		[Code("FROMAIRPORT")]
		AirportPickup,
		[Code("WAITANDRETURN")]
		WaitAndReturn,
		[Code("ONEWAYPICKUP")]
		OneWayPickup,
		[Code("ONEWAYDROPOFF")]
		OneWayDropoff,
		[Code("DRIVEMEHOME")]
		DriveMeHome
	}
}
