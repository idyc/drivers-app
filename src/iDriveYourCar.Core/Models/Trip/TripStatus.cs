﻿using System;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models
{
	public class TripStatus : BaseEntityModel
	{
		[JsonProperty(PropertyName = "status_type"), JsonConverter(typeof(CodeEnumJsonConverter))]
		public TripStatusType StatusType { get; set; }

		public string Name { get; set; }

		public string Shortcode { get; set; }

		public string Color { get; set; }

		[JsonProperty(PropertyName = "new_reservation"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NewReservation { get; set; }

		[JsonProperty(PropertyName = "new_online_reservation"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NewOnlineReservation { get; set; }

		[JsonProperty(PropertyName = "new_client"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NewClient { get; set; }

		[JsonProperty(PropertyName = "new_driver"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NewDriver { get; set; }

		[JsonProperty(PropertyName = "notify_operator"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NotifyOperator { get; set; }

		[JsonProperty(PropertyName = "notify_driver_manager"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NotifyDriverManager { get; set; }

		[JsonProperty(PropertyName = "notify_relationship_manager"), JsonConverter(typeof(StringNumberToBoolConverter))]
		public bool NotifyRelationshipManager { get; set; }

		//[JsonProperty(PropertyName = "updated_at")]
		//public DateTime UpdatedAt { get; set; }

		//[JsonProperty(PropertyName = "created_at")]
		//public DateTime CreatedAt { get; set; }
	}
}
