﻿namespace iDriveYourCar.Core.Models
{
    public enum ActiveTripState
    {
        NotStarted,
        NavigateToStartPoint,
        NavigateToVehicle,
        NavigateToPickup,
        StartTrip,
        ArrivedAtPickup,
        ArrivedAtDropoff,
        EndTrip,
        SubmitTrip
    }
}
