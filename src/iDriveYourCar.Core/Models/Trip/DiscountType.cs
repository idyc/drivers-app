﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
	public enum DiscountType : long
	{
		[Code]
		None = 0,
		[Code("PERCENTAGE")]
		Percentage = 1
	}
}
