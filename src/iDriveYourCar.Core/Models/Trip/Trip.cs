using System;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Converters.Json;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using iDriveYourCar.Core.Converters.Json;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class Trip : BaseEntityModel
    {
        public Trip()
        {
            this.Stops = new List<TripStop>();
            this.Passengers = new List<Passenger>();
            this.PublicNotes = new List<string>();
        }

        [JsonProperty(PropertyName = "credit_card_id")]
        public long CreditCardId { get; set; }

        [JsonProperty(PropertyName = "pickup_date")]
        public string PickupDate { get; set; }

        [JsonProperty(PropertyName = "pickup_time")]
        public string PickupTime { get; set; }

        [JsonProperty(PropertyName = "dropoff_date")]
        public string DropoffDate { get; set; }

        [JsonProperty(PropertyName = "dropoff_time")]
        public string DropoffTime { get; set; }

        [JsonProperty(PropertyName = "duration")]
        public decimal? Duration { get; set; }

        [JsonProperty(PropertyName = "dropoff_date_real")]
        public string DropoffDateReal { get; set; }

        [JsonProperty(PropertyName = "dropoff_time_real")]
        public string DropoffTimeReal { get; set; }

        [JsonProperty(PropertyName = "duration_real")]
        public decimal? DurationReal { get; set; }

        [JsonProperty(PropertyName = "flight_number")]
        public string FlightNumber { get; set; }

        [JsonProperty(PropertyName = "meetup_instrutions")]
        public string MeetupInstructions { get; set; }

        [JsonProperty(PropertyName = "offer")]
        public string Offer { get; set; }

        [JsonProperty(PropertyName = "key_location_vehicle_description")]
        public string KeyLocationVehicleDescription { get; set; }

        [JsonProperty(PropertyName = "trip_type"), JsonConverter(typeof(CodeEnumJsonConverter))]
        public TripType TripType { get; set; }

        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "is_fixed"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool IsFixed { get; set; }

        [JsonProperty(PropertyName = "gratuity")]
        public decimal? Gratuity { get; set; }

        [JsonProperty(PropertyName = "discount_type"), JsonConverter(typeof(CodeEnumJsonConverter))]
        public DiscountType DiscountType { get; set; }

        [JsonProperty(PropertyName = "discount_quantity")]
        public decimal? DiscountQuantity { get; set; }

        //public object Credits { get; set; }

        [JsonProperty(PropertyName = "total_estimated")]
        public decimal? TotalEstimated { get; set; }

        [JsonProperty(PropertyName = "total_real")]
        public decimal? TotalReal { get; set; }

        public decimal Rating { get; set; }

        [JsonProperty(PropertyName = "poor_driving"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool PoorDriving { get; set; }

        [JsonProperty(PropertyName = "poor_comunication"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool PoorCommunication { get; set; }

        [JsonProperty(PropertyName = "unpunctual"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool Unpunctual { get; set; }

        [JsonProperty(PropertyName = "rate_comment")]
        public string RateComment { get; set; }

        [JsonProperty(PropertyName = "date_comment")]
        public string DateComment { get; set; }

        [JsonProperty(PropertyName = "is_finished"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool IsFinished { get; set; }

        public decimal Distance { get; set; }

        public bool IsCancelled { get; set; }

        [JsonProperty(PropertyName = "has_trip_conflict"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool HasTripConflict { get; set; }

        [JsonProperty(PropertyName = "has_unavailability_conflict"), JsonConverter(typeof(StringNumberToBoolConverter))]
        public bool HasUnavailabilityConflict { get; set; }

        //[JsonProperty(PropertyName = "updated_at")]
        //public DateTime UpdatedAt { get; set; }

        //[JsonProperty(PropertyName = "created_at")]
        //public DateTime CreatedAt { get; set; }



        [OneToMany(CascadeOperations = CascadeOperation.All)]
        [JsonProperty(PropertyName = "expenses")]
        public List<Expense> Expenses { get; set; }




        [ForeignKey(typeof(Trip))]
        public long TripReturnId { get; set; }

        [JsonProperty(PropertyName = "trip_return")]
        [OneToOne(CascadeOperations = CascadeOperation.All)]
        public Trip TripReturn { get; set; }



        [ForeignKey(typeof(TripStatus))]
        public long StatusId { get; set; }

        [OneToOne(CascadeOperations = CascadeOperation.All)]
        public TripStatus Status { get; set; }



        [ForeignKey(typeof(Driver))]
        [JsonProperty(PropertyName = "driver_id")]
        public long? DriverId { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
        public Driver Driver { get; set; }



        [ForeignKey(typeof(Client))]
        [JsonProperty(PropertyName = "client_id")]
        public long ClientId { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
        public Client Client { get; set; }



        [TextBlob("PublicNotesBlobbed")]
        [JsonProperty(PropertyName = "public_notes")]
        public List<string> PublicNotes { get; set; }

        public string PublicNotesBlobbed { get; set; }



        [TextBlob("FieldsChangedBlobbed")]
        [JsonProperty(PropertyName = "fields_changed"), JsonConverter(typeof(ListCodeEnumJsonConverter<TripFieldChanged>))]
        public List<TripFieldChanged> FieldsChanged { get; set; }

        public string FieldsChangedBlobbed { get; set; }



        [JsonProperty(PropertyName = "vehicle_address_id")]
        public long? VehicleAddressId { get; set; }

        [ManyToOne(nameof(VehicleAddressId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "vehicle_address")]
        public Address VehicleAddress { get; set; }



        [JsonProperty(PropertyName = "pickup_address_id")]
        public long? PickupAddressId { get; set; }

        [ManyToOne(nameof(PickupAddressId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "pickup_address")]
        public Address PickupAddress { get; set; }



        [JsonProperty(PropertyName = "dropoff_address_id")]
        public long? DropoffAddressId { get; set; }

        [ManyToOne(nameof(DropoffAddressId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "dropoff_address")]
        public Address DropoffAddress { get; set; }



        [OneToMany(CascadeOperations = CascadeOperation.All)]
        [JsonProperty(PropertyName = "stops")]
        public List<TripStop> Stops { get; set; }



        [JsonProperty(PropertyName = "to_airport_id")]
        public long? ToAirportId { get; set; }

        [ManyToOne(nameof(ToAirportId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "to_airport")]
        public Airport ToAirport { get; set; }



        [JsonProperty(PropertyName = "to_airline_id")]
        public long? ToAirlineId { get; set; }

        [ManyToOne(nameof(ToAirlineId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "to_airline")]
        public Airline ToAirline { get; set; }



        [JsonProperty(PropertyName = "from_airport_id")]
        public long? FromAirportId { get; set; }

        [ManyToOne(nameof(FromAirportId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "from_airport")]
        public Airport FromAirport { get; set; }



        [JsonProperty(PropertyName = "from_airline_id")]
        public long? FromAirlineId { get; set; }

        [ManyToOne(nameof(FromAirlineId), CascadeOperations = CascadeOperation.CascadeInsert | CascadeOperation.CascadeRead)]
        [JsonProperty(PropertyName = "from_airline")]
        public Airline FromAirline { get; set; }



        [JsonProperty(PropertyName = "coupon_id")]
        public long? CouponId { get; set; }

        [OneToOne(CascadeOperations = CascadeOperation.All)]
        [JsonProperty(PropertyName = "coupon")]
        public Coupon Coupon { get; set; }



        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeInsert)]
        [JsonProperty(PropertyName = "passengers")]
        public List<Passenger> Passengers { get; set; }



        public ActiveTripState ActiveTripState { get; set; }
    }
}
