﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum TripFieldChanged : long
    {
        [Code("pickup_date")]
        PickupDate,

        [Code("pickup_time")]
        PickupTime,

        [Code("vehicle_address_id")]
        VehicleAddressId,

        [Code("key_location_vehicle_description")]
        KeyLocationVehicleDescription,

        [Code("public_notes")]
        PublicNotes,

        [Code("stops")]
        Stops,

        [Code("offer")]
        Offer,

        [Code("meetup_instrutions")]
        MeetupInstructions,

        [Code("notes_for_driver")]
        NotesForDriver,

        [Code("dropoff_date_estimated")]
        DropoffDateEstimated,

        [Code("dropoff_time_estimated")]
        DropoffTimeEstimated,

        [Code("dropoff_address_id")]
        DropoffAddressId,

        [Code("pickup_address_id")]
        PickupAddressId,

        [Code("credit_card_id")]
        CreditCardId,

        [Code("note_id")]
        NoteId
    }
}
