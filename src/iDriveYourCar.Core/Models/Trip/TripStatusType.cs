﻿using System;
using DGenix.Mobile.Fwk.Core.Models.Attributes;

namespace iDriveYourCar.Core.Models
{
    public enum TripStatusType : long
    {
        [Code("ASSIGNED")]
        Assigned,

        [Code("ACTIVE_TRIP")]
        ActiveTrip,

        [Code("UNASSIGNED")]
        Unassigned,

        [Code("FINAL")]
        Final
    }
}
