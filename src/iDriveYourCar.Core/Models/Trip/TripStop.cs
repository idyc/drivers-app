﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class TripStop : BaseEntityModel
    {
        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }


        [JsonProperty(PropertyName = "trip_id")]
        [ForeignKey(typeof(Trip))]
        public long TripId { get; set; }



        [JsonProperty(PropertyName = "address_id")]
        [ForeignKey(typeof(Address))]
        public long AddressId { get; set; }

        [ManyToOne]
        [JsonProperty(PropertyName = "address")]
        public Address Address { get; set; }
    }
}
