﻿using System;
using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models
{
	public class Airline : BaseEntityModel
	{
		[JsonProperty(PropertyName = "code")]
		public string Code { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }
	}
}
