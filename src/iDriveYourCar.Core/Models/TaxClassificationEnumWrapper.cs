﻿using System;
using System.Resources;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace iDriveYourCar.Core.Models
{
    public class TaxClassificationEnumWrapper : BaseEnumWrapperModel<TaxClassification>
    {
        public TaxClassificationEnumWrapper()
        {
        }

        public TaxClassificationEnumWrapper(TaxClassification val)
        : base(val)
        {
        }

        protected override string NullValueName => null;

        public override string Name => this.ResourceManager.GetString(this.Value.ToString());

        protected override ResourceManager ResourceManager => IDriveYourCarStrings.ResourceManager;
    }
}
