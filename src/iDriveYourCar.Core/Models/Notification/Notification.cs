﻿using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;

namespace iDriveYourCar.Core.Models
{
    public class Notification : BaseEntityModel
    {
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "time_ago")]
        public string TimeAgo { get; set; }
    }
}
