﻿using System;
namespace iDriveYourCar.Core.Models
{
    public class StartupHint
    {
        public StartupHint()
        {
            this.InitialScreen = InitialScreen.MyTrips;
        }

        public InitialScreen InitialScreen { get; set; }

        public object Hint { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }
    }
}