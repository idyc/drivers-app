﻿using DGenix.Mobile.Fwk.Repository.SQLite.Models;
using Newtonsoft.Json;
using SQLiteNetExtensions.Attributes;

namespace iDriveYourCar.Core.Models
{
    public class Expense : BaseEntityModel
    {
        public string Description { get; set; }

        public decimal? Amount { get; set; }

        public string Filename { get; set; }



        [ForeignKey(typeof(Trip))]
        [JsonProperty(PropertyName = "trip_id")]
        public long TripId { get; set; }
    }
}
