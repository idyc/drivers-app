﻿using System;
using Nito.AsyncEx;

namespace iDriveYourCar.Core
{
	// This class is never actually executed, but when Xamarin linking is enabled it does how to ensure types and properties
	// are preserved in the deployed app
	public class LinkerPleaseInclude
	{
#pragma warning disable 414

		public void Include(INotifyTaskCompletion taskCompletion)
		{
			var isFaulted = taskCompletion.IsFaulted;
			var isNotCompleted = taskCompletion.IsNotCompleted;
			var isCompleted = taskCompletion.IsCompleted;
			var isSuccessfullyCompleted = taskCompletion.IsSuccessfullyCompleted;
			var exception = taskCompletion.Exception;
			var innerException = taskCompletion.InnerException;
			var taskCompleted = taskCompletion.TaskCompleted;
			var status = taskCompletion.Status;
			var isCanceled = taskCompletion.IsCanceled;
			var error = taskCompletion.ErrorMessage;
		}

#pragma warning restore 414
	}
}
