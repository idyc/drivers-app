﻿namespace iDriveYourCar.Core.PushNotifications
{
    public static class PushConstants
    {
        public const string APS = "aps";
        public const string ALERT = "alert";
        public const string TITLE = "title";
        public const string BODY = "body";
        public const string BADGE = "badge";
        public const string SOUND = "sound";

        public const string TYPE = "type";

        public const string DATA = "data";

        public const string CODE = "code";

        public const string TRIP = "trip";
        public const string DROPOFF_ADDRESS = "dropoff_address";
        public const string ESTIMATED_TIME = "estimated_time";
        public const string ID = "id";
        public const string PICKUP_ADDRESS = "pickup_address";
        public const string PICKUP_DATE = "pickup_date";
        public const string PICKUP_TIME = "pickup_time";
        public const string RETURN = "return";
        public const string TIME_FROM_HOME = "time_from_home";

        public const string TRIP_ID = "trip_id";
        public const string FIELDS_CHANGED = "fields_changed";

        public const string PUSH_HINT = "pushHint";

        public const string STAFF_MEMBER_ID = "staff_member_id";
    }
}
