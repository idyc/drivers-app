﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Handlers;
using DGenix.Mobile.Fwk.Core.Resources;
using iDriveYourCar.Core.ViewModels;
using MvvmCross.Platform.Platform;
using iDriveYourCar.Core.Models.Push;

namespace iDriveYourCar.Core.PushNotifications
{
    public class PushNotificationNavigator : FwkBaseNavigatingObject
    {
        private readonly IAlertHandler alertHandler;
        private readonly IMvxJsonConverter jsonConverter;

        public PushNotificationNavigator(
            IAlertHandler alertHandler,
            IMvxJsonConverter jsonConverter)
        {
            this.alertHandler = alertHandler;
            this.jsonConverter = jsonConverter;
        }

        public void Navigate(StartupHint hint)
        {
            this.alertHandler.ShowMessage(
                hint.Title,
                hint.Body,
                positiveButtonText: Strings.Ok,
                negativeButtonText: Strings.No,
                positiveCallback: () => this.PerformNavigationByHint(hint));
        }

        public void PerformNavigationByHint(StartupHint hint)
        {
            switch(hint.InitialScreen)
            {
                case InitialScreen.MyTrips:
                    this.ShowViewModelWithTrack<MyTripsViewModel>();
                    break;

                case InitialScreen.AcceptTrip:
                    var tripPushSerialized = this.jsonConverter.SerializeObject(hint.Hint);
                    this.ShowViewModelWithTrack<TripAcceptanceViewModel>(new { tripPushSerialized });
                    break;

                case InitialScreen.CurrentTripDetails:
                    var tripIdPush = this.jsonConverter.DeserializeObject<TripIdPush>((string)hint.Hint);

                    this.ShowViewModelWithTrack<CurrentTripDetailViewModel>(new { tripId = tripIdPush.TripId });
                    break;

                case InitialScreen.Referrals:
                    this.ShowViewModelWithTrack<InvitesViewModel>();
                    break;

                case InitialScreen.UpcomingTripDetails:
                    var tripId = this.jsonConverter.DeserializeObject<TripIdPush>((string)hint.Hint);

                    this.ShowViewModelWithTrack<UpcomingTripDetailViewModel>(new { tripId = tripId.TripId });
                    break;

                case InitialScreen.Profile:
                    this.ShowViewModelWithTrack<ProfileViewModel>();
                    break;

                case InitialScreen.UpcomingTripModificationsDetails:
                    var changesSerialized = this.jsonConverter.SerializeObject(hint.Hint);

                    this.ShowViewModelWithTrack<UpcomingTripModifiedDetailViewModel>(new { tripChangesSerialized = changesSerialized });
                    break;
            }
        }
    }
}
