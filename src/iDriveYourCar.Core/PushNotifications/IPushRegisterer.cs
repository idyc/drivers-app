﻿namespace iDriveYourCar.Core.PushNotifications
{
    public interface IPushNotificationRegisterer
    {
        void UnregisterForPushNotifications();

        string GetDeviceId();
    }
}
