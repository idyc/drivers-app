using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public interface IDriverProvider : IBaseProvider
    {
        Task<Driver> GetDriverDataAsync(DateTime startDate, DateTime endDate);

        Task<Driver> UpdateDriverDataAsync(Driver driver);

        Task<Address> UpdateDriverAddressAsync(Address address);

        Task<IPagedListResult<Review>> GetDriverReviewsAsync(ReviewCriteria criteria);

        Task UpdateDriverLocationAsync(Geoposition position);

        Task UpdateDriverLocationAsync(Geoposition position, long operatorId);

        Task UpdateDeviceIdAsync(string deviceId);

        Task<Driver> UpdateBankingInformationAsync(Driver driver);

        Task<Driver> CreateDriverAsync(RegisterDriver driver);
    }
}