﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IForgotPasswordProvider : IBaseProvider
	{
		Task RecoverPasswordDataAsync(string email);
	}
}
