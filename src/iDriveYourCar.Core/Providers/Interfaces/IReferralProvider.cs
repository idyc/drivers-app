﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public interface IReferralProvider : IBaseProvider
    {
        Task<Referrals> GetDriverReferralsAsync();

        Task RemindAsync(long invitationId);

        Task RedeemAsync(long invitationId);

        Task RedeemAsync();

        Task InviteEmailFriendsAsync(InvitationType type, IEnumerable<EmailInvitation> invitations);

        Task InviteFriendsManuallyAsync(InvitationType type, IEnumerable<string> emails);
    }
}
