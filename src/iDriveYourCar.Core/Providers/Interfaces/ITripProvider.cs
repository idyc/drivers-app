﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public interface ITripProvider : IBaseProvider
    {
        Task<IEnumerable<Trip>> GetOpenTripsAsync(OpenTripCriteria openTripCriteria);

        Task<Driver> AcceptTripAsync(long tripId);

        Task RejectTripAsync(long tripId);

        Task<Trip> PerformTripWorkflowAsync(long tripId, ActiveTripState toState);

        Task<Trip> SubmitTripAsync(long tripId, string notesForDriver);

        Task<Trip> AddExpenseAsync(long tripId, string description, decimal amount);

        Task<Trip> AddExpenseAsync(long tripId, Stream file, string fileName, string description, decimal amount);

        Task<Trip> DeleteExpenseAsync(long tripId, long expenseId);

        Task<Trip> AcceptTripChangesAsync(long tripId);
    }
}
