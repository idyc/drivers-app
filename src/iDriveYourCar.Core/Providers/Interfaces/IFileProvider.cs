﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;
using System.IO;

namespace iDriveYourCar.Core.Providers
{
	public interface IFileProvider : IBaseProvider
	{
		Task<Document> UploadDocumentAsync(Stream file, string fileName, DocumentType type);

		Task<Stream> GetDocumentAsync(Document document);

		Task<Driver> UploadAvatarAsync(Stream file, string fileName);
	}
}
