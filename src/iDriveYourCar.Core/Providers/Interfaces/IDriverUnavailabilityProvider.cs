﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
	public interface IDriverUnavailabilityProvider : IBaseProvider
	{
		Task<Driver> UpdateDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities, long? maxDrivingTime);

		Task<Driver> UpdateDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities);
	}
}
