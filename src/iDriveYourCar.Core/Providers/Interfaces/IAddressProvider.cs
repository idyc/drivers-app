﻿using System;
using DGenix.Mobile.Fwk.Core.Providers;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using System.Threading.Tasks;

namespace iDriveYourCar.Core.Providers
{
	public interface IAddressProvider : IBaseProvider
	{
		Task<IEnumerable<Prediction>> GetAddressPredictionsAsync(string input);

		Task<PlaceDetails> GetAddressDetailsAsync(string placeId);
	}
}
