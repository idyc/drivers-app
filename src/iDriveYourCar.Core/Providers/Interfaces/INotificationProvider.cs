﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Criteria;
using DGenix.Mobile.Fwk.Core.Providers;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public interface INotificationProvider : IBaseProvider
    {
        Task<IPagedListResult<Notification>> GetNotificationsAsync(NotificationCriteria criteria);

        Task RemoveNotificationAsync(long notificationId);
    }
}
