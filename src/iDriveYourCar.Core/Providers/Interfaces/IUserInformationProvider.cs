﻿using System;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IUserInformationProvider : IBaseProvider
	{
		Task<User> VerifyUserAsync(string username, string password);

		Task LogoutAsync();
	}
}
