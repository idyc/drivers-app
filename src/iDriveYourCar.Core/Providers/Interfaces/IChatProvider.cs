﻿using System;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;
using System.Threading.Tasks;

namespace iDriveYourCar.Core.Providers
{
    public interface IChatProvider : IBaseProvider
    {
        Task<ChatToken> GetTwilioChatToken(string deviceId);
    }
}
