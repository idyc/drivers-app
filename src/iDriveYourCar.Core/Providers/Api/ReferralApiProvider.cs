﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.Providers
{
    public class ReferralApiProvider : IDYCApiProvider, IReferralApiProvider
    {
        private readonly IReferralLocalProvider localProvider;

        public ReferralApiProvider(IApiServicesAdapter adapter, IReferralLocalProvider localProvider)
            : base(adapter)
        {
            this.localProvider = localProvider;
        }

        public async Task<Referrals> GetDriverReferralsAsync()
        {
            var referrals = await base.CallAsync<Referrals>(IDYCApiEndpoints.GetAllReferrals.Build()).ConfigureAwait(false);

            referrals = referrals ?? new Referrals() { Clients = new List<Client>(), Invitations = new List<DriverInvitation>() };

            await this.localProvider.PersistDriverReferralsAsync(referrals).ConfigureAwait(false);

            return referrals;
        }

        public async Task RemindAsync(long invitationId)
        {
            await base.CallAsync(IDYCApiEndpoints.ResendInvitation.Build(), () => new { invitation_id = invitationId }).ConfigureAwait(false);
        }

        public async Task RedeemAsync(long invitationId)
        {
            await base.CallAsync(IDYCApiEndpoints.RequestRedeem.Build(), () => new { invitation_id = invitationId, origin = InvitationType.Driver.GetCode() }).ConfigureAwait(false);
        }

        public async Task RedeemAsync()
        {
            await base.CallAsync(IDYCApiEndpoints.RequestRedeem.Build(), () => new { origin = InvitationType.Client.GetCode() }).ConfigureAwait(false);
        }

        public async Task InviteEmailFriendsAsync(InvitationType type, IEnumerable<EmailInvitation> invitations)
        {
            await base.CallAsync(
                IDYCApiEndpoints.InviteGmailFriends.Build(),
                () => new { type = type.GetCode(), invitations = invitations }
            ).ConfigureAwait(false);
        }

        public async Task InviteFriendsManuallyAsync(InvitationType type, IEnumerable<string> emails)
        {
            await base.CallAsync(
                IDYCApiEndpoints.InviteManuallyEmails.Build(),
                () => new { type = type.GetCode(), emails = emails }
            ).ConfigureAwait(false);
        }
    }
}
