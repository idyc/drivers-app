﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Repositories;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.Providers
{
    public class UserInformationApiProvider : IDYCApiProvider, IUserInformationApiProvider
    {
        private readonly IUserInformationLocalProvider localProvider;

        public UserInformationApiProvider(IApiServicesAdapter adapter, IUserInformationLocalProvider localProvider)
            : base(adapter)
        {
            this.localProvider = localProvider;
        }

        public Task LogoutAsync()
        {
            return this.localProvider.LogoutAsync();
        }

        public async Task<User> VerifyUserAsync(string username, string password)
        {
            var credentials = new { email = username, password = password };

            var userToken = await base.ApiAdapter.CallAsync<UserToken>(IDYCApiEndpoints.Login.Build(), new { credentials = credentials }).ConfigureAwait(false);

            await this.localProvider.PersistUserInformationAsync(userToken).ConfigureAwait(false);

            return userToken.User;
        }
    }
}