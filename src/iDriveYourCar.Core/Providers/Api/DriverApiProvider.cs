using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules;
using Plugin.DeviceInfo;

namespace iDriveYourCar.Core.Providers
{
    public class DriverApiProvider : IDYCApiProvider, IDriverApiProvider
    {
        private const string FIRST_NAME = "first_name";
        private const string LAST_NAME = "last_name";
        //private const string USERNAME = "username";
        private const string PASSWORD = "password";
        private const string EMAIL = "email";
        //private const string PHONE = "phone";
        //private const string HAS_VALID_DRIVER_LICENSE = "has_valid_driver_license";
        //private const string HAS_CLEAN_MOTOR_VEHICLE_RECORD = "has_clean_motor_vehicle_record";
        //private const string HAS_CLEAN_CRIMINAL_RECORD = "has_clean_criminal_record";
        private const string HAS_EXPERIENCE_AS_PROFESSIONAL_DRIVER = "has_experience_as_professional_driver";
        private const string EXPERIENCE_AS_PROFESSIONAL_DRIVER = "experience_as_professional_driver";
        private const string ADDITIONAL_QUALIFICATIONS_OR_LICENSES = "additional_qualifications_or_licenses";
        private const string ADDITIONAL_COMMENTS_ABOUT_SKILLS = "additional_comments_about_skills";
        private const string REF = "ref";
        private const string ADDRESS = "address";
        private const string CELL_PHONE = "cell_phone";
        private const string BIRTHDATE = "birthdate";
        //private const string BUSSINES_PHONE = "bussines_phone";
        //private const string HOME_PHONE = "home_phone";
        //private const string BUSSINES_EMAIL = "bussines_email";
        //private const string PERSONAL_EMAIL = "personal_email";
        private const string AVATAR = "avatar";
        private const string DOCS_0_FILE_NAME = "docs[0][file_name]";
        private const string DOCS_0_DOCUMENT_TYPE = "docs[0][document_type]";
        private const string DOCS_1_FILE_NAME = "docs[1][file_name]";
        private const string DOCS_1_DOCUMENT_TYPE = "docs[1][document_type]";
        private const string ADDRESS_NAME = "address[name]";
        private const string ADDRESS_REGULAR = "address[regular]";
        private const string ADDRESS_PRIMARY = "address[primary]";
        private const string ADDRESS_POSTAL_CODE = "address[postal_code]";
        private const string ADDRESS_CITY = "address[city]";
        private const string ADDRESS_COUNTY = "address[county]";
        private const string ADDRESS_STATE = "address[state]";
        private const string ADDRESS_RAW = "address[raw]";
        private const string ADDRESS_LAT = "address[lat]";
        private const string ADDRESS_LNG = "address[lng]";
        private const string ADDRESS_GOOGLE_PLACE_ID = "address[google_place_id]";

        private readonly IDriverLocalProvider localProvider;
        private readonly ITripWorkflowManager tripWorkflowManager;

        public DriverApiProvider(IApiServicesAdapter adapter, IDriverLocalProvider localProvider, ITripWorkflowManager tripWorkflowManager)
            : base(adapter)
        {
            this.localProvider = localProvider;
            this.tripWorkflowManager = tripWorkflowManager;
        }

        public async Task<Driver> GetDriverDataAsync(DateTime startDate, DateTime endDate)
        {
            var driverData = await base.CallAsync<Driver>(
                IDYCApiEndpoints.GetDriverData.Build(),
                () => new
                {
                    start_date = startDate.ToFormattedString(DateStringFormat.ShortDateMonthYear),
                    end_date = endDate.ToFormattedString(DateStringFormat.ShortDateMonthYear)
                }).ConfigureAwait(false);

            driverData.FormatTrips();

            var currentDriverData = await this.localProvider.GetDriverDataAsync().ConfigureAwait(false);

            if (currentDriverData != null)
                this.MergeDriverData(currentDriverData, driverData);

            await this.localProvider.PersistDriverDataAsync(driverData).ConfigureAwait(false);

            return driverData;
        }

        public async Task<Driver> UpdateDriverDataAsync(Driver driver)
        {
            var updatedDriver = await base.CallAsync<Driver>(
                IDYCApiEndpoints.EditDriverInfo.Build(),
                () => new
                {
                    first_name = driver.FirstName,
                    last_name = driver.LastName,
                    email = driver.User.Email,
                    phone = driver.User.Phone,
                    personal_email = driver.PersonalEmail,
                    bussines_email = driver.BusinessEmail,
                    home_phone = driver.HomePhone,
                    cell_phone = driver.CellPhone,
                    bussines_phone = driver.BusinessPhone,
                    fax_number = driver.FaxNumber,
                    birthdate = driver.Birthday
                }
            ).ConfigureAwait(false);

            await this.localProvider.PersistDriverDataAsync(updatedDriver).ConfigureAwait(false);

            return updatedDriver;
        }

        public async Task<IPagedListResult<Review>> GetDriverReviewsAsync(ReviewCriteria criteria)
        {
            var driverReviews = await base.CallAsync<IEnumerable<Review>>(
                IDYCApiEndpoints.GetDriverReviews.Build(),
                () => new
                {
                    page = criteria.Page,
                    results = criteria.Results
                }).ConfigureAwait(false);

            driverReviews = driverReviews ?? new List<Review>();

            driverReviews.OrderByDescending(r => r.Date.Value);

            if (criteria.Page == 1)
                await this.localProvider.PersistDriverReviewsAsync(driverReviews).ConfigureAwait(false);

            return new PagedListResultDto<Review>
            {
                Result = driverReviews,
                HasNextPage = driverReviews.Any()
            };
        }

        public async Task UpdateDriverLocationAsync(Geoposition position)
        {
            var updatedDriver = await base.CallAsync<User>(IDYCApiEndpoints.SendLocation.Build(),
                () => new
                {
                    lat = position.Latitude,
                    lng = position.Longitude
                }).ConfigureAwait(false);
        }

        public async Task UpdateDriverLocationAsync(Geoposition position, long operatorId)
        {
            var updatedDriver = await base.CallAsync<User>(
                IDYCApiEndpoints.SendLocation.Build(),
                () => new
                {
                    lat = position.Latitude,
                    lng = position.Longitude
                },
                () => new string[] { operatorId.ToString() }
            ).ConfigureAwait(false);
        }

        public async Task UpdateDeviceIdAsync(string deviceId)
        {
            await base.CallAsync<User>(IDYCApiEndpoints.SendDeviceId.Build(),
                () => new
                {
                    device_token = deviceId,
                    type = CrossDeviceInfo.Current.Platform.ToString().ToUpper()
                }
            ).ConfigureAwait(false);
        }

        public async Task<Address> UpdateDriverAddressAsync(Address address)
        {
            var updatedAddress = await base.CallAsync<Address>(
                IDYCApiEndpoints.UpdateAddress.Build(),
                () => new
                {
                    raw = address.Raw,
                    lat = address.Lat,
                    lng = address.Lng,
                    place_id = address.GooglePlaceId,
                    city = address.City,
                    county = address.Country,
                    state = address.State,
                    postal_code = address.PostalCode,
                    regular = address.Regular ? "1" : "0",
                    primary = address.Primary ? "1" : "0"
                }).ConfigureAwait(false);

            var driver = await this.localProvider.GetDriverDataAsync().ConfigureAwait(false);

            driver.User.PrimaryAddress = updatedAddress;

            await this.localProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return updatedAddress;
        }

        public async Task<Driver> UpdateBankingInformationAsync(Driver driver)
        {
            var updatedDriver = await base.CallAsync<Driver>(
                IDYCApiEndpoints.UpdateBankingInfo.Build(),
                () => new
                {
                    name_bank_account = driver.NameBankAccount,
                    tax_id = EnumExtensions.GetCodeFromEnum(driver.TaxId),
                    routing_number = driver.RoutingNumber,
                    account_number = driver.AccountNumber,
                    ssn_tin = driver.SSNTIN
                }
            ).ConfigureAwait(false);

            await this.localProvider.PersistDriverDataAsync(updatedDriver).ConfigureAwait(false);

            return updatedDriver;
        }

        public async Task<Driver> CreateDriverAsync(RegisterDriver driver)
        {
            if (driver.Avatar != null)
                driver.Avatar.FileKey = AVATAR;

            if (driver.VehicleInsurance != null)
                driver.VehicleInsurance.FileKey = DOCS_0_FILE_NAME;

            if (driver.DriverLicense != null)
                driver.DriverLicense.FileKey = DOCS_1_FILE_NAME;

            var fileRequests = new List<StreamFileRequest>();

            if (driver.Avatar != null)
                fileRequests.Add(driver.Avatar);

            if (driver.DriverLicense != null)
                fileRequests.Add(driver.DriverLicense);

            if (driver.VehicleInsurance != null)
                fileRequests.Add(driver.VehicleInsurance);

            var newDriver = await base.UploadFilesAsync<Driver>(
                IDYCApiEndpoints.CreateDriver.Build(),
                fileRequests,
                () =>
            {
                var dictionary = new Dictionary<string, string>
                {
                    [FIRST_NAME] = driver.FirstName,
                    [LAST_NAME] = driver.LastName,
                    [PASSWORD] = driver.Password,
                    [EMAIL] = driver.User.Email,
                    [CELL_PHONE] = driver.CellPhone,
                    [HAS_EXPERIENCE_AS_PROFESSIONAL_DRIVER] = driver.HasExperienceAsProfessionalDriver.ToString(),
                    [ADDITIONAL_COMMENTS_ABOUT_SKILLS] = driver.HowDoCreateAnAmazingExperienceForYourClientsExplanation,
                    [REF] = driver.OptionalInviteCode
                };

                if (driver.HasExperienceAsProfessionalDriver)
                    dictionary.Add(EXPERIENCE_AS_PROFESSIONAL_DRIVER, driver.HaveYouEverServedAsProfessionalExplanation);

                if (driver.Birthday != null)
                    dictionary.Add(BIRTHDATE, driver.Birthday.ToString());

                if (driver.DriverLicense != null)
                    dictionary.Add(DOCS_0_DOCUMENT_TYPE, DocumentType.DriverLicense.GetCode());

                if (driver.VehicleInsurance != null)
                {
                    if (driver.DriverLicense != null)
                        dictionary.Add(DOCS_1_DOCUMENT_TYPE, DocumentType.VehicleInsurance.GetCode());
                    else
                        dictionary.Add(DOCS_0_DOCUMENT_TYPE, DocumentType.VehicleInsurance.GetCode());
                }

                if (driver.User.PrimaryAddress != null)
                {
                    dictionary.Add(ADDRESS_NAME, driver.User.PrimaryAddress.Name);
                    dictionary.Add(ADDRESS_REGULAR, driver.User.PrimaryAddress.Regular ? "1" : "0");
                    dictionary.Add(ADDRESS_PRIMARY, driver.User.PrimaryAddress.Primary ? "1" : "0");
                    dictionary.Add(ADDRESS_POSTAL_CODE, driver.User.PrimaryAddress.PostalCode);
                    dictionary.Add(ADDRESS_CITY, driver.User.PrimaryAddress.City);
                    dictionary.Add(ADDRESS_COUNTY, driver.User.PrimaryAddress.Country);
                    dictionary.Add(ADDRESS_STATE, driver.User.PrimaryAddress.State);
                    dictionary.Add(ADDRESS_RAW, driver.User.PrimaryAddress.Raw);
                    dictionary.Add(ADDRESS_LAT, driver.User.PrimaryAddress.Lat.ToString());
                    dictionary.Add(ADDRESS_LNG, driver.User.PrimaryAddress.Lng.ToString());
                    dictionary.Add(ADDRESS_GOOGLE_PLACE_ID, driver.User.PrimaryAddress.GooglePlaceId);
                }

                return dictionary;
            }).ConfigureAwait(false);


            //driver is not stored currently
            //await this.localProvider.PersistDriverDataAsync(newDriver).ConfigureAwait(false);

            return newDriver;
        }

        private void MergeDriverData(Driver currentDriverData, Driver driverData)
        {
            foreach (var upcomingTrip in driverData.UpcomingTrips)
            {
                var oldTrip = currentDriverData.UpcomingTrips.FirstOrDefault(t => t.Id == upcomingTrip.Id);
                if (oldTrip != null)
                {
                    // if trip exists locally
                    upcomingTrip.ActiveTripState = oldTrip.ActiveTripState;
                }

                // if the trip is initiated but locally is stated as NotStarted:
                if (upcomingTrip.ActiveTripState == ActiveTripState.NotStarted && upcomingTrip.Status?.StatusType == TripStatusType.ActiveTrip)
                    upcomingTrip.ActiveTripState = this.tripWorkflowManager.GetFollowingTripState(upcomingTrip.TripType, ActiveTripState.NotStarted);

                //if the trip is finished mark it as finished
                if (upcomingTrip.Status?.StatusType == TripStatusType.Final)
                    upcomingTrip.ActiveTripState = ActiveTripState.SubmitTrip;
            }
        }
    }
}
