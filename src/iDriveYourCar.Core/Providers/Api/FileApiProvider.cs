﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Repositories;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.Providers
{
    public class FileApiProvider : IDYCApiProvider, IFileApiProvider
    {
        private const string ACCESS_TOKEN_KEY = "access_token";
        private const string AVATAR_KEY = "avatar";
        private const string FILE_NAME_KEY = "file_name";
        private const string DOCUMENT_TYPE_KEY = "document_type";

        private readonly IDriverLocalProvider driverLocalProvider;
        private readonly IFileLocalProvider fileLocalProvider;
        private readonly IUserSettings userSettings;

        public FileApiProvider(
            IApiServicesAdapter adapter,
            IUserSettings userSettings,
            IDriverLocalProvider driverLocalProvider,
            IFileLocalProvider fileLocalProvider)
            : base(adapter)
        {
            this.driverLocalProvider = driverLocalProvider;
            this.fileLocalProvider = fileLocalProvider;
            this.userSettings = userSettings;
        }

        public async Task<Driver> UploadAvatarAsync(Stream file, string fileName)
        {

            var stringContent = new Dictionary<string, string>
            {
                [ACCESS_TOKEN_KEY] = this.userSettings.Token
            };

            var user = await this.UploadFileAsync<User>(
                IDYCApiEndpoints.UploadAvatar.Build(),
                new StreamFileRequest(file, fileName, AVATAR_KEY),
                () => stringContent
            ).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            driver.User = user;

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return driver;
        }

        public async Task<Document> UploadDocumentAsync(Stream file, string fileName, DocumentType type)
        {
            var stringContent = new Dictionary<string, string>
            {
                [ACCESS_TOKEN_KEY] = this.userSettings.Token,
                [DOCUMENT_TYPE_KEY] = EnumExtensions.GetCodeFromEnum(type)
            };

            var document = await this.UploadFileAsync<Document>(
                IDYCApiEndpoints.SendDocument.Build(),
                new StreamFileRequest(file, fileName, FILE_NAME_KEY),
                () => stringContent).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            driver.Documents.Add(document);

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return document;
        }

        public async Task<Stream> GetDocumentAsync(Document document)
        {
            Stream documentStream = await this.fileLocalProvider.GetDocumentAsync(document).ConfigureAwait(false);

            if (documentStream == null || documentStream == Stream.Null)
            {
                documentStream = await base.DownloadFileStreamAsync(
                    IDYCApiEndpoints.GetDocument.Build(),
                    () => new { file_id = document.Id, access_token = this.userSettings.Token }).ConfigureAwait(false);

                // if document was successfully stored, reload it from cache
                if (await this.fileLocalProvider.PersistDocumentAsync(documentStream, document.Id.ToString()))
                    documentStream = await this.fileLocalProvider.GetDocumentAsync(document).ConfigureAwait(false);
            }

            return documentStream;
        }
    }
}