﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.Providers
{
    public class ChatApiProvider : IDYCApiProvider, IChatApiProvider
    {
        private readonly IChatLocalProvider localProvider;

        public ChatApiProvider(
            IApiServicesAdapter adapter,
            IChatLocalProvider localProvider)
            : base(adapter)
        {
            this.localProvider = localProvider;
        }

        public async Task<ChatToken> GetTwilioChatToken(string deviceId)
        {
            var chatToken = await base.CallAsync<ChatToken>(
                IDYCApiEndpoints.GetTwilioChatToken.Build(),
                () => new
                {
                    device_id = deviceId
                }).ConfigureAwait(false);

            await this.localProvider.PersistChatTokenAsync(chatToken).ConfigureAwait(false);

            return chatToken;
        }
    }
}
