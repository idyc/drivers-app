﻿using System;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IFileApiProvider : IApiBaseProvider, IFileProvider
	{
	}
}
