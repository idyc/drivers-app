﻿using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IDriverUnavailabilityApiProvider : IApiBaseProvider, IDriverUnavailabilityProvider
	{
	}
}
