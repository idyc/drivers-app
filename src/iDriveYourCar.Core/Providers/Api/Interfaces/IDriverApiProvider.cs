﻿using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IDriverApiProvider : IApiBaseProvider, IDriverProvider
	{

	}
}
