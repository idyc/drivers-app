﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Rules;

namespace iDriveYourCar.Core.Providers
{
    public class TripApiProvider : IDYCApiProvider, ITripApiProvider
    {
        private const string DESCRIPTION_KEY = "description";
        private const string AMOUNT_KEY = "amount";
        private const string EXPENSE_DOC_KEY = "file";

        private readonly IDriverLocalProvider driverLocalProvider;

        public TripApiProvider(
            IApiServicesAdapter adapter,
            IDriverLocalProvider driverLocalProvider)
            : base(adapter)
        {
            this.driverLocalProvider = driverLocalProvider;
        }

        public async Task<Driver> AcceptTripAsync(long tripId)
        {
            var acceptedTrip = await base.CallAsync<Trip>(
                IDYCApiEndpoints.AcceptTrip.Build(),
                queryStringIds: () => new[] { tripId.ToString() }
            ).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            driver.UpcomingTrips.Add(acceptedTrip);
            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return driver;
        }

        public async Task RejectTripAsync(long tripId)
        {
            await base.CallAsync(
                IDYCApiEndpoints.RejectTrip.Build(),
                queryStringIds: () => new[] { tripId.ToString() }
            ).ConfigureAwait(false);
        }

        public async Task<Trip> AddExpenseAsync(long tripId, string description, decimal amount)
        {
            var createdExpense = await this.CallAsync<Expense>(
                IDYCApiEndpoints.AddExpense.Build(),
                () => new Dictionary<string, string>
                {
                    [DESCRIPTION_KEY] = description,
                    [AMOUNT_KEY] = amount.ToString()
                },
                () => new[] { tripId.ToString() }).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            var trip = driver.UpcomingTrips.First(t => t.Id == tripId);
            trip.Expenses.Add(createdExpense);

            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return trip;
        }

        public async Task<Trip> AddExpenseAsync(long tripId, Stream file, string fileName, string description, decimal amount)
        {
            var createdExpense = await this.UploadFileAsync<Expense>(
                IDYCApiEndpoints.AddExpense.Build(),
                new StreamFileRequest(file, fileName, EXPENSE_DOC_KEY),
                () => new Dictionary<string, string>
                {
                    [DESCRIPTION_KEY] = description,
                    [AMOUNT_KEY] = amount.ToString()
                },
                () => new[] { tripId.ToString() }
            ).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            var trip = driver.UpcomingTrips.First(t => t.Id == tripId);
            trip.Expenses.Add(createdExpense);

            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return trip;
        }

        public async Task<IEnumerable<Trip>> GetOpenTripsAsync(OpenTripCriteria openTripCriteria)
        {
            var tripsData = await base.CallAsync<IEnumerable<Trip>>(
                IDYCApiEndpoints.FindOpenTrips.Build(),
                () => new
                {
                    lat = openTripCriteria.Latitude,
                    lng = openTripCriteria.Longitude,
                    max_driving_time = openTripCriteria.MaxDrivingTime,
                    start_date = openTripCriteria.StartDate.ToFormattedString(DateStringFormat.ShortDateMonthYear),
                    end_date = openTripCriteria.EndDate.ToFormattedString(DateStringFormat.ShortDateMonthYear)
                }).ConfigureAwait(false);

            tripsData = tripsData ?? new List<Trip>();

            return tripsData;
        }

        public async Task<Trip> PerformTripWorkflowAsync(long tripId, ActiveTripState toState)
        {
            var tripUpdated = await base.CallAsync<Trip>(
                this.GetEndpointForState(toState).Build(),
                queryStringIds: () => new[] { tripId.ToString() }).ConfigureAwait(false);

            tripUpdated.ActiveTripState = toState;

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            driver.UpcomingTrips.Replace(tripUpdated);
            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return tripUpdated;
        }

        public async Task<Trip> SubmitTripAsync(long tripId, string notesForDriver)
        {
            var tripUpdated = await base.CallAsync<Trip>(
                this.GetEndpointForState(ActiveTripState.SubmitTrip).Build(),
                () => new
                {
                    notes_for_drivers = notesForDriver
                },
                () => new[] { tripId.ToString() }).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            // now that the trip is done, remove it from upcoming ones
            driver.UpcomingTrips.ReplaceIfExists(tripUpdated, trip => trip.Id == tripUpdated.Id);

            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return tripUpdated;
        }

        public async Task<Trip> DeleteExpenseAsync(long tripId, long expenseId)
        {
            var updatedTrip = await base.CallAsync<Trip>(
                IDYCApiEndpoints.DeleteExpense.Build(),
                queryStringIds: () => new[] { tripId.ToString(), expenseId.ToString() }
            ).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            driver.UpcomingTrips.ReplaceIfExists(updatedTrip, oldTrip => oldTrip.Id == updatedTrip.Id);

            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return updatedTrip;
        }

        public async Task<Trip> AcceptTripChangesAsync(long tripId)
        {
            var tripUpdated = await base.CallAsync<Trip>(
                IDYCApiEndpoints.AcceptTripChanges.Build(),
                () => new[] { tripId.ToString() }).ConfigureAwait(false);

            var driver = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

            // now that the trip is done, remove it from upcoming ones
            driver.UpcomingTrips.Remove(tripUpdated);
            //... and add it to the finished trips collection
            driver.FinishedTrips.Add(tripUpdated);

            driver.FormatTrips();

            await this.driverLocalProvider.PersistDriverDataAsync(driver).ConfigureAwait(false);

            return tripUpdated;
        }

        private IDYCApiEndpoints GetEndpointForState(ActiveTripState toState)
        {
            switch(toState)
            {
                case ActiveTripState.NavigateToPickup:
                case ActiveTripState.NavigateToStartPoint:
                case ActiveTripState.NavigateToVehicle:
                    return IDYCApiEndpoints.CheckInTrip;

                case ActiveTripState.ArrivedAtPickup:
                    return IDYCApiEndpoints.ArrivedTrip;

                case ActiveTripState.StartTrip:
                    return IDYCApiEndpoints.ActiveTrip;

                case ActiveTripState.ArrivedAtDropoff:
                    return IDYCApiEndpoints.ClientDroppedOff;

                case ActiveTripState.EndTrip:
                    return IDYCApiEndpoints.SetDropoff;

                case ActiveTripState.SubmitTrip:
                    return IDYCApiEndpoints.CompleteTrip;

                default:
                    throw new NotImplementedException($"Endpoint not implemented for state {toState}");
            }
        }
    }
}
