using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.GooglePlacesAPI;

namespace iDriveYourCar.Core.Providers
{
    public class AddressApiProvider : IDYCApiProvider, IAddressApiProvider
    {
        public AddressApiProvider(IApiServicesAdapter adapter) : base(adapter)
        {
        }

        public async Task<PlaceDetails> GetAddressDetailsAsync(string placeId)
        {
            var result = await GPlacesAPIConnector.Current.GetPlaceDetailsAsync("AIzaSyCK6dhqiAW9haCfGgOro5N5-zeqGlidviI", placeId).ConfigureAwait(false);

            return result.Result;
        }

        public async Task<IEnumerable<Prediction>> GetAddressPredictionsAsync(string input)
        {
            var parameters = new Dictionary<string, string>
            {
                ["types"] = "address"
            };

            var result = await GPlacesAPIConnector.Current.GetAutocompletePredictionsAsync("AIzaSyCK6dhqiAW9haCfGgOro5N5-zeqGlidviI", input, parameters).ConfigureAwait(false);

            return result.Predictions;
        }
    }
}
