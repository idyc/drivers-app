﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using System.Linq;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.Providers
{
    public class NotificationApiProvider : IDYCApiProvider, INotificationApiProvider
    {
        private readonly INotificationLocalProvider localProvider;

        public NotificationApiProvider(
            IApiServicesAdapter adapter,
            INotificationLocalProvider localProvider)
            : base(adapter)
        {
            this.localProvider = localProvider;
        }

        public async Task<IPagedListResult<Notification>> GetNotificationsAsync(NotificationCriteria criteria)
        {
            var notifications = await base.CallAsync<IEnumerable<Notification>>(
                IDYCApiEndpoints.GetNotifications.Build(),
                () => new
                {
                    page = criteria.Page,
                    results = criteria.Results
                }).ConfigureAwait(false);

            notifications = notifications ?? new List<Notification>();

            notifications.OrderByDescending(n => n.Date.ToDateTimeFromFormat());

            if(criteria.Page == 1)
                await this.localProvider.PersistNotificationsAsync(notifications).ConfigureAwait(false);

            return new PagedListResultDto<Notification>
            {
                Result = notifications,
                HasNextPage = notifications.Any()
            };
        }

        public async Task RemoveNotificationAsync(long notificationId)
        {
            await base.CallAsync(
                IDYCApiEndpoints.RemoveNotification.Build(),
                queryStringIds: () => new[] { notificationId.ToString() }
            ).ConfigureAwait(false);
        }
    }
}
