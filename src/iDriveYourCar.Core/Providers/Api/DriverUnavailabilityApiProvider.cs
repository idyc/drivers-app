﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Support.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public class DriverUnavailabilityApiProvider : IDYCApiProvider, IDriverUnavailabilityApiProvider
    {
        private readonly IDriverUnavailabilityLocalProvider localProvider;

        public DriverUnavailabilityApiProvider(IApiServicesAdapter adapter, IDriverUnavailabilityLocalProvider localProvider)
            : base(adapter)
        {
            this.localProvider = localProvider;
        }

        public async Task<Driver> UpdateDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities)
        {
            var items = new List<dynamic>();
            foreach(var unav in driverUnavailabilities)
                items.Add(new
                {
                    start_date = unav.StartDate.ToFormattedString(DateStringFormat.ShortDateMonthYear),
                    end_date = unav.EndDate.ToFormattedString(DateStringFormat.ShortDateMonthYear),
                    id = unav.Id != default(long) ? unav.Id : (long?)null
                });

            var updatedDriverUnavailabilities = await base.CallAsync<DriverUnavailability>(
                IDYCApiEndpoints.UpdateDriverUnavailability.Build(),
                () => new
                {
                    unavailability = items
                }).ConfigureAwait(false);

            // Note: Currently the endpoint returns the latest added item, therefore the result can be discarded
            //updatedDriverUnavailabilities = updatedDriverUnavailabilities ?? new List<DriverUnavailability>();

            return await this.localProvider.PersistDriverUnavailabilityAsync(driverUnavailabilities).ConfigureAwait(false);
        }

        public async Task<Driver> UpdateDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities, long? maxDrivingTime)
        {
            var items = new List<dynamic>();
            foreach(var unav in driverUnavailabilities)
                items.Add(new { day = unav.Day, time_start = unav.TimeStart.ToShortTimeString(), time_stop = unav.TimeStop.ToShortTimeString(), id = unav.Id != default(long) ? unav.Id : (long?)null });

            var updatedDriverUnavailabilities = await base.CallAsync<IEnumerable<DriverUnavailabilityWeek>>(
                IDYCApiEndpoints.UpdateDriverUnavailabilityWeek.Build(),
                () => new
                {
                    max_driving_time = maxDrivingTime,
                    unavailability_week = items
                }).ConfigureAwait(false);

            updatedDriverUnavailabilities = updatedDriverUnavailabilities ?? new List<DriverUnavailabilityWeek>();

            return await this.localProvider.PersistDriverWeeklyUnavailabilityAsync(updatedDriverUnavailabilities).ConfigureAwait(false);
        }
    }
}
