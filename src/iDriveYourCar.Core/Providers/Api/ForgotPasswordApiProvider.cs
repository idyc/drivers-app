﻿using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Repositories;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;

namespace iDriveYourCar.Core.Providers
{
    public class ForgotPasswordApiProvider : IDYCApiProvider, IForgotPasswordApiProvider
    {
        public ForgotPasswordApiProvider(IApiServicesAdapter adapter)
            : base(adapter)
        {

        }

        public async Task RecoverPasswordDataAsync(string email)
        {
            await base.CallAsync(IDYCApiEndpoints.ForgotPassword.Build(), () => new { email = email }).ConfigureAwait(false);
        }
    }
}
