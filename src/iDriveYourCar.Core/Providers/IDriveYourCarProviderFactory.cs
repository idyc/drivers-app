﻿using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
    public class IDriveYourCarProviderFactory : BaseProviderFactory
    {
        [Preserve]
        public IDriveYourCarProviderFactory(IConnectivityHelper connectivityHelper)
            : base(connectivityHelper)
        {
        }

        public override T Create<T>(bool cacheProvider = false)
        {
            //#if MOCK
            //			return (T)Activator.CreateInstance(this._mocksByProviderType[typeof(T)]);
            //#else
            return base.Create<T>(cacheProvider);
            //#endif
        }

        protected override void AddProvidersByConnectionType()
        {
            this.AddProviderByConnectionType<IUserInformationProvider, IUserInformationApiProvider, IUserInformationLocalProvider>();
            this.AddProviderByConnectionType<IDriverProvider, IDriverApiProvider, IDriverLocalProvider>();
            this.AddProviderByConnectionType<IFileProvider, IFileApiProvider, IFileLocalProvider>();
            this.AddProviderByConnectionType<ITripProvider, ITripApiProvider, ITripLocalProvider>();
            this.AddProviderByConnectionType<IReferralProvider, IReferralApiProvider, IReferralLocalProvider>();
            this.AddProviderByConnectionType<IForgotPasswordProvider, IForgotPasswordApiProvider, IForgotPasswordLocalProvider>();
            this.AddProviderByConnectionType<IDriverUnavailabilityProvider, IDriverUnavailabilityApiProvider, IDriverUnavailabilityLocalProvider>();
            this.AddProviderByConnectionType<IAddressProvider, IAddressApiProvider, IAddressLocalProvider>();
            this.AddProviderByConnectionType<IChatProvider, IChatApiProvider, IChatLocalProvider>();
            this.AddProviderByConnectionType<INotificationProvider, INotificationApiProvider, INotificationLocalProvider>();
        }
    }
}
