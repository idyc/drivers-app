﻿using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Repositories;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Repositories;

namespace iDriveYourCar.Core.Providers
{
    public class UserInformationLocalProvider : IDYCLocalProvider, IUserInformationLocalProvider
    {
        private readonly IUserSettings userSettings;
        private readonly IIDYCSettings idycSettings;
        private readonly IUserInformationRepository userInformationRepository;

        public UserInformationLocalProvider(
            IUserSettings userSettings,
            IIDYCSettings idycSettings,
            IUserInformationRepository userInformationRepository)
        {
            this.userSettings = userSettings;
            this.idycSettings = idycSettings;
            this.userInformationRepository = userInformationRepository;
        }

        public async Task PersistUserInformationAsync(UserToken userToken)
        {
            await this.userInformationRepository.CreateOrUpdateAsync(userToken.User).ConfigureAwait(false);

            this.userSettings.SetUserValues(userToken.AccessToken, userToken.User.Id);
        }

        public Task<User> VerifyUserAsync(string username, string password)
        {
            throw new NoInternetException();
        }

        public async Task LogoutAsync()
        {
            this.userSettings.CleanUserValues();

            this.idycSettings.CookieToken = string.Empty;
            this.idycSettings.DriverId = default(long);
            this.idycSettings.ChatIdentity = null;
            this.idycSettings.ChatToken = null;
            this.idycSettings.HasBankInfo = false;
            this.idycSettings.PushNotificationsCounter = 0;
            this.idycSettings.UserImagePath = null;

            await this.userInformationRepository.ClearDatabaseAsync().ConfigureAwait(false);
        }
    }
}
