﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Repositories;
using iDriveYourCar.Core.Rules;

namespace iDriveYourCar.Core.Providers
{
    public class DriverLocalProvider : IDYCLocalProvider, IDriverLocalProvider
    {
        private readonly IDriverRepository driverRepository;
        private readonly IReviewRepository reviewRepository;
        private readonly IIDYCSettings idycSettings;

        public DriverLocalProvider(
            IDriverRepository driverRepository,
            IReviewRepository reviewRepository,
            IIDYCSettings idycSettings)
        {
            this.driverRepository = driverRepository;
            this.reviewRepository = reviewRepository;
            this.idycSettings = idycSettings;
        }

        public async Task PersistDriverDataAsync(Driver driverData)
        {
            this.idycSettings.DriverId = driverData.Id;

            await this.driverRepository.CreateOrUpdateAsync(driverData).ConfigureAwait(false);
        }

        public async Task<Driver> GetDriverDataAsync()
        {
            Driver driver = null;

            if(this.idycSettings.DriverId != default(long))
                driver = await this.driverRepository.GetCurrentDriverAsync(this.idycSettings.DriverId).ConfigureAwait(false);

            driver?.FormatTrips();

            return driver;
        }

        public Task<Driver> GetDriverDataAsync(DateTime startDate, DateTime endDate)
        {
            return this.GetDriverDataAsync();
        }

        public Task<Driver> UpdateDriverDataAsync(Driver driver)
        {
            throw new NoInternetException();
        }

        public Task PersistDriverReviewsAsync(IEnumerable<Review> driverReviews)
        {
            return this.reviewRepository.PersistManyAsync(driverReviews);
        }

        public Task UpdateDriverLocationAsync(Geoposition position)
        {
            throw new DoNothingException();
        }

        public Task UpdateDeviceIdAsync(string deviceId)
        {
            throw new DoNothingException();
        }

        public Task<Address> UpdateDriverAddressAsync(Address address)
        {
            throw new NoInternetException();
        }

        public Task<Driver> UpdateBankingInformationAsync(Driver driver)
        {
            throw new NoInternetException();
        }

        public Task<Driver> CreateDriverAsync(RegisterDriver driver)
        {
            throw new NoInternetException();
        }

        public Task UpdateDriverLocationAsync(Geoposition position, long operatorId)
        {
            throw new DoNothingException();
        }

        public async Task<IPagedListResult<Review>> GetDriverReviewsAsync(ReviewCriteria criteria)
        {
            var reviews = await this.reviewRepository.GetAllAsync().ConfigureAwait(false);

            reviews.OrderBy(n => n.Date.Value);

            return new PagedListResultDto<Review>
            {
                Result = reviews,
                HasNextPage = true
            };
        }
    }
}
