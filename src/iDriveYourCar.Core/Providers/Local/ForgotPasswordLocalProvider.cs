﻿using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public class ForgotPasswordLocalProvider : IDYCLocalProvider, IForgotPasswordLocalProvider
	{
		public Task RecoverPasswordDataAsync(string email)
		{
			throw new NoInternetException();
		}
	}
}
