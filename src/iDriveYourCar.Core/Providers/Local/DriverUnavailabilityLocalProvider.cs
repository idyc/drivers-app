﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
	public class DriverUnavailabilityLocalProvider : IDYCLocalProvider, IDriverUnavailabilityLocalProvider
	{
		private readonly IDriverLocalProvider driverLocalProvider;

		public DriverUnavailabilityLocalProvider(IDriverLocalProvider driverLocalProvider)
		{
			this.driverLocalProvider = driverLocalProvider;
		}

		public async Task<Driver> PersistDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities)
		{
			var driverData = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

			driverData.Unavailability = driverUnavailabilities.ToList();

			await this.driverLocalProvider.PersistDriverDataAsync(driverData).ConfigureAwait(false);

			return driverData;
		}

		public async Task<Driver> PersistDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities)
		{
			var driverData = await this.driverLocalProvider.GetDriverDataAsync().ConfigureAwait(false);

			driverData.WeeklyUnavailability = driverUnavailabilities.ToList();

			await this.driverLocalProvider.PersistDriverDataAsync(driverData).ConfigureAwait(false);

			return driverData;
		}

		public Task<Driver> UpdateDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities)
		{
			throw new NoInternetException();
		}

		public Task<Driver> UpdateDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities, long? maxDrivingTime)
		{
			throw new NoInternetException();
		}
	}
}
