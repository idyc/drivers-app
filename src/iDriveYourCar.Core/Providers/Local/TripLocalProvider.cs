﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Repositories;

namespace iDriveYourCar.Core.Providers
{
    public class TripLocalProvider : IDYCLocalProvider, ITripLocalProvider
    {
        private readonly ITripRepository tripsRepository;

        public TripLocalProvider(
            ITripRepository tripsRepository)
        {
            this.tripsRepository = tripsRepository;
        }

        public async Task PersistTripsDataAsync(IEnumerable<Trip> tripsData)
        {
            foreach(var trip in tripsData)
                await this.tripsRepository.CreateOrUpdateAsync(trip).ConfigureAwait(false);
        }

        public Task<IEnumerable<Trip>> GetOpenTripsAsync(OpenTripCriteria openTripCriteria)
        {
            throw new NoInternetException();
        }

        public Task<Driver> AcceptTripAsync(long tripId)
        {
            throw new NoInternetException();
        }

        public Task<Trip> PerformTripWorkflowAsync(long tripId, ActiveTripState toState)
        {
            throw new NoInternetException();
        }

        public Task<Trip> AddExpenseAsync(long tripId, string description, decimal amount)
        {
            throw new NoInternetException();
        }

        public Task<Trip> AddExpenseAsync(long tripId, Stream file, string fileName, string description, decimal amount)
        {
            throw new NoInternetException();
        }

        public Task<Trip> SubmitTripAsync(long tripId, string notesForDriver)
        {
            throw new NoInternetException();
        }

        public Task<Trip> DeleteExpenseAsync(long tripId, long expenseId)
        {
            throw new NoInternetException();
        }

        public Task RejectTripAsync(long tripId)
        {
            throw new NoInternetException();
        }

        public Task<Trip> AcceptTripChangesAsync(long tripId)
        {
            throw new NoInternetException();
        }
    }
}