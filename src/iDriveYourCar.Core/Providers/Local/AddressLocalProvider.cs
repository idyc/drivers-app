﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;

namespace iDriveYourCar.Core.Providers
{
	public class AddressLocalProvider : IDYCLocalProvider, IAddressLocalProvider
	{
		public AddressLocalProvider()
		{
		}

		public Task<PlaceDetails> GetAddressDetailsAsync(string placeId)
		{
			throw new NoInternetException();
		}

		public Task<IEnumerable<Prediction>> GetAddressPredictionsAsync(string input)
		{
			throw new NoInternetException();
		}
	}
}
