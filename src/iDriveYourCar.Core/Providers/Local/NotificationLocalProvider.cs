﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Repositories;
using DGenix.Mobile.Fwk.Core.Models;
using System.Linq;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;

namespace iDriveYourCar.Core.Providers
{
    public class NotificationLocalProvider : IDYCLocalProvider, INotificationLocalProvider
    {
        private readonly INotificationRepository notificationRepository;

        public NotificationLocalProvider(
            INotificationRepository notificationRepository)
        {
            this.notificationRepository = notificationRepository;
        }

        public Task PersistNotificationsAsync(IEnumerable<Notification> notifications)
        {
            return this.notificationRepository.PersistManyAsync(notifications);
        }

        public async Task<IPagedListResult<Notification>> GetNotificationsAsync(NotificationCriteria criteria)
        {
            var notifications = await this.notificationRepository.GetAllAsync().ConfigureAwait(false);

            notifications.OrderBy(n => n.Date.ToDateTimeFromFormat());

            return new PagedListResultDto<Notification>
            {
                Result = notifications,
                HasNextPage = true
            };
        }

        public Task RemoveNotificationAsync(long notificationId)
        {
            throw new NoInternetException();
        }
    }
}
