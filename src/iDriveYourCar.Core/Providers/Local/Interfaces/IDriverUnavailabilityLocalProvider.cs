﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
	public interface IDriverUnavailabilityLocalProvider : ILocalBaseProvider, IDriverUnavailabilityProvider
	{
		Task<Driver> PersistDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities);

		Task<Driver> PersistDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities);
	}
}
