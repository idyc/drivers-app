﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
	public interface ITripLocalProvider : ILocalBaseProvider, ITripProvider
	{
		Task PersistTripsDataAsync(IEnumerable<Trip> tripsData);
	}
}