﻿using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
	public interface IReferralLocalProvider : ILocalBaseProvider, IReferralProvider
	{
		Task PersistDriverReferralsAsync(Referrals referral);
	}
}
