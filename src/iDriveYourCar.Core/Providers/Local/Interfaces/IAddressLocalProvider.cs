﻿using System;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IAddressLocalProvider : ILocalBaseProvider, IAddressProvider
	{
	}
}
