﻿using System;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IUserInformationLocalProvider : ILocalBaseProvider, IUserInformationProvider
	{
		Task PersistUserInformationAsync(UserToken userToken);
	}
}
