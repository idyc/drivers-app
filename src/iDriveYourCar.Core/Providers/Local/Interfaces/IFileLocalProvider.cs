﻿using System;
using System.Threading.Tasks;
using System.IO;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IFileLocalProvider : ILocalBaseProvider, IFileProvider
	{
		Task<bool> PersistDocumentAsync(Stream stream, string id);
	}
}
