﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public interface IChatLocalProvider : ILocalBaseProvider, IChatProvider
    {
        Task PersistChatTokenAsync(ChatToken chatToken);
    }
}
