﻿using System;
using DGenix.Mobile.Fwk.Core.Providers;
using System.Threading.Tasks;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
    public interface INotificationLocalProvider : ILocalBaseProvider, INotificationProvider
    {
        Task PersistNotificationsAsync(IEnumerable<Notification> notifications);
    }
}
