﻿using System;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.Core.Providers;

namespace iDriveYourCar.Core.Providers
{
	public interface IDriverLocalProvider : ILocalBaseProvider, IDriverProvider
	{
		Task<Driver> GetDriverDataAsync();

		Task PersistDriverDataAsync(Driver driverData);

		Task PersistDriverReviewsAsync(IEnumerable<Review> driverReviews);
	}
}
