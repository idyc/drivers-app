﻿using System;
using System.IO;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Providers
{
	public class FileLocalProvider : IDYCLocalProvider, IFileLocalProvider
	{
		private readonly IStreamCacheHelper streamCacheHelper;

		public FileLocalProvider(IStreamCacheHelper streamCacheHelper)
		{
			this.streamCacheHelper = streamCacheHelper;
		}

		public async Task<Stream> GetDocumentAsync(Document document)
		{
			var documentStream = await this.streamCacheHelper.LoadCacheAsync(document.Id.ToString()).ConfigureAwait(false);

			return documentStream;
		}

		public async Task<bool> PersistDocumentAsync(Stream stream, string id)
		{
			return await this.streamCacheHelper.SaveCacheAsync(stream, id).ConfigureAwait(false);
		}

		public Task<Driver> UploadAvatarAsync(Stream file, string fileName)
		{
			throw new NoInternetException();
		}

		public Task<Document> UploadDocumentAsync(Stream file, string fileName, DocumentType type)
		{
			throw new NoInternetException();
		}
	}
}
