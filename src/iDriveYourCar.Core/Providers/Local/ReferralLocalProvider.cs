﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Repositories;

namespace iDriveYourCar.Core.Providers
{
    public class ReferralLocalProvider : IDYCLocalProvider, IReferralLocalProvider
    {
        private readonly IReferralRepository referralRepository;

        public ReferralLocalProvider(IReferralRepository referralRepository)
        {
            this.referralRepository = referralRepository;
        }

        public Task<Referrals> GetDriverReferralsAsync()
        {
            return this.referralRepository.GetDriverReferrals();
        }

        public Task PersistDriverReferralsAsync(Referrals referral)
        {
            return this.referralRepository.CreateOrUpdateAsync(referral);
        }

        public Task RemindAsync(long invitationId)
        {
            throw new NoInternetException();
        }

        public Task RedeemAsync(long invitationId)
        {
            throw new NoInternetException();
        }

        public Task RedeemAsync()
        {
            throw new NoInternetException();
        }

        public Task InviteEmailFriendsAsync(InvitationType type, IEnumerable<EmailInvitation> invitations)
        {
            throw new NoInternetException();
        }

        public Task InviteFriendsManuallyAsync(InvitationType type, IEnumerable<string> emails)
        {
            throw new NoInternetException();
        }
    }
}
