﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;

namespace iDriveYourCar.Core.Providers
{
    public class ChatLocalProvider : IDYCLocalProvider, IChatLocalProvider
    {
        private readonly IIDYCSettings idycSettings;

        public ChatLocalProvider(
            IIDYCSettings idycSettings)
        {
            this.idycSettings = idycSettings;
        }

        public Task<ChatToken> GetTwilioChatToken(string deviceId)
        {
            throw new NoInternetException();
        }

        public Task PersistChatTokenAsync(ChatToken chatToken)
        {
            this.idycSettings.ChatIdentity = chatToken.Identity;
            this.idycSettings.ChatToken = chatToken.Token;

            return Task.FromResult(0);
        }
    }
}
