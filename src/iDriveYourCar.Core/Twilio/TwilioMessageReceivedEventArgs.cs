﻿using System;
namespace iDriveYourCar.Core.Twilio
{
    public class TwilioMessageReceivedEventArgs : EventArgs
    {
        public string Channel { get; set; }

        public string Message { get; set; }
    }
}
