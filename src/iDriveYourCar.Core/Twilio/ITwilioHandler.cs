﻿using System;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using System.Collections.Generic;

namespace iDriveYourCar.Core.Twilio
{
    public interface ITwilioHandler
    {
        Task Initialize(string token);

        Task<bool> TryJoinOrCreateChannelAsync(string channel);

        Task<bool> TryDeleteChannelAsync(string channel);

        Task<IList<ChatMessage>> GetMessagesAsync(int startIndex, int pageSize, string channel);

        Task<bool> TrySendMessageAsync(string channel, string text);

        event EventHandler<TwilioMessageReceivedEventArgs> MessageReceived;
    }
}
