﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;
using MvvmCross.Platform;

namespace iDriveYourCar.Core.Rules
{
    public class TripWorkflowRuleFactory : ITripWorkflowRuleFactory
    {
        private readonly Dictionary<TripType, Type> tripTypesToWorkflowRuleDictionary = new Dictionary<TripType, Type>
        {
            [TripType.WaitAndReturn] = typeof(WaitAndReturnWorkflowRule),
            [TripType.DriveMeHome] = typeof(DriveMeHomeWorkflowRule),
            [TripType.AirportDropoff] = typeof(AirportDepartureWorkflowRule),
            [TripType.AirportPickup] = typeof(AirportArrivalWorkflowRule),
            [TripType.OneWayPickup] = typeof(OneWayPickUpWorkflowRule),
            [TripType.OneWayDropoff] = typeof(OneWayDropoffWorkflowRule)
        };

        public TripWorkflowRuleFactory()
        {
        }

        public ITripWorkflowRule GetTripWorkflowRule(TripType tripType)
        {
            Type type = null;

            if(!this.tripTypesToWorkflowRuleDictionary.TryGetValue(tripType, out type))
                throw new InvalidOperationException($"The type {type.FullName} is not registered in the dictionary.");

            var workflowRule = Mvx.IocConstruct(type) as ITripWorkflowRule;

            return workflowRule;
        }
    }
}
