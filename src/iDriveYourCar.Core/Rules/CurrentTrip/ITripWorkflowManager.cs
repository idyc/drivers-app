﻿using iDriveYourCar.Core.Models;
using System;
using System.Collections.Generic;

namespace iDriveYourCar.Core.Rules
{
    public interface ITripWorkflowManager
    {
        ActiveTripState GetFollowingTripState(TripType tripType, ActiveTripState currentState);

        IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState);

        bool IsTripActive(Trip trip);

        Trip GetActiveTrip(Driver driver);
    }
}
