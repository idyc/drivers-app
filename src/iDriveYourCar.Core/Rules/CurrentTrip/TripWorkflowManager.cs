﻿using System;
using System.Collections.Generic;
using System.Linq;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    public class TripWorkflowManager : ITripWorkflowManager
    {
        private readonly ITripWorkflowRuleFactory tripWorkflowRuleFactory;

        public TripWorkflowManager(ITripWorkflowRuleFactory tripWorkflowRuleFactory)
        {
            this.tripWorkflowRuleFactory = tripWorkflowRuleFactory;
        }

        public IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState)
        {
            return this.tripWorkflowRuleFactory.GetTripWorkflowRule(trip.TripType).GetFollowingDirectionsByState(trip, toState);
        }

        public ActiveTripState GetFollowingTripState(TripType tripType, ActiveTripState currentState)
        {
            return this.tripWorkflowRuleFactory.GetTripWorkflowRule(tripType).GetFollowingTripState(currentState);
        }

        public bool IsTripActive(Trip trip)
        {
            // if date is equal to yesterday* OR date is equal to today and current time is >= (PickupTime - 1 hour)

            // * Yesterday: Trip is active or finished
            return trip.ActiveTripState != ActiveTripState.SubmitTrip
                &&
                (
                    DateTime.Parse(trip.PickupDate).Date == DateTime.Today.AddDays(-1).Date
                    ||
                     (
                        DateTime.Parse(trip.PickupDate).Date == DateTime.Today.Date
                         && DateTime.Now.TimeOfDay >= DateTime.Parse(trip.PickupTime).AddHours(-1).TimeOfDay
                     )
                );
        }

        public Trip GetActiveTrip(Driver driver)
        {
            return driver.UpcomingTrips.FirstOrDefault(trip => this.IsTripActive(trip));
        }
    }
}