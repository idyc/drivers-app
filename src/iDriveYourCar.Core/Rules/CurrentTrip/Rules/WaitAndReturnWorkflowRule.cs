﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    /// <summary>
    /// Wait and return workflow rule.
    /// 
    /// NAVIGATION IS DONE WITHOUT MAPS BECAUSE THE PATH IS ALWAYS UNCLEAR
    /// 
    /// </summary>
    public class WaitAndReturnWorkflowRule : ITripWorkflowRule
    {
        public WaitAndReturnWorkflowRule()
        {
        }

        public IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState)
        {
            return new List<Tuple<decimal, decimal>>();
        }

        public ActiveTripState GetFollowingTripState(ActiveTripState currentState)
        {
            switch(currentState)
            {
                case ActiveTripState.NotStarted:
                    return ActiveTripState.NavigateToPickup;

                case ActiveTripState.NavigateToPickup:
                    return ActiveTripState.StartTrip;

                case ActiveTripState.StartTrip:
                    return ActiveTripState.EndTrip;

                case ActiveTripState.EndTrip:
                    return ActiveTripState.SubmitTrip;

                default:
                    throw new NotImplementedException("Workflow status not supported for Wait and return");
            }
        }
    }
}
