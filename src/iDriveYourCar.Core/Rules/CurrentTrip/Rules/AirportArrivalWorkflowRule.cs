﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;
using System.Linq;

namespace iDriveYourCar.Core.Rules
{
    /// <summary>
    /// Airport arrival workflow rule.
    /// 
    /// DRIVER POSITION -> VEHICLE LOCATION (dropoff) -> PICK UP CLIENT IN AIRPORT -> DROPOFF AND END TRIP.
    /// 
    /// </summary>
    public class AirportArrivalWorkflowRule : ITripWorkflowRule
    {
        public AirportArrivalWorkflowRule()
        {
        }

        public IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState)
        {
            var navigationStops = new List<Tuple<decimal, decimal>>();
            switch(toState)
            {
                case ActiveTripState.NavigateToVehicle:

                    // the trip starts where it ends (unless specified)
                    navigationStops.Add(new Tuple<decimal, decimal>(trip.DropoffAddress.Lat.Value, trip.DropoffAddress.Lng.Value));
                    break;

                case ActiveTripState.StartTrip:

                    // next step: Go to pick up the client to the airport where he comes from
                    navigationStops.Add(new Tuple<decimal, decimal>(trip.FromAirport.Lat.Value, trip.FromAirport.Lng.Value));
                    break;

                case ActiveTripState.ArrivedAtPickup:

                    // next step: navigate through all stops till dropoff
                    foreach(var stop in trip.Stops)
                        navigationStops.Add(new Tuple<decimal, decimal>(stop.Address.Lat.Value, stop.Address.Lng.Value));

                    navigationStops.Add(new Tuple<decimal, decimal>(trip.DropoffAddress.Lat.Value, trip.DropoffAddress.Lng.Value));
                    break;

                case ActiveTripState.EndTrip:
                case ActiveTripState.SubmitTrip:
                case ActiveTripState.NotStarted:
                    break;
            }

            return navigationStops;
        }

        public ActiveTripState GetFollowingTripState(ActiveTripState currentState)
        {
            switch(currentState)
            {
                case ActiveTripState.NotStarted:
                    return ActiveTripState.NavigateToVehicle;

                case ActiveTripState.NavigateToVehicle:
                    return ActiveTripState.StartTrip;

                case ActiveTripState.StartTrip:
                    return ActiveTripState.ArrivedAtPickup;

                case ActiveTripState.ArrivedAtPickup:
                    return ActiveTripState.EndTrip;

                case ActiveTripState.EndTrip:
                    return ActiveTripState.SubmitTrip;

                default:
                    throw new NotImplementedException("Workflow status not supported for Wait and return");
            }
        }
    }
}
