﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    /// <summary>
    /// Drive me home workflow rule.
    /// 
    /// DRIVER POSITION -> START POINT (dropoff) -> PICK UP CLIENT -> DROPOFF AND END TRIP.
    /// 
    /// </summary>
    public class DriveMeHomeWorkflowRule : ITripWorkflowRule
    {
        public DriveMeHomeWorkflowRule()
        {
        }

        public IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState)
        {
            var navigationStops = new List<Tuple<decimal, decimal>>();
            switch(toState)
            {
                case ActiveTripState.NavigateToStartPoint:

                    // the trip starts where it ends (unless specified)
                    navigationStops.Add(new Tuple<decimal, decimal>(trip.DropoffAddress.Lat.Value, trip.DropoffAddress.Lng.Value));
                    break;

                case ActiveTripState.StartTrip:

                    // next step: Go to pick up the client
                    navigationStops.Add(new Tuple<decimal, decimal>(trip.PickupAddress.Lat.Value, trip.PickupAddress.Lng.Value));
                    break;

                case ActiveTripState.ArrivedAtPickup:

                    // next step: navigate through all stops till dropoff
                    foreach(var stop in trip.Stops)
                        navigationStops.Add(new Tuple<decimal, decimal>(stop.Address.Lat.Value, stop.Address.Lng.Value));

                    navigationStops.Add(new Tuple<decimal, decimal>(trip.DropoffAddress.Lat.Value, trip.DropoffAddress.Lng.Value));
                    break;

                case ActiveTripState.EndTrip:
                case ActiveTripState.SubmitTrip:
                case ActiveTripState.NotStarted:
                    break;
            }

            return navigationStops;
        }

        public ActiveTripState GetFollowingTripState(ActiveTripState currentState)
        {
            switch(currentState)
            {
                case ActiveTripState.NotStarted:
                    return ActiveTripState.NavigateToStartPoint;

                case ActiveTripState.NavigateToStartPoint:
                    return ActiveTripState.StartTrip;

                case ActiveTripState.StartTrip:
                    return ActiveTripState.ArrivedAtPickup;

                case ActiveTripState.ArrivedAtPickup:
                    return ActiveTripState.EndTrip;

                case ActiveTripState.EndTrip:
                    return ActiveTripState.SubmitTrip;

                default:
                    throw new NotImplementedException("Workflow status not supported for Wait and return");
            }
        }
    }
}
