﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    /// <summary>
    /// One way dropoff workflow rule.
    /// 
    /// DRIVER POSITION -> PICK UP (vehicle and client) -> ARRIVED AT PICKUP -> START TRIP (navigate through all stops till final location) -> DROPOFF CLIENT AND RETURN VEHICLE TO ORIGINAL POSITION -> END TRIP
    /// 
    /// </summary>
    public class OneWayDropoffWorkflowRule : ITripWorkflowRule
    {
        public OneWayDropoffWorkflowRule()
        {
        }

        public IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState)
        {
            var navigationStops = new List<Tuple<decimal, decimal>>();
            switch(toState)
            {
                case ActiveTripState.NavigateToPickup:

                    // the trip starts in pickup
                    navigationStops.Add(new Tuple<decimal, decimal>(trip.PickupAddress.Lat.Value, trip.PickupAddress.Lng.Value));
                    break;

                case ActiveTripState.ArrivedAtPickup:

                    // next step: the driver has arrived to pickup and waits for the client

                    break;

                case ActiveTripState.StartTrip:

                    // next step: navigate through all stops till dropoff in specific location
                    foreach(var stop in trip.Stops)
                        navigationStops.Add(new Tuple<decimal, decimal>(stop.Address.Lat.Value, stop.Address.Lng.Value));

                    navigationStops.Add(new Tuple<decimal, decimal>(trip.DropoffAddress.Lat.Value, trip.DropoffAddress.Lng.Value));
                    break;

                case ActiveTripState.ArrivedAtDropoff:

                    // the trip ends in pickup address (the driver has to return the vehicle to its original position)
                    navigationStops.Add(new Tuple<decimal, decimal>(trip.PickupAddress.Lat.Value, trip.PickupAddress.Lng.Value));
                    break;

                case ActiveTripState.EndTrip:
                case ActiveTripState.SubmitTrip:
                case ActiveTripState.NotStarted:
                    break;
            }

            return navigationStops;
        }

        public ActiveTripState GetFollowingTripState(ActiveTripState currentState)
        {
            switch(currentState)
            {
                case ActiveTripState.NotStarted:
                    return ActiveTripState.NavigateToPickup;

                case ActiveTripState.NavigateToPickup:
                    return ActiveTripState.ArrivedAtPickup;

                case ActiveTripState.ArrivedAtPickup:
                    return ActiveTripState.StartTrip;

                case ActiveTripState.StartTrip:
                    return ActiveTripState.ArrivedAtDropoff;

                case ActiveTripState.ArrivedAtDropoff:
                    return ActiveTripState.EndTrip;

                case ActiveTripState.EndTrip:
                    return ActiveTripState.SubmitTrip;

                default:
                    throw new NotImplementedException("Workflow status not supported for Wait and return");
            }
        }
    }
}
