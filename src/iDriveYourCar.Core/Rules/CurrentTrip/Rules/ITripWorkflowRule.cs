﻿using System;
using System.Collections.Generic;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    public interface ITripWorkflowRule
    {
        ActiveTripState GetFollowingTripState(ActiveTripState currentState);

        IEnumerable<Tuple<decimal, decimal>> GetFollowingDirectionsByState(Trip trip, ActiveTripState toState);
    }
}
