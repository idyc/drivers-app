﻿using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    public interface ITripWorkflowRuleFactory
    {
        ITripWorkflowRule GetTripWorkflowRule(TripType tripType);
    }
}
