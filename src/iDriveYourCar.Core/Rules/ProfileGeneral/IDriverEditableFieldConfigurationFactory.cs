﻿using System;
using iDriveYourCar.Core.Models;
namespace iDriveYourCar.Core.Rules.ProfileGeneral
{
	public interface IDriverEditableFieldConfigurationFactory
	{
		DriverEditableFieldConfigurationModel GetDriverEditableFieldConfiguration(Driver driver, DriverEditableField field);

		void UpdateDriverEditableField(Driver driver, DriverEditableField field, object newValue);
	}
}
