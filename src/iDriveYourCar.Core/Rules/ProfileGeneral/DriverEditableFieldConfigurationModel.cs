﻿using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using System;

namespace iDriveYourCar.Core.Rules.ProfileGeneral
{
    public class DriverEditableFieldConfigurationModel
    {
        public DriverEditableFieldConfigurationModel()
        {

        }

        public DriverEditableFieldConfigurationModel(
            string title,
            DriverEditableField field,
            string hint = null,
            string message = null,
            InputType inputType = InputType.None,
            string errorMessage = null,
            string defaultValue = null,
            Func<string, bool> validateEntry = null)
        {
            this.Title = title;
            this.Field = field;
            this.Hint = hint;
            this.Message = message ?? string.Format(IDriveYourCarStrings.PleaseEnterYourX, title);
            this.InputType = inputType;
            this.ErrorMessage = errorMessage ?? IDriveYourCarStrings.TheEnteredValueIsNotValid;
            this.DefaultValue = defaultValue;
            this.ValidateEntry = validateEntry;
        }

        public string Title { get; set; }

        public DriverEditableField Field { get; set; }

        public string Hint { get; set; }

        public string Message { get; set; }

        public string DefaultValue { get; set; }

        public string ErrorMessage { get; set; }

        public InputType InputType { get; set; }

        public Func<string, bool> ValidateEntry { get; private set; }
    }
}
