﻿using iDriveYourCar.Core.Models;
using System.Linq;
using DGenix.Mobile.Fwk.Core;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform;

namespace iDriveYourCar.Core.Rules.ProfileGeneral
{
    public class DriverEditableFieldConfigurationFactory : IDriverEditableFieldConfigurationFactory
    {
        private readonly Dictionary<DriverEditableField, DriverEditableFieldConfigurationModel> fieldToConfigurationDictionary = new Dictionary<DriverEditableField, DriverEditableFieldConfigurationModel>
        {
            [DriverEditableField.CellPhone] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.MobilePhone, DriverEditableField.CellPhone, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.MobilePhone), inputType: InputType.Phone),

            [DriverEditableField.Email] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.Email, DriverEditableField.Email, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.Email), inputType: InputType.Email),

            [DriverEditableField.HomePhone] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.HomePhone, DriverEditableField.HomePhone, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.HomePhone), inputType: InputType.Phone),

            [DriverEditableField.Raw] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.Address, DriverEditableField.Raw, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.Address), inputType: InputType.Text),


            // banking info
            [DriverEditableField.NameBankAccount] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.NameOnAccount, DriverEditableField.NameBankAccount, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.NameOnAccount), inputType: InputType.None, validateEntry: entry => !string.IsNullOrEmpty(entry)),

            [DriverEditableField.TaxId] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.TaxClassification, DriverEditableField.TaxId, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.TaxClassification)),

            [DriverEditableField.SSNTIN] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.SSNTIN, DriverEditableField.SSNTIN, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.SSNTIN), inputType: InputType.IntNumber, validateEntry: entry => !string.IsNullOrEmpty(entry) && entry.Length == 9),

            [DriverEditableField.RoutingNumber] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.RoutingNumber, DriverEditableField.RoutingNumber, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.RoutingNumber), inputType: InputType.None, validateEntry: entry => !string.IsNullOrEmpty(entry) && entry.Length == 9),

            [DriverEditableField.AccountNumber] = new DriverEditableFieldConfigurationModel(IDriveYourCarStrings.AccountNumber, DriverEditableField.AccountNumber, string.Format(IDriveYourCarStrings.EnterYourX, IDriveYourCarStrings.AccountNumber), inputType: InputType.None, validateEntry: entry => !string.IsNullOrEmpty(entry) && entry.Length >= 6 && entry.Length <= 16)
        };

        public DriverEditableFieldConfigurationModel GetDriverEditableFieldConfiguration(Driver driver, DriverEditableField field)
        {
            var configuration = this.fieldToConfigurationDictionary[field];

            var property = typeof(Driver).GetProperties().FirstOrDefault(p => p.Name == field.ToString());
            if(property != null)
            {
                configuration.DefaultValue = property.GetValue(driver) as string;
            }
            else
            {
                property = typeof(User).GetProperties().FirstOrDefault(p => p.Name == field.ToString());
                if(property != null)
                {
                    configuration.DefaultValue = property.GetValue(driver.User) as string;
                }
                else
                {
                    property = typeof(Address).GetProperties().FirstOrDefault(p => p.Name == field.ToString());

                    if(driver.User.PrimaryAddress == null)
                    {
                        driver.User.PrimaryAddress = new Address();
                    }

                    configuration.DefaultValue = property.GetValue(driver.User.PrimaryAddress) as string;
                }

            }

            return configuration;
        }

        public void UpdateDriverEditableField(Driver driver, DriverEditableField field, object newValue)
        {
            var property = typeof(Driver).GetProperties().FirstOrDefault(p => p.Name == field.ToString());
            if(property != null)
            {
                property.SetValue(driver, newValue);
            }
            else
            {
                property = typeof(User).GetProperties().FirstOrDefault(p => p.Name == field.ToString());
                if(property != null)
                {
                    property.SetValue(driver.User, newValue);
                }
                else
                {
                    property = typeof(Address).GetProperties().FirstOrDefault(p => p.Name == field.ToString());
                    property.SetValue(driver.User.PrimaryAddress, newValue);
                }
            }
        }
    }
}
