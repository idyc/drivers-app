﻿namespace iDriveYourCar.Core.Rules.ProfileGeneral
{
    public enum DriverEditableField
    {
        CellPhone,
        Email,
        HomePhone,
        Raw,
        NameBankAccount,
        TaxId,
        SSNTIN,
        RoutingNumber,
        AccountNumber
    }
}
