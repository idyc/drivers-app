﻿using System;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Rules
{
    public static class DriverExtensions
    {
        public static void FormatTrips(this Driver driver)
        {
            foreach(var upcoming in driver.UpcomingTrips.ToArray())
            {
                if(upcoming.TripReturn != null && !driver.UpcomingTrips.Contains(upcoming.TripReturn))
                {
                    if(upcoming.TripReturn.Driver == null)
                        upcoming.TripReturn.Driver = driver;

                    if(upcoming.TripReturn.Client == null)
                        upcoming.TripReturn.Client = upcoming.Client;

                    driver.UpcomingTrips.Add(upcoming.TripReturn);
                }
            }
            foreach(var finished in driver.FinishedTrips.ToArray())
            {
                if(finished.TripReturn != null && !driver.FinishedTrips.Contains(finished.TripReturn))
                {
                    if(finished.TripReturn.Driver == null)
                        finished.TripReturn.Driver = driver;

                    if(finished.TripReturn.Client == null)
                        finished.TripReturn.Client = finished.Client;

                    driver.FinishedTrips.Add(finished.TripReturn);
                }
            }
            foreach(var canceled in driver.CanceledTrips.ToArray())
            {
                if(canceled.TripReturn != null && !driver.CanceledTrips.Contains(canceled.TripReturn))
                {
                    if(canceled.TripReturn.Driver == null)
                        canceled.TripReturn.Driver = driver;

                    if(canceled.TripReturn.Client == null)
                        canceled.TripReturn.Client = canceled.Client;

                    driver.CanceledTrips.Add(canceled.TripReturn);
                }
            }
        }
    }
}
