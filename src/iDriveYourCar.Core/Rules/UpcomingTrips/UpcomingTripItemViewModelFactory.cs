﻿using System;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.Rules.UpcomingTrips
{
    public class UpcomingTripItemViewModelFactory : IUpcomingTripItemViewModelFactory
    {
        public IBaseTripItemViewModel Create(Trip trip)
        {
            return new UpcomingTripItemViewModel(trip);
        }

        public IBaseTripItemViewModel Create(DateTime date)
        {
            return new DateUpcomingTripItemViewModel(date);
        }
    }
}