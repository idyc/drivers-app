﻿using System;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.Rules.UpcomingTrips
{
	public interface IUpcomingTripItemViewModelFactory
	{
		IBaseTripItemViewModel Create(DateTime date);
		IBaseTripItemViewModel Create(Trip trip);
	}
}
