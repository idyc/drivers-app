﻿using System;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.Rules.OpenTrips
{
    public interface IOpenTripItemViewModelFactory
    {
        IBaseTripItemViewModel Create(DateTime date);

        IBaseTripItemViewModel Create(Trip trip);
    }
}
