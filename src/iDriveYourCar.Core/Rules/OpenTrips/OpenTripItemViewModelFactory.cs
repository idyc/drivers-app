﻿using System;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.ViewModels.Items;

namespace iDriveYourCar.Core.Rules.OpenTrips
{
    public class OpenTripItemViewModelFactory : IOpenTripItemViewModelFactory
    {
        public IBaseTripItemViewModel Create(Trip trip)
        {
            return new OpenTripItemViewModel(trip);
        }

        public IBaseTripItemViewModel Create(DateTime date)
        {
            return new DateOpenTripItemViewModel(date);
        }
    }
}
