﻿using System;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using System.IO;
using DGenix.Mobile.Fwk.Core.Services;

namespace iDriveYourCar.Core.Services
{
    public interface IFileService : IService
    {
        Task UploadDocumentAsync(Stream file, string fileName, DocumentType type, Action<Document> success);

        Task GetDocumentUrl(Document document, Action<Stream> success);

        Task UploadAvatarAsync(Stream file, string fileName, Action<Driver> success);
    }
}
