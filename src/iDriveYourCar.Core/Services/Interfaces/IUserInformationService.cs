﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services
{
    public interface IUserInformationService : IService
    {
        Task VerifyUserAsync(string username, string password, Action<User> success);

        Task LogoutAsync(Action success);
    }
}