﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Services;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services
{
    public interface IReferralsService : IService
    {
        Task GetAllReferralsAsync(Action<Referrals> cacheSuccess, Action<Referrals> success);

        Task RemindAsync(long invitationId, Action success);

        Task RedeemAsync(long invitationId, Action success);

        Task RedeemAsync(Action success);

        Task InviteEmailFriendsAsync(InvitationType type, IEnumerable<EmailInvitation> invitations, Action success);

        Task InviteFriendsManuallyAsync(InvitationType type, IEnumerable<string> emails, Action success);
    }
}
