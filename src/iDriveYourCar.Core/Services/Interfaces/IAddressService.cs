﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using DGenix.Mobile.Fwk.Core.Services;

namespace iDriveYourCar.Core.Services
{
    public interface IAddressService : IService
    {
        Task GetAddressPredictionsAsync(string input, Action<IEnumerable<Prediction>> success);

        Task GetAddressDetailsAsync(string placeId, Action<PlaceDetails> success);
    }
}
