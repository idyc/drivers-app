﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Services;

namespace iDriveYourCar.Core.Services
{
    public interface IForgotPasswordService : IService
    {
        Task RecoverPasswordDataAsync(string email, Action success);
    }
}
