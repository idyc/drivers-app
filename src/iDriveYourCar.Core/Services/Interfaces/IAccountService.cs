﻿using System;
namespace iDriveYourCar.Core.Services
{
	public interface IAccountService : IService
	{
		Task GetAccountsAsync(Action<IEnumerable<Account>> success, Action fail);
	}
}
