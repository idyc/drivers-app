﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services
{
    public interface ITripsService : IService
    {
        Task GetOpenTripsAsync(OpenTripCriteria openTripCriteria, Action<IEnumerable<Trip>> success);

        Task AcceptTripAsync(long tripId, Action<Driver> success);

        Task RejectTripAsync(long tripId, Action success);

        Task PerformTripWorkflowAsync(long tripId, ActiveTripState toState, Action<Trip> success);

        Task SubmitTripAsync(long tripId, string notesForDriver, Action<Trip> success);

        Task AddExpenseAsync(long tripId, string description, decimal amount, Action<Trip> success);

        Task AddExpenseAsync(long tripId, Stream file, string fileName, string description, decimal amount, Action<Trip> success);

        Task DeleteExpenseAsync(long tripId, long expenseId, Action<Trip> success);

        Task AcceptTripChangesAsync(long tripId, Action<Trip> success);
    }
}
