﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services
{
    public interface IChatService : IService
    {
        Task GetTwilioChatToken(string deviceId, Action<ChatToken> success);
    }
}
