﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services.Interfaces
{
    public interface IDriverUnavailabilityService : IService
    {
        Task UpdateDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities, long? maxDrivingTime, Action<Driver> success);

        Task UpdateDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities, Action<Driver> success);
    }
}
