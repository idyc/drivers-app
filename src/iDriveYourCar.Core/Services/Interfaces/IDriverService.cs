using System;
using System.Threading.Tasks;
using iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Services;

namespace iDriveYourCar.Core.Services
{
    public interface IDriverService : IService
    {
        Task GetCachedDriverDataAsync(DateTime startDate, DateTime endDate, Action<Driver> success);

        Task GetDriverDataAsync(DateTime startDate, DateTime endDate, Action<Driver> success);

        Task GetDriverReviewsAsync(ReviewCriteria criteria, Action<IPagedListResult<Review>> cacheSuccess, Action<IPagedListResult<Review>> success);

        Task GetDriverReviewsAsync(ReviewCriteria criteria, Action<IPagedListResult<Review>> success);

        Task UpdateDriverDataAsync(Driver driver, Action<Driver> success);

        Task UpdateDriverAddressAsync(Address address, Action<Address> success);

        Task UpdateDriverLocationAsync(Geoposition position, Action success);

        Task UpdateDriverLocationAsync(Geoposition position, long operatorId, Action success);

        Task UpdateDeviceIdAsync(string deviceId, Action success);

        Task UpdateBankingInformationAsync(Driver driver, Action<Driver> success);

        Task CreateDriverAsync(RegisterDriver driver, Action<Driver> success);
    }
}
