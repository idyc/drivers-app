﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services
{
    public interface INotificationService : IService
    {
        Task GetNotificationsAsync(NotificationCriteria criteria, Action<IPagedListResult<Notification>> cacheSuccess, Action<IPagedListResult<Notification>> success);

        Task GetNotificationsAsync(NotificationCriteria criteria, Action<IPagedListResult<Notification>> success);

        Task RemoveNotificationAsync(long notificationId, Action success);
    }
}