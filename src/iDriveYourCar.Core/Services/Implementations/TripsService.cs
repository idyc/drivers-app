﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class TripsService : BaseService, ITripsService
    {
        public TripsService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {
        }

        public Task AcceptTripAsync(long tripId, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().AcceptTripAsync(tripId))
                       .Success(success)
                       .Run();
        }

        public Task RejectTripAsync(long tripId, Action success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().RejectTripAsync(tripId))
                       .Success(success)
                       .Run();
        }

        public Task AddExpenseAsync(long tripId, string description, decimal amount, Action<Trip> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().AddExpenseAsync(tripId, description, amount))
                   .Success(success)
                   .Run();
        }

        public Task AddExpenseAsync(long tripId, Stream file, string fileName, string description, decimal amount, Action<Trip> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().AddExpenseAsync(tripId, file, fileName, description, amount))
                       .Success(success)
                       .Run();
        }

        public Task DeleteExpenseAsync(long tripId, long expenseId, Action<Trip> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().DeleteExpenseAsync(tripId, expenseId))
                       .Success(success)
                       .Run();
        }

        public Task GetOpenTripsAsync(OpenTripCriteria openTripCriteria, Action<IEnumerable<Trip>> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().GetOpenTripsAsync(openTripCriteria))
                       .Success(success)
                       .Run();
        }

        public Task PerformTripWorkflowAsync(long tripId, ActiveTripState toState, Action<Trip> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().PerformTripWorkflowAsync(tripId, toState))
                       .Success(success)
                       .Run();
        }

        public Task SubmitTripAsync(long tripId, string notesForDriver, Action<Trip> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().SubmitTripAsync(tripId, notesForDriver))
                       .Success(success)
                       .Run();
        }

        public Task AcceptTripChangesAsync(long tripId, Action<Trip> success)
        {
            return base.Execute(() => base.GetProvider<ITripProvider>().AcceptTripChangesAsync(tripId))
                       .Success(success)
                       .Run();
        }
    }
}
