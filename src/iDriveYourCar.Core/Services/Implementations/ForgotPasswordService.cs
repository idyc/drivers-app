﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class ForgotPasswordService : BaseService, IForgotPasswordService
    {
        public ForgotPasswordService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task RecoverPasswordDataAsync(string email, Action success)
        {
            return base.Execute(() => base.GetProvider<IForgotPasswordProvider>().RecoverPasswordDataAsync(email))
                       .Success(success)
                       .Run();
        }
    }
}
