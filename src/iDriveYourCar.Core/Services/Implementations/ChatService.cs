﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class ChatService : BaseService, IChatService
    {
        public ChatService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task GetTwilioChatToken(string deviceId, Action<ChatToken> success)
        {
            return base.Execute(() => base.GetProvider<IChatProvider>().GetTwilioChatToken(deviceId))
                       .Success(success)
                       .Run();
        }
    }
}
