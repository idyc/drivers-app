﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;
using iDriveYourCar.Core.Services.Interfaces;

namespace iDriveYourCar.Core.Services.Implementations
{
    public class DriverUnavailabilityService : BaseService, IDriverUnavailabilityService
    {
        public DriverUnavailabilityService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task UpdateDriverUnavailabilityAsync(IEnumerable<DriverUnavailability> driverUnavailabilities, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverUnavailabilityProvider>().UpdateDriverUnavailabilityAsync(driverUnavailabilities))
                       .Success(success)
                       .Run();
        }

        public Task UpdateDriverWeeklyUnavailabilityAsync(IEnumerable<DriverUnavailabilityWeek> driverUnavailabilities, long? maxDrivingTime, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverUnavailabilityProvider>().UpdateDriverWeeklyUnavailabilityAsync(driverUnavailabilities, maxDrivingTime))
                       .Success(success)
                       .Run();
        }
    }
}
