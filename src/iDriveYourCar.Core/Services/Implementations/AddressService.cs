﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using DGenix.Mobile.Fwk.GooglePlacesAPI.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class AddressService : BaseService, IAddressService
    {
        public AddressService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {
        }

        public Task GetAddressDetailsAsync(string placeId, Action<PlaceDetails> success)
        {
            return base.Execute(() => base.GetProvider<IAddressProvider>().GetAddressDetailsAsync(placeId))
                       .Success(success)
                       .Run();
        }

        public Task GetAddressPredictionsAsync(string input, Action<IEnumerable<Prediction>> success)
        {
            return base.Execute(() => base.GetProvider<IAddressProvider>().GetAddressPredictionsAsync(input))
                       .Success(success)
                       .Run();
        }
    }
}
