using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class DriverService : BaseService, IDriverService
    {
        public DriverService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task CreateDriverAsync(RegisterDriver driver, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().CreateDriverAsync(driver))
                       .Success(success)
                       .Run();
        }

        public Task GetCachedDriverDataAsync(DateTime startDate, DateTime endDate, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>(true).GetDriverDataAsync(startDate, endDate))
                       .Success(success)
                       .Run();
        }

        public Task GetDriverDataAsync(DateTime startDate, DateTime endDate, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().GetDriverDataAsync(startDate, endDate))
                       .Success(success)
                       .Run();
        }

        public Task GetDriverReviewsAsync(ReviewCriteria criteria, Action<IPagedListResult<Review>> cacheSuccess, Action<IPagedListResult<Review>> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>(true).GetDriverReviewsAsync(criteria),
                                () => base.GetProvider<IDriverProvider>().GetDriverReviewsAsync(criteria))
                       .CacheSuccess(cacheSuccess)
                       .Success(success)
                       .Run();
        }

        public Task GetDriverReviewsAsync(ReviewCriteria criteria, Action<IPagedListResult<Review>> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().GetDriverReviewsAsync(criteria))
                       .Success(success)
                       .Run();
        }

        public Task UpdateBankingInformationAsync(Driver driver, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().UpdateBankingInformationAsync(driver))
                       .Success(success)
                       .Run();
        }

        public Task UpdateDeviceIdAsync(string deviceId, Action success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().UpdateDeviceIdAsync(deviceId))
                       .Success(success)
                       .Run();
        }

        public Task UpdateDriverAddressAsync(Address address, Action<Address> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().UpdateDriverAddressAsync(address))
                       .Success(success)
                       .Run();
        }

        public Task UpdateDriverDataAsync(Driver driver, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().UpdateDriverDataAsync(driver))
                       .Success(success)
                       .Run();
        }

        public Task UpdateDriverLocationAsync(Geoposition position, Action success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().UpdateDriverLocationAsync(position))
                       .Success(success)
                       .Run();
        }

        public Task UpdateDriverLocationAsync(Geoposition position, long operatorId, Action success)
        {
            return base.Execute(() => base.GetProvider<IDriverProvider>().UpdateDriverLocationAsync(position, operatorId))
                       .Success(success)
                       .Run();
        }
    }
}
