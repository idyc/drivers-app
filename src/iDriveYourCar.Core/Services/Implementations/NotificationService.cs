﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Criteria;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class NotificationService : BaseService, INotificationService
    {
        public NotificationService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task GetNotificationsAsync(NotificationCriteria criteria, Action<IPagedListResult<Notification>> success)
        {
            return base.Execute(() => base.GetProvider<INotificationProvider>().GetNotificationsAsync(criteria))
                       .Success(success)
                       .Run();
        }

        public Task GetNotificationsAsync(NotificationCriteria criteria, Action<IPagedListResult<Notification>> cacheSuccess, Action<IPagedListResult<Notification>> success)
        {
            return base.Execute(() => base.GetProvider<INotificationProvider>(true).GetNotificationsAsync(criteria),
                                () => base.GetProvider<INotificationProvider>().GetNotificationsAsync(criteria))
                       .CacheSuccess(cacheSuccess)
                       .Success(success)
                       .Run();
        }

        public Task RemoveNotificationAsync(long notificationId, Action success)
        {
            return base.Execute(() => base.GetProvider<INotificationProvider>().RemoveNotificationAsync(notificationId))
               .Success(success)
               .Run();
        }
    }
}