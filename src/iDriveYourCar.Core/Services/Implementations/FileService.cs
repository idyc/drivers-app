﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Providers;
using System.IO;
using iDriveYourCar.Core.Models;

namespace iDriveYourCar.Core.Services
{
    public class FileService : BaseService, IFileService
    {
        public FileService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {
        }

        public Task GetDocumentUrl(Document document, Action<Stream> success)
        {
            return base.Execute(() => base.GetProvider<IFileProvider>().GetDocumentAsync(document))
                       .Success(success)
                       .Run();
        }

        public Task UploadAvatarAsync(Stream file, string fileName, Action<Driver> success)
        {
            return base.Execute(() => base.GetProvider<IFileProvider>().UploadAvatarAsync(file, fileName))
                       .Success(success)
                       .Run();
        }

        public Task UploadDocumentAsync(Stream file, string fileName, Models.DocumentType type, Action<Document> success)
        {
            return base.Execute(() => base.GetProvider<IFileProvider>().UploadDocumentAsync(file, fileName, type))
                       .Success(success)
                       .Run();
        }
    }
}
