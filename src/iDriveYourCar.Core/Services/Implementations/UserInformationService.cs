﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class UserInformationService : BaseService, IUserInformationService
    {
        public UserInformationService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task LogoutAsync(Action success)
        {
            return base.Execute(() => base.GetProvider<IUserInformationProvider>().LogoutAsync())
                       .Success(success)
                       .Run();
        }

        public Task VerifyUserAsync(string username, string password, Action<User> success)
        {
            return base.Execute(() => base.GetProvider<IUserInformationProvider>().VerifyUserAsync(username, password))
                       .Success(success)
                       .Run();
        }
    }
}
