﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Services;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Models;
using iDriveYourCar.Core.Providers;

namespace iDriveYourCar.Core.Services
{
    public class ReferralService : BaseService, IReferralsService
    {
        public ReferralService(
            IServiceSandBox serviceSandBox,
            IProviderFactory providerFactory)
            : base(serviceSandBox, providerFactory)
        {

        }

        public Task GetAllReferralsAsync(Action<Referrals> cacheSuccess, Action<Referrals> success)
        {
            return base.Execute(() => base.GetProvider<IReferralProvider>(true).GetDriverReferralsAsync(),
                                () => base.GetProvider<IReferralProvider>().GetDriverReferralsAsync())
                       .CacheSuccess(cacheSuccess)
                       .Success(success)
                       .Run();
        }

        public Task RemindAsync(long invitationId, Action success)
        {
            return base.Execute(() => base.GetProvider<IReferralProvider>().RemindAsync(invitationId))
                       .Success(success)
                       .Run();
        }

        public Task RedeemAsync(long invitationId, Action success)
        {
            return base.Execute(() => base.GetProvider<IReferralProvider>().RedeemAsync(invitationId))
                       .Success(success)
                       .Run();
        }

        public Task RedeemAsync(Action success)
        {
            return base.Execute(() => base.GetProvider<IReferralProvider>().RedeemAsync())
                       .Success(success)
                       .Run();
        }

        public Task InviteEmailFriendsAsync(InvitationType type, IEnumerable<EmailInvitation> invitations, Action success)
        {
            return base.Execute(() => base.GetProvider<IReferralProvider>().InviteEmailFriendsAsync(type, invitations))
                       .Success(success)
                       .Run();
        }

        public Task InviteFriendsManuallyAsync(InvitationType type, IEnumerable<string> emails, Action success)
        {
            return base.Execute(() => base.GetProvider<IReferralProvider>().InviteFriendsManuallyAsync(type, emails))
                       .Success(success)
                       .Run();
        }
    }
}
