﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Controls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class IDYCPopoverMenuView : PopoverMenuView<PopoverMenuTableViewCell, string>
    {
        public IDYCPopoverMenuView(IList<Tuple<string, ICommand>> items) : base(items)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.TableView.RowHeight = Metrics.PopoverMenuRowHeight;
            this.TableView.ContentInset = new UIEdgeInsets(16f, 0f, 16f, 0f);
            this.View.BackgroundColor = IDYCColors.BackgroundColor;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.View.Superview.Layer.CornerRadius = 2f;
            this.View.Superview.Layer.ShadowRadius = 2f;
            this.View.Superview.Layer.ShadowColor = UIColor.Black.CGColor;
            this.View.Superview.Layer.ShadowOpacity = 0.54f;
            this.View.Superview.Layer.ShadowOffset = new CoreGraphics.CGSize(2f, 2f);
        }
    }
}
