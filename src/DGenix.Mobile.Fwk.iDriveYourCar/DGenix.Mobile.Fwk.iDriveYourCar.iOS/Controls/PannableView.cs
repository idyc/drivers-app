﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class PannableView : BaseView
    {
        public float MARGIN_TO_PERFORM_ACTION { get; set; } = 80f;

        public UIView FrontView { get; set; }

        public UIPanGestureRecognizer PanGesture { get; set; }

        public event EventHandler OnPanned;

        private FluentLayout leftConstraint;

        private nfloat beginTranslationPointX;
        private bool onPannedCalled;

        public PannableView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.FrontView = new UIView
            {
                UserInteractionEnabled = true,
                ClipsToBounds = true
            };

            this.PanGesture = new UIPanGestureRecognizer(this.HandlePan);
            this.FrontView.AddGestureRecognizer(this.PanGesture);

            this.AggregateSubviews(this.FrontView);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.leftConstraint = this.FrontView.AtLeftOf(this);
            this.AddConstraints(
                this.leftConstraint,
                this.FrontView.AtTopOf(this),
                this.FrontView.WithSameHeight(this),
                this.FrontView.WithSameWidth(this)
            );
        }

        public void Close()
        {
            if(this.leftConstraint == null)
                return;

            this.leftConstraint.Constant = 0;
            this.SetNeedsLayout();

            this.onPannedCalled = false;
        }

        private void HandlePan(UIPanGestureRecognizer panGesture)
        {
            switch(panGesture.State)
            {
                case UIGestureRecognizerState.Began:
                    this.beginTranslationPointX = panGesture.TranslationInView(this.FrontView).X;
                    break;
                case UIGestureRecognizerState.Changed:
                    var newPoint = panGesture.TranslationInView(this.FrontView);

                    if(this.leftConstraint.Constant <= 0 && newPoint.X < 0)
                        return;

                    var newConstant = this.leftConstraint.Constant + newPoint.X - this.beginTranslationPointX;
                    this.leftConstraint.Constant = newConstant >= 0 ? newConstant : 0;
                    this.beginTranslationPointX = newPoint.X;

                    this.SetNeedsLayout();

                    break;
                case UIGestureRecognizerState.Ended:
                    this.HandleVelocity(panGesture);

                    break;
            }
        }

        private void HandleVelocity(UIPanGestureRecognizer panGesture)
        {
            var velocity = panGesture.VelocityInView(this.FrontView);

            if(velocity.X > 0 && this.leftConstraint.Constant >= (this.Bounds.X + this.Bounds.Width) / 2)
            {
                this.leftConstraint.Constant = this.Bounds.X + this.Bounds.Width * 3;
                if(!this.onPannedCalled)
                {
                    this.OnPanned?.Invoke(this, new EventArgs());
                    this.onPannedCalled = true;
                    return;
                }
            }

            this.leftConstraint.Constant = 0;
        }
    }
}
