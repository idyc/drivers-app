﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class BorderedTextView : BaseBorderedView
    {
        private const float TEXTVIEW_PADDING = 8f;

        public IDYCTextView TextView { get; set; }

        public IDYCMultilineLabel Placeholder { get; set; }

        public IDYCMultilineLabel LblError { get; set; }

        public float TextViewHeight { get; set; } = 80f;

        public BorderedTextView()
        {
        }

        public BorderedTextView(string placeholder)
        {
            this.TextView.Text = placeholder;
            this.Placeholder.Text = placeholder;
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.TextView = new IDYCTextView(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                TextAlignment = UITextAlignment.Justified,
                ShowsVerticalScrollIndicator = true,
                BackgroundColor = UIColor.Clear
            };
            this.TextView.ShouldBeginEditing = t =>
            {
                if(string.IsNullOrEmpty(this.TextView.Text))
                    this.Placeholder.Hidden = true;

                return true;
            };
            this.Placeholder = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.LightGrayTextColor
            };
            this.LblError = new IDYCMultilineLabel(Metrics.FontSizeTiny)
            {
                TextColor = IDYCColors.ErrorTextColor
            };

            this.AggregateSubviews(this.TextView, this.Placeholder, this.LblError);

            this.BringSubviewToFront(this.Placeholder);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.TextView.AtLeftOf(this, TEXTVIEW_PADDING),
                this.TextView.AtTopOf(this, TEXTVIEW_PADDING),
                this.TextView.AtRightOf(this, TEXTVIEW_PADDING),
                this.TextView.Height().EqualTo(this.TextViewHeight),

                this.Placeholder.WithSameLeft(this.TextView).Plus(4f),
                this.Placeholder.WithSameTop(this.TextView).Plus(2f),
                this.Placeholder.WithSameRight(this.TextView),

                this.LblError.Below(this.TextView),
                this.LblError.AtLeftOf(this, PADDING_HORIZONTAL),
                this.LblError.AtRightOf(this, PADDING_HORIZONTAL),
                this.LblError.AtBottomOf(this, PADDING_VERTICAL)
            );
        }
    }
}
