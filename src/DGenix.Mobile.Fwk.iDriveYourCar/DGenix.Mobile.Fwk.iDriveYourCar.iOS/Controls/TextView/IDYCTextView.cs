﻿using System;
using DGenix.Mobile.Fwk.iOS.Controls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class IDYCTextView : BaseTextView
	{
		public IDYCTextView(float fontSize, string font = Metrics.RobotoRegular)
		{
			this.Font = UIFont.FromName(font, fontSize);
		}
	}
}
