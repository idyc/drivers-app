﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class InformationHeaderView : IDYCView
	{
		private const float TOP_MARGIN = 36f;
		private const float BOTTOM_MARGIN = 8f;

		public IDYCLabel Title { get; set; }

		public InformationHeaderView(string title)
		{
			this.Title.Text = title;
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.Title = new IDYCLabel(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.MidGrayTextColor
			};
			this.AggregateSubviews(this.Title);
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.AddConstraints(
				this.Title.AtLeftOf(this),
				this.Title.AtTopOf(this, TOP_MARGIN),
				this.Title.AtRightOf(this),
				this.Title.AtBottomOf(this, BOTTOM_MARGIN)
			);
		}
	}
}
