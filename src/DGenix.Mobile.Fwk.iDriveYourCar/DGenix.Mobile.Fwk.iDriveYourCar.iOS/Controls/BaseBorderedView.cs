﻿using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class BaseBorderedView : IDYCView
    {
        protected const float PADDING_HORIZONTAL = 12f;
        protected const float PADDING_VERTICAL = 8f;
        protected const float CONTROLS_DISTANCE = 4f;
        protected const float CORNER_RADIUS = 2f;
        protected const float BORDER_WIDTH = 1f;

        public BaseBorderedView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.White;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.Layer.CornerRadius = CORNER_RADIUS;
            this.Layer.BorderWidth = BORDER_WIDTH;
            this.Layer.BorderColor = IDYCColors.BorderColor.CGColor;
        }
    }
}
