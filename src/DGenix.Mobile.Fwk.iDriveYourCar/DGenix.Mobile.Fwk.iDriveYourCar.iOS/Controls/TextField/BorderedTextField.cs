﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class BorderedTextField : BaseBorderedView
	{
		public IDYCTextField TextField { get; set; }

		public IDYCMultilineLabel LblError { get; set; }

		public BorderedTextField()
		{
		}

		public BorderedTextField(string placeholder)
		{
			this.TextField.Placeholder = placeholder;
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.TextField = new IDYCTextField(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.LightGrayTextColor,
				TextAlignment = UITextAlignment.Justified
			};
			this.LblError = new IDYCMultilineLabel(Metrics.FontSizeTiny)
			{
				TextColor = IDYCColors.ErrorTextColor
			};

			this.AddSubviews(this.TextField, this.LblError);
			this.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.AddConstraints(
				this.TextField.AtLeftOf(this, PADDING_HORIZONTAL),
				this.TextField.AtTopOf(this, PADDING_VERTICAL),
				this.TextField.AtRightOf(this, PADDING_HORIZONTAL),

				this.LblError.Below(this.TextField),
				this.LblError.AtLeftOf(this, PADDING_HORIZONTAL),
				this.LblError.AtRightOf(this, PADDING_HORIZONTAL),
				this.LblError.AtBottomOf(this, PADDING_VERTICAL)
			);
		}
	}
}
