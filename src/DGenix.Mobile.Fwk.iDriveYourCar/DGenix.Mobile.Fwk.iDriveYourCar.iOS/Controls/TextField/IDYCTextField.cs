﻿using System;
using DGenix.Mobile.Fwk.iOS.Controls;
using UIKit;
namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class IDYCTextField : BaseTextField
	{
		public IDYCTextField(float fontSize, string font = Metrics.RobotoRegular)
			: base(fontSize)
		{
			this.Font = UIFont.FromName(font, fontSize);
		}
	}
}