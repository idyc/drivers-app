﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Extensions;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class BorderedIconTextField : BaseBorderedView
    {
        public IDYCTextField TextField { get; set; }

        public UIImageView RightIcon { get; set; }

        public BorderedIconTextField()
        {
        }

        public BorderedIconTextField(string placeholder, string imageIcon = null)
        {
            this.TextField.Placeholder = placeholder;

            if(!string.IsNullOrEmpty(imageIcon))
                this.RightIcon.Image = UIImage.FromBundle(imageIcon);
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.TextField = new IDYCTextField(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.LightGrayTextColor,
                TextAlignment = UITextAlignment.Justified
            };
            this.RightIcon = new UIImageView();

            this.AggregateSubviews(this.TextField, this.RightIcon);

            this.TextField.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.RightIcon.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Horizontal);
            this.TextField.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);

            this.AddConstraints(
                this.TextField.AtLeftOf(this, PADDING_HORIZONTAL),
                this.TextField.AtTopOf(this, PADDING_VERTICAL),
                this.TextField.AtBottomOf(this, PADDING_VERTICAL),

                this.RightIcon.ToRightOf(this.TextField, CONTROLS_DISTANCE),
                this.RightIcon.AtRightOf(this, PADDING_HORIZONTAL),
                this.RightIcon.WithSameCenterY(this)
            );
        }
    }
}
