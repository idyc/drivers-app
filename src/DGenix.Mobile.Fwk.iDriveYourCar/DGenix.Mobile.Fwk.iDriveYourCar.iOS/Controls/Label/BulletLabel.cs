﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class BulletLabel : IDYCView
    {
        private const float BULLET_SIZE = 5f;
        private const float MARGIN = 8f;
        private const float CONTROLS_DISTANCE = 8f;

        public UIView Bullet { get; set; }

        public IDYCMultilineLabel Label { get; set; }

        public BulletLabel(string text, float fontSize, string font = Metrics.RobotoRegular)
        {
            this.Label.Font = UIFont.FromName(font, fontSize);
            this.Label.Text = text;
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Bullet = new UIView { BackgroundColor = IDYCColors.DarkGrayTextColor };

            this.Label = new IDYCMultilineLabel(Metrics.FontSizeSmall)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.AddSubviews(this.Bullet, this.Label);
            this.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Bullet.AtLeftOf(this),
                this.Bullet.AtTopOf(this, MARGIN),
                this.Bullet.Width().EqualTo(BULLET_SIZE),
                this.Bullet.Height().EqualTo(BULLET_SIZE),

                this.Label.ToRightOf(this.Bullet, CONTROLS_DISTANCE),
                this.Label.WithSameCenterY(this.Bullet),
                this.Label.AtRightOf(this),
                this.Label.AtBottomOf(this, MARGIN)
            );
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.Bullet.Layer.CornerRadius = this.Bullet.Frame.Width / 2;
        }
    }
}
