﻿using System;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;
using Cirrious.FluentLayouts.Touch;
namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class OptionsView : IDYCView
	{
		public OAStackView.OAStackView OptionsStack { get; set; }

		public OptionsView()
		{

		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.OptionsStack = new OAStackView.OAStackView
			{
				Axis = UILayoutConstraintAxis.Vertical,
				Distribution = OAStackView.OAStackViewDistribution.Fill,
				Spacing = Metrics.DEFAULT_DISTANCE
			};

			this.AggregateSubviews(this.OptionsStack);
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.AddConstraints(
				this.OptionsStack.AtLeftOf(this),
				this.OptionsStack.AtTopOf(this),
				this.OptionsStack.AtRightOf(this),
				this.OptionsStack.AtBottomOf(this)
			);
		}
	}
}
