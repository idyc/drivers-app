﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using Cirrious.FluentLayouts.Touch;
namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls.CustomAlertView
{
	public class SingleInputContentView : IDYCView
	{
		private const float CONTROLS_DISTANCE = 8f;

		public BorderedTextField TextField { get; set; }

		public IDYCMultilineLabel LblError { get; set; }

		public SingleInputContentView()
		{
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.TextField = new BorderedTextField();
			this.LblError = new IDYCMultilineLabel(Metrics.FontSizeTiny)
			{
				TextColor = IDYCColors.ErrorTextColor
			};

			this.AddSubviews(this.TextField, this.LblError);
			this.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.AddConstraints(
				this.TextField.AtLeftOf(this),
				this.TextField.AtTopOf(this),
				this.TextField.AtRightOf(this),

				this.LblError.Below(this.TextField, CONTROLS_DISTANCE),
				this.LblError.WithSameLeft(this.TextField),
				this.LblError.WithSameRight(this.TextField),
				this.LblError.AtBottomOf(this)
			);
		}
	}
}
