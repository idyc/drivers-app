﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls.CustomAlertView
{
	public class DoubleInputContentView : IDYCView
	{
		private const float CONTROLS_DISTANCE = 8f;

		public BorderedTextField TextField1 { get; set; }

		public BorderedTextField TextField2 { get; set; }

		public IDYCMultilineLabel LblError1 { get; set; }

		public IDYCMultilineLabel LblError2 { get; set; }

		public DoubleInputContentView()
		{
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.TextField1 = new BorderedTextField();
			this.LblError1 = new IDYCMultilineLabel(Metrics.FontSizeTiny)
			{
				TextColor = IDYCColors.ErrorTextColor
			};

			this.TextField2 = new BorderedTextField();
			this.LblError2 = new IDYCMultilineLabel(Metrics.FontSizeTiny)
			{
				TextColor = IDYCColors.ErrorTextColor
			};

			this.AddSubviews(this.TextField1, this.LblError1, this.TextField2, this.LblError2);
			this.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.AddConstraints(
				this.TextField1.AtLeftOf(this),
				this.TextField1.AtTopOf(this),
				this.TextField1.AtRightOf(this),

				this.LblError1.Below(this.TextField1, CONTROLS_DISTANCE),
				this.LblError1.WithSameLeft(this.TextField1),
				this.LblError1.WithSameRight(this.TextField1),

				this.TextField2.Below(this.LblError1, CONTROLS_DISTANCE),
				this.TextField2.AtLeftOf(this),
				this.TextField2.AtRightOf(this),

				this.LblError2.Below(this.TextField2, CONTROLS_DISTANCE),
				this.LblError2.WithSameLeft(this.TextField2),
				this.LblError2.WithSameRight(this.TextField2),
				this.LblError2.AtBottomOf(this)
			);
		}
	}
}
