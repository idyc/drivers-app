﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.Core;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Plugins.Color.iOS;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls.CustomAlertView
{
    public class CustomAlertView : UIView
    {
        private const float ANIMATION_DURATION = .2f;
        private const float PADDING = 16f;
        private const float CONTROLS_DISTANCE = 12f;
        private const float BUTTON_HEIGHT = 36f;

        private readonly IAppConfiguration appConfiguration;

        #region Fields

        private string title, message, btnSubmitTitle, btnCancelTitle;

        private UIView alertBox;
        private PrimaryTextButton btnCancel, btnSubmit;
        private IDYCLabel lblTitle;
        private IDYCMultilineLabel lblMessage;

        private NSObject mKeyboardDidSHowNotification;
        private NSObject mKeyboardDidHideNotification;

        private UIView mBackingView;

        private UIView contentView;

        private Func<bool> CanSubmit, HandleSubmit;

        private Action HandleCancel;

        private FluentLayout centerYConstraint;

        public UIColor AlertBackgroundColor
        {
            get
            {
                return this.alertBox.BackgroundColor;
            }
            set
            {
                this.alertBox.BackgroundColor = value;
            }
        }

        public UIColor AlertTitleColor
        {
            get
            {
                return this.lblTitle.TextColor;
            }
            set
            {
                this.lblTitle.TextColor = value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomAlertView"/> class.
        /// </summary>
        public CustomAlertView(
            IAppConfiguration appConfiguration,
            string title,
            string message = "",
            string submitButtonText = null,
            string cancelButtonText = null,
            UIView contentView = null,
            Func<bool> canSubmit = null,
            Func<bool> handleSubmit = null,
            Action handleCancel = null)
            : base()
        {
            this.appConfiguration = appConfiguration;

            this.CanSubmit = canSubmit ?? (() => { return true; });

            this.HandleSubmit = handleSubmit;
            this.HandleCancel = handleCancel;

            this.title = title;
            this.message = message;
            this.btnSubmitTitle = submitButtonText;
            this.btnCancelTitle = cancelButtonText;

            this.contentView = contentView;

            this.BackgroundColor = UIColor.FromWhiteAlpha(1.0f, 0.0f);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Show this instance.
        /// </summary>
        public void Show(UIView parentView)
        {
            this.Alpha = 0.0f;

            this.SetupView();

            UIView.Animate(ANIMATION_DURATION, () => this.Alpha = 1.0f);

            var window = UIApplication.SharedApplication.Windows[UIApplication.SharedApplication.Windows.Length - 1];
            this.mBackingView = new UIView(window.Bounds);
            this.mBackingView.BackgroundColor = UIColor.FromWhiteAlpha(0.1f, 0.5f);
            parentView.AddSubviews(this.mBackingView, this);

            parentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            this.centerYConstraint = this.WithSameCenterY(parentView).Minus(20f).SetActive(true).WithIdentifier("centerYConstraint");

            var width = Convert.ToSingle(Math.Min(325, parentView.Frame.Size.Width - 50));
            parentView.AddConstraints(
                this.WithSameCenterX(parentView),
                this.centerYConstraint,
                this.Width().EqualTo(width),

                this.mBackingView.AtLeftOf(parentView),
                this.mBackingView.AtTopOf(parentView),
                this.mBackingView.AtRightOf(parentView),
                this.mBackingView.AtBottomOf(parentView)
            );

            var tapGesture = new UITapGestureRecognizer(
                gesture =>
            {
                this.CancelButtonTapped();
            });
            this.mBackingView.AddGestureRecognizer(tapGesture);

            UIDevice.CurrentDevice.BeginGeneratingDeviceOrientationNotifications();

            NSNotificationCenter.DefaultCenter.AddObserver(new NSString("UIDeviceOrientationDidChangeNotification"), this.DeviceOrientationDidChange);

            this.mKeyboardDidSHowNotification = UIKeyboard.Notifications.ObserveDidShow((s, e) => KeyboardDidShow(e.Notification));

            this.mKeyboardDidHideNotification = UIKeyboard.Notifications.ObserveDidHide((s, e) => KeyboardDidHide(e.Notification));

            parentView.BringSubviewToFront(this);
        }

        /// <summary>
        /// Show this instance.
        /// </summary>
        public void Show()
        {
            var window = UIApplication.SharedApplication.Windows[UIApplication.SharedApplication.Windows.Length - 1];
            this.Show(window);
        }

        /// <summary>
        /// Hide this instance.
        /// </summary>
        public void Hide()
        {
            UIView.AnimateNotify(ANIMATION_DURATION, () => this.Alpha = 0.0f,
             (finished) =>
            {
                this.RemoveFromSuperview();
                this.mBackingView.RemoveFromSuperview();

                UIDevice.CurrentDevice.EndGeneratingDeviceOrientationNotifications();

                NSNotificationCenter.DefaultCenter.RemoveObserver(new NSString("UIDeviceOrientationDidChangeNotification"));

                this.mKeyboardDidSHowNotification.Dispose();
                this.mKeyboardDidHideNotification.Dispose();
            });
        }

        /// <summary>
        /// Setups the view.
        /// </summary>
        private void SetupView()
        {
            // init alertBox
            this.alertBox = new UIView();
            this.alertBox.BackgroundColor = IDYCColors.BackgroundColor;
            this.alertBox.Layer.CornerRadius = 2f;
            this.alertBox.Layer.MasksToBounds = true;
            this.AddSubview(this.alertBox);

            this.lblTitle = new IDYCLabel(Metrics.FontSizeHuge, Metrics.RobotoMedium)
            {
                Text = this.title,
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.alertBox.AddSubview(this.lblTitle);

            this.lblMessage = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                Text = this.message,
                TextColor = IDYCColors.LightGrayTextColor
            };
            this.alertBox.AddSubview(this.lblMessage);

            this.alertBox.AddSubview(this.contentView);

            // start add buttons

            if(!string.IsNullOrEmpty(this.btnSubmitTitle))
            {
                this.btnSubmit = new PrimaryTextButton(this.appConfiguration, Metrics.FontSizeNormal);
                this.btnSubmit.SetTitle(this.btnSubmitTitle.ToUpper(), UIControlState.Normal);
                this.btnSubmit.SetTitleColor(this.appConfiguration.PrimaryColor.ToNativeColor(), UIControlState.Normal);
                this.btnSubmit.SetTitleColor(this.appConfiguration.LightPrimaryColor.ToNativeColor(), UIControlState.Highlighted);
                this.btnSubmit.TouchUpInside += (object sender, EventArgs e) => SubmitButtonTapped();
                this.alertBox.AddSubview(btnSubmit);

                if(!string.IsNullOrEmpty(this.btnCancelTitle))
                {
                    this.btnCancel = new PrimaryTextButton(this.appConfiguration, Metrics.FontSizeNormal);
                    this.btnCancel.SetTitle(this.btnCancelTitle.ToUpper(), UIControlState.Normal);
                    this.btnCancel.SetTitleColor(this.appConfiguration.PrimaryColor.ToNativeColor(), UIControlState.Normal);
                    this.btnCancel.SetTitleColor(this.appConfiguration.LightPrimaryColor.ToNativeColor(), UIControlState.Highlighted);
                    this.btnCancel.TouchUpInside += (object sender, EventArgs e) => CancelButtonTapped();
                    this.alertBox.AddSubview(btnCancel);
                }
            }
            this.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            this.alertBox.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            this.AddConstraints(
                this.alertBox.AtLeftOf(this),
                this.alertBox.AtTopOf(this),
                this.alertBox.AtRightOf(this),
                this.alertBox.AtBottomOf(this)
            );

            this.alertBox.AddConstraints(
                this.lblTitle.AtLeftOf(this.alertBox, PADDING),
                this.lblTitle.AtTopOf(this.alertBox, PADDING),
                this.lblTitle.AtRightOf(this.alertBox, PADDING),

                this.lblMessage.Below(lblTitle, CONTROLS_DISTANCE),
                this.lblMessage.AtLeftOf(this.alertBox, PADDING),
                this.lblMessage.AtRightOf(this.alertBox, PADDING),

                this.contentView.Below(this.lblMessage, CONTROLS_DISTANCE),
                this.contentView.AtLeftOf(this.alertBox, PADDING),
                this.contentView.AtRightOf(this.alertBox, PADDING)
            );

            if(this.btnSubmit != null)
            {
                this.alertBox.AddConstraints(
                    this.btnSubmit.Below(this.contentView, PADDING),
                    this.btnSubmit.AtRightOf(this.alertBox, PADDING / 2),
                    this.btnSubmit.AtBottomOf(this.alertBox, PADDING / 2),
                    this.btnSubmit.Height().EqualTo(BUTTON_HEIGHT)
                );

                if(this.btnCancel != null)
                {
                    this.alertBox.AddConstraints(
                        this.btnCancel.ToLeftOf(btnSubmit, PADDING / 2),
                        this.btnCancel.WithSameCenterY(btnSubmit),
                        this.btnCancel.Height().EqualTo(BUTTON_HEIGHT)
                    );
                }
            }
            else
            {
                this.alertBox.AddConstraints(
                    this.contentView.AtBottomOf(this.alertBox, PADDING)
                );
            }
        }

        /// <summary>
        /// Determines whether this instance cancel button tapped.
        /// </summary>
        /// <returns><c>true</c> if this instance cancel button tapped; otherwise, <c>false</c>.</returns>
        private void CancelButtonTapped()
        {
            this.HandleCancel?.Invoke();

            this.Hide();
        }

        /// <summary>
        /// Submits the button tapped.
        /// </summary>
        private void SubmitButtonTapped()
        {
            if(this.CanSubmit.Invoke())
            {
                if(this.HandleSubmit == null || this.HandleSubmit.Invoke())
                    this.Hide();
            }
        }

        /// <summary>
        /// Devices the orientation did change.
        /// </summary>
        /// <param name="notification">Notification.</param>
        private void DeviceOrientationDidChange(NSNotification notification)
        {
            //if(this.isKeyboardShown)
            //{
            //	UIView.Animate(ANIMATION_DURATION / 2, () =>
            //	{
            //		CGRect frame = this.Frame;
            //		frame.Y -= YCorrection();
            //		this.Frame = frame;
            //	});
            //}
            //this.ResetFrame(true);
        }

        /// <summary>
        /// Keyboards the did show.
        /// </summary>
        /// <param name="notification">Notification.</param>
        private void KeyboardDidShow(NSNotification notification)
        {
            //this.ResetFrame(true);
            //this.isKeyboardShown = true;

            //var nsValue = notification.UserInfo.ValueForKey(new NSString("UIKeyboardFrameBeginUserInfoKey")) as NSValue;

            ////this.bottomConstraint.Constant = this.keyboardFrameSize.Height / 2;

            ////this.bottomConstraint.Active = true;
            ////this.centerYConstraint.Active = false;

            //this.Superview.SetNeedsLayout();

            //UIView.Animate(ANIMATION_DURATION, () => this.Superview.LayoutIfNeeded());

            ////UIView.Animate(ANIMATION_DURATION / 2, () =>
            ////{
            ////	CGRect frame = this.Frame;
            ////	frame.Y -= YCorrection();
            ////	this.Frame = frame;
            ////});
        }

        /// <summary>
        /// Keyboards the did hide.
        /// </summary>
        /// <param name="notification">Notification.</param>
        private void KeyboardDidHide(NSNotification notification)
        {
            //this.isKeyboardShown = false;

            ////this.bottomConstraint.Active = false;
            ////this.centerYConstraint.Active = true;

            //this.Superview.SetNeedsLayout();

            //UIView.Animate(ANIMATION_DURATION, () => this.Superview.LayoutIfNeeded());

            ////this.ResetFrame(true);
        }

        /// <summary>
        /// Ys the correction.
        /// </summary>
        /// <returns>The correction.</returns>
        //private nfloat YCorrection()
        //{
        //	var yCorrection = this.Frame.Height;

        //	if(UIDeviceOrientationExtensions.IsLandscape(UIDevice.CurrentDevice.Orientation))
        //	{
        //		if(UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
        //		{
        //			yCorrection = 80.0f;
        //		}
        //		else if(UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        //		{
        //			yCorrection = 100.0f;
        //		}
        //	}
        //	else
        //	{
        //		if(UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        //		{
        //			yCorrection = 0.0f;
        //		}
        //	}
        //	return yCorrection;
        //}

        /// <summary>
        /// Resets the frame.
        /// </summary>
        /// <param name="animated">If set to <c>true</c> animated.</param>
        //private void ResetFrame(Boolean animated)
        //{
        //	var window = UIApplication.SharedApplication.Windows[UIApplication.SharedApplication.Windows.Length - 1];

        //	//this.Frame = window.Frame;

        //	// 
        //	if(animated)
        //	{
        //		UIView.Animate(0.3f, () =>
        //		{
        //			//this.Center = new CGPoint(window.Center.X, window.Center.Y);
        //			//this.alertBox.Center = this.Center;
        //			this.mBackingView.Frame = this.Bounds;
        //		});

        //	}
        //	else
        //	{
        //		//this.Center = new CGPoint(window.Center.X, window.Center.Y);
        //		//this.alertBox.Center = this.Center;
        //		this.mBackingView.Frame = this.Bounds;
        //	}
        //}

        /// <summary>
        /// Displaies the N decimal.
        /// </summary>
        /// <returns>The N decimal.</returns>
        /// <param name="dbValue">Db value.</param>
        /// <param name="nDecimal">N decimal.</param>
        //private string DisplayNDecimal(double dbValue, int nDecimal)
        //{
        //	string decimalPoints = "0";
        //	if(nDecimal > 0)
        //	{
        //		decimalPoints += ".";
        //		for(int i = 0; i < nDecimal; i++)
        //			decimalPoints += "0";
        //	}
        //	return dbValue.ToString(decimalPoints);
        //}

        #endregion
    }
}
