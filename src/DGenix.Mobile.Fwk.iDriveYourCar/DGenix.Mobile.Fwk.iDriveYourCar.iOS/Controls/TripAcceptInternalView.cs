﻿using System;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class TripAcceptInternalView : IDYCView
    {
        private const float CONTROLS_DISTANCE = 8f;

        public IDYCLabel TopLabel { get; set; }

        public UIImageView Icon { get; set; }

        public IDYCLabel BottomLabel { get; set; }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.TopLabel = new IDYCLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                TextAlignment = UITextAlignment.Center
            };
            this.Icon = new UIImageView();
            this.BottomLabel = new IDYCLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                TextAlignment = UITextAlignment.Center
            };

            this.AggregateSubviews(this.TopLabel, this.Icon, this.BottomLabel);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.TopLabel.AtLeftOf(this),
                this.TopLabel.AtTopOf(this, CONTROLS_DISTANCE),
                this.TopLabel.AtRightOf(this),

                this.Icon.WithSameCenterY(this),
                this.Icon.WithSameCenterX(this),

                this.BottomLabel.AtLeftOf(this),
                this.BottomLabel.AtRightOf(this),
                this.BottomLabel.AtBottomOf(this, CONTROLS_DISTANCE)
            );
        }
    }
}
