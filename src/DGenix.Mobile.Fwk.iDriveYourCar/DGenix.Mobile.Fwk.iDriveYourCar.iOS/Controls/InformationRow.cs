﻿using System;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iOS.Extensions;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class InformationRow : IDYCView
	{
		private const float VERTICAL_MARGIN = 16f;
		private const float HORIZONTAL_MARGIN = 8f;

		public UIView ViewLine { get; set; }

		public IDYCMultilineLabel LblName { get; set; }

		public IDYCMultilineLabel LblInformation { get; set; }

		public InformationRow()
		{

		}

		public InformationRow(string name)
		{
			this.LblName.Text = name;
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.UserInteractionEnabled = true;

			this.ViewLine = new UIView
			{
				BackgroundColor = IDYCColors.LineColor
			};
			this.LblName = new IDYCMultilineLabel(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.DarkGrayTextColor
			};
			this.LblInformation = new IDYCMultilineLabel(Metrics.FontSizeNormal)
			{
				TextColor = IDYCColors.MidGrayTextColor
			};

			this.AggregateSubviews(this.ViewLine, this.LblName, this.LblInformation);
		}

		protected override void CreateConstraints()
		{
			base.CreateConstraints();

			this.AddConstraints(
				this.ViewLine.AtLeftOf(this),
				this.ViewLine.AtTopOf(this),
				this.ViewLine.AtRightOf(this),
				this.ViewLine.Height().EqualTo(1f),

				this.LblName.Below(this.ViewLine, VERTICAL_MARGIN),
				this.LblName.AtLeftOf(this),
				this.LblName.AtRightOf(this, HORIZONTAL_MARGIN),

				this.LblInformation.Below(this.LblName),
				this.LblInformation.AtLeftOf(this),
				this.LblInformation.AtRightOf(this, HORIZONTAL_MARGIN),
				this.LblInformation.AtBottomOf(this, VERTICAL_MARGIN)
			);
		}

		public void SetEnabled(bool enabled)
		{
			this.UserInteractionEnabled = enabled;
			this.LblName.TextColor = enabled ? IDYCColors.DarkGrayTextColor : IDYCColors.LightGrayTextColor;
			this.LblInformation.TextColor = enabled ? IDYCColors.MidGrayTextColor : IDYCColors.LightGrayTextColor;
		}
	}
}