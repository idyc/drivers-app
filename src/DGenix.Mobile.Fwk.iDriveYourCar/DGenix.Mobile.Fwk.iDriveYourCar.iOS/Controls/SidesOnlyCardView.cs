﻿using System;
using CoreGraphics;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class SidesOnlyCardView : CardView
	{
		public SidesOnlyCardView()
		{
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			this.Layer.CornerRadius = 0f;

			var shadowPath = UIBezierPath.FromRoundedRect(this.Bounds.Inset(0, 4), CORNER_RADIUS);
			this.Layer.ShadowPath = shadowPath.CGPath;
			this.Layer.ShadowOffset = new CGSize(0, 0);
		}
	}
}
