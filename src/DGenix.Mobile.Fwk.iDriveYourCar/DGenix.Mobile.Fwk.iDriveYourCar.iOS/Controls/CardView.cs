﻿using CoreGraphics;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class CardView : IDYCView
	{
		protected const float CORNER_RADIUS = 3f;
		protected const float SHADOW_OFFSET_WIDTH = 0;
		protected const float SHADOW_OFFSET_HEIGHT = 2f;
		protected const float SHADOW_OPACITY = .4f;

		protected UIColor shadowColor = UIColor.LightGray;

		public CardView()
		{
		}

		protected override void CreateViews()
		{
			base.CreateViews();

			this.BackgroundColor = UIColor.White;
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			this.Layer.CornerRadius = CORNER_RADIUS;

			var shadowPath = UIBezierPath.FromRoundedRect(this.Bounds, CORNER_RADIUS);

			this.Layer.MasksToBounds = false;
			this.Layer.ShadowColor = this.shadowColor.CGColor;
			this.Layer.ShadowOffset = new CGSize(SHADOW_OFFSET_WIDTH, SHADOW_OFFSET_HEIGHT);
			this.Layer.ShadowOpacity = SHADOW_OPACITY;
			this.Layer.ShadowPath = shadowPath.CGPath;
		}
	}
}
