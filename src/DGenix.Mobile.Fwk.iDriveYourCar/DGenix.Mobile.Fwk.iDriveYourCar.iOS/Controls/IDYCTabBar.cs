﻿using System;
using CoreGraphics;
using MaterialControls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class IDYCTabBar : MDTabBar
    {
        public IDYCTabBar()
        {
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.Layer.ShadowColor = UIColor.Gray.CGColor;
            this.Layer.ShadowRadius = 2.0f;
            this.Layer.ShadowOpacity = 0.8f;
            this.Layer.ShadowOffset = new CGSize(2, 2);
            this.Layer.ShadowPath = UIBezierPath.FromRect(this.Layer.Bounds).CGPath;
            this.Layer.MasksToBounds = false;
        }
    }
}
