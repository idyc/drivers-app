﻿using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using MaterialControls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class WhiteBorderedButton : MDButton
    {
        public WhiteBorderedButton(float fontSize = Metrics.FontSizeBig, string fontName = Metrics.RobotoRegular) : base()
        {
            this.ContentEdgeInsets = new UIEdgeInsets(12f, 8f, 12f, 8f);

            this.BackgroundColor = UIColor.White;
            this.RippleColor = IDYCColors.LightGrayTextColor;

            this.SetTitleColor(IDYCColors.MidGrayTextColor, UIControlState.Normal);
            this.SetTitleColor(IDYCColors.LightGrayTextColor, UIControlState.Disabled);

            this.SetEnabledStyle();

            this.TitleLabel.Font = UIFont.FromName(fontName, fontSize);
            this.Layer.CornerRadius = 2f;
            this.Layer.BorderWidth = 1f;
            this.Layer.BorderColor = IDYCColors.LineColor.CGColor;
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                if(value)
                    this.SetEnabledStyle();
                else
                    this.SetDisabledStyle();
            }
        }

        private void SetEnabledStyle()
        {
            this.BackgroundColor = UIColor.White;
            this.RippleColor = IDYCColors.LightGrayTextColor;
        }

        private void SetDisabledStyle()
        {
            this.BackgroundColor = IDYCColors.BackgroundColor;
        }
    }
}
