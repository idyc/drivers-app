﻿using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.iOS.Controls;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class PrimaryTextButton : BaseButton
	{
		public PrimaryTextButton(IAppConfiguration appConfiguration, float fontSize, string font = Metrics.RobotoRegular)
		{
			this.TitleLabel.Font = UIFont.FromName(font, fontSize);

			this.SetTitleColor(appConfiguration.PrimaryColor.ToNativeColor(), UIControlState.Normal);
			this.SetTitleColor(appConfiguration.AccentColor.ToNativeColor(), UIControlState.Highlighted);
		}
	}
}
