﻿using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using MaterialControls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class GreenBackgroundButton : MDButton
    {
        public GreenBackgroundButton(float fontSize = Metrics.FontSizeBig, string fontName = Metrics.RobotoRegular) : base()
        {
            this.ContentEdgeInsets = new UIEdgeInsets(12f, 0, 12f, 0);

            this.SetTitleColor(UIColor.White, UIControlState.Normal);
            this.SetTitleColor(IDYCColors.DarkGrayTextColor, UIControlState.Disabled);

            this.SetEnabledStyle();

            this.TitleLabel.Font = UIFont.FromName(fontName, fontSize);
            this.Layer.CornerRadius = 2f;
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                if(value)
                    this.SetEnabledStyle();
                else
                    this.SetDisabledStyle();
            }
        }

        private void SetEnabledStyle()
        {
            this.BackgroundColor = IDYCColors.GreenButtonColor;
            this.RippleColor = IDYCColors.LightGrayTextColor;

        }

        private void SetDisabledStyle()
        {
            this.BackgroundColor = IDYCColors.BorderColor;
        }
    }
}
