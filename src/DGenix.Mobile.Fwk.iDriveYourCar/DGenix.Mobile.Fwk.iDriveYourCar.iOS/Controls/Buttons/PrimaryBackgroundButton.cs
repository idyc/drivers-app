﻿using DGenix.Mobile.Fwk.Core;
using MaterialControls;
using MvvmCross.Plugins.Color.iOS;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class PrimaryBackgroundButton : MDButton
    {
        protected readonly IAppConfiguration appConfiguration;

        public PrimaryBackgroundButton(IAppConfiguration appConfiguration, float fontSize = Metrics.FontSizeBig, string fontName = Metrics.RobotoRegular) : base()
        {
            this.appConfiguration = appConfiguration;

            this.ContentEdgeInsets = new UIEdgeInsets(12f, 0, 12f, 0);

            this.SetTitleColor(UIColor.White, UIControlState.Normal);
            this.SetTitleColor(this.appConfiguration.AccentColor.ToNativeColor(), UIControlState.Highlighted);
            this.SetTitleColor(IDYCColors.DarkGrayTextColor, UIControlState.Disabled);

            this.SetEnabledStyle();

            this.TitleLabel.Font = UIFont.FromName(fontName, fontSize);
            this.Layer.CornerRadius = 2f;
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                if(value)
                    this.SetEnabledStyle();
                else
                    this.SetDisabledStyle();
            }
        }

        protected virtual void SetEnabledStyle()
        {
            this.BackgroundColor = this.appConfiguration.PrimaryColor.ToNativeColor();
            this.RippleColor = this.appConfiguration.LightPrimaryColor.ToNativeColor();

        }

        protected virtual void SetDisabledStyle()
        {
            this.BackgroundColor = IDYCColors.BackgroundColor;
        }
    }
}
