﻿using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Controls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
	public class DarkGrayTextButton : BaseButton
	{
		public DarkGrayTextButton(float fontSize, string font = Metrics.RobotoRegular)
		{
			this.TitleLabel.Font = UIFont.FromName(font, fontSize);

			this.SetTitleColor(IDYCColors.DarkGrayTextColor, UIControlState.Normal);
			this.SetTitleColor(IDYCColors.MidGrayTextColor, UIControlState.Highlighted);
		}
	}
}
