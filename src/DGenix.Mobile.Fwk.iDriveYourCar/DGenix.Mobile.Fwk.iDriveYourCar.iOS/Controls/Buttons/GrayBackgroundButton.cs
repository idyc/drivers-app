﻿using System;
using MaterialControls;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class GrayBackgroundButton : MDButton
    {
        public GrayBackgroundButton(float fontSize = Metrics.FontSizeBig, string fontName = Metrics.RobotoRegular) : base()
        {
            this.ContentEdgeInsets = new UIEdgeInsets(12f, 0, 12f, 0);

            this.SetTitleColor(IDYCColors.MidGrayTextColor, UIControlState.Normal);
            this.SetTitleColor(IDYCColors.LightGrayTextColor, UIControlState.Disabled);

            this.SetEnabledStyle();

            this.TitleLabel.Font = UIFont.FromName(fontName, fontSize);
            this.Layer.CornerRadius = 2f;
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                if(value)
                    this.SetEnabledStyle();
                else
                    this.SetDisabledStyle();
            }
        }

        private void SetEnabledStyle()
        {
            this.BackgroundColor = UIColor.White;
            this.RippleColor = IDYCColors.LightGrayTextColor;

        }

        private void SetDisabledStyle()
        {
            this.BackgroundColor = IDYCColors.BorderColor;
        }
    }
}
