﻿using DGenix.Mobile.Fwk.Core;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls
{
    public class InviteManuallyButton : PrimaryBackgroundButton
    {
        public InviteManuallyButton(IAppConfiguration appConfiguration, float fontSize = Metrics.FontSizeBig, string fontName = Metrics.RobotoRegular)
            : base(appConfiguration, fontSize, fontName)
        {
            this.SetTitleColor(UIColor.White, UIControlState.Disabled);
        }

        protected override void SetDisabledStyle()
        {
            this.BackgroundColor = this.appConfiguration.PrimaryColor.ToNativeColor().ColorWithAlpha(.26f);
        }
    }
}
