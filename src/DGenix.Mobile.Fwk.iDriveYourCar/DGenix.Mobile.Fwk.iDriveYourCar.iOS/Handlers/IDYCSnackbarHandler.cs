﻿using System;
using DGenix.Mobile.Fwk.Core.Handlers;
using SnackBarTTG.Source;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Handlers
{
    public class IDYCSnackbarHandler : ISnackbarHandler
    {
        public IDYCSnackbarHandler()
        {
        }

        public void ShowMessage(string title, SnackbarDuration duration = SnackbarDuration.Middle)
        {
            var snackBar = new TTGSnackbar(title, this.GetNativeDuration(duration));

            this.SetupSnackbar(snackBar);
            snackBar.ActionText = null;

            snackBar.Show();

            snackBar.LeftMargin = 0f;
            snackBar.RightMargin = 0f;
            snackBar.BottomMargin = 0f;
            snackBar.CornerRadius = 0f;
            snackBar.AnimationDuration = 1f;
        }

        private void SetupSnackbar(TTGSnackbar snackBar)
        {
            snackBar.AnimationType = TTGSnackbarAnimationType.SlideFromBottomBackToBottom;
            snackBar.MessageTextFont = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeNormal);
        }

        private TTGSnackbarDuration GetNativeDuration(SnackbarDuration duration)
        {
            switch(duration)
            {
                case SnackbarDuration.Short:
                    return TTGSnackbarDuration.Short;
                case SnackbarDuration.Middle:
                    return TTGSnackbarDuration.Middle;
                case SnackbarDuration.Long:
                    return TTGSnackbarDuration.Long;
                case SnackbarDuration.Forever:
                    return TTGSnackbarDuration.Forever;
                default:
                    return TTGSnackbarDuration.Middle;
            }
        }
    }
}
