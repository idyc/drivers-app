﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls.CustomAlertView;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews;
using DGenix.Mobile.Fwk.iOS;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Controls.Gestures;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Foundation;
using MaterialControls;
using UIKit;
using Cirrious.FluentLayouts.Touch;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Handlers
{
    public class InputFieldHandler : IInputFieldHandler
    {
        private readonly IAppConfiguration appConfiguration;

        public InputFieldHandler(IAppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        public Task<string> ShowSingleInputFieldAsync(string title, string message, Func<string, string> inputValidation, InputType inputType = InputType.None, string defaultValue = null)
        {
            var tcs = new TaskCompletionSource<string>();

            var inputContent = new SingleInputContentView();
            inputContent.TextField.TextField.Text = defaultValue;

            var alert = new CustomAlertView(
                this.appConfiguration,
                title,
                message,
                Strings.Ok,
                Strings.Cancel,
                inputContent,
                handleSubmit: () =>
                {
                    var errorText = inputValidation.Invoke(inputContent.TextField.TextField.Text);
                    if(!string.IsNullOrEmpty(errorText))
                    {
                        inputContent.LblError.Text = errorText;
                        return false;
                    }
                    else
                    {
                        tcs.SetResult(inputContent.TextField.TextField.Text);
                        return true;
                    }
                },
                handleCancel: () => tcs.SetResult(null));
            alert.Show();

            alert.AddGestureRecognizer(new UITapGestureRecognizer(() => inputContent.TextField.TextField.ResignFirstResponder()));
            inputContent.TextField.TextField.BecomeFirstResponder();

            return tcs.Task;
        }

        public Task<Tuple<string, string>> ShowDoubleInputFieldsAsync(string title, string message, Func<string, string> inputValidation, InputType inputType = InputType.None, string defaultFirstValue = null, string defaultSecondValue = null)
        {
            var tcs = new TaskCompletionSource<Tuple<string, string>>();

            var inputContent = new DoubleInputContentView();
            inputContent.TextField1.TextField.Text = defaultFirstValue;
            inputContent.TextField2.TextField.Text = defaultSecondValue;

            var alert = new CustomAlertView(
                this.appConfiguration,
                title,
                message,
                Strings.Ok,
                Strings.Cancel,
                inputContent,
                handleSubmit: () =>
                {
                    var errorText1 = inputValidation.Invoke(inputContent.TextField1.TextField.Text);
                    var errorText2 = inputValidation.Invoke(inputContent.TextField2.TextField.Text);

                    inputContent.LblError1.Text = errorText1 ?? string.Empty;
                    inputContent.LblError2.Text = errorText1 ?? string.Empty;

                    if(!string.IsNullOrEmpty(errorText1) || !string.IsNullOrEmpty(errorText2))
                    {
                        return false;
                    }
                    else
                    {
                        tcs.SetResult(new Tuple<string, string>(inputContent.TextField1.TextField.Text, inputContent.TextField2.TextField.Text));
                        return true;
                    }
                },
                handleCancel: () => tcs.SetResult(null));
            alert.Show();

            alert.AddGestureRecognizer(new UITapGestureRecognizer(
                () =>
                {
                    inputContent.TextField1.TextField.ResignFirstResponder();
                    inputContent.TextField2.TextField.ResignFirstResponder();
                }));

            inputContent.TextField1.TextField.BecomeFirstResponder();

            return tcs.Task;
        }

        public Task<DateTime?> ShowDateInputFieldAsync(DateTime? defaultValue = default(DateTime?))
        {
            var tcs = new TaskCompletionSource<DateTime?>();

            var appDelegate = UIApplication.SharedApplication.Delegate as BaseAppDelegate;

            var modalPickerDialog = new ModalPickerViewController(
                ModalPickerType.Date,
                string.Empty,
                appDelegate.Presenter.GetTopViewController(appDelegate.Window.RootViewController),
                Strings.Ok,
                Strings.Cancel,
                UIColor.White,
                IDYCColors.DarkGrayTextColor,
                UIColor.White);

            modalPickerDialog.DatePicker.SetDate(defaultValue.Value.ToNsDate(), false);

            modalPickerDialog.OnModalPickerDismissed += (sender, e) =>
            {
                tcs.SetResult(modalPickerDialog.DatePicker.Date?.ToDateTime());
            };
            modalPickerDialog.OnModalPickerCanceled += (sender, e) =>
            {
                tcs.SetResult(null);
            };

            appDelegate.Presenter.PresentModalViewController(modalPickerDialog, true);

            return tcs.Task;
        }

        public Task<DateTime?> ShowTimeInputFieldAsync(DateTime? defaultValue = default(DateTime?))
        {
            var tcs = new TaskCompletionSource<DateTime?>();

            var appDelegate = UIApplication.SharedApplication.Delegate as BaseAppDelegate;

            var modalPickerDialog = new ModalPickerViewController(
                ModalPickerType.Time,
                string.Empty,
                appDelegate.Presenter.GetTopViewController(appDelegate.Window.RootViewController),
                Strings.Ok,
                Strings.Cancel,
                UIColor.White,
                IDYCColors.DarkGrayTextColor,
                UIColor.White);

            modalPickerDialog.DatePicker.SetDate(defaultValue.Value.ToNsDate(), false);

            modalPickerDialog.OnModalPickerDismissed += (sender, e) =>
            {
                tcs.SetResult(modalPickerDialog.DatePicker.Date?.ToDateTime());
            };
            modalPickerDialog.OnModalPickerCanceled += (sender, e) =>
            {
                tcs.SetResult(null);
            };

            appDelegate.Presenter.PresentModalViewController(modalPickerDialog, true);

            return tcs.Task;
        }

        public Task<string> ShowOptionsAlertAsync(string title, IEnumerable<string> options)
        {
            var tcs = new TaskCompletionSource<string>();

            var optionsContent = new OptionsView();

            var alert = new CustomAlertView(
                this.appConfiguration,
                title: title,
                contentView: optionsContent,
                handleCancel: () =>
                {
                    if(tcs.Task.Status != TaskStatus.RanToCompletion)
                        tcs.SetResult(null);
                });

            foreach(var option in options)
            {
                var label = new IDYCLabel(Metrics.FontSizeNormal)
                {
                    Text = option,
                    TextColor = UIColor.Black
                };
                label.UserInteractionEnabled = true;
                label.AddGestureRecognizer(new TagTapGestureRecognizer(option, (gesture) =>
                {
                    alert.Hide();
                    tcs.SetResult(((TagTapGestureRecognizer)gesture).Tag);
                }));
                optionsContent.OptionsStack.AddArrangedSubview(label);
            }

            alert.Show();

            alert.AlertBackgroundColor = UIColor.White;
            alert.AlertTitleColor = UIColor.Black;

            return tcs.Task;
        }

        public void DisplayEmailContactsExplanation()
        {
            var popoverVC = new SyncEmailExplanationView();

            popoverVC.ModalPresentationStyle = UIModalPresentationStyle.Popover;

            var appDelegate = UIApplication.SharedApplication.Delegate as BaseAppDelegate;

            var mBackingView = new UIView { TranslatesAutoresizingMaskIntoConstraints = false };
            mBackingView.BackgroundColor = UIColor.FromWhiteAlpha(0.1f, 0.5f);

            var parentView = appDelegate.Window.RootViewController.View;
            parentView.AddSubview(mBackingView);
            parentView.AddConstraints(
                mBackingView.AtLeftOf(parentView),
                mBackingView.AtTopOf(parentView),
                mBackingView.AtRightOf(parentView),
                mBackingView.AtBottomOf(parentView)
            );

            var popover = popoverVC.PopoverPresentationController;
            popover.Delegate = new CustomPopoverDelegate(mBackingView);
            popover.SourceView = appDelegate.Window.RootViewController.View;
            popover.PermittedArrowDirections = 0;
            popover.SourceRect = new CoreGraphics.CGRect(
                appDelegate.Window.RootViewController.View.Bounds.Width / 2,
                appDelegate.Window.RootViewController.View.Bounds.Height / 2,
                1,
                1);

            appDelegate.Presenter.PresentModalViewController(popoverVC, true);
        }
    }

    public class DateDelegate : MDDatePickerDialogDelegate
    {
        private TaskCompletionSource<DateTime?> taskCompletionSource;

        public DateDelegate(TaskCompletionSource<DateTime?> taskCompletionSource)
        {
            this.taskCompletionSource = taskCompletionSource;
        }

        public override void DatePickerDialogDidSelectDate(NSDate date)
        {
            var datetime = date.ToDateTime();

            this.taskCompletionSource.SetResult(datetime);
        }
    }

    public class TimeDelegate : MDTimePickerDialogDelegate
    {
        private TaskCompletionSource<DateTime?> taskCompletionSource;

        public TimeDelegate(TaskCompletionSource<DateTime?> taskCompletionSource)
        {
            this.taskCompletionSource = taskCompletionSource;
        }

        public override void DidSelectHour(MDTimePickerDialog timePickerDialog, nint hour, nint minute)
        {
            var time = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, (int)hour, (int)minute, 0);

            this.taskCompletionSource.SetResult(time);
        }
    }
}
