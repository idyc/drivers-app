﻿using DGenix.Mobile.Fwk.iOS;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Handlers;
using DGenix.Mobile.Fwk.Core.Handlers;
using MvvmCross.Binding.Bindings.Target.Construction;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.MvxBindings;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS
{
    public abstract class IDYCSetup : BaseSetup
    {
        protected IDYCSetup(MvxApplicationDelegate delg, MvxBaseIosViewPresenter presenter)
            : base(delg, presenter)
        {

        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();

            Mvx.RegisterType<IInputFieldHandler, InputFieldHandler>();
            Mvx.RegisterType<ISnackbarHandler, IDYCSnackbarHandler>();
        }

        protected override MvvmCross.Binding.Bindings.Target.Construction.IMvxTargetBindingFactoryRegistry RegisterCustomBindings()
        {
            var registry = base.RegisterCustomBindings();

            registry.RegisterFactory(new MvxCustomBindingFactory<UIView>(MvxBindings.NavigateManually, (view) => new NavigateManuallySnackbarTargetBinding(view)));

            return registry;
        }
    }
}
