﻿using CoreGraphics;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Extensions
{
    public static class UINavigationControllerExtensions
    {
        public static void DisplayBottomShadow(this UINavigationController navigationController)
        {
            var idycNavController = navigationController as IDYCNavigationController;

            if(idycNavController == null || idycNavController.IsBottomShadowDisplayed)
                return;

            idycNavController.NavigationBar.Layer.ShadowColor = UIColor.Gray.CGColor;
            idycNavController.NavigationBar.Layer.ShadowRadius = 2.0f;
            idycNavController.NavigationBar.Layer.ShadowOpacity = 0.8f;
            idycNavController.NavigationBar.Layer.ShadowOffset = new CGSize(2, 2);
            idycNavController.NavigationBar.Layer.ShadowPath = UIBezierPath.FromRect(idycNavController.NavigationBar.Layer.Bounds).CGPath;
            idycNavController.NavigationBar.Layer.MasksToBounds = false;

            idycNavController.IsBottomShadowDisplayed = true;
        }

        public static void HideBottomShadow(this UINavigationController navigationController)
        {
            var idycNavController = navigationController as IDYCNavigationController;

            if(idycNavController == null || !idycNavController.IsBottomShadowDisplayed)
                return;

            idycNavController.NavigationBar.Layer.ShadowColor = UIColor.Clear.CGColor;
            idycNavController.NavigationBar.Layer.ShadowRadius = 0;
            idycNavController.NavigationBar.Layer.ShadowOpacity = 0;
            idycNavController.NavigationBar.Layer.ShadowOffset = new CGSize(0, 0);
            //idycNavController.NavigationBar.Layer.ShadowPath = null;
            //idycNavController.NavigationBar.Layer.MasksToBounds = false;

            idycNavController.IsBottomShadowDisplayed = false;
        }
    }
}
