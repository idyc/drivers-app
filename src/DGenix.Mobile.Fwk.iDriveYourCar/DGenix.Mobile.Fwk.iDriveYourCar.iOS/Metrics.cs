﻿using System;
namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS
{
    public static class Metrics
    {
        // fonts
        public const string RobotoRegular = "Roboto-Regular";
        public const string RobotoLight = "Roboto-Light";
        public const string RobotoMedium = "Roboto-Medium";

        // font sizes
        public const float FontSizeTitanic = 24f;
        public const float FontSizeGigant = 22f;
        public const float FontSizeHuge = 18f;
        public const float FontSizeBig = 16f;
        public const float FontSizeNormalPlus = 15f;
        public const float FontSizeNormal = 14f;
        public const float FontSizeNormalMinus = 13f;
        public const float FontSizeSmall = 12f;
        public const float FontSizeTiny = 10f;

        // dimensions
        public const float LineThickness = 1f;

        public const float WINDOW_MARGIN = 8f;

        public const float DEFAULT_DISTANCE = 16f;

        public const float PopoverMenuRowHeight = 40f;

        public const float SwipeButtonHeight = 64f;
    }
}
