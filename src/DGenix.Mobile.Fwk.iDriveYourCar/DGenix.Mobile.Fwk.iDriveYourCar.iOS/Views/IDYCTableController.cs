﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iOS.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views
{
	public abstract class IDYCTableController<TViewModel> : BaseTableController<TViewModel>
		where TViewModel : FwkBaseViewModel
	{
		private IDYCLabel lblTitle;
		public new string Title
		{
			get
			{
				return this.lblTitle.Text;
			}
			set
			{
				this.lblTitle.Text = value;
			}
		}

		protected IDYCTableController()
		{

		}

		protected IDYCTableController(IntPtr handle) : base(handle)
		{
		}

		public override void CreateViews()
		{
			base.CreateViews();

			this.lblTitle = new IDYCLabel(Metrics.FontSizeBig);
			this.lblTitle.Frame = new CoreGraphics.CGRect(0, 40, this.View.Frame.Width, 40);
			this.lblTitle.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			this.lblTitle.TextColor = UIColor.White;
			this.lblTitle.TextAlignment = UITextAlignment.Left;
			this.NavigationItem.TitleView = this.lblTitle;

			this.EdgesForExtendedLayout = UIKit.UIRectEdge.None;

			this.View.BackgroundColor = IDYCColors.BackgroundColor;
		}

		public override void CreateConstraints()
		{
			base.CreateConstraints();
		}

		public override void CreateMvxBindings()
		{
			base.CreateMvxBindings();
		}

		public override void MvxSubscribe()
		{
			base.MvxSubscribe();
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			if(this.NavigationController != null && this.NavigationController.NavigationBar.BackItem != null)
				this.NavigationController.NavigationBar.BackItem.Title = string.Empty;
		}

		public override void MvxUnsubscribe()
		{
			base.MvxUnsubscribe();
		}
	}
}
