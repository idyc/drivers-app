﻿using System;
using MaterialControls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Delegates
{
	public class TabBarDelegate : MDTabBarDelegate
	{
		public EventHandler<int> SelectedIndexChanged;

		public TabBarDelegate()
		{
		}

		public override void DidChangeSelectedIndex(MDTabBar tabBar, nuint selectedIndex)
		{
			this.SelectedIndexChanged?.Invoke(tabBar, (int)selectedIndex);
		}

	}
}
