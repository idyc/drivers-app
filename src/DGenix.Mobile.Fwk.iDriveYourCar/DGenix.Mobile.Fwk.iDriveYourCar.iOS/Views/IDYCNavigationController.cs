﻿using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views
{
    public class IDYCNavigationController : UINavigationController
    {
        public bool IsBottomShadowDisplayed { get; set; }

        public IDYCNavigationController() : base()
        {
        }

        public IDYCNavigationController(UIViewController rootViewController) : base(rootViewController)
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.DisplayBottomShadow();
        }
    }
}
