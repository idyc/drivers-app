﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views
{
    public abstract class IDYCController<TViewModel> : BaseController<TViewModel>
        where TViewModel : FwkBaseViewModel
    {
        private IDYCLabel lblTitle;

        private bool showShadow = false;

        public new string Title
        {
            get
            {
                return this.lblTitle.Text;
            }
            set
            {
                this.lblTitle.Text = value;
            }
        }

        protected IDYCController()
        {

        }

        protected IDYCController(IntPtr handle) : base(handle)
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.lblTitle = new IDYCLabel(Metrics.FontSizeBig);
            this.lblTitle.Frame = new CoreGraphics.CGRect(0, 40, this.View.Frame.Width, 40);
            this.lblTitle.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            this.lblTitle.TextColor = UIColor.White;
            this.lblTitle.TextAlignment = UITextAlignment.Left;
            this.NavigationItem.TitleView = this.lblTitle;

            this.EdgesForExtendedLayout = UIRectEdge.None;

            this.View.BackgroundColor = UIColor.White;
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (this.NavigationController != null && this.NavigationController.NavigationBar.BackItem != null)
                this.NavigationController.NavigationBar.BackItem.Title = string.Empty;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();
        }

        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            if (this.NavigationController != null && this.NavigationController is IDYCNavigationController)
            {
                var navigationController = (IDYCNavigationController)this.NavigationController;
                if (showShadow)
                    navigationController.DisplayBottomShadow();
            }
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            base.WillRotate(toInterfaceOrientation, duration);

            if (this.NavigationController != null && this.NavigationController is IDYCNavigationController)
            {
                var navigationController = (IDYCNavigationController)this.NavigationController;
                if (navigationController.IsBottomShadowDisplayed)
                {
                    showShadow = true;
                    navigationController.HideBottomShadow();
                }
            }
        }
    }
}
