﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Delegates;
using DGenix.Mobile.Fwk.iOS.Controls;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views
{
    public class IDYCCurrentTripController<TViewModel> : IDYCController<TViewModel>
        where TViewModel : FwkBaseViewModel, IWithCurrentTripMenuNavigationActionsWrapper
    {
        protected IDYCPopoverMenuView menuMorePopover;

        protected UIBarButtonItem barBtnMore;
        private UIButton btnMore;

        public ICommand CallPassengerCommand { get; set; }

        public ICommand TextPassengerCommand { get; set; }

        public ICommand AddExpenseCommand { get; set; }

        public ICommand MessageDispatchCommand { get; set; }

        public ICommand CallDispatchCommand { get; set; }

        public IDYCCurrentTripController()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.barBtnMore = new UIBarButtonItem(UIImage.FromBundle("ic_more"), UIBarButtonItemStyle.Plain, this.BtnMore_ClickHandler);
            this.NavigationItem.SetRightBarButtonItems(new UIBarButtonItem[] { this.barBtnMore }, false);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<IDYCCurrentTripController<TViewModel>, IWithCurrentTripMenuNavigationActionsWrapper>();
            set.Bind(this).For(v => v.CallPassengerCommand).To(vm => vm.CurrentTripMenuNavigationActionsWrapper.CallPassengerCommand);
            set.Bind(this).For(v => v.TextPassengerCommand).To(vm => vm.CurrentTripMenuNavigationActionsWrapper.TextPassengerCommand);
            set.Bind(this).For(v => v.AddExpenseCommand).To(vm => vm.CurrentTripMenuNavigationActionsWrapper.AddExpenseCommand);
            set.Bind(this).For(v => v.MessageDispatchCommand).To(vm => vm.CurrentTripMenuNavigationActionsWrapper.MessageDispatchCommand);
            set.Bind(this).For(v => v.CallDispatchCommand).To(vm => vm.CurrentTripMenuNavigationActionsWrapper.CallDispatchCommand);
            set.Apply();
        }

        private void BtnMore_ClickHandler(object sender, EventArgs args)
        {
            var popoverItems = this.GetItemsForPopoverMenu();
            this.menuMorePopover = new IDYCPopoverMenuView(popoverItems);
            this.menuMorePopover.ModalPresentationStyle = UIModalPresentationStyle.Popover;
            this.menuMorePopover.PreferredContentSize = new CoreGraphics.CGSize(
                186f, 32f + Metrics.PopoverMenuRowHeight * popoverItems.Count);

            var popover = this.menuMorePopover.PopoverPresentationController;
            popover.BarButtonItem = this.barBtnMore;
            popover.Delegate = new CustomPopoverDelegate();
            popover.PermittedArrowDirections = 0;
            this.PresentViewController(this.menuMorePopover, true, null);
        }

        protected virtual IList<Tuple<string, ICommand>> GetItemsForPopoverMenu()
        {
            return new List<Tuple<string, ICommand>>
            {
                new Tuple<string, ICommand>(IDriveYourCarStrings.CallPassenger, this.CallPassengerCommand),
                new Tuple<string, ICommand>(IDriveYourCarStrings.MessagePassenger, this.TextPassengerCommand),
                new Tuple<string, ICommand>(IDriveYourCarStrings.AddExpense, this.AddExpenseCommand),
                new Tuple<string, ICommand>(IDriveYourCarStrings.MessageDispatch, this.MessageDispatchCommand),
                new Tuple<string, ICommand>(IDriveYourCarStrings.CallDispatch, this.CallDispatchCommand)
            };
        }
    }
}
