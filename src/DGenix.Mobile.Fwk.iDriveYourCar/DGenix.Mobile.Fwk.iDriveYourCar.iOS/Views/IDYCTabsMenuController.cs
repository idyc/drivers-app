﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Delegates;
using DGenix.Mobile.Fwk.iOS.Extensions;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views
{
    public class IDYCTabsMenuController<TViewModel> : IDYCMenuController<TViewModel>, IUIPageViewControllerDataSource
        where TViewModel : FwkBaseViewModel, IWithMenuNavigationActionsWrapper
    {
        protected const float TABS_BOTTOM_MARGIN = 2f;

        protected virtual float TopTabBarHeight => 48f;

        protected virtual float TabsControllerMarginBottom => 0;

        protected IDYCTabBar topTabBar;
        protected TabBarDelegate tabBarDelegate;
        protected UIPageViewController pageViewController;
        protected List<UIViewController> tabsViewControllers;

        protected int currentSelectedTab;

        public IDYCTabsMenuController()
        {
            this.tabsViewControllers = new List<UIViewController>();
        }

        public override void CreateViews()
        {
            base.CreateViews();

            // create TopTabBar
            this.topTabBar = new IDYCTabBar();
            this.topTabBar.HorizontalInset = 0f;
            this.topTabBar.TextFont = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeNormal);

            // create TopTabBar delegate
            this.tabBarDelegate = new TabBarDelegate();
            this.topTabBar.Delegate = this.tabBarDelegate;
            this.topTabBar.BackgroundColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor();

            // create PageViewController
            this.pageViewController = new UIPageViewController(UIPageViewControllerTransitionStyle.Scroll, UIPageViewControllerNavigationOrientation.Horizontal, UIPageViewControllerSpineLocation.None);
            this.pageViewController.DataSource = this;

            this.AddChildViewController(this.pageViewController);
            this.View.AggregateSubviews(this.topTabBar, this.pageViewController.View);


            this.pageViewController.DidMoveToParentViewController(this);
            this.pageViewController.DidFinishAnimating += this.PageViewController_DidFinishAnimating;
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();

            this.View.AddConstraints(
                this.topTabBar.AtTopOf(this.View),
                this.topTabBar.AtLeftOf(this.View),
                this.topTabBar.AtRightOf(this.View),
                this.topTabBar.Height().EqualTo(this.TopTabBarHeight),

                this.pageViewController.View.Below(this.topTabBar, TABS_BOTTOM_MARGIN),
                this.pageViewController.View.AtLeftOf(this.View),
                this.pageViewController.View.AtRightOf(this.View),
                this.pageViewController.View.AtBottomOf(this.View, this.TabsControllerMarginBottom)
            );
        }

        public override void MvxSubscribe()
        {
            base.MvxSubscribe();

            this.tabBarDelegate.SelectedIndexChanged += this.TabBarSelectedIndexChanged;
        }

        public override void MvxUnsubscribe()
        {
            base.MvxUnsubscribe();

            this.tabBarDelegate.SelectedIndexChanged -= this.TabBarSelectedIndexChanged;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.NavigationController.HideBottomShadow();
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            this.NavigationController.DisplayBottomShadow();
        }

        void TabBarSelectedIndexChanged(object sender, int e)
        {
            this.pageViewController.SetViewControllers(
                new UIViewController[] { this.tabsViewControllers[e] },
                e > this.currentSelectedTab ? UIPageViewControllerNavigationDirection.Forward : UIPageViewControllerNavigationDirection.Reverse,
                true,
                null);

            this.currentSelectedTab = e;
        }

        private bool CanDisplayDesiredTab(int newTab)
        {
            return newTab >= 0 && newTab < this.tabsViewControllers.Count;
        }

        public UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            var oldIndex = this.tabsViewControllers.IndexOf(referenceViewController);

            if(!this.CanDisplayDesiredTab(oldIndex - 1))
                return null;

            return this.tabsViewControllers[oldIndex - 1];
        }

        public UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            var oldIndex = this.tabsViewControllers.IndexOf(referenceViewController);

            if(!this.CanDisplayDesiredTab(oldIndex + 1))
                return null;

            return this.tabsViewControllers[oldIndex + 1];
        }

        void PageViewController_DidFinishAnimating(object sender, UIPageViewFinishedAnimationEventArgs e)
        {
            if(e.Completed)
            {
                var currentViewController = this.pageViewController.ViewControllers.First();

                this.currentSelectedTab = this.tabsViewControllers.IndexOf(currentViewController);

                this.topTabBar.SelectedIndex = (nuint)this.currentSelectedTab;
            }
        }
    }
}
