﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Controls;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views
{
    public class IDYCMenuController<TViewModel> : IDYCController<TViewModel>
        where TViewModel : FwkBaseViewModel, IWithMenuNavigationActionsWrapper
    {
        protected IDYCPopoverMenuView menuMorePopover;

        protected UIBarButtonItem barBtnMore;

        public ICommand CallDispatchCommand { get; set; }
        public ICommand MessageDispatchCommand { get; set; }

        public IDYCMenuController()
        {
        }

        public override void CreateViews()
        {
            base.CreateViews();

            this.barBtnMore = new UIBarButtonItem(UIImage.FromBundle("ic_more"), UIBarButtonItemStyle.Plain, this.BtnMore_ClickHandler);
            this.NavigationItem.SetRightBarButtonItems(new UIBarButtonItem[] { this.barBtnMore }, false);
        }

        public override void CreateConstraints()
        {
            base.CreateConstraints();
        }

        public override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<IDYCMenuController<TViewModel>, IWithMenuNavigationActionsWrapper>();
            set.Bind(this).For(v => v.CallDispatchCommand).To(vm => vm.MenuNavigationActionsWrapper.CallDispatchCommand);
            set.Bind(this).For(v => v.MessageDispatchCommand).To(vm => vm.MenuNavigationActionsWrapper.MessageDispatchCommand);
            set.Apply();
        }

        private void BtnMore_ClickHandler(object sender, EventArgs args)
        {
            var popoverItems = this.GetItemsForPopoverMenu();
            this.menuMorePopover = new IDYCPopoverMenuView(popoverItems);

            this.menuMorePopover.ModalPresentationStyle = UIModalPresentationStyle.Popover;
            this.menuMorePopover.PreferredContentSize = new CoreGraphics.CGSize(
                186f, 32f + Metrics.PopoverMenuRowHeight * popoverItems.Count);

            var popover = this.menuMorePopover.PopoverPresentationController;
            popover.BarButtonItem = this.barBtnMore;
            popover.Delegate = new CustomPopoverDelegate();
            popover.PermittedArrowDirections = 0;
            this.PresentViewController(this.menuMorePopover, true, null);
        }

        protected virtual IList<Tuple<string, ICommand>> GetItemsForPopoverMenu()
        {
            return new List<Tuple<string, ICommand>>
            {
                new Tuple<string, ICommand>(IDriveYourCarStrings.CallDispatch, this.CallDispatchCommand),
                new Tuple<string, ICommand>(IDriveYourCarStrings.MessageDispatch, this.MessageDispatchCommand),
            };
        }
    }
}
