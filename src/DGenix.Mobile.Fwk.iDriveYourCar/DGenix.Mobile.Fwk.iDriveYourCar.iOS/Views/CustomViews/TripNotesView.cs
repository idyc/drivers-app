﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripNotesView : IDYCView
    {
        public UIImageView Icon { get; set; }

        public IDYCLabel Title { get; set; }

        public IDYCMultilineLabel Text { get; set; }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Icon = new UIImageView();
            this.Title = new IDYCLabel(Metrics.FontSizeNormal);
            this.Text = new IDYCMultilineLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.AggregateSubviews(this.Icon, this.Title, this.Text);
        }

        public TripNotesView()
        {

        }

        public TripNotesView(string iconName)
        {
            if(!string.IsNullOrEmpty(iconName))
                this.Icon.Image = UIImage.FromBundle(iconName);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Icon.AtLeftOf(this, Metrics.WINDOW_MARGIN * 2),
                this.Icon.AtTopOf(this, Metrics.DEFAULT_DISTANCE),
                this.Icon.Width().EqualTo(18f),
                this.Icon.Height().EqualTo().WidthOf(this.Icon),

                this.Title.WithSameTop(this.Icon),
                this.Title.ToRightOf(this.Icon, Metrics.DEFAULT_DISTANCE),
                this.Title.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.Text.Below(this.Title),
                this.Text.WithSameLeft(this.Title),
                this.Text.WithSameRight(this.Title),
                this.Text.AtBottomOf(this, Metrics.DEFAULT_DISTANCE)
            );
        }
    }
}
