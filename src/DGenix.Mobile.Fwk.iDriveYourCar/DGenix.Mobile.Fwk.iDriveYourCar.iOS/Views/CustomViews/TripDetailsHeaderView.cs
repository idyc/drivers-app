﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;
using MvvmCross.Plugins.Color.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripDetailsHeaderView : IDYCView
    {
        private const float HORIZONTAL_CONTROLS_DISTANCE = 4f;
        private const float VERTICAL_CONTROLS_DISTANCE = 4f;

        public IDYCLabel Title { get; set; }

        public CardView ReturnTrip { get; set; }

        public IDYCLabel TripDate { get; set; }

        public IDYCLabel TripTime { get; set; }

        public IDYCLabel Footer { get; set; }

        public UIView ViewLine { get; set; }

        private IDYCLabel lblRT;

        public TripDetailsHeaderView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Title = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.ReturnTrip = new CardView
            {
                BackgroundColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor(),
                UserInteractionEnabled = true
            };
            this.lblRT = new IDYCLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.ReturnTripTag,
                TextColor = UIColor.White
            };
            this.TripDate = new IDYCLabel(Metrics.FontSizeSmall);

            this.TripTime = new IDYCLabel(Metrics.FontSizeSmall, Metrics.RobotoMedium);

            this.Footer = new IDYCLabel(Metrics.FontSizeSmall)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.ViewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(
                this.Title,
                this.ReturnTrip,
                this.TripDate,
                this.TripTime,
                this.Footer,
                this.ViewLine);
            this.ReturnTrip.AggregateSubviews(this.lblRT);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Title.AtLeftOf(this, Metrics.WINDOW_MARGIN * 2),
                this.Title.AtTopOf(this, Metrics.WINDOW_MARGIN * 3),
                this.Title.Right().LessThanOrEqualTo().LeftOf(this.ReturnTrip).Minus(HORIZONTAL_CONTROLS_DISTANCE * 2),

                this.ReturnTrip.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),
                this.ReturnTrip.WithSameCenterY(this.Title),
                this.ReturnTrip.Width().EqualTo(25f),
                this.ReturnTrip.Height().EqualTo(20f),

                this.TripDate.Below(this.Title, VERTICAL_CONTROLS_DISTANCE),
                this.TripDate.WithSameLeft(this.Title),

                this.TripTime.WithSameRight(this.ReturnTrip),
                this.TripTime.WithSameCenterY(this.TripDate),

                this.Footer.Below(this.TripDate, VERTICAL_CONTROLS_DISTANCE),
                this.Footer.WithSameLeft(this.Title),
                this.Footer.WithSameRight(this.ReturnTrip),

                this.ViewLine.Below(this.Footer, Metrics.DEFAULT_DISTANCE),
                this.ViewLine.AtLeftOf(this),
                this.ViewLine.AtRightOf(this),
                this.ViewLine.Height().EqualTo(Metrics.LineThickness),
                this.ViewLine.AtBottomOf(this)
            );

            this.ReturnTrip.AddConstraints(
                this.lblRT.AtLeftOf(this.ReturnTrip, VERTICAL_CONTROLS_DISTANCE),
                this.lblRT.AtTopOf(this.ReturnTrip),
                this.lblRT.AtRightOf(this.ReturnTrip, VERTICAL_CONTROLS_DISTANCE),
                this.lblRT.AtBottomOf(this.ReturnTrip)
            );
        }
    }
}
