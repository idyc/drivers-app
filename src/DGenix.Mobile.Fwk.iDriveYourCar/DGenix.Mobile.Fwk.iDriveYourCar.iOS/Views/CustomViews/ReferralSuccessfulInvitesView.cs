﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Controls;
using MvvmCross.Plugins.Color.iOS;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralSuccessfulInvitesView<TView, TItemType> : IDYCView
        where TView : BindableBaseView, new()
        where TItemType : class
    {
        private const float MARGIN = 24f;

        public IDYCLabel LblSuccessfulInvites { get; set; }

        public IDYCLabel LblSuccessfulInvitesValue { get; set; }

        public SimpleBindableStackView<TView, TItemType> SuccessfulInvitesStackView { get; set; }

        public ReferralSuccessfulInvitesView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblSuccessfulInvites = new IDYCLabel(Metrics.FontSizeHuge)
            {
                Text = IDriveYourCarStrings.SuccessfulInvites,
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UIKit.UITextAlignment.Right
            };
            this.LblSuccessfulInvitesValue = new IDYCLabel(28f)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor(),
                TextAlignment = UIKit.UITextAlignment.Right
            };
            this.SuccessfulInvitesStackView = new SimpleBindableStackView<TView, TItemType>(UIKit.UILayoutConstraintAxis.Vertical, 0f);

            this.AggregateSubviews(this.LblSuccessfulInvites, this.LblSuccessfulInvitesValue, this.SuccessfulInvitesStackView);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblSuccessfulInvites.AtTopOf(this, MARGIN),
                this.LblSuccessfulInvites.AtRightOf(this, MARGIN),
                this.LblSuccessfulInvites.AtLeftOf(this, MARGIN),

                this.LblSuccessfulInvitesValue.Below(this.LblSuccessfulInvites),
                this.LblSuccessfulInvitesValue.AtRightOf(this, MARGIN),
                this.LblSuccessfulInvitesValue.AtLeftOf(this, MARGIN),

                this.SuccessfulInvitesStackView.Below(this.LblSuccessfulInvitesValue, MARGIN),
                this.SuccessfulInvitesStackView.AtLeftOf(this),
                this.SuccessfulInvitesStackView.AtRightOf(this),
                this.SuccessfulInvitesStackView.AtBottomOf(this)
            );
        }
    }
}
