﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using CoreGraphics;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralSocialView : IDYCView
    {
        private const float TOP_MARGIN = 32f;
        private const float CONTROLS_DISTANCE = 8f;
        private const float REGION_VERTICAL_DISTANCE = 32f;
        private const float CARD_SOCIAL_MARGIN = 12f;

        public IDYCMultilineLabel LblInviteWithYourCode { get; set; }

        public IDYCMultilineLabel LblSendYourFriends { get; set; }

        public IDYCLabel LblYourInviteCode { get; set; }

        public UserCodeView UserCode { get; set; }

        public IDYCLabel LblShareOrTweet { get; set; }

        public CardView BtnFacebook { get; set; }

        public CardView BtnTwitter { get; set; }

        public UIView ViewLine { get; set; }

        public UIImageView ImgFacebook { get; set; }

        public UIImageView ImgTwitter { get; set; }

        public ReferralSocialView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblInviteWithYourCode = new IDYCMultilineLabel(Metrics.FontSizeGigant)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.InviteWithYourCode
            };
            this.LblSendYourFriends = new IDYCMultilineLabel(Metrics.FontSizeNormalMinus)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                Text = IDriveYourCarStrings.SendYourFriendsYourUniqueInviteCode
            };
            this.LblYourInviteCode = new IDYCLabel(Metrics.FontSizeNormalMinus, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.YourInviteCode
            };

            this.UserCode = new UserCodeView();

            this.LblShareOrTweet = new IDYCLabel(Metrics.FontSizeNormalMinus, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.ShareOrTweet
            };

            this.BtnFacebook = new CardView { UserInteractionEnabled = true };
            this.BtnFacebook.Layer.BorderWidth = Metrics.LineThickness;
            this.BtnFacebook.Layer.BorderColor = IDYCColors.BorderColor.CGColor;

            this.BtnTwitter = new CardView { UserInteractionEnabled = true };
            this.BtnTwitter.Layer.BorderWidth = Metrics.LineThickness;
            this.BtnTwitter.Layer.BorderColor = IDYCColors.BorderColor.CGColor;

            this.ImgFacebook = new UIImageView(UIImage.FromBundle(Assets.Facebook));
            this.ImgTwitter = new UIImageView(UIImage.FromBundle(Assets.Twitter));

            this.ViewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(
                this.LblInviteWithYourCode,
                this.LblSendYourFriends,
                this.LblYourInviteCode,
                this.UserCode,
                this.LblShareOrTweet,
                this.BtnFacebook,
                this.BtnTwitter,
                this.ViewLine);

            this.BtnFacebook.AggregateSubviews(this.ImgFacebook);
            this.BtnTwitter.AggregateSubviews(this.ImgTwitter);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblInviteWithYourCode.AtLeftOf(this),
                this.LblInviteWithYourCode.AtTopOf(this, TOP_MARGIN),
                this.LblInviteWithYourCode.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),

                this.LblSendYourFriends.Below(this.LblInviteWithYourCode, CONTROLS_DISTANCE),
                this.LblSendYourFriends.WithSameLeft(this.LblInviteWithYourCode),
                this.LblSendYourFriends.WithSameRight(this.LblInviteWithYourCode),

                this.LblYourInviteCode.Below(this.LblSendYourFriends, REGION_VERTICAL_DISTANCE),
                this.LblYourInviteCode.WithSameLeft(this.LblInviteWithYourCode),
                this.LblYourInviteCode.WithSameRight(this.LblInviteWithYourCode),

                this.UserCode.Below(this.LblYourInviteCode, CONTROLS_DISTANCE),
                this.UserCode.WithSameLeft(this.LblInviteWithYourCode),
                this.UserCode.WithSameRight(this.LblInviteWithYourCode),

                this.LblShareOrTweet.Below(this.UserCode, REGION_VERTICAL_DISTANCE / 2),
                this.LblShareOrTweet.WithSameLeft(this.LblInviteWithYourCode),
                this.LblShareOrTweet.WithSameRight(this.LblInviteWithYourCode),

                this.BtnFacebook.Below(this.LblShareOrTweet, CONTROLS_DISTANCE),
                this.BtnFacebook.WithSameLeft(this.LblInviteWithYourCode),

                this.BtnTwitter.ToRightOf(this.BtnFacebook, CONTROLS_DISTANCE),
                this.BtnTwitter.WithSameTop(this.BtnFacebook),

                this.ViewLine.Below(this.BtnFacebook, REGION_VERTICAL_DISTANCE),
                this.ViewLine.WithSameLeft(this.LblInviteWithYourCode),
                this.ViewLine.WithSameRight(this.LblInviteWithYourCode),
                this.ViewLine.AtBottomOf(this),
                this.ViewLine.Height().EqualTo(Metrics.LineThickness)
            );

            this.BtnFacebook.AddConstraints(
                this.ImgFacebook.AtLeftOf(this.BtnFacebook, CARD_SOCIAL_MARGIN),
                this.ImgFacebook.AtTopOf(this.BtnFacebook, CARD_SOCIAL_MARGIN),
                this.ImgFacebook.AtRightOf(this.BtnFacebook, CARD_SOCIAL_MARGIN),
                this.ImgFacebook.AtBottomOf(this.BtnFacebook, CARD_SOCIAL_MARGIN),
                this.ImgFacebook.Width().EqualTo(18f),
                this.ImgFacebook.Height().EqualTo(18f)
            );

            this.BtnTwitter.AddConstraints(
                this.ImgTwitter.AtLeftOf(this.BtnTwitter, CARD_SOCIAL_MARGIN),
                this.ImgTwitter.AtTopOf(this.BtnTwitter, CARD_SOCIAL_MARGIN),
                this.ImgTwitter.AtRightOf(this.BtnTwitter, CARD_SOCIAL_MARGIN),
                this.ImgTwitter.AtBottomOf(this.BtnTwitter, CARD_SOCIAL_MARGIN),
                this.ImgTwitter.Width().EqualTo(18f),
                this.ImgTwitter.Height().EqualTo(18f)
            );
        }
    }
}
