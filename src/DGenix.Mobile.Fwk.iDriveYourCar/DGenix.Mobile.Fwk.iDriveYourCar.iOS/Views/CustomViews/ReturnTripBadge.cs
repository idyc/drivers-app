﻿using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReturnTripBadge : PaddingView<IDYCLabel>
    {
        public IDYCLabel Label { get; set; }

        public ReturnTripBadge() : base(new IDYCLabel(Metrics.FontSizeTiny), 3f, 0f)
        {
            this.InnerView.TextColor = IDYCColors.MidGrayTextColor;
            this.InnerView.Text = IDriveYourCarStrings.ReturnTripTag;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.Layer.BorderWidth = 1f;
            this.Layer.CornerRadius = 3f;
            this.Layer.BorderColor = IDYCColors.MidGrayTextColor.CGColor;
        }
    }
}
