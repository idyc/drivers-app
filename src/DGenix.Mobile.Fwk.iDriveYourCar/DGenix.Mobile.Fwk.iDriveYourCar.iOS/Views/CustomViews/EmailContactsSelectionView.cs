﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Sources;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class EmailContactsSelectionView : IDYCView
    {
        private const float MARGIN = 8f;
        private const float HEADER_HEIGHT = 44f;

        public UITableView TblContacts { get; set; }

        public SimpleTableSource<ContactSelectionTableViewCell, EmailInvitation> Source { get; set; }

        public UIView ViewHeader { get; set; }

        public UIButton BtnClose { get; set; }

        public PrimaryTextButton BtnSelectAll { get; set; }

        public PrimaryBackgroundButton BtnSendInvites { get; set; }

        public EmailContactsSelectionView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.TblContacts = new UITableView
            {
                RowHeight = 44f,
                SeparatorStyle = UITableViewCellSeparatorStyle.None
            };
            this.Source = new SimpleTableSource<ContactSelectionTableViewCell, EmailInvitation>(this.TblContacts);
            this.TblContacts.Source = this.Source;

            this.ViewHeader = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.BtnClose = new UIButton();
            this.BtnClose.SetImage(UIImage.FromBundle("ic_close"), UIControlState.Normal);
            this.BtnSelectAll = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeSmall);
            this.BtnSelectAll.SetTitle(IDriveYourCarStrings.SelectAll, UIControlState.Normal);

            this.BtnSendInvites = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.BtnSendInvites.SetTitle(IDriveYourCarStrings.SendInvites.ToUpper(), UIControlState.Normal);
            this.BtnSendInvites.Layer.CornerRadius = 0f;

            this.AggregateSubviews(this.ViewHeader, this.TblContacts, this.BtnSendInvites);
            this.ViewHeader.AggregateSubviews(this.BtnClose, this.BtnSelectAll);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.ViewHeader.AtLeftOf(this),
                this.ViewHeader.AtTopOf(this),
                this.ViewHeader.AtRightOf(this),
                this.ViewHeader.Height().EqualTo(HEADER_HEIGHT),

                this.TblContacts.Below(this.ViewHeader),
                this.TblContacts.AtLeftOf(this),
                this.TblContacts.AtRightOf(this),

                this.BtnSendInvites.Below(this.TblContacts),
                this.BtnSendInvites.AtLeftOf(this),
                this.BtnSendInvites.AtRightOf(this),
                this.BtnSendInvites.AtBottomOf(this),
                this.BtnSendInvites.Height().EqualTo(HEADER_HEIGHT)
            );

            this.ViewHeader.AddConstraints(
                this.BtnClose.AtLeftOf(this.ViewHeader, MARGIN),
                this.BtnClose.WithSameCenterY(this.ViewHeader),

                this.BtnSelectAll.AtRightOf(this.ViewHeader, MARGIN),
                this.BtnSelectAll.WithSameCenterY(this.ViewHeader)
            );
        }
    }
}
