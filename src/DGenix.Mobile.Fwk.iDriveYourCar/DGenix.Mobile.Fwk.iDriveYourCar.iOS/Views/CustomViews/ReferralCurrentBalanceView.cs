﻿using System;
using MvvmCross.Plugins.Color.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralCurrentBalanceView : IDYCView
    {
        private const float VERTICAL_DISTANCE = 32f;
        private const float TEXT_BUTTON_DISTANCE = 24f;

        public IDYCLabel LblCurrentBalance { get; set; }

        public IDYCLabel LblCurrentBalanceValue { get; set; }

        public InviteManuallyButton BtnRedeemBalance { get; set; }

        public ReferralCurrentBalanceView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblCurrentBalanceValue = new IDYCLabel(28f)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor()
            };
            this.LblCurrentBalance = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.CurrentBalance
            };
            this.BtnRedeemBalance = new InviteManuallyButton(this.appConfiguration.Value);
            this.BtnRedeemBalance.SetTitle(IDriveYourCarStrings.RedeemBalance.ToUpper(), UIKit.UIControlState.Normal);

            this.AggregateSubviews(this.LblCurrentBalance, this.LblCurrentBalanceValue, this.BtnRedeemBalance);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblCurrentBalanceValue.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),
                this.LblCurrentBalanceValue.AtTopOf(this, VERTICAL_DISTANCE),

                this.LblCurrentBalance.AtLeftOf(this),
                this.LblCurrentBalance.WithSameBottom(this.LblCurrentBalanceValue).Minus(2f),
                this.LblCurrentBalance.ToLeftOf(this.LblCurrentBalanceValue),

                this.BtnRedeemBalance.Below(this.LblCurrentBalanceValue, TEXT_BUTTON_DISTANCE),
                this.BtnRedeemBalance.WithSameLeft(this.LblCurrentBalance),
                this.BtnRedeemBalance.WithSameRight(this.LblCurrentBalanceValue),
                this.BtnRedeemBalance.AtBottomOf(this, VERTICAL_DISTANCE + 8)
            );
        }
    }
}
