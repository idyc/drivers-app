﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripVehicleLocationView : IDYCView
    {
        public UIImageView Icon { get; set; }

        public IDYCLabel Title { get; set; }

        public IDYCMultilineLabel Text { get; set; }

        public UIView ViewLine { get; set; }

        public TripVehicleLocationView()
        {

        }

        public TripVehicleLocationView(string iconName)
        {
            if(!string.IsNullOrEmpty(iconName))
                this.Icon.Image = UIImage.FromBundle(iconName);
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Icon = new UIImageView();
            this.Title = new IDYCLabel(Metrics.FontSizeNormal)
            {
                Text = IDriveYourCarStrings.VehicleLocation
            };
            this.Text = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.ViewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(this.Icon, this.Title, this.Text, this.ViewLine);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Icon.AtLeftOf(this, Metrics.WINDOW_MARGIN * 2),
                this.Icon.WithSameTop(this.Title),
                this.Icon.Width().EqualTo(18f),
                this.Icon.Height().EqualTo().WidthOf(this.Icon),

                this.Title.ToRightOf(this.Icon, Metrics.DEFAULT_DISTANCE / 2),
                this.Title.AtTopOf(this, Metrics.DEFAULT_DISTANCE),
                this.Title.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.Text.Below(this.Title, Metrics.DEFAULT_DISTANCE / 2),
                this.Text.WithSameLeft(this.Title),
                this.Text.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.ViewLine.Below(this.Text, Metrics.DEFAULT_DISTANCE),
                this.ViewLine.WithSameLeft(this.Title),
                this.ViewLine.AtRightOf(this),
                this.ViewLine.Height().EqualTo(Metrics.LineThickness),
                this.ViewLine.AtBottomOf(this)
            );
        }
    }
}
