﻿using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class UserCodeView : IDYCView
    {
        private const float VERTICAL_PADDING = 8f;
        private const float HORIZONTAL_PADDING = 16f;
        private const float CODE_WIDTH_MULTIPLIER = .6f;
        private const float COPY_WIDTH_MULTIPLIER = .3f;
        private const float CORNER_RADIUS = 3f;
        protected const float SHADOW_OFFSET_WIDTH = 0;
        protected const float SHADOW_OFFSET_HEIGHT = 2f;
        protected const float SHADOW_OPACITY = .4f;

        protected UIColor shadowColor = UIColor.LightGray;

        public IDYCLabel LblCode { get; set; }

        public UIView ViewLineVertical { get; set; }

        private UIView btnCopyWrapper;

        public UIView BtnCopy { get; set; }

        public UIImageView ImgCopy { get; set; }

        public IDYCLabel LblCopy { get; set; }

        public UserCodeView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.White;

            this.LblCode = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.ViewLineVertical = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.btnCopyWrapper = new UIView();
            this.BtnCopy = new UIView
            {
                UserInteractionEnabled = true
            };
            this.ImgCopy = new UIImageView(UIImage.FromBundle("ic_done"));
            this.LblCopy = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor(),
                Text = IDriveYourCarStrings.Copy.ToUpper()
            };
            this.AggregateSubviews(this.LblCode, this.ViewLineVertical, this.btnCopyWrapper);
            this.btnCopyWrapper.AggregateSubviews(this.BtnCopy);
            this.BtnCopy.AggregateSubviews(this.ImgCopy, this.LblCopy);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblCode.AtLeftOf(this, HORIZONTAL_PADDING),
                this.LblCode.AtTopOf(this, VERTICAL_PADDING),
                this.LblCode.AtBottomOf(this, VERTICAL_PADDING),
                this.LblCode.WithRelativeWidth(this, CODE_WIDTH_MULTIPLIER),

                this.btnCopyWrapper.AtRightOf(this, VERTICAL_PADDING),
                this.btnCopyWrapper.WithSameTop(this.LblCode),
                this.btnCopyWrapper.WithSameBottom(this.LblCode),
                this.btnCopyWrapper.WithRelativeWidth(this, COPY_WIDTH_MULTIPLIER),

                this.ViewLineVertical.ToLeftOf(this.btnCopyWrapper),
                this.ViewLineVertical.WithSameTop(this.LblCode),
                this.ViewLineVertical.WithSameBottom(this.LblCode),
                this.ViewLineVertical.Width().EqualTo(Metrics.LineThickness)
            );

            this.btnCopyWrapper.AddConstraints(
                this.BtnCopy.AtTopOf(this.btnCopyWrapper),
                this.BtnCopy.WithSameCenterX(this.btnCopyWrapper),
                this.BtnCopy.AtBottomOf(this.btnCopyWrapper),

                this.BtnCopy.Left().GreaterThanOrEqualTo().LeftOf(this.btnCopyWrapper),
                this.BtnCopy.Right().LessThanOrEqualTo().RightOf(this.btnCopyWrapper)
            );

            this.BtnCopy.AddConstraints(
                this.ImgCopy.AtLeftOf(this.BtnCopy),
                this.ImgCopy.AtTopOf(this.BtnCopy),
                this.ImgCopy.AtBottomOf(this.BtnCopy),

                this.LblCopy.ToRightOf(this.ImgCopy),
                this.LblCopy.AtRightOf(this.BtnCopy),
                this.LblCopy.WithSameTop(this.ImgCopy),
                this.LblCopy.WithSameBottom(this.ImgCopy)
            );
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.Layer.BorderWidth = Metrics.LineThickness;
            this.Layer.BorderColor = IDYCColors.BorderColor.CGColor;

            this.Layer.CornerRadius = CORNER_RADIUS;

            var shadowPath = UIBezierPath.FromRoundedRect(this.Bounds, CORNER_RADIUS);

            this.Layer.MasksToBounds = false;
            this.Layer.ShadowColor = this.shadowColor.CGColor;
            this.Layer.ShadowOffset = new CGSize(SHADOW_OFFSET_WIDTH, SHADOW_OFFSET_HEIGHT);
            this.Layer.ShadowOpacity = SHADOW_OPACITY;
            this.Layer.ShadowPath = shadowPath.CGPath;
        }
    }
}
