﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripSwipeableButton : PannableView
    {
        private UIView swipeContainer;

        public IDYCLabel LblSwipe { get; set; }

        public UIImageView ImgSwipe { get; set; }

        public UIImageView ImgSwiped { get; set; }

        public TripSwipeableButton()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.swipeContainer = new UIView();
            this.LblSwipe = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = UIColor.White
            };
            this.ImgSwipe = new UIImageView();
            this.ImgSwiped = new UIImageView();

            this.AggregateSubviews(this.ImgSwiped);
            this.FrontView.AggregateSubviews(this.swipeContainer);
            this.swipeContainer.AggregateSubviews(this.ImgSwipe, this.LblSwipe);

            this.BringSubviewToFront(this.FrontView);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ImgSwipe.SetContentHuggingPriority(501, UILayoutConstraintAxis.Horizontal);
            this.LblSwipe.SetContentHuggingPriority(499, UILayoutConstraintAxis.Horizontal);

            this.ImgSwiped.SetContentHuggingPriority(501, UILayoutConstraintAxis.Horizontal);

            this.AddConstraints(
                this.swipeContainer.WithSameCenterY(this),
                this.swipeContainer.WithSameCenterX(this)
            );

            this.swipeContainer.AddConstraints(
                this.ImgSwipe.AtLeftOf(this.swipeContainer),
                this.ImgSwipe.AtTopOf(this.swipeContainer),
                this.ImgSwipe.AtBottomOf(this.swipeContainer),

                this.LblSwipe.ToRightOf(this.ImgSwipe),
                this.LblSwipe.WithSameCenterY(this.swipeContainer),
                this.LblSwipe.AtRightOf(this.swipeContainer)
            );

            this.AddConstraints(
                this.ImgSwiped.WithSameCenterX(this),
                this.ImgSwiped.WithSameCenterY(this)
            );
        }
    }
}
