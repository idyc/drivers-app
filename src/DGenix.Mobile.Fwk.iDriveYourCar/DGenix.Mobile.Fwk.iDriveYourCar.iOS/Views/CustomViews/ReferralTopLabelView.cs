﻿using System;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralTopLabelView : IDYCView
    {
        private const float VERTICAL_MARGIN = 24f;

        public IDYCMultilineLabel Label { get; set; }

        public UIView ViewLine { get; set; }


        public ReferralTopLabelView()
        {
        }

        public ReferralTopLabelView(string text)
        {
            this.Label.Text = text;
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Label = new IDYCMultilineLabel(Metrics.FontSizeGigant)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.ViewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(this.Label, this.ViewLine);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Label.AtLeftOf(this),
                this.Label.AtTopOf(this, VERTICAL_MARGIN),
                this.Label.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),

                this.ViewLine.Below(this.Label, VERTICAL_MARGIN),
                this.ViewLine.AtLeftOf(this),
                this.ViewLine.AtRightOf(this),
                this.ViewLine.AtBottomOf(this),
                this.ViewLine.Height().EqualTo(Metrics.LineThickness)
            );
        }
    }
}
