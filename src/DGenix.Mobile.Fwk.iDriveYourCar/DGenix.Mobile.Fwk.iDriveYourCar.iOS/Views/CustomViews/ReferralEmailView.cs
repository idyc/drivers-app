﻿using System;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralEmailView : IDYCView
    {
        private const float VERTICAL_MARGIN = 24f;
        private const float CONTROLS_DISTANCE = 8f;

        public IDYCMultilineLabel LblInviteWithGmail { get; set; }

        public IDYCMultilineLabel LblFindFriends { get; set; }

        public PrimaryTextButton BtnLearnMore { get; set; }

        public PrimaryBackgroundButton BtnInviteFriends { get; set; }

        public UIView ViewLine { get; set; }

        public ReferralEmailView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblInviteWithGmail = new IDYCMultilineLabel(Metrics.FontSizeGigant)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.InviteWithEmail
            };
            this.LblFindFriends = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                Text = IDriveYourCarStrings.FindFriendsToInviteFromYourEmailContacts
            };
            this.BtnLearnMore = new PrimaryTextButton(this.appConfiguration.Value, Metrics.FontSizeNormalMinus);
            this.BtnLearnMore.SetTitle(IDriveYourCarStrings.LearnMore.ToUpper(), UIControlState.Normal);

            this.BtnInviteFriends = new PrimaryBackgroundButton(this.appConfiguration.Value);
            this.BtnInviteFriends.SetTitle(IDriveYourCarStrings.InviteFriends.ToUpper(), UIControlState.Normal);

            this.ViewLine = new UIView { BackgroundColor = IDYCColors.LineColor };
            this.AggregateSubviews(
                this.LblInviteWithGmail,
                this.LblFindFriends,
                this.BtnLearnMore,
                this.BtnInviteFriends,
                this.ViewLine);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblInviteWithGmail.AtLeftOf(this),
                this.LblInviteWithGmail.AtTopOf(this, VERTICAL_MARGIN),
                this.LblInviteWithGmail.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),

                this.LblFindFriends.Below(this.LblInviteWithGmail, CONTROLS_DISTANCE),
                this.LblFindFriends.WithSameLeft(this.LblInviteWithGmail),
                this.LblFindFriends.WithSameRight(this.LblInviteWithGmail),

                this.BtnLearnMore.Below(this.LblFindFriends, CONTROLS_DISTANCE * 2),
                this.BtnLearnMore.WithSameLeft(this.LblInviteWithGmail),

                this.BtnInviteFriends.Below(this.BtnLearnMore, CONTROLS_DISTANCE * 2),
                this.BtnInviteFriends.WithSameLeft(this.LblInviteWithGmail),
                this.BtnInviteFriends.WithSameRight(this.LblInviteWithGmail),

                this.ViewLine.Below(this.BtnInviteFriends, VERTICAL_MARGIN),
                this.ViewLine.WithSameLeft(this.LblInviteWithGmail),
                this.ViewLine.WithSameRight(this.LblInviteWithGmail),
                this.ViewLine.AtBottomOf(this),
                this.ViewLine.Height().EqualTo(Metrics.LineThickness)
            );
        }
    }
}
