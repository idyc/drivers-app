﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class CollapsedCompletedTripsDayView : IDYCView
    {
        private const float HEIGHT = 44f;
        private const float CONTROLS_DISTANCE = 4f;
        private const float LABEL_WIDTH_MULTIPLIER = .4f;
        private const float ACCESORY_SIZE = 20f;
        private const float MARGIN = 16f;
        private const float MARGIN_RIGHT = 28f;

        public UIButton BtnAccessory { get; set; }

        public IDYCLabel LblDay { get; set; }

        public IDYCLabel LblEarning { get; set; }

        public CollapsedCompletedTripsDayView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BtnAccessory = new UIButton();
            this.BtnAccessory.SetImage(UIImage.FromBundle("arrow_right"), UIControlState.Normal);
            this.LblDay = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.LblEarning = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                TextAlignment = UITextAlignment.Right
            };

            this.AggregateSubviews(this.BtnAccessory, this.LblDay, this.LblEarning);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            //this.BtnAccessory.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Horizontal);
            //this.LblDay.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);

            this.AddConstraints(
                this.Height().EqualTo(HEIGHT),

                this.BtnAccessory.AtLeftOf(this, MARGIN),
                this.BtnAccessory.WithSameCenterY(this),
                this.BtnAccessory.Width().EqualTo(ACCESORY_SIZE),
                this.BtnAccessory.Height().EqualTo(ACCESORY_SIZE),

                //this.LblEarning.WithRelativeWidth(this, LABEL_WIDTH_MULTIPLIER),
                this.LblEarning.AtRightOf(this, MARGIN_RIGHT),
                this.LblEarning.WithSameCenterY(this),

                this.LblDay.ToLeftOf(this.LblEarning, CONTROLS_DISTANCE),
                this.LblDay.ToRightOf(this.BtnAccessory, CONTROLS_DISTANCE * 2),
                this.LblDay.WithSameCenterY(this)
            //this.LblDay.WithRelativeWidth(this, LABEL_WIDTH_MULTIPLIER)
            );
        }
    }
}
