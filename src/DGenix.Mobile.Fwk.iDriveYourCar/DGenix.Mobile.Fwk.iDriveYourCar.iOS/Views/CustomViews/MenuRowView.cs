﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using MvvmCross.Plugins.Color.iOS;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class MenuRowView : IDYCView
    {
        private const float MARGIN_HORIZONTAL = 16f;
        private const float MARGIN_VERTICAL = 12f;
        private const float CONTROLS_DISTANCE = 24f;

        public UIImageView ImgOption { get; set; }

        public IDYCLabel LblOption { get; set; }

        private bool optionActive;
        public bool OptionActive
        {
            get
            {
                return this.optionActive;
            }
            set
            {
                this.optionActive = value;

                var color = this.optionActive ? IDYCColors.DarkGrayTextColor : IDYCColors.LightGrayTextColor;

                this.ImgOption.TintColor = color;
                this.LblOption.TextColor = color;
            }
        }

        public MenuRowView(string imgName, string optionText)
            : base()
        {
            this.ImgOption.Image = UIImage.FromBundle(imgName).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            this.LblOption.Text = optionText;
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.ImgOption = new UIImageView(UIImage.FromBundle("ic_menu_my_trips").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate));
            this.ImgOption.TintColor = IDYCColors.DarkGrayTextColor;
            this.LblOption = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.UserInteractionEnabled = true;

            this.AddSubviews(this.ImgOption, this.LblOption);
            this.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ImgOption.SetContentHuggingPriority(501f, UILayoutConstraintAxis.Horizontal);
            this.LblOption.SetContentHuggingPriority(499f, UILayoutConstraintAxis.Horizontal);

            this.AddConstraints(
                this.ImgOption.AtLeftOf(this, MARGIN_HORIZONTAL),
                this.ImgOption.AtTopOf(this, MARGIN_VERTICAL),
                this.ImgOption.AtBottomOf(this, MARGIN_VERTICAL),
                this.ImgOption.Width().EqualTo(24f),
                this.ImgOption.Height().EqualTo(24f),

                this.LblOption.WithSameCenterY(this.ImgOption),
                this.LblOption.ToRightOf(this.ImgOption, CONTROLS_DISTANCE),
                this.LblOption.AtRightOf(this, MARGIN_HORIZONTAL)
            );
        }

        public void SetChecked(bool check)
        {
            var color = check ? this.appConfiguration.Value.PrimaryColor.ToNativeColor() : IDYCColors.DarkGrayTextColor;

            this.ImgOption.TintColor = color;
            this.LblOption.TextColor = color;
        }
    }
}
