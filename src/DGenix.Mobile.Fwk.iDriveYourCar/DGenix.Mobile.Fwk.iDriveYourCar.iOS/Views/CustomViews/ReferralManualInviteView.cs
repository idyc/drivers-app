﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using MvvmCross.Plugins.Color.iOS;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralManualInviteView : IDYCView
    {
        private const float VERTICAL_MARGIN = 24f;
        private const float CONTROLS_DISTANCE = 8f;
        private const float REGION_DISTANCE = 16f;

        public IDYCMultilineLabel LblInviteManually { get; set; }

        public IDYCMultilineLabel LblEarnForEveryFriend { get; set; }

        public IDYCLabel LblPotentialReward { get; set; }

        public IDYCLabel LblPotentialRewardValue { get; set; }

        public BorderedTextView TxtEmails { get; set; }

        public InviteManuallyButton BtnInviteFriends { get; set; }

        public IDYCMultilineLabel LblInviteExplanation { get; set; }

        public ReferralManualInviteView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblInviteManually = new IDYCMultilineLabel(Metrics.FontSizeGigant)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.OrInviteManually
            };
            this.LblEarnForEveryFriend = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                Text = IDriveYourCarStrings.EarnMoneyForEveryFriendYouInviteToDrive
            };
            this.LblPotentialRewardValue = new IDYCLabel(28f)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor()
            };
            this.LblPotentialReward = new IDYCLabel(Metrics.FontSizeNormal, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.PotentialReward
            };
            this.TxtEmails = new BorderedTextView(IDriveYourCarStrings.EnterFriendsEmailsHereSeparateWithCommas);
            this.TxtEmails.TextView.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeSmall);
            this.TxtEmails.Placeholder.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeSmall);
            this.TxtEmails.TextView.KeyboardType = UIKeyboardType.EmailAddress;

            this.BtnInviteFriends = new InviteManuallyButton(this.appConfiguration.Value);
            this.BtnInviteFriends.SetTitle(IDriveYourCarStrings.InviteFriends.ToUpper(), UIKit.UIControlState.Normal);

            this.LblInviteExplanation = new IDYCMultilineLabel(Metrics.FontSizeSmall)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };

            this.AggregateSubviews(
                this.LblInviteManually,
                this.LblEarnForEveryFriend,
                this.LblPotentialRewardValue,
                this.LblPotentialReward,
                this.TxtEmails,
                this.BtnInviteFriends,
                this.LblInviteExplanation);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblInviteManually.AtLeftOf(this),
                this.LblInviteManually.AtTopOf(this, VERTICAL_MARGIN),
                this.LblInviteManually.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),

                this.LblEarnForEveryFriend.Below(this.LblInviteManually, CONTROLS_DISTANCE),
                this.LblEarnForEveryFriend.WithSameLeft(this.LblInviteManually),
                this.LblEarnForEveryFriend.WithSameRight(this.LblInviteManually),

                this.LblPotentialRewardValue.Below(this.LblEarnForEveryFriend, CONTROLS_DISTANCE),
                this.LblPotentialRewardValue.WithSameRight(this.LblInviteManually),

                this.LblPotentialReward.WithSameBottom(this.LblPotentialRewardValue).Minus(2f),
                this.LblPotentialReward.WithSameLeft(this.LblInviteManually),
                this.LblPotentialReward.ToLeftOf(this.LblPotentialRewardValue),

                this.TxtEmails.Below(this.LblPotentialRewardValue, CONTROLS_DISTANCE),
                this.TxtEmails.WithSameLeft(this.LblInviteManually),
                this.TxtEmails.WithSameRight(this.LblInviteManually),
                this.TxtEmails.Height().EqualTo(120f),

                this.BtnInviteFriends.Below(this.TxtEmails, CONTROLS_DISTANCE * 2),
                this.BtnInviteFriends.WithSameLeft(this.LblInviteManually),
                this.BtnInviteFriends.WithRelativeWidth(this, .5f),

                this.LblInviteExplanation.Below(this.BtnInviteFriends, CONTROLS_DISTANCE * 2),
                this.LblInviteExplanation.WithSameLeft(this.LblInviteManually),
                this.LblInviteExplanation.WithSameRight(this.LblInviteManually),
                this.LblInviteExplanation.AtBottomOf(this, VERTICAL_MARGIN)
            );
        }
    }
}
