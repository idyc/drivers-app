﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Controls;
using MvvmCross.Plugins.Color.iOS;
using UIKit;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralPendingInvitesView<TView, TItemType> : IDYCView
        where TView : BindableBaseView, new()
        where TItemType : class
    {
        private const float MARGIN = 24f;

        public IDYCLabel LblPendingInvites { get; set; }

        public IDYCLabel LblPendingInvitesValue { get; set; }

        public SimpleBindableStackView<TView, TItemType> PendingInvitesStackView { get; set; }

        public ReferralPendingInvitesView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblPendingInvites = new IDYCLabel(Metrics.FontSizeHuge)
            {
                Text = IDriveYourCarStrings.PendingInvites,
                TextColor = IDYCColors.DarkGrayTextColor,
                TextAlignment = UITextAlignment.Right
            };
            this.LblPendingInvitesValue = new IDYCLabel(28f)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor(),
                TextAlignment = UITextAlignment.Right
            };
            this.PendingInvitesStackView = new SimpleBindableStackView<TView, TItemType>(UILayoutConstraintAxis.Vertical, 0f);

            this.AggregateSubviews(this.LblPendingInvites, this.LblPendingInvitesValue, this.PendingInvitesStackView);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblPendingInvites.AtTopOf(this, MARGIN),
                this.LblPendingInvites.AtRightOf(this, MARGIN),
                this.LblPendingInvites.AtLeftOf(this, MARGIN),

                this.LblPendingInvitesValue.Below(this.LblPendingInvites),
                this.LblPendingInvitesValue.AtRightOf(this, MARGIN),
                this.LblPendingInvitesValue.AtLeftOf(this, MARGIN),

                this.PendingInvitesStackView.Below(this.LblPendingInvitesValue, MARGIN),
                this.PendingInvitesStackView.AtLeftOf(this),
                this.PendingInvitesStackView.AtRightOf(this),
                this.PendingInvitesStackView.AtBottomOf(this)
            );
        }
    }
}
