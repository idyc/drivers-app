﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using MvvmCross.Plugins.Color.iOS;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class ReferralEarnedView : IDYCView
    {
        private const float VERTICAL_DISTANCE = 24f;

        public IDYCLabel LblEarnedToDate { get; set; }

        public IDYCLabel LblEarnedToDateValue { get; set; }

        public ReferralEarnedView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.LblEarnedToDateValue = new IDYCLabel(28f)
            {
                TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor()
            };
            this.LblEarnedToDate = new IDYCLabel(Metrics.FontSizeBig)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.EarnedToDate
            };

            this.AggregateSubviews(this.LblEarnedToDate, this.LblEarnedToDateValue);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.LblEarnedToDateValue.AtRightOf(this, Metrics.WINDOW_MARGIN * 2),
                this.LblEarnedToDateValue.AtTopOf(this, VERTICAL_DISTANCE),
                this.LblEarnedToDateValue.AtBottomOf(this, VERTICAL_DISTANCE),

                this.LblEarnedToDate.AtLeftOf(this),
                this.LblEarnedToDate.WithSameBottom(this.LblEarnedToDateValue).Minus(2f),
                this.LblEarnedToDate.ToLeftOf(this.LblEarnedToDateValue)
            );
        }
    }
}
