﻿using System;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using DGenix.Mobile.Fwk.iOS;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class SyncEmailExplanationView : UIViewController
    {
        private const float VERTICAL_MARGIN = 24f;
        private const float IMG_SIZE = 36f;

        public UIImageView ImgShield { get; set; }

        public IDYCLabel LblTitle { get; set; }

        public IDYCMultilineLabel LblInfo { get; set; }

        public SyncEmailExplanationView()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.View.BackgroundColor = UIColor.White;

            this.ImgShield = new UIImageView(UIImage.FromBundle("ic_safety"));
            this.LblTitle = new IDYCLabel(Metrics.FontSizeHuge, Metrics.RobotoMedium)
            {
                TextColor = IDYCColors.DarkGrayTextColor,
                Text = IDriveYourCarStrings.SyncEmailContacts,
                TextAlignment = UITextAlignment.Center
            };
            this.LblInfo = new IDYCMultilineLabel(Metrics.FontSizeTiny)
            {
                TextColor = IDYCColors.MidGrayTextColor,
                Text = IDriveYourCarStrings.SyncEmailContactsExplanation,
                TextAlignment = UITextAlignment.Center
            };

            this.View.AggregateSubviews(this.ImgShield, this.LblTitle, this.LblInfo);

            this.View.AddConstraints(
                this.ImgShield.AtTopOf(this.View, VERTICAL_MARGIN),
                this.ImgShield.WithSameCenterX(this.View),
                this.ImgShield.Height().EqualTo(IMG_SIZE),
                this.ImgShield.Width().EqualTo(IMG_SIZE),

                this.LblTitle.Below(this.ImgShield, Metrics.DEFAULT_DISTANCE),
                this.LblTitle.AtLeftOf(this.View, Metrics.WINDOW_MARGIN * 2),
                this.LblTitle.AtRightOf(this.View, Metrics.WINDOW_MARGIN * 2),

                this.LblInfo.Below(this.LblTitle, Metrics.DEFAULT_DISTANCE),
                this.LblInfo.AtLeftOf(this.View, Metrics.WINDOW_MARGIN * 2),
                this.LblInfo.AtRightOf(this.View, Metrics.WINDOW_MARGIN * 2),
                this.LblInfo.AtBottomOf(this.View, VERTICAL_MARGIN)
            );
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            var dynamicSize = new CGSize();

            dynamicSize.Height += 100f;
            dynamicSize.Height += this.ImgShield.IntrinsicContentSize.Height;
            dynamicSize.Height += this.LblTitle.IntrinsicContentSize.Height;
            dynamicSize.Height += (this.LblInfo.IntrinsicContentSize.Width / (this.View.Frame.Width - Metrics.WINDOW_MARGIN * 4)) * this.LblInfo.IntrinsicContentSize.Height;

            this.PreferredContentSize = dynamicSize;
        }

        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            var appDelegate = UIApplication.SharedApplication.Delegate as BaseAppDelegate;

            this.PopoverPresentationController.SourceRect = new CGRect(
                appDelegate.Window.RootViewController.View.Bounds.Width / 2,
                appDelegate.Window.RootViewController.View.Bounds.Height / 2,
                1,
                1);
        }
    }
}
