﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripActionOverlayView : IDYCView
    {
        private float MARGIN_SIDES = 32f;

        private UIScrollView scrollView;
        private UIView viewContainer;

        public float MarginTop => 120f;

        public UIImageView Icon { get; set; }

        public IDYCLabel ActionTitle { get; set; }

        public IDYCMultilineLabel ActionDetail { get; set; }

        public PrimaryBackgroundButton BtnTop { get; set; }

        public GrayBackgroundButton BtnBottom { get; set; }

        public FluentLayout IconToTopConstraint { get; set; }

        public TripActionOverlayView(string iconName)
        {
            if(!string.IsNullOrEmpty(iconName))
                this.Icon.Image = UIImage.FromBundle(iconName);
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.BackgroundColor = UIColor.Black;

            this.scrollView = new UIScrollView { DelaysContentTouches = false };
            this.viewContainer = new UIView();

            this.Icon = new UIImageView();
            this.ActionTitle = new IDYCLabel(Metrics.FontSizeGigant, Metrics.RobotoMedium)
            {
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center
            };
            this.ActionDetail = new IDYCMultilineLabel(Metrics.FontSizeNormal)
            {
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center
            };
            this.BtnTop = new PrimaryBackgroundButton(this.appConfiguration.Value, Metrics.FontSizeBig);
            this.BtnBottom = new GrayBackgroundButton(Metrics.FontSizeBig);

            this.AggregateSubviews(this.scrollView);
            this.scrollView.AggregateSubviews(this.viewContainer);
            this.viewContainer.AggregateSubviews(this.Icon, this.ActionTitle, this.ActionDetail, this.BtnTop, this.BtnBottom);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.scrollView.AtLeftOf(this),
                this.scrollView.AtTopOf(this),
                this.scrollView.AtRightOf(this),
                this.scrollView.AtBottomOf(this)
            );

            this.scrollView.AddConstraints(
                this.viewContainer.AtLeftOf(this.scrollView),
                this.viewContainer.AtTopOf(this.scrollView),
                this.viewContainer.AtRightOf(this.scrollView),
                this.viewContainer.AtBottomOf(this.scrollView)
            );

            this.AddConstraints(
                this.viewContainer.WithSameWidth(this)
            //this.viewContainer.WithSameCenterY(this)
            );

            this.IconToTopConstraint = this.Icon.AtTopOf(this.viewContainer, this.MarginTop * 1.5f);

            this.viewContainer.AddConstraints(
                this.Icon.WithSameCenterX(this.viewContainer),
                this.IconToTopConstraint,

                this.ActionTitle.AtTopOf(this.viewContainer, this.MarginTop * 2),
                this.ActionTitle.AtLeftOf(this.viewContainer, MARGIN_SIDES),
                this.ActionTitle.AtRightOf(this.viewContainer, MARGIN_SIDES),

                this.ActionDetail.Below(this.ActionTitle, Metrics.DEFAULT_DISTANCE),
                this.ActionDetail.WithSameLeft(this.ActionTitle),
                this.ActionDetail.WithSameRight(this.ActionTitle),

                this.BtnTop.Below(this.ActionDetail, Metrics.DEFAULT_DISTANCE),
                this.BtnTop.WithSameLeft(this.ActionTitle),
                this.BtnTop.WithSameRight(this.ActionTitle),

                this.BtnBottom.Below(this.BtnTop, Metrics.DEFAULT_DISTANCE),
                this.BtnBottom.WithSameLeft(this.ActionTitle),
                this.BtnBottom.WithSameRight(this.ActionTitle),
                this.BtnBottom.AtBottomOf(this.viewContainer, Metrics.WINDOW_MARGIN)
            );
        }
    }
}
