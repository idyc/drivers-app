﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripDetailsStopsView<TView, TItemType> : IDYCView
        where TView : BindableBaseView, new()
        where TItemType : class
    {
        public SimpleBindableStackView<TView, TItemType> StopsStackView { get; set; }

        public TripDetailsStopsView()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.StopsStackView = new SimpleBindableStackView<TView, TItemType>(UILayoutConstraintAxis.Vertical, 0f);

            this.AggregateSubviews(this.StopsStackView);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.StopsStackView.AtTopOf(this),
                this.StopsStackView.AtLeftOf(this),
                this.StopsStackView.AtRightOf(this),
                this.StopsStackView.AtBottomOf(this)
            );
        }
    }
}
