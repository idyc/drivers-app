﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;
using MvvmCross.Plugins.Color.iOS;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class UniqueStopView : IDYCView
    {
        private const float TEXT_SEPARATION = 4f;

        public IDYCLabel Title { get; set; }

        public IDYCLabel Subtitle { get; set; }

        public IDYCLabel Airline { get; set; }

        public UIImageView Icon { get; set; }

        public UIView ViewLine { get; set; }

        public UniqueStopView()
        {

        }

        public UniqueStopView(string iconName)
        {
            if(!string.IsNullOrEmpty(iconName))
                this.Icon.Image = UIImage.FromBundle(iconName);
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Title = new IDYCLabel(Metrics.FontSizeNormal) { TextColor = IDYCColors.DarkGrayTextColor };
            this.Subtitle = new IDYCLabel(Metrics.FontSizeNormalMinus) { TextColor = IDYCColors.MidGrayTextColor };
            this.Airline = new IDYCLabel(Metrics.FontSizeSmall) { TextColor = this.appConfiguration.Value.PrimaryColor.ToNativeColor() };
            this.Icon = new UIImageView();

            this.ViewLine = new UIView { BackgroundColor = IDYCColors.LineColor };

            this.AggregateSubviews(this.Title, this.Subtitle, this.Airline, this.Icon, this.ViewLine);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Icon.AtLeftOf(this, Metrics.WINDOW_MARGIN * 2),
                this.Icon.WithSameTop(this.Title),
                this.Icon.Width().EqualTo(18f),
                this.Icon.Height().EqualTo().WidthOf(this.Icon),

                this.Title.ToRightOf(this.Icon, Metrics.DEFAULT_DISTANCE / 2),
                this.Title.AtTopOf(this, Metrics.DEFAULT_DISTANCE),
                this.Title.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.Subtitle.Below(this.Title, TEXT_SEPARATION),
                this.Subtitle.WithSameLeft(this.Title),
                this.Subtitle.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.Airline.Below(this.Subtitle, TEXT_SEPARATION),
                this.Airline.WithSameLeft(this.Title),
                this.Airline.AtRightOf(this, Metrics.WINDOW_MARGIN),

                this.ViewLine.Below(this.Airline, Metrics.DEFAULT_DISTANCE),
                this.ViewLine.WithSameLeft(this.Title),
                this.ViewLine.AtRightOf(this),
                this.ViewLine.Height().EqualTo(Metrics.LineThickness),
                this.ViewLine.AtBottomOf(this)
            );
        }
    }
}
