﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iOS.Extensions;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.CustomViews
{
    public class TripOfferView : IDYCView
    {
        public UIImageView Icon { get; set; }

        public IDYCMultilineLabel Text { get; set; }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.Icon = new UIImageView();
            this.Text = new IDYCMultilineLabel(Metrics.FontSizeNormalMinus);

            this.AggregateSubviews(this.Icon, this.Text);
        }

        public TripOfferView()
        {

        }

        public TripOfferView(string iconName)
        {
            if(!string.IsNullOrEmpty(iconName))
                this.Icon.Image = UIImage.FromBundle(iconName);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.AddConstraints(
                this.Icon.AtTopOf(this, Metrics.DEFAULT_DISTANCE),
                this.Icon.AtLeftOf(this, Metrics.WINDOW_MARGIN * 2),
                this.Icon.Width().EqualTo(18f),
                this.Icon.Height().EqualTo().WidthOf(this.Icon),

                this.Text.WithSameTop(this.Icon),
                this.Text.ToRightOf(this.Icon, Metrics.DEFAULT_DISTANCE / 2),
                this.Text.AtRightOf(this, Metrics.WINDOW_MARGIN),
                this.Text.AtBottomOf(this, Metrics.DEFAULT_DISTANCE)
            );
        }
    }
}
