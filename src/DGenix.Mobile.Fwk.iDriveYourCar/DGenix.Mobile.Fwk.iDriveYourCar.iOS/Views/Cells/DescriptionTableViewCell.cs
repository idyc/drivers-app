﻿using System;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells
{
	public class DescriptionTableViewCell : BaseTableViewCell
	{
		public DescriptionTableViewCell(IntPtr handle) : base(handle)
		{

		}

		protected override void CreateMvxBindings()
		{
			base.CreateMvxBindings();

			this.TextLabel.Font = UIFont.FromName(Metrics.RobotoRegular, Metrics.FontSizeNormal);
			this.TextLabel.Lines = 0;
			this.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;

			this.AddBindings(this.TextLabel, "Text Description", null);
		}
	}
}
