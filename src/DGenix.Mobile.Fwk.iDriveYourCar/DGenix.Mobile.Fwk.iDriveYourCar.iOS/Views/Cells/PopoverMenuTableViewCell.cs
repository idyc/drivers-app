﻿using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Controls;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells
{
    public class PopoverMenuTableViewCell : PopoverMenuViewCell<string>
    {
        private const float PADDING = 16f;

        public IDYCLabel Text { get; set; }

        public PopoverMenuTableViewCell() : base()
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.SelectionStyle = UIKit.UITableViewCellSelectionStyle.None;
            this.Text = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };

            this.ContentView.AggregateSubviews(this.Text);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                this.Text.AtLeftOf(this.ContentView, PADDING),
                this.Text.AtRightOf(this.ContentView, PADDING),
                this.Text.WithSameCenterY(this.ContentView)
            );
        }

        public override void Configure(string item)
        {
            this.Text.Text = item;
        }
    }
}
