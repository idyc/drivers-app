﻿using System;
using DGenix.Mobile.Fwk.iOS.Views.Cells;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells
{
	public class IDYCTableViewCell : BaseTableViewCell
	{
		public IDYCTableViewCell(IntPtr handle) : base(handle)
		{
		}

		public IDYCTableViewCell() : base()
		{
		}
	}
}
