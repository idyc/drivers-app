﻿using System;
using Cirrious.FluentLayouts.Touch;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Controls;
using DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles;
using DGenix.Mobile.Fwk.iOS.Extensions;
using DGenix.Mobile.Fwk.iOS.Views.Cells;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.Cells
{
    public class ContactSelectionTableViewCell : BaseTableViewCell
    {
        private const float CONTROLS_DISTANCE = 8f;

        private IDYCLabel lblContactName, lblEmail;

        private UIButton btnCheckbox;

        public ContactSelectionTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateViews()
        {
            base.CreateViews();

            this.lblContactName = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.DarkGrayTextColor
            };
            this.lblEmail = new IDYCLabel(Metrics.FontSizeNormal)
            {
                TextColor = IDYCColors.MidGrayTextColor
            };
            this.btnCheckbox = new UIButton();
            this.btnCheckbox.SetImage(UIImage.FromBundle("ic_check"), UIControlState.Normal);
            this.btnCheckbox.SetImage(UIImage.FromBundle("ic_check_checked"), UIControlState.Selected);

            this.ContentView.AggregateSubviews(this.lblContactName, this.lblEmail, this.btnCheckbox);
        }

        protected override void CreateConstraints()
        {
            base.CreateConstraints();

            this.ContentView.AddConstraints(
                this.btnCheckbox.AtRightOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.btnCheckbox.WithSameCenterY(this.ContentView),
                this.btnCheckbox.WithRelativeWidth(this.ContentView, .2f),

                this.lblContactName.AtLeftOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.lblContactName.AtTopOf(this.ContentView, Metrics.WINDOW_MARGIN),
                this.lblContactName.ToLeftOf(this.btnCheckbox, CONTROLS_DISTANCE),

                this.lblEmail.Below(this.lblContactName),
                this.lblEmail.WithSameLeft(this.lblContactName),
                this.lblEmail.WithSameRight(this.lblContactName)
            );
        }

        protected override void CreateMvxBindings()
        {
            base.CreateMvxBindings();

            var set = this.CreateBindingSet<ContactSelectionTableViewCell, EmailInvitation>();
            set.Bind(this.lblContactName).To(vm => vm.Contact);
            set.Bind(this.lblEmail).To(vm => vm.Email);
            set.Bind(this.btnCheckbox).For(v => v.Selected).To(vm => vm.Selected).Mode(MvvmCross.Binding.MvxBindingMode.TwoWay);
            set.Bind(this.btnCheckbox).To(vm => vm.ToggleSelectedCommand);
            set.Apply();
        }
    }
}
