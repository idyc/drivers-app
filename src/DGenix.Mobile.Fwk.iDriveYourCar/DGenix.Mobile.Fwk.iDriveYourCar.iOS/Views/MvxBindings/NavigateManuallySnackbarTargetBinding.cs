﻿using System;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using SnackBarTTG.Source;
using UIKit;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Views.MvxBindings
{
    public class NavigateManuallySnackbarTargetBinding : MvxTargetBinding
    {
        public NavigateManuallySnackbarTargetBinding(object target)
            : base(target)
        {
        }

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        public override Type TargetType => typeof(UIViewController);

        public override void SetValue(object value)
        {
            if(value == null)
                throw new ArgumentNullException(nameof(value));

            var faulted = (bool)value;
            if(value == null)
                throw new ArgumentException("Value must be boolean");

            if(faulted)
            {
                var snackBar = new TTGSnackbar(IDriveYourCarStrings.NavigationInitializationFailedPleaseNavigateManually, TTGSnackbarDuration.Long);
                snackBar.AnimationType = TTGSnackbarAnimationType.SlideFromBottomBackToBottom;
                snackBar.Show();
            }
        }
    }
}
