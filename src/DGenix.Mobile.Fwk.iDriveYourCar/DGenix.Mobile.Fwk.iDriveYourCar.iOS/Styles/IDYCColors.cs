﻿using System;
using DGenix.Mobile.Fwk.iOS.Extensions.Color;
using UIKit;

namespace DGenix.Mobile.Fwk.iDriveYourCar.iOS.Styles
{
    public static class IDYCColors
    {
        public static UIColor BackgroundColor => "#E7E7E7".ToUIColor();

        public static UIColor EntryBackgroundColor => "#FAFAFA".ToUIColor();

        public static UIColor BorderColor => "#D9D9D9".ToUIColor();

        public static UIColor LineColor => "#E4E4E4".ToUIColor();

        public static UIColor DarkGrayTextColor => "#4F4F4F".ToUIColor();

        public static UIColor MidGrayTextColor => "#9D9D9D".ToUIColor();

        public static UIColor IconGrayColor => "#808080".ToUIColor();

        public static UIColor LightGrayTextColor => "#BABABA".ToUIColor();

        public static UIColor ErrorTextColor => "#FF0000".ToUIColor();

        public static UIColor GreenButtonColor => "#43B859".ToUIColor();

        public static UIColor SwipedButtonColor => "#96BF48".ToUIColor();
    }
}
