﻿using System;
using Newtonsoft.Json;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Converters.Json
{
	public class DateTimeToTimeConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return reader.Value?.ToString();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if(value != null)
			{
				if(this.CanConvert(value.GetType()))
				{
					var dateTime = DateTime.Parse(value.ToString());
					var timeSpan = new TimeSpan(dateTime.Hour, dateTime.Minute, dateTime.Second);
					writer.WriteValue(timeSpan);
				}
				else
				{
					writer.WriteValue(value);
				}
			}
			else
			{
				writer.WriteValue((string)null);
			}

		}
	}
}
