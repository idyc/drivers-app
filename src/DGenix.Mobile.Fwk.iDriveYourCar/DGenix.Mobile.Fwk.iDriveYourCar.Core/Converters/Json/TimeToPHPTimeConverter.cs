﻿using System;
using Newtonsoft.Json;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Converters.Json
{
    public class TimeToPHPTimeConverter : JsonConverter
    {
        private const string PHP_TIME_WITH_SECONDS = "24:00:00";
        private const string CSHARP_TIME_WITH_SECONDS = "23:59:59";

        private const string PHP_TIME = "24:00";
        private const string CSHARP_TIME = "23:59";

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var incomingValue = reader.Value?.ToString();

            if(!this.CanConvert(objectType) || string.IsNullOrWhiteSpace(incomingValue))
                return null;

            if(incomingValue.Equals(PHP_TIME_WITH_SECONDS, StringComparison.OrdinalIgnoreCase)
                || incomingValue.Equals(PHP_TIME, StringComparison.OrdinalIgnoreCase))
            {
                var timeSpanCSharp = TimeSpan.Parse(CSHARP_TIME_WITH_SECONDS);
                var csharpDateTime = new DateTime(
                                            DateTime.Today.Year,
                                            DateTime.Today.Month,
                                            DateTime.Today.Day,
                                            timeSpanCSharp.Hours,
                                            timeSpanCSharp.Minutes,
                                            timeSpanCSharp.Seconds);
                return csharpDateTime;
            }
            var timeSpan = TimeSpan.Parse(incomingValue);
            var parsedDateTime = new DateTime(
                                    DateTime.Today.Year,
                                    DateTime.Today.Month,
                                    DateTime.Today.Day,
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);
            return parsedDateTime;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if(!this.CanConvert(value.GetType()) || value == null)
            {
                writer.WriteValue((string)null);
                return;
            }

            if(value.Equals(CSHARP_TIME_WITH_SECONDS))
            {
                writer.WriteValue(PHP_TIME_WITH_SECONDS);
                return;
            }
            if(value.Equals(CSHARP_TIME))
            {
                writer.WriteValue(PHP_TIME);
                return;
            }
            //writer.WriteValue(value);

            var dateTime = DateTime.Parse(value.ToString());
            var timeSpan = new TimeSpan(dateTime.Hour, dateTime.Minute, dateTime.Second);
            writer.WriteValue(timeSpan);
        }
    }
}
