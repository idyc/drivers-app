﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings
{
    public class IDYCSettings : IIDYCSettings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        #region Setting Constants

        private const string CookieTokenKey = "cookieToken";
        private const string UserImagePathKey = "userImagePath";
        private const string DriverIdKey = "driverId";
        private const string PushNotificationsTokenKey = "pushNotificationsToken";
        private const string PushNotificationsCounterKey = "pushNotificationsCounter";
        private const string ChatIdentityKey = "chatIdentity";
        private const string ChatTokenKey = "chatToken";
        private const string HasBankInfoKey = "hasBankInfo";

        #endregion

        public string CookieToken
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CookieTokenKey, string.Empty);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CookieTokenKey, value);
            }
        }

        public string UserImagePath
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserImagePathKey, string.Empty);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserImagePathKey, value);
            }
        }

        public long DriverId
        {
            get
            {
                return AppSettings.GetValueOrDefault<long>(DriverIdKey, default(long));
            }
            set
            {
                AppSettings.AddOrUpdateValue(DriverIdKey, value);
            }
        }

        public string PushNotificationsToken
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(PushNotificationsTokenKey, string.Empty);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PushNotificationsTokenKey, value);
            }
        }

        public int PushNotificationsCounter
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(PushNotificationsCounterKey);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PushNotificationsCounterKey, value);
            }
        }

        public string ChatIdentity
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(ChatIdentityKey, string.Empty);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ChatIdentityKey, value);
            }
        }

        public string ChatToken
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(ChatTokenKey, string.Empty);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ChatTokenKey, value);
            }
        }

        public bool HasBankInfo
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(HasBankInfoKey);
            }
            set
            {
                AppSettings.AddOrUpdateValue(HasBankInfoKey, value);
            }
        }
    }
}
