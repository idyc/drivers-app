﻿using System;
namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings
{
    public interface IIDYCSettings
    {
        string CookieToken { get; set; }

        string UserImagePath { get; set; }

        long DriverId { get; set; }

        string PushNotificationsToken { get; set; }

        int PushNotificationsCounter { get; set; }

        string ChatIdentity { get; set; }

        string ChatToken { get; set; }

        bool HasBankInfo { get; set; }
    }
}