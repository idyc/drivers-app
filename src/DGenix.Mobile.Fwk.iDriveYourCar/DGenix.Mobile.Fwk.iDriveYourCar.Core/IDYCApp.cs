using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using MvvmCross.Platform;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers;
using DGenix.Mobile.Fwk.Core.Providers;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core
{
    public class IDYCApp : BaseApp
    {
        public IDYCApp()
            : base(false, false)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            Mvx.RegisterType<IAppConfiguration, AppConfiguration>();
            Mvx.RegisterType<IApiEndpointAdapterFactory, IDCYApiEndpointServicesFactory>();

            // Adapter
            Mvx.RegisterType<IApiProxy, IDYCApiProxy>();
            Mvx.RegisterType<IApiServicesAdapter, IDYCApiServicesAdapter>();

            Mvx.RegisterType<IHttpStatusResponseStrategy, HttpStatusResponseStrategy>();

            this.serverResponseExceptionStrategy = new IDYCServerResponseExceptionStrategy(this.userSettings).ConfigureServerResponseExceptionStrategiesByType();
            Mvx.RegisterSingleton<IServerResponseExceptionStrategy>(this.serverResponseExceptionStrategy);
        }
    }
}