﻿using System;
using DGenix.Mobile.Fwk.Core.Adapters;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
	public class IDYCApiEndpoint : IApiEndpoint
	{
		public IDYCApiEndpoints Service { get; set; }
	}
}