using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support;
using MvvmCross.Platform.Platform;
using Xamarin;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
    public class HttpStatusResponseStrategy : BaseHttpStatusResponseStrategy
    {
        private readonly IMvxJsonConverter jsonConverter;

        public HttpStatusResponseStrategy(IMvxJsonConverter jsonConverter)
        {
            this.jsonConverter = jsonConverter;
        }

        protected override async Task HandleBadRequestStatusCode(HttpResponseMessage response)
        {
            var iDYCResponse = this.jsonConverter.DeserializeObject<IDYCResponse>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

            Insights.Track($"Conflict StatusCode ---- {iDYCResponse.Message}");
            throw new BusinessException(iDYCResponse.Message);
        }

        protected override async Task HandleNotFoundStatusCode(HttpResponseMessage response)
        {
            // we must figure out if it is a real 404 error or an intended 404 from the REST service
            try
            {
                var iDYCResponse = this.jsonConverter.DeserializeObject<IDYCResponse>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
            catch
            {
                Insights.Track("NotFound StatusCode");
                throw new Exception("NotFound");
            }
        }

        protected override Task HandleUnauthorizedStatusCode(HttpResponseMessage response)
        {
            throw new UnauthorizedException();
        }

        protected override Task HandleMethodNotAllowedStatusCode(HttpResponseMessage response)
        {
            throw new Exception($"HTTP Method used for the endpoint is not allowed!! {response.RequestMessage.RequestUri.ToString()}");
        }

        protected override async Task HandleConflictStatusCode(HttpResponseMessage response)
        {
            var iDYCResponse = this.jsonConverter.DeserializeObject<IDYCResponse>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

            Insights.Track($"Conflict StatusCode ---- {iDYCResponse.Message}");
            throw new BusinessException(iDYCResponse.Message);
        }

        protected override async Task HandlePreconditionFailedStatusCode(HttpResponseMessage response)
        {
            var iDYCResponse = this.jsonConverter.DeserializeObject<IDYCResponse>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

            Insights.Track($"PreconditionFailed StatusCode ---- {iDYCResponse.Message}");
            throw new BusinessException(iDYCResponse.Message);
        }

        protected override Task HandleUnprocessableEntityStatusCode(HttpResponseMessage response)
        {
            Insights.Track("UnprocessableEntity StatusCode");
            throw new Exception("UnprocessableEntity StatusCode");
        }

        protected override Task HandleInternalServerErrorStatusCode(HttpResponseMessage response)
        {
            Insights.Track("InternalServerError StatusCode");
            throw new Exception("InternalServerError StatusCode");
        }
    }
}