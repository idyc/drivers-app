﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Helpers;
using DGenix.Mobile.Fwk.Core.Repositories;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Repositories.Settings;
using MvvmCross.Platform.Platform;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
    public class IDYCApiProxy : BaseApiProxy
    {
        private readonly IIDYCSettings idycSettings;

        private const string GetCookieKey = "Set-Cookie";
        private const string SetCookieKey = "Cookie";

        public IDYCApiProxy(
            IMvxJsonConverter jsonConverter,
            IApiEndpointAdapterFactory endpointServicesFactory,
            IApiEndpointUriFactory endpointUriFactory,
            IUserSettings userSettings,
            IConnectivityHelper connectivityHelper,
            IAppConfiguration appConfiguration,
            IIDYCSettings idycSettings,
            IHttpStatusResponseStrategy httpStatusResponseManager)
            : base(jsonConverter, endpointServicesFactory, endpointUriFactory, userSettings, connectivityHelper, appConfiguration, httpStatusResponseManager)
        {
            this.idycSettings = idycSettings;
        }

        #region protected override methods
        protected override void FillGlobalRequestHeaders(HttpRequestMessage request)
        {
            if (!string.IsNullOrEmpty(this.idycSettings.CookieToken))
                request.Headers.Add(SetCookieKey, this.idycSettings.CookieToken);

            if (!string.IsNullOrEmpty(this.userSettings.Token))
                request.Headers.Add(HttpHeaders.Authorization, $"Bearer {this.userSettings.Token}");
        }

        protected override void FillGlobalRequestHeaders(MultipartFormDataContent request)
        {
            if (!string.IsNullOrEmpty(this.idycSettings.CookieToken))
                request.Headers.Add(SetCookieKey, this.idycSettings.CookieToken);
        }

        protected override void GetGlobalResponseHeaders(HttpResponseMessage response)
        {
            IEnumerable<string> cookies;
            response.Headers.TryGetValues(GetCookieKey, out cookies);

            if (cookies != null && cookies.Any())
                this.idycSettings.CookieToken = cookies.First();
        }
        #endregion
    }
}