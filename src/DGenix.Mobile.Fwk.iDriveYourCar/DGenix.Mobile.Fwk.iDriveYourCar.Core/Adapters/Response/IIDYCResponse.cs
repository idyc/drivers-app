﻿using System;
namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
	public interface IIDYCResponse
	{
		int Code { get; set; }
	}

	public interface IIDYCResponse<T> : IIDYCResponse
	{
		T Data { get; set; }
	}
}
