﻿using System;
namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
	public class IDYCResponse : IIDYCResponse
	{
		public string Message { get; set; }

		public int Code { get; set; }
	}

	public class IDYCResponse<T> : IDYCResponse, IIDYCResponse<T>
	{
		public T Data { get; set; }
	}
}
