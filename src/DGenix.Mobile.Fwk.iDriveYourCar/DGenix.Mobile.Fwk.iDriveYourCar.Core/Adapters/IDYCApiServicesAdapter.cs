﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Models;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
    public class IDYCApiServicesAdapter : BaseApiServicesAdapter
    {
        public IDYCApiServicesAdapter(
            IApiProxy restServiceProxy,
            IApiEndpointAdapterFactory endpointServicesFactory)
            : base(restServiceProxy, endpointServicesFactory)
        {
        }

        public override async Task<TResult> CallAsync<TResult>(IApiEndpoint service, object data = null, params string[] queryStringIds)
        {
            // make call to proxy and encapsulate response handler
            var response = await Task.Run(() => this.restServiceProxy.MakeApiCall<IDYCResponse<TResult>>(service, data, queryStringIds));

            return this.GetDataOrDefaultFromResponse(response);
        }

        public override async Task CallAsync(IApiEndpoint service, object data = null, params string[] queryStringIds)
        {
            // make call to proxy and encapsulate response handler
            await Task.Run(() => this.restServiceProxy.MakeApiCall<IDYCResponse>(service, data, queryStringIds));
        }

        public override async Task<Stream> DownloadFileStreamAsync(IApiEndpoint service, object data = null, params string[] queryStringIds)
        {
            // make call to proxy and encapsulate response handler
            var response = await Task.Run(() => this.restServiceProxy.MakeApiCall<Stream>(service, data, queryStringIds));

            return response;
        }

        public override async Task<byte[]> DownloadFileByteArrayAsync(IApiEndpoint service, object data = null, params string[] queryStringIds)
        {
            // make call to proxy and encapsulate response handler
            var response = await Task.Run(() => this.restServiceProxy.MakeApiCall<byte[]>(service, data, queryStringIds));

            return response;
        }

        public override async Task<TResult> UploadFilesAsync<TResult>(IApiEndpoint service, IEnumerable<FileRequest> fileRequests, IDictionary<string, string> stringContent = null, params string[] queryStringIds)
        {
            var response = await Task.Run(() => this.restServiceProxy.MakeApiCall<IDYCResponse<TResult>>(service, fileRequests, stringContent, queryStringIds));

            return this.GetDataOrDefaultFromResponse(response);
        }

        private TResult GetDataOrDefaultFromResponse<TResult>(IDYCResponse<TResult> response)
        {
            switch(response.Code)
            {
                case (int)HttpStatusCode.OK:
                    return response.Data;
                case (int)HttpStatusCode.Created:
                    return response.Data;
                case (int)HttpStatusCode.NotFound:
                    break;
                    //throw new BusinessException(response.Message);
            }

            return default(TResult);
        }
    }
}
