﻿using System.Collections.Generic;
using System.Net.Http;
using DGenix.Mobile.Fwk.Core.Adapters;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters
{
    public class IDCYApiEndpointServicesFactory : IApiEndpointAdapterFactory
    {
        private readonly Dictionary<IDYCApiEndpoints, ApiServiceEndpoint> servicesEndpointUrlsCollection = new Dictionary<IDYCApiEndpoints, ApiServiceEndpoint>
        {
            // no auth requests
            [IDYCApiEndpoints.Login] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("account/login"), RequiresAuth = false, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.CreateDriver] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers"), RequiresAuth = false, HttpType = HttpMethod.Post },

            // auth requests

            // GET
            [IDYCApiEndpoints.GetDriverData] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self"), RequiresAuth = true },
            [IDYCApiEndpoints.GetDriverReviews] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/reviews"), RequiresAuth = true },
            [IDYCApiEndpoints.FindOpenTrips] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/open_trips"), RequiresAuth = true },
            [IDYCApiEndpoints.GetAllReferrals] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/referals"), RequiresAuth = true },
            [IDYCApiEndpoints.GetDocument] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/document"), RequiresAuth = true },
            [IDYCApiEndpoints.RefreshToken] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("oauth/refresh-token"), RequiresAuth = true },
            [IDYCApiEndpoints.GetNotifications] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/notifications"), RequiresAuth = true },

            // POST
            [IDYCApiEndpoints.UploadAvatar] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/avatar"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.SendDocument] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/send_document"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.AcceptTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/accept"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.RejectTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/reject"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.ResendInvitation] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/resend_email_invitation"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.RequestRedeem] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/ask_for_redeem"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.InviteGmailFriends] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/gmail_invitations"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.UpdateDriverUnavailabilityWeek] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/unavailability_week"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.UpdateDriverUnavailability] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/unavailability"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.ForgotPassword] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("account/password/email"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.UpdateAddress] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("addresses/self"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.InviteManuallyEmails] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/email_invitations"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.AddExpense] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/{0}/expenses"), RequiresAuth = true, HttpType = HttpMethod.Post },
            [IDYCApiEndpoints.GetTwilioChatToken] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("twilio/chat_token"), RequiresAuth = true, HttpType = HttpMethod.Post },

            // PUT/PATCH
            [IDYCApiEndpoints.EditDriverInfo] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.SendDeviceId] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/device"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.SendLocation] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/location"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.AcceptTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/accept/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.CheckInTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/checking/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.ArrivedTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/arrived/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.ActiveTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/active/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.ClientDroppedOff] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/client_dropped/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.CompleteTrip] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/self/complete/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.SetDropoff] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/{0}/dropoff"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.UpdateBankingInfo] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/update_bank_info"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },
            [IDYCApiEndpoints.AcceptTripChanges] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/accept_changes/{0}"), RequiresAuth = true, HttpType = new HttpMethod("PATCH") },

            // DELETE
            [IDYCApiEndpoints.DeleteExpense] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("trips/{0}/expenses/{1}"), RequiresAuth = true, HttpType = HttpMethod.Delete },
            [IDYCApiEndpoints.RemoveNotification] = new ApiServiceEndpoint { RawUrl = BuildRawUrlEndpoint("drivers/self/notifications/{0}"), RequiresAuth = true, HttpType = HttpMethod.Delete },
        };

        public IApiServiceEndpoint GetApiEndPointMetaData<T>(T apiEndPoint) where T : IApiEndpoint
        {
            return this.GetSpecificApiEndPointMetaData((IDYCApiEndpoint)((IApiEndpoint)apiEndPoint));
        }

        private IApiServiceEndpoint GetSpecificApiEndPointMetaData(IDYCApiEndpoint apiEndPoint)
        {
            return servicesEndpointUrlsCollection[apiEndPoint.Service];
        }

        private static string BuildRawUrlEndpoint(string endpoint) => $"{endpoint}";
    }
}