﻿using System;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Support;
using DGenix.Mobile.Fwk.Core.Repositories;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers
{
    public class IDYCServerResponseExceptionStrategy : BaseServerResponseExceptionStrategy
    {
        private readonly IUserSettings userSettings;

        public IDYCServerResponseExceptionStrategy(IUserSettings userSettings)
        {
            this.userSettings = userSettings;
        }

        public override BaseServerResponseExceptionStrategy ConfigureServerResponseExceptionStrategiesByType()
        {
            this.AddServerResponseExceptionConcreteStrategyByType<UnauthorizedException>(new UnauthorizedExceptionStrategy(this.userSettings));
            return this;
        }
    }
}
