﻿using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Providers;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers
{
    public class IDYCApiProvider : BaseApiProvider
    {
        public IDYCApiProvider(IApiServicesAdapter adapter)
        : base(adapter)
        {
        }
    }
}
