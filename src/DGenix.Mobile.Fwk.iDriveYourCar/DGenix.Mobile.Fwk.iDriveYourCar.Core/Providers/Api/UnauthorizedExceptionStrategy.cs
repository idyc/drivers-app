﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core.Adapters;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Providers;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.Core.Repositories;
using System.IO;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Providers
{
    public class UnauthorizedExceptionStrategy : IServerResponseExceptionStrategy
    {
        private readonly IUserSettings userSettings;

        public UnauthorizedExceptionStrategy(IUserSettings userSettings)
        {
            this.userSettings = userSettings;
        }

        public async Task<byte[]> ManageByteArrayServerResponseException(ServerResponseException exception, IApiBaseProvider apiProvider, IApiEndpoint apiEndpoint, Func<object> dataAsFunc = null, Func<string[]> queryStringIds = null)
        {
            await this.RefreshTokenAsync(apiProvider).ConfigureAwait(false);

            return await apiProvider.DownloadFileByteArrayAsync(apiEndpoint, dataAsFunc, queryStringIds).ConfigureAwait(false);
        }

        public async Task ManageServerResponseException(ServerResponseException exception, IApiBaseProvider apiProvider, IApiEndpoint apiEndpoint, Func<object> dataAsFunc = null, Func<string[]> queryStringIds = null)
        {
            await this.RefreshTokenAsync(apiProvider).ConfigureAwait(false);

            await apiProvider.CallAsync(apiEndpoint, dataAsFunc, queryStringIds).ConfigureAwait(false);
        }

        public async Task<TResult> ManageServerResponseException<TResult>(ServerResponseException exception, IApiBaseProvider apiProvider, IApiEndpoint apiEndpoint, Func<object> dataAsFunc = null, Func<string[]> queryStringIds = null)
        {
            await this.RefreshTokenAsync(apiProvider).ConfigureAwait(false);

            return await apiProvider.CallAsync<TResult>(apiEndpoint, dataAsFunc, queryStringIds).ConfigureAwait(false);
        }

        public async Task<TResult> ManageServerResponseException<TResult>(ServerResponseException exception, IApiBaseProvider apiProvider, IApiEndpoint apiEndpoint, IEnumerable<FileRequest> fileRequests, Func<IDictionary<string, string>> stringContentAsFunc, Func<string[]> queryStringIds = null)
        {
            await this.RefreshTokenAsync(apiProvider).ConfigureAwait(false);

            return await apiProvider.UploadFilesAsync<TResult>(apiEndpoint, fileRequests, stringContentAsFunc, queryStringIds).ConfigureAwait(false);
        }

        public async Task<Stream> ManageStreamServerResponseException(ServerResponseException exception, IApiBaseProvider apiProvider, IApiEndpoint apiEndpoint, Func<object> dataAsFunc = null, Func<string[]> queryStringIds = null)
        {
            await this.RefreshTokenAsync(apiProvider).ConfigureAwait(false);

            return await apiProvider.DownloadFileStreamAsync(apiEndpoint, dataAsFunc, queryStringIds).ConfigureAwait(false);
        }

        /// <summary>
        /// Refresh token and store the result
        /// </summary>
        /// <returns>The token async.</returns>
        private async Task RefreshTokenAsync(IApiBaseProvider apiProvider)
        {
            var newToken = await apiProvider.ApiAdapter.CallAsync<Token>(new IDYCApiEndpoint { Service = IDYCApiEndpoints.RefreshToken }).ConfigureAwait(false);

            this.userSettings.SetUserValues(newToken.AccessToken, this.userSettings.UserId);
        }
    }
}
