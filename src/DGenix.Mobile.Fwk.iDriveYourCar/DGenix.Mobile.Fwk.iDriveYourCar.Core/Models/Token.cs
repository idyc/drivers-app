﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Models
{
	public class Token
	{
		public string AccessToken { get; set; }

		public long AccessTokenExpiration { get; set; }
	}
}
