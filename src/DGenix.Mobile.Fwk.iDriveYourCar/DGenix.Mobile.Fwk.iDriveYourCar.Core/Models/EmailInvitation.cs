﻿using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using PropertyChanged;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Models
{
	[ImplementPropertyChanged]
	public class EmailInvitation
	{
		public EmailInvitation()
		{
			this.SetSelectedCommand = new MvxCommand<bool>(selected => this.Selected = selected);

			this.ToggleSelectedCommand = new MvxCommand(() => this.Selected = !this.Selected);
		}

		public EmailInvitation(string contact, string email) : this()
		{
			this.Contact = contact;
			this.Email = email;
		}

		[JsonProperty(PropertyName = "email")]
		public string Email { get; set; }

		[JsonProperty(PropertyName = "contact")]
		public string Contact { get; set; }

		[JsonIgnore]
		public bool Selected { get; set; }

		[JsonIgnore]
		public ICommand SetSelectedCommand { get; private set; }

		[JsonIgnore]
		public ICommand ToggleSelectedCommand { get; private set; }
	}
}
