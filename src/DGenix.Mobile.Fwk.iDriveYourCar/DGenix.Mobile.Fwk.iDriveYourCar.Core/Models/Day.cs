﻿using System;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Models
{
	public class Day
	{
		public int Number { get; set; }

		public string Name { get; set; }
	}
}
