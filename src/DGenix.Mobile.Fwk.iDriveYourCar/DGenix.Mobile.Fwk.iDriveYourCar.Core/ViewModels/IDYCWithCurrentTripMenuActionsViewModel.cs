﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public class IDYCWithCurrentTripMenuActionsViewModel : IDYCViewModel, IWithCurrentTripMenuNavigationActionsWrapper
    {
        public IDYCWithCurrentTripMenuActionsViewModel(ICurrentTripMenuNavigationActionsWrapper currentTripMenuNavigationActionsWrapper)
        {
            this.CurrentTripMenuNavigationActionsWrapper = currentTripMenuNavigationActionsWrapper;
        }

        public ICurrentTripMenuNavigationActionsWrapper CurrentTripMenuNavigationActionsWrapper { get; private set; }
    }
}
