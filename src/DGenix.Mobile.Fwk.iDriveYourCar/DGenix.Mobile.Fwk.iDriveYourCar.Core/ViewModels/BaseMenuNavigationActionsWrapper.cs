﻿using System;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public abstract class BaseMenuNavigationActionsWrapper : FwkBaseNavigatingObject, IMenuNavigationActionsWrapper
	{
		protected BaseMenuNavigationActionsWrapper()
		{
			this.InitializeCommands();
		}

		public ICommand CallDispatchCommand { get; protected set; }

		public ICommand MessageDispatchCommand { get; protected set; }

		protected abstract void InitializeCommands();
	}
}