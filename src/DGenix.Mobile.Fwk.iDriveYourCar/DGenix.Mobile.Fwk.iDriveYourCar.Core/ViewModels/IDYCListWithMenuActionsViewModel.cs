﻿using System;
using DGenix.Mobile.Fwk.Core.Criterias;
using DGenix.Mobile.Fwk.Core.Models;
using DGenix.Mobile.Fwk.Core.Services;
using DGenix.Mobile.Fwk.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public abstract class IDYCListWithMenuActionsViewModel<TService, TModel, TItem, TCriteria> : FwkBaseListViewModel<TService, TModel, TItem, TCriteria>, IWithMenuNavigationActionsWrapper
        where TService : IService
        where TModel : BaseModel
        where TCriteria : ISimplePagedListCriteriaModel
    {
        public IDYCListWithMenuActionsViewModel(
            IFwkBaseListViewModelServiceBridge<TService, TModel, TCriteria> serviceBridge,
            IMenuNavigationActionsWrapper menuNavigationActionsWrapper)
            : base(serviceBridge)
        {
            this.MenuNavigationActionsWrapper = menuNavigationActionsWrapper;
        }

        public IMenuNavigationActionsWrapper MenuNavigationActionsWrapper { get; private set; }
    }
}
