﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public class IDYCWithCurrentTripMenuActionsHostViewModel : IDYCHostViewModel, IWithCurrentTripMenuNavigationActionsWrapper
    {
        public IDYCWithCurrentTripMenuActionsHostViewModel(ICurrentTripMenuNavigationActionsWrapper currentTripMenuNavigationActionsWrapper)
        {
            this.CurrentTripMenuNavigationActionsWrapper = currentTripMenuNavigationActionsWrapper;
        }

        public ICurrentTripMenuNavigationActionsWrapper CurrentTripMenuNavigationActionsWrapper { get; private set; }
    }
}
