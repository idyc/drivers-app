﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public class IDYCWithMenuActionsViewModel : IDYCViewModel, IWithMenuNavigationActionsWrapper
	{
		public IDYCWithMenuActionsViewModel(IMenuNavigationActionsWrapper menuNavigationActionsWrapper)
		{
			this.MenuNavigationActionsWrapper = menuNavigationActionsWrapper;
		}

		public IMenuNavigationActionsWrapper MenuNavigationActionsWrapper { get; private set; }
	}
}