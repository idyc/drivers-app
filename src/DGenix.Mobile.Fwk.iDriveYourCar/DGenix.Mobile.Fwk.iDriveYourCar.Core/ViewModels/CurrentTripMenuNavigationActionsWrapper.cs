﻿using System;
using System.Windows.Input;
using DGenix.Mobile.Fwk.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public abstract class CurrentTripMenuNavigationActionsWrapper : FwkBaseNavigatingObject, ICurrentTripMenuNavigationActionsWrapper
    {
        protected CurrentTripMenuNavigationActionsWrapper()
        {
            this.InitializeCommands();
        }

        public ICommand CallPassengerCommand { get; protected set; }

        public ICommand TextPassengerCommand { get; protected set; }

        public ICommand AddExpenseCommand { get; protected set; }

        public ICommand MessageDispatchCommand { get; protected set; }

        public ICommand CallDispatchCommand { get; protected set; }

        protected abstract void InitializeCommands();

        public abstract void ToggleMenuAvailability(CurrentTripMenuAction currentTripMenuAction);
    }
}