﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;
namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public class IDYCNavigatingObject : FwkBaseNavigatingObject
	{
		public IDYCNavigatingObject()
		{
		}

		public string TryToGetImagePath(Func<string> partialPathFunc)
		{
			try
			{
				var partialPath = partialPathFunc.Invoke();

				return !string.IsNullOrEmpty(partialPath) ?
							  (this.appConfiguration.Value.BaseUrlAPIMedia + partialPath) : "ASD";
			}
			catch(Exception)
			{
				return "ASD";
			}
		}
	}
}
