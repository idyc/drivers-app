﻿using System;
using System.Windows.Input;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public interface ICurrentTripMenuNavigationActionsWrapper
    {
        ICommand CallPassengerCommand { get; }

        ICommand TextPassengerCommand { get; }

        ICommand AddExpenseCommand { get; }

        ICommand MessageDispatchCommand { get; }

        ICommand CallDispatchCommand { get; }

        void ToggleMenuAvailability(CurrentTripMenuAction currentTripMenuAction);
    }

    public enum CurrentTripMenuAction
    {
        CallDispatch,
        MessageDispatch,
        AddExpense,
        CallPassenger,
        TextPassenger
    }
}
