﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public interface IWithCurrentTripMenuNavigationActionsWrapper
    {
        ICurrentTripMenuNavigationActionsWrapper CurrentTripMenuNavigationActionsWrapper { get; }
    }
}