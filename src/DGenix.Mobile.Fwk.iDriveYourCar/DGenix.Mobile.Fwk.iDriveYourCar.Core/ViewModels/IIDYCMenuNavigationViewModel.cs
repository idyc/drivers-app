﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public interface IIDYCMenuNavigationViewModel
	{
		BaseMenuNavigationActionsWrapper MenuNavigationActions { get; }
	}
}