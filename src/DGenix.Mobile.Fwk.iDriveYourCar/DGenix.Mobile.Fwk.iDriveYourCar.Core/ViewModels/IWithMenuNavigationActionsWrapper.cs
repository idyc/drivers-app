﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public interface IWithMenuNavigationActionsWrapper
	{
		IMenuNavigationActionsWrapper MenuNavigationActionsWrapper { get; }
	}
}