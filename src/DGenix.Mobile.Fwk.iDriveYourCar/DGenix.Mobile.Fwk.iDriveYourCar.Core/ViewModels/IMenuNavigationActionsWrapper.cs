﻿using System.Windows.Input;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public interface IMenuNavigationActionsWrapper
	{
		ICommand CallDispatchCommand { get; }

		ICommand MessageDispatchCommand { get; }
	}
}