﻿using System;
namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
    public interface IIDYCCurrentTripMenuNavigationViewModel
    {
        CurrentTripMenuNavigationActionsWrapper MenuNavigationActions { get; }
    }
}
