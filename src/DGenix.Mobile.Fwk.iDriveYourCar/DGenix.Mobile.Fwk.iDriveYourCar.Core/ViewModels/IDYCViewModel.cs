﻿using System;
using DGenix.Mobile.Fwk.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public class IDYCViewModel : FwkBaseViewModel
	{
		public string TryToGetImagePath(Func<string> partialPathFunc)
		{
			try
			{
				var partialPath = partialPathFunc.Invoke();

				return !string.IsNullOrEmpty(partialPath) ?
							  (this.appConfiguration.Value.BaseUrlAPIMedia + partialPath) : "ASD";
			}
			catch(Exception)
			{
				return "ASD";
			}
		}
	}
}