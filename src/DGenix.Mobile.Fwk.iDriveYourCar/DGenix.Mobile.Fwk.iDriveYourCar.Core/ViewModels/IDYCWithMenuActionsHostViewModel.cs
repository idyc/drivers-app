﻿namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels
{
	public class IDYCWithMenuActionsHostViewModel : IDYCHostViewModel, IWithMenuNavigationActionsWrapper
	{
		public IDYCWithMenuActionsHostViewModel(IMenuNavigationActionsWrapper menuNavigationActionsWrapper)
		{
			this.MenuNavigationActionsWrapper = menuNavigationActionsWrapper;
		}

		public IMenuNavigationActionsWrapper MenuNavigationActionsWrapper { get; private set; }
	}
}