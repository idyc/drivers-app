﻿using System;
using System.Threading.Tasks;
using DGenix.Mobile.Fwk.Core;
using System.Collections.Generic;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers
{
	public interface IInputFieldHandler
	{
		Task<string> ShowSingleInputFieldAsync(string title, string message, Func<string, string> inputValidation, InputType inputType = InputType.None, string defaultValue = null);

		Task<Tuple<string, string>> ShowDoubleInputFieldsAsync(string title, string message, Func<string, string> inputValidation, InputType inputType = InputType.None, string defaultFirstValue = null, string defaultSecondValue = null);

		Task<DateTime?> ShowDateInputFieldAsync(DateTime? defaultValue = null);

		Task<DateTime?> ShowTimeInputFieldAsync(DateTime? defaultValue = null);

		Task<string> ShowOptionsAlertAsync(string title, IEnumerable<string> options);

		void DisplayEmailContactsExplanation();
	}
}