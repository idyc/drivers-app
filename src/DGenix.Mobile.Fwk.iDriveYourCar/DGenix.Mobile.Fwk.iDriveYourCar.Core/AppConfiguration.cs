﻿using System;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Platform.UI;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core
{
    public class AppConfiguration : IAppConfiguration
    {
        #region private members
        private MvxColor _primaryColor, _lightPrimaryColor, _darkPrimaryColor, _accentColor, _lightAccentColor;

        private string _appName, _appVersion;
        #endregion

        [Preserve]
        public AppConfiguration()
        {
            this.InitializeAppValues();
        }

        private void InitializeAppValues()
        {
            // name
            this._appName = "DriveMyCar";

            // version
            this._appVersion = "0.4.1";

            // colors
            this._primaryColor = new MvxColor(60, 128, 246, 255);
            this._lightPrimaryColor = new MvxColor(117, 165, 247, 255);
            this._darkPrimaryColor = new MvxColor(48, 104, 199, 255);
            this._accentColor = new MvxColor(255, 170, 48, 255);
            this._lightAccentColor = new MvxColor(255, 202, 128, 255);
        }

        #region Properties for private members

        public string AppName => this._appName;

        public string AppVersion => this._appVersion;

        public MvxColor PrimaryColor => this._primaryColor;

        public MvxColor DarkPrimaryColor => this._darkPrimaryColor;

        public MvxColor AccentColor => this._accentColor;

        public MvxColor LightPrimaryColor => this._lightPrimaryColor;

        public MvxColor LightAccentColor => this._lightAccentColor;

        public string FilenameUpdateAppPlaceholder
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string FilenameNoInternetConnectionSmall => "ic_general_nonetwork";
        public string FilenameNoInternetConnectionBig => "placeholder_nonetwork_reload";

        #endregion

        public int BreakingChangeAPIVersion => 1;

        //public string BaseUrlAPIMedia => "https://api.idriveyourcar.com/avatars/";

        public string BaseUrlAPIMedia => "https://api.chauffeurprodriver.com/avatars/";

        public string BaseUrlAPI
        {
            get
            {
#if DEBUG
                //return "https://api.idriveyourcar.com/v1";
                return "https://api.chauffeurprodriver.com/v1";
#else
                return "https://api.chauffeurprodriver.com/v1";
                //return "https://api.idriveyourcar.com/v1";
#endif
            }
        }

        public string GetStringResource(string stringResourceKey)
        {
            return IDriveYourCarStrings.ResourceManager.GetString(stringResourceKey);
        }
    }
}