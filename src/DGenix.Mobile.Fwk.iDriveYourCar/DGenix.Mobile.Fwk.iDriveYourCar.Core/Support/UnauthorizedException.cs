﻿using System;
using DGenix.Mobile.Fwk.Core.Support.Exceptions;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Support
{
    public class UnauthorizedException : ServerResponseException
    {
        public UnauthorizedException()
        {
        }

        public UnauthorizedException(string message)
            : base(message)
        {
        }
    }
}
