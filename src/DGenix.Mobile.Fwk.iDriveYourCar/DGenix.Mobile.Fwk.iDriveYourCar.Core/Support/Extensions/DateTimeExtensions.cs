﻿using System;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions
{
    public static class DateTimeExtensions
    {
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private const string DATE_FORMAT_SLASHES = "yyyy/MM/dd";
        private const string SHORT_MONTH_DAY_NUMBER_YEAR = "MMM dd, yyyy";
        private const string MONTH_DAY_NUMBER_YEAR = "MMMM dd, yyyy";
        private const string DAY_MONTH_DAY_NUMBER_YEAR = "dddd MMMM dd, yyyy";
        private const string DAY_MONTH_SINGLE_DAY_NUMBER_YEAR = "dddd MMMM d, yyyy";
        private const string DAY_MONTH_DAY_NUMBER = "dddd, MMMM dd";
        private const string SHORT_DAY_SHORT_MONTH_DAY_NUMBER = "ddd, MMM dd";
        private const string MONTH_FORMAT = "Y";
        private const string TIME_AM_PM = "hh:mm tt";

        public static string ToFormattedString(this DateTime date, DateStringFormat format)
        {
            string formatToUse = string.Empty;
            switch(format)
            {
                case DateStringFormat.ShortDateMonthYear:
                    formatToUse = DATE_FORMAT;
                    break;
                case DateStringFormat.ShortDateMonthYearSlashes:
                    formatToUse = DATE_FORMAT_SLASHES;
                    break;
                case DateStringFormat.ShortMonthNumberYear:
                    formatToUse = SHORT_MONTH_DAY_NUMBER_YEAR;
                    break;
                case DateStringFormat.MonthDayNumberYear:
                    formatToUse = MONTH_DAY_NUMBER_YEAR;
                    break;
                case DateStringFormat.DayMonthDayNumberYear:
                    formatToUse = DAY_MONTH_SINGLE_DAY_NUMBER_YEAR;
                    break;
                case DateStringFormat.DayMonthSingleDayNumberYear:

                case DateStringFormat.DayMonthDayNumber:
                    formatToUse = DAY_MONTH_DAY_NUMBER;
                    break;
                case DateStringFormat.ShortDayShortMonthDayNumber:
                    formatToUse = SHORT_DAY_SHORT_MONTH_DAY_NUMBER;
                    break;
                case DateStringFormat.MonthYear:
                    formatToUse = MONTH_FORMAT;
                    break;
                case DateStringFormat.TimeAMPM:
                    formatToUse = TIME_AM_PM;
                    return date.ToString(formatToUse).Replace("a. m.", "AM").Replace("p. m.", "PM");
            }

            return date.ToString(formatToUse);
        }

        public static DateTime ToDateTimeFromFormat(this string stringFormatted)
        {
            DateTime value = new DateTime();

            if(!string.IsNullOrEmpty(stringFormatted) && !DateTime.TryParseExact(stringFormatted, DATE_FORMAT, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
            {
                if(!DateTime.TryParseExact(stringFormatted, DAY_MONTH_DAY_NUMBER_YEAR, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
                {
                    if(!DateTime.TryParseExact(stringFormatted, DAY_MONTH_SINGLE_DAY_NUMBER_YEAR, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
                    {
                        if(!DateTime.TryParseExact(stringFormatted, MONTH_DAY_NUMBER_YEAR, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
                            throw new InvalidOperationException("Could not parse string to DateTime");
                    }
                }
            }

            return value;
        }
    }

    public enum DateStringFormat
    {
        ShortDateMonthYear,
        ShortDateMonthYearSlashes,
        ShortMonthNumberYear,
        MonthDayNumberYear,
        DayMonthSingleDayNumberYear,
        DayMonthDayNumberYear,
        DayMonthDayNumber,
        ShortDayShortMonthDayNumber,
        MonthYear,
        TimeAMPM
    }
}
