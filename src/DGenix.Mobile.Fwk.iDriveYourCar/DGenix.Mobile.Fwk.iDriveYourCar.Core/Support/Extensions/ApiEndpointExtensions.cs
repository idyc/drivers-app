﻿using System;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Adapters;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Support.Extensions
{
    public static class ApiEndpointExtensions
    {
        public static IDYCApiEndpoint Build(this IDYCApiEndpoints endpoint) => new IDYCApiEndpoint { Service = endpoint };
    }
}
