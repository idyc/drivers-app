﻿using System;
using System.Collections.Generic;
using System.Linq;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Core.Support
{
	public static class DaysOfWeek
	{
		private static readonly Lazy<IList<Day>> _days = new Lazy<IList<Day>>(() => new List<Day>
		{
			new Day { Number = 0, Name = IDriveYourCarStrings.Monday },
			new Day { Number = 1, Name = IDriveYourCarStrings.Tuesday },
			new Day { Number = 2, Name = IDriveYourCarStrings.Wednesday },
			new Day { Number = 3, Name = IDriveYourCarStrings.Thursday },
			new Day { Number = 4, Name = IDriveYourCarStrings.Friday },
			new Day { Number = 5, Name = IDriveYourCarStrings.Saturday },
			new Day { Number = 6, Name = IDriveYourCarStrings.Sunday }
		});

		public static IList<Day> Days => _days.Value;

		public static Day GetByNumber(int number)
		{
			var day = Days.SingleOrDefault(d => d.Number == number);

			if(day == null)
				throw new ArgumentOutOfRangeException($"There is no day with number {number} in the collection of DaysOfWeek.");

			return day;
		}
	}
}
