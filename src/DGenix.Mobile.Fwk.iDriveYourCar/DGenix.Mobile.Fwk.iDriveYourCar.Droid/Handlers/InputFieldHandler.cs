﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using DGenix.Mobile.Fwk.Core;
using DGenix.Mobile.Fwk.Droid.Controls;
using DGenix.Mobile.Fwk.Droid.Extensions;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using Plugin.CurrentActivity;
using FwkStrings = DGenix.Mobile.Fwk.Core.Resources.Strings;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid
{
    public class InputFieldHandler : IInputFieldHandler
    {
        private readonly IAppConfiguration appConfiguration;

        private Lazy<FragmentActivity> activity = new Lazy<FragmentActivity>(() => CrossCurrentActivity.Current.Activity as FragmentActivity);

        public InputFieldHandler(IAppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        public Task<string> ShowSingleInputFieldAsync(string title, string message, Func<string, string> inputValidation, InputType inputType = InputType.None, string defaultValue = null)
        {
            var tcs = new TaskCompletionSource<string>();

            View dialogLayout = this.activity.Value.LayoutInflater.Inflate(Resource.Layout.profile_popup_alert_handler_enter_value, null);

            var alert = new Android.Support.V7.App.AlertDialog.Builder(this.activity.Value, Resource.Style.MyAlertDialogStyle);

            var txtMessage = dialogLayout.FindViewById<TextView>(Resource.Id.profile_popup_alert_handler_enter_value_message);
            if(!string.IsNullOrEmpty(message))
            {
                txtMessage.Text = message;
                txtMessage.Visibility = ViewStates.Visible;
            }
            else
            {
                txtMessage.Visibility = ViewStates.Gone;
            }

            var value = dialogLayout.FindViewById<EditText>(Resource.Id.profile_popup_alert_handler_enter_value_value);
            this.SetInputType(value, inputType);
            value.Text = defaultValue;
            value.SetSelection(value.Text.Length);

            var txtError = dialogLayout.FindViewById<TextView>(Resource.Id.profile_popup_alert_handler_error_validation);

            alert.SetTitle(title);
            alert.SetView(dialogLayout);
            alert.SetCancelable(false);

            alert.SetPositiveButton(
                FwkStrings.Accept,
                (EventHandler<DialogClickEventArgs>)null
            );

            alert.SetNegativeButton(
                FwkStrings.Cancel,
                new EventHandler<DialogClickEventArgs>(delegate (object sender, DialogClickEventArgs e)
                {
                    this.activity.Value.HideSoftKeyboard();
                    ((Android.Support.V7.App.AlertDialog)sender).Dismiss();
                    tcs.SetResult(null);
                })
            );

            var dialog = alert.Create();

            dialog.Window.SetSoftInputMode(SoftInput.StateVisible);
            dialog.Show();

            dialog.GetButton((int)DialogButtonType.Positive).Click += (object sender, EventArgs e) =>
            {
                this.activity.Value.HideSoftKeyboard(value.WindowToken);

                var errorText = inputValidation.Invoke(value.Text);
                if(!string.IsNullOrEmpty(errorText))
                {
                    txtError.Text = errorText;
                }
                else
                {
                    tcs.SetResult(value.Text);
                    dialog.Dismiss();
                }
            };

            this.activity.Value.ShowSoftKeyboard(value);

            return tcs.Task;
        }

        public Task<Tuple<string, string>> ShowDoubleInputFieldsAsync(string title, string message, Func<string, string> inputValidation, InputType inputType = InputType.None, string defaultFirstValue = null, string defaultSecondValue = null)
        {
            var tcs = new TaskCompletionSource<Tuple<string, string>>();

            View dialogLayout = this.activity.Value.LayoutInflater.Inflate(Resource.Layout.first_name_and_last_name_popup_alert_handler_enter_value, null);

            var alert = new Android.Support.V7.App.AlertDialog.Builder(this.activity.Value, Resource.Style.MyAlertDialogStyle);

            var txtMessage = dialogLayout.FindViewById<TextView>(Resource.Id.first_name_and_last_name_popup_alert_handler_enter_value_message);
            if(!string.IsNullOrEmpty(message))
            {
                txtMessage.Text = message;
                txtMessage.Visibility = ViewStates.Visible;
            }
            else
            {
                txtMessage.Visibility = ViewStates.Gone;
            }

            var txtFirstName = dialogLayout.FindViewById<EditText>(Resource.Id.first_name_popup_alert_handler_enter_value_value);
            this.SetInputType(txtFirstName, inputType);
            txtFirstName.Text = defaultFirstValue;
            txtFirstName.SetSelection(txtFirstName.Text.Length);

            var txtLastName = dialogLayout.FindViewById<EditText>(Resource.Id.last_name_popup_alert_handler_enter_value_value);
            this.SetInputType(txtLastName, inputType);
            txtLastName.Text = defaultSecondValue;

            var txtErrorFirstName = dialogLayout.FindViewById<TextView>(Resource.Id.first_name_popup_alert_handler_error_validation);
            var txtErrorLastName = dialogLayout.FindViewById<TextView>(Resource.Id.last_name_popup_alert_handler_error_validation);

            alert.SetTitle(title);
            alert.SetView(dialogLayout);
            alert.SetCancelable(false);

            alert.SetPositiveButton(
                FwkStrings.Accept,
                (EventHandler<DialogClickEventArgs>)null
            );

            alert.SetNegativeButton(
                FwkStrings.Cancel,
                new EventHandler<DialogClickEventArgs>(delegate (object sender, DialogClickEventArgs e)
                {
                    this.activity.Value.HideSoftKeyboard();
                    ((Android.Support.V7.App.AlertDialog)sender).Dismiss();
                    tcs.SetResult(null);
                })
            );

            var dialog = alert.Create();

            dialog.Window.SetSoftInputMode(SoftInput.StateVisible);
            dialog.Show();

            dialog.GetButton((int)DialogButtonType.Positive).Click += (object sender, EventArgs e) =>
            {
                this.activity.Value.HideSoftKeyboard(txtFirstName.WindowToken);

                txtErrorFirstName.Text = string.Empty;
                txtErrorLastName.Text = string.Empty;

                var errorTextFirstName = inputValidation.Invoke(txtFirstName.Text);
                var errorTextLastName = inputValidation.Invoke(txtLastName.Text);

                if(!string.IsNullOrEmpty(errorTextFirstName))
                {
                    txtErrorFirstName.Text = errorTextFirstName;
                }
                if(!string.IsNullOrEmpty(errorTextLastName))
                {
                    txtErrorLastName.Text = errorTextLastName;
                }

                if(string.IsNullOrEmpty(errorTextFirstName) && string.IsNullOrEmpty(errorTextLastName))
                {
                    var firstNameAndLastName = new Tuple<string, string>(txtFirstName.Text, txtLastName.Text);

                    tcs.SetResult(firstNameAndLastName);
                    dialog.Dismiss();
                }
            };

            this.activity.Value.ShowSoftKeyboard(txtFirstName);

            return tcs.Task;
        }

        public Task<DateTime?> ShowDateInputFieldAsync(DateTime? defaultValue = null)
        {
            var tcs = new TaskCompletionSource<DateTime?>();

            var datePickerFragment = new DatePickerDialogFragment(
                this.activity.Value,
                defaultValue ?? DateTime.Now,
                (sender, e) =>
                {
                    if(tcs.Task.Status != TaskStatus.RanToCompletion)
                        tcs.SetResult(e.Date);
                },
                () =>
                {
                    if(tcs.Task.Status != TaskStatus.RanToCompletion)
                        tcs.SetResult(null);
                });
            datePickerFragment.Cancelable = false;
            datePickerFragment.Show(this.activity.Value.SupportFragmentManager, "date");

            return tcs.Task;
        }

        public Task<DateTime?> ShowTimeInputFieldAsync(DateTime? defaultValue = null)
        {
            var tcs = new TaskCompletionSource<DateTime?>();

            var date = defaultValue ?? DateTime.Now;

            var timePickerFragment = new TimePickerDialogFragment(
                this.activity.Value,
                date,
                (sender, e) =>
                {
                    if(tcs.Task.Status != TaskStatus.RanToCompletion)
                        tcs.SetResult(new DateTime(date.Year, date.Month, date.Day, e.HourOfDay, e.Minute, 0));
                },
                () =>
                {
                    if(tcs.Task.Status != TaskStatus.RanToCompletion)
                        tcs.SetResult(null);
                },
                false);
            timePickerFragment.Cancelable = false;
            timePickerFragment.Show(this.activity.Value.SupportFragmentManager, "time");

            return tcs.Task;
        }

        public Task<string> ShowOptionsAlertAsync(string title, IEnumerable<string> options)
        {
            var tcs = new TaskCompletionSource<string>();

            View dialogLayout = this.activity.Value.LayoutInflater.Inflate(Resource.Layout.dialog_multiple_options, null);

            var alert = new AlertDialog.Builder(this.activity.Value);
            alert.SetTitle(title);
            alert.SetView(dialogLayout);
            var dialog = alert.Create();
            dialog.CancelEvent += (sender, e) =>
            {
                if(tcs.Task.Status != TaskStatus.RanToCompletion)
                    tcs.SetResult(null);
            };

            var container = dialogLayout.FindViewById<LinearLayout>(Resource.Id.ll_options);

            var paddingSides = 24;
            var verticalDistance = 24;

            foreach(var option in options)
            {
                var textView = new TextView(this.activity.Value);
                textView.Text = option;
                textView.Click += (ev, args) =>
                {
                    if(tcs.Task.Status != TaskStatus.RanToCompletion)
                        tcs.SetResult(((ev as TextView).Text));

                    dialog.Dismiss();
                };
                textView.Gravity = GravityFlags.Left;
                textView.TextAlignment = TextAlignment.TextStart;
                textView.SetTextSize(Android.Util.ComplexUnitType.Sp, 17);
                textView.SetTextColor(Color.Black);
                textView.SetPadding(paddingSides, verticalDistance, paddingSides, verticalDistance);

                container.AddView(textView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent));
            }

            dialog.Show();

            return tcs.Task;
        }

        public void DisplayEmailContactsExplanation()
        {
            View dialogLayout = this.activity.Value.LayoutInflater.Inflate(Resource.Layout.sync_email_explanation, null);

            var alert = new Android.Support.V7.App.AlertDialog.Builder(this.activity.Value, Resource.Style.MyAlertDialogStyle);

            var txtSyncEmailContacts = dialogLayout.FindViewById<TextView>(Resource.Id.txt_sync_email_contacts);
            txtSyncEmailContacts.Text = IDriveYourCarStrings.SyncEmailContacts;

            var txtSyncEmailContactsExplanation = dialogLayout.FindViewById<TextView>(Resource.Id.txt_sync_email_contacts_explanation);
            txtSyncEmailContactsExplanation.Text = IDriveYourCarStrings.SyncEmailContactsExplanation;

            alert.SetView(dialogLayout);
            alert.SetCancelable(true);

            var dialog = alert.Create();
            dialog.Show();
        }

        #region private methods

        private async Task SetSelectedDate(Action<DateTime?> selectDateAction)
        {
            var date = await this.ShowDateInputFieldAsync();

            selectDateAction.Invoke(date);
        }

        private async Task SetSelectedTime(Action<DateTime?> selectTimeAction)
        {
            var time = await this.ShowTimeInputFieldAsync();

            selectTimeAction.Invoke(time);
        }

        private void SetInputType(EditText value, InputType inputType)
        {
            switch(inputType)
            {
                case InputType.Email:
                    value.InputType = Android.Text.InputTypes.TextVariationEmailAddress;
                    break;
                case InputType.Phone:
                    value.InputType = Android.Text.InputTypes.ClassNumber;
                    break;
                default:
                    break;
            }
        }

        #endregion
    }
}
