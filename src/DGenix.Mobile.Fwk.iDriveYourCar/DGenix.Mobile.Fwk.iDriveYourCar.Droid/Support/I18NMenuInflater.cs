﻿using System.Resources;
using Android.Content;
using DGenix.Mobile.Fwk.Droid.Support;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Support
{
    public class IDYCI18NMenuInflater : BaseI18NMenuInflater
    {
        public IDYCI18NMenuInflater(Context context)
            : base(context)
        {
        }

        protected override ResourceManager ResourceManager => Core.Resources.IDriveYourCarStrings.ResourceManager;

        protected override void RegisterMenus()
        {
            this.RegisterMenuWithMenuItems(Resource.Menu.main_toolbar_items, Resource.Id.menu_call_dispatch, Resource.Id.menu_message_dispatch);
        }
    }
}