﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using DGenix.Mobile.Fwk.Droid;
using DGenix.Mobile.Fwk.Droid.Presenters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Handlers;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter;
using DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views.MvxBindings;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid
{
    public abstract class IDYCSetup : BaseSetup
    {
        public override BaseFragmentsViewPresenter FragmentsPresenter { get; set; }

        public List<Type> RootViewModels { get; set; }

        public IDYCSetup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();

            Mvx.RegisterType<IInputFieldHandler, InputFieldHandler>();
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            this.FragmentsPresenter = new IDYCViewPresenter(this.AndroidViewAssemblies, this.RootViewModels);
            Mvx.RegisterSingleton<IMvxAndroidViewPresenter>(this.FragmentsPresenter);
            return this.FragmentsPresenter;
        }

        protected override void FillTargetFactories(MvvmCross.Binding.Bindings.Target.Construction.IMvxTargetBindingFactoryRegistry registry)
        {
            base.FillTargetFactories(registry);

            registry.RegisterFactory(new MvxCustomBindingFactory<ViewGroup>(MvxBindings.NavigateManually, (ViewGroup) => new NavigateManuallyTargetBinding(ViewGroup)));
        }
    }
}
