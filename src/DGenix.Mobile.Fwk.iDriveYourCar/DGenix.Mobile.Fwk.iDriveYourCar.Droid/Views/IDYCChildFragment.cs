﻿using DGenix.Mobile.Fwk.Droid.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
	public abstract class IDYCChildFragment<TViewModel> : BaseChildFragment<TViewModel>
		where TViewModel : IDYCViewModel
	{
	}
}
