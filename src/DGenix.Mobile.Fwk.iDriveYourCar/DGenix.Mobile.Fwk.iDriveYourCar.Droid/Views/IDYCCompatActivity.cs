﻿using System;
using System.ComponentModel;
using DGenix.Mobile.Fwk.Droid.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
    public abstract class IDYCCompatActivity<TViewModel> : BaseAppCompatActivity<TViewModel>
        where TViewModel : IDYCViewModel
    {
        protected override void OnResume()
        {
            base.OnResume();

            this.ViewModel.PropertyChanged += this.ViewModel_PropertyChanged;
        }

        protected override void OnPause()
        {
            base.OnPause();

            this.ViewModel.PropertyChanged -= this.ViewModel_PropertyChanged;
        }

        protected virtual void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
    }
}
