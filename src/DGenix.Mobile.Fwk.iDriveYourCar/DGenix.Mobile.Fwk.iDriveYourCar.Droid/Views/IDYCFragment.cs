using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Views;
using MvvmCross.Droid.Support.V7.AppCompat;
using DGenix.Mobile.Fwk.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
    public abstract class IDYCFragment<TViewModel> : BaseFragment<TViewModel>, View.IOnClickListener
        where TViewModel : FwkBaseViewModel
    {
        private Toolbar toolbar;
        private MvxActionBarDrawerToggle drawerToggle;

        protected abstract int? ToolbarLayoutId { get; }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var baseActivity = this.BaseActivity as IDrawerLayoutBaseCachingFragmentCompatActivity;

            if(this.ToolbarLayoutId.HasValue)
            {
                this.toolbar = view.FindViewById<Toolbar>(this.ToolbarLayoutId.Value);
                this.BaseActivity.SetSupportActionBar(this.toolbar);
                this.BaseActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(true);

                this.drawerToggle = new MvxActionBarDrawerToggle(
                    this.Activity,                               // host Activity
                    ((IDrawerLayoutBaseCachingFragmentCompatActivity)this.Activity).DrawerLayout,  // DrawerLayout object
                    this.toolbar,                               // nav drawer icon to replace 'Up' caret
                    Resource.String.drawer_open,                // "open drawer" description
                    Resource.String.drawer_close                // "close drawer" description
                );
                baseActivity.DrawerLayout.AddDrawerListener(this.drawerToggle);

                this.drawerToggle.ToolbarNavigationClickListener = this;

                if(this.IsRoot)
                {
                    this.drawerToggle.DrawerIndicatorEnabled = true;
                    baseActivity.DrawerLayout.SetDrawerLockMode(DrawerLayout.LockModeUnlocked, GravityCompat.Start);
                }
                else
                {
                    this.drawerToggle.DrawerIndicatorEnabled = false;
                    baseActivity.DrawerLayout.SetDrawerLockMode(DrawerLayout.LockModeLockedClosed, GravityCompat.Start);
                }
                this.drawerToggle.SyncState();
            }

            return view;
        }

        public void OnClick(View v)
        {
            // this method is called when DrawerLayout is locked and closed
            this.BaseActivity.SupportFragmentManager.PopBackStackImmediate();
        }
    }
}