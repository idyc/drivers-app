﻿using Android.Views;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
    public abstract class IDYCRootFragment<TViewModel> : IDYCFragment<TViewModel>, IMenuItemOnMenuItemClickListener
        where TViewModel : FwkBaseViewModel, IWithMenuNavigationActionsWrapper
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;

            return base.OnCreateView(inflater, container, savedInstanceState);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.main_toolbar_items, menu);

            var mnuCallDispatch = menu.FindItem(Resource.Id.menu_call_dispatch);
            mnuCallDispatch.SetOnMenuItemClickListener(this);
            mnuCallDispatch.SetShowAsAction(ShowAsAction.Never);

            var mnuMessageDispatch = menu.FindItem(Resource.Id.menu_message_dispatch);
            mnuMessageDispatch.SetOnMenuItemClickListener(this);
            mnuMessageDispatch.SetShowAsAction(ShowAsAction.Never);

            base.OnCreateOptionsMenu(menu, inflater);
        }

        public virtual bool OnMenuItemClick(IMenuItem item)
        {
            if(item.ItemId == Resource.Id.menu_call_dispatch)
            {
                this.ViewModel.MenuNavigationActionsWrapper.CallDispatchCommand.Execute(null);
                return true;
            }
            if(item.ItemId == Resource.Id.menu_message_dispatch)
            {
                this.ViewModel.MenuNavigationActionsWrapper.MessageDispatchCommand.Execute(null);
                return true;
            }

            return false;
        }
    }
}