﻿using System;
using Android.Support.Design.Widget;
using Android.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Resources;
using MvvmCross.Binding;
using MvvmCross.Binding.Droid.Target;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views.MvxBindings
{
    public class NavigateManuallyTargetBinding : MvxAndroidTargetBinding
    {
        public NavigateManuallyTargetBinding(object target)
            : base(target)
        {
        }

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        public override Type TargetType => typeof(ViewGroup);

        /// <summary>
        /// Performs the action to set the value.
        /// </summary>
        /// <param name="target">Target.</param>
        /// <param name="value">Value indicating whether there is no Internet.</param>
        protected override void SetValueImpl(object target, object value)
        {
            if(value == null)
                throw new ArgumentNullException(nameof(value));

            var faulted = (bool)value;
            if(value == null)
                throw new ArgumentException("Value must be boolean");

            if(faulted)
            {
                Snackbar.Make(
                    target as ViewGroup,
                    IDriveYourCarStrings.NavigationInitializationFailedPleaseNavigateManually,
                    Snackbar.LengthLong).Show();
            }
        }
    }
}
