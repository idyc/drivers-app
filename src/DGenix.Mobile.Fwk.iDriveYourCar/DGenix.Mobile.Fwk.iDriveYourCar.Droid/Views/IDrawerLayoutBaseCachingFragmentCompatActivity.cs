using Android.Support.V4.Widget;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
	public interface IDrawerLayoutBaseCachingFragmentCompatActivity
	{
		DrawerLayout DrawerLayout { get; }
	}
}