﻿using Android.Views;
using DGenix.Mobile.Fwk.Core.ViewModels;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
    public abstract class IDYCCurrentTripFragment<TViewModel> : IDYCFragment<TViewModel>, IMenuItemOnMenuItemClickListener
        where TViewModel : FwkBaseViewModel, IWithCurrentTripMenuNavigationActionsWrapper
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;

            return base.OnCreateView(inflater, container, savedInstanceState);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.toolbar_current_trip, menu);

            var menuCallPassenger = menu.FindItem(Resource.Id.menu_call_passenger);
            menuCallPassenger.SetOnMenuItemClickListener(this);
            menuCallPassenger.SetShowAsAction(ShowAsAction.Never);

            var menuTextPassenger = menu.FindItem(Resource.Id.menu_message_passenger);
            menuTextPassenger.SetOnMenuItemClickListener(this);
            menuTextPassenger.SetShowAsAction(ShowAsAction.Never);

            var menuAddExpense = menu.FindItem(Resource.Id.menu_add_expense);
            menuAddExpense.SetOnMenuItemClickListener(this);
            menuAddExpense.SetShowAsAction(ShowAsAction.Never);

            var mnuCallDispatch = menu.FindItem(Resource.Id.menu_call_dispatch);
            mnuCallDispatch.SetOnMenuItemClickListener(this);
            mnuCallDispatch.SetShowAsAction(ShowAsAction.Never);

            var mnuMessageDispatch = menu.FindItem(Resource.Id.menu_message_dispatch);
            mnuMessageDispatch.SetOnMenuItemClickListener(this);
            mnuMessageDispatch.SetShowAsAction(ShowAsAction.Never);

            base.OnCreateOptionsMenu(menu, inflater);
        }

        public virtual bool OnMenuItemClick(IMenuItem item)
        {
            if(item.ItemId == Resource.Id.menu_call_passenger)
            {
                this.ViewModel.CurrentTripMenuNavigationActionsWrapper.CallPassengerCommand.Execute(null);
                return true;
            }
            if(item.ItemId == Resource.Id.menu_message_passenger)
            {
                this.ViewModel.CurrentTripMenuNavigationActionsWrapper.TextPassengerCommand.Execute(null);
                return true;
            }
            if(item.ItemId == Resource.Id.menu_add_expense)
            {
                this.ViewModel.CurrentTripMenuNavigationActionsWrapper.AddExpenseCommand.Execute(null);
                return true;
            }
            if(item.ItemId == Resource.Id.menu_message_dispatch)
            {
                this.ViewModel.CurrentTripMenuNavigationActionsWrapper.MessageDispatchCommand.Execute(null);
                return true;
            }
            if(item.ItemId == Resource.Id.menu_call_dispatch)
            {
                this.ViewModel.CurrentTripMenuNavigationActionsWrapper.CallDispatchCommand.Execute(null);
                return true;
            }
            return false;
        }
    }
}