using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Views;
using DGenix.Mobile.Fwk.Droid.Views;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.ViewModels;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Views
{
    public abstract class IDYCCachingFragmentCompatActivity<TViewModel> : BaseCachingFragmentCompatActivity<TViewModel>, IDrawerLayoutBaseCachingFragmentCompatActivity
        where TViewModel : IDYCViewModel
    {
        private DrawerLayout _drawerLayout;

        protected abstract int DrawerLayoutId { get; }

        public DrawerLayout DrawerLayout => this._drawerLayout;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            this._drawerLayout = this.FindViewById<DrawerLayout>(this.DrawerLayoutId);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch(item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.HideSoftKeyboard();

                    switch(this.DrawerLayout.GetDrawerLockMode(GravityCompat.Start))
                    {
                        case DrawerLayout.LockModeLockedClosed:
                            this.SupportFragmentManager.PopBackStackImmediate();
                            return true;
                        case DrawerLayout.LockModeUnlocked:
                            this.DrawerLayout.OpenDrawer(GravityCompat.Start);
                            return true;
                    }
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        public override void OnBackPressed()
        {
            this.HideSoftKeyboard();

            if(this.DrawerLayout != null && this.DrawerLayout.IsDrawerOpen(GravityCompat.Start))
                this.DrawerLayout.CloseDrawers();
            else
                base.OnBackPressed();
        }
    }
}