﻿using Android.App;
using Android.Views;
using Android.Widget;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.Models;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Adapters
{
	public class SpinnerAdapter : ArrayAdapter<Day>
	{
		private Activity activity;

		private Day[] values;

		public SpinnerAdapter(Activity activity, int textViewResourceId, Day[] values)
			: base(activity, textViewResourceId, values)
		{
			this.activity = activity;
			this.values = values;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var spinnerNameView = this.activity.LayoutInflater.Inflate(Resource.Layout.item_spinner_name, null);
			var label = spinnerNameView.FindViewById<TextView>(Resource.Id.txt_name);
			label.Text = values[position].Name;

			return spinnerNameView;
		}

		public override View GetDropDownView(int position, View convertView, ViewGroup parent)
		{
			var spinnerDropdownNameView = this.activity.LayoutInflater.Inflate(Resource.Layout.item_spinnerdropdown_name, null);
			var label = spinnerDropdownNameView.FindViewById<TextView>(Resource.Id.txt_name);
			label.Text = values[position].Name;

			return spinnerDropdownNameView;
		}
	}
}
