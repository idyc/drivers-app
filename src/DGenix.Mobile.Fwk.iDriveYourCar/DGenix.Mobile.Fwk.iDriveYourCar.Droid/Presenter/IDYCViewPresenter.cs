﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MvvmCross.Core.ViewModels;
using System.Linq;
using DGenix.Mobile.Fwk.Droid.Presenters;
using DGenix.Mobile.Fwk.iDriveYourCar.Core.MvxHints;
using DGenix.Mobile.Fwk.Droid.Views;
using Android.Support.V4.App;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter
{
    public class IDYCViewPresenter : BaseFragmentsViewPresenter
    {
        private IEnumerable<Type> rootViewModels;

        public IDYCViewPresenter(
            IEnumerable<Assembly> androidViewAssemblies,
            IEnumerable<Type> rootViewModels)
            : base(androidViewAssemblies)
        {
            this.rootViewModels = rootViewModels;
        }

        public override void ChangePresentation(MvxPresentationHint hint)
        {
            if(hint.GetType() == typeof(ClearBackStackHint))
            {
                var fragmentHost = this.GetActualFragmentHost();
                if(fragmentHost == null)
                    return;

                var activity = fragmentHost as BaseCachingFragmentCompatActivity;
                activity.HideSoftKeyboard();
                var transactionToRemove = activity.SupportFragmentManager.GetBackStackEntryAt(1);
                activity.SupportFragmentManager.PopBackStack(transactionToRemove.Name, FragmentManager.PopBackStackInclusive);
                var root = activity.SupportFragmentManager.Fragments.FirstOrDefault(f => f is IIDYCRootFragment);
                (root as IIDYCRootFragment)?.ResetFragment();

                return;
            }
            base.ChangePresentation(hint);
        }

        protected override Android.Content.Intent CreateIntentForRequest(MvxViewModelRequest request)
        {
            var intent = base.CreateIntentForRequest(request);

            if(this.rootViewModels.Contains(request.ViewModelType))
                intent.AddFlags(Android.Content.ActivityFlags.ClearTask);

            return intent;
        }
    }
}
