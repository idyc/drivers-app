﻿using System;
using Android.Support.V4.App;

namespace DGenix.Mobile.Fwk.iDriveYourCar.Droid.Presenter
{
    public interface IIDYCRootFragment
    {
        void ResetFragment();
    }
}
